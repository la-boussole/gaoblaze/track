{ trackSrc ? { outPath = ./.; revCount = 0; shortRev = "abcdef"; rev = "HEAD";}
, officialRelease ? false
, includeShells ? false
, nixpkgs ? <nixpkgs> }:
let
  androidSdkLicenseAccepted = true;
  pkgs = import nixpkgs { config = { android_sdk.accept_license = androidSdkLicenseAccepted; }; overlays = []; };
  project = import ./nix { inherit pkgs; };
  version = "0.1" + (if officialRelease then "" else "pre${toString trackSrc.revCount}_${trackSrc.shortRev}");
  gitignoreSrc = pkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore";
    rev = "7415c4feb127845553943a3856cbc5cb967ee5e0";
    sha256 = "sha256:1zd1ylgkndbb5szji32ivfhwh04mr1sbgrnvbrqpmfb67g2g3r9i";
  };
  minimalTrackSrc = if (pkgs.lib.canCleanSource ./.) then (pkgs.lib.cleanSourceWith {
    filter = pkgs.lib.cleanSourceFilter;
    src = ./.;
    name = "source";
  }) else (./.);
  inherit (import gitignoreSrc { inherit (pkgs) lib; }) gitignoreSource;
in
  let trackPureSource = gitignoreSource minimalTrackSrc;
in
  let
    mainJobs = rec {
      nodeDependencies = project.npmDeps {
        root = trackPureSource;
        development = true;
        override = {
          postInstall = ''
            ln -s $out/lib/node_modules/track/node_modules/.bin $out/bin
          '';
        };
      };
      mavenAndNpmDependencies = (project.mavenAndNpmDeps trackPureSource nodeDependencies).drv;
      releaseTrackApp = project.release {
        build-type = "release";
        finalDependencies = { drv = mavenAndNpmDependencies; shell = (project.mavenAndNpmDeps trackPureSource nodeDependencies).shell; };
        src = trackPureSource;
      };
    };
  shells = {
    generate-maven-and-npm-deps-shell = project.generate-maven-and-npm-deps-shell mainJobs.nodeDependencies;
    mavenAndNpmDependencies-shell = (project.mavenAndNpmDeps trackPureSource mainJobs.nodeDependencies).shell;
  };
in
  mainJobs // (pkgs.lib.optionalAttrs includeShells shells)
