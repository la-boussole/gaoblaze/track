# Gao & Blaze

Gao & Blaze is a casual free open source video game developed by <http://laboussole.coop>

This serious game is designed to educate users about privacy and data protection by making them play with the permissions they grant to the applications installed on their phones.

Gao & Blaze intends to target Android mobile devices.
You can download the game through F-Droid, Play Store, or from our website <https://gaoandblaze.org>

The application is built on top of the React Native framework.

Besides, you will find a quick guide to set-up a dev environment (Linux & macOS).

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.fr.laboussole.track/)

## License

This game is distributed under the free license :
- AGPL v3 ( https://www.gnu.org/licenses/agpl-3.0.html ) for the code (please check additional termes in the license section concerning publicity)
- Creative Commons CC BY-SA v4.0 International ( https://creativecommons.org/licenses/by-sa/4.0/ ) for creations outside the code (except for embedded works under third-party licenses)
Please check https://gaoandblaze.org/licence for the third-party licenses list.

Authorship (2021): La Boussole and association MAIF Prévention.

"Gao&Blaze", "Gao & Blaze", "Gao and Blaze", "Gao y Blaze" and "Gao et Blaze" are registered trademarks.

If you have any questions about the license, use, modification or redistribution of Gao&Blaze, please contact contact@gaoandblaze.org .

# Setup a dev environment

https://reactnative.dev/docs/getting-started

## macOS

You choose the **React Native CLI Quickstart** version of the tutorial, then you take:

- Development OS: **macOS**
- Target OS: **Android**

Technically you don't need Xcode since we don't build for iOS, but you need the **Command Line Tools** installed with
https://apps.apple.com/us/app/xcode/id497799835?mt=12

```sh
xcode-select --install
```

... and if necessary update them with:

```sh
softwareupdate --install -a
```

You need Android Studio:

```sh
brew cask install android-studio
brew cask install android-sdk
```

You will also need https://nodejs.org/ and:

```sh
brew install yarn
```

## Linux

Ensure that you have `yarn` & recent Node.js (tested with `v12.9.0`) from whatever you prefer, refer to <https://classic.yarnpkg.com/en/docs/install/> for more information.

You can skip to "Running the app" or continue to read if you want tips on your text editor.

## Visual Studio Code

https://code.visualstudio.com/docs/nodejs/reactjs-tutorial

### Extensions

- https://marketplace.visualstudio.com/items?itemName=msjsdiag.vscode-react-native
- https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome

## Chrome

- https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en

# Running the app

Choose (again) the _Android_ version of the tutorial: https://reactnative.dev/docs/getting-started

```sh
git clone https://gitlab.com/la-boussole/track-members/track
```

In the `track` folder:

```sh
yarn install # install node dependencies
```

Run an Android Virtual Device (AVD) or connect your physical device (see below), and:

```sh
FLAVOR=fullGaoGames yarn run install:android # install .apk on device
```

Start a metro development server and launch the app on device:

```sh
yarn start # start a metro development server
```

## Emulator

You can create an AVD, ensure you have proper set up virtualization, downloaded system images, etc.

```sh
avdmanager create avd -n my_emulator <your options to choose an ABI/platform/etc.>
```

Then you can run it:

```sh
emulator my_emulator
```

Theorically, React Native will pick up everything automatically.

## On a Physical Device

https://reactnative.dev/docs/running-on-device (_Android_ version, again)

```sh
adb devices # list android devices
adb -s <device> reverse tcp:8081 tcp:8081 # reverse port
```

We have a custom `adb` command in our `package.json` that sets you up automatically:

```sh
yarn run adb
```

# Learn React & MobX

While the stuff is downloading/installing I urge you to read https://reactjs.org/tutorial/tutorial.html which is a very good introduction to React!

Then you can read https://mobx.js.org which is what we use in the app for _state management_ ;)

Finally, if you want to train try to do mini tricks with https://github.com/facebook/create-react-app

## (optional) extra tools

- How to use https://docs.fastlane.tools/actions/supply/

```sh
brew install fastlane
```

**Note** : `fastlane` is not well supported on Linux.

- How to export `.ai` from Adobe Illustrator CC 2017:

  https://helpx.adobe.com/fr/illustrator/using/collect-assets-export-for-screens.html

# Emergency cheatsheet (FAQ)

## `Gradle deprecated` error

If build fail with error `Gradle deprecated`, try this:

```sh
yarn run jetify
```

... you won't be disappointed!

## `port 8081 is already in use`

N.B .: to kill the an already running metro server if necessary:

```sh
npx kill-port 8081
```

## Clean the cache: `watchman`, `yarn`, `react-native` package

```sh
### 1. Clear watchman watches:
watchman watch-del-all
### 2. Delete node_modules:
rm -rf node_modules
### then run :
yarn install
### 3. Reset Metro's cache:
yarn start --reset-cache
### 4. Remove the cache:
rm -rf /tmp/metro-*
```

## Translation with `i18n`

Import function from `i18n`:

```js
import { useTranslation } from "react-i18next"
```

... and just before the export class, add decorator:

```js
@withTranslation ()
```

... also don't forget to add the variable `t` to the props in the constructor:

```js
constructor (props, t) {...}
```

... then the syntax is:

```js
{
  t("my text")
}
```

Note: the files `en/*`, `es.json`, `fr.json` are in `app/components/i18n`

Weblate will extract automatically from those files (https://www.i18next.com/translation-function/essentials)

# `README.md` from the code boilerplate we use

This is the boilerplate that [Infinite Red](https://infinite.red) uses as a way to test bleeding-edge changes to our React Native stack.

Currently includes:

- React Native
- React Navigation
- MobX State Tree
- TypeScript
- And more!

## Quick Start

The Ignite Bowser boilerplate project's structure will look similar to this:

```
ignite-project
├── app
│   ├── components
│   ├── i18n
│   ├── utils
│   ├── models
│   ├── navigation
│   ├── screens
│   ├── services
│   ├── theme
│   ├── app.tsx
├── storybook
│   ├── views
│   ├── index.ts
│   ├── storybook-registry.ts
│   ├── storybook.ts
├── test
│   ├── __snapshots__
│   ├── storyshots.test.ts.snap
│   ├── mock-i18n.ts
│   ├── mock-reactotron.ts
│   ├── setup.ts
│   ├── storyshots.test.ts
├── README.md
├── android
│   ├── app
│   ├── build.gradle
│   ├── gradle
│   ├── gradle.properties
│   ├── gradlew
│   ├── gradlew.bat
│   ├── keystores
│   └── settings.gradle
├── ignite
│   ├── ignite.json
│   └── plugins
├── index.js
├── ios
│   ├── IgniteProject
│   ├── IgniteProject-tvOS
│   ├── IgniteProject-tvOSTests
│   ├── IgniteProject.xcodeproj
│   └── IgniteProjectTests
├── .env
└── package.json

```

### ./app directory

Included in an Ignite boilerplate project is the `app` directory. This is a directory you would normally have to create when using vanilla React Native.

The inside of the src directory looks similar to the following:

```
app
│── components
│── i18n
├── models
├── navigation
├── screens
├── services
├── theme
├── utils
└── app.tsx
```

**components**
This is where your React components will live. Each component will have a directory containing the `.tsx` file, along with a story file, and optionally `.presets`, and `.props` files for larger components. The app will come with some commonly used components like Button.

**i18n**
This is where your translations will live if you are using `react-native-i18n`.

**models**
This is where your app's models will live. Each model has a directory which will contain the `mobx-state-tree` model file, test file, and any other supporting files like actions, types, etc.

**navigation**
This is where your `react-navigation` navigators will live.

**screens**
This is where your screen components will live. A screen is a React component which will take up the entire screen and be part of the navigation hierarchy. Each screen will have a directory containing the `.tsx` file, along with any assets or other helper files.

**services**
Any services that interface with the outside world will live here (think REST APIs, Push Notifications, etc.).

**theme**
Here lives the theme for your application, including spacing, colors, and typography.

**utils**
This is a great place to put miscellaneous helpers and utilities. Things like date helpers, formatters, etc. are often found here. However, it should only be used for things that are truely shared across your application. If a helper or utility is only used by a specific component or model, consider co-locating your helper with that component or model.

**app.tsx** This is the entry point to your app. This is where you will find the main App component which renders the rest of the application. This is also where you will specify whether you want to run the app in storybook mode.

### ./ignite directory

The `ignite` directory stores all things Ignite, including CLI and boilerplate items. Here you will find generators, plugins and examples to help you get started with React Native.

### ./storybook directory

This is where your stories will be registered and where the Storybook configs will live

### ./test directory

This directory will hold your Jest configs and mocks, as well as your [storyshots](https://github.com/storybooks/storybook/tree/master/addons/storyshots) test file. This is a file that contains the snapshots of all your component storybooks.

## Running Storybook

From the command line in your generated app's root directory, enter `yarn run storybook`
This starts up the storybook server.

In `app/app.tsx`, change `SHOW_STORYBOOK` to `true` and reload the app.

For Visual Studio Code users, there is a handy extension that makes it easy to load Storybook use cases into a running emulator via tapping on items in the editor sidebar. Install the `React Native Storybook` extension by `Orta`, hit `cmd + shift + P` and select "Reconnect Storybook to VSCode". Expand the STORYBOOK section in the sidebar to see all use cases for components that have `.story.tsx` files in their directories.

## Previous Boilerplates

- [2017 aka Andross](https://github.com/infinitered/ignite-andross)
- [2016 aka Ignite 1.0](https://github.com/infinitered/ignite-ir-boilerplate-2016)

## Premium Support

[Ignite CLI](https://infinite.red/ignite), [Ignite Andross](https://github.com/infinitered/ignite-andross), and [Ignite Bowser](https://github.com/infinitered/ignite-bowser), as open source projects, are free to use and always will be. [Infinite Red](https://infinite.red/) offers premium Ignite support and general mobile app design/development services. Email us at [hello@infinite.red](mailto:hello@infinite.red) to get in touch with us for more details.


## Run in Visual Studio

npx react-native run-android --variant fullGaoGamesDebug --appId com.fr.laboussole.track.gao.full

## Generate release

npx react-native run-android --variant fullGaoGamesRelease --appId com.fr.laboussole.track.gao.full
