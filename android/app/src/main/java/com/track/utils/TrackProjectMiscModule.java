package com.track.utils;

import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.Manifest;
import android.util.Base64;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.annotation.RequiresApi;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.*;

import android.provider.AlarmClock;
import android.content.Intent;
import android.app.WallpaperManager;

import java.net.URL;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Arrays;
import android.app.ActivityManager;
import android.os.Build;


// It will contain specifically misc functions which requires to be in Java world rather than in JavaScript.

@RequiresApi(api = 26)
public class TrackProjectMiscModule extends ReactContextBaseJavaModule {
  public static ReactApplicationContext reactContext;

  //FIXME: Color is horribly limited before android version 4.4.4 api 26
  private static final int COLOR_SENSIBILITY = Color.parseColor("#5D963C");
  private static final double PERCENT = 0.05;
  // FIXME: Before API 26 construction a color is HORRIBLE disabled before getting a fix
  // private static final String COLOR_SENSIBILITY_KEY = "COLOR_SENSIBILITY";
  private static final int FAILED_TO_READ_IMAGE = 0x1;
  private static final String FAILED_TO_READ_IMAGE_KEY = "FAILED_TO_READ_IMAGE";
  public static String LAST_ALARM_GAO_THE_SPY = "";

  public TrackProjectMiscModule(ReactApplicationContext context) {
    super(context);
    reactContext = context;
  }

  @Override
  public String getName() {
    return "TrackProjectMisc";
  }

  @RequiresApi(api = 26)
  /**
   * Compare two colors represented as int with a color sensibility
   */
  public boolean compareRGBColors(int a, int b, int s) {
    return (
      (Color.red(a) - Color.red(b)) * (Color.red(a) - Color.red(b)) +
        (Color.green(a) - Color.green(b)) * (Color.green(a) - Color.green(b)) +
        (Color.blue(a) - Color.blue(b)) * (Color.blue(a) - Color.blue(b)) <=
        Color.red(s) * Color.red(s) + Color.blue(s) * Color.blue(s) + Color.green(s) * Color.green(s)
    );
  }

  @RequiresApi(api = 26)
  /**
   * Count numbers of pixel of color c represented as an int
   */
  public boolean percentageOfColorInBitmap(Bitmap img, int c1, int c2) {
    int[] q = qtyOfColorInBitmap(img, c1, c2);
    return 1. * q[0] / (img.getWidth() * img.getHeight()) >= PERCENT && 1. * q[1] / (img.getWidth() * img.getHeight()) >= PERCENT;
  }

  public int[] qtyOfColorInBitmap(Bitmap img, int c1, int c2) {
    int[] acc = new int[]{0, 0};
    for (int x = 0; x < img.getWidth(); x++) {
      for (int y = 0; y < img.getHeight(); y++) {
        int currentColor = img.getPixel(x, y);
        acc[0] += compareRGBColors(currentColor, c1, COLOR_SENSIBILITY) ? 1 : 0;
        acc[1] += compareRGBColors(currentColor, c2, COLOR_SENSIBILITY) ? 1 : 0;
      }
    }

    return acc;
  }

  @RequiresApi(api = 26)
  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();

    // Color.getComponents() is not usable before API26 on int colors.
    // constants.put(COLOR_SENSIBILITY_KEY, COLOR_SENSIBILITY);
    constants.put(FAILED_TO_READ_IMAGE_KEY, FAILED_TO_READ_IMAGE);

    return constants;
  }

  @RequiresApi(api = 26)
  @ReactMethod
  public void percentageOfColor(String imageURI, String color1, String color2, Promise promise) {
    try {
      InputStream inputStream = new URL(imageURI).openStream();
      Bitmap img = BitmapFactory.decodeStream(inputStream);

      if (img == null) {
        promise.reject(FAILED_TO_READ_IMAGE_KEY);
      }

      promise.resolve(percentageOfColorInBitmap(img, Color.parseColor(color1), Color.parseColor(color2)));
    } catch (Exception ex) {
      promise.reject(ex.getMessage());
    }
  }

  @ReactMethod
  public void addAlarm(int hour, int minutes, String message) {
    Intent alarmIntent = new Intent(AlarmClock.ACTION_SET_ALARM);
    alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    alarmIntent.putExtra(AlarmClock.EXTRA_MESSAGE, message);
    alarmIntent.putExtra(AlarmClock.EXTRA_HOUR, hour);
    alarmIntent.putExtra(AlarmClock.EXTRA_MINUTES, minutes);
    alarmIntent.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
    alarmIntent.putExtra(AlarmClock.EXTRA_RINGTONE, AlarmClock.VALUE_RINGTONE_SILENT);
    reactContext.startActivity(alarmIntent);
  }

  @ReactMethod
  public void getApplicationList(Promise promise) {
        PackageManager packageManager = reactContext.getPackageManager();
        List<ApplicationInfo> list = packageManager.getInstalledApplications(0);
        String result = "";
        for (int i = 0; i < list.size(); i++) {
            try {
              ApplicationInfo applicationInfo = list.get(i);
              PackageInfo info = packageManager.getPackageInfo(applicationInfo.packageName, 0);
              result += (i > 0 ? "\n" : "") + applicationInfo.packageName + ":" + applicationInfo.loadLabel(packageManager) + ":" + info.versionName + ":" + info.versionCode;
            } catch(Exception e) {

            }
        }
        promise.resolve(result);
    }

  @ReactMethod
  public void getLastAlarmGaoTheSpy(Promise promise) {
    promise.resolve(LAST_ALARM_GAO_THE_SPY);
  }

  @ReactMethod
  public void loadWallpaper(String image, Promise promise) {

    WallpaperManager wallpaperManager = WallpaperManager.getInstance(reactContext);
    try {
      Bitmap img;
      if (image.startsWith("http")) {
        InputStream inputStream = new URL(image).openStream();
        img = BitmapFactory.decodeStream(inputStream);
      } else {
        int resourceId = reactContext.getResources().getIdentifier(image, "drawable", reactContext.getPackageName());
        img = BitmapFactory.decodeResource(reactContext.getResources(), resourceId);
      }
      wallpaperManager.setBitmap(img);
      promise.resolve("Done");
    } catch (Exception e) {
      promise.reject(e.getMessage());
    }
  }

  @ReactMethod
  public void copyFile(String source, String to, Promise promise) {
        try {
            int resourceId = reactContext.getResources().getIdentifier(source, "drawable", reactContext.getPackageName());
            if(source.contains("pdf")) {
              resourceId = reactContext.getResources().getIdentifier(source, "raw", reactContext.getPackageName());
            }
            File file = new File(to);
            InputStream stream = reactContext.getResources().openRawResource(resourceId);
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            OutputStream outStream = new FileOutputStream(file);
            outStream.write(buffer);
            outStream.flush();
            outStream.close();
            stream.close();
            promise.resolve("Done");
        } catch (Exception e) {
            promise.reject(e.getMessage());
        }
    }

  @ReactMethod
  public void getWallpaperBase64(Promise promise) {
    WallpaperManager wallpaperManager = WallpaperManager.getInstance(reactContext);
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

      Bitmap bitmap = ((BitmapDrawable) wallpaperManager.getDrawable()).getBitmap();
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
      byte[] byteArray = byteArrayOutputStream.toByteArray();

      promise.resolve(Base64.encodeToString(byteArray, Base64.DEFAULT));

    } catch (Exception e) {
      e.printStackTrace();
    }
    promise.resolve(null);
  }

  @ReactMethod
  public void loadWallpaperFromBase64(String image) {
    WallpaperManager wallpaperManager = WallpaperManager.getInstance(reactContext);
    try {
      byte[] imageBytes = Base64.decode(image, Base64.DEFAULT);
      Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
      wallpaperManager.setBitmap(decodedImage);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @ReactMethod
  public void getApplicationImage(String packageName, Promise promise) {
        try {
            PackageManager packageManager = reactContext.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
            Drawable drawable = applicationInfo.loadIcon(packageManager);
            Bitmap bitmap = drawableToBitmap(drawable);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            promise.resolve(encoded);
        } catch (PackageManager.NameNotFoundException e) {
            promise.resolve(null);
        }
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

  @ReactMethod
  public void clearAppData(Promise promise) {
    try {
      // clearing app data
      if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
        ((ActivityManager)reactContext.getSystemService(reactContext.ACTIVITY_SERVICE)).clearApplicationUserData(); // note: it has a return value!
      } else {
        String packageName = reactContext.getPackageName();
        Runtime runtime = Runtime.getRuntime();
        runtime.exec("pm clear "+packageName);
      }
      promise.resolve("Done");

    } catch (Exception e) {
      promise.resolve("Error " + e.getMessage());
    }
  }
}
