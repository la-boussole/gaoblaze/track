package com.track.utils;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;
import java.util.Date;


public class AlarmReceiver extends BroadcastReceiver {

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  @Override
  public void onReceive(Context context, Intent intent) {
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    try {
      TrackProjectMiscModule.LAST_ALARM_GAO_THE_SPY = new Date(alarmManager.getNextAlarmClock().getTriggerTime()).toString();
    } catch (Exception e) {
      TrackProjectMiscModule.LAST_ALARM_GAO_THE_SPY = "<error>";
    }
  }
}
