Gao&Blaze es un juego de investigación narrativa. Al unirse a la comunidad en línea formada en torno a jueguitos gratis, los Gao Games, conocerás poco a poco a otros jugadoras y jugadores y descubrirás sus historias. Y como en cualquier pequeña comunidad perfecta, no hay nada que ocultar, ¿verdad?

<b>Advertencia</b>
Este juego respeta tu privacidad y NO recoge tus datos sin tu conocimiento.
Es software libre con licencia CC BY-SA 4.0 y AGPL 3.0 ( gaoandblaze.org ).
Ese juego trata de varios temas, incluso el ciber-acoso. 

Gao & Blaze - Gao et Blaze - Gao and Blaze
