module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  env: {
    production: {},
  },
  plugins: [
     ["wildcard", {
        "exts": ["js", "es6", "es", "jsx", "javascript", "json", "ts", "tsx"]
    }],
    [
      "@babel/plugin-proposal-decorators",
      {
        legacy: true,
      },
    ],
    ["@babel/plugin-proposal-optional-catch-binding"],
    ["macros"],
    ["import-customization", { env: "APP_FLAVOUR", legacy: true }],
    [
      "module-resolver",
      {
        root: ["./app"],
        extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
        alias: {},
      },
    ],
  ],
}
