import {ImageStyle, StyleSheet, TextStyle, View, ViewStyle} from "react-native"
import {color, spacing, typography} from "gao-theme/index"
import { palette } from "./palette"


/* *** Utils *** */

export const DEBUG: ViewStyle = {
  borderWidth: 1,
  borderColor: "red",
}

export const wrapperView: ViewStyle = {
  flex: 1,
}

export const homeButton: TextStyle = {
  textAlign: "center",
}

export const wrapperColumnView: ViewStyle = {
  flex: 1,
  flexDirection: "column"
}

export const spacerView: ViewStyle = wrapperView

export const containerKeyboardAvoidingView: ViewStyle = {
  ...spacerView,

}

/* *** Fonts *** */

export const primaryFontText: TextStyle = {
  fontFamily: typography.primary,
}

export const secondaryFontText: TextStyle = {
  fontFamily: typography.secondary,
}

/* *** PlayScreen *** */

export const playImage: ImageStyle = {
  resizeMode: "contain",
  width: 110,
  height: 110,
}

export const playText: TextStyle = {
  ...primaryFontText,
  textAlign: "center",
  marginTop: -40,
  paddingBottom: spacing.large,
  fontSize: spacing.small,
  color: color.palette.white,
  width: "60%",
  alignSelf: "center",
  //backgroundColor: 'white'
}

/* *** GaoTopBar *** */

export const topBarView: ViewStyle = {
  position: "absolute",
  width: "100%",
  flexDirection: "row",
  height: spacing.massive,
  paddingHorizontal: spacing.large,
}

export const topBarText: TextStyle = {
  ...primaryFontText,
  flex: 1,
  textAlign: "left",
  textAlignVertical: "center",
  fontSize: spacing.larger,
  color: color.palette.black,
}

export const iconTopImage: ImageStyle = {
  resizeMode: "center",
  width: spacing.huge,
  height: spacing.massive,
}

export const pawnContainerView: ViewStyle = {
  flexDirection: "row",
}

/* *** Checkbox (TODO) *** */

export const BASE_ROOT: ViewStyle = {
  flexDirection: "row",
  alignSelf: "flex-start",
  paddingVertical: spacing.border,
}
export const ROOT: ViewStyle = {
  ...BASE_ROOT,
  marginLeft: spacing.large,
}

export const BASE_DIMENSIONS = {width: spacing.medium, height: spacing.medium}
export const DIMENSIONS = {...BASE_DIMENSIONS}

export const BASE_OUTLINE: ViewStyle = {
  ...BASE_DIMENSIONS,
  marginTop: spacing.border, // finicky and will depend on font/line-height/baseline/weather
  justifyContent: "center",
  alignItems: "center",
  borderWidth: 1,
  borderColor: color.primaryDarker,
  borderRadius: 1,
}
export const OUTLINE: ViewStyle = {...BASE_OUTLINE}

export const BASE_FILL: ViewStyle = {
  width: DIMENSIONS.width - spacing.tiny,
  height: DIMENSIONS.height - spacing.tiny,
  backgroundColor: color.primary,
}
export const FILL: ViewStyle = {...BASE_FILL}

export const BASE_LABEL: TextStyle = {
  fontFamily: typography.primary, //FIXME: check if ok
  color: color.primary,
  paddingLeft: spacing.tiny
}
export const LABEL: TextStyle = {...BASE_LABEL} // TODO: check if ok (used in checkox.tsx only)

/* *** Button *** */

export const buttonGaoTabBaseView: ViewStyle = {
  alignItems: "center",
  width: "38%",
  marginLeft: "4%",
  marginVertical: spacing.none,
  marginTop: -spacing.larger,
  marginBottom: -spacing.smaller,
  paddingVertical: spacing.tiny,
  borderWidth: spacing.tiny,
  borderRadius: spacing.medium,
  borderBottomEndRadius: spacing.none,
  borderBottomLeftRadius: spacing.none,
  borderBottomColor: color.transparent,
  borderColor: color.palette.black,
  backgroundColor: color.backgroundDarker,
  elevation: 0,
}

export const buttonGaoTabBaseViewSelected: ViewStyle = {
  ...buttonGaoTabBaseView,
  marginTop: -(spacing.larger + spacing.smaller),
  paddingVertical: spacing.smaller,
  backgroundColor: color.background,
}

export const buttonGaoBaseView: ViewStyle = {
  elevation: 4,
  justifyContent: "center",
  marginHorizontal: spacing.tiny,
  marginVertical: spacing.tiny,
  paddingHorizontal: spacing.tiny,
  paddingVertical: spacing.tiny,
  borderWidth: spacing.border,
  alignItems: "center",
  borderColor: color.palette.black,
  borderRadius: 55, // FIXME
  /* shadow from https://ethercreative.github.io/react-native-shadow-generator/ */
  shadowColor: color.palette.white,
  shadowOffset: {width: spacing.none, height: 2},
  shadowOpacity: 0.25,
  shadowRadius: 3.84,
  backgroundColor: color.background,
}

export const buttonGaoHomeView: ViewStyle = {
  // FIXME: use flexbox...
  left: "22%",
  width: "50%",
}

// A button without extras.
export const buttonGaoLinkView: ViewStyle = {
  ...buttonGaoBaseView,
  alignItems: "flex-start",
  paddingHorizontal: spacing.none,
  paddingVertical: spacing.none,
  alignSelf: "center",
  width: "40%",
}

// Buttons for Gao the prosecutor form.
export const buttonGaoFormEnabled: ViewStyle = {
  ...buttonGaoBaseView,
  justifyContent: "center",
  // paddingHorizontal: spacing.massive,
  // paddingVertical: spacing.tiny,
  width: "40%",
  alignSelf: "center",
  backgroundColor: color.palette.green,
}

export const buttonGaoFormDisabled: ViewStyle = {
  ...buttonGaoBaseView,
  width: "40%",
  backgroundColor: color.palette.lightGrey
}

export const buttonGaoBaseText: TextStyle = {
  ...primaryFontText,
  paddingHorizontal: spacing.smaller,
  fontSize: spacing.large,
  color: color.palette.black,
}

export const buttonGaoLinkText: TextStyle = {
  ...buttonGaoBaseText,
  paddingHorizontal: spacing.none,
  paddingVertical: spacing.none,
  color: color.text,
}

/* *** Page (Container) *** */

export const mainView: ViewStyle = {
  ...StyleSheet.absoluteFillObject,
  flex: 1,
  paddingTop: spacing.massive,
}

export const mainViewGames: ViewStyle = {
  ...StyleSheet.absoluteFillObject,
  flex: 1,
  paddingTop: spacing.massive,
  flexDirection: "column-reverse",
}

export const mainCommunityView: ViewStyle = {
  ...mainView,
  paddingTop: spacing.none
}

export const mainDialogView: ViewStyle = { // FIXME: duplicated with mainView?
  ...StyleSheet.absoluteFillObject,
  flex: 1,
  justifyContent: "center",
  zIndex: 1,
  alignItems: "center",
}

export const mainDialogScoreView: ViewStyle = { // FIXME: duplicated with mainView?
  ...mainDialogView,
  top: spacing.massive,
}

// FIXME:(beta) too much mainDialogView here ...
export const mainDialogContainerView: ViewStyle = {
  flex: 1,
  marginHorizontal: spacing.large,
  marginBottom: spacing.large,
}

export const mainDialogBoxView: ViewStyle = {
  marginHorizontal: 55,
  marginVertical: 5,
  paddingHorizontal: spacing.large,
  paddingVertical: spacing.large,
  borderRadius: spacing.large,
  borderWidth: spacing.tiny,
  borderColor: color.palette.black,
  backgroundColor: "white",
}

export const iconImageView: ImageStyle = {
  alignSelf: "flex-end",
  height: spacing.larger,
  width: spacing.larger,
  marginTop: spacing.large,
  marginBottom: spacing.tiny,
  borderWidth: 1,
}

export const iconImageSettingsView: ImageStyle = {
  ...iconImageView,
  marginTop: -spacing.larger,
}

export const avatarView: ViewStyle = {
  alignItems: "center",
  marginBottom: spacing.small,
}

export const mainRenderView: ViewStyle = {
  flex: 1,
  justifyContent: "space-between",
}

export const defaultContainerView: ViewStyle = {
  flex: 1,
  borderWidth: 1,
  borderColor: "transparent",
  justifyContent: "center",
}

export const containerViewGaoTheSpy: ViewStyle = {
  alignItems: "center",
  borderWidth: 1,
  borderColor: "transparent",
}

export const containerViewGaoTheSpyRules: ViewStyle = {
  alignItems: "center",
  borderWidth: 1,
  borderColor: "transparent",
  width: "200%",
  height: "200%",
}

export const defaultContainerViewGaoThSpyRight: ViewStyle = {
  borderWidth: 1,
  borderColor: "transparent",
  width: "45%",
  alignSelf: "flex-end",
  justifyContent: "center"
}

export const defaultContainerViewGaoThSpyLeft: ViewStyle = {
  borderWidth: 1,
  borderColor: "transparent",
  width: "45%",
  alignSelf: "flex-start",
  justifyContent: "center"
}

export const containerView: ViewStyle = {
  ...defaultContainerView,
  alignItems: "center",
  overflow: "visible",
}

export const gameContainerView: ViewStyle = {
  ...defaultContainerView,
  alignItems: "center",
}

// Prosecutor
export const containerEnvelopView: ViewStyle = {
  ...defaultContainerView,
  // paddingTop: spacing.medium,
}

// Spy
export const buttonHomeView: ViewStyle = {
  ...defaultContainerView,
  ...StyleSheet.absoluteFillObject,
  flex: 1,
  alignItems: "baseline"
}

// Spy
export const containerFilterViewGaoTheSpy: ViewStyle = {
  ...StyleSheet.absoluteFillObject,
  elevation: 8,
  top: -150,
}

export const envelopeViewGaoTheProsecutor: ViewStyle = {
  paddingHorizontal: spacing.larger,
}

export const contentViewGaoTheForger: ViewStyle = {
  justifyContent: "center",
  alignSelf: "center",
  width: "100%",
  height: "100%",
}

export const centerView: ViewStyle = {
  flex: 1,
  alignItems: "center"
}

export const cameraPhotoButtonGaoThePhotographer: ImageStyle = {
  height: 25,
  width: 25,
  resizeMode: "contain",
  zIndex: 100,
}

export const containerPhotoButtonGaoThePhotographer: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-evenly",
  width: "100%",
  height: "16%",
  alignItems: "center",
}

export const cameraContainerGaoThePhotographer: ViewStyle = {
  width: "100%",
  height: "84%",
  overflow: "hidden",
}

export const cameraGaoThePhotographer: ViewStyle = {height: "100%", width: "100%"}

export const containerBoxViewPlayMenu: ViewStyle = { // FIXME
  alignItems: "center",
  height: "100%",
  justifyContent: "center",
  alignContent: "center",
  marginTop: -40,
}


export const containerSettingsView: ViewStyle = {
  ...centerView,
  justifyContent: "center"
}

export const exploreView: ViewStyle = {
  flexDirection: "row",
  flex: 1,
  backgroundColor: color.palette.white,
  marginBottom: spacing.smaller,
  paddingVertical: spacing.smaller,
  paddingLeft: spacing.smaller,
  elevation: 3
}

export const mainTitleView: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  marginHorizontal: spacing.medium,
}

export const flatListView: ViewStyle = {
  marginHorizontal: spacing.huge,
  marginTop: spacing.huge,
  marginBottom: 40
}

export const tradePawsContainer: ViewStyle = {
  height: "55%",
  width: "80%",
  alignSelf: "center"
}

export const mainDialogAdviceView: ViewStyle = {
  flex: 1,
  position: "absolute", // FIXME
  bottom: -spacing.medium, // FIXME
  right: 130, // FIXME
  zIndex: 1, // FIXME
}

export const gaoBubbleContainer: ViewStyle = {
  position: "absolute",
  right: -45,
  bottom: -60,
  width: 340,
  height: 220,
  zIndex: 2
}

export const gaoBubbleBackground: ViewStyle = {
  width: 300,
  height: 213
}

export const gaoSageCatGif: ViewStyle = {
  width: 130,
  height: 130,
  zIndex: 1
}
export const gaoSageCatGifContainer: ViewStyle = {
  bottom: 0,
  right: -15,
  position: "absolute",
  zIndex: 1
}

export const viewGaoAvatarView: ViewStyle = { // used in gao-dialog:gao-avatar
  alignItems: "center",
}

export const viewFlagView: ViewStyle = { // used in BlazeStreamDialog only
  alignItems: "center",
  marginBottom: -spacing.huge, // FIXME
}

export const pawnScoreView: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  marginTop: spacing.small,
}

export const photographerAdviceBoxView: ViewStyle = {
  marginVertical: spacing.medium,
  paddingHorizontal: spacing.small,
  paddingVertical: spacing.small,
  borderRadius: spacing.small,
  borderWidth: spacing.tiny,
  borderColor: color.palette.black,
  backgroundColor: "white",
}

export const adviceBoxView: ViewStyle = {
  ...photographerAdviceBoxView,
  marginHorizontal: spacing.medium,
  borderWidth: spacing.border,
}

export const blackboardSpaceView: ViewStyle = {
  height: "100%",
  flexDirection: "column",
  width: "100%",
  justifyContent: "space-around",
}

export const flexRowView: ViewStyle = {
  width: "100%",
  flexDirection: "row",
  justifyContent: "center",
  flex: 1.5,
  alignItems: "center",
  alignContent: "center",
  alignSelf: "center",
}

export const flexRowViewText: ViewStyle = {
  width: "100%",
  justifyContent: "center",
  flex: 1,
  alignItems: "center",
}

export const tradePawsScreenflexRowView: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "space-evenly",
  marginHorizontal: spacing.big,
}

export const communityScreenflexRowView: ViewStyle = {
  justifyContent: "space-between",
  flexDirection: "row",
  alignItems: "center",
}

export const confidenceRowView: ViewStyle = {
  marginHorizontal: 8,
}

export const communityStarRowView: ViewStyle = {
  marginTop: spacing.smaller,
  flexDirection: "row",
}

export const flexRowViewScore: ViewStyle = {
  flex: 1,
  alignItems: "center",
  justifyContent: "flex-start",
}

/* *** Elements (Components) *** */

export const flatListItemView: ViewStyle = { // TODO: rename me
  flex: 1,
  alignItems: "center",
}

export const customFlatListView: ViewStyle = {
  ...flatListItemView,
  alignContent: "flex-end",
  // paddingBottom: spacing.large,
}

export const flatListScoreView: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  marginBottom: spacing.large,
}

export const buttonView: ViewStyle = {
  marginBottom: -spacing.huge, // FIXME
}

export const timerView: ViewStyle = {
  alignSelf: "center",
  paddingVertical: spacing.none,
  paddingHorizontal: spacing.small,
  marginHorizontal: spacing.none,
  borderWidth: spacing.border,
  borderRadius: spacing.smaller,
  borderColor: color.palette.black,
  backgroundColor: "white",
  position: "absolute",
  zIndex: 100,
  top: -100
}

// Spy
export const timerLeftSpyView: ViewStyle = {
  zIndex: 1,
}

// Spy
export const timerRightSpyView: ViewStyle = {
  zIndex: 1,
}

// Prosecutor
export const timerProsecutorView: ViewStyle = {
  ...timerView,
  zIndex: 3,
  top: -50
}

// Intruder
export const timerIntruderView: TextStyle = {
  ...timerView,
  position: "relative",
  color: "#eb358a",
  fontSize: 80,
  top: 0,
  width: "90%",
  height: "40%",
  textAlign: "center",
  textAlignVertical: "center",
}

/* *** Typography (Text) *** */

export const mainText: TextStyle = {
  ...primaryFontText,
  color: color.text,
  fontSize: spacing.large,
}

export const titleText: TextStyle = {
  ...mainText,
  textAlign: "left",
  width: 300,
  marginBottom: spacing.smaller,
}

// FIXME: not so good..
export const titleTextTradePaws: TextStyle = {
  ...mainText,
  marginLeft: spacing.large,
  marginBottom: spacing.smaller,
}

export const pText: TextStyle = {
  ...secondaryFontText,
  fontSize: spacing.large,
  color: color.palette.black,
}

export const h1Text: TextStyle = {
  ...pText,
  textAlign: "left",
  fontSize: spacing.huge,
  padding: spacing.large,
  paddingBottom: spacing.medium,
}

export const h1TextNoPaddingTop: TextStyle = {
  ...h1Text,
  fontSize: spacing.big,
  padding: spacing.none,
}

export const h1CenterText: TextStyle = {
  ...h1Text,
  paddingTop: spacing.none,
  paddingBottom: spacing.none,
  textAlign: "center",
}

export const h2Text: TextStyle = {
  ...h1Text,
  fontSize: spacing.larger,
  padding: spacing.small,
}

export const h2CompactText: TextStyle = {
  ...pText,
  padding: spacing.none,
  paddingBottom: spacing.none,
  fontSize: 20,
  textAlign: "center",
}

export const h2PurpleText: TextStyle = {
  ...h2Text,
  color: color.palette.purple,
}

export const gaoTheSageQuestionContainer: ViewStyle = {
  width: "100%",
  height: "100%",
  paddingVertical: "10%",
  justifyContent: "space-around"
}

export const chalk: TextStyle = {
  ...pText,
  paddingHorizontal: 30,
  color: "white",
  fontSize: 25,
}

export const h3Text: TextStyle = {
  ...pText,
  padding: spacing.none,
  paddingBottom: spacing.none,
  fontSize: 20,
}

export const h3CompactText: TextStyle = {
  ...h3Text,
  color: color.palette.purple,
  textAlign: "center",
}

export const h4Text: TextStyle = {
  ...pText,
  textAlign: "center",
  fontSize: spacing.large,
  color: color.palette.purple,
}

export const h5Text: TextStyle = {
  ...pText,
  fontSize: spacing.large,
  color: color.palette.grey,
  marginTop: spacing.medium,
  marginBottom: spacing.small
}

export const h6Text: TextStyle = {
  ...pText,
  fontSize: spacing.larger,
  color: color.palette.black,
  paddingBottom: spacing.tiny
}

export const h7Text: TextStyle = {
  ...pText,
  textAlign: "center",
  fontSize: spacing.medium,
  color: color.palette.purple,
}
export const communityNameText: TextStyle = {
  ...pText,
  fontSize: spacing.larger,
  paddingLeft: spacing.small,
  paddingTop: spacing.medium,
}

export const imageButtonText: TextStyle = {
  ...h2Text,
  marginTop: -73, // FIXME
  fontSize: spacing.large,
  textAlign: "center"
}

export const defaultPrimaryText: TextStyle = {
  ...primaryFontText,
  textAlign: "center",
  fontSize: spacing.huge,
  color: color.palette.green,
}

export const defaultSecondaryText: TextStyle = {
  ...defaultPrimaryText,
  fontSize: spacing.semilarge,
  color: color.palette.black,
}

export const defaultSecondaryTextLastMessage: TextStyle = {
  ...defaultPrimaryText,
  fontSize: spacing.large,
  color: color.palette.green,
}

export const pawsScoreText: TextStyle = {
  ...defaultSecondaryText,
  fontSize: spacing.larger,
}

export const defaultThirdText: TextStyle = {
  ...primaryFontText,
  textAlign: "center",
  fontSize: spacing.medium,
  color: color.palette.black,
}

// VAGABOND FLAG
export const titleFlagText: TextStyle = { // vagabond flag
  ...primaryFontText,
  textAlign: "center",
  fontSize: spacing.big,
  color: color.palette.white,
  marginTop: -spacing.massive,
  zIndex: 2,
}

export const baseText: TextStyle = { // FIXME: rename me (used in gao athlete for number of steps)
  ...primaryFontText,
  textAlign: "center",
  fontSize: spacing.huge,
  color: color.palette.red,
}

export const timerText: TextStyle = {
  ...baseText,
  fontSize: spacing.medium,
}

export const timerSpyText: TextStyle = {
  ...timerText,
  fontSize: spacing.huge,
  color: color.palette.pink,
}

export const playerStepsBlueText: TextStyle = {
  ...baseText,
  color: color.palette.blue,
}

export const colorFontGameOverText: TextStyle = {
  color: color.palette.red,
}

export const playerStepsRedText: TextStyle = {
  ...playerStepsBlueText,
  ...colorFontGameOverText,
}

export const playerStepsText: TextStyle = playerStepsRedText

export const gaoStepsText: TextStyle = {
  ...playerStepsText,
  marginBottom: -spacing.large, // FIXME
  fontSize: spacing.huge,
  color: color.text,
}

export const infoText: TextStyle = {
  ...primaryFontText,
  fontSize: spacing.medium,
  color: color.text,
}

export const loadingTextGaoTheForger: TextStyle = {
  ...infoText,
  textAlign: "center",
}

export const messageText: TextStyle = {
  ...primaryFontText,
  flex: 1,
  alignSelf: "center",
  color: color.palette.white,
  fontSize: spacing.larger,
}

export const blackboardNumberText: TextStyle = {
  ...primaryFontText,
  textAlign: "center",
  fontSize: spacing.huge,
  color: color.palette.white,
}

export const infoAlarmsText: TextStyle = {
  ...primaryFontText,
  textAlign: "center",
  fontSize: spacing.small,
  color: color.palette.pink,
  marginTop: -10
}

export const blackboardText: TextStyle = {
  ...primaryFontText,
  color: color.palette.white,
  fontSize: spacing.large,
}

export const countdownText: TextStyle = {
  ...primaryFontText,
  textAlign: "center",
  alignSelf: "center",
  width: 180, // FIXME
  marginVertical: spacing.huge,
  borderWidth: 3,
  borderRadius: 100, // FIXME
  borderColor: color.text,
  fontSize: 130, // FIXME
  color: color.text,
  elevation: 3
}


export const countdownTextForger: TextStyle = {
  ...countdownText,
  position: "absolute"
}

/* *** Images (Icons) *** */


export const notificationIcon: ImageStyle = {
  position: "absolute",
  top: 7,
  right: -3,
  width: 18,
  height: 18,
  alignItems: "center",
  justifyContent: "center",
  //elevation: 10, // trick: work better than zIndex here
  zIndex: 1000
}

export const editIcon: ImageStyle = {
  position: "absolute",
  top: -3,
  right: 13,
  width: spacing.large,
  height: spacing.large,
  alignItems: "center",
  justifyContent: "center",
  alignSelf: "center",
  zIndex: 100,
}

export const pawnImage: ImageStyle = {
  height: spacing.large,
  width: spacing.large,
  marginTop: spacing.smaller,
}

export const avatarImage: ImageStyle = {
  height: spacing.avatar,
  width: spacing.avatar,
}

export const avatarBlazeWelcomeImage: ImageStyle = {
  height: 170,
  width: spacing.avatar,
}

export const avatarBlazeVagabondImage: ImageStyle = {
  ...avatarBlazeWelcomeImage,
  height: 200,
}

export const defaultDialogImage: ImageStyle = { // FIXME
  height: 85,
  width: 90,
  zIndex: 2,
  top: 40,
}

export const flagDialogImage: ImageStyle = {
  height: spacing.tall,
  resizeMode: "contain",
  marginBottom: -spacing.small,
  zIndex: 2,
}

export const flagDialogCelebration: ImageStyle = {
  ...flagDialogImage,
  height: spacing.massive,
  resizeMode: "contain",
  marginBottom: -spacing.small,
  zIndex: 2,
}

export const mapCanvaImage: ImageStyle = {
  flex: 1,
  resizeMode: "contain",
}

export const pawnDialogImage: ImageStyle = {
  height: 53, // exact height fo gaodialog 'score' pawns
  width: spacing.huge,
  resizeMode: "center"
}

export const iconImage: ImageStyle = {
  ...pawnDialogImage,
  position: "absolute",
  marginTop: spacing.larger,
  zIndex: 2,
}

export const clockImage: ImageStyle = {
  width: 100,
  height: 100,
  marginTop: spacing.larger,
}

export const checkedIconImage: ImageStyle = {
  position: "absolute",
  resizeMode: "contain",
  width: "100%",
  alignItems: "center",
  height: spacing.larger,
  marginTop: spacing.massive,
  zIndex: 1,
}

export const checkedIconImageGaoTheForge: ImageStyle = {
  position: "absolute",
  resizeMode: "contain",
  width: spacing.larger,
  height: spacing.larger,
  zIndex: 1,
}

export const catGif: ImageStyle = {
  // FIXME: magic constants based on Nexus 5 screen -> use aspectRatio
  height: spacing.avatar,
  width: 220, // TODO: just enough for athlete, check others,
}

export const catCenterGif: ImageStyle = {
  ...catGif,
  alignSelf: "center",
  width: 200, // TODO
}

export const catRightGif: ImageStyle = {
  ...catGif,
  alignSelf: "flex-end",
  width: 130,
  resizeMode: "center",
}

export const pictureGameImageGaoTheForge: ImageStyle = {
  width: "100%",
  height: "100%",
  resizeMode: "stretch",
}

export const pickedImage: ImageStyle = { // TODO: rename centerImage
  alignSelf: "center",
}

export const loadingImage: ImageStyle = pickedImage

export const topIconArrow: ImageStyle = {
  // position: "relative",
  width: spacing.big,
  height: spacing.larger,
}

export const IconTriangle: ImageStyle = {
  width: spacing.large,
  height: spacing.larger,
  marginHorizontal: spacing.medium
}

export const IconCheckBox: ImageStyle = {
  width: spacing.larger,
  height: spacing.larger,
  marginLeft: spacing.tiny,
  marginRight: spacing.large
}


export const IconStar: ImageStyle = {
  width: 20, //FIXME
  height: 20, //FIXME
  marginLeft: spacing.border,
}

/* *** Form (Inputs) *** */

export const borderView: ViewStyle = {
  borderWidth: 1,
  borderColor: color.palette.black,
  alignItems: "center",
  alignSelf: "center"
}

export const formTitleText: TextStyle = {
  alignSelf: "center",
  fontSize: spacing.massive,
  lineHeight: spacing.massive,
  marginTop: spacing.small,
  marginBottom: -spacing.large,
  color: color.palette.black,
}

export const rowContainerView: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: spacing.small,
  marginVertical: spacing.small,
}

export const inputRowContainerView: ViewStyle = {
  ...rowContainerView,
  alignItems: "baseline",
}

export const formKeyText: TextStyle = {
  fontSize: spacing.small,
  color: color.palette.black,
}

export const gaoAdvice: TextStyle = {
  ...formKeyText
}

export const inputText: TextStyle = {
  flex: 1,
  textAlign: "center",
  padding: spacing.none,
  borderTopWidth: spacing.none,
  borderBottomWidth: 1,
  color: color.palette.white,
}

export const submitGaoTheSage: ViewStyle = {
  marginHorizontal: 25,
  marginTop: -30,
}

export const inputTextGaoTheSage: TextStyle = {
  paddingVertical: 0,
  marginTop: spacing.none,
  marginBottom: spacing.huge,
  backgroundColor: color.palette.lighterGrey,
  borderBottomWidth: 0,
  borderRadius: 2,
  color: color.palette.black,
  marginHorizontal: 35
}

export const inputTextWelcome: TextStyle = {
  paddingVertical: 0,
  marginTop: spacing.smaller,
  marginBottom: spacing.huge,
  backgroundColor: color.palette.lighterGrey,
  borderBottomWidth: 0,
  borderRadius: 2,
  color: color.palette.black,
}

export const formValueText: TextStyle = {
  flex: 1,
  alignItems: "center",
  textAlign: "center",
  fontSize: spacing.medium,
  color: color.palette.white,
  borderTopWidth: spacing.none,
  borderBottomWidth: 1,
}

export const questionView: ViewStyle = { // TODO: rename centeredView
  alignItems: "center",
  marginVertical: spacing.medium,
  marginBottom: spacing.big,
}

export const questionText: TextStyle = {
  fontSize: spacing.small,
  marginBottom: spacing.tiny,
  color: color.palette.black,
}

export const wrongText: TextStyle = {
  position: "absolute",
  fontSize: spacing.small,
  top: spacing.huge,
  alignSelf: "center",
  fontWeight: "bold",
  color: color.palette.angry,
}

/* *** Gao Community *** */

export const communityContainerView: ViewStyle = {
  flex: 1,
  paddingVertical: spacing.small,
  paddingHorizontal: spacing.small,
  paddingBottom: 16,
  borderRadius: spacing.large,
  borderWidth: spacing.tiny,
  borderColor: color.palette.black,
  backgroundColor: color.background,
}

export const settingsContainerView: ViewStyle = {
  flex: 1,
  paddingVertical: spacing.small,
  paddingHorizontal: spacing.small,
  borderRadius: spacing.large,
  borderWidth: spacing.tiny,
  borderColor: color.palette.black,
  backgroundColor: color.background,
}

export const profileInsightView: ViewStyle = {
  marginHorizontal: spacing.large,
  marginTop: spacing.tiny,
  paddingVertical: spacing.tiny,
  paddingHorizontal: spacing.large,
  borderRadius: spacing.small,
  backgroundColor: color.palette.white,
}

export const chooseStyle: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: color.transparent
}

export const selectedChooseStyle: ViewStyle = {
  ...chooseStyle,
  backgroundColor: color.primaryDarker,
}

export const chatContainerView: ViewStyle = {
  ...communityContainerView,
  paddingVertical: spacing.none,
  paddingHorizontal: spacing.none,
  backgroundColor: color.palette.greyBlue,
}

export const friendsContainerView: ViewStyle = {
  ...communityContainerView,
  paddingVertical: spacing.none,
  paddingHorizontal: spacing.none,
  backgroundColor: color.palette.greyBlue,
}

export const chatMessagesView: ViewStyle = {
  ...profileInsightView,
  borderRadius: spacing.none,
  marginHorizontal: spacing.none,
  flex: 1,
}

export const chatMessagesViewLoading: ViewStyle = {
  width: "100%",
  position: "absolute",
  backgroundColor: palette.greyBlue,
  height: "100%",
  zIndex: 10,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
  justifyContent: "center",
  alignItems: "center",
}

export const chatMessagesViewEmpty: ViewStyle = {
  width: "100%",
  flex: 1,
  backgroundColor: palette.greyBlue,
  justifyContent: "center",
  alignItems: "center",
  alignSelf: "center",
  padding: 20,
}

export const chatMessagesViewEmptyText: TextStyle = {
  textAlign: "center",
  fontFamily: typography.primary,
  fontSize: 18,
}

export const chatHeaderView: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  padding: 15
}

export const chatCelebrationHeaderView: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  padding: 15,
  height: "40%"
}

export const chatStoryChoice: TextStyle = {
  ...h2PurpleText,
  fontSize: spacing.large,
  borderTopWidth: spacing.border,
  borderColor: color.palette.black,
  color: color.palette.black,
}

export const chatMemesGifsBar: TextStyle = {
  ...chatStoryChoice,
  flexDirection: "row",
  zIndex: 10,
  marginBottom: -spacing.border,
  backgroundColor: color.background,
}

export const settingsTopBar: TextStyle = {
  ...chatStoryChoice,
  marginHorizontal: spacing.small,
  marginTop: spacing.small,
  borderColor: color.palette.grey,
  padding: spacing.none,
  paddingBottom: spacing.none,
}

export const chatMemesGifsBarLinks: TextStyle = {
  ...h2PurpleText,
  fontSize: spacing.large,
  paddingLeft: spacing.border,
  marginVertical: -spacing.small,
  paddingTop: spacing.none,
  paddingBottom: spacing.none,
  textDecorationLine: "underline",
  color: color.palette.darkBlue,
}

export const profileAboutMeView: ViewStyle = {
  ...profileInsightView,
  flex: 1,
  marginBottom: spacing.tiny,
  borderRadius: spacing.none,
  borderColor: color.palette.lightGrey,
  borderBottomWidth: spacing.border,
  borderRightWidth: spacing.border,
}

export const chatButtonGroup: ViewStyle = {
  marginTop: spacing.small,
  marginHorizontal: 0,
}

export const chatButtonGroupLiveStream: ViewStyle = {
  ...chatButtonGroup,
  borderColor: color.palette.black,
  borderWidth: 3,
  height: "100%", // FIXME: depends on image height (now its 200)
}

export const defaultTiles: TextStyle = {
  ...profileInsightView,
  flex: 1,
  flexDirection: "row",
  height: 90,
  marginHorizontal: spacing.smaller,
  borderRadius: spacing.none,
  borderColor: color.palette.lightGrey,
  borderBottomWidth: spacing.border,
  borderRightWidth: spacing.border,
}

export const friendsTiles: TextStyle = {
  ...defaultTiles,
  height: spacing.mega,
  marginHorizontal: spacing.tiny,
  paddingHorizontal: spacing.tiny,
}

export const contactTiles: TextStyle = {
  ...defaultTiles,
  paddingHorizontal: spacing.tiny,
  borderBottomWidth: spacing.none,
  borderRightWidth: spacing.none,
  backgroundColor: color.transparent,
}

export const emptyTiles: TextStyle = {
  ...friendsTiles,
  backgroundColor: color.palette.grey,
  borderBottomWidth: spacing.none,
  borderRightWidth: spacing.none,
}

export const friendAvatar: ImageStyle = {
  width: spacing.massive,
  height: spacing.massive,
  borderColor: color.palette.black, // FIXME
  borderWidth: 3, // FIXME
  borderRadius: 60, // FIXME
}

export const commuProfileAvatar: ImageStyle = {
  ...friendAvatar,
  width: 100,
  height: 100,
}

export const sentMessage: TextStyle = {
  ...pText,
  padding: spacing.small,
  textAlign: "right",
  color: color.palette.darkBlue,
}

export const receivedMessage: TextStyle = {
  ...pText,
  marginRight: "auto",
  padding: spacing.small,
  borderRadius: spacing.smaller,
  backgroundColor: color.background,
  color: color.text,
}

export const exploreImage: ImageStyle = {
  resizeMode: "contain",
  width: "90%",
  marginHorizontal: "5%",
  marginVertical: -20,
}

export const mainSettingsView: ViewStyle = {
  padding: spacing.huge,
  flex: 1,
}

export const blazeRoom: ImageStyle = {
  flex: 1,
  width: 275,
  resizeMode: "contain"
}

export const gaoBubbleAnswer: TextStyle = {
  ...h3CompactText,
  height: 180,
  alignContent: "center",
  textAlignVertical: "center",
  marginHorizontal: 25
}

/*****
 * Splash
 */

export const splashMainContainer: ViewStyle = {
  width: "100%",
  height: "100%",
  backgroundColor: palette.white,
  alignItems: "center",
  zIndex: 100,
}

export const splashWarningContainer: ViewStyle = {
  position: "absolute",
  width: "100%",
  height: "100%",
  backgroundColor: "black",
  alignItems: "center",
  zIndex: 5,
}

export const splashWarningImage: ImageStyle = {
  width: "100%",
  flex: 1,
  resizeMode: "stretch",
  backgroundColor: "black"
}

export const splashBoussoleImage: ImageStyle = {
  width: "80%",
  flex: 2,
  resizeMode: "contain",
}

export const splashMAIF: ViewStyle = {
  flex: 1,
  width: "100%",
  alignItems: "center",
}

export const splashMAIFImages: ViewStyle = {
  flexDirection: "row",
  flex: 1,
}

export const splashMAIFImage: ImageStyle = {
  width: "80%",
  resizeMode: "contain",
  height: "100%"
}

export const splashText: TextStyle = {
  fontFamily: "CupcakeSmiles-Regular",
  fontSize: 32,
}

export const gaoBlazePage: ViewStyle = {
  marginTop: 20,
  width: 250,
  height: 40,
  justifyContent: "center"
}

export const gaoBlazePageText: TextStyle = {
  fontFamily: "PatrickHand-Regular",
  fontSize: 26,
  alignSelf: "center",
  marginTop: -5,
}

export const splashGifContainer: ViewStyle = {
  width: "100%",
  height: "100%",
  position: "absolute",
  backgroundColor: "#fbfbfb",
}

export const splashGif: ImageStyle = {
  width: "80%",
  height: "100%",
  resizeMode: "contain",
  alignSelf: "center",
}

export const filekeeperAdvice: ViewStyle = {
  position: "absolute",
  bottom: 0,
  alignSelf: "center"
}

export const downloadContainer: ViewStyle = {
  width: "80%",
  height: 20,
  alignSelf: "center",
}

export const gaoDownloadContainer: ViewStyle = {
  width: "80%",
  height: 25,
  alignSelf: "center",
}

export const downloadBar: ImageStyle = {
  width: "100%",
  height: "100%",
  resizeMode: "center",
}

export const gaoDownloadBar: ImageStyle = {
  width: "100%",
  height: "100%",
  resizeMode: "center",
  borderWidth: 2,
  borderColor: color.palette.black,
  borderRadius: 5,
}

export const popupContainerDownload: ViewStyle = {
  ...mainDialogBoxView,
  width: "80%",
}

export const matrixImageContainer: ViewStyle = {
  position: "absolute",
  zIndex: 10,
}

export const matrixImage: ImageStyle = {
  width: "100%",
  height: "100%"
}

export const ajayMessage: ViewStyle = {
  position: "absolute",
  width: 120,
  height: 50,
  top: "50%",
  left: "30%"
}

export const ajayMessageImage: ImageStyle = {
  width: "100%",
  height: "100%",
  resizeMode: "contain",
}
