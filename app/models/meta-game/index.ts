import { types, Instance, getRoot } from "mobx-state-tree"
import { CharacterProfileModel } from "models/character-profile"
import PushNotification from "react-native-push-notification"
import { RootStore } from "models/root-store"
import { ShopItem } from "screens/gao-memes-screen"
import RNFS from "react-native-fs"
// eslint-disable-next-line react-native/split-platform-components
import { Image, NativeModules, PermissionsAndroid } from "react-native"
import membersData from "assets/community_members/profile"

export enum GameStyle {
  Undetermined = "Undetermined",
  Free = "Free",
  Realtime = "Realtime",
}

const RANGE_LETTERS = [[0, 15], [16, 30], [31, 50], [51, 70], [71, 100000]]

const REWARD_DIRECTORY = RNFS.DownloadDirectoryPath

const DONT_SEND_NOTIFICATION = [
  "Alex 8",
  "Ally-3a",
]


const SPECIAL_MESSAGE = [
  {
    func: (metaGame, navigation) => {
      metaGame.setShowBlazeMessageGaoTheVagabond()
    },
    messages: ["Go !", "<3!", "Go!"]
  },
  {
    func: (metaGame, navigation) => {
      if (!metaGame.developMode) {
        setTimeout(() => {
          metaGame.setDevelopMode()
          navigation.navigate("blazeAppDownload")
        }, 1000)
      }
    },
    messages: [
      "Tiens, regarde.", "Mira.", "Look."
    ]
  },
  {
    func: (metaGame, navigation) => {
      //setTimeout(() => navigation.navigate("blazeLastMessage"), 2000)
      metaGame.setGoToEnd()
    },
    messages: [
      "1/ Tu laisses tomber Alex pour vivre ta meilleure vie avec les papattes, l’argent et Ally pour seule compagnie. Good luck with that…\n2/ Accepter la proposition d’Ally et du Studio et la compensation financière pour Alex et ceux dont tu as vu les profils (et tant pis pour les autres)\n3/ Tu soutiens Alex dans son combat contre le Studio. Ce ne sera pas facile. Et tu dis adieu aux petits jeux, aux papattes et à toutes les offres alléchantes du Studio.",
      "1/ ...olvidarte de Alex y entregarte a los regalos de plata, paticas y memes del Estudio. Quién sabe, hasta de pronto le dices a Ally que se tomen unas vacaciones.\n2/ Decirle a Alex que acepte la propuesta de compensación financiera del Estudio (para ella y para aquellos jugadores cuyos perfiles alcanzaste a explorar—pero para NADIE más).\n3/ Apoyar a Alex en su lucha contra el Estudio. Sé que no será fácil y que te tocará decirle adiós a Gao, a sus paticas y a todos los regalitos que el Estudio te iba a dar si la traicionaba.",
      "1/ Forget about Alex and go live the grand life, with paws, money and Ally as my partner in crime. Good luck with that…\n2/ Urge Alex to accept Ally's proposal of financial reparations for her and those whose profiles I managed to discover (and THEM ONLY).\n3/ Urge Alex onward on her fight against the Studio, knowing it won't be easy, knowing I'll have to say goodbye to Gao, its mini-games, its paws, and the Studio's monetary compensation..."
    ]
  },
  {
    messages: [
      "Arf. Le retour du petit noob. T’as trouvé quelque chose pour m’impressionner ? Va falloir taper fort, bb. Mon temps est précieux au cas où t’aurais pas capté.",
      "¡Uy! Pero si es el regreso de la pequeña noob. ¿Por fin encontraste algo con qué impresionarme? Si no es INCREÍBLE me voy a molestar. \n\nPorque mi tiempo es oro, baby.",
      "Ooooohhhh... \"The return of the Little Noob\". \n\nHave you found something to impress me with, baby? It has to be good. My time is precious (which you should have noticed by now, btw)."
    ],
    func: (metaGame, navigation) => metaGame.setCanDebugMode()
  },
  {
    messages: [
      "Pour chaque connaissance que t’as récupérée, je vais te rajouter des infos sur quoi faire, OK? On fait ça au fur et à mesure. Comme ça ça fera des sortes de tutoriels que tu peux regarder pour réagir.\n\nLe B-A BA, le manuel de base, tu vois le genre?\n\nJe te laisse faire ça, pendant ce temps-là je t’envoie les infos à toi et Alex.",
      "... Y entonces me agradeces una y mil veces, baby. Porque yo si sé que hacer. \n\nPor cada conocimiento que hayas recuperado, te voy a dar unos accionables una chimba. ¿Listo? Así al final tendrás una especie de \"Biblia de la protección digital\". xD\n\nVe y pesca todos los conocimientos, mientras tanto hago lo mismo para Alex. Para ayudarla a tomar una decisión.",
      "So I'm gonna expand a little on what he formulated. I'm gonna add some *actually* useful information on what can be done. \n\nWe'll do this as we go. Each time you unlock one of his thoughts, I'll translate it into actionables. And we'll build a sort of \"digital security\" manual.\n\nGo do your thing now. In the meantime, I'm going to do the necessary tweaks for you and Alex.",
      "In the meantime...\nIf you have nothing else to do.\nTell me what's inside the App.\nAre any Players implicated?\nOr are there clues about Blaze's disappearance?\nFind me something I can use\nand I will fix Alex's little problem.", "I need to know what's inside the App:\nAre any Players implicated?\nOr are there clues about Blaze's disappearance?\nFind me something I can use\nand I will fix Alex's little problem.",
    ],
    func: (metaGame, navigation) => {
      metaGame.setShowSkills()
    }
  },
  {
    messages: [
      "Pour chaque connaissance que t’as récupérée, je vais te rajouter des infos sur quoi faire, OK? On fait ça au fur et à mesure. Comme ça ça fera des sortes de tutoriels que tu peux regarder pour réagir.\n\nLe B-A BA, le manuel de base, tu vois le genre?\n\nJe te laisse faire ça, pendant ce temps-là je t’envoie les infos à toi et Alex.",
      "... Y entonces me agradeces una y mil veces, baby. Porque yo si sé que hacer. \n\nPor cada conocimiento que hayas recuperado, te voy a dar unos accionables una chimba. ¿Listo? Así al final tendrás una especie de \"Biblia de la protección digital\". xD\n\nVe y pesca todos los conocimientos, mientras tanto hago lo mismo para Alex. Para ayudarla a tomar una decisión.",
      "In the meantime...\nIf you have nothing else to do.\nTell me what's inside the App.\nAre any Players implicated?\nOr are there clues about Blaze's disappearance?\nFind me something I can use\nand I will fix Alex's little problem.", "I need to know what's inside the App:\nAre any Players implicated?\nOr are there clues about Blaze's disappearance?\nFind me something I can use\nand I will fix Alex's little problem.",
      "So I'm gonna expand a little on what he formulated. I'm gonna add some *actually* useful information on what can be done. \n\nWe'll do this as we go. Each time you unlock one of his thoughts, I'll translate it into actionables. And we'll build a sort of \"digital security\" manual.\n\nGo do your thing now. In the meantime, I'm going to do the necessary tweaks for you and Alex."
    ],
    func: (metaGame, navigation) => {
      metaGame.setShowAlexBlazeChat()
    }
  }
]

export const ScanAppModel = types.model("ScanApp").props({
  handle: types.identifier,
  scanValue: types.number
})

export const GamePlayModel = types
  .model("GamePlay")
  .props({
    name: types.identifier,
    playCount: types.optional(types.integer, 0),
  })
  .actions(self => ({
    play() {
      self.playCount += 1
    }
  }))

export const QuestionPollModel = types
  .model("QuestionPoll")
  .props({
    key: types.identifier,
    value: types.optional(types.string, ""),
  })
  .actions(self => ({
  }))

export const ScanHistoryModel = types.model("ScanHistory").props({
  date: types.optional(types.number, 0),
  scanNumber: types.optional(types.number, 0),
  scanValue: types.optional(types.number, 0)
})

export const MetaGameModel = types
  .model("MetaGame")
  .props({
    userName: types.maybeNull(types.string),
    favoriteGame: types.maybeNull(types.string),
    // avatar: types.maybeNull(types.string), // TODO: change types and a default avatar
    unreadGaoNPC: types.map(types.reference(CharacterProfileModel)),
    firstStart: types.optional(types.boolean, true),
    paws: types.optional(types.integer, 0),
    bought: types.array(types.string),
    locations: types.array(types.frozen()),
    gameStyle: types.optional(
      types.enumeration<GameStyle>("GameStyle", Object.values(GameStyle)), // Maudit sois-tu TypeScript.
      GameStyle.Undetermined,
    ),
    cheater: types.optional(types.boolean, false),
    gamesPlayed: types.map(GamePlayModel),
    onlinePlayerAssessments: types.optional(types.integer, 0),
    notificationCalled: types.array(types.string),
    showBlazeMessageGaoTheVagabond: types.optional(types.boolean, false),
    showGaoTheVagabond: types.optional(types.boolean, false),
    inGame: types.optional(types.boolean, false),
    animationCharacters: types.array(types.string),
    developMode: types.optional(types.boolean, false),
    canDebugMode: types.optional(types.boolean, false),
    isDebugMode: types.optional(types.boolean, false),
    aboutMePlayer: types.optional(types.string, ""),
    imagePlayer: types.optional(types.string, ""),
    permissions: types.array(types.string),
    showThoughts: types.array(types.number),
    readThoughts: types.array(types.number),
    blazeLogged: types.optional(types.boolean, false),
    showSkills: types.optional(types.boolean, false),
    readSkills: types.array(types.string),
    showChat: types.optional(types.boolean, false),
    scanCheck: types.array(types.string),
    fillQuestionary: types.optional(types.boolean, false),
    appScan: types.map(ScanAppModel),
    historyScan: types.array(ScanHistoryModel),
    totalTimeInGame: types.optional(types.number, 0),
    lastOptionsSelected: types.optional(types.string, ""),
    questionary: types.map(QuestionPollModel),
    showAlexBlazeChat: types.optional(types.boolean, false),
    goToEnd: types.optional(types.boolean, false)
  })
  .views(self => ({
    notebookUnlocked() {
      return self.onlinePlayerAssessments >= 1
    },
    blazeChatUnlocked() {
      return self.onlinePlayerAssessments >= 2 && self.showChat
    },
    hasUnreadGaoMessages() {
      return self.unreadGaoNPC.size > 0
    },
    getAllScanApps() {
      if(!self.appScan || self.appScan.size === 0)return null
      let sumScan = 0, value = -1
      self.appScan.forEach(item => {
        sumScan += item.scanValue
      })
      RANGE_LETTERS.forEach((item, index) => {
        if(sumScan/self.appScan.size >= item[0] && sumScan/self.appScan.size <= item[1])
          value = index
      })
      return value
    }
  }))
  .actions(self => ({
    setGoToEnd() {
      self.goToEnd = true
    },
    setShowAlexBlazeChat() {
      self.showAlexBlazeChat = true
    },
    addReponseQuestionPoll(key, value) {
      self.questionary.put(QuestionPollModel.create({
        key,
        value
      }))
    },
    addScanHistory() {
      if(self.appScan && self.appScan.size > 0) {
        let sumScan = 0, value = -1
        self.appScan.forEach(item => {
          sumScan += item.scanValue
        })
        RANGE_LETTERS.forEach((item, index) => {
          if (sumScan / self.appScan.size >= item[0] && sumScan / self.appScan.size <= item[1])
            value = index
        })
        self.historyScan.push({scanValue: sumScan / self.appScan.size, scanNumber: value, date: new Date().getTime()})
      }
      return self.historyScan
    },
    setFillQuestionary() {
      self.fillQuestionary = true
    },
    setLastOptionSelected(option) {
      self.lastOptionsSelected = option
    },
    addTimeInGame(time) {
      console.log(self.totalTimeInGame, time, self.totalTimeInGame + time)
      self.totalTimeInGame = self.totalTimeInGame + time
    },
    addAppScan(handle, scanValue) {
      let value = -1
      RANGE_LETTERS.forEach((item, index) => {
        if(scanValue >= item[0] && scanValue <= item[1])
          value = index
      })
      self.appScan.put({handle, scanValue})
      return value
    },
    loginBlaze() {
      self.blazeLogged = true
    },
    checkScan(item) {
      if(self.scanCheck.includes(item)) {
        self.scanCheck.remove(item)
      } else {
        self.scanCheck.push(item)
      }
    },
    addReadThought(thouhgt) {
      if (!self.readThoughts.includes(thouhgt)) {
        self.readThoughts.push(thouhgt)
      }
    },
    addPermission(permission) {
      if (!self.permissions.includes(permission)) {
        self.permissions.push(permission)
      }
    },
    setImagePlayer(text) {
      self.imagePlayer = text
    },
    setAboutMe(text) {
      self.aboutMePlayer = text
    },
    setCanDebugMode() {
      self.canDebugMode = true
    },
    textChoice(text, navigation) {
      SPECIAL_MESSAGE.forEach(item => {
        if (item.messages.includes(text)) {
          item.func(self, navigation)
        }
      })
    },
    setDevelopMode() {
      self.developMode = true
    },
    setShowGaoTheVagabond() {
      self.showGaoTheVagabond = true
    },
    setShowBlazeMessageGaoTheVagabond() {
      self.showBlazeMessageGaoTheVagabond = !self.showBlazeMessageGaoTheVagabond
    },
    setInGame(inGame) {
      self.inGame = inGame
    },
    setUserName(userName) {
      self.userName = userName
    },
    setFavoriteGame(favoriteGame) {
      self.favoriteGame = favoriteGame
    },
    setNotFirstStart() {
      self.firstStart = false
    },
    sendChatNotification(srcCharacter: string, text: string, id: string, node: string, passive: boolean, navigation, t): boolean {
      const mapGames = { "Rokaya": "Photographer", "Amin": "Filekeeper", "Ally": "Athlete", "Lucas": "Prosecutor", "Sol": "Intruder", "Masako": "Forger" }
      const rootStore: RootStore = getRoot(self)
      const character = rootStore.communityStore.characters.get(srcCharacter)
      if (!self.notificationCalled.includes(id)) {
        rootStore.communityStore.scenarios.get(character.name).setCurrent(node)
        self.notificationCalled.push(id)
        if (node.includes("Alex-6"))
          self.showBlazeMessageGaoTheVagabond = true
        const nikki1= "Nikki1"
        if (id === nikki1 && navigation) {
          rootStore.setFixedChat(true)
          navigation.navigate("chat", {character: "Nikki", hideClose: true})
        }
        let sendNotification = !passive || (passive && mapGames[srcCharacter])
        DONT_SEND_NOTIFICATION.forEach(item => sendNotification = sendNotification && !node.endsWith(item))
        if (sendNotification) {
          self.unreadGaoNPC.put(character)
          rootStore.addToast({
            channelId: passive ? "GaoBlazePassive" : "GaoBlaze",
            message: passive ? srcCharacter + t("common:chat-1", " est aussi en train de jouer à ") + mapGames[srcCharacter] + " !" : text,
            // FIXME: i18n this.
            title: `${t("common:message-from", "Message de")} ${srcCharacter.replace("Blaze", "")}`,
            vibrate: !passive,
            largeIcon: membersData[character.name].avatar,
            userInfo: { name: character.name, isBlaze: srcCharacter.includes("Blaze"), passive },
            soundName: srcCharacter.includes("Blaze") ? "blaze_message_in.ogg" : "message_in.ogg",
          })
        }
        // FIXME: use a real value.
      }
      return true
    },
    // FIXME: will we need this for Blaze?
    markAsRead(srcCharacter) {
      self.unreadGaoNPC.delete(srcCharacter)
    },
    markEverythingAsRead() {
      self.unreadGaoNPC.clear()
    },
    logLocation(location) {
      self.locations.push(location)
    }
  }))
  .actions(self => ({
    refreshChatTriggers(navigation, t) {
      const rootStore: RootStore = (getRoot(self) as RootStore)
      rootStore.communityStore.fireAllTriggers(
        self.sendChatNotification,
        rootStore.gameState(),
        navigation, t
      )
    }
  }))
  .actions(self => ({
    playGame(gameName, navigation, t) {
      if (!self.gamesPlayed.has(gameName)) {
        self.gamesPlayed.put(GamePlayModel.create({
          name: gameName,
          playCount: 1
        }))
        self.refreshChatTriggers(navigation, t)
      } else {
        self.gamesPlayed.get(gameName).play()
      }
    },
    reward(paws: number) {
      self.paws += paws
    },
    sell(paws: number) {
      if (paws <= self.paws) {
        self.paws -= paws
      }
    },
    buyItem(item: ShopItem) {
      // Cannot buy.
      // if (item.price > self.paws) return Promise.resolve(false)

      // Deduct price.
      self.paws -= item.price
      // Register the reward.
      self.bought.push(item.id)
      // Give the reward.

      const resolvedAssetSrc = (typeof item.assetsPath === "string" && (item.assetsPath as string).startsWith("file"))?
        Image.resolveAssetSource({uri: item.assetsPath}) : Image.resolveAssetSource(item.assetsPath)
      // Check if the reward directory exist.
      return PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]
      )
        .then(() => {
          self.addPermission("READ_EXTERNAL_STORAGE")
          self.addPermission("WRITE_EXTERNAL_STORAGE")
          RNFS.mkdir(REWARD_DIRECTORY)
        })
        // @ts-ignore
        .then(() => {
          const rootStore: RootStore = getRoot(self)
          rootStore.addToast({
            channelId: "GaoBlaze",
            message: "The file was downloaded",
            // FIXME: i18n this.
            title: "Gao download",
            largeIcon: item.assetsPath,
            userInfo: { file: REWARD_DIRECTORY + "/" + item.id }
          })
          // In development, the packager sends it through HTTP.
          if (resolvedAssetSrc.uri.startsWith("http")) {

            return RNFS.downloadFile({
              fromUrl: resolvedAssetSrc.uri,
              toFile: `${REWARD_DIRECTORY}/${item.id}`,
            }).promise
          } else {
            // In production, it's offline.
            if((typeof item.assetsPath === "string" && (item.assetsPath as string).startsWith("file"))) {
              return RNFS.copyFile(item.assetsPath, `${REWARD_DIRECTORY}/${item.id}`)
            }

            return NativeModules.TrackProjectMisc.copyFile(
              resolvedAssetSrc.uri, `${REWARD_DIRECTORY}/${item.id}`)
          }
        })
        .then(() => true).catch((er) => console.log(er))
    },
    performOnlinePlayerAssessment(navigation, character, t) {
      if (!self.animationCharacters.includes(character.key)) {
        self.animationCharacters.push(character.key)
        self.onlinePlayerAssessments += 1
        self.refreshChatTriggers(navigation, t)
        if (self.onlinePlayerAssessments >= 2) {
          if (self.isDebugMode) {
            self.showChat = true
          }
          self.isDebugMode = false
        }
      }
      navigation.navigate("onlinePlayerAssessment", { character })
    },
    addShowTouhght(tought, showNotification, t) {
      if (!self.showThoughts.includes(tought)) {
        const rootStore: RootStore = getRoot(self)
        self.showThoughts.push(tought)
        if (showNotification) {
          rootStore.addToast({
            channelId: "GaoBlaze",
            message: t("common:noti-thought", "Nikki a ajouté un tutoriel pour vous"),
            // FIXME: i18n this.
            title: "Gao download",
            largeIcon: require("assets/images/community/avatars/B-Nikki.png"),
            userInfo: { tought: tought }
          })
        }
      }
    },
    setShowSkills() {
      self.showSkills = true
    },
    readSkill(skill) {
      if(!self.readSkills.includes(skill)) {
        self.readSkills.push(skill)
      }
    }
  })).actions(self => ({
    setIsDebugMode(t) {
      self.playGame("DebugMode", null, t)
      self.isDebugMode = true
      self.showChat = true
    },
  }))

export interface MetaGame extends Instance<typeof MetaGameModel> { }
export interface GamePlay extends Instance<typeof GamePlayModel> { }
