import { CharacterProfileModel, CharacterProfile } from "./character-profile"

test("can be created", () => {
  const instance: CharacterProfile = CharacterProfileModel.create({})

  expect(instance).toBeTruthy()
})