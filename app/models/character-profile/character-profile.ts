import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { ChatHistoryModel } from "models/chat-history"
import { evaluatePseudoCondition } from "utils/chat-engine/node-visitor"
import { IGameState } from "utils/chat-engine/types"

export const DEPENDENCIES = [
  {id: "Alex 1", options: []},
  {id: "Alex 2", options: ["Pas de soucis, à plus tard !", "Très bien ! Je te recontacte bientôt de toute façon !", ":))",
    "¡No pasa nada! ¡Hablamos luego!", "¡Muy bien! ¡Igual te escribo en un rato para ver cómo vas!", ":))",
    "Great ! I'll text you later then!", "You're welcome !", ":))"]},
  {id: "Alex 3", options: [
    "^^\nJ’avoue\nJe resterai bien un peu plus longtemps avec toi\nPour te convaincre de l’intérêt majeur de ces gifs\nEt de l’apport inestimable qu’ils seraient dans tes conversations\nHolala\nFaut que je te laisse. Mon boss vient de décider que j’avais un truc intelligent à dire MAINTENANT",
    "^^\nI'll confess that if you start using GIFs in our conversations I might just stick with you for a while.\n\nAsdf\nI have to leave you now. My boss just decided that I have something important to say RIGHT NOW.",
    "La verdad es que sí. ^^\nTe confieso\nMe encantaría quedarme hablando contigo más tiempo para convencerte de la importancia de estos GIFs en nuestras conversaciones y en TU VIDA.\n\nPero tengo que irme. :( Mi jefe acaba de decidir que tengo algo importante que decir YA MISMO.",
    "Tu verras, rien de compliqué !\n\nBon.\nPeut-être qu’il me reste encore un peu de travail\nPour te convaincre de l’intérêt majeur de ces gifs\nEt de l’apport inestimable qu’ils seraient dans tes conversations\nET DANS TA VIE\nHolala\nFaut que je te laisse. Mon boss vient de décider que j’avais un truc intelligent à dire MAINTENANT",
    "Seems like I have some more work to do to convince you of the importance of these GIFs...\nAnd of the invaluable value they would bring to our conversations.\nAND TO YOUR LIFE. :p\n\nAsdf\nI have to leave you now. My boss just decided that I have something important to say RIGHT NOW.",
    "¡Ya verás! ^^\n\nY si no, seguiré trabajando para probarte la importancia de estos GIFs en nuestras conversaciones... Y EN TU VIDA. xD\n\nAsdf. Tengo que irme. Mi jefe acaba de decidir que tengo algo importante que decir YA MISMO.",
    "Oui ! Des gif !\nDes tonnes de gif !\nHolala\nFaut que je te laisse. Mon boss vient de décider que j’avais un truc intelligent à dire MAINTENANT",
    "FOR GIIIIIIFFFFSPARTAAA!\n(Doesn't really work, does it?)\n\nAsdf\nI have to leave you now. My boss just decided that I have something important to say RIGHT NOW.",
    "¡GIFs!¡GIFs!¡GIFs!\n¡Toneladas de GIFs!\n\nAsdf. Tengo que irme. Mi jefe acaba de decidir que tengo algo importante que decir YA MISMO.",
    "Holala !\nÇa y est !\nOn va pouvoir faire des battles !\n\nHolala\nFaut que je te laisse. Mon boss vient de décider que j’avais un truc intelligent à dire MAINTENANT",
    "Yes!\nSoon I will be able to challenge you!\n\nAsdf\nI have to leave you now. My boss just decided that I have something important to say RIGHT NOW.",
    "¡Siiiii!\n¡Qué nota! Pronto voy a poder retarte.\n\nAsdf. Tengo que irme. Mi jefe acaba de decidir que tengo algo importante que decir YA MISMO."
  ]},
  {id: "Alex 4", options: ["<3", "Des fois jme dis ça",
    "... Debería. :(",
    "... I don't know."]},
  {id: "Alex 5", options: ["Ne t'inquiète pas, ma première motivation dans ma relation avec Sol, c'est bien Sol elle-même. <3", "Hahaha !", "Merci pour tes conseils :)",
    "No te preocupes. Mi prioridad número uno en todo esto es que Sol esté bien. <3", "Jajaja!", "Gracias por escucharme. :)",
    "Don't worry. She's my main concern in all of this. <3", "Hahaha!", "... Thank you."]},
  {id: "Alex 6", options: ["Il va falloir !", "Merci pour tes conseils. Ça décoiffe par moment, mais j’apprécie ta franchise. ;)",
    "¡Tienes razón!", "Gracias por tus consejos. Tengo que irme, pero aprecio mucho que me hayas acompañado en todo esto. ;)",
    "It is... :)", "Ha! Thank you for being here. I know it's crazy, but I really appreciate you being honest. ;)"]},
  // Act 2
  {id: "Ally 2", options: ["Promis", "T’inquiète", "Ok....",
    "Te prometo que no.", "No te preocupes.",
    "Thank you...\n\nI'm going to log out for a moment.\n\nThe insults keep coming and...\n\nI'll check every now and then for your messages.\n\nPlease, don't tell anyone about it."]},
  {id: "Alex 8", options: ["Hmm.\nje comprends\ntu soutiens Alex\net sa situation est compliquée\nje ne suis que graphiste, mais les gens m’écoutent un peu ici\nsi elle signale les utilisateurs problématiques\nje veillerais personnellement à ce qu’ils soient sanctionnés\nEt si c’est vraiment grave, elle peut porter plainte\nOn pourra pas le faire à sa place\nC’est à chacun d’agir à sa mesure\nMais ce sont des chamailleries\nTout ça ne mérite pas qu’on dérange la police, j’imagine...\nBref\nj’attends de tes nouvelles", "C’est le mieux que je puisse te répondre,\nil faudra t’en contenter\nJe ne peux pas outrepasser mes prérogatives, tu comprends\nJ’attends de tes nouvelles", "J’attends de tes nouvelles",
    "Mira... Sé que quieres ayudar a Alex.\n\n... Y yo también. Supongo.\nVoy a intentar ayudarla a través del Estudio.\nDile que reporte a los jugadores que le escriben y yo superviso que el Estudio los sancione.\n\nY dile, que si realmente siente que es demasiado, que vaya a la policía y denuncie (porque el Estudio no podría).\n\nIgual estoy segura que es una situación pasajera que no amerita escalarlo a la policía.\n\nEn fin. Espero noticias tuyas (y de Alex).", "Quisiera poder darte otra respuesta. Pero no tengo manera de extralimitarme en mis funciones.", "Dale. Espero noticias.",
    "I get it.\nYou support Alex\nand her current situation is... complicated.\nThough I'm just responsible for the graphics,\nI have some saying around here:\nif she flags those \"problem users\"\nI'll personally see to it that they are punished.\nAnd, if she considers it's not enough, \nshe can report the situation herself.\nWe can't do it for her.\n\n(I'm sure it's just friendly bickering,\nthere's no need to get the Police involved)\n\nAnyhow,\ntext me when you hear something.", "It's the best I can offer.\n\n... Anyhow,\ntext me when you hear something.", "Text me when you hear something."]},
  {id: "Alex-9", options: ["C'est bon, ça marche ?", "Rio Grande",
    "Goooooooooooooo.", "¡Ay, si! ¡Se llama Rio Grande!",
    "Yay! ^^", "Rio Grande"]},
  {id: "Nikki - 2", options: ["Bon, t'es déjà partie...", "......",
    "—El usuario no está disponible.—",
    "——The user you're trying to reach is offline——"]},
  {id: "Ally-3a", options: ["GG\nLaisse moi un peu de temps ensuite.",
    "Estaré pendiente para hacer mi magia, baby.",
    "GG\nNow sit back and let the master do their work. :)"]},
  {id: "Nikki 3-0", /*options: ["c’est ça\net si tu n’as rien d’autre à faire\nj’ai besoin de savoir ce qu’il y a dans l’app\ns’il y a des noms de joueurs impliqués\ns’il y a un indice qui me permet de savoir pourquoi Blaze s’est tiré.\nTrouve-moi quelque chose d’utile\net j’arrange la vie d’Alex", "j’ai besoin de savoir ce qu’il y a dans l’app\ns’il y a des noms de joueurs impliqués\ns’il y a un indice qui me permet de savoir pourquoi Blaze s’est tiré.\nTrouve-moi quelque chose d’utile\net j’arrange la vie d’Alex",
    "Mientras tanto.\nPodrías cooperar un poco más\ny contarme del App\nsi hay algo del Estudio en juego, o si hay algo que explique por qué Blaze se fue.\nEncuéntrame algo útil y te prometo que le arreglo la vida a Alex.\n:)", "NECESITO saber que hay en el App.\nYa sabes, dame alguna información al respecto y le arreglo la vida a Alex."]},*/
    options: ["GG\nLaisse moi un peu de temps ensuite.",
      "Estaré pendiente para hacer mi magia, baby.",
      "GG\nNow sit back and let the master do their work. :)"]},
  {id: "Alex 10", options: ["Pour chaque connaissance que t’as récupérée, je vais te rajouter des infos sur quoi faire, OK? On fait ça au fur et à mesure. Comme ça ça fera des sortes de tutoriels que tu peux regarder pour réagir.\n\nLe B-A BA, le manuel de base, tu vois le genre?\n\nJe te laisse faire ça, pendant ce temps-là je t’envoie les infos à toi et Alex.",
    "... Y entonces me agradeces una y mil veces, baby. Porque yo si sé que hacer. \n\nPor cada conocimiento que hayas recuperado, te voy a dar unos accionables una chimba. ¿Listo? Así al final tendrás una especie de \"Biblia de la protección digital\". xD\n\nVe y pesca todos los conocimientos, mientras tanto hago lo mismo para Alex. Para ayudarla a tomar una decisión.",
    "In the meantime...\nIf you have nothing else to do.\nTell me what's inside the App.\nAre any Players implicated?\nOr are there clues about Blaze's disappearance?\nFind me something I can use\nand I will fix Alex's little problem.", "I need to know what's inside the App:\nAre any Players implicated?\nOr are there clues about Blaze's disappearance?\nFind me something I can use\nand I will fix Alex's little problem.",
    "So I'm gonna expand a little on what he formulated. I'm gonna add some *actually* useful information on what can be done. \n\nWe'll do this as we go. Each time you unlock one of his thoughts, I'll translate it into actionables. And we'll build a sort of \"digital security\" manual.\n\nGo do your thing now. In the meantime, I'm going to do the necessary tweaks for you and Alex."]},
  {id: "Ally_3b", options: ["La réponse est dans l’appli. Enfin, j’espère. ><", "Ouf", "J’espère...",
    "Seguro la respuesta está en el App.\n\n... Espero que esté. ><", "*Respira hondo, Alex.*", "Sí...",
    "I think the answer is somewhere in the App... I hope it is. ><", "*sigh*", "I hope you're right..."]},
  {id: "Alex-11", options: ["Je te laisse réfléchir\nSouviens-toi d’une chose\nMieux vaut un bon arrangement qu’un long procès\nJe peux alléger ton fardeau\nou l’alourdir\nSi tu redeviens raisonnable, fais-moi signe", "Et moi je n’aime pas avoir à courir après Blaze\nChacun sa croix\nJe peux alléger ton fardeau\nou l’alourdir\nc’est ton choix", "Tout va bien. Tu as choisi le bon camp",
    "\\_(=)_/\n\n(Solo recuerda, más vale un acuerdo fácil que un proceso largo.)\n\nSi cambias de opinión, avísame.", "No tanto como el de Blaze. \\_(=)_/\nA cada quién su cruz.\n\n(No olvides que puedo alivianar tus cargas—y de paso las de Alex—si cambias de opinión.", "No te equivoques. :)",
    "I'll let you to it then.\nJust remember.\nIt's better to settle than to dispute for years.", "And I don't like running after Blaze.\nTo each their own cross to bear...\n\nI can make yours lighter,\nif you want.", "Get to work then."]},
  {id: "Nikki 4-0", options: ["Merci. Le temps presse. Il faut que je prenne une décision.", "Je ne sais plus sur qui je peux compter...\nVoyons comment je me débrouille à ce jeu-là.", "...", "Je n’ai plus rien à te dire.\n\nAlly peut alléger ton fardeau ou l’alourdir\nsi tu redeviens raisonnable, fais-nous signe",
    "... Gracias. Por todo... Voy a pensarlo bien.", "... No sé si pueda volver a confiar en ti...\n\nVoy a ver cómo me las arreglo.", "........", "Ya no tengo nada que decirte.",
    "..... Thank you.", "...\n\nI'll talk to you later.", "...........", "I have nothing else to say to you."]},
]

const SPECIAL_TRIGGER = [
  {id: "Alex-9", func: (gameState) => {
    return gameState.databaseVisited > 0
  }},
  {id: "Nikki - 2", func: (gameState) => {
    return gameState.databaseVisited > 0
  }},
  {id: "Ally-3a", func: (gameState => gameState.canDebugMode)},
  {id: "Nikki 3-0", func: gameState => {
    return gameState.onlinePlayerAssessments >= 2 && gameState.showChat
  }},
  // TODO acomodar para cuando tenga los knowledge
  {id: "Alex 10", func: gameState => gameState.onlinePlayerAssessments >= 2},
  {id: "Alex 8", func: gameState => {
    //console.log("---->", !gameState.blazeLogged)
    return !gameState.blazeLogged
  }}
]

export const validateSpecialTriggers = (idNode, gameState): boolean => {
  const search = SPECIAL_TRIGGER.find((item) => idNode.endsWith(item.id))
  //console.log(">>>>", idNode, search)
  if(search) {
    return search.func(gameState)
  }
  return true
}

export const validateDependencies = (idNode, dependencies): boolean => {
  const search = DEPENDENCIES.find((item) => idNode.endsWith(item.id))
  //console.log(".....", idNode, search, dependencies)
  if(search) {
    return search.options.length === 0 || search.options.some(item => dependencies.includes(item))
  }
  return true
}

const buildTrigger = ({ id, delay, text, conditionNode, idNode }, dependencies, gameState, item, navigation, t) => sendChatNotification => metaGame => {
  if(isNaN(delay))
    delay = 0
  if(validateDependencies(idNode, dependencies) && validateSpecialTriggers(idNode, gameState)) {
    if (!conditionNode) {
      setTimeout(() => {
        sendChatNotification(text, id, idNode, item.type === "passive", navigation, t)
      }, delay || 0)
    } else {
      const condition = evaluatePseudoCondition(conditionNode)
      if (condition(metaGame)) {
        setTimeout(() => {
          sendChatNotification(text, id, idNode, item.type === "passive", navigation, t)
        }, delay || 0)
      }
    }
  }
}


/**
 * Model description here for TypeScript hints.
 */
export const CharacterProfileModel = types
  .model("CharacterProfile")
  .props({
    name: types.identifier,
    confidence: types.optional(types.integer, 0), // FIXME
    gaoChatHistory: types.maybeNull(types.reference(ChatHistoryModel)),
    blazeChatHistory: types.maybeNull(types.reference(ChatHistoryModel)),
    isFriend: types.optional(types.boolean, false),
    disabled: types.optional(types.boolean, false),
    triggers: types.array(types.frozen()),
    dependencies: types.array(types.string),
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    fireTriggers (notify: (character: string, text: string, id: string, node: string, passive: boolean, navigation, t) => boolean, gameState: IGameState, navigation, t) {
      for (const trigger of self.triggers.map((item) => buildTrigger(item, self.dependencies, gameState, item, navigation, t))) {
        trigger((text, id, idNode, passive) => notify(self.name, text, id, idNode, passive, navigation, t))(gameState)
      }
    },
    addDependency(text): boolean {
      let value = false
      if(!self.dependencies.includes(text)) {
        let add = false
        DEPENDENCIES.forEach((item) => add = add || item.options.includes(text))
        if (add) {
          self.dependencies.push(text)
          value = true
        }
      }
      return value
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type CharacterProfileType = Instance<typeof CharacterProfileModel>
export interface CharacterProfile extends CharacterProfileType {}
type CharacterProfileSnapshotType = SnapshotOut<typeof CharacterProfileModel>
export interface CharacterProfileSnapshot extends CharacterProfileSnapshotType {}
