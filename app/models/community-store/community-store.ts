import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { ChatHistoryModel } from "models/chat-history"
import { CharacterProfileModel } from "models/character-profile"
import { ScenarioModel } from "models/scenario"
import { IGameState } from "utils/chat-engine/types"


const ChoiceModel = types.model("ChoiceModel").props({id: types.identifier})
/**
 * Model description here for TypeScript hints.
 */
export const CommunityStoreModel = types
  .model("CommunityStore")
  .props({
    scenarios: types.map(ScenarioModel), // contains all scenarios of the game.
    histories: types.map(ChatHistoryModel), // contains all chat history during the game.
    characters: types.map(CharacterProfileModel), // contains all characters during this game.
    selectedProfile: types.maybeNull(types.reference(CharacterProfileModel)),
    friends: types.array(types.reference(CharacterProfileModel)),
    interactedWith: types.map(types.reference(CharacterProfileModel)),
    choicesMade: types.map(types.reference(ChoiceModel)),
    databaseVisited: types.optional(types.integer, 0),
  })
  .views(self => ({
    hasSelectedProfile() {
      return self.selectedProfile !== null
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    addSelectedChoice(keyChoice) {
      self.choicesMade.put(ChoiceModel.create({id: keyChoice}))
    },
    addDataBaseVisited() {
      self.databaseVisited = self.databaseVisited + 1
    },
    addSelectedProfileToFriends () {
      if (!self.friends.find(friend => friend.name == self.selectedProfile.name)) {
        self.friends.push(self.selectedProfile.name)
      }
    },
    initChat(character: string, gameChapter: string) {
      const history = ChatHistoryModel.create({
        id: `${gameChapter}-${character}`,
        scenario: self.scenarios.get(character).id
      })
      const characterModel = self.characters.get(character)
      characterModel[`${gameChapter}ChatHistory`] = history.id
      self.histories.put(history)
    }
  }))
  .actions(self => ({
    selectCharacter(character: string,
      gameChapter: "gao" | "blaze",
      gameState: IGameState) {
      if(self.scenarios.get(character)) {
        self.selectedProfile = self.characters.get(character)

        if (self.selectedProfile)
          self.interactedWith.put(self.selectedProfile)

        if (!self.selectedProfile[`${gameChapter}ChatHistory`])
          self.initChat(character, gameChapter)

        self.selectedProfile[`${gameChapter}ChatHistory`].openChat(gameState)
      }
    },
    fireAllTriggers (notify: (character: string, text: string, id: string, node: string, passive: boolean, navigation, t) => boolean, state: IGameState, navigation, t) {
      for (const character of self.characters.values()) {
        character.fireTriggers(notify, state, navigation, t)
      }
    },
    fireDependency(text) {
      for(const character of self.characters.values())
        character.addDependency(text)
    }
  }))

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type CommunityStoreType = Instance<typeof CommunityStoreModel>
export interface CommunityStore extends CommunityStoreType {}
type CommunityStoreSnapshotType = SnapshotOut<typeof CommunityStoreModel>
export interface CommunityStoreSnapshot extends CommunityStoreSnapshotType {}
