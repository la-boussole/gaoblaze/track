import { CommunityStore, CommunityStoreModel } from "models/community-store"
import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { NavigationStoreModel } from "navigation/navigation-store"
import { MetaGameModel } from "models/meta-game"
import { IGamePlay, IGameState } from "utils/chat-engine/types"
import { CharacterProfileModel } from "models/character-profile"
import { Scenario, ScenarioModel } from "models/scenario"
import ScenarioDataFR from "assets/scenarios"
import ScenarioDataES from "assets/scenarios/es"
import ScenarioDataEN from "assets/scenarios/en"
import OtherProfiles from "assets/community_members/other-profiles"
import { ToastManagerMessage } from '../../components/toast/ToastManager'
import { SegmentedControlIOSBase } from 'react-native'

function loadAllStoryCharacters (language) {
  const ScenarioData = language === "es" ? ScenarioDataES : language === "en" ? ScenarioDataEN : ScenarioDataFR
  const characters = {}
  const scenarios = {}

  for (const [character, scenario] of Object.entries(ScenarioData)) {
    let loadSuccess = false
    let triggers = []

    try {
      console.log("Loading scenario", scenario.name, character)
      const scenarioModel = ScenarioModel.create({
        // @ts-ignore
        id: scenario.name,
        // @ts-ignore
        ifid: scenario.ifid,
        state: "empty"
      })

      triggers = scenarioModel.init(scenario)
      scenarios[scenarioModel.id] = scenarioModel
      loadSuccess = true
      triggers.forEach((trigger, index) => trigger.id = character + index)
    } catch (error) {
      console.error("Failure to load the scenario for ", character, "error", error)
    }

    characters[character] = CharacterProfileModel.create({
      name: character,
      disabled: !loadSuccess,
      triggers
    })

  }

  OtherProfiles.forEach((item) => {
    characters[item.id] = CharacterProfileModel.create(
      {
        name: item.id,
        disabled: true,
        confidence: item.confidence
      }
    )
  })

  return {characters, scenarios}
}

export function initCommunity (language) {
  return loadAllStoryCharacters(language)
}

const ToastMessageModel = types.model("ToastMessageModel").props({
  message: types.optional(types.string, ""),
  title: types.optional(types.string, ""),
  vibrate: types.optional(types.boolean, false),
  largeIcon: types.optional(types.frozen(), 0),
  userInfo: types.optional(types.frozen(), {}),
  soundName: types.optional(types.string, ""),
  isVisible: types.boolean,
})

/**
 * A RootStore model.
 */
export const RootStoreModel = types.model("RootStore").props({
  // @ts-ignore
  communityStore: types.optional(CommunityStoreModel, {}),
  navigationStore: types.optional(types.late(() => NavigationStoreModel), {}),
  metaGame: types.optional(MetaGameModel, {}),
  song: types.optional(types.string, "gao_main_sound.ogg"),
  isFirstOpenApp: types.optional(types.boolean, true),
  isFirstStart: types.optional(types.boolean, true),
  isEndedChat: types.optional(types.boolean, false),
  homeMusic: types.optional(types.boolean, true),
  gameMusic: types.optional(types.boolean, true),
  soundMusic: types.optional(types.boolean, true),
  playSong: types.optional(types.boolean, true),
  idGamer: types.optional(types.string, ""),
  isFixedChat: types.optional(types.boolean, false),
  isSendDataExodusAccept: types.optional(types.boolean, false),
  lastDate: types.optional(types.Date, new Date()),
  selectedLanguage: types.optional(types.string, ""),
  isLiveStreamPlayed: types.optional(types.boolean, false),
  toasts: types.optional(ToastMessageModel, {isVisible: false}),
}).views(self => ({
  gameState (): IGameState {
    return {
      pawCount: self.metaGame.paws,
      profilesDiscovered: self.communityStore.interactedWith,
      gamesPlayed: self.metaGame.gamesPlayed,
      choicesMade: self.communityStore.choicesMade,
      databaseVisited: self.communityStore.databaseVisited,
      canDebugMode: self.metaGame.canDebugMode,
      onlinePlayerAssessments: self.metaGame.onlinePlayerAssessments,
      blazeLogged: self.metaGame.blazeLogged,
      showChat: self.metaGame.showChat
    }
  },
  getSong (): string {
    if(self.song)
      return self.song
    return "gao_main_sound.ogg"
  },
  getToast(): ToastManagerMessage {
    if(self.toasts && self.toasts.isVisible) {
      return {message: self.toasts.message, 
        title: self.toasts.title,
        vibrate: self.toasts.vibrate,
        largeIcon: self.toasts.largeIcon,
        userInfo: self.toasts.userInfo,
        soundName: self.toasts.soundName}
    }
    return null
  }
})).actions(self => ({
  setCommunity(language) {
    self.communityStore = initCommunity(language)
    console.log(self.selectedLanguage, language, self.communityStore)
  },
  setLiveStreamPlayed() {
    self.isLiveStreamPlayed = true
  },
  setSelectedLanguage(language) {
    self.selectedLanguage = language
  },
  setLastDate() {
    self.lastDate = new Date()
  },
  setFirstOpenApp() {
    self.isFirstOpenApp = false
  },
  setSendDataExodusAccept() {
    self.isSendDataExodusAccept = true
  },
  setFixedChat(isFixed) {
    self.isFixedChat = isFixed
  },
  setIdGamer(id) {
    self.idGamer = id
  },
  setSettings(homeMusic, gameMusic, soundMusic) {
    self.homeMusic = homeMusic
    self.gameMusic = gameMusic
    self.soundMusic = soundMusic
  },
  setFirstStart() {
    self.isFirstStart = false
  },
  setEndedChat() {
    self.isEndedChat = true
  },
  setSong(song, playSong) {
    console.log("set song", song, playSong)
    self.song = song
    self.playSong = playSong
  },
  addToast(toastManagerMessage: ToastManagerMessage) {
    self.toasts = {
      message: toastManagerMessage.message, isVisible: true,
      title: toastManagerMessage.title,
      vibrate: toastManagerMessage.vibrate,
      largeIcon: toastManagerMessage.largeIcon,
      userInfo: toastManagerMessage.userInfo,
      soundName: toastManagerMessage.soundName
    };
  },
  removeToast() {
    self.toasts = {
      message: "", isVisible: false,
      title: "",
      vibrate: false,
      largeIcon: 0,
      userInfo: {},
      soundName: ""
    };
  }
}
))

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
