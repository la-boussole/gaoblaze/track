import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { ScenarioModel, unwrapAndUnwait } from "models/scenario"
import { IGameState, IStaticNode } from "utils/chat-engine/types"

/**
 * Model description here for TypeScript hints.
 */
export const ChatHistoryModel = types
  .model("ChatHistory")
  .props({
    id: types.identifier,
    scenario: types.reference(ScenarioModel),
    staticMessages: types.array(types.frozen<IStaticNode>())
  })
  .views(self => ({
    get currentMessage () {
      if (self.scenario.currentMessage) {
        return (self.scenario.currentMessage.type === "wrapper"
          ? self.scenario.currentMessage.wrapped : self.scenario.currentMessage)
      }
      return self.scenario.currentMessage
    },
    get currentChoices () {
      return self.scenario.currentChoices
    },
    get pendingMessages () {
      return self.scenario.pendingMessages
    },
    get requireUserInteraction () {
      if (self.scenario.currentMessage) {
        return (
          self.scenario.currentMessage.type === "wrapper"
          && self.scenario.currentMessage.requiresUserInteraction
        )
      }

      return false
    },
    get hasFinishedToWrite () {
      return self.scenario.currentMessage === null
    }
  }))
  .actions(self => {
    // Perform re-evaluation of the current node.
    function openChat(gameState: IGameState) {
      if (!self.scenario.currentNode) {
        console.error("No current node for the provided scenario! Cannot initialize the chat.",
          self.id)
        throw new Error("No current node in scenario")
      }

      self.scenario.evaluateCurrent(gameState)
    }

    function archive () {
      if (self.scenario.currentMessage) {
        const frozen = unwrapAndUnwait(self.scenario.currentMessage)
        if (frozen) self.staticMessages.push(
          frozen
        )

        self.scenario.archive()
      }
    }

    function choose (gameState: IGameState, choice, id: number): boolean {
      if (!self.scenario.choose(choice.passageTitle?choice.passageTitle:choice)) return false
      self.staticMessages.push({
        type: "text",
        text: choice.displayed?choice.displayed:choice,
        isMe: true
      })
      archive()
      // The scenario will determine if it'll produce a message or not.
      // According to the meta game parameters and the scenario itself.
      // This might be asynchronous, so it's required.
      self.scenario.produceNewMessage(gameState, self)
      /*.then(receiveEvents)
        .catch(err => {
          console.error("Unexpected failure during production of new messages.", err)
          throw err
        })*/
      return true
    }

    return {
      openChat,
      choose,
      archive
    }
  })

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type ChatHistoryType = Instance<typeof ChatHistoryModel>
export interface ChatHistory extends ChatHistoryType {}
type ChatHistorySnapshotType = SnapshotOut<typeof ChatHistoryModel>
export interface ChatHistorySnapshot extends ChatHistorySnapshotType {}
