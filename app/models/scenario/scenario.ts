import {Instance, SnapshotOut, types} from "mobx-state-tree"
import {
  ChoiceType,
  IChoice,
  IChoiceNode,
  IGameState,
  IRuntimeNode,
  IStaticNode,
  ISubNode, ITextNode, ITriggerNode,
} from "utils/chat-engine/types"
import parser from "utils/parser/twine"
import NodeVisitor, {evaluateCondition, evaluateShowCondition} from "utils/chat-engine/node-visitor"
import * as util from "util"

const consolidateChoiceNodes = (choiceNodes: IChoiceNode[]): IChoiceNode => {
  return {
    type: "choices",
    choices: ([] as ChoiceType[]).concat(...(choiceNodes.map(c => c.choices)))
  }
}

function deepFlatten(arr) {
  const flattenArr = arr.map(value =>
    Array.isArray(value) ? deepFlatten(value) : value
  )
  return [].concat(...flattenArr)
}

function deepUnwrap(node, out) {
  if (node.type === "wrapper" && node.wrapped) {
    node.wrapped
      .forEach(subnode => {
        if (subnode.type === "wrapper")
          deepUnwrap(subnode, out)
        else
          out.push(subnode)
      })
  } else
    out.push(node)
}

const evaluateRawRepresentation = (gameState: IGameState, rawRepresentation: ISubNode[]): IRuntimeNode[] => {
  const visitor = new NodeVisitor(gameState)

  let deepRuntimeNodes: IRuntimeNode[] = deepFlatten(rawRepresentation.map(visitor.visitNode))
  deepRuntimeNodes = deepRuntimeNodes.filter(n => !!n)
  let runtimeNodes: IRuntimeNode[] = []
  deepRuntimeNodes.forEach(node => deepUnwrap(node, runtimeNodes))

  runtimeNodes = deepFlatten(runtimeNodes)
  const choiceNodes: IChoiceNode[] = runtimeNodes.filter(
    (n): n is IChoiceNode => n.type === "choices"
  )
  runtimeNodes = runtimeNodes.filter(n => n.type !== "choices")
  runtimeNodes.push(consolidateChoiceNodes(choiceNodes))

  return runtimeNodes
}

export const unwrapAndUnwait = (n: IRuntimeNode): IStaticNode | null => {
  // Remove wait node.
  if (n.type === "wait") return null
  if (n.type === "wrapper") return unwrapAndUnwait(n.wrapped)

  return (n as IStaticNode)
}

function hasChoice(choice, evaluation: IRuntimeNode[]): boolean {
  if (evaluation.length === 0) return false

  const lastNode: IRuntimeNode = evaluation[evaluation.length - 1]

  if ("choices" in lastNode) {
    console.log(lastNode.choices
      .map(choice => choice.displayed ? choice.passageTitle : choice))
    return lastNode.choices
      .map(choice => choice.displayed ? choice.passageTitle : choice)
      .includes(choice)
  }

  return false
}

function getChoices(evaluation: IRuntimeNode[]): ChoiceType[] {
  if (!evaluation) return null
  if (evaluation.length === 0) return null

  const lastNode: IChoiceNode = (evaluation[evaluation.length - 1] as IChoiceNode)
  return lastNode.choices
}

function hasNotificationTrigger(node: ISubNode) {
  if (node.type === "trigger_notification") return true
  if (node.type === "passive") return true
  if ("contents" in node) return node.contents.some(hasNotificationTrigger)
  if ("condition" in node) return hasNotificationTrigger(node.then)

  return false
}

function findTriggerNode(nodes: ISubNode[] | ISubNode): ITriggerNode {
  if ("find" in nodes) {
    return (nodes.find(node => node.type === "trigger_notification") as ITriggerNode) // Vérité
  } else {
    return (nodes as ITriggerNode) // Mensonge
  }
}

// Perform a visit while ignoring all non-Text nodes, this will miss any "deep" nodes, of course.
// It's useful for notification purpose.
function extractKnownText(nodes: ISubNode[] | ISubNode): string {
  if ("find" in nodes) {
    // Join everything.
    return nodes
      .filter(node => node.type === "text")
      .map(node => (node as ITextNode).text)
      .join("")
  } else if (nodes.type === "text") {
    return nodes.text
  } else {
    return ""
  }
}

function collectTriggerData(nodes: ISubNode[], startIndex: number) {
  const nodeOfInterest = nodes[startIndex]
  if (nodeOfInterest.type === "trigger_notification") {
    return {
      text: extractKnownText(nodes[startIndex + 1]),
      delay: nodeOfInterest.unit === "ms" ? nodeOfInterest.delay : 1000 * nodeOfInterest.delay,
      condition: undefined
    }
  } else if ("condition" in nodeOfInterest) {
    const tNode = findTriggerNode(nodeOfInterest.then)
    return {
      text: extractKnownText(nodeOfInterest.then),
      delay: tNode.unit === "ms" ? tNode.delay : 1000 * tNode.delay,
      conditionNode: nodeOfInterest.condition
    }
  } else if ("contents" in nodeOfInterest && nodeOfInterest.type !== "passive" && nodeOfInterest.contents[0].type !== "passive") {
    const tNode = findTriggerNode(nodeOfInterest.contents)
    return {
      text: extractKnownText(nodeOfInterest.contents),
      delay: tNode.unit === "ms" ? tNode.delay : 1000 * tNode.delay,
      conditionNode: nodeOfInterest
    }
  } else if ("contents" in nodeOfInterest && (nodeOfInterest.type === "passive" || nodeOfInterest.contents[0].type === "passive")) {
    
    return {
      text: extractKnownText(nodeOfInterest.contents),
      conditionNode: nodeOfInterest,
      type: "passive"
    }
  }
  else {
    throw new Error(`Unsupported node of interest ${nodeOfInterest.type}`)
  }
}

/**
 * Model description here for TypeScript hints.
 */
const ScenarioNode = types
  .model("ScenarioNode")
  .props({
    id: types.identifier,
    name: types.string,
    rawRepresentation: types.frozen<ISubNode[]>(),
    currentEvaluation: types.frozen<IRuntimeNode[]>([]), // Empty evaluation.
    wasEvaluated: types.boolean,
    frozenNode: types.boolean,
    resumeIndex: types.optional(types.integer, 0),
    isInitNode: types.optional(types.boolean, false)
  }).actions(self => ({
    shift() {
      const [, ...rest] = self.currentEvaluation
      self.currentEvaluation = rest
      self.resumeIndex += 1
    },
    fastForward() {
      self.currentEvaluation = self.currentEvaluation.slice(self.resumeIndex)
    },
    // Might take some time with complex scenarios.
    // Better use structural sharing.
    evaluate(gameState) {
      // Do not evaluate frozen nodes.
      if (self.frozenNode) return

      self.currentEvaluation = evaluateRawRepresentation(gameState,
        self.rawRepresentation)
      self.wasEvaluated = true
    }
  })).actions(self => ({
    freeze() {
      // We don't freeze unevaluated nodes.
      if (!self.wasEvaluated) throw new Error("Freezing a non-evaluated node!")
      self.currentEvaluation = self.currentEvaluation
        .map(unwrapAndUnwait)
        .filter(n => !!n)
      self.frozenNode = true
    }
  }))
export const ScenarioModel = types
  .model("Scenario")
  .props({
    id: types.identifier,
    ifid: types.string,
    nodes: types.map(ScenarioNode),
    currentNode: types.maybeNull(types.reference(ScenarioNode)),
    state: types.enumeration("State", ["empty",
      "initializing",
      "evaluating",
      "ready"])
  })
  .views(self => ({
    get currentChoices() {
      return getChoices(self.currentNode.currentEvaluation)
    },
    get currentMessage() {
      if (!self.currentNode.currentEvaluation) return null
      if (self.currentNode.currentEvaluation.length === 0) return null
      const head = self.currentNode.currentEvaluation[0]
      if (!head || head.type === "choices") {
        return null
      } else {
        return head
      }
    },
    get pendingMessages() {
      return self.currentNode.currentEvaluation ? self.currentNode.currentEvaluation.slice(1).splice(-1, 1) : []
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    // Parse the contents of filename.
    // Register the whole graph in this scenario model.
    init(twineScenario) {
      self.state = "initializing"
      let startChoice = twineScenario.passages.find(
        passage => passage.pid === twineScenario["startnode"]
      )
      if (!startChoice) {
        console.error("No start choice in the provided Twine scenario!")
        throw new Error("Invalid scenario")
      }
      startChoice = startChoice.name

      const triggers = []
      for (const rawNode of twineScenario.passages) {
        const rawRepr = parser.parse(rawNode.text)
        self.nodes.put(
          {
            id: `${self.ifid}-${rawNode.name}`,
            name: rawNode.name,
            rawRepresentation: rawRepr,
            wasEvaluated: false,
            frozenNode: false
          }
        )

        // We only support notification trigger for the first node.
        if (hasNotificationTrigger(rawRepr[0])) {
          const triggerData = collectTriggerData(rawRepr, 0)
          triggerData.idNode = self.ifid + "-" + rawNode.name

          triggers.push(
            triggerData
          )
        }
      }

      self.currentNode = self.nodes.get(`${self.ifid}-${startChoice}`)
      self.state = "ready"

      return triggers
    },
    setCurrent(node) {
      self.currentNode = self.nodes.get(node)
      self.currentNode.isInitNode = true
    },
    evaluateCurrent(gameState) {
      self.currentNode.evaluate(gameState)
      if (self.currentNode.resumeIndex > 0)
        self.currentNode.fastForward()
    },
    freezeCurrent() {
      self.currentNode.freeze()
      return (self.currentNode.currentEvaluation as IStaticNode[])
    },
    archive() {
      if (!self.currentNode.currentEvaluation) return
      // Archive the first message (we don't really lose anything.)
      self.currentNode.shift()
    },
    choose(choice): boolean {
      if (!hasChoice(choice, self.currentNode.currentEvaluation)) {
        console.debug(`Current evaluation has no such choice ${choice}`, self.currentNode)
        return false
      }
      const checkpointNode = self.currentNode
      self.currentNode = self.nodes.get(`${self.ifid}-${choice}`)
      if (!self.currentNode) {
        self.currentNode = checkpointNode
        //console.warn("Invalid choice, rollback'd choice to previous node.")
        return false
      }
      return true
    },
    produceNewMessage(gameState, chatHistory) {
      // FIXME(beta): take in account gameplay type.
      if (!self.currentNode.wasEvaluated) {
        self.state = "evaluating"
        self.currentNode.evaluate(gameState)
      }
      self.state = "ready"
    }
  }))

/**
 * Un-comment the following to omit model attributes from your snapshots (and from async storage).
 * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

 * Note that you'll need to import `omit` from ramda, which is already included in the project!
 *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
 */

type ScenarioType = Instance<typeof ScenarioModel>

export interface Scenario extends ScenarioType {
}

type ScenarioSnapshotType = SnapshotOut<typeof ScenarioModel>

export interface ScenarioSnapshot extends ScenarioSnapshotType {
}
