import * as RNLocalize from "react-native-localize"
import i18n from "i18next"
import { initReactI18next } from "react-i18next"

import * as enRes from "./en"
import * as frRes from "./fr"
import * as esRes from "./es"

function lowercaseAllKeys(o) {
  return Object.fromEntries(Object.entries(o).map(([k, v]) => [k.toLowerCase(), v]))
}

// FIXME(Ryan): think of a better way to glob import a folder of JSON known in advance…
const catalog = {
  en: lowercaseAllKeys(enRes),
  fr: lowercaseAllKeys(frRes),
  es: lowercaseAllKeys(esRes),
}

const fallback = { languageTag: "en", isRTL: false }
const { languageTag } = RNLocalize.findBestAvailableLanguage(Object.keys(catalog)) || fallback

export const changeLanguage = (language) => {
  i18n.use(initReactI18next).init({
    resources: catalog,
    lng: language?language:languageTag,
    interpolation: {
      espaceValue: false,
    },
  })
}

export default i18n
