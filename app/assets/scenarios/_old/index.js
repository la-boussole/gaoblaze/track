export default {
  "Ajay": require("./Ajay.json"),
  "Ally": require("./Ally.json"),
  "Amin": require("./Amin.json"),
  "Lucas": require("./Lucas.json"),
  "Masako": require("./Masako.json"),
  "Nikki": require("./Nikki.json"),
  "Rokaya": require("./Rokaya.json"),
  "Sol": require("./Sol.json"),
  "Alex": require("./Alex.json"),
  "NikkiBlaze": require("./Nikki-Blaze.json"),
  "AlexBlaze": require("./Alex-Blaze.json"),
  //"Test": require("./Alex-11.json"),
}
