import { BlazeProfile } from "./blaze-profiles";

export const BLAZE_PROFILES_ES: BlazeProfile[] = [
  {
    key: "Ajay",
    about: "19 años. Soltero. No tiene hijos. Estudiante. Top player (Gao The Sage).",
    image: require("assets/images/community/avatars/B-Ajay.png"),
    informationShared: "SMS, Acelerómetro, Contactos, Geolocalización, Almacenamiento, Wifi, Red, Notificaciones, Bluetooth, NFC, Alarma, Fondo de pantalla, Micrófono, Lector de huellas dactilares, Calendario, Cámara",
    overview: "Estudiante, vegetariano, sociable, curioso, nuevo inmigrante, amante de la ciencia.",
    recommendations: "Datos de intereses y redes sociales recopilados (venta de clientes #12579 - Agencia de Seguridad Nacional): El análisis de la base de datos de comunicaciones y respuestas de tipo 3 de Gao The Sage indican que el sujeto tiene muchos conocidos y amigos implicados en los movimientos sociales de Oriente Medio. El mapeo de metadatos identificó el 78% de sus contactos.\n"+
      "Nota interna: reciente caída de las comunicaciones con sus contactos, tras la anterior venta de datos (cliente nº 55578 - agencia de seguridad privada).",
    thought: {
      title: 6,
      tweet: "Represión",
      overview: "La vigilancia masiva facilita la represión estatal",
      message: "La vigilancia masiva en sí misma ya es una violación de nuestros derechos fundamentales. Desgraciadamente, también permite que se cometan infracciones aún más graves. Los gobiernos no democráticos pueden utilizarla, por ejemplo, para suprimir el poder compensatorio de los medios de comunicación o de los denunciantes, que con su vigilancia fortalecen el estado de derecho. \n"+
      "Como reveló el denunciante https://es.wikipedia.org/wiki/Edward_Snowden, la vigilancia llevada a cabo por los gigantes de Internet alimenta directamente la vigilancia llevada a cabo por ciertos Estados. Protegerse de uno de ellos le permite protegerse del otro, pero también proteger a los que le rodean. De hecho, nuestras prácticas tienen consecuencias para las personas que nos rodean. Para algunos de ellos, mantener la información sobre sí mismos en secreto puede ser muy importante, incluso vital.\n" +
      "Actualmente, el Programa Pegasus es el https://es.wikipedia.org/wiki/Pegasus_(spyware) más reciente de cómo algunos gobiernos vigilan a otros (jefes de Estado, ciudadanos, periodistas) a través de sus teléfonos móviles.",
      links: [
        {
          text: "ejemplo",
          link: "https://es.wikipedia.org/wiki/Pegasus_(spyware)"
        },
        {
          text: "Edward Snowden",
          link: "https://es.wikipedia.org/wiki/Edward_Snowden"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Haz un balance de tu situación",
              abstract: "Empecemos por identificar los datos que quieres proteger, de qué, de quién, dependiendo de tu situación. Para ello, identifica qué datos son importantes o sensibles para ti y piensa en los posibles peligros si los pierdieras o si cualquier otra persona tuviera acceso a ellos. Esto es lo que se llama en seguridad informática \"establecer un modelo de amenaza\", pero nosotros lo llamaremos modelo de protección.\n" +
              "El siguiente paso es identificar, para cada dato, la gravedad y probabilidad de su pérdida o difusión. De hecho, cada riesgo tiene una probabilidad de ocurrir y una gravedad diferente. Por ejemplo, un riesgo muy, muy, muy improbable sería que los marcianos utilizaran fotos íntimas tuyas para decorar sus naves espaciales. Pero un riesgo muy probable sería que Google utilizara esas mismas fotos para conocerte mejor y venderte productos relacionados.\n" +
              "Al estimar estos dos criterios, la gravedad y la probabilidad, resulta fácil separar los riesgos urgentes y por los que no hay que preocuparse tanto. Puedes utilizar https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ES.cleaned.pdf para pensar en tu propio modelo de amenaza.",
                links: [
                {
                  text: "este modelo",
                  link: "https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ES.cleaned.pdf"
                },
              ],
            },
            {
              title: "Decidir sobre las acciones a realizar",
              abstract: "Gracias a la cartografía que acabas de realizar, ahora puedes identificar los lugares que hay que proteger primero.\n \n" +
                        "Esta vez, necesitas encontrar apoyo de alguien si sientes que no te es sencillo imaginarte cómo avanzar. Cada medida puede ser más o menos difícil de aplicar, pero no te desanimes. A veces hay soluciones muy técnicas, pero otras veces no. Para cada dato que debes proteger, te invito a que te hagas estas preguntas:\n"+
                          "• ¿Mis dispositivos tienen una buena contraseña?\n"+
                          "• ¿Los dejo junto al grifo de agua y no tengo una copia de seguridad de mis datos?\n"+
                          "• ¿La solución que se me ocurre es fácil o difícil de usar? \n"+
                          "• ¿Quién puede darme consejos o ideas sobre cómo proteger tal o cual información?\n"+
                          "• ¿Estas medidas son sólo para mí o más bien a todo un grupo?\n \n" +
                        "Algunas soluciones tendrán ventajas, otras inconvenientes, otras pueden empeorar la situación y otras serán super sencillas y adecuadas. Pero recuerda ser consciente de los peligros que identificaste ya es el principio de la solución, así no pongas ninguna alguna solución en práctica.\n \n"+
                        "Por último, sólo queda elegir y poner en marcha las soluciones que identificaste y de vez en cuando ver si debes adaptar tu plan de soluciones si te encuentrs en nuevos contextos o si tienes nuevas necesidades en términos de seguridad.\n \n"+
                        "Para saber más sobre cómo elaborar un plan de seguridad, te recomiendo la lectura de esta breve https://ssd.eff.org/es/module/evaluando-tus-riesgos publicada en la web de Autodefensa de Vigilancia para completar el documento que te sugerí más arriba.",
              links: [
                {
                  text: "guía",
                  link: "https://ssd.eff.org/es/module/evaluando-tus-riesgos"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_1.pdf"),
          tooltitle: "Protección",
          tweet: "Las amenazas y necesidades de cada persona pueden ser muy diferentes. Veamos cómo encontrar una solución a la medida.",
          title: "¿De qué y de quién protegerse?",
        },
        {
          subskills: [
            {
              title: "Crear varias direcciones de correo electrónico",
              abstract: "En primer lugar, puedes tener direcciones de correo electrónico para diferentes cosas. Lo típico es: una para los seres queridos, otra para el trabajo y otra para asuntos sin importancia, otra para la salud, etc. De este modo, separamos los distintos aspectos de nuestras vidas y así se vuelve más difícil conectarlos entre si.",
            },
            {
              title: "Crear varias cuentas en línea",
              abstract:"Del mismo modo, es posible crear varias cuentas en las redes sociales, con diferentes seudónimos, y configurarlas de forma diferente según las personas a las que nos queramos dirigir. De este modo, sólo las personas que elijamos pueden acceder a nuestras publicaciones en línea y será difícil que pasen de una cuenta a otra sin que lo sepamos.\n \n"+
              "Para reforzar esta protección, una buena práctica es limitar la cantidad de información que proporcionamos a las plataformas en línea. El hecho de que nos pidan datos (aunque lo hagan de manera muy educada) no significa que debamos entregárselos. ¡Siempre podemos poner cualquier cosa aleatoria, no? ",
            },
            {
              title:"Multiplicar tus herramientas digitales",
              abstract:"Para que separar tus círculos sea aún más eficaz, también puedes utilizar varios navegadores web, varias sesiones en tu compu o incluso diferentes aparatos. Aunque estos métodos por sí solos no son suficientes para evitar por completo que las grandes empresas te vigilen, será más difícil relacionarlos.\n \n" +
              "Para saber más sobre esta y otras formas de proteger tus datos, te recomiendo que leas este https://www.ritimo.org/La-confidentialite-la-surveillance-sur-le-Net-le-suivi-des-donnees-pourquoi-est publicado en ritimo.org, una red de información y documentación para la solidaridad y el desarrollo sostenible.",
              links: [
                {
                  text: "artículo [en francés]",
                  link: "https://www.ritimo.org/La-confidentialite-la-surveillance-sur-le-Net-le-suivi-des-donnees-pourquoi-est"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_2.pdf"),
          tooltitle: "Ámbitos",
          tweet: "Una forma sencilla de proteger mejor nuestros datos personales es separar los distintos ámbitos de nuestras vidas (por ejemplo, amigos, trabajo, salud, compras, etc.). Pero, ¿cómo podemos hacerlo?",
          title: "Separar tus distintos círculos sociales",
        },
        {
          subskills: [
            {
              title: "Elije una contraseña",
              abstract: "Para proteger tus datos, es importante tener un código de desbloqueo : puedes decidir utilizar un patrón, números o una contraseña, est una de las medidas más importantes y sencillas ! Después de cada cierre de pantalla, será necesario introducir este código para acceder a tu teléfono.\n \n" +
                "Para configurarlo o cambiarlo, dirígete a los Ajustes de tu teléfono y, a continuación, al menú Huellas dactilares, cara y contraseña. Para decidir cuánto tiempo debe pasar antes de que la pantalla se apague automáticamente, ve al menú Pantalla y brillo.\n \n" +
                "Para mayor seguridad, te recomiendo que evites las contraseñas fáciles, como los métodos biométricos (cara o huellas dactilares) o los dibujos o códigos demasiado fáciles de adivinar (como la forma de la primera letra de tu nombre o tu fecha de nacimiento). Para saber más, he aquí https://chayn.gitbook.io/basic-diy-online-privacy/es/passwords para elegir contraseñas seguras y la https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut para utilizar un gestor de contraseñas que ofrece el sitio web NextINpact.com.",
              links: [
                {
                  text: "recomendaciones",
                  link: "https://chayn.gitbook.io/basic-diy-online-privacy/es/passwords"
                },
                {
                  text: "guía [en francés]",
                  link: "https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut"
                },
              ],
            },
            {
              title:"Copia tu código IMEI",
              abstract:"El código IMEI es un número que identifica su teléfono de forma exclusiva. Puedes conseguirlo marcando *#06#. También lo encontrarás en la caja y la factura de tu teléfono. Guárdalo bien : lo necesitarás para bloquear tu teléfono permanentemente en caso de robo.",
            },
            {
              title:"Mantén tu teléfono actualizado",
              abstract:"Instala las actualizaciones siempre y tan pronto como sea posible. Además de nuevas funciones, suelen incluir correcciones de errores.\n \n"+
              "Para quitarte la molestia, ve a los Ajustes de tu tienda de aplicaciones, y luego al menú de Actualizaciones si usas el Play Store. Aquí, pulsa sobre Activar actualizaciones automáticas u Obtener actualizaciones automáticamente.",
            },
            {
              title:"Cifra tu teléfono",
              abstract:"La mayoría de los teléfonos ofrecen encriptación de los datos que contienen, es decir, sólo la persona que conoce la contraseña de desbloqueo puede acceder. \n"+
              "Dependiendo de tu versión de Android, el proceso y el método pueden variar. En caso de que la opción no esté ya activada por defecto y si está disponible, puedes cifrar la memoria de tu teléfono o tu tarjeta micro SD. Para ello, dirígete a los Ajustes de tu teléfono y luego al menú de Seguridad.\n \n"+
              "Sin embargo, ¡ten cuidado! En el caso de una tarjeta SD, la operación comienza borrando todos los datos, así que asegúrate de tener una copia en otro lugar. Y en cualquier caso, si pierdes la contraseña, tus datos también se perderán realmente.",
            },
            {
              title:"No olvides los metadatos",
              abstract:"Cuando tomamos fotos, nuestro teléfono móvil añade automáticamente informaciones, como la fecha, la geolocalización o el ajuste de la cámara. Esto se llama metadatos. Esta información se utiliza para clasificar nuestras fotos, pero puede revelar mucha más información que la propia imagen. Recuerda comprobarlos antes de compartir tus fotos, o eliminarlos por completo. Para ello, puedes utilizar la aplicación Scrambled Exif (que encuentras en https://f-droid.org/es/packages/com.jarsilio.android.scrambledeggsif/ o en el Google https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif).",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/com.jarsilio.android.scrambledeggsif/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif"
                },
              ],
            },
            {
              title:"Desactiva funciones innecesarias",
              abstract:"Cuando no la uses, desactiva la geolocalización, el wifi y el bluetooth. Al hacer esto, limitarás los datos que se recojen sobe ti, mejorarás tu seguridad y la duración de la batería. Para ello, coloca el dedo en la barra de estado, deslízalo hacia abajo y pulsa en los íconos de cada función.",
            },
            {
              title:"Dale prioridad a las páginas web en vez de las app",
              abstract:"En general, es mejor utilizar la página web que su aplicación dedicada: limitarás los datos a los que puede acceder en tu teléfono.",
            },
            {
              title:"Reinicia tus dispositivos nuevos y viejos",
              abstract:"Si recibes un teléfono usado o te dejas de usar el tuyo, no olvides borrar todos los datos que contiene. Esta breve https://myshadow.org/es/browser-tracking de la Comisión Nacional de Informática y Libertades (CNIL) explica cómo hacerlo.",
              links: [
                {
                  text: "guía",
                  link: "https://myshadow.org/es/browser-tracking"
                },
              ],
            },
            {
              title:"Haz balance cada tanto",
              abstract:"Ninguna solución informática es 100% segura ni durará para siempre. Recuerda estar pendiente de vez en cuando si las elecciones que ha hecho siguen siendo pertinentes y si todavía te convienen.",
            },
            {
              title:"No olvides los detalles",
              abstract:"Cuando se trata de seguridad informática, un sistema es igual de fuerte que su eslabón más débil: osea basta con un pequeño detalle para que todo se vuelva frágil. Por lo tanto, presta atención a lo parecieran detalles.\n \n" +
              "Por ejemplo, te recomiendo que cambies el teclado que viene por defecto (Gboard) en tu teléfono por un teclado libre. Los que te recomiendo son OpenBoard o Android AOSP, que puedes instalar desde https://f-droid.org/es/packages/org.dslul.openboard.inputmethod.latin/ o https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin. Para usarlo, ve a los Ajustes de tu teléfono, luego a Otros ajustes, luego a Teclado y método de entrada para elegir OpenBoard o AOSP y configúralo.",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/org.dslul.openboard.inputmethod.latin/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin"
                },
              ],
            },
            {
              title:"Adoptar una actitud respetuosa",
              abstract:"Más allá de todas estas recomendaciones, proteger tus datos y los de tus seres queridos también implica adoptar actitudes respetuosas con los demás. Por ejemplo, no olvides preguntar a la persona si puedes compartir información sobre ella, no guardes todo por costumbre y minimiza la cantidad de datos que generas. Ayúda a personas que conocen menos del tema, discute y comparte tus descubrimientos...\n \n" +
                "Y para llegar a soluciones realmente satisfactorias y no dejar a nadie rezagado, es necesario organizarse colectivamente para mejorar las leyes y las herramientas digitales que utilizamos para protegernos. Si te interesa esta aventura o simplemente quiere estar al día de las últimas noticias sobre el tema, te aconsejo que visites la https://www.laquadrature.net/es/ Para ir más allá, encontrarás muchas otras recomendaciones en el sitio de https://datadetoxkit.org/es/home/ y en la completísima https://guide.boum.org/ .",
              links: [
                {
                  text: "página web de La Quadrature du Net.",
                  link: "https://www.laquadrature.net/es/"
                },
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/es/home/"
                },
                {
                  text: "guía de autodefensa digital [en francés]",
                  link: "https://guide.boum.org/"
                },
              ],
            }
          ],
          tweet: "Además de todas las habilidades que te he presentado, hay algunas cosas sencillas que podemos hacer para que nuestros teléfonos sean más seguros de forma rápida y sencilla. Algunos incluso las llaman \"reglas de higiene digital\".",
          //pdf: require("assets/pdf/Ajay_3.pdf"),
          tooltitle: "Proteger",
          title: "Proteger nuestros datos y los de nuestros seres queridos",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Sally" },
  { key: "Pierre" },
  { key: "Molly" },
  {
    key: "Alex",
    about: "28 años. Separada (ex-cónyuge usuario#1547699, nombre de usuario \"Ben\"). 1 bebé (\"Ulysse\"). Desempleada. Top player (Gao the Intruder).",
    image: require("assets/images/community/avatars/B-Alex.png"),
    informationShared: "Contactos, Geolocalización, Almacenamiento, Wifi, Red, Notificaciones, SMS, Bluetooth, NFC, Alarma, Fondo de pantalla, Micrófono, Lector de huellas dactilares, Calendario, Cámara, Acelerómetro",
    overview: "Sociable, eficiente en el trabajo, falta de confianza en sí misma, 86% de probabilidad de estar deprimida, invertida en Gao Games (moderador de chat), cerca de Blaze.",
    recommendations: "Datos del estado emocional recogidos (uso interno): Perfil psicológicamente frágil susceptible de invertir mucho tiempo de trabajo voluntario en Gao Games a cambio de reconocimiento. Ofrecer oportunidades de trabajo.\n" +
      "Nota interna (manager): Se debe supervisar manualmente. Actividades sospechosas tras la desaparición de Blaze.",
    enabled: true
  },
  {
    key: "Ally",
    about: "35 años. Soltera. Sin hijos. Diseñadora gráfica en Gao Games. Top player (Gao the Athlete).",
    image: require("assets/images/community/avatars/B-Ally.png"),
    informationShared: "Acelerómetro, Contactos, Geolocalización, Almacenamiento, Wifi, Red, Notificaciones, SMS, Bluetooth, NFC, Alarma, Fondo de pantalla, Micrófono, Lector de huellas dactilares, Calendario, Cámara",
    overview: "Eficiente, organizada, poco empática, buena planificadora, problemas de salud.",
    recommendations: "Datos de salud recogidos (venta de clientes #16854 - aseguradora médica): sobrepeso persistente a pesar de hacer ejercicio regularmente, 72% de probabilidad de problemas de tiroides, mayor riesgo de eventos cardiovasculares. Sujeto no rentable. Considerar la posibilidad de cancelar el seguro de salud o aumentar la póliza.",
    thought: {
      title: 7,
      tweet: "Robots y humanos",
      overview: "¡Los robots también pueden cometer errores!",
      message: "Las herramientas digitales son cada vez más importantes en nuestra vida cotidiana y nos ayudan en cada vez más actividades. Sin que seamos necesariamente conscientes de ello, las decisiones que afectan a nuestras vidas son tomadas por máquinas. O mejor dicho... ¡por la gente que los diseñó! \n"+
               "Por supuesto, estas máquinas están lejos de ser infalibles: sus programas informáticos pueden funcionar mal, o los datos utilizados para tomar estas decisiones pueden ser incorrectos. En otros casos, las personas que diseñaron estos programas imaginaron que nuestros cuerpos y nuestros usos se parecerían a los suyos, y... https://www.uoc.edu/portal/es/news/actualitat/2020/441-racismo-sexismo-internet.html .\n"+
               "Todos estos problemas potenciales pueden llevar al mismo resultado: los ciudadanos no pueden utilizar estas herramientas con normalidad ni beneficiarse de los servicios a los que dan acceso. Por desgracia, las personas que más experimentan estas dificultades suelen ser las que ya sufren otras formas de desigualdad. Por ejemplo, las voces femeninas o la piel oscura son menos reconocidas por muchas herramientas digitales que las voces masculinas y la piel clara. Esto es molesto cuando quieres usar el comando de voz de tu GPS o una aplicación de reconocimiento facial. Si te interesa este tema, puede escuchar este podcast en francés.",
      links: [
        {
          text: "se equivocaron",
          link: "https://www.uoc.edu/portal/es/news/actualitat/2020/441-racismo-sexismo-internet.html"
        },
        {
          text: "podcast [en francés]",
          link: "https://www.binge.audio/podcast/les-couilles-sur-la-table/des-ordis-des-souris-et-des-hommes/"
        }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Limpia las autorizaciones ya concedidas",
              abstract: "Cuando utilizas una aplicación por primera vez, te debe pedir permiso antes de acceder a tus datos o a las funciones de tu teléfono. Por ejemplo, podría tratarse de tu geolocalización, micrófono, cámara, lista de contactos... ¡datos y funciones potencialmente sensibles!\n" +

                "Con el tiempo, puede ser difícil recordar qué permisos has concedido o no a una aplicación. Para ver cuáles ya diste y poderlos cambiar, ve a la configuración de tu teléfono y luego al menú de permisos o aplicaciones.",
            },
            {
              title: "¿Dónde puedo encontrar la lista de autorizaciones?",
              abstract: "Encontrarás una lista completa de las aplicaciones ya instaladas en tu teléfono y los permisos concedidos a cada aplicación. Si buscas por \"permisos\", podrás ver todas las aplicaciones que acceden a un permiso en particular (por ejemplo, todas las aplicaciones que acceden a tu geolocalización o a tus contactos). Desde este menú, puedes desinstalar las aplicaciones o denegar los permisos ya concedidos.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss4.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss5.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss6.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss7.png")
                },
              ],
            },
            {
              title: "Obtén información detallada",
              abstract: "Para obtener información aún más detallada sobre los permisos de tu teléfono, no dudes en utilizar el \"escáner\" de Nikki, el CatScan. (Si no sabes lo que es, aún tienes mucho que descubrir aquí… ! )\n" +

                "El CatScan te permitirá ver el número de rastreadores en las aplicaciones instaladas y controlar tu progreso protegiendo tus datos en tu teléfono. ¡Es super útil !",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss8.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss9.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_1.pdf"),
          tooltitle: "Permisos",
          tweet: "Para funcionar, algunas aplicaciones requieren permisos especiales. Por ejemplo, si una aplicación de \"cámara\" solicita acceso a la cámara, parece lógico. Pero otras veces, las solicitudes de permisos de acceso no tienen nada que ver con el funcionamiento de la aplicación y sólo están ahí para recoger datos personales nuestros. Por ejemplo, una app de música que te pida acceso a tus contactos y a tus movimientos parece extraño. ¡Vamos a ver cómo configurar los para que nos protejan mejor!",
          title: "Configurar los permisos",
          showCheckBoxes: true
        },
        {
          subskills: [
            {
              title: "Controla el uso de tus datos",
              abstract: "Puedes contactar a las organizaciones privadas o públicas que recogen, utilizan o venden nuestros datos personales, y solicitarles información, correcciones, supresiones o copias de nuestros datos.\n" +
              "Esto es válido para las cuentas y servicios abiertos por residentes de la Unión europea y sus ciudadanos cuando están en otros países.\n" +

              "Cada país de la Unión Europea tiene su propia institución encargada de verificar que se apliquen estas medidas. https://edpb.europa.eu/about-edpb/about-edpb/members_es tienes la lista por países. Y para los demás países del mundo, te recomiendo este https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde. En Francia, la Comisión Nacional de Informática y Libertades (CNIL) se encarga de ayudarnos a hacer valer nuestros derechos en este ámbito y nos proporciona guías sobre nuestros https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles y https://www.cnil.fr/modeles/courrier de cartas para hacerlos valer fácilmente.\n" +

                "Imaginemos que buscas tu nombre en Internet y encuentras información personal que no quieres que sea pública. El primer paso para eliminar esa info es escribir a la dirección de contacto de la página en cuestión. Una vez que hayas enviado tu solicitud, los responsables del sitio deben responderte en un plazo de 30 días y muchas veces este mail es suficiente para que quiten tu información.",
              links: [
                {
                  text: "aquí",
                  link: "https://edpb.europa.eu/about-edpb/about-edpb/members_es"
                },
                {
                  text: "mapa detallado [en francés]",
                  link: "https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde"
                },
                {
                  text: "distintos derechos [en francés]",
                  link: "https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles"
                },
                {
                  text: "modelos [en francés]",
                  link: "https://www.cnil.fr/modeles/courrier"
                },
              ],
            },
            {
              title: "Poner una demanda",
              abstract:"Si no eliminan tu información al cabo de 30 días, puedes enviar https://www.cnil.fr/fr/plaintes a la CNIL o a la autoridad de tu país, que puede sancionar la página. Tristemente la CNIL y otras instituciones disponen de unos pocos recursos (y por lo tanto de personas) para atender los mensajes y pueden tardarse en responderte. Si transcurren 30 días sin dar respuesta, puedes presentar https://www.service-public.fr/particuliers/vosdroits/F1435  ante la policía o la fiscalía para hacer valer sus derechos.",
              links: [
                {
                  text: "una reclamación [en francés]",
                  link: "https://www.cnil.fr/fr/plaintes"
                },
                {
                  text: "una denuncia [en francés]",
                  link: "https://www.service-public.fr/particuliers/vosdroits/F1435"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_2.pdf"),
          tooltitle: "Derechos",
          tweet: "La Unión Europea garantiza la protección de datos de sus nacionales y residentes. Desde su entrada en vigor en 2018, el Reglamento General de Protección de Datos (RGPD) todas las personas pueden controlar el uso que se hacen de sus datos. ¡Veamos de qué se trata ",
          title: "Protegerte más allá de las fronteras!",
        },
        {
          subskills: [
            {
              title: "Navegar por la web con Tor Browser",
              abstract: "Para protegerse de los bichos y preservar tu privacidad, la solución más sencilla y eficaz es utilizar https://www.torproject.org/es/download/#android. Este navegador basado en Firefox es gratuito y de código abierto, y está diseñado específicamente para evitar la vigilancia y la censura.\n" +

              "Sin embargo, tenga cuidado: aunque sea muy eficiente, el Navegador Tor no le exime de seguir navegando en HTTPS o de actualizarse regularmente. Además, los sitios web que visitas pueden reconocerte en determinadas situaciones, por ejemplo, si decides identificarte con una cuenta o un correo electrónico.",
              links: [
                {
                  text: "Tor Browser.",
                  link: "https://www.torproject.org/es/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Navegar por la web con el navegador Duckduckgo",
              abstract: "Si tu conexión no es lo suficientemente rápida o algunos de los sitios que quieres visitar bloquean Tor Browser, la solución alternativa que recomiendo es utilizar el navegador https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (también disponible en la tienda de aplicaciones de https://f-droid.org/es/packages/com.duckduckgo.mobile.android/). Se trata de un navegador gratuito que bloquea los rastreadores y te indica el grado de privacidad de los sitios que visitas. Sin embargo, ten en cuenta que no hará que tu conexión sea completamente anónima (los sitios conocerán tu dirección IP y dónde te encuentras).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/com.duckduckgo.mobile.android/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "Cuando navegamos por la web, los rastreadores nos siguen de un sitio a otro ( https://myshadow.org/es/browser-tracking ). Lo hacen para deducir nuestros gustos, intereses y un montón de cosas más sobre nosotros. ¡Veamos cómo protegernos!",
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Rastradores",
          title: "Protegerse de los rasteadores que nos siguen ",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Fred" },
  {
    key: "Amin",
    about: "Entre 36 y 43 años. Casado (usuario cónyuge#6874215, nombre de usuario \"Masako\"). 1 infante de corta edad (\"Alma\"). Ingeniero de calidad. Top player (Gao the Filekeeper).",
    image: require("assets/images/community/avatars/B-Amin.png"),
    informationShared: "Almacenamiento, SMS, Alarma, Micrófono, Calendario, Cámara, Contactos, Geolocalización, Wifi, Red",
    overview: "Tranquilo, responsable, respetuoso de la ley, con un crédito bancario (por estudios), hija enferma (enfermedad degenerativa), ansioso, nostálgico, soñador.",
    recommendations: "Datos del estado emocional recogidos (venta del cliente #03548 - agencia de publicidad): sujeto con estrés (46% enfermedad de la hija, 28% lidiar con la esposa, 26% sobrecarga de trabajo) emocionalmente receptivo. Deseos de viajar. 79% de probabilidades de comprar servicios de viajes turísticos (tipo de escapada).",
    thought: {
      title: 8,
      tweet: "Manipulación",
      overview: "Interfaces diseñadas para engañarnos y manipularnos",
      message: "En Internet y en nuestros teléfonos, muchas empresas utilizan interfaces engañosas, cuidadosamente diseñadas para manipularnos. En inglés, se les denominan dark patterns. \n"+
      "A través de notificaciones engañosas, organización de los botones, los colores elegidos, de las opciones preseleccionadas, o de informaciones difíciles de ver... es posible hacer que terminemos aceptando cosas que probablemente habríamos rechazado si nos las hubieran presentado con claridad. . Estas empresas saben utilizar los mecanismos de placer y recompensa de nuestro cerebro y que queramos utilizar sus aplicaciones, sus servicios, que pasemos cada vez más tiempo usándolos incluso pueden generarnos verdaderas adicciones. Peor aún: a partir de los muchos datos que recopilan sobre nosotros, las empresas establecen perfiles precisos para detectar nuestras debilidades y empujarnos a sobreconsumir. Por ejemplo, intentan determinar cuándo estamos cansados o cuándo nos falta confianza, y se aprovechan para impulsarnos a hacer compras cosas que habríamos rechazado el resto del tiempo.  Participé en esta manipulación con los Juegos de Gao, fue un error por mi parte. Cuando lo entendí realmente, solo pude huir del monstruo que había creado.\n" +
      "Para saber más al respecto, puede ver esta https://www.arte.tv/es/videos/RC-017841/dopamina/ de ARTE, este https://www.internetactu.net/2016/06/16/du-design-de-nos-vulnerabilites/ del sitio web InternetActu.net y esta https://linc.cnil.fr/dark-patterns-quelle-grille-de-lecture-pour-les-reguler de la Comisión Nacional de la Informática y las Libertades (CNIL).",
      links: [
        {
          text: "miniserie",
          link: "https://www.arte.tv/es/videos/RC-017841/dopamina/"
        },
        {
          text: "artículo [en francés]",
          link: "https://www.internetactu.net/2016/06/16/du-design-de-nos-vulnerabilites/"
        },
        {
          text: "publicación [en francés]",
          link: "https://linc.cnil.fr/dark-patterns-quelle-grille-de-lecture-pour-les-reguler"
        },
      ],
      skills: [
        {
          subskills: [
            {
              abstract: "Para ello, te recomiendo que añadas la extensión https://ublockorigin.com/ a tu navegador web, y que instales las aplicaciones https://play.google.com/store/apps/details?id=org.blokada.alarm.dnschanger y/o https://f-droid.org/es/packages/org.adaway/ (si utilizas la tienda de aplicaciones F-Droid).\n" +
                "Una vez que hayas instalado una de estas aplicaciones, lo único que tienes que hacer es lanzarla y activarla para que te proteja (ya verás, es muy sencillo). Para saber más sobre cómo bloquear la publicidad en tu compu, te sugiero que visites esta página https://bloquelapub.net/",
              title: "Bloquear las propagandas ",
              links: [
                {
                  text: "uBlock Origin",
                  link: "https://ublockorigin.com/"
                },
                {
                  text: "Blokada",
                  link: "https://play.google.com/store/apps/details?id=org.blokada.alarm.dnschanger"
                },
                {
                  text: "AdAway",
                  link: "https://f-droid.org/es/packages/org.adaway/"
                },
                {
                  text: "bloquelapub.net [en francés]",
                  link: "https://bloquelapub.net/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss12.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss13.png")
                },
              ],
            },
          ],
          tweet: "La publicidad es una puerta de entrada para instalar de virus en nuestros aparatos. También causa muchos otros problemas, como la contaminación con desechos informáticos, que la navegación se lenta, aumento del consumo de energía, permite la difusión de estereotipos y fomenta el consumismo. Afortunadamente, se puede deshacerse de ella en tu teléfono (y computadores).",
          title: "Bloquear las propagandas",
          //pdf: require("assets/pdf/Amin_1.pdf"),
          tooltitle: "Propagandas",
        },
        {
          subskills: [
            {
              abstract: "Cuanto más tiempo pasamos en las herramientas de los gigantes de Internet, más datos recogen sobre nosotros y mayores son sus ganancias a costa nuestra. También aumenta el riesgo de que desarrollemos un comportamiento adictivo y compulsivo.\n \n" +
              "¡Lo peor es que son estas mismas empresas que intentan hacernos creer que es nuestra culpa y nos https://www.blogdumoderateur.com/encourager-addiction-promouvoir-deconnexion/ ! Para saber más, te recomiendo que leas las http://www.internetactu.net/2019/01/14/retro-design-de-lattention-cest-complique/ del trabajo de la Fundación Internet de Nueva Generación (Fing) sobre este tema.\n \n" +
              "Reconocer los mecanismos que explotan nuestro inconsciente es complicado: están diseñados para ser difíciles de identificar. Te invito a que veas sin moderación la miniserie de Arte \"Dopamina\" donde entendemos cómo esta hormona es estimulada por algunas de las aplicaciones de nuestros teléfonos. ¡Comprender es actuar!\n" +
              "Es realmente importante mantener el control y una relación sana con las herramientas digitales. He aquí algunas soluciones.",
              title: "Comprender este tema",
              links: [
                {
                  text: "hacen sentir culpables de la addición [en francés]",
                  link: "https://www.blogdumoderateur.com/encourager-addiction-promouvoir-deconnexion/"
                },
                {
                  text: "conclusiones",
                  link: "http://www.internetactu.net/2019/01/14/retro-design-de-lattention-cest-complique/"
                },
              ],
            },
            {
              abstract: "En primer lugar, empieza por hacer un balance personal : ¿Qué quieres hacer con este aparato? ¿Qué usos conectados te parecen útiles e interesantes? ¿Cuáles prefiere dejar de lado? ¿Qué efectos negativos quieres limitar? ¿Qué espacio quieres darles en tu vida ?\n \n" +
                "Por supuesto, las respuestas a estas preguntas no serán las mismas para todo el mundo: algunas personas encontrarán un servicio online realmente gratificante, mientras que otras lo considerarán una pérdida de tiempo. No importa: lo importante es que respondas a estas preguntas con honestidad y encuentres las respuestas que te sirvan, para que puedas redirigir tu atención a lo que crees que realmente vale la pena.\n \n" +
                "El primer hábito que hay que adoptar es evitar las prisas y tomarse el tiempo necesario para leer realmente lo que le ofrecen los sitios y las aplicaciones. Cuanto más un sitio o servicio te pongq presión para tomar una decisión, más importante es asegurarse que entiendes para qué sirve y cómo funciona.\n \n" +
                "Otro buen hábito es determinar los momentos y lugares en los que decides no utilizar tus dispositivos digitales. Por ejemplo, en la habitación en donde duermes, o sólo en tu cama, en la cocina, durante las comidas, antes de acostarte, cuando estás con alguien... Incluso puedes discutirlo con tus seres queridos para decidir unas normas comunes.",
              title: "Hacer balance y desarrollar buenos hábitos",
            },
            {
              abstract: "Un buen reflejo es desconfiar sistemáticamente de las sugerencias e invitaciones automáticas a consultar una aplicación, sobre todo cuando utilizan tus emociones (como recordarte fotos o momentos en particular). Para poder rechazar lo que el dispositivo te sugiere hacer, empieza por tomarte el tiempo de preguntarte si realmente quieres hacerlo.\n \n" +
              "También hay una solución para que tu teléfono deje de molestarte y puedas consultarlo sólo cuando quieras: desactivar las notificaciones. Para desactivarlas por completo, ve a los Ajustes de tu teléfono, luego al menú de Notificaciones y Barra de Estado y al Aviso de Notificación. Por último, selecciona \"Sin aviso\".",
              title: "Desactivar todas las notificaciones",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss14.png")
                },
              ],
            },
            {
              abstract: "Si prefieres, también puedes optar por ajustar y permitir o no las notificaciones de cada aplicación individualmente. Ve a los Ajustes de tu teléfono, luego al menú de Notificaciones y barra de estado y a Gestionar notificaciones. Allí encontrarás la lista de las aplicaciones instaladas. Pulsando sobre cada una de ellas, puedes desmarcar la opción Permitir notificaciones.",
              title: "Afina tu configuración",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss15.png")
                },
              ],
            },
            {
              abstract: "Otra forma de hacerlo es activar el modo No Molestar durante un tiempo. Para ello, coloca el dedo en la barra de estado, deslízalo hacia abajo y pulsa el icono de No molestar.\n \n" +
              "También es posible activar automáticamente este modo durante las franjas horarias y los días que elijas (por ejemplo, para poder despertarte e irte a la cama con tranquilidad), así como decidir qué se bloquea o se permite (por ejemplo, las llamadas de determinados contactos o las llamadas repetidas). Para ello, ve a los Ajustes de tu teléfono y luego al menú No molestar.",
              title: "Utilizar el modo \"No molestar\"",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss16.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss17.png")
                },
              ],
            },
            {
              abstract: "Otra forma de mantener el control es limitar el uso de tu dispositivo o de ciertas aplicaciones a un tiempo determinado. Una vez transcurrido el tiempo, la aplicación en cuestión se desactivará durante el resto del día.\n" +
              "Obviamente, este indicador http://www.internetactu.net/2019/01/15/retro-design-de-lattention-depasser-le-temps/. Tú decides lo que crees que es útil e importante. Para configurar estos ajustes, ve a los Ajustes de tu teléfono, luego al menú de Bienestar Digital y Control Parental y pulsa Tablero en la sección Para cerrar sesión.\n \n"+
              "En este menú puedes medir el tiempo que pasas en tu dispositivo y el tiempo que pasas usando cada aplicación. Aquí también se puede configurar el modo descanso, que convierte la pantalla en blanco y negro para que llame menos la atención. También en este menú, puedes configurar el modo Libre de Distracciones, que es similar a la configuración de No Molestar que vimos anteriormente.",
              title: "Limitar el tiempo de uso",
              links: [
                {
                  text: "mide el tiempo de uso de una aplicación, pero no su utilidad",
                  link: "http://www.internetactu.net/2019/01/15/retro-design-de-lattention-depasser-le-temps/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss18.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss19.png")
                },
              ],
            },
            {
              abstract: "Por supuesto, se nos ocurren muchas otras soluciones: alejar las aplicaciones menos útiles de la pantalla de inicio, elegir un paquete con una conexión limitada, obligar a hacer descansos, poner un fondo de pantalla que te incite a preguntarte si realmente quieres usar tu dispositivo, configurar la pantalla en blanco y negro... Algunas de ellas funcionarán en algunas personas, pero no en otras.\n \n" +
              "El sitio web https://datadetoxkit.org/es/wellbeing/essentials/#step-1 ofrece muchas otras soluciones. Y si estos dispositivos están ocupando realmente más espacio en tu vida del que deseas, no dudes en hablarlo con tu familia o con un adictólogo.",
              title: "Encontrar una solución a medida",
              links: [
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/es/wellbeing/essentials/#step-1"
                },
              ],
            }
          ],
          tweet: "Para captar cada vez más nuestra atención y, por tanto, nuestros datos, los gigantes de Internet explotan incluso el funcionamiento de nuestro subconsciente y nuestro cerebro. Esto es lo que llamamos \"sesgo cognitivo y psicológico\". ¡Veamos cómo protegernos!",
          title: "Reconocer y esquivar los mecanismos que nos manipulan  ",
          //pdf: require("assets/pdf/Amin_2.pdf"),
          tooltitle: "Manipulacion",
        },
        {
          subskills: [
            {
              title: "Navegar por la web con Tor Browser",
              abstract: "Para protegerse de los bichos y preservar tu privacidad, la solución más sencilla y eficaz es utilizar https://www.torproject.org/es/download/#android .Este navegador basado en Firefox es gratuito y de código abierto, y está diseñado específicamente para evitar la vigilancia y la censura.\n" +

              "Sin embargo, tenga cuidado: aunque sea muy eficiente, el Navegador Tor no le exime de seguir navegando en HTTPS o de actualizarse regularmente. Además, los sitios web que visitas pueden reconocerte en determinadas situaciones, por ejemplo, si decides identificarte con una cuenta o un correo electrónico.",
              links: [
                {
                  text: "Tor Browser",
                  link: "https://www.torproject.org/es/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Navegar por la web con el navegador Duckduckgo",
              abstract: "Si tu conexión no es lo suficientemente rápida o algunos de los sitios que quieres visitar bloquean Tor Browser, la solución alternativa que recomiendo es utilizar el navegador https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (también disponible en la tienda de aplicaciones de https://f-droid.org/es/packages/com.duckduckgo.mobile.android/). Se trata de un navegador gratuito que bloquea los rastreadores y te indica el grado de privacidad de los sitios que visitas. Sin embargo, ten en cuenta que no hará que tu conexión sea completamente anónima (los sitios conocerán tu dirección IP y dónde te encuentras).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/com.duckduckgo.mobile.android/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "Cuando navegamos por la web, los rastreadores nos siguen de un sitio a otro (https://myshadow.org/es/browser-tracking). Lo hacen para deducir nuestros gustos, intereses y un montón de cosas más sobre nosotros. ¡Veamos cómo protegernos!",
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Rastradores",
          title: "Protegerse de los rasteadores que nos siguen ",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "John" },
  { key: "Judith" },
  { key: "Bill" },
  {
    key: "Nikki",
    enabled: true,
    selected: true,
    about: "Eliott Anderson. 123 años. Divorciado. 47 niños. Entrenador de elefantes. Color favorito: plátano chocolate. Top player (Gao the Spy).",
    image: require("assets/images/community/avatars/B-Nikki.png"),
    recommendations: "Los datos recogidos no son utilizables.",
    overview: "null",
    informationShared: "null",
  },
  { key: "Bob" },
  { key: "Eugene" },
  { key: "Maria" },
  { key: "Mariane" },
  { key: "Billy" },
  { key: "Devin" },
  { key: "Dan" },
  {
    key: "Rokaya",
    about: "26 años. Soltera. No hay niños. Colorista / influencer. Top player (Gao the Photographer).",
    image: require("assets/images/community/avatars/B-Rokaya.png"),
    informationShared: "Cámara, almacenamiento, acelerómetro, contactos, geolocalización, Wifi, red, Bluetooth, NFC, fondo de pantalla, micrófono, calendario",
    overview: "Influencer, artística, segura de sí misma, dinámica, popular.",
    recommendations: "Recomendaciones publicitarias (venta al cliente #69755 - anunciante): usuario muy popular en las redes sociales. Contrato firmado para la venta de espacios publicitarios patrocinados en sus publicaciones (según las condiciones generales de uso).\n" +
      "Nota interna 1: Atención, la usuaria se puso en contacto con el departamento de publicidad para quejarse de la asociación con uno de nuestros anunciantes de productos. Explica que esta marca está vinculada a un escándalo de \"trabajo infantil esclavo\", y no desea apoyar su actividad. A la espera de respuesta.  \n" +
      " Nota interna 2 (respuesta del manager): ignorar la petición de la usuaria, el contrato nos permite hacer todo tipo de asociaciones publicitarias con sus fotos.",
    thought: {
      title: 11,
      tweet: "¿Herramientas a nuestro servicio?",
      overview: "¿Herramientas a nuestro servicio? ¿De verdad?",
      message: "Utilizar los servicios de los \"Gigantes de la Red\" tiene sus ventajas: son fáciles de usar, muy a menudo gratuitos y populares. Sin embargo, estos servicios responden ante todo a los intereses de estas empresas: las necesidades de los usuarios son secundarias.\n" +
      "Este es obviamente el caso de los problemas de vigilancia, pero no sólo. El uso de estos servicios nos hace depender de las decisiones de las personas que los diseñan. Estas empresas pueden eliminar nuestras cuentas o algunas de nuestras publicaciones, limitar el acceso, añadir o eliminar funcionalidades, no mostrarnos ciertas publicaciones, sólo destacar ciertas opiniones o incluso vender nuestra actividad en linea a otra empresa, o detenerlo por completo.  Son poderes casi ilimitados sabiendo también que mucha gente utiliza estos servicios.\n" +
      "Afortunadamente, hay alternativas que funcionan igual de bien y nos devuelven la capacidad de decidir. La asociación Framasoft, por ejemplo, ofrece muchas herramientas alternativas a las de los gigantes de Internet en el sitio web https://degooglisons-internet.org/es/.",
      links: [
        {
          text: "degooglisonsinternet.org",
          link: "https://degooglisons-internet.org/es/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Instala otra tienda",
              abstract: "Para evitar que Google, que posee el Google Play Store, use tus datos y para que puedas empezar a recuperar el control de tus datos, puedes usar tiendas más éticas, gratuitas y de código abierto. Por ejemplo el https://f-droid.org/es/packages/com.aurora.store/. Permite descargar exactamente las mismas aplicaciones que en el Google Play Store, pero sin necesidad de tener una cuenta de Google y sin recoger tus datos personales.\n \n" +
                "Pero sobretodo te recomiendo que utilices https://f-droid.org/es/. que es otra tienda que sólo te ofrece aplicaciones gratuitas y de código abierto que son más respetuosas con tus datos. Por supuesto, no encontrarás allí todas las aplicaciones de las otras tiendas. Pero apuesto a que en F-Droid encontrarás alternativas a las app que quieres",
              links: [
                {
                  text: "Aurora Store",
                  link: "https://f-droid.org/es/packages/com.aurora.store/"
                },
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss20.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss21.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss22.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss23.png")
                },
              ],
            },
            {
              title: "¿Cómo se instalan?",
              abstract: "Para utilizar estas dos tiendas, es necesario dar la autorización de instalación de aplicaciones de fuentes desconocidas. Para ello, dirígete a los ajustes de tu teléfono y, a continuación, al menú Seguridad o Dispositivo y privacidad (según tu versión de Android).",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss24.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss25.png")
                },
              ],
            },
            {
              title: "¿Y descargar las aplicaciones desde páginas?",
              abstract: "Te desaconsejo totalmente instalar aplicaciones sin pasar por una tienda de applicaciones (con un archivo .apk). Esta práctica es peligrosa y corres el riesgo de instalar aplicaciones modificadas por personas malintencionadas.",
            }
          ],
          //pdf: require("assets/pdf/Rokaya_1.pdf"),
          tooltitle: "Aplicaciones",
          tweet: "Muchas personas utilizan el Google Play Store para elegir e instalar sus aplicaciones. Pero esta no es la única solución que existe.",
          title: "Cambiar el almacén de aplicaciones",
        },
        {
          subskills: [
            {
              title: "Consulta los directorios",
              abstract: "Para ayudarnos a ubicarnos en medio de todas las soluciones existentes, sitios como https://prism-break.org/fr/categories/android/ y https://degooglisons-internet.org/es/ presentan listas de servicios y software éticos. Hojeando estas páginas, tenemos propuestas concretas para sustituir las aplicaciones más dañinas.",
              links: [
                {
                  text: "PRISM-Break.org",
                  link: "https://prism-break.org/es/"
                },
                {
                  text: "Desgooglicemos Internet",
                  link: "https://degooglisons-internet.org/es/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss26.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss27.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss28.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss29.png")
                },
              ],
            },
            {
              title: "Encuentra un host",
              abstract:"También te invito a echarle un vistazo al Collectif des hébergeurs alternatifs transparents ouverts neutres et solidaires, los https://entraide.chatons.org/fr/. que reúne estructuras que ofrecen servicios en línea éticos y que cumplen con criterios exigentes. Con un poco de suerte, podrá encontrar uno https://chatons.org/fr/find-by-localisation !",
              links: [
                {
                  text: "CHATONS [en francés]",
                  link: "https://entraide.chatons.org/fr/"
                },
                {
                  text: "cerca a ti",
                  link: "https://chatons.org/fr/find-by-localisation"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss30.png")
                },
              ],
            },
            {
              title: "Conoce los criterios indispensables",
              abstract:"Estos son los 3 criterios importantes para que un servicio en línea sea seguro y te proteja :\n" +
              "• Libre: es decir, cualquiera pueda estudiar su funcionamiento, modificarlo y compartir el resultado. De esta manera, podemos asegurarnos que el servicio hace lo que dice hacer, y nada más. Para saber más sobre el movimiento de software libre, te invito a consultar los trabajos de la https://www.april.org/fr/articles/intro.\n" +
              "• Cifrado de extremo a extremo: este método de protección de los datos implica que sólo pueden hacerlo aquellos a quienes están dirigidos. Y nadie más. Ni siquiera las personas que administran el servicio en cuestión o las empresas.\n" +
              "• Descentralizado: esto significa que la herramienta digital no debe depender de una única autoridad, que podría imponer sus decisiones a todo el mundo de la noche a la mañana. Por tanto, que sea un bien común y que nadie puede apropiárselo.\n \n" +
              "Para ir más allá, te recomiendo que lea la guía https://ssd.eff.org/es/module/eligiendo-sus-herramientas propuesta por el sitio Surveillance Self-Defense.",
              links: [
                {
                  text: "asociación de April [en francés]",
                  link: "https://www.april.org/fr/articles/intro"
                },
                {
                  text: "Elegir sus herramientas",
                  link: "https://ssd.eff.org/es/module/eligiendo-sus-herramientas"
                },
              ]
            }
          ],
          //pdf: require("assets/pdf/Rokaya_2.pdf"),
          tooltitle: "Reemplazar",
          tweet: "Encontrar una herramienta digital que realmente se adapte a tus necesidades, deseos y valores puede ser complicado. ¡Veamos cómo hacerlo en la práctica!",
          title: "Buscar un servicio en linea que sea ético",
        },
        {
          subskills: [
            {
              title: "Comprender de qué se trata",
              abstract: "Aunque las aceptamos mecánicamente, estas casillas que marcamos antes de acceder a un servicio pueden tener consecuencias importantes, sobre todo para la protección de nuestros datos y la propiedad de los contenidos que producimos (nuestros comentarios, publicaciones, vídeos, etc.).\n" +
                "Lastimosamente, los términos y condiciones de la mayoría de los servicios en línea son simplemente https://www.vice.com/fr/article/xwbg7j/la-plupart-des-termes-dutilisation-en-ligne-sont-incomprehensibles y a veces incluso ilegales: https://www.numerama.com/tech/675927-pourquoi-google-affiche-jugement-du-tgi-de-paris-sur-sa-page-daccueil.html, https://www.nextinpact.com/article/29309/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement, https://www.lemonde.fr/pixels/article/2020/06/15/apple-condamne-en-france-pour-les-conditions-abusives-d-itunes-et-apple-music_6042948_4408996.html y muchos otros ya han sido condenados por ello. Además, estas empresas suelen permitirse la posibilidad de cambiar estos contenidos cuando lo deseen.",
              links: [
                {
                  text: "incomprensibles [en francés]",
                  link: "https://www.vice.com/fr/article/xwbg7j/la-plupart-des-termes-dutilisation-en-ligne-sont-incomprehensibles"
                },
                {
                  text: "Google [en francés]",
                  link: "https://www.numerama.com/tech/675927-pourquoi-google-affiche-jugement-du-tgi-de-paris-sur-sa-page-daccueil.html"
                },
                {
                  text: "Facebook [en francés] ",
                  link: "https://www.nextinpact.com/article/29309/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement"
                },
                {
                  text: "Apple [en francés]",
                  link: "https://www.lemonde.fr/pixels/article/2020/06/15/apple-condamne-en-france-pour-les-conditions-abusives-d-itunes-et-apple-music_6042948_4408996.html"
                },
              ],
            },
            {
              title: "Encontrar la información esencial",
              abstract: "Afortunadamente, algunos servicios en línea son más éticos que los de los gigantes de Internet. Los miembros del proyecto https://tosdr.org/es_MX/frontpage (ToS;DR), se han tomado la molestia de resumir y explicar estos contratos de forma comprensible.\n" +

                "La herramienta aun no traducida al español  https://disinfo.quaidorsay.fr/fr/cgus te permite seguir y encontrar los cambios en las condiciones generales de los principales proveedores de servicios en línea. Es útil para comparar las diferentes versiones a lo largo del tiempo.",
              links: [
                {
                  text: "Terms of Service; Didn't Read",
                  link: "https://tosdr.org/es_MX/frontpage"
                },
                {
                  text: "Open Terms Archive [en francés]",
                  link: "https://disinfo.quaidorsay.fr/fr/cgus"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss31.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss32.png")
                },
              ],
            }
          ],
          tweet: "Para utilizar un servicio en línea, generalmente hay que aceptar sus condiciones de uso (o TyC), ¡incluso en los casos en los que no puede rechazarlas de todos modos! Veamos cómo navegar por estos complicados documentos.",
          //pdf: require("assets/pdf/Rokaya_3.pdf"),
          tooltitle: "Contratos",
          title: "Entender los contratos de los servicios que usamos",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Dominic" },
  { key: "Jackson" },
  { key: "Sebastian" },
  { key: "Egaeus nobot" },
  { key: "James" },
  {
    key: "Masako",
    about: "Entre 35 y 42 años. Casada (usuario cónyuge#8765489, nombre de usuario \"Amin\"). 1 niño pequeño (\"Alma\"). Ingeniera de materiales. Top player (Gao the Forger).",
    image: require("assets/images/community/avatars/B-Masako.png"),
    informationShared: "Fondo de pantalla, Almacenamiento, SMS, Acelerómetro, Micrófono, Calendario, Cámara, Contactos, Geolocalización, Wifi, Red",
    overview: "Enérgica, aventurera, independiente, creativa, perseverante, niña enferma (enfermedad degenerativa), comprometida.",
    recommendations: "Datos de empleabilidad recogidos (venta al cliente #35468 - red social y reclutador profesional): cambio de dirección reciente. El análisis de los intercambios de mensajes y de las búsquedas en línea indican una disminución de la motivación profesional ( cambio a un trabajo a tiempo parcial, disminución de las conversaciones orientadas business) y un aumento de las preocupaciones familiares ( visualización significativa de las fotos de los niños, tensiones de pareja). Indicar a los encargados de la selección de personal de RRHH un descenso del 27% en la puntuación de empleabilidad.",
    thought: {
      title: 10,
      tweet: "¿Datos colectivos?",
      overview: "¿Datos personales o datos colectivos?",
      message: "Aunque generalmente se denominan ”datos personales”, gran parte de lo que los \"Gigantes de la Red\" explotan comercialmente son datos colectivos. A esto se le conoce como gráficos sociales o redes sociales y son las conexiones y relaciones entre las personas.\n" +
      "Cuando estas empresas logran recopilar información sobre las opiniones y los gustos de alguien, logran de paso deducir las del entorno de esta persona. Tener comunicaciones confidenciales depende de todas las personas que participan en la discusión. Basta con que un solo destinatario de nuestros mensajes se preocupe poco por la privacidad que los demás para que incluso con las herramientas más protectoras, los esfuerzos sean menos eficaces. Cuidar nuestros datos personales es también cuidar los de nuestros seres queridos. Por ejemplo, podemos preguntarnos si las personas que salen en nuestras fotografías están realmente de acuerdo en que se pongan en Internet o se compartan estas imágenes.  Y no se trata sólo de humanos: por ejemplo, los cazadores https://qz.com/206069/geotagged-safari-photos-could-lead-poachers-right-to-endangered-rhinos/ [en] utilizan los metadatos de las imágenes publicadas en Internet por los turistas para localizar y cazar animales salvajes o en vía de extinción.\n" +
      "Para saber más sobre la dimensión colectiva de los datos \"personales\", puede leer este https://scinfolex.com/2018/05/25/rgpd-la-protection-de-nos-vies-numeriques-est-un-enjeu-collectif-tribune-le-monde/.",
      links: [
        {
          text: "utilisen",
          link: "https://qz.com/206069/geotagged-safari-photos-could-lead-poachers-right-to-endangered-rhinos/"
        },
        {
          text: "artículo",
          link: "https://scinfolex.com/2018/05/25/rgpd-la-protection-de-nos-vies-numeriques-est-un-enjeu-collectif-tribune-le-monde/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Elije una contraseña",
              abstract: "Para proteger tus datos, es importante tener un código de desbloqueo : puedes decidir utilizar un patrón, números o una contraseña, est una de las medidas más importantes y sencillas ! Después de cada cierre de pantalla, será necesario introducir este código para acceder a tu teléfono.\n \n" +
                "Para configurarlo o cambiarlo, dirígete a los Ajustes de tu teléfono y, a continuación, al menú Huellas dactilares, cara y contraseña. Para decidir cuánto tiempo debe pasar antes de que la pantalla se apague automáticamente, ve al menú Pantalla y brillo.\n \n" +
                "Para mayor seguridad, te recomiendo que evites las contraseñas fáciles, como los métodos biométricos (cara o huellas dactilares) o los dibujos o códigos demasiado fáciles de adivinar (como la forma de la primera letra de tu nombre o tu fecha de nacimiento). Para saber más, he aquí las https://www.ssi.gouv.fr/guide/mot-de-passe/ e la Agencia Nacional de Seguridad de los Sistemas de Información (ANSSI) para elegir contraseñas seguras y la https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut para utilizar un gestor de contraseñas que ofrece el sitio web NextINpact.com.",
              links: [
                {
                  text: "recomendaciones",
                  link: "https://www.ssi.gouv.fr/guide/mot-de-passe/"
                },
                {
                  text: "guía",
                  link: "https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut"
                },
              ],
            },
            {
              title:"Copia tu código IMEI",
              abstract:"El código IMEI es un número que identifica su teléfono de forma exclusiva. Puedes conseguirlo marcando *#06#. También lo encontrarás en la caja y la factura de tu teléfono. Guárdalo bien : lo necesitarás para bloquear tu teléfono permanentemente en caso de robo.",
            },
            {
              title:"Mantén tu teléfono actualizado",
              abstract:"Instala las actualizaciones siempre y tan pronto como sea posible. Además de nuevas funciones, suelen incluir correcciones de errores.\n \n" +
              "Para quitarte la molestia, ve a los Ajustes de tu tienda de aplicaciones, y luego al menú de Actualizaciones si usas el Play Store. Aquí, pulsa sobre Activar actualizaciones automáticas u Obtener actualizaciones automáticamente.",
            },
            {
              title:"Cifra tu teléfono",
              abstract:"La mayoría de los teléfonos ofrecen encriptación de los datos que contienen, es decir, sólo la persona que conoce la contraseña de desbloqueo puede acceder.\n \n" +
                "Dependiendo de tu versión de Android, el proceso y el método pueden variar. En caso de que la opción no esté ya activada por defecto y si está disponible, puedes cifrar la memoria de tu teléfono o tu tarjeta micro SD. Para ello, dirígete a los Ajustes de tu teléfono y luego al menú de Seguridad.\n \n" +
                "Sin embargo, ¡ten cuidado! En el caso de una tarjeta SD, la operación comienza borrando todos los datos, así que asegúrate de tener una copia en otro lugar. Y en cualquier caso, si pierdes la contraseña, tus datos también se perderán realmente.",
            },
            {
              title:"No olvides los metadatos",
              abstract:"Cuando tomamos fotos, nuestro teléfono móvil añade automáticamente informaciones, como la fecha, la geolocalización o el ajuste de la cámara. Esto se llama metadatos. Esta información se utiliza para clasificar nuestras fotos, pero puede revelar mucha más información que la propia imagen. Recuerda comprobarlos antes de compartir tus fotos, o eliminarlos por completo. Para ello, puedes utilizar la aplicación Scrambled Exif (que encuentras en https://f-droid.org/es/packages/com.jarsilio.android.scrambledeggsif/ o en el Google https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif).",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/com.jarsilio.android.scrambledeggsif/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif"
                },
              ],
            },
            {
              title:"Desactiva funciones innecesarias",
              abstract:"Cuando no la uses, desactiva la geolocalización, el wifi y el Bluetooth. Al hacer esto, limitarás los datos que se recojen sobe ti, mejorarás tu seguridad y la duración de la batería. Para ello, coloca el dedo en la barra de estado, deslízalo hacia abajo y pulsa en los íconos de cada función.",
            },
            {
              title:"Dale prioridad a las páginas web en vez de las app",
              abstract:"En general, es mejor utilizar la página web que su aplicación dedicada: limitarás los datos a los que puede acceder en tu teléfono.",
            },
            {
              title:"Reinicia tus dispositivos nuevos y viejos",
              abstract:"Si recibes un teléfono usado o te dejas de usar el tuyo, no olvides borrar todos los datos que contiene. Esta breve https://www.cnil.fr/fr/effacer-ses-donnees-dun-ordinateur-dun-telephone-ou-dune-tablette-avant-de-sen-separer de la Comisión Nacional de Informática y Libertades (CNIL) explica cómo hacerlo.",
              links: [
                {
                  text: "guía [en francés]",
                  link: "https://www.cnil.fr/fr/effacer-ses-donnees-dun-ordinateur-dun-telephone-ou-dune-tablette-avant-de-sen-separer"
                },
              ],
            },
            {
              title:"Haz balance cada tanto",
              abstract:"Ninguna solución informática es 100% segura ni durará para siempre. Recuerda estar pendiente de vez en cuando si las elecciones que ha hecho siguen siendo pertinentes y si todavía te convienen.",
            },
            {
              title:"No olvides los detalles",
              abstract:"Cuando se trata de seguridad informática, un sistema es igual de fuerte que su eslabón más débil: osea basta con un pequeño detalle para que todo se vuelva frágil. Por lo tanto, presta atención a lo parecieran detalles.\n \n" +
              "Por ejemplo, te recomiendo que cambies el teclado que viene por defecto (Gboard) en tu teléfono por un teclado libre. Los que te recomiendo son OpenBoard o Android AOSP, que puedes instalar desde https://f-droid.org/es/packages/org.dslul.openboard.inputmethod.latin/ o https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin. Para usarlo, ve a los Ajustes de tu teléfono, luego a Otros ajustes, luego a Teclado y método de entrada para elegir OpenBoard o AOSP y configúralo.",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/org.dslul.openboard.inputmethod.latin/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin"
                },
              ],
            },
            {
              title:"Adoptar una actitud respetuosa",
              abstract:"Más allá de todas estas recomendaciones, proteger tus datos y los de tus seres queridos también implica adoptar actitudes respetuosas con los demás. Por ejemplo, no olvides preguntar a la persona si puedes compartir información sobre ella, no guardes todo por costumbre y minimiza la cantidad de datos que generas. Ayúda a personas que conocen menos del tema, discute y comparte tus descubrimientos...\n \n" +
              "Y para llegar a soluciones realmente satisfactorias y no dejar a nadie rezagado, es necesario organizarse colectivamente para mejorar las leyes y las herramientas digitales que utilizamos para protegernos. Si te interesa esta aventura o simplemente quiere estar al día de las últimas noticias sobre el tema, te aconsejo que visites la https://laquadrature.net/ . Para ir más allá, encontrarás muchas otras recomendaciones en el sitio de https://datadetoxkit.org/es/home y en la completísima https://guide.boum.org/.",
              links: [
                {
                  text: "página web de La Quadrature du Net.",
                  link: "https://laquadrature.net/"
                },
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/es/home/"
                },
                {
                  text: "guía de autodefensa digital",
                  link: "https://ssd.eff.org/es"
                },
              ],
            }
          ],
          tweet: "Además de todas las habilidades que te he presentado, hay algunas cosas sencillas que podemos hacer para que nuestros teléfonos sean más seguros de forma rápida y sencilla. Algunos incluso las llaman \"reglas de higiene digital\".",
          //pdf: require("assets/pdf/Ajay_3.pdf"),
          tooltitle: "Proteger",
          title: "Proteger nuestros datos y los de nuestros seres queridos",
        },
        {
          subskills: [
            {
              title: "Consulta los directorios ",
              abstract: "Para ayudarnos a ubicarnos en medio de todas las soluciones existentes, sitios como https://prism-break.org/fr/categories/android/ y https://degooglisons-internet.org/fr/ presentan listas de servicios y software éticos. Hojeando estas páginas, tenemos propuestas concretas para sustituir las aplicaciones más dañinas.",
              links: [
                {
                  text: "PRISM-Break.org",
                  link: "https://prism-break.org/fr/categories/android/"
                },
                {
                  text: "Desgooglicemos Internet",
                  link: "https://degooglisons-internet.org/es/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss26.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss27.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss28.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss29.png")
                },
              ],
            },
            {
              title: "Encuentra un host",
              abstract:"También te invito a echarle un vistazo al Collectif des hébergeurs alternatifs transparents ouverts neutres et solidaires, los https://entraide.chatons.org/fr. que reúne estructuras que ofrecen servicios en línea éticos y que cumplen con criterios exigentes. Con un poco de suerte, podrá encontrar uno https://chatons.org/fr/find-by-localisation !",
              links: [
                {
                  text: "CHATONS [en francés]",
                  link: "https://entraide.chatons.org/fr"
                },
                {
                  text: "cerca a ti",
                  link: "https://chatons.org/fr/find-by-localisation"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss30.png")
                },
              ],
            },
            {
              title: "Conoce los criterios indispensables",
              abstract:"Estos son los 3 criterios importantes para que un servicio en línea sea seguro y te proteja :\n" +
              "• Libre: es decir, cualquiera pueda estudiar su funcionamiento, modificarlo y compartir el resultado. De esta manera, podemos asegurarnos que el servicio hace lo que dice hacer, y nada más. Para saber más sobre el movimiento de software libre, te invito a consultar los trabajos de la https://www.april.org/fr/articles/intro.\n" +
              "• Cifrado de extremo a extremo: este método de protección de los datos implica que sólo pueden hacerlo aquellos a quienes están dirigidos. Y nadie más. Ni siquiera las personas que administran el servicio en cuestión o las empresas.\n" +
              "• Descentralizado: esto significa que la herramienta digital no debe depender de una única autoridad, que podría imponer sus decisiones a todo el mundo de la noche a la mañana. Por tanto, que sea un bien común y que nadie puede apropiárselo.\n \n" +
              "Para ir más allá, te recomiendo que lea la guía https://ssd.eff.org/es/module/eligiendo-sus-herramientas propuesta por el sitio Surveillance Self-Defense.",
              links: [
                {
                  text: "asociación de April [en francés]",
                  link: "https://www.april.org/fr/articles/intro"
                },
                {
                  text: "Elegir sus herramientas",
                  link: "https://ssd.eff.org/es/module/eligiendo-sus-herramientas"
                },
              ]
            }
          ],
          //pdf: require("assets/pdf/Rokaya_2.pdf"),
          tooltitle: "Reemplazar",
          tweet: "Encontrar una herramienta digital que realmente se adapte a tus necesidades, deseos y valores puede ser complicado. ¡Veamos cómo hacerlo en la práctica!",
          title: "Buscar un servicio en linea que sea ético",
        },
        {
          subskills: [
            {
              title: "Navegar por la web con Tor Browser",
              abstract: "Para protegerse de los bichos y preservar tu privacidad, la solución más sencilla y eficaz es utilizar https://www.torproject.org/es/download/#android. Este navegador basado en Firefox es gratuito y de código abierto, y está diseñado específicamente para evitar la vigilancia y la censura.\n \n" +
                "Sin embargo, tenga cuidado: aunque sea muy eficiente, el Navegador Tor no le exime de seguir navegando en HTTPS o de actualizarse regularmente. Además, los sitios web que visitas pueden reconocerte en determinadas situaciones, por ejemplo, si decides identificarte con una cuenta o un correo electrónico.",
              links: [
                {
                  text: "Tor Browser",
                  link: "https://www.torproject.org/es/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Navegar por la web con el navegador Duckduckgo",
              abstract: "Si tu conexión no es lo suficientemente rápida o algunos de los sitios que quieres visitar bloquean Tor Browser, la solución alternativa que recomiendo es utilizar el navegador https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (también disponible en la tienda de aplicaciones de https://f-droid.org/es/packages/com.duckduckgo.mobile.android/). Se trata de un navegador gratuito que bloquea los rastreadores y te indica el grado de privacidad de los sitios que visitas. Sin embargo, ten en cuenta que no hará que tu conexión sea completamente anónima (los sitios conocerán tu dirección IP y dónde te encuentras).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/es/packages/com.duckduckgo.mobile.android/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "Cuando navegamos por la web, los rastreadores nos siguen de un sitio a otro (https://myshadow.org/es/browser-tracking). Lo hacen para deducir nuestros gustos, intereses y un montón de cosas más sobre nosotros. ¡Veamos cómo protegernos!",
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Rastradores",
          title: "Protegerse de los rasteadores que nos siguen ",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Joel" },
  { key: "Jillian" },
  { key: "Alvaro" },
  { key: "Atom nobot" },
  {
    key: "Lucas",
    about: "50 años. En relación (cónyuge usuario#8764563, nombre de usuario \"Lola\"). No hay niños. Inspector de policía. Top player (Gao the Prosecutor).",
    image: require("assets/images/community/avatars/B-Lucas.png"),
    informationShared: "Registro de llamadas, contactos, geolocalización, SMS, Wifi, red, almacenamiento",
    overview: "Curioso, juguetón, reservado, desconfiado, cariñoso, obstinado.",
    recommendations: "Datos de redes sociales recogidos (venta de clientes #XXXXX - estudio multimedia y de videojuegos): mantiene una relación sentimental desde hace 6 meses con el usuario#8764563 (nombre de usuario \"Lola\"),abogada.\n" +
      "Nota interna 1: Dígale al departamento de facturación que mantenga oculta la identificación del cliente con respecto a la transacción de datos.  \n" +
      " Nota interna 2 (confidencial): el cliente #87654, investigado judicialmente por el equipo del usuario \"Lola\" muy interesada en datos que pudieran probar una relación con el usuario \"Lucas\" (registros de llamadas, contenido de SMS y correos electrónicos, geolocalización completa). Para mantener las buenas relaciones comerciales y en el contexto de la generosa oferta de compra de Gao Games por parte del cliente, la transacción debe ser confidencial.  \n" +
      " Nota interna 3 (confidencial): la usuaria \"Lola\" parece sospechar que sus comunicaciones están siendo vigiladas, y ha cortado el contacto durante 72 horas. Información transmitida al cliente #87654.",
    thought: {
      title: 9,
      tweet: "No sólo los individuos",
      overview: "Las empresas y las administraciones también son victimas de monitoreo",
      message: "Las herramientas de vigilancia masiva facilitan el espionaje industrial o económico.  Los motivos pueden ser competir con otras empresas, robar sus descubrimientos o datos internos, pedirles un rescate, o amenazarlas con  para sabotearlas o a sus clientes. Muchos actores tienen interés en obtener informaciones confidenciales de las empresas !\n" +
      "Este tema es muy importante también para las administraciones y los servicios del estado. Casi siempre, los funcionarios tienen acceso legítimo a información confidencial y sin saberlo, pueden permitir que personas malintencionadas intercepten esta información. Esta situación se agrava a medida que estas organizaciones tele trabajan cada vez más y abren el acceso a sus herramientas digitales desde fuera de sus instalaciones.\n" +
      "La escala masiva de estas prácticas, así como la implicación de los servicios de inteligencia estatales en estas actividades, han sido demostradas por los documentos revelados por Edward Snowden en 2013 y más recientemente, en 2021 con el descubrimiento del uso del software Pegasus, que algunos gobiernos usaron para vigilar a terceros (desde responsables políticos, jefes de Estado, ciudadanos, periodistas) a través de sus teléfonos móviles.\n" +
      "Así, protegernos de la vigilancia nos permite proteger los datos de las estructuras en las que trabajamos y evitar situaciones a veces catastróficas. En el sitio web https://www.cybermalveillance.gouv.fr/ encontrará muchos consejos para limitar estos riesgos",
      links: [
        {
          text: "cybermalveillance.gouv.fr",
          link: "https://www.cybermalveillance.gouv.fr/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Instalar Signal",
              abstract: "Cifrar tus comunicaciones significa que, gracias a un proceso matemático, tus mensajes serán ilegibles (\"indescifrables\") salvo para las personas a las que realmente van dirigidos. Sin que seamos necesariamente conscientes de ello, este proceso es aplicado por muchos servicios que utilizamos en nuestra vida cotidiana, por ejemplo para la navegación por Internet o los pagos en línea.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss33.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss34.png")
                },
              ],
            },
            {
              title: "¿Por qué esta aplicación?",
              abstract: "Muchas aplicaciones de comunicación dicen ofrecer esta seguridad, pero... no todas son fiables. Sobre todo porque su modelo de negocio es usar y vender nuestros datos y nuestros metadatos. Además, cuando su código no es libre (como Whatsapp o Telegram), son aún menos fiables, porque nadie puede saber cómo funcionan realmente.\n \n" +
              "Por supuesto, para que tus intercambios estén protegidos, tus contactos también tendrán que utilizar esta aplicación. Para saber más, aquí tienes una https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-signal-en-android que detalla todos los pasos, desde la instalación hasta el uso de la aplicación.\n \n"+
              "Y para ir más allá y saberlo todo sobre el cifrado, te recomiendo que leas este https://www.nextinpact.com/news/99777-chiffrement-notre-antiseche-pour-expliquer-a-vos-parents.htm de NextINpact.com.",
              links: [
                {
                  text: "guía práctica",
                  link: "https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-signal-en-android"
                },
                {
                  text: "artículo [en francés] ",
                  link: "https://www.nextinpact.com/news/99777-chiffrement-notre-antiseche-pour-expliquer-a-vos-parents.htm"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Lucas_1.pdf"),
          tooltitle: "Proteger",
          tweet: "Para de mejorar de manera sencilla la confidencialidad de tus comunicaciones puedes elegir aplicaciones libres que ofrezcan encriptación. Hoy en día la más fácil de usar es Signal (https://signal.org/). Aunque no sea perfecta, es el mejor compromiso entre seguridad y facilidad de uso.",
          title: "Proteger tus comuniciones",
        },
        {
          subskills: [
            {
              title: "Haz un balance de tu situación",
              abstract: "Empecemos por identificar los datos que quieres proteger, de qué, de quién, dependiendo de tu situación. Para ello, identifica qué datos son importantes o sensibles para ti y piensa en los posibles peligros si los pierdieras o si cualquier otra persona tuviera acceso a ellos. Esto es lo que se llama en seguridad informática \"establecer un modelo de amenaza\", pero nosotros lo llamaremos modelo de protección.\n \n" +
                "El siguiente paso es identificar, para cada dato, la gravedad y probabilidad de su pérdida o difusión. De hecho, cada riesgo tiene una probabilidad de ocurrir y una gravedad diferente. Por ejemplo, un riesgo muy, muy, muy improbable sería que los marcianos utilizaran fotos íntimas tuyas para decorar sus naves espaciales. Pero un riesgo muy probable sería que Google utilizara esas mismas fotos para conocerte mejor y venderte productos relacionados.\n \n" +
                "Al estimar estos dos criterios, la gravedad y la probabilidad, resulta fácil separar los riesgos urgentes y por los que no hay que preocuparse tanto. Puedes utilizar https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ES.cleaned.pdf para pensar en tu propio modelo de amenaza. ",
                links: [
                {
                  text: "este modelo",
                  link: "https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ES.cleaned.pdf"
                   },
              ],
            },
            {
              title: "Decidir sobre las acciones a realizar",
              abstract: "Gracias a la cartografía que acabas de realizar, ahora puedes identificar los lugares que hay que proteger primero.\n \n" +
                        "Esta vez, necesitas encontrar apoyo de alguien si sientes que no te es sencillo imaginarte cómo avanzar. Cada medida puede ser más o menos difícil de aplicar, pero no te desanimes. A veces hay soluciones muy técnicas, pero otras veces no. Para cada dato que debes proteger, te invito a que te hagas estas preguntas:\n" +
                          "• ¿Mis dispositivos tienen una buena contraseña?\n" +
                          "• ¿Los dejo junto al grifo de agua y no tengo una copia de seguridad de mis datos?\n" +
                          "• ¿La solución que se me ocurre es fácil o difícil de usar? \n" +
                          "• ¿Quién puede darme consejos o ideas sobre cómo proteger tal o cual información?\n" +
                          "• ¿Estas medidas son sólo para mí o más bien a todo un grupo?\n \n" +
                        "Algunas soluciones tendrán ventajas, otras inconvenientes, otras pueden empeorar la situación y otras serán super sencillas y adecuadas. Pero recuerda ser consciente de los peligros que identificaste ya es el principio de la solución, así no pongas ninguna alguna solución en práctica.\n \n" +
                        "Por último, sólo queda elegir y poner en marcha las soluciones que identificaste y de vez en cuando ver si debes adaptar tu plan de soluciones si te encuentrs en nuevos contextos o si tienes nuevas necesidades en términos de seguridad.\n \n" +
                        "Para saber más sobre cómo elaborar un plan de seguridad, te recomiendo la lectura de esta breve https://ssd.eff.org/es/module/evaluando-tus-riesgos publicada en la web de Autodefensa de Vigilancia para completar el documento que te sugerí más arriba.",
              links: [
                {
                  text: "guía",
                  link: "https://ssd.eff.org/es/module/evaluando-tus-riesgos"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_1.pdf"),
          tooltitle: "Protegerse",
          tweet: "Las amenazas y necesidades de cada persona pueden ser muy diferentes. Veamos cómo encontrar una solución a la medida.",
          title: "¿De qué y de quién protegerse?",
        },
        {
          subskills: [
            {
              title: "Comprender el riesgo",
              abstract: "Estas estafas buscan captar la información que los visitantes engañados ponen en la página web si esta fuera real. También puede tener como objetivo engañarles para que instalen virus aplicaciones o programas nocivos o llamen a un servicio de tarificación adicional.\n" +

                "A menudo, se envían enlaces a este tipo de páginas por mail o por mensajes y chats que pretenden asustarnos haciéndonos creer que tenemos una gran suerte o una amenaza de virus en nuestros dispositivos. Por https://cyberguerre.numerama.com/10020-attention-a-ces-phishings-francais-qui-ne-disparaissent-jamais.html, estos mensajes pueden anunciar que te ganaste la lotería, que se ha retrasado en el pago de una factura o que tu teléfono móvil ha sido infectado por un virus.",
              links: [
                {
                  text: "ejemplo",
                  link: "https://cyberguerre.numerama.com/10020-attention-a-ces-phishings-francais-qui-ne-disparaissent-jamais.html"
                },
              ],
            },
            {
              title: "¡Está alerta !",
              abstract: "Para detectar estos intentos de estafa, debes prestar atención a la dirección de los sitios que el mensaje quiere que visites. Por ejemplo, https://gaogames.arnaque.fr/ no tiene nada que ver con los juegos de Gao. Puedes comprobarlo pulsando prolongadamente el enlace con el dedo antes de abrirlo o en la esquina de su navegador pasando el ratón por encima del enlace, ya que bajo la apariencia de ir a https://gaogames.fr en realidad va a https://gaogames.arnaque.fr como te mostramos a continuación",
              links: [
                {
                  text: "https://gaogames.fr",
                  link: "https://gaogames.fr/"
                },
                {
                  text: "https://gaogames.fr",
                  link: "https://gaogames.arnaque.fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss35.png")
                },
              ],
            },
            {
              title: "¿Cómo detectarlos? ",
              abstract: "En este caso, la parte más importante de la dirección es estafa.com : es el dominio al que te va a llevar realmente. Recuerda comprobar qué lugar quieres ir. También hay que tener cuidado con los errores en las direcciones (por ejemplo, Wikypedia en lugar de Wikipedia). Esto requiere mucha atención de tu parte cada vez que navegas o abre enlaces.\n \n" +
              "Estos elementos pueden darte algunas pistas: faltas de ortografía, avisos legales, mensaje super urgente para hacer algo, el aspecto general de la página...\n \n" +
              "En caso de duda, lo mejor es tomarte un tiempo para reflexionar y buscar a alguien que te aconseje. También puedes echarle un vistazo a la https://lookup.icann.org/lookup sobre el nombre de dominio. En cualquier caso, no hagas clic en ningún enlace que te parezca extraño y mejor que busques la página desde tu motor de búsqueda favorito.\n \n" +
              "Para informarte más sobre estas estafas y cómo protegerse, te invito a leer las ces https://www.cnil.fr/fr/phishing-detecter-un-message-malveillant de la Comisión Nacional de Informática y Libertades (CNIL) o este https://cyberguerre.numerama.com/2724-phishing-comment-font-les-hackers-comment-vous-proteger.html publicado en el sitio web Numerama.com.",
              links: [
                {
                  text: "información pública",
                  link: "https://lookup.icann.org/lookup"
                },
                {
                  text: "recomendaciones [en francés]",
                  link: "https://www.cnil.fr/fr/phishing-detecter-un-message-malveillant"
                },
                {
                  text: "artículo [en francés]",
                  link: "https://cyberguerre.numerama.com/2724-phishing-comment-font-les-hackers-comment-vous-proteger.html"
                },
              ],
            }
          ],
          tweet: "El phishing es una de las estafas más comunes en Internet. Consiste en hacerse pasar por una página imitando (a veces muy bien) su apariencia. Veamos cómo protegerse.",
          //pdf: require("assets/pdf/Lucas_3.pdf"),
          tooltitle: "Estafas",
          title: "Evitar las estafas en línea",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  {
    key: "Sol",
    about: "39 años. Soltera. No hay niños. Empresaria. Top player (Gao the Intruder).",
    image: require("assets/images/community/avatars/B-Sol.png"),
    informationShared: "Contactos, Geolocalización, Wifi, Red, SMS, Alarma, Micrófono, Calendario, Cámara, Acelerómetro",
    overview: "Pragmática, segura de sí misma, extrovertida, arribista, ambiciosa, amplio círculo social.",
    recommendations: "Datos de la red social recogidos (venta de clientes #15467 - banco): la libreta de direcciones y los contactos de la red del sujeto indican una alta proporción de conocidos con bajos ingresos económicos (impacto -53% en la puntuación del banco). 61% de probabilidad de no devolver un préstamo bancario (estimación basada en las redes de amigos). Considere la posibilidad de rechazar un préstamo.",
    thought: {
      title: 12,
      tweet: "¿Quién decide por nosotros?",
      overview: "Nuestros datos pueden ser usados para tomar decisiones que nos afectan",
      message: "En todo el mundo, las empresas están creando programas informáticos para tomar decisiones sobre nosotros basándose en nuestros datos personales. Puede tratarse de nuestra probabilidad de devolver un préstamo al banco, si somos la persona idónea para ocupar un cargo, si nos vamos a enfermar en los próximos años o incluso si es posible que cometamos algún delito... Estas predicciones pueden parecer interesantes, pero estas prácticas tienen muchas fallas. Los resultados a menudo resultan inexactos y son más bien el producto de los estereotipos de quienes.\n \n" +
               "Por ejemplo, las personas de piel oscura tienen más probabilidades de morir de cáncer de piel: los médicos se forman poco para conocer estas pieles y, por tanto, las tecnologías avanzadas de inteligencia artificial diseñadas para identificar cánceres reproducen este y otros sesgos, https://theconversation.com/discriminacion-racial-en-la-inteligencia-artificial-142334.\n \n" +
               "Afortunadamente, en Europa, las decisiones totalmente automatizadas están prohibidas si pueden tener consecuencias importantes para las personas afectadas. Para saber más al respecto, le recomendamos que lea este https://www.internetactu.net/2018/01/15/de-lautomatisation-des-inegalites/ ssobre cómo los sistemas de decisión automatizados refuerzan las desigualdades o esta https://www.cnil.fr/fr/profilage-et-decision-entierement-automatisee del sitio web de la Comisión Nacional de Informática y Libertades francesa(CNIL).",
      links: [
        {
          text: "como explica este artículo",
          link: "https://theconversation.com/discriminacion-racial-en-la-inteligencia-artificial-142334"
        },
        {
          text: "artículo [en francés]",
          link: "https://www.internetactu.net/2018/01/15/de-lautomatisation-des-inegalites/"
        },
        {
          text: "página [en francés]",
          link: "https://www.cnil.fr/fr/profilage-et-decision-entierement-automatisee"
        }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Controla el uso de tus datos",
              abstract: "Puedes contactar a las organizaciones privadas o públicas que recogen, utilizan o venden nuestros datos personales, y solicitarles información, correcciones, supresiones o copias de nuestros datos.\n" +
                "Esto es válido para las cuentas y servicios abiertos por residentes de la Unión europea y sus ciudadanos cuando están en otros países.\n \n" +
                "Cada país de la Unión Europea tiene su propia institución encargada de verificar que se apliquen estas medidas. https://edpb.europa.eu/about-edpb/board/members_es tienes la lista por países de la Unión Europea. Y para los demás países del mundo, te recomiendo este https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde\n" +
                "En Francia, la Comisión Nacional de Informática y Libertades (CNIL) se encarga de ayudarnos a hacer valer nuestros derechos en este ámbito y nos proporciona guías sobre nuestros https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles y https://www.cnil.fr/modeles/courrier de cartas para hacerlos valer fácilmente.\n \n" +
                "Imaginemos que buscas tu nombre en Internet y encuentras información personal que no quieres que sea pública. El primer paso para eliminar esa info es escribir a la dirección de contacto de la página en cuestión. Una vez que hayas enviado tu solicitud, los responsables del sitio deben responderte en un plazo de 30 días y muchas veces este mail es suficiente para que quiten tu información.",
              links: [
                {
                  text: "Aquí",
                  link: "https://edpb.europa.eu/about-edpb/board/members_es"
                },
                {
                  text: "mapa detallado",
                  link: "https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde"
                },
                {
                  text: "distintos derechos [en francés]",
                  link: "https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles"
                },
                {
                  text: "modelos [en francés]",
                  link: "https://www.cnil.fr/modeles/courrier"
                },
              ],
            },
            {
              title: "Poner una demanda",
              abstract:"Si no eliminan tu información al cabo de 30 días, puedes enviar https://www.cnil.fr/fr/plaintes a la CNIL o a la autoridad de tu país, que puede sancionar la página. Tristemente la CNIL y otras instituciones disponen de unos pocos recursos (y por lo tanto de personas) para atender los mensajes y pueden tardarse en responderte. Si transcurren 30 días sin dar respuesta, puedes presentar https://www.service-public.fr/particuliers/vosdroits/F1435 ante la policía o la fiscalía para hacer valer sus derechos.",
              links: [
                {
                  text: "una reclamación [en francés]",
                  link: "https://www.cnil.fr/fr/plaintes"
                },
                {
                  text: "una denuncia [en francés]",
                  link: "https://www.service-public.fr/particuliers/vosdroits/F1435"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_2.pdf"),
          tooltitle: "Derechos",
          tweet: "La Unión Europea garantiza la protección de datos de sus nacionales y residentes. Desde su entrada en vigor en 2018, el Reglamento General de Protección de Datos (RGPD) todas las personas pueden controlar el uso que se hacen de sus datos. ¡Veamos de qué se trata !",
          title: "Protegerte más allá de las fronteras",
        },
        {
          subskills: [
            {
              title: "¿De qué se trata? ",
              abstract: "Para funcionar, un teléfono móvil necesita un sistema operativo, es decir, un programa principal para que las aplicaciones y los botones funcionen cuando los oprimes. Este sistema operativo actúa como intermediario entre los botones y el aparato físico.",
            },
            {
              title: "Instalación de un sistema alternativo",
              abstract:"Afortunadamente para los teléfonos con Android, es posible sustituir el sistema operativo con https://lineageos.org/, https://e.foundation/es/ o https://www.replicant.us/, que son totalmente gratuitos y libres. Esto puede ser un proceso complejo. Todavía no todos los télefonos son compatibles con los sistemas alternativos. Pero si tu teléfono es compatible y quieres instalar otro sistama, lo primero es hacer una copia de seguridad completa de tus datos antes de empezar y seguir las instrucciones. Sobretodo, no olvides hacerlo con un teléfono totalmente cargado y en el orden indicado. De lo contrario, tu teléfono puede, en algunos casos, quedar inutilizable. Cambiar el sistema operativo es una operación delicada, pero gracias a ella es posible retomar realmente el control de su teléfono y proteger mejor tus datos personales.\n \n" +
                "Para saber más y quizás probarlo, te recomiendo que empieces por https://fsfe.org/news/2012/news-20120228-01.es.html de la Free Software Foundation Europe y esta https://linuxfr.org/news/installer-lineageos-sur-son-appareil-android publicada por LinuxFR.org. Y para resolver cualquier problema que puedas encontrar, te recomiendo el sitio web https://www.xda-developers.com/.",
              links: [
                {
                  text: "Lineage OS",
                  link: "https://lineageos.org/"
                },
                {
                  text: "/e/",
                  link: "https://e.foundation/es/"
                },
                {
                  text: "Replicant [en inglés]",
                  link: "https://www.replicant.us/"
                },
                {
                  text: "este artículo",
                  link: "https://fsfe.org/news/2012/news-20120228-01.es.html"
                },
                {
                  text: "guía práctica [en francés]",
                  link: "https://linuxfr.org/news/installer-lineageos-sur-son-appareil-android"
                },
                {
                  text: "xda-developers.com [en inglés]",
                  link: "https://www.xda-developers.com/"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Sol_2.pdf"),
          tooltitle: "Sistema alternativo",
          tweet: "Advertencia: esta habilidad es compleja, pero que no cunda el pánico, voy a explicarte bien",
          title: "Instalar un sistema alternativo en tu teléfono",
        },
        {
          subskills: [
            {
              title: "Conoce tus derechos",
              abstract: "Cerrar una cuenta en un servicio online es un derecho: desde que entró en vigor el Reglamento General de Protección de Datos (el GDPR) en 2018, podemos https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3#Article17 una organización que elimine todos los datos e información sobre nosotros. En teoría, todos los sitios web que te ofrecen crear una cuenta deberían ofrecerte también una forma de borrarla y los datos que contiene.\n \n" +
                "En la práctica, el proceso puede ser muy sencillo o muy complicado. A menudo, bastará con hacer clic en el botón \"eliminar mi cuenta\", pero algunos sitios ocultan este menú, intentan disuadirnos, nos imponen un límite de tiempo, nos obligan a ponernos en contacto con ellos por teléfono o a enviarles documentos... o incluso no ofrecen la posibilidad de hacerlo.",
              links: [
                {
                  text: "exigirle [en francés]",
                  link: "https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3#Article17"
                },
              ],
            },
            {
              title: "Buscar la opción",
              abstract: "En cualquier caso, el primer paso es comprobar si la página en cuestión ofrece eliminar su cuenta. Para ello, inicia sesión y ve a una sección llamada Configuración personal, Gestionar mi cuenta o algo similar. Con un motor de búsqueda, a menudo es posible encontrar rápidamente explicaciones detalladas sobre cómo hacer.",
            },
            {
              title: "Contacta la página",
              abstract: "Si no te dan una solución automática para eliminar tu cuenta, el segundo paso es ponerse en contacto directamente con las personas gestionan la página. La Comisión Nacional de la Informática y las Libertades (CNIL) ofrece una https://www.cnil.fr/fr/retrouver-les-coordonnees-dun-organisme-pour-exercer-vos-droits para encontrar los datos de la persona a la que dirigirse, así como un https://www.cnil.fr/fr/modele/courrier/cloturer-un-compte-en-ligne para rellenar y enviar (es la autoridad administrativa francesa encargada de ayudarnos a hacer valer nuestros derechos en este ámbito).",
              links: [
                {
                  text: "guía detallada [en francés]",
                  link: "https://www.cnil.fr/fr/retrouver-les-coordonnees-dun-organisme-pour-exercer-vos-droits"
                },
                {
                  text: "modelo de carta [en francés]",
                  link: "https://www.cnil.fr/fr/modele/courrier/cloturer-un-compte-en-ligne"
                },
              ],
            },
            {
              title: "Presentar una queja",
              abstract: "Si vives en un país de la UE y si todos estos pasos han fracasado y no has recibido una respuesta satisfactoria en el plazo de 30 días, puedes reclamar a la https://edpb.europa.eu/about-edpb/board/members_es pour lui demander d’intervenir.y pedirle que intervenga.\n \n" +
              "En Francia, es la https://www.cnil.fr/fr/plaintes/internet. En caso de práctica ilegal, la CNIL puede sancionar al sitio web y obligarlo a eliminar su cuenta y sus datos. Por desgracia, el tiempo que se tarda en obtener una respuesta de ellos suele ser demorado.",
              links: [
                {
                  text: "administración encargada de este asunto en tu país ",
                  link: "https://edpb.europa.eu/about-edpb/board/members_es"
                },
                {
                  text: "CNIL [en francés]",
                  link: "https://www.cnil.fr/fr/plaintes/internet"
                },
              ],
            }
          ],
          tweet: "Hay mil y una buenas razones para querer cerrar una cuentas en un sitio web. ¡Veamos cómo hacerlo en la práctica!",
          //pdf: require("assets/pdf/Sol_3.pdf"),
          tooltitle: "Cuentas",
          title: "Suprimir una cuenta de un servicio en línea ",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Jimmy" },
  { key: "Julie" },
  {
    key: "Nikki-Thought-1",
    isNikkiTought: true,
    thought: {
      title: 1,
      tweet: "Comunicaciones vigiladas",
      overview: "Nuestras comunicaciones pueden ser vigilada",
      message: "Intenté escribir un largo mensaje a los fans de Gao Games para explicarles por qué me iba, pero no pude hacerlo. Estaba demasiado cansado, se me hacía demasiado complicado y, para ser sincero, no tenía el valor... Lo sé, es una mierda. Sin embargo, dejé acá mis notas del borrador. No está muy claro, pero espero que alguien pueda hacer algo con esto. Ordené por temas las cosas tratando  abarcarlo todo. Esta nota por ejemplo es sobre el espionaje de las discusiones...\n" +
        "Por defecto, nuestros medios de comunicación digitales (correo electrónico, SMS, redes sociales, etc.) no son muy seguros y no garantizan la confidencialidad de nuestros intercambios. Especialmente los teléfonos móviles, porque su funcionamiento es oscuro a propósito y es difícil de modificar.\n" +
        "Todas nuestras conversaciones pueden ser vigiladas por empresas que ganan dinero con esta actividad (como Gao Games), por los estados y sus administraciones, o por personas malintencionadas de nuestro entorno ",
      links: [],
      skills: [
        {
          subskills: [
            {
              title: "Instalar Signal",
              abstract: "Cifrar tus comunicaciones significa que, gracias a un proceso matemático, tus mensajes serán ilegibles (\"indescifrables\") salvo para las personas a las que realmente van dirigidos. Sin que seamos necesariamente conscientes de ello, este proceso es aplicado por muchos servicios que utilizamos en nuestra vida cotidiana, por ejemplo para la navegación por Internet o los pagos en línea. https://signal.org/ -",
              links: [
                {
                  text: "Signal",
                  link: "https://signal.org/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss33.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss34.png")
                },
              ],
            },
            {
              title: "¿Por qué esta aplicación?",
              abstract: "Muchas aplicaciones de comunicación dicen ofrecer esta seguridad, pero... no todas son fiables. Sobre todo porque su modelo de negocio es usar y vender nuestros datos y nuestros metadatos. Además, cuando su código no es libre (como Whatsapp o Telegram), son aún menos fiables, porque nadie puede saber cómo funcionan realmente\n \n" +
              "Por supuesto, para que tus intercambios estén protegidos, tus contactos también tendrán que utilizar esta aplicación. Para saber más, aquí tienes una https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-signal-en-android que detalla todos los pasos, desde la instalación hasta el uso de la aplicación.\n \n"+
              "Y para ir más allá y saberlo todo sobre el cifrado, te recomiendo que leas este https://ssd.eff.org/es/module/%C2%BFqu%C3%A9-es-el-cifrado.",
              links: [
                {
                  text: " guía práctica",
                  link: "https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-signal-en-android"
                },
                {
                  text: "artículo",
                  link: "https://ssd.eff.org/es/module/%C2%BFqu%C3%A9-es-el-cifrado"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Lucas_1.pdf"),
          tooltitle: "Encriptar",
          tweet: "Para de mejorar de manera sencilla la confidencialidad de tus comunicaciones puedes elegir aplicaciones libres que ofrezcan encriptación. Hoy en día la más fácil de usar es Signal. Aunque no sea perfecta, es el mejor compromiso entre seguridad y facilidad de uso.",
          title: "Proteger tus comuniciones",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-2",
    isNikkiTought: true,
    thought: {
      title: 2,
      tweet: "¿Quién sabe dónde estoy?",
      overview: "Nuestro teléfono avisa en dónde estamos",
      message: "Al conectarse a una red móvil, nuestro operador de telefonía nos ubica de manera exacta (por ejemplo, Movistar, Claro, Tigo,  etc.). También es posible localizar móviles por Wifi o si el Bluetooth está activado.\n" +
      "Pero lo más grave son las aplicaciones que tenemos instaladas. Muchas usan el GPS para transmitir nuestra ubicación a empresas que tratan o venden esa información, entre ellas muchos de las grandes multinacionales de Internet, que llamaremos \"Gigantes de la Red\". Estas empresas y sus clientes -así como la policía y los servicios de inteligencia de los Estados- pueden saber dónde estamos, pero también a dónde hemos ido, cuántas veces, cuánto tiempo, con quién... Si quieres saber más sobre este tema, lee este https://ssd.eff.org/es/module/el-problema-con-los-tel%C3%A9fonos-m%C3%B3viles del sitio web Surveillance Self-Defense",
      links: [{
        text: "artículo",
        link: "https://ssd.eff.org/es/module/el-problema-con-los-tel%C3%A9fonos-m%C3%B3viles"
      }],
      skills: [
        {
          subskills: [
            {
              title: "Limpia las autorizaciones ya concedidas",
              abstract: "Cuando utilizas una aplicación por primera vez, te debe pedir permiso antes de acceder a tus datos o a las funciones de tu teléfono. Por ejemplo, podría tratarse de tu geolocalización, micrófono, cámara, lista de contactos... ¡datos y funciones potencialmente sensibles!\n"+

              "Con el tiempo, puede ser difícil recordar qué permisos has concedido o no a una aplicación. Para ver cuáles ya diste y poderlos cambiar, ve a la configuración de tu teléfono y luego al menú de permisos o aplicaciones.",
            },
            {
              title: "¿Dónde puedo encontrar la lista de autorizaciones?",
              abstract: "Encontrarás una lista completa de las aplicaciones ya instaladas en tu teléfono y los permisos concedidos a cada aplicación. Si buscas por \"permisos\", podrás ver todas las aplicaciones que acceden a un permiso en particular (por ejemplo, todas las aplicaciones que acceden a tu geolocalización o a tus contactos). Desde este menú, puedes desinstalar las aplicaciones o denegar los permisos ya concedidos.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss4.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss5.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss6.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss7.png")
                },
              ],
            },
            {
              title: "Obtén información detallada",
              abstract: "Para obtener información aún más detallada sobre los permisos de tu teléfono, no dudes en utilizar el \"escáner\" de Nikki, el CatScan. (Si no sabes lo que es, aún tienes mucho que descubrir aquí… ! )\n \n" +
              "El CatScan te permitirá ver el número de rastreadores en las aplicaciones instaladas y controlar tu progreso protegiendo tus datos en tu teléfono. ¡Es super útil !",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss8.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss9.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_1.pdf"),
          tooltitle: "Permisos",
          tweet: "Para funcionar, algunas aplicaciones requieren permisos especiales. Por ejemplo, si una aplicación de \"cámara\" solicita acceso a la cámara, parece lógico. Pero otras veces, las solicitudes de permisos de acceso no tienen nada que ver con el funcionamiento de la aplicación y sólo están ahí para recoger datos personales nuestros. Por ejemplo, una app de música que te pida acceso a tus contactos y a tus movimientos parece extraño. ¡Vamos a ver cómo configurar los para que nos protejan mejor!",
          title: "Configurar los permisos",
          showCheckBoxes: true
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-3",
    isNikkiTought: true,
    thought: {
      title: 3,
      tweet: "Dinero de los datos",
      overview: "Las empresas ganan mucho dinero vigilándonos",
      message: "El modelo económico de las grandes empresas de internet se basa en la colecta y el uso de nuestros datos personales. Las más conocidas se les llama GAFAM -acrónimo de Google, Amazon, Facebook, Apple y Microsoft-, pero no son las únicas\n" +
      "Al conocer en detalle nuestros comportamientos, estas empresas saben cómo influenciar nuestros comportamientos (para comprar algún producto, ver una película, entrar en una tienda cercana...). Cuanto más saben estas empresas sobre nosotros, más dinero ganan.\n"+
      " La mayoría de lo que hacemos en internet sirve también para recopilar cada vez más y más información sobre nuestras vidas: las búsquedas que hacemos en Internet, los videos que vemos, a qué horas y cómo usamos nuestros celulares, las tarjetas de pago y puntos que tenemos, el uso de televisores, relojes, parlantes y otros aparatos conectados... Cada acción nuestra da la oportunidad a que empresas accedan datos sueltos y que, una vez agregados, pueden decir mucho sobre nuestras vidas, nuestras opiniones y nuestro cotidiano. Aunque muchas de estas prácticas suelen ser ilegales, son muy frecuentes.\n" +
      "Afortunadamente, hay formas de protegerse de esto.",
      links: [],
      skills: [
        {
          subskills: [
            {
              title: "Desde la pantalla principal",
              abstract: "La forma más fácil de desinstalar una aplicación es hacer una pulsación larga sobre el ícono: aparecerá un mensaje que te ofrecerá borrarla. Algunas aplicaciones preinstaladas no se pueden desinstalar completamente. Es frustrante, pero en este caso, lo mejor que puedes hacer es abrir la información de la aplicación, forzar su cierre y eliminar los permisos.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss36.png")
                },
              ],
            },
            {
              title: "Desde la configuración",
              abstract: "Otra forma de desinstalar una aplicación es ir a los Ajustes del teléfono y luego al menú de Gestión de Aplicaciones. A continuación, simplemente selecciona la aplicación y pulsa Desinstalar y confirma tu elección.\n \n" +
              "¡Y ya ! De vez en cuando, no dudes en limpiar tus aplicaciones borrando las que no quieres usar o ya no usas.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss37.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss38.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/though 4"),
          tooltitle: "Desinstalar",
          tweet: "Hay varias formas de desinstalar una aplicación del teléfono. Así es como se hace:",
          title: "Desinstalar una aplicación",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-4",
    isNikkiTought: true,
    thought: {
      title: 4,
      tweet: "Vigilancia y Democracia",
      overview: "La vigilancia masiva debilita la democracia",
      message: "Mediante el monitoreo de nuestras acciones, las empresas son capaces de deducir nuestras opiniones políticas y los argumentos que nos pueden convence para inducirnos a hacer o no algo. E incluso votar o no votar por alguna persona en elecciones democráticas.\n" +
      "La empresa https://es.wikipedia.org/wiki/Cambridge_Analytica por ejemplo, está acusada de haber utilizado datos https://www.europapress.es/portaltic/socialmedia/noticia-esto-todo-debes-saber-escandalo-facebook-filtracion-datos-cambridge-analytica-20180321132211.html en parte vía https://la-rem.eu/2020/06/facebook-a-nouveau-condamne-dans-le-cadre-de-laffaire-cambridge-analytica/ para influenciar la elección de Donald Trump en Estados Unidos o en el referéndum sobre la salida de la Gran Bretaña de la Unión Europea.\n" +
      "En total, Cambridge Analytica influyó en más de 200 elecciones en todo el mundo en el sentido que querían todas las personas que pueden pagar por este tipo de servicios. Obviamente este tipo de influencia va en contra del funcionamiento de la democracia, en la que son las personas quienes toman sus decisiones libremente y sin ser manipuladas. Sabernos bajo vigilancia, cambia nuestra forma de comportarnos. Por ejemplo, la gente se abstendrá de hacer o decir algo (por ejemplo https://www.slate.fr/story/116035/surveillance-masse-opinions-minoritaires) por miedo a ser sancionada. De manera más general, los gigantes de Internet tienen un gran poder sobre la información https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/ en nuestra visión del mundo. En una situación ideal, este control debería estar en manos de los usuarios de estos servicios, y no en manos de empresas privadas.",
      links: [{
        link: "https://es.wikipedia.org/wiki/Cambridge_Analytica",
        text: "Cambridge Analytica",
      },{
        link: "https://www.europapress.es/portaltic/socialmedia/noticia-esto-todo-debes-saber-escandalo-facebook-filtracion-datos-cambridge-analytica-20180321132211.html",
        text: "recogidos",
      }, {
        link: "https://la-rem.eu/2020/06/facebook-a-nouveau-condamne-dans-le-cadre-de-laffaire-cambridge-analytica/",
        text: "Facebook [en francés] ",
      }, {
        link: "https://www.slate.fr/story/116035/surveillance-masse-opinions-minoritaires",
        text: "expresar una opinión minoritaria [en francés]",
      },{
        link: "https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/",
        text: "que nos llega e influye [en inglés]",
      }],
      skills: [
        {
          subskills: [
            {
              title: "Comprender el tema",
              abstract: "Según la Agencia Francesa para la Transición Ecológica (ADEME), https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-impacts-smartphone.pdf del impacto medioambiental de los teléfonos móviles está relacionado con su fabricación, principalmente por sus pantallas y complejos componentes electrónicos, como los microprocesadores.\n \n" +
                "Cuando se extraen las materias primas para estos aparatos también hay consecuencias sociales dramáticas.",
              links: [
                {
                  text: "environ 75 % [en francés]",
                  link: "https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-impacts-smartphone.pdf"
                },
              ],
            },
            {
              title: "Hacer que tus electrodomésticos duren",
              abstract: "La mejor manera de reducir las consecuencias de nuestros usos es hacer que nuestros aparatos duren y no reemplazarlos mientras sigan funcionando. Según la ADEME, utilizándolos durante 4 años en lugar de 2, mejoraríamos el balance ecológico de nuestros aparatos en un 50%.\n \n" +
                "Para ello, podemos protegerlos con fundas, carcasas y películas para pantallas y asegurarnos de dejarlos enfriar cuando empiecen a sobrecalentarse. Por último, no esperes a que la batería de tu dispositivo se agote por completo para recargarlo y evita dejarlo cargando toda la noche. De este modo, durante más tiempo.\n \n" +
                "Si uno de sus aparatos se daña, a menudo es posible repararlo. Si tiene menos de 2 años, ¡incluso tiene una garantía legal para ello! También puedes intentar repararlo tú : muchos tutoriales online explican cómo hacerlo, por ejemplo los de la web https://es.ifixit.com/, y cada vez más asociaciones podrán echarte una mano.",
              links: [
                {
                  text: "ifixit.com",
                  link: "https://es.ifixit.com/"
                },
              ],
            },
            {
              title: "Piensa antes de comprar",
              abstract: "Y cuando tengas que remplazar un electrodoméstico, no olvides tener en cuenta lo siguiente : informarte y elije aparatos que realmente necesitas y sirvan, que puedan repararse fácilmente. Para ello asegúrate que las marcas no hagan funcionar mal sus productos a propósito (a esto se llama obsolescencia programada).\n \n" +
              "Para los teléfonos es buena idea es elegir uno de segunda mano : pagarás menos y evitarás el coste medioambiental de fabricar un aparato nuevo. Por último, no olvides reciclar el que ya no usas para que pueda tener una segunda vida.\n \n" +
              "Si quieres un teléfono nuevo, puedes consultar los teléfonos que ofrece la empresa https://www.fairphone.com/es/, cuyo objetivo es fabricar de manera ética, o pensar en alquilarlos en lugar de poseerlos. Es posible que haya cada vez más fabricantes similares en un futuro: lo importante no es la marca, sino el enfoque.",
              links: [
                {
                  text: "Fairphone",
                  link: "https://www.fairphone.com/es/"
                },
              ],
            }
          ],
          tweet: "Como hemos visto, las herramientas digitales también contaminan. Veamos cómo limitar los daños.",
          tooltitle: "Ecología",
          //pdf: require("assets/pdf/though 5"),
          title: "Limitar las consecuencias ecológicas y sociales de nuestros usos digitales",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-5",
    isNikkiTought: true,
    thought: {
      title: 5,
      tweet: "Contaminación",
      overview: "Las herramientas digitales también contaminan",
      message: "En contra de lo que se podría pensar, usar las herramientas digitales contamina el planeta. La fabricación de aparatos y equipos, la navegación por internet, con páginas y servicios disponibles 24 horas al día, el envío de correos electrónicos, las impresiones en casa... ¡todas estas actividades consumen energía, producen residuos y requieren grandes volúmenes de metales raros!\n" +
      "Lejos de sustituir nuestras usos que contaminan (por ejemplo, hacer una vídeo llamada en lugar de hacer un viaje en avión), las herramientas digitales hacen que se multipliquen.  Los https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/ muestran que el uso de la informática generan más gases de efecto invernadero que el transporte aéreo, y que estas emisiones pronto alcanzarán los niveles del transporte terrestre.\n" +
      "Las condiciones de fabricación de nuestros aparatos  y sus componentes suelen ser https://www.monde-diplomatique.fr/2020/07/BELKAID/61982 (esclavage, travail des enfants…). Afortunadamente, existen soluciones y podemos actuar para limitar las consecuencias sociales y medioambientales negativas de nuestras prácticas digitales.",
      links: [{
        link: "https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/",
        text: "estudios [en francés] "
      },{
        link: "https://www.monde-diplomatique.fr/2020/07/BELKAID/61982",
        text: "escandalosas [en francés]",
      }],
      skills: [
        {
          subskills: [
            {
              title: "¿Cómo circula la información entre un sitio y nosotros?",
              abstract: "Tienes que imaginar que cada vez que vas a internet, es como si la página y tu teléfono estuvieran \"chateando\", es decir, enviándose información. Cuando quieres ver una pestaña o un vídeo, la página que consultas necesita saber qué debe mostrarte. Si vas a comprar algo, tienes tu que enviarle tu número de tarjeta de crédito. Al utilizar el protocolo HTTPS, los datos se envían de forma secreta y completa, es la confidencialidad y la integridad de los datos. Si una persona malintencionada interceptara estos datos que se envían la página y tu teléfono, si estás usando el HTTPS, sólo podría acceder a una serie cadena de caracteres imposibles de leer.\n" +
                "Hoy en día, muchos sitios web usan el protocolo de seguridad HTTPS por defecto. Si la página que ves no lo tiene, simplemente añade una  s  al HTTP del sitio que estás visitando. Una forma de acordarte es pensar en la S como \"seguro\"). Y si no quieres hacerlo a la mano cada vez, puedes instalar la extensión https://www.eff.org/https-everywhere/, que lo hace automáticamente por ti.",
              links: [
                {
                  text: "HTTPS Everywhere",
                  link: "https://www.eff.org/https-everywhere/"
                },
              ],
            },
            {
              title: "Visualización en un navegador web",
              abstract: "Los navegadores web muestran un candadito cerrado cuando son seguros, o tachado cuando no lo son.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss39.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss40.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss41.png")
                },
              ],
            },
            {
              title: "Conexión no segura",
              abstract: "Si al añadir una s al HTTP ves un mensaje de error la conexión no es segura y verás esto :",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss42.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss43.png")
                },
              ],
            },
            {
              title: "¿Qué hacer?",
              abstract: "¡Que no cunda el pánico ! Quizás la página no ofrece esta seguridad. En este caso, visítalo en HTTP simple, pero ten especial cuidado de no introducir contraseñas, números de tarjetas de crédito o cualquier otra información.\n \n" +
                "Pero si el protocolo HTTPS suele funcionar para esta página y sigues viendo el mismo mensaje de error, pueden ser dos cosas :\n \n" +
                "• Un problema de configuración de la página, y volviendo unos minutos más tarde puede que todo esté en orden.\n" +
                "• Si el mensaje sigue ahí, también podría ser un intento de estafa y que la página ha sido hackeada.\n \n" +
                "Si decides no tener en cuenta los mensajes de error y de alerta, recuerda que navegas bajo tu propia responsabilidad. Y, de nuevo, nunca introduzcas una contraseña, un número de tarjeta de crédito o cualquier otra información. Estos mensajes están ahí para informarte del estado de la página y que tus datos están en peligro. ",
            }
          ],
          tweet: "Al navegar en Internet, lo mejor es protegerse de las estafas más comunes.",
          //pdf: require("assets/pdf/though 3-1"),
          tooltitle: "Navegar",
          title: "Mejora tu seguridad al navegar",
        },
        {
          subskills: [
            {
              title: "Evitar riesgos",
              abstract: "Incluso para expertos, es difícil determinar con exactitud el nivel de seguridad de una aplicación ya que no se puede saber a ciencia cierta lo que hace y no que no puede hacer. Para descargar una nueva aplicación de manera segura, es importante pasar por una tienda de aplicaciones. Estas tiendas en general comprueban que las aplicaciones no contengan virus y que nos pedirán permisos antes de acceder a nuestros datos y a los sensores de nuestro dispositivo.\n \n" +
                "Por el contrario, instalar una aplicación desde una fuente distinta a estas tiendas te expone a un riesgo importante de acabar con un programa engañoso (por ejemplo, un archivo .apk gratis ofrecida por Internet).",
            },
            {
              title: "Recoger pruebas",
              abstract: "Aveces es difícil orientarse entre todas las aplicaciones que ofrecen estas tiendas. Varios elementos pueden ayudarte a medir la fiabilidad de una aplicación: el número de descargas, la descripción, las opiniones y comentarios de otras personas, las autorizaciones que pide la aplicación cuando se lanza por primera vez o tras una actualización...\n \n"+
              "También puedes averiguar qué organización es la propietaria de la aplicación. Por ejemplo, además de Android, Google también posee Chrome, YouTube, Waze y muchos otros. Facebook controla Instagram y WhatsApp. En caso de duda, es mejor no instalar la aplicación en cuestión y elegir otra.\n" +
              "Por último, gracias a nuestra herramienta, que utiliza https://reports.exodus-privacy.eu.org/es/ es posible proporcionarte información sobre las aplicaciones más conocidas e indicar para cada una de ellas el número de rastreadores que incorpora y las autorizaciones que necesita para funcionar.",
              links: [
                {
                  text: "Exodus Privacy,",
                  link: "https://reports.exodus-privacy.eu.org/es/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss44.png")
                },
              ],
            },
            {
              title: "Utiliza una fuente confiable",
              abstract: "Además de ofrecer aplicaciones libres, gratuitas y respetuosas (y, por tanto, más seguras), la tienda F-Droid permite medir la fiabilidad de las aplicaciones. En las descripciones de las aplicaciones, se enumeran las características que podrían no gustarte. Esto puede facilitar la visualización y la comparación de varias aplicaciones antes de elegir la que más te convenga.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss45.png")
                },
              ],
            }
          ],
          tweet: "Antes de instalar una aplicación, es importante asegurarse que puedes confiar en ella, porque va a acceder a nuestros datos, recoger nuevos y borrarlos o pasarlos a personas o empresas malintencionadas.",
          //pdf: require("assets/pdf/though 3-2"),
          tooltitle: "Confianza",
          title: "Estimar qué tan confiable es una aplicación ",
        },
        {
          subskills: [
            {
              title: "Encuentra el menú",
              abstract: "Para hacer una copia de seguridad de tus datos, ve a los Ajustes de tu teléfono, luego al menú Otros Ajustes y pulsa Copia de seguridad y reinicio (en la parte inferior):",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss46.png")
                },
              ],
            },
            {
              title: "Selecciona los datos de los que deseas hacer una copia de seguridad",
              abstract: "Una vez estés en este menú, puls Copia de seguridad y restauración, luego Crear copia de seguridad y elije los datos de los que desea hacer una copia de seguridad. Puedes dejar todas las casillas marcadas.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss47.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss48.png")
                },
              ],
            },
            {
              title: "Iniciar la copia de seguridad",
              abstract: "A continuación, pulsa Iniciar copia de seguridad y ¡ya está! Al cabo de unos instantes, la copia de seguridad se habrá completado y aparecerá en el menú. Al pulsarlo, ahora puede restaurar los datos de su elección siempre que lo necesite. Pero no te detengas ahí.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss49.png")
                },
              ],
            },
            {
              title: "Exporta tu copia de seguridad",
              abstract: "Para que tus datos estén realmente seguros, debes copiarlos ara poder acceder a ellos y reinstalarlos en tu teléfono si lo llegaras a perder. Para realizar esta copia, conecta tu teléfono a un compu con un cable USB. Aparecerá una notificación en su pantalla: selecciona Transferir archivos.\n" +
                "Ahora puedes acceder al contenido del teléfono desde tu compu. Busca la carpeta Backup y cópiala en tu compu: es ahí en donde se almacenan tus copias.\n" +

                "Si no tienes un compu, también puedes introducir una tarjeta SD en tu teléfono y copiar el archivo en ella. En este caso, no olvides guardar la tarjeta SD de forma segura después (no la dejes en el teléfono, para no arriesgarte a perder las dos cosas a la vez).",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss50.png")
                },
              ],
            },
            {
              title: "Asegurar las copias de seguridad",
              abstract: "Una buena regla de salvaguardia es la regla \"3 2 1 *\", es decir:\n" +
                    "• 3 copias de cada dato (el original en el teléfono y 2 copias)\n" +
                    "• Al menos 2 soportes diferentes (por ejemplo, un compu y un disco duro externo o una tarjeta SD)\n" +
                    "• Al menos 1 copia en una ubicación diferente a las demás (puede ser en línea)\n" +
                    "• * y la estrella para indicar que estas copias de seguridad deben ser encriptadas.\n \n" +
                  "Así, sea cual sea el problema que te encuentres, una de las copias de seguridad siempre estará disponible para restaurar tus datos. Ya sea un robo, un incendio, un error de funcionamiento o un problema técnico que destruya tu ordenador y tu teléfono al mismo tiempo, podrás hacer frente a todas estas situaciones.\n" +
                  "Por supuesto, también puedes adoptar un método más sencillo si éste te parece demasiado complicado: ¡mejor tener una copia que ninguna!\n \n" +
                  "Es importante elegir cuidadosamente el soporte en el que se van a almacenar tus copias. Lo ideal es que ese soporte esté encriptado. Esta breve https://www.osi.es/es/actualidad/blog/2019/05/29/cifrado-y-almacenamiento-seguro-de-ficheros-paso-paso explica cómo hacerlo en la práctica.\n \n"+
                  "Algunas personas optan por que sus copias de seguridad las guarden los GAFAM u otros gigantes de Internet. Como habrás adivinado, esta no es la solución que te recomiendo. Si aún así quieres hacerlo, te sugiero que empieces por tomarte el tiempo necesario para decidir qué aplicaciones y datos vas a sincronizar en línea y cuáles es mejor guardar en otra parte.\n \n" +
                  "Un último consejo: no olvides comprobar de vez en cuando que tus copias de seguridad han funcionado y que es posible restaurar tus datos. Y si prefieres una solución a medida, por ejemplo para poder hacer también una copia de seguridad de los datos de tus aplicaciones, te recomiendo utilizar las aplicaciones https://f-droid.org/es/packages/com.machiav3lli.backup/ (sólo disponible en la tienda F-Droid) o https://twrp.me/app/.",
              links: [
                {
                  text: "guía,",
                  link: "https://www.osi.es/es/actualidad/blog/2019/05/29/cifrado-y-almacenamiento-seguro-de-ficheros-paso-paso"
                },
                {
                  text: "OandbackupX,",
                  link: "https://f-droid.org/es/packages/com.machiav3lli.backup/"
                },
                {
                  text: "TWRP [en inglés]",
                  link: "https://twrp.me/app/"
                },
              ],
            }
          ],
          tweet: "Uno de los objetivos de la seguridad informática es proteger los datos de la destrucción. Para ello, es esencial hacer copias de seguridad de tus datos con frecuencia, sobre todo porque los teléfonos son, por naturaleza, fáciles de perder o se pueden dañar. En cualquier momento puede surgir un problema, así que es mejor estar preparado de antemano.",
          //pdf: require("assets/pdf/though 3-3"),
          tooltitle: "Backup",
          title: "Hacer una copia de seguridad de tus datos",
        }
      ]
    }
  }
]
