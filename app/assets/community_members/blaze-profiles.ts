export interface BlazeProfile {
  key: string;
  selected?: boolean;
  enabled?: boolean;
  hasAnimation?: boolean;
  about?: string,
  image?: any,
  informationShared?: string,
  overview?: string,
  recommendations?: string,
  imageUri?: string,
  thought?: BlazeProfileThought
  isForReturn?: boolean
  isNikkiTought?: boolean
}

export interface BlazeProfileThought {
  tweet?: string, title: number, message: string, overview: string,
  links?: { text: string, link: string }[], skills?: { tooltitle?: string, showCheckBoxes?: boolean, pdf?: string,
    subskills: {abstract: string, title: string, links?: { text: string, link: string }[],images?: { text: string, image: string}[]}[], tweet: string, title?: string }[]
}

export const BLAZE_PROFILES: BlazeProfile[] = [
  {
    key: "Ajay",
    about: "19 ans. Célibataire. Sans enfants. Étudiant. Top player (Gao the Sage).",
    image: require("assets/images/community/avatars/B-Ajay.png"),
    informationShared: "SMS, Accéléromètre, Contacts, Géolocalisation, Stockage, Wifi, Réseau, Notifications, Bluetooth, NFC, Alarme, Fond d’écran, Microphone, Lecteur d’empreintes digitales, Calendrier, Appareil photo",
    overview: "Étudiant, végétarien, sociable, curieux, immigré primo-arrivant, passionné de sciences.",
    recommendations: "Données de centres d’intérêt et de réseau social collectées (vente client #12579 – agence de sécurité nationale): l’analyse des communications et de la base de réponses de type 3 de Gao the Sage indique que le sujet a de nombreuses connaissances amicales impliquées dans des manifestations au Moyen-Orient. La cartographie des méta-données a permis d’identifier 78% de ses contacts.\n" +
      "@Note interne:@ récente baisse des communications avec ses contacts, suite à la précédente vente de données (client #55578 – agence de sécurité privée).",
    thought: {
      title: 6,
      tweet: "Répression",
      overview: "La surveillance de masse facilite la répression exercée par les États",
      message: "En soi, la surveillance de masse est déjà une violation de nos droits fondamentaux. Malheureusement, elle permet aussi des atteintes encore plus graves aux droits humains. Des gouvernements l’utilisent par https://www.bastamag.net/Khashoggi-Mansoor-Whatsapp-espionnage-spyware-Pegasus-droits-humains-opposants-NSO-Group-Amnesty pour réprimer des mouvements de contestation ou pour s’en prendre à des groupes  de personnes, comme des opposant·es politiques, des défenseu·ses de diverses causes, des journalistes ou des gens n’ayant pas « la bonne » couleur de peau, opinion, orientation sexuelle, ou n’importe quoi d’autre. Comme le lanceur d’alerte https://fr.wikipedia.org/wiki/Edward_Snowden l’a révélé, la surveillance exercée par les géants du Net nourrit directement celle exercée par certains États. Se protéger de l’une permet de se protéger de l’autre, mais aussi de protéger ses proches. En effet, nos pratiques ont des conséquences sur les personnes qui nous entourent. Pour certaines d’entre elles, garder les informations les concernant  secrètes peut être très important — voire vital.",
      links: [
        {
          text: "exemple",
          link: "https://www.bastamag.net/Khashoggi-Mansoor-Whatsapp-espionnage-spyware-Pegasus-droits-humains-opposants-NSO-Group-Amnesty"
        },
        {
          text: "Edward Snowden",
          link: "https://fr.wikipedia.org/wiki/Edward_Snowden"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Choisir un mot de passe",
              abstract: "Que tu choisisses d’utiliser un schéma, une suite de chiffres ou un mot de passe, l’une des mesures les plus importantes pour protéger ses données est de configurer un code de déverrouillage. Après chaque extinction de l’écran, il sera nécessaire de l’entrer pour pouvoir accéder aux données contenues dans l’appareil.\n"+
                        "Pour le configurer ou le modifier, rends-toi dans les Paramètres de ton téléphone, puis dans le menu Empreinte, visage & mot de passe. Pour régler le délai avant l’extinction automatique de l’écran en cas d’inactivité, rends-toi dans le menu Affichage et luminosité.\n"+
                        "Pour plus de sécurité, je te recommande d’éviter les méthodes biométriques et les dessins ou codes trop faciles à deviner (comme la forme de la première lettre de ton prénom ou ta date de naissance). Pour en savoir plus, voici les https://www.ssi.gouv.fr/guide/mot-de-passe/ de l’Agence nationale de la sécurité des systèmes informatiques (ANSSI) pour choisir des mots de passe robustes et le https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut pour utiliser un gestionnaire de mots de passe proposé par le site NextINpact.com.          ",
              links: [
                {
                  text: "recommandations",
                  link: "https://www.ssi.gouv.fr/guide/mot-de-passe/"
                },
                {
                  text: "guide",
                  link: "https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut"
                },
              ],
            },
            {
              title:"Copier ton code IMEI",
              abstract:"Le code IMEI est un numéro permettant d’identifier de manière unique ton téléphone. Tu peux l’obtenir en composant le code *#06#. Tu le trouveras également sur la boite et la facture de ton appareil. Conserve-le précieusement : il te serait demandé pour définitivement bloquer ton appareil en cas de vol.",
            },
            {
              title:"Garder ton téléphone à jour",
              abstract:"Installe les mises à jour chaque fois que possible pour garder tes applications et ton appareil sécurisés. En plus des nouvelles fonctionnalités, elles comportent souvent des corrections de failles et de bugs. Pour être sûr·e de n’en rater aucune, le plus simple est d’activer les mises à jour automatiques.\n"+
                       "Pour cela, rends-toi dans les Paramètres de ton magasin d’applications, puis dans le menu Mises à jour si tu utilises le Play Store. Ici, appuie sur Activer les mises à jour automatiques ou Récupérer automatiquement les mises à jour, et configure la fréquence de la vérification.          ",
            },
            {
              title:"Chiffrer ton téléphone",
              abstract:"Depuis quelques années, la plupart les téléphones proposent de chiffrer les données qu’ils contiennent. Cela signifie que seule la personne qui connaît le mot de passe permettant de déverrouiller l’appareil peut accéder à ces données.\n"+
                       "En fonction de la version d’Android installée sur ton téléphone, le processus et la méthode pourront varier. Au cas où l’option ne serait pas déjà activée par défaut et si elle est disponible, tu peux chiffrer la mémoire de ton téléphone ou celle de ta carte micro SD. Pour cela, rends-toi dans les Paramètres du téléphone, puis dans le menu Sécurité ou équivalent.\n"+
                       "Attention tout de même ! Pour une carte SD, l’opération implique de commencer par supprimer toutes les données qu’elle contient. Et dans tous les cas, si tu perdais le mot de passe, tes données seraient vraiment perdues aussi.          ",
            },
            {
              title:"Ne pas oublier les métadonnées",
              abstract:"Lorsque nous prenons une photographie, notre téléphone y incorpore automatiquement des métadonnées. Il s’agit d’informations telles que la date, la géolocalisation, le réglage de l’appareil…  Toutes ces informations peuvent révéler beaucoup de chose sans que nous le souhaitions.\n"+
                       "Ainsi, pense à les vérifier avant de partager tes photographies, ou carrément à les effacer. Pour cela, tu peux utiliser l’application Scrambled Exif (avec https://f-droid.org/fr/packages/com.jarsilio.android.scrambledeggsif/ ou le https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif).          ",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/fr/packages/com.jarsilio.android.scrambledeggsif/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif"
                },
              ],
            },
            {
              title:"Désactiver les fonctionnalités inutiles",
              abstract:"Lorsque tu ne les utilises pas, désactive la géolocalisation, le wifi et le bluetooth. En prenant cette habitude, tu limiteras la possibilité de collecter des données sur toi et ton appareil et tu amélioreras sa sécurité et l’autonomie de sa batterie. Pour cela, place ton doigt sur la barre de statuts, glisse-le vers le bas, et appuis sur les icônes correspondantes.",
            },
            {
              title:"Préférer les sites aux applications",
              abstract:"De manière générale, mieux vaut utiliser le site internet d’un service en ligne que son application dédiée : tu limiteras les données auxquelles il pourra accéder sur ton téléphone.",
            },
            {
              title:"Remettre à zéro tes nouveaux et tes anciens appareils",
              abstract:"Si tu récupères un téléphone ou que tu te sépares du tien, n’oublie pas d’effacer toutes les données qu’il contient. Ce petit https://www.cnil.fr/fr/effacer-ses-donnees-dun-ordinateur-dun-telephone-ou-dune-tablette-avant-de-sen-separer de la Commission nationale de l’informatique et des libertés (CNIL) nous explique comment faire en pratique.",
              links: [
                {
                  text: "guide",
                  link: "https://www.cnil.fr/fr/effacer-ses-donnees-dun-ordinateur-dun-telephone-ou-dune-tablette-avant-de-sen-separer"
                },
              ],
            },
            {
              title:"Faire le point régulièrement",
              abstract:"Aucune solution informatique n’est fiable ni à 100 %, ni pour toujours. N’oublie pas de garder un œil sur le sujet et de te demander de temps en temps si les choix que tu as faits sont toujours pertinents et s’ils te conviennent toujours.",
            },
            {
              title:"Ne pas oublier les détails",
              abstract:"En matière de sécurité informatique, la solidité d’un ensemble dépend de son maillon le plus faible : il peut suffire d’un petit détail pour que l’ensemble soit compromis. Ainsi, il faut aussi faire attention à ce qui peut sembler être de petits détails.\n"+
                            "Par exemple, je te recommande de remplacer le clavier installé par défaut (Gboard) sur ton téléphone par un clavier libre et respectueux de tes données. Celui que je conseille est OpenBoard, que tu peux installer depuis les magasins d’applications https://f-droid.org/fr/packages/org.dslul.openboard.inputmethod.latin/ ou https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin. Pour l’utiliser, rends-toi ensuite dans les Paramètres du téléphone, puis dans Autres paramètre, puis Clavier et méthode de saisie pour choisir OpenBoard et pouvoir le configurer.          ",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/fr/packages/org.dslul.openboard.inputmethod.latin/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin"
                },
              ],
            },
            {
              title:"Adopter une attitude respectueuse",
              abstract:"Au-delà de toutes ces recommandations, protéger tes données et celles de tes proches passe aussi par l’adoption d’attitudes respectueuses les un·es envers les autres. Il s’agit par exemple de ne pas oublier de demander à la personne concernée si l’on peut partager des informations à son sujet, de ne pas tout enregistrer par défaut et de minimiser la quantité de données que l’on produit, de s’entraider, de discuter et partager ses découvertes…\n"+
                       "Et pour arriver à des solutions réellement satisfaisantes et ne laisser personne au bord du chemin, il est nécessaire de nous organiser collectivement pour améliorer les lois et les outils numériques que nous utilisons pour nous protéger. Si l’aventure t’intéresse ou simplement pour suivre l’actualité en la matière, c’est le https://laquadrature.net/ de La Quadrature du Net que je te conseille d’aller lire.\n"+
                       "Pour aller plus loin, tu trouveras de nombreux autres recommandations sur le site https://datadetoxkit.org/fr/home et dans le très complet https://guide.boum.org/.          ",
              links: [
                {
                  text: "site",
                  link: "https://laquadrature.net/"
                },
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/fr/home"
                },
                {
                  text: "guide d’autodéfense numérique",
                  link: "https://guide.boum.org/"
                },
              ],
            }
          ],
          tweet: "En plus de tous les autres savoir-faire, quelques gestes simples permettent d’améliorer facilement et rapidement la sécurité de nos téléphones. Certaines personnes en parlent même comme de règles d’hygiène numérique, pour établir un parallèle avec les règles de base de l’hygiène corporelle.",
          //pdf: require("assets/pdf/Ajay_3.pdf"),
          tooltitle: "Protéger",
          title: "Protéger nos données et celles de nos proches",
        },
        {
          subskills: [
            {
              title: "Faire le point sur sa situation",
              abstract: "Afin de limiter efficacement les risques dans le domaine de la sécurité informatique, il est indispensable de commencer par identifier les données que tu souhaites protéger, et de quoi ou qui, en fonction de ta situation.\n"+
                            "En effet, un outil numérique indispensable dans certains cas peut s’avérer dangereux dans d’autres, par exemple parce qu’il signale que tu souhaites protéger ces données.\n"+
                            "Pour commencer, il faut donc que tu fasses le point sur ta situation, en listant les données importantes ou sensibles pour toi et en réfléchissant aux dangers potentiels. En d’autres termes, il s’agit d’établir ton modèle de menaces.\n"+
                            "La suite du travail consiste à analyser les risques que tu identifies. Tous n’ont pas la même probabilité de se réaliser, ni n’auraient des conséquences de même gravité. En estimant ces 2 critères, il devient facile de faire le tri entre les risques que tu dois prendre au sérieux rapidement et ceux dont il n’est pas vraiment nécessaire de te soucier.          ",
            },
            {
              title: "Décider des actions à mener",
              abstract: "Grâce à la cartographie que tu as réalisée lors de l’étape précédente, tu peux maintenant envisager les actions à mener pour te prémunir des risques que tu juges en valoir la peine.\n"+
              		              "Cette fois, c’est l’efficacité et la complexité de la réalisation de chaque piste de solution que tu devras estimer, qu’elle soit de nature technique ou autre. À cette étape, il est particulièrement important de prendre en compte l’ensemble des éléments de la situation donnée :\n"+
                                "          •	l’intégrité des équipements et la protection physique des lieux,\n"+
                                "          •	la sensibilisation et la formation des personnes concernées,\n"+
                                "          •	le choix et la configuration des outils,\n"+
                                "          •	le bien être des personnes concernées et leur acceptation sociale des outils et pratiques,\n"+
                                "          •	les mesures techniques et organisationnelles,\n"+
                                "          •	…\n"+
                                "Certaines idées présenteront plus d’inconvénients que d’avantages ou pourraient même empirer la situation, tandis que d’autres se révéleront particulièrement simples et adaptées. Dans certains cas, les risques s’avéreront trop complexes ou coûteux à traiter, et ne pourront qu’être acceptés. Mais même sans solution immédiate, avoir conscience du danger reste primordiale !\n"+
                                "Enfin, il ne te restera plus qu’à choisir et planifier les actions à réaliser, puis à revenir sur ce travail de temps en temps pour vérifier qu’il ne nécessite pas d’être adapté à de nouveaux besoins et contextes.\n"+
                                "Pour en apprendre davantage sur la réalisation d’un plan de sécurité, je te recommande la lecture de ce petit https://ssd.eff.org/fr/module/votre-plan-de-sécurité publié sur le site Surveillance Self-Defense ou https://laboussole.coop/2019/04/16/document-modele-etablir-un-plan-de-securite-informatique/ de la coopérative de formation et de recherche La Boussole.          ",
              links: [
                {
                  text: "guide",
                  link: "https://ssd.eff.org/fr/module/votre-plan-de-sécurité"
                },
                {
                  text: "celui",
                  link: "https://laboussole.coop/2019/04/16/document-modele-etablir-un-plan-de-securite-informatique/"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_1.pdf"),
          tooltitle: "Modèle protection",
          tweet: "En matière de sécurité informatique, les menaces et besoins de chacun·e peuvent être très différents. Voyons comment trouver une solution sur-mesure.",
          title: "Établir un modèle de menace",
        },
        {
          subskills: [
            {
              title: "Créer plusieurs adresses emails",
              abstract: "Il s’agit par exemple de créer plusieurs adresses emails et de les utiliser chacune pour des choses différentes. Typiquement : une pour les ami·es, une pour le travail et une pour les choses peu importantes. De cette manière, nous séparons les différents aspects de notre identité et il devient plus difficile de les relier.",
            },
            {
              title: "Créer plusieurs comptes en ligne",
              abstract:"De la même manière, il est possible de créer plusieurs comptes sur les réseaux sociaux, avec des pseudonymes différents, et de les paramétrer différemment en fonction des personnes à qui l’on souhaite s’adresser. Ainsi, seules les personnes que nous choisissons réellement pourront accéder à nos publications en ligne et il leur sera difficile de passer d’un compte à l’autre sans notre accord.              Pour renforcer cette protection, une bonne pratique est de limiter au strict nécessaire le nombre d’informations que nous indiquons et transmettons aux plateformes en ligne. C’est-à-dire ce qui nous semble utile à nous. Et si elles en demandent plus, alors nous pouvons toujours donner de fausses informations !          ",
            },
            {
              title:"Multiplier ses outils numériques",
              abstract:"	Pour que ce cloisonnement soit encore plus efficace, tu peux également utiliser plusieurs navigateurs Web, plusieurs sessions sur ton ordinateur, ou carrément des machines différentes. Même si ces méthodes ne suffiront pas à elles seules d’empêcher totalement les grandes entreprises de te surveiller, les traces que tu laisseras chaque fois que tu utiliseras un service en ligne seront dispersées et il deviendra plus difficile de les relier.              Pour en savoir plus à ce sujet et découvrir d’autres méthodes pour protéger tes données, je te recommande la lecture de cet https://www.ritimo.org/La-confidentialite-la-surveillance-sur-le-Net-le-suivi-des-donnees-pourquoi-est publié sur le site ritimo.org, un réseau d’information et de documentation pour la solidarité et le développement durable.          ",
              links: [
                {
                  text: "article",
                  link: "https://www.ritimo.org/La-confidentialite-la-surveillance-sur-le-Net-le-suivi-des-donnees-pourquoi-est"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_2.pdf"),
          tooltitle: "Identités",
          tweet: "Un moyen simple pour mieux protéger ses données personnelles est de pratiquer le cloisonnement des identités. Cette pratique consiste à multiplier les profils que nous créons lorsque nous utilisons des outils numériques.",
          title: "Le cloisonnement des identités",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Sally" },
  { key: "Pierre" },
  { key: "Molly" },
  {
    key: "Alex",
    about: "28 ans. Séparée (ex-conjoint user#1547699, username «Ben»). 1 enfant en bas âge («Ulysse»). Sans emploi. Top player (Gao the Intruder).",
    image: require("assets/images/community/avatars/B-Alex.png"),
    informationShared: "Contacts, Géolocalisation, Stockage, Wifi, Réseau, Notifications, SMS, Bluetooth, NFC, Alarme, Fond d’écran, Microphone, Lecteur d’empreintes digitales, Calendrier, Appareil photo, Accéléromètre",
    overview: "Sociable, efficace au travail, manque de confiance en soi, 86% probabilité d’être en dépression, investie dans Gao Games (modératrice chat), proche de Blaze.",
    recommendations: "Données d’état émotionnel collectées (usage interne):Profil psychologiquement fragile susceptible d’investir beaucoup de temps bénévole dans Gao Games en échange de reconnaissance. Proposer des responsabilités.\n" +
      "@Note interne (manager):@ à surveiller manuellement. Activités suspectes suite à la disparition de Blaze.",
    enabled: true
  },
  {
    key: "Ally",
    about: "35 ans. Célibataire. Sans enfants. Graphic designer chez Gao Games. Top player (Gao the Athlete).",
    image: require("assets/images/community/avatars/B-Ally.png"),
    informationShared: "Accéléromètre, Contacts, Géolocalisation, Stockage, Wifi, Réseau, Notifications, SMS, Bluetooth, NFC, Alarme, Fond d’écran, Microphone, Lecteur d’empreintes digitales, Calendrier, Appareil photo",
    overview: "Efficace, organisée, peu empathique, bonne planificatrice, problèmes de santé.",
    recommendations: "Données de santé collectées (vente client #16854 – assureur médical): surpoids persistant malgré exercice régulier, 72% probabilité de problèmes thyroïdiques, risque accru d’accident cardio-vasculaire. Sujet peu rentable. Envisager résiliation de l’assurance maladie ou augmentation de la cotisation.",
    thought: {
      title: 7,
      tweet: "Les robots et les humains ",
      overview: "Les robots aussi peuvent se tromper !",
      message: "Les outils numériques prennent de plus en plus de place dans nos quotidiens et nous assistent dans un nombre toujours plus important d’activités. Sans que nous en ayons forcément conscience, des décisions ayant des conséquences sur nos vies sont prises par des machines. Ou plutôt… par les personnes qui les ont conçues ! Évidemment, ces machines sont loin d’être infaillibles : leurs programmes informatiques peuvent dysfonctionner, ou alors les données utilisées pour faire ces choix peuvent être incomplètes ou incorrectes. Dans d’autres cas encore, les personnes qui ont inventé ces programmes imaginaient que nos corps et nos usages ressembleraient aux leurs, et… https://korii.slate.fr/tech/discrimination-racisme-portiques-aeroport-femmes-noires. Tous ces problèmes potentiels peuvent aboutir au même résultat : des personnes ne peuvent pas utiliser ces outils normalement ou bénéficier des services auxquels ils donnent accès. Malheureusement, les personnes qui rencontrent le plus ces difficultés sont souvent celles déjà victimes d’autres formes d’inégalités. Par exemple, les voix féminines ou les peaux foncées sont moins bien reconnues par beaucoup de techniques numériques que les voix masculines et les peaux claires. Ennuyeux, lorsqu’on veut utiliser la commande vocale de son GPS ou une application de reconnaissance faciale ! Si cette question t’intéresse, tu peux écouter ce https://www.binge.audio/podcast/les-couilles-sur-la-table/des-ordis-des-souris-et-des-hommes/.",
      links: [
        {
          text: "se sont trompées",
          link: "https://korii.slate.fr/tech/discrimination-racisme-portiques-aeroport-femmes-noires"
        },
        {
          text: "podcast",
          link: "https://www.binge.audio/podcast/les-couilles-sur-la-table/des-ordis-des-souris-et-des-hommes/"
        }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Faire le ménage dans les autorisations déjà accordées",
              abstract: "Lorsque tu utilises une application pour la première fois, elle doit te demander la permission avant d’accéder à tes données ou aux fonctionnalités de ton téléphone. Il peut par exemple s’agir de sa géolocalisation, du microphone, de l’appareil photo, de la liste de tes contacts… Des données et des fonctions potentiellement sensibles !              Avec le temps, il peut être difficile de te souvenir des autorisations que tu as ou non accordées à telle ou telle application. Pour les vérifier et les modifier, rends-toi dans les paramètres de ton téléphone, puis dans le menu dédié aux autorisations ou aux applications.          ",
            },
            {
              title: "Où se trouve la liste des autorisations ?",
              abstract: "Tu trouveras la liste complète des applications installées sur ton appareil et les autorisations accordées à chaque application. Si tu cherches par « autorisations », tu pourras voir toutes les applications qui accèdent à une autorisation en particulier (par exemple toutes les applications qui accèdent à ta géolocalisation ou tes contacts). Depuis ces menus tu peux soit désinstaller les applications ou refuser les autorisations déjà accordées simplement en appuyant dessus. ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss4.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss5.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss6.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss7.png")
                },
              ],
            },
            {
              title: "Obtenir des informations détaillées",
              abstract: "Pour obtenir des informations encore plus détaillées sur les autorisations de ton téléphone, n’hésite pas à utiliser le « scanner » de Nikki ! (si tu ne sais pas ce que c’est, c’est qu’il te reste des choses à découvrir ici...)\n"+
                            "Cela te permettra de connaitre le nombre de pisteurs des applications installées et de suivre sur la durée tes progrès dans la protection de tes données sur le téléphone.            ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss8.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss9.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_1.pdf"),
          tooltitle: "Autorisations",
          tweet: "Pour fonctionner, certaines applications demandent la permission d’accéder à différentes informations et fonctionnalités de nos appareils. Par exemple une application « appareil photo » demande l’accès à la caméra. Parfois, ces demandes d’accès n’ont rien à voir avec le fonctionnement de l’application et ne sont destinées qu’à collecter des données personnelles nous concernant. Par exemple un lecteur de musique qui demande à avoir accès à vos contacts et vos déplacements. Voyons ensemble comment les configurer pour mieux nous protéger !",
          title: "Configurer les autorisations accordées aux applications de son téléphone",
          showCheckBoxes: true
        },
        {
          subskills: [
            {
              title: "Contrôler les usages de tes données",
              abstract: "		Il est possible de contacter les organisations privées ou publiques qui collectent, utilisent ou vendent nos données personnelles, et de leur demander des informations, corrections, suppressions ou copies de nos données.\n"+
                            "Cela concerne les services ouverts pour des résidents européens ou ses citoyens dans d’autres pays.\n"+
                            "Chaque pays l’Union européenne a son institution en charge de vérifier la bonne application de ces protections. Voici la liste par pays https://edpb.europa.eu/about-edpb/board/members_fr. Et pour les autres pays du monde, je te recommande cette https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde.                  En France, c’est la Commission nationale de l’informatique et des libertés (CNIL) se charge nous aider à faire valoir nos droits en la matière et nous propose des guides sur nos https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles et des https://www.cnil.fr/modeles/courrier de courriers pour les faire valoir facilement.                  Imaginons : en cherchant tes noms et prénoms sur internet, tu trouves une information personnelle dont tu ne souhaites pas la diffusion. La première étape pour la faire disparaître est d’écrire à l’adresse de contact indiquée sur le site en question. Une fois ta demande envoyée, les personnes qui gèrent le site ont l’obligation de te répondre en moins de 30 jours. Bien souvent, cette seule prise de contact suffit à obtenir une réponse satisfaisante.              ",
              links: [
                {
                  text: "ici",
                  link: "https://edpb.europa.eu/about-edpb/board/members_fr"
                },
                {
                  text: "carte détaillée",
                  link: "https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde"
                },
                {
                  text: "différents droits",
                  link: "https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles"
                },
                {
                  text: "modèles",
                  link: "https://www.cnil.fr/modeles/courrier"
                },
              ],
            },
            {
              title: "Déposer plainte",
              abstract:"		Si jamais cette info n’est pas supprimée après les 30 jours, tu peux envoyer https://www.cnil.fr/fr/plaintes à la CNIL qui pourra sanctionner le site en cas de faute. Malheureusement, la CNIL ne disposant que de peu de ressources (et donc de personnes pour traiter les messages, leur réponse peut mettre un certain temps. Si 30 jours se passent sans réponse de la CNIL tu peux alors https://www.service-public.fr/particuliers/vosdroits/F1435 directement auprès de la police, de la gendarmerie ou procureur de la République pour faire valoir tes droits.",
              links: [
                {
                  text: "une plainte",
                  link: "https://www.cnil.fr/fr/plaintes"
                },
                {
                  text: "porter plainte",
                  link: "https://www.service-public.fr/particuliers/vosdroits/F1435"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_2.pdf"),
          tooltitle: "Droits",
          tweet: "L’Union européenne garantit la protection des données de ses citoyen·nes et ainsi que des personnes résidant sur leur territoire. Depuis son entrée en application en 2018, le Règlement général sur la protection des données (RGPD) permet aux citoyen·nes et résident·es de l’Union européenne de mieux contrôler les usages faits de leurs données. Voyons ensemble de quoi il s’agit !",
          title: "Te protéger par delà les frontières !",
        },
        {
          subskills: [
            {
              title: "Naviguer sur le web avec le Tor Browser",
              abstract: "		Pour se protéger des mouchards et préserver son intimité, la solution la plus simple et la plus efficace est d’utiliser le https://www.torproject.org/fr/download/#android. Ce navigateur web basé sur Firefox est libre et gratuit, et est spécifiquement conçu pour contourner la surveillance et la censure.\n"+
                            "Attention tout de même : même s’il est très efficace, le Tor Browser ne dispense pas de continuer de naviguer en HTTPS ou de faire les mises à jour régulièrement. Aussi, tu peux tout de même être reconnu·e par les sites internet que tu visites dans certaines situations, par exemple si tu décides de t’identifier avec un compte.          ",
              links: [
                {
                  text: "recommandations",
                  link: "https://www.torproject.org/fr/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Naviguer sur le web avec le navigateur Duckduckgo",
              abstract: "		Si ta connexion n’est pas assez rapide ou que certains des sites que tu souhaites visiter bloquent Tor Browser, la solution alternative que je te recommande est d’utiliser le navigateur https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (disponible aussi dans le magasin d’applications F-Droid). C’est un navigateur web libre qui bloque les traqueurs et t’informe à quel point les sites que tu visites respectent ta vie privée. Attention cependant, il ne rendra pas ta connexion totalement anonyme (les sites connaitront ton adresse IP et où tu te trouves).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "		Lorsque que l’on navigue sur le web, des mouchards nous suivent de site en site pour pouvoir déduire nos goûts, nos centres d’intérêts et tout un tas d’autres choses nous concernant. Voyons comment nous protéger !",
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Pistage",
          title: "Se protéger des mouchards qui suivent nos navigations",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Fred" },
  {
    key: "Amin",
    about: "Entre 36 et 43 ans. Marié (conjoint user#6874215, username «Masako»). 1 enfant en bas âge («Alma»). Ingénieur qualité. Top player (Gao the Filekeeper).",
    image: require("assets/images/community/avatars/B-Amin.png"),
    informationShared: "Stockage, SMS, Alarme, Microphone, Calendrier, Appareil photo, Contacts, Géolocalisation, Wifi, Réseau",
    overview: "Calme, consciencieux, respectueux des lois, engagé avec un prêt bancaire (études), fille malade (maladie dégénérative), anxieux, nostalgique, rêveur.",
    recommendations: "Données d’état émotionnel collectées (vente client #03548 – régie publicitaire): sujet sous stress (46% maladie de sa fille, 28% gestion difficile avec sa femme, 26% surcharge de travail) émotionnellement réceptif. Envies de voyage. 79% probabilité d’acheter des prestations de voyage touristique (type «évasion»).",
    thought: {
      title: 8,
      tweet: "Manipulation",
      overview: "Des interfaces conçues pour nous tromper et nous manipuler",
      message: "Sur Internet et jusque dans nos téléphones, beaucoup d’entreprises ont recours à ce que l’on appelle des interfaces truquées, soigneusement conçues pour nous manipuler – en anglais, on les nomme des dark patterns. Interruptions, présentations et choix des couleurs volontairement trompeurs, options présélectionnées, informations difficilement repérables… toutes ces astuces ergonomiques sont destinées à nous faire accepter des choses que nous aurions probablement refusées si elles avaient été présentées clairement. Et ces entreprises ont même recours à des procédés reposant sur notre fonctionnement biologique. En activant dans notre cerveau des mécanismes liés aux processus du plaisir et de la récompense, ces applications et services nous donnent envie d’être utilisés toujours plus longtemps, parfois jusqu’à provoquer de véritables addictions. Pire encore : à partir des nombreuses données qu’elles collectent sur nous, des entreprises établissent des profils précis pour déceler nos faiblesses et nous pousser à surconsommer. Par exemple, elles tentent de déterminer les moments où nous sommes fatigué·es ou ceux où nous manquons de confiance en nous, et elles en profitent pour nous faire acheter des choses que nous aurions refusées le reste du temps. J’aiparticipé à cette manipulation avec les Gao Games, c’était une erreur de ma part. Pour en savoir plus là-dessus, tu peux regarder cette https://www.arte.tv/fr/videos/RC-017841/dopamine/ de vidéos d’ARTE, cet https://www.internetactu.net/2016/06/16/du-design-de-nos-vulnerabilites/ du site InternetActu.net et cette https://linc.cnil.fr/dark-patterns-quelle-grille-de-lecture-pour-les-reguler de la Commission nationale de l’informatique et des libertés (CNIL).",
      links: [
        {
          text: "minisérie",
          link: "https://www.arte.tv/fr/videos/RC-017841/dopamine/"
        },
        {
          text: "article",
          link: "https://www.internetactu.net/2016/06/16/du-design-de-nos-vulnerabilites/"
        },
        {
          text: "publication",
          link: "https://linc.cnil.fr/dark-patterns-quelle-grille-de-lecture-pour-les-reguler"
        },
      ],
      skills: [
        {
          subskills: [
            {
              abstract: "Pour cela, je te recommande d’ajouter l’extension https://ublockorigin.com/ à ton navigateur web, et d’installer les applications https://play.google.com/store/apps/details?id=org.blokada.alarm.dnschanger et/ou https://f-droid.org/fr/packages/org.adaway/ (si tu utilises le magasin d’applications F-Droid).\n"+
                            "Une fois l’une de ces applications installées, il ne te restera plus qu’à la lancer et à activer sa protection (tu verras, c’est très simple). Et pour en savoir plus sur comment bloquer la publicité sur ton ordinateur, je te suggère de visiter le site https://bloquelapub.net/              ",
              title: "Installer un bloqueur de pub",
              links: [
                {
                  text: "uBlock Origin",
                  link: "https://ublockorigin.com/"
                },
                {
                  text: "Blokada",
                  link: "https://play.google.com/store/apps/details?id=org.blokada.alarm.dnschanger"
                },
                {
                  text: "AdAway",
                  link: "https://f-droid.org/fr/packages/org.adaway/"
                },
                {
                  text: "bloquelapub.net",
                  link: "https://bloquelapub.net/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss12.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss13.png")
                },
              ],
            },
          ],
          tweet: "La publicité peut-être la porte ouverte vers des sites malveillants ou l’installation de virus sur nos appareils. Elle pose aussi bien d’autres problèmes comme la pollution informatique, ralentissement de nos navigations, augmentation de la consommation d’énergie, diffusion de stéréotypes, encouragement de la surconsommation. Heureusement, il est plutôt simple de s’en débarrasser sur son téléphone (et ordinateurs) !",
          title: "Bloquer la publicité",
          //pdf: require("assets/pdf/Amin_1.pdf"),
          tooltitle: "Publicité",
        },
        {
          subskills: [
            {
              abstract: "Plus nous passons du temps sur les outils des géants du Net, plus la quantité de données qu’ils collectent sur nous augmente, et plus leurs profits deviennent importants. Mais aussi le risque que nous développions des comportements addictifs et compulsifs.\n"+
                            "Le comble est que ces entreprises tentent ensuite de nous faire croire que c’est de notre faute et de https://www.blogdumoderateur.com/encourager-addiction-promouvoir-deconnexion/ ! Pour en savoir plus, je te recommande de lire les http://www.internetactu.net/2019/01/14/retro-design-de-lattention-cest-complique/ du travail de la Fondation Internet Nouvelle Génération (Fing) sur ce sujet.\n"+
                            "Reconnaître les mécanismes qui exploitent notre inconscient est compliqué : ils sont conçus pour être difficiles à identifier. Je t’invite à regarder sans modération la mini série d’Arte  « Dopamine » où nous comprenons comment cette hormone est stimulée par certaines des applications présentes sur nos téléphone. Comprendre, c’est déjà agir !          Y parvenir et réussir à les contourner est tout de même vraiment important pour garder le contrôle et un rapport sain aux outils numériques. Voici quelques solutions !          ",
              title: "Comprendre cet enjeu",
              links: [
                {
                  text: "nous faire culpabiliser",
                  link: "https://www.blogdumoderateur.com/encourager-addiction-promouvoir-deconnexion/"
                },
                {
                  text: "conclusions",
                  link: "http://www.internetactu.net/2019/01/14/retro-design-de-lattention-cest-complique/"
                },
              ],
            },
            {
              abstract: "Avant tout, commence par faire le point avec toi-même : que veux-tu faire avec cet appareil ? Quels usages connectés trouves-tu utiles et intéressants ? Lesquels préférerais-tu arrêter ? Quels effets négatifs veux-tu limiter ? Quelle place veux-tu laisser dans ta vie à ces objets… ?\n"+
                            "Évidemment, les réponses à ces questions ne seront pas les mêmes pour tout le monde : certain·es personnes trouveront un service en ligne vraiment enrichissant, quand d’autres n’y verront qu’une perte de temps. Peu importe : l’important est que tu répondes honnêtement à ces questions et que tu trouves les réponses qui te conviennent, afin de pouvoir réorienter ton attention vers ce que tu juges en valoir réellement la peine.\n"+
                            "La première des habitudes à adopter est d’éviter la précipitation et de prendre le temps de vraiment lire ce que les sites et applications te proposent. Plus un site ou un service te presse de prendre une décision, plus il est important de t’assurer d’avoir compris ce sur quoi tu t’apprêtes à appuyer ou cliquer.\n"+
                            "Une autre bonne habitude à adopter est de déterminer des moments et des endroits où tu choisis de ne pas utiliser tes appareils numériques. Par exemple, dans la chambre où tu dors, ou seulement dans ton lit, dans la cuisine, pendant les repas, avant d’aller te coucher, lorsque tu es avec quelqu’un… Tu peux même en discuter avec tes proches pour décider des règles communes.              ",
              title: "Faire le point et prendre de bonnes habitudes",
            },
            {
              abstract: "Un bon réflexe est de systématiquement te méfier des suggestions automatiques et des invitations à consulter une application, surtout lorsqu’elles utilisent le registre de l’émotion. Pour pouvoir refuser ce que l’appareil te suggère de faire, commence par prendre le temps de te demander si tu en as vraiment envie.\n"+
                            "Il existe aussi une solution technique pour empêcher ton téléphone de te déranger et pour t’aider à le consulter seulement lorsque tu l’as décidé : désactiver les notifications. Pour les désactiver complètement, rends-toi dans les Paramètres de ton téléphone, puis dans le menu Notifications et barre d’état et Invite de notification. Enfin, sélectionne Pas d’invite.          ",
              title: "Désactiver toutes les notifications",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss14.png")
                },
              ],
            },
            {
              abstract: "Si tu le préfères, il est aussi possible de choisir un réglage plus fin et d’autoriser ou d’interdire les notifications application par application. Pour cela, rends-toi dans les Paramètres du téléphone, puis dans le menu Notifications et barre d’état et dans Gérer les notifications. Tu y trouveras la liste des applications installées sur ton appareil. En appuyant sur l’une d’elles, tu pourras décocher Autoriser les notifications ou les configurer précisément.",
              title: "Affiner ta configuration",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss15.png")
                },
              ],
            },
            {
              abstract: "Une autre manière de faire est d’activer le mode Ne pas déranger pour un moment. Pour cela, place ton doigt sur la barre de statuts, glisse-le vers le bas, et appuis sur l’icône Ne pas déranger.\n"+
                            "Il est aussi possible d’activer automatiquement ce mode pendant les plages horaires et les jours de ton choix (par exemple pour pouvoir te réveiller et te coucher tranquillement), ainsi que de décider des éléments bloqués ou autorisés (par exemple, les appels de certains contacts ou bien les appels répétés). Pour cela, rends-toi dans les Paramètres du téléphone puis dans le menu Ne pas déranger.          ",
              title: "Utiliser le mode « Ne pas déranger »",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss16.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss17.png")
                },
              ],
            },
            {
              abstract: "Une autre méthode pour garder le contrôle est de limiter l’usage de ton appareil ou de certaines applications à un temps déterminé. Une fois le temps écoulé, l’application en question sera désactivée pour le reste de la journée.\n"+
                            "Évidemment, cet indicateur http://www.internetactu.net/2019/01/15/retro-design-de-lattention-depasser-le-temps/. À toi de choisir ce que tu juges utile et important ! Pour mettre en place ces réglages, rends-toi dans les Paramètres du téléphone, puis dans le menu Bien-être numérique et contrôles parentaux et appuie sur Tableau de bord dans la partie Pour déconnecter.          Dans ce menu, tu peux mesurer le temps que tu passes sur ton appareil et le temps à utiliser chacune des applications. C’est aussi là que tu peux configurer le mode Coucher, qui permet de passer ton écran en noir et blanc, afin qu’il attire moins ton attention. Toujours dans ce menu, tu peux également configurer le mode Sans distractions, qui ressemble au réglage Ne pas déranger que nous avons déjà vu plus haut.          ",
              title: "Limiter ton temps d’usage",
              links: [
                {
                  text: "mesure le temps passé à utiliser une application, mais pas son utilité",
                  link: "http://www.internetactu.net/2019/01/15/retro-design-de-lattention-depasser-le-temps/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss18.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss19.png")
                },
              ],
            },
            {
              abstract: "Évidemment, nous pouvons imaginer encore des tas d’autres solutions : ranger les applications les moins utiles loin de l’écran d’accueil, choisir un forfait avec une connexion limitée, t’imposer des pauses, mettre un fond d’écran qui t’encourage à te demander si tu souhaites vraiment utiliser ton appareil, configurer ton écran pour qu’il soit en noir et blanc...Certains fonctionneront sur certaines personnes mais pas sur d’autres.\n"+
                            "Le site https://datadetoxkit.org/fr/wellbeing/essentials#step-1 propose de nombreuses autres solutions. Et si ces appareils prennent vraiment plus de place dans ta vie que tu ne le souhaites, n’hésite pas à en parler à tes proches ou à un·e addictologue.          ",
              title: "Trouver une solution sur mesure",
              links: [
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/fr/wellbeing/essentials#step-1"
                },
              ],
            }
          ],
          tweet: "Pour accaparer toujours plus notre attention et donc nos données, les géants du Net exploitent jusqu’au fonctionnement de notre inconscient et nos cerveaux. C’est que que l’on appelle les « biais cognitifs et psychologiques ». Voyons comment nous protéger !",
          title: "Reconnaître et contourner les mécanismes qui nous manipulent",
          //pdf: require("assets/pdf/Amin_2.pdf"),
          tooltitle: "Manipulation",
        },
        {
          subskills: [
            {
              title: "Naviguer sur le web avec le Tor Browser",
              abstract: "		Pour se protéger des mouchards et préserver son intimité, la solution la plus simple et la plus efficace est d’utiliser le https://www.torproject.org/fr/download/#android. Ce navigateur web basé sur Firefox est libre et gratuit, et est spécifiquement conçu pour contourner la surveillance et la censure.\n"+
                            "Attention tout de même : même s’il est très efficace, le Tor Browser ne dispense pas de continuer de naviguer en HTTPS ou de faire les mises à jour régulièrement. Aussi, tu peux tout de même être reconnu·e par les sites internet que tu visites dans certaines situations, par exemple si tu décides de t’identifier avec un compte.          ",
              links: [
                {
                  text: "recommandations",
                  link: "https://www.torproject.org/fr/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Naviguer sur le web avec le navigateur Duckduckgo",
              abstract: "		Si ta connexion n’est pas assez rapide ou que certains des sites que tu souhaites visiter bloquent Tor Browser, la solution alternative que je te recommande est d’utiliser le navigateur https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (disponible aussi dans le magasin d’applications F-Droid). C’est un navigateur web libre qui bloque les traqueurs et t’informe à quel point les sites que tu visites respectent ta vie privée. Attention cependant, il ne rendra pas ta connexion totalement anonyme (les sites connaitront ton adresse IP et où tu te trouves).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "		Lorsque que l’on navigue sur le web, des mouchards nous suivent de site en site pour pouvoir déduire nos goûts, nos centres d’intérêts et tout un tas d’autres choses nous concernant. Voyons comment nous protéger !",
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Pistage",
          title: "Se protéger des mouchards qui suivent nos navigations",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "John" },
  { key: "Judith" },
  { key: "Bill" },
  {
    key: "Nikki",
    enabled: true,
    selected: true,
    about: "Eliott Anderson. 123 ans. Divorcé. 47 enfants. Dresseur d’éléphants. Couleur préférée : chocolat banane. Top player (Gao the Spy).",
    image: require("assets/images/community/avatars/B-Nikki.png"),
    recommendations: "Données collectées non exploitables.",
    overview: "null",
    informationShared: "null",
  },
  { key: "Bob" },
  { key: "Eugene" },
  { key: "Maria" },
  { key: "Mariane" },
  { key: "Billy" },
  { key: "Devin" },
  { key: "Dan" },
  {
    key: "Rokaya",
    about: "26 ans. Célibataire. Sans enfants. Coloriste / influenceuse. Top player (Gao the Photographer).",
    image: require("assets/images/community/avatars/B-Rokaya.png"),
    informationShared: "Appareil photo, Stockage, Accéléromètre, Contacts, Géolocalisation, Wifi, Réseau, Bluetooth, NFC, Fond d’écran, Microphone, Calendrier",
    overview: "Influenceuse, artiste, sûre d’elle, dynamique, populaire.",
    recommendations: "Recommandations publicitaires (vente client #69755 – annonceur publicitaire): utilisatrice très populaire sur les réseaux sociaux. Contrat signé pour la vente d’espaces publicitaires sponsorisés dans ses publications (selon conditions générales d’utilisation).\n" +
      "@Note interne 1:@ attention, l’utilisatrice a contacté le service publicitaire pour se plaindre de l’association avec un de nos annonceurs produit. Elle explique que cette marque est liée à un scandale «d’enfants travailleurs esclaves», et ne souhaite pas soutenir leur activité. En attente de réponse.\n" +
      "@Note interne 2 (réponse manager):@ ignorer la requête de l’utilisatrice, le contrat nous autorise à faire tous types d’associations publicitaires avec ses photos.",
    thought: {
      title: 11,
      tweet: "Des outils à notre service ? ",
      overview: "Des outils à notre service ? Vraiment ?",
      message: "Avoir recourt aux services en ligne des « Géants du Net » pour publier et partager des contenus présente quelques avantages : ils sont relativement simples d’utilisation, souvent gratuits et très populaires. Mais les services proposés par ces entreprises répondent d’abord et avant tout à… leur propre intérêt. Et non pas à celui des gens qui s’en servent. C’est évidemment le cas pour les questions liées à la surveillance, mais pas uniquement. Utiliser ces services nous rend dépendant·e des décisions des personnes qui les conçoivent. Selon leurs envies et objectifs, elles peuvent effacer notre compte ou certaines de nos publications, en limiter l’accès, ajouter ou supprimer des fonctionnalités, ne pas nous montrer certaines publications ou au contraire ne mettre en avant que certaines opinions… ou même vendre l’activité à une autre société, ou carrément l’arrêter. Des pouvoirs très importants, surtout quand vraiment beaucoup de gens utilisent ces services. Heureusement, il existe divers choix possibles qui tournent tout aussi bien et nous redonnent la capacité de décider ! L’association Framasoft propose par exemple de nombreux outils alternatifs à ceux des géants du Net sur le site https://degooglisons-internet.org/fr/.",
      links: [
        {
          text: "degooglisonsinternet.org",
          link: "https://degooglisons-internet.org/fr/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Utiliser un autre magasin",
              abstract: "Pour contourner cette entreprise prédatrice et reprendre le contrôle de tes données, il existe des magasins plus éthiques, libres et gratuits, comme l’https://f-droid.org/fr/packages/com.aurora.store/. Celui-ci permet de télécharger les mêmes applications que le Play Store, mais sans avoir besoin de compte Google et sans collecter tes données personnelles pour cela.\n"+
                            "Je te recommande  particulièrement d’utiliser https://f-droid.org/fr/. Il ne te propose que des applications gratuites, libres et plus respectueuses de tes données. Évidemment, tu n’y retrouveras donc pas toutes les applications des autres magasins ! Mais je parie que des alternatives à celle que tu recherches existent déjà et que tu en trouveras vite une qui te conviendra.          ",
              links: [
                {
                  text: "Aurora Store",
                  link: "https://f-droid.org/fr/packages/com.aurora.store/"
                },
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss20.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss21.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss22.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss23.png")
                },
              ],
            },
            {
              title: "Comment les installer ?",
              abstract: "Pour utiliser ces magasins, tu dois les autoriser dans le menu permettant l’installation d’applications depuis des sources inconnues. Pour cela, rends-toi dans les paramètres de ton téléphone, puis dans le menu Sécurité ou Appareil et confidentialité (selon ta version d’Android).",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss24.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss25.png")
                },
              ],
            },
            {
              title: "Et télécharger les applis sur des sites ?",
              abstract: "Enfin, je te déconseille fortement d’installer des applications sans passer par 	un magasin dédié (avec un fichier .apk). Cette pratique est dangereuse et te fait courir le risque d’installer des applications modifiées par des personnes 	malveillantes.",
            }
          ],
          //pdf: require("assets/pdf/Rokaya_1.pdf"),
          tooltitle: "Magasin",
          tweet: "Beaucoup de personnes utilisent le magasin d’applications de Google, le Google Play Store pour choisir et installer leurs applications. Mais ce n’est pas la seule solution !",
          title: "Changer de magasin d’applications",
        },
        {
          subskills: [
            {
              title: "Visiter des annuaires",
              abstract: "Pour nous aider à nous y retrouver parmi toutes les solutions existantes, des sites comme https://prism-break.org/fr/categories/android/ et https://degooglisons-internet.org/fr/ nous présentent des listes de services et logiciels éthiques. En parcourant ces pages, nous avons des propositions concrètes pour les remplacer.",
              links: [
                {
                  text: "PRISM-Break.org",
                  link: "https://prism-break.org/fr/categories/android/"
                },
                {
                  text: "Dégooglisons Internet",
                  link: "https://degooglisons-internet.org/fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss26.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss27.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss28.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss29.png")
                },
              ],
            },
            {
              title: "Trouver un hébergeur",
              abstract:"Je t’invite également à jeter un œil au Collectif des hébergeurs alternatifs transparents ouverts neutres et solidaires, les https://entraide.chatons.org/fr. Il rassemble des structures proposant des services en ligne éthiques et répondant à des critères exigeants. Avec un peu de chance, tu pourras peut-être en trouver un https://chatons.org/fr/find-by-localisation !",
              links: [
                {
                  text: "CHATONS",
                  link: "https://entraide.chatons.org/fr"
                },
                {
                  text: "près de chez toi",
                  link: "https://chatons.org/fr/find-by-localisation"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss30.png")
                },
              ],
            },
            {
              title: "Connaître les critères importants",
              abstract:"De manière générale, voici 3 critères indispensables pour qu’un service en ligne soit sécurisé et protecteur. Il doit être :\n"+
                            "•	Libre : c’est-à-dire que tout le monde peut étudier son fonctionnement, le modifier pour l’adapter à ses envies, puis partager le résultat. Ainsi, nous pouvons être plus sûr·es que le service fait bien ce qu’il prétend, et rien d’autre. Pour en savoir plus sur cet écosystème, je t’invite à consulter le travail de l’https://www.april.org/fr/articles/intro.\n"+
                            "•	Chiffré de bout en bout : grâce à cette méthode de protection des données, seules les personnes à qui elles sont destinées peuvent réellement le faire. Et personne d’autre. Pas même les personnes qui administrent le service en question.\n"+
                            "•	Décentralisé : cela signifie que l’outil numérique ne doit pas dépendre d’une seule autorité, qui pourrait imposer à tout le monde ses décisions du jour au lendemain. Ainsi, il s’agit d’un bien commun : il appartient à tout le monde et personne ne peut se l’approprier au détriment des autres.\n"+
                            "Pour aller plus loin, je te recommande la lecture du guide https://ssd.eff.org/fr/module/choisir-vos-outils proposé par le site Surveillance Self-Defense.              ",
              links: [
                {
                  text: "association April",
                  link: "https://www.april.org/fr/articles/intro"
                },
                {
                  text: "Choisir vos outils",
                  link: "https://ssd.eff.org/fr/module/choisir-vos-outils"
                },
              ]
            }
          ],
          //pdf: require("assets/pdf/Rokaya_2.pdf"),
          tooltitle: "Remplacer",
          tweet: "Trouver un outil numérique correspondant réellement à ses besoins, envies et valeurs peut-être compliqué. Voyons comment faire en pratique !",
          title: "Rechercher un service en ligne éthique",
        },
        {
          subskills: [
            {
              title: "Comprendre cet enjeu",
              abstract: "Bien qu’on les accepte mécaniquement, ces cases que l’on coche avant d’accéder à un service peuvent avoir des conséquences importantes, notamment pour la protection de nos données et la propriété des contenus que nous produisons (nos commentaires, publications, vidéos…).\n"+
                            "Malheureusement, les CGU de la plupart des services en ligne sont tout simplement https://www.vice.com/fr/article/xwbg7j/la-plupart-des-termes-dutilisation-en-ligne-sont-incomprehensibles et parfois même illicites : https://www.numerama.com/tech/675927-pourquoi-google-affiche-jugement-du-tgi-de-paris-sur-sa-page-daccueil.html, https://www.nextinpact.com/article/29309/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement, https://www.lemonde.fr/pixels/article/2020/06/15/apple-condamne-en-france-pour-les-conditions-abusives-d-itunes-et-apple-music_6042948_4408996.html et bien d’autres ont même déjà été condamnés pour cela. De plus, ces entreprises s’accordent généralement la possibilité de modifier ces contenus quand bon leur semble.          ",
              links: [
                {
                  text: "incompéhensibles",
                  link: "https://www.vice.com/fr/article/xwbg7j/la-plupart-des-termes-dutilisation-en-ligne-sont-incomprehensibles"
                },
                {
                  text: "Google",
                  link: "https://www.numerama.com/tech/675927-pourquoi-google-affiche-jugement-du-tgi-de-paris-sur-sa-page-daccueil.html"
                },
                {
                  text: "Facebook",
                  link: "https://www.nextinpact.com/article/29309/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement"
                },
                {
                  text: "Apple",
                  link: "https://www.lemonde.fr/pixels/article/2020/06/15/apple-condamne-en-france-pour-les-conditions-abusives-d-itunes-et-apple-music_6042948_4408996.html"
                },
              ],
            },
            {
              title: "Trouver les informations essentielles",
              abstract: "Heureusement, certains services en ligne sont tout de même plus vertueux que ces géants du Net. Les membres du projet https://tosdr.org/ (ToS;DR), que l’on pourrait traduire par « CGU : pas lues », ont pris la peine de résumer et d’expliquer ces contrats d’une manière compréhensible. Leur site internet n’est pas encore traduit en français, il peut tout de même nous aider à choisir les services que nous souhaitons utiliser !\n"+
                            "L’outil https://disinfo.quaidorsay.fr/fr/cgus permet quant à lui de suivre et de retrouver les évolutions des CGU des principaux fournisseurs de services en ligne. Pratique si l’on veut comparer les différentes versions dans le temps !              ",
              links: [
                {
                  text: "Terms of Service; Didn't Read",
                  link: "https://tosdr.org/"
                },
                {
                  text: "Open Terms Archive",
                  link: "https://disinfo.quaidorsay.fr/fr/cgus"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss31.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss32.png")
                },
              ],
            }
          ],
          tweet: "Pour utiliser un service en ligne, il faut généralement accepter ses conditions générales d’utilisation (ou CGU) même dans les cas où l’on ne peut de toutes façons pas les refuser ! Voyons voir comment nous y retrouver dans ces documents compliqués.",
          //pdf: require("assets/pdf/Rokaya_3.pdf"),
          tooltitle: "Contrats",
          title: "Tenter de comprendre les contrats qui nous lient aux services en ligne",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Dominic" },
  { key: "Jackson" },
  { key: "Sebastian" },
  { key: "Egaeus nobot" },
  { key: "James" },
  {
    key: "Masako",
    about: "Entre 35 et 42 ans. Mariée (conjoint user#8765489, username «Amin»). 1 enfant en bas âge («Alma»). Ingénieure en matériaux. Top player (Gao the Forger).",
    image: require("assets/images/community/avatars/B-Masako.png"),
    informationShared: "Fond d’écran, Stockage, SMS, Accéléromètre, Microphone, Calendrier, Appareil photo, Contacts, Géolocalisation, Wifi, Réseau",
    overview: "Énergique, aventureuse, indépendante, créative, persévérante, fille malade (maladie dégénérative), impliquée.",
    recommendations: "Données d’employabilité collectées (vente client #35468 – réseau social et recruteur professionnel): changement récent d’orientation. L’analyse des échanges de messages et des recherches en ligne indique une baisse de motivation professionnelle (passage à temps partiel, baisse des conversations orientées business) et une hausse des préoccupations familiales (consultation significative de photos d’enfants, tensions avec son mari). Indiquer aux recruteurs RH une baisse de 27% du score d’employabilité.",
    thought: {
      title: 10,
      tweet: "Données collectives ? ",
      overview: "Données personnelles ou données collectives ?",
      message: "Bien que l’on parle généralement de « données personnelles », une grande partie de ce que les « Géants du Net » exploitent commercialement sont des données collectives. Il s’agit de ce que l’on appelle des graphes sociaux (ou réseaux sociaux) : les connexions et relations entre les personnes. En parvenant à collecter des informations sur les opinions et goûts de  quelqu’un, ces entreprises peuvent facilement déduire ceux de son entourage. Et comme les communications impliquent par nature plusieurs personnes, leur  confidentialité dépend des choix de l’ensemble des participant·es à la conversation. Si un·e seul·e destinataire de nos messages se soucie moins de la confidentialité des échanges que les autres, même les outils les plus protecteurs ont beaucoup moins d’efficacité. Dans ces conditions, il importe d’avoir en tête la préservation des données de nos proches lorsque nous utilisons des services en ligne ou des applications qui pourraient nous surveiller. Mais aussi quand nous publions des informations ! Notamment, en se  demandant si les proches visibles sur nos photographies sont réellement  d’accord pour qu’elles soient mises sur Internet ou partagées. Et cela ne concerne pas que les êtres humains : des braconniers https://qz.com/206069/geotagged-safari-photos-could-lead-poachers-right-to-endangered-rhinos/ [en] par exemple les métadonnées d’images publiées sur Internet par des touristes pour localiser des animaux et les chasser. Pour en savoir plus sur la dimension collective des infos « personnelles », tu peux lire cet https://scinfolex.com/2018/05/25/rgpd-la-protection-de-nos-vies-numeriques-est-un-enjeu-collectif-tribune-le-monde/.",
      links: [
        {
          text: "utilisen",
          link: "https://qz.com/206069/geotagged-safari-photos-could-lead-poachers-right-to-endangered-rhinos/"
        },
        {
          text: "article",
          link: "https://scinfolex.com/2018/05/25/rgpd-la-protection-de-nos-vies-numeriques-est-un-enjeu-collectif-tribune-le-monde/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Choisir un mot de passe",
              abstract: "Que tu choisisses d’utiliser un schéma, une suite de chiffres ou un mot de passe, l’une des mesures les plus importantes pour protéger ses données est de configurer un code de déverrouillage. Après chaque extinction de l’écran, il sera nécessaire de l’entrer pour pouvoir accéder aux données contenues dans l’appareil.\n"+
                            "Pour le configurer ou le modifier, rends-toi dans les Paramètres de ton téléphone, puis dans le menu Empreinte, visage & mot de passe. Pour régler le délai avant l’extinction automatique de l’écran en cas d’inactivité, rends-toi dans le menu Affichage et luminosité.\n"+
                            "Pour plus de sécurité, je te recommande d’éviter les méthodes biométriques et les dessins ou codes trop faciles à deviner (comme la forme de la première lettre de ton prénom ou ta date de naissance). Pour en savoir plus, voici les https://www.ssi.gouv.fr/guide/mot-de-passe/ de l’Agence nationale de la sécurité des systèmes informatiques (ANSSI) pour choisir des mots de passe robustes et le https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut pour utiliser un gestionnaire de mots de passe proposé par le site NextINpact.com.          ",
              links: [
                {
                  text: "recommandations",
                  link: "https://www.ssi.gouv.fr/guide/mot-de-passe/"
                },
                {
                  text: "guide",
                  link: "https://www.nextinpact.com/article/24004/101627-mots-passe-on-vous-aide-a-choisir-gestionnaire-quil-vous-faut"
                },
              ],
            },
            {
              title:"Copier ton code IMEI",
              abstract:"Le code IMEI est un numéro permettant d’identifier de manière unique ton téléphone. Tu peux l’obtenir en composant le code *#06#. Tu le trouveras également sur la boite et la facture de ton appareil. Conserve-le précieusement : il te serait demandé pour définitivement bloquer ton appareil en cas de vol.",
            },
            {
              title:"Garder ton téléphone à jour",
              abstract:"Installe les mises à jour chaque fois que possible pour garder tes applications et ton appareil sécurisés. En plus des nouvelles fonctionnalités, elles comportent souvent des corrections de failles et de bugs. Pour être sûr·e de n’en rater aucune, le plus simple est d’activer les mises à jour automatiques.\n"+
                            "Pour cela, rends-toi dans les Paramètres de ton magasin d’applications, puis dans le menu Mises à jour si tu utilises le Play Store. Ici, appuie sur Activer les mises à jour automatiques ou Récupérer automatiquement les mises à jour, et configure la fréquence de la vérification.          ",
            },
            {
              title:"Chiffrer ton téléphone",
              abstract:"Depuis quelques années, la plupart les téléphones proposent de chiffrer les données qu’ils contiennent. Cela signifie que seule la personne qui connaît le mot de passe permettant de déverrouiller l’appareil peut accéder à ces données.\n"+
                        "En fonction de la version d’Android installée sur ton téléphone, le processus et la méthode pourront varier. Au cas où l’option ne serait pas déjà activée par défaut et si elle est disponible, tu peux chiffrer la mémoire de ton téléphone ou celle de ta carte micro SD. Pour cela, rends-toi dans les Paramètres du téléphone, puis dans le menu Sécurité ou équivalent.\n"+
                        "Attention tout de même ! Pour une carte SD, l’opération implique de commencer par supprimer toutes les données qu’elle contient. Et dans tous les cas, si tu perdais le mot de passe, tes données seraient vraiment perdues aussi.          ",
            },
            {
              title:"Ne pas oublier les métadonnées",
              abstract:"Lorsque nous prenons une photographie, notre téléphone y incorpore automatiquement des métadonnées. Il s’agit d’informations telles que la date, la géolocalisation, le réglage de l’appareil…  Toutes ces informations peuvent révéler beaucoup de chose sans que nous le souhaitions.\n"+
                            "Ainsi, pense à les vérifier avant de partager tes photographies, ou carrément à les effacer. Pour cela, tu peux utiliser l’application Scrambled Exif (avec https://f-droid.org/fr/packages/com.jarsilio.android.scrambledeggsif/ ou le https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif).          ",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/fr/packages/com.jarsilio.android.scrambledeggsif/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif"
                },
              ],
            },
            {
              title:"Désactiver les fonctionnalités inutiles",
              abstract:"Lorsque tu ne les utilises pas, désactive la géolocalisation, le wifi et le bluetooth. En prenant cette habitude, tu limiteras la possibilité de collecter des données sur toi et ton appareil et tu amélioreras sa sécurité et l’autonomie de sa batterie. Pour cela, place ton doigt sur la barre de statuts, glisse-le vers le bas, et appuis sur les icônes correspondantes.",
            },
            {
              title:"Préférer les sites aux applications",
              abstract:"De manière générale, mieux vaut utiliser le site internet d’un service en ligne que son application dédiée : tu limiteras les données auxquelles il pourra accéder sur ton téléphone.",
            },
            {
              title:"Remettre à zéro tes nouveaux et tes anciens appareils",
              abstract:"Si tu récupères un téléphone ou que tu te sépares du tien, n’oublie pas d’effacer toutes les données qu’il contient. Ce petit https://www.cnil.fr/fr/effacer-ses-donnees-dun-ordinateur-dun-telephone-ou-dune-tablette-avant-de-sen-separer de la Commission nationale de l’informatique et des libertés (CNIL) nous explique comment faire en pratique.",
              links: [
                {
                  text: "guide",
                  link: "https://www.cnil.fr/fr/effacer-ses-donnees-dun-ordinateur-dun-telephone-ou-dune-tablette-avant-de-sen-separer"
                },
              ],
            },
            {
              title:"Faire le point régulièrement",
              abstract:"Aucune solution informatique n’est fiable ni à 100 %, ni pour toujours. N’oublie pas de garder un œil sur le sujet et de te demander de temps en temps si les choix que tu as faits sont toujours pertinents et s’ils te conviennent toujours.",
            },
            {
              title:"Ne pas oublier les détails",
              abstract:"En matière de sécurité informatique, la solidité d’un ensemble dépend de son maillon le plus faible : il peut suffire d’un petit détail pour que l’ensemble soit compromis. Ainsi, il faut aussi faire attention à ce qui peut sembler être de petits détails.\n"+
                            "Par exemple, je te recommande de remplacer le clavier installé par défaut (Gboard) sur ton téléphone par un clavier libre et respectueux de tes données. Celui que je conseille est OpenBoard, que tu peux installer depuis les magasins d’applications https://f-droid.org/fr/packages/org.dslul.openboard.inputmethod.latin/ ou https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin. Pour l’utiliser, rends-toi ensuite dans les Paramètres du téléphone, puis dans Autres paramètre, puis Clavier et méthode de saisie pour choisir OpenBoard et pouvoir le configurer.          ",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/fr/packages/org.dslul.openboard.inputmethod.latin/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin"
                },
              ],
            },
            {
              title:"Adopter une attitude respectueuse",
              abstract:"Au-delà de toutes ces recommandations, protéger tes données et celles de tes proches passe aussi par l’adoption d’attitudes respectueuses les un·es envers les autres. Il s’agit par exemple de ne pas oublier de demander à la personne concernée si l’on peut partager des informations à son sujet, de ne pas tout enregistrer par défaut et de minimiser la quantité de données que l’on produit, de s’entraider, de discuter et partager ses découvertes…\n"+
                            "Et pour arriver à des solutions réellement satisfaisantes et ne laisser personne au bord du chemin, il est nécessaire de nous organiser collectivement pour améliorer les lois et les outils numériques que nous utilisons pour nous protéger. Si l’aventure t’intéresse ou simplement pour suivre l’actualité en la matière, c’est le https://laquadrature.net/ de La Quadrature du Net que je te conseille d’aller lire.\n"+
                            "Pour aller plus loin, tu trouveras de nombreux autres recommandations sur le site https://datadetoxkit.org/fr/home et dans le très complet https://guide.boum.org/.          ",
              links: [
                {
                  text: "site",
                  link: "https://laquadrature.net/"
                },
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/fr/home"
                },
                {
                  text: "guide d’autodéfense numérique",
                  link: "https://guide.boum.org/"
                },
              ],
            }
          ],
          tweet: "En plus de tous les autres savoir-faire, quelques gestes simples permettent d’améliorer facilement et rapidement la sécurité de nos téléphones. Certaines personnes en parlent même comme de règles d’hygiène numérique, pour établir un parallèle avec les règles de base de l’hygiène corporelle.",
          //pdf: require("assets/pdf/Ajay_3.pdf"),
          tooltitle: "Protéger",
          title: "Protéger nos données et celles de nos proches",
        },
        {
          subskills: [
            {
              title: "Visiter des annuaires",
              abstract: "Pour nous aider à nous y retrouver parmi toutes les solutions existantes, des sites comme https://prism-break.org/fr/categories/android/ et https://degooglisons-internet.org/fr/ nous présentent des listes de services et logiciels éthiques. En parcourant ces pages, nous avons des propositions concrètes pour les remplacer.",
              links: [
                {
                  text: "PRISM-Break.org",
                  link: "https://prism-break.org/fr/categories/android/"
                },
                {
                  text: "Dégooglisons Internet",
                  link: "https://degooglisons-internet.org/fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss26.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss27.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss28.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss29.png")
                },
              ],
            },
            {
              title: "Trouver un hébergeur",
              abstract:"Je t’invite également à jeter un œil au Collectif des hébergeurs alternatifs transparents ouverts neutres et solidaires, les https://entraide.chatons.org/fr. Il rassemble des structures proposant des services en ligne éthiques et répondant à des critères exigeants. Avec un peu de chance, tu pourras peut-être en trouver un https://chatons.org/fr/find-by-localisation !",
              links: [
                {
                  text: "CHATONS",
                  link: "https://entraide.chatons.org/fr"
                },
                {
                  text: "près de chez toi",
                  link: "https://chatons.org/fr/find-by-localisation"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss30.png")
                },
              ],
            },
            {
              title: "Connaître les critères importants",
              abstract:"De manière générale, voici 3 critères indispensables pour qu’un service en ligne soit sécurisé et protecteur. Il doit être :\n"+
                            "•	Libre : c’est-à-dire que tout le monde peut étudier son fonctionnement, le modifier pour l’adapter à ses envies, puis partager le résultat. Ainsi, nous pouvons être plus sûr·es que le service fait bien ce qu’il prétend, et rien d’autre. Pour en savoir plus sur cet écosystème, je t’invite à consulter le travail de l’https://www.april.org/fr/articles/intro.\n"+
                            "•	Chiffré de bout en bout : grâce à cette méthode de protection des données, seules les personnes à qui elles sont destinées peuvent réellement le faire. Et personne d’autre. Pas même les personnes qui administrent le service en question.\n"+
                            "•	Décentralisé : cela signifie que l’outil numérique ne doit pas dépendre d’une seule autorité, qui pourrait imposer à tout le monde ses décisions du jour au lendemain. Ainsi, il s’agit d’un bien commun : il appartient à tout le monde et personne ne peut se l’approprier au détriment des autres.\n"+
                            "Pour aller plus loin, je te recommande la lecture du guide https://ssd.eff.org/fr/module/choisir-vos-outils proposé par le site Surveillance Self-Defense.              ",
              links: [
                {
                  text: "association April",
                  link: "https://www.april.org/fr/articles/intro"
                },
                {
                  text: "Choisir vos outils",
                  link: "https://ssd.eff.org/fr/module/choisir-vos-outils"
                },
              ]
            }
          ],
          //pdf: require("assets/pdf/Rokaya_2.pdf"),
          tooltitle: "Remplacer",
          tweet: "Trouver un outil numérique correspondant réellement à ses besoins, envies et valeurs peut-être compliqué. Voyons comment faire en pratique !",
          title: "Rechercher un service en ligne éthique",
        },
        {
          subskills: [
            {
              title: "Naviguer sur le web avec le Tor Browser",
              abstract: "		Pour se protéger des mouchards et préserver son intimité, la solution la plus simple et la plus efficace est d’utiliser le https://www.torproject.org/fr/download/#android. Ce navigateur web basé sur Firefox est libre et gratuit, et est spécifiquement conçu pour contourner la surveillance et la censure.\n"+
                            "Attention tout de même : même s’il est très efficace, le Tor Browser ne dispense pas de continuer de naviguer en HTTPS ou de faire les mises à jour régulièrement. Aussi, tu peux tout de même être reconnu·e par les sites internet que tu visites dans certaines situations, par exemple si tu décides de t’identifier avec un compte.          ",
              links: [
                {
                  text: "recommandations",
                  link: "https://www.torproject.org/fr/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Naviguer sur le web avec le navigateur Duckduckgo",
              abstract: "		Si ta connexion n’est pas assez rapide ou que certains des sites que tu souhaites visiter bloquent Tor Browser, la solution alternative que je te recommande est d’utiliser le navigateur https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (disponible aussi dans le magasin d’applications F-Droid). C’est un navigateur web libre qui bloque les traqueurs et t’informe à quel point les sites que tu visites respectent ta vie privée. Attention cependant, il ne rendra pas ta connexion totalement anonyme (les sites connaitront ton adresse IP et où tu te trouves).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "		Lorsque que l’on navigue sur le web, des mouchards nous suivent de site en site pour pouvoir déduire nos goûts, nos centres d’intérêts et tout un tas d’autres choses nous concernant. Voyons comment nous protéger !",
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Pistage",
          title: "Se protéger des mouchards qui suivent nos navigations",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Joel" },
  { key: "Jillian" },
  { key: "Alvaro" },
  { key: "Atom nobot" },
  {
    key: "Lucas",
    about: "50 ans. En relation (conjoint user#8764563, username «Lola»). Sans enfants. Inspecteur de police. Top player (Gao the Prosecutor).",
    image: require("assets/images/community/avatars/B-Lucas.png"),
    informationShared: "Journal d’appels, Contacts, Géolocalisation, SMS, Wifi, Réseau, Stockage",
    overview: "Curieux, joueur, réservé, méfiant, amoureux, obstiné.",
    recommendations: "Données de réseau social collectées (vente client #XXXXX – studio multimédia et jeu-vidéo): entretient depuis 6 mois une relation amoureuse suivie avec user#8764563 (username «Lola»), qui est juriste.\n" +
      "@Note interne 1:@ indiquer au service facturation de garder masqué l’identifiant du client concernant la transaction de données.\n" +
      "@Note interne2 (confidentiel):@ le client #87654, sous le coup d’une enquête judiciaire menée par l’équipe de l’utilisatrice «Lola», s’est montré très intéressé par les données qui permettraient de prouver une relation avec l’utilisateur «Lucas» (journal d’appels, contenu des SMS et mails, géolocalisation complète). Afin de maintenir de bonnes relations commerciales et dans le cadre de la généreuse offre de rachat de Gao Games par le client, la transaction doit rester confidentielle.\n" +
      "@Note interne 3 (confidentiel):@ l’utilisatrice «Lola» semble suspecter la surveillance de ses communications, et a coupé contact depuis 72h. Information transmise au client #87654.",
    thought: {
      title: 9,
      tweet: "Pas que les individus ",
      overview: "Les entreprises et les administrations aussi sont surveillées",
      message: "Les outils de la surveillance de masse facilitent grandement l’espionnage industriel ou économique. Qu’il s’agisse de concurrencer les entreprises, de leur dérober leurs découvertes, de les rançonner, de les saboter, ou de se renseigner sur leurs client·es par ce biais, de nombreux acteurs ont intérêt à obtenir les informations confidentielles des sociétés. De la même manière, cet enjeu est également très important pour les administrations et les services publics. Le plus souvent, ce sont les personnes qui y travaillent et qui ont légitimement accès à ces informations confidentielles qui permettent involontairement aux personnes malveillantes d’y accéder. Et cette situation s’aggrave au fur et à mesure que ces organisations ont de plus en plus recours au télétravail et ouvrent des accès à leurs outils numériques depuis l’extérieur de leurs locaux. Le caractère massif et l’échelle mondiale de ces pratiques,ainsi que la participation des services de renseignement des États à ces activités ont notamment été démontrés par les documents révélés par le lanceur d’alerte Edward Snowden en 2013. Ainsi, nous protéger de la surveillance permet de protéger les données des structures dans lesquelles nous travaillons et d’éviter des situations parfois catastrophiques. Tu peux trouver de nombreux conseils pour limiter ces risques sur le site https://www.cybermalveillance.gouv.fr/",
      links: [
        {
          text: "cybermalveillance.gouv.fr",
          link: "https://www.cybermalveillance.gouv.fr/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "•	Installer Signal",
              abstract: "Chiffrer ses communications, cela signifie que, grâce à un procédé mathématique, tes messages seront illisibles (« indéchiffrables ») à part pour les personnes à qui ils sont réellement destinés. Sans forcément que nous en ayons conscience, ce procédé est mis en œuvre par de nombreux services que nous utilisons au quotidien, par exemple pour la navigation sur internet ou les paiements en ligne.  - https://signal.org/ -",
              links: [
                {
                  text: "Signal",
                  link: "https://signal.org/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss33.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss34.png")
                },
              ],
            },
            {
              title: "•	Pourquoi cette appli ?",
              abstract: "De nombreuses applications de communication proposent cette sécurité, mais… toutes ne sont pas dignes de confiance. Notamment car leur modèle commercial est celui de l’exploitation et la vente de nos données. Par ailleurs, quand leur code n’est pas libre, elles sont encore moins fiables car personne ne peut savoir comment elles fonctionnent vraiment.\n"+
                            "Évidemment, pour que tes échanges soient protégés, il faudra que tes contacts utilisent aussi cette application. Pour en savoir plus, voici un https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-android détaillant toutes les étapes, de l’installation à l’utilisation de l’application.\n"+
                            "Et pour aller plus loin et tout savoir sur le chiffrement, je te recommande la lecture de cet https://www.nextinpact.com/news/99777-chiffrement-notre-antiseche-pour-expliquer-a-vos-parents.htm du site NextINpact.com !          ",
              links: [
                {
                  text: "guide pratique",
                  link: "https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-android"
                },
                {
                  text: "article",
                  link: "https://www.nextinpact.com/news/99777-chiffrement-notre-antiseche-pour-expliquer-a-vos-parents.htm"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Lucas_1.pdf"),
          tooltitle: "Chiffrer",
          tweet: "Un moyen simple d’améliorer la confidentialité de ses communications est de choisir des applications libres et proposant le chiffrement. Aujourd’hui la plus simple d’utilisation est Signal. Même si elle n’est pas parfaite, il s’agit d’un bon compromis entre sécurité et simplicité d’usage.",
          title: "Chiffrer ses communications",
        },
        {
          subskills: [
            {
              title: "Faire le point sur sa situation",
              abstract: "Afin de limiter efficacement les risques dans le domaine de la sécurité informatique, il est indispensable de commencer par identifier les données que tu souhaites protéger, et de quoi ou qui, en fonction de ta situation.\n"+
                            "En effet, un outil numérique indispensable dans certains cas peut s’avérer dangereux dans d’autres, par exemple parce qu’il signale que tu souhaites protéger ces données.\n"+
                            "Pour commencer, il faut donc que tu fasses le point sur ta situation, en listant les données importantes ou sensibles pour toi et en réfléchissant aux dangers potentiels. En d’autres termes, il s’agit d’établir ton modèle de menaces.\n"+
                            "La suite du travail consiste à analyser les risques que tu identifies. Tous n’ont pas la même probabilité de se réaliser, ni n’auraient des conséquences de même gravité. En estimant ces 2 critères, il devient facile de faire le tri entre les risques que tu dois prendre au sérieux rapidement et ceux dont il n’est pas vraiment nécessaire de te soucier.          ",
            },
            {
              title: "Décider des actions à mener",
              abstract: "Grâce à la cartographie que tu as réalisée lors de l’étape précédente, tu peux maintenant envisager les actions à mener pour te prémunir des risques que tu juges en valoir la peine.		\n"+
                            "Cette fois, c’est l’efficacité et la complexité de la réalisation de chaque piste de solution que tu devras estimer, qu’elle soit de nature technique ou autre. À cette étape, il est particulièrement important de prendre en compte l’ensemble des éléments de la situation donnée :\n"+
                            "•	l’intégrité des équipements et la protection physique des lieux,\n"+
                            "•	la sensibilisation et la formation des personnes concernées,\n"+
                            "•	le choix et la configuration des outils,\n"+
                            "•	le bien être des personnes concernées et leur acceptation sociale des outils et pratiques,\n"+
                            "•	les mesures techniques et organisationnelles,\n"+
                            "•	…\n"+
                            "Certaines idées présenteront plus d’inconvénients que d’avantages ou pourraient même empirer la situation, tandis que d’autres se révéleront particulièrement simples et adaptées. Dans certains cas, les risques s’avéreront trop complexes ou coûteux à traiter, et ne pourront qu’être acceptés. Mais même sans solution immédiate, avoir conscience du danger reste primordiale !\n"+
                            "Enfin, il ne te restera plus qu’à choisir et planifier les actions à réaliser, puis à revenir sur ce travail de temps en temps pour vérifier qu’il ne nécessite pas d’être adapté à de nouveaux besoins et contextes.\n"+
                            "Pour en apprendre davantage sur la réalisation d’un plan de sécurité, je te recommande la lecture de ce petit https://ssd.eff.org/fr/module/votre-plan-de-sécurité publié sur le site Surveillance Self-Defense ou https://laboussole.coop/2019/04/16/document-modele-etablir-un-plan-de-securite-informatique/ de la coopérative de formation et de recherche La Boussole.          ",
              links: [
                {
                  text: "guide",
                  link: "https://ssd.eff.org/fr/module/votre-plan-de-sécurité"
                },
                {
                  text: "celui",
                  link: "https://laboussole.coop/2019/04/16/document-modele-etablir-un-plan-de-securite-informatique/"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_1.pdf"),
          tooltitle: "Modèle protection",
          tweet: "En matière de sécurité informatique, les menaces et besoins de chacun·e peuvent être très différents. Voyons comment trouver une solution sur-mesure.",
          title: "Établir un modèle de menace",
        },
        {
          subskills: [
            {
              title: "Comprendre le risque",
              abstract: "L’objectif de cette arnaque est de récupérer les informations que les visiteu·ses trompé·es pourraient entrer sur un site internet. Elle peut aussi viser à les pousser à installer un logiciel malveillant ou à appeler un service surtaxé.\n"+
                           "Souvent, les liens vers de tels sites sont envoyés par email, avec des messages destinés à faire peur ou à faire croire à un grand coup de chance ou une menace de virus présent sur nos appareils. Par https://cyberguerre.numerama.com/10020-attention-a-ces-phishings-francais-qui-ne-disparaissent-jamais.html, ces messages peuvent annoncer que tu as gagné à un jeu, que tu es en retard pour payer une facture, ou bien que ton portable a été infecté par un virus.          ",
              links: [
                {
                  text: "exemple",
                  link: "https://cyberguerre.numerama.com/10020-attention-a-ces-phishings-francais-qui-ne-disparaissent-jamais.html"
                },
              ],
            },
            {
              title: "Être vigilant·e",
              abstract: "Pour repérer ces tentatives d’arnaque, il faut faire attention à l’adresse des sites que le message veut te faire visiter. Par exemple, https://gaogames.arnaque.fr/ n’aurait rien à voir avec Gao games. Tu peux le constater en appuyant longuement sur le lien avec ton doigt avant de l’ouvrir ou dans le coin de ton navigateur en survolant le lien avec ta souris puisque sous l’apparence d’aller sur https://gaogames.fr, https://gaogames.arnaque.fr/",
              links: [
                {
                  text: "https://gaogames.fr",
                  link: "https://gaogames.arnaque.fr/"
                },
                {
                  text: "tu vas, en réalité à https://gaogames.arnaque.fr comme nous te montrons ci-dessous.",
                  link: "https://gaogames.arnaque.fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss35.png")
                },
              ],
            },
            {
              title: "Comment les repérer ?",
              abstract: "Ici, la partie la plus importante de l’adresse est arnarque.fr : c’est sur ce domaine que tu irais naviguer. Pense à vérifier que c’est bien là que tu veux aller ! Attention également aux erreurs dans les adresses (par exemple Wikypedia au lieu de Wikipedia). Cela demande une grande attention de ta part à chaque fois que tu navigues ou que tu ouvres des liens.\n"+
                            "D’autres éléments peuvent te mettre la puce à l’oreille : des fautes d’orthographe, des mentions légales introuvables, un message trop pressant pour faire quelque chose, l’allure globale du site…\n"+
                            "En cas de doute, mieux vaut prendre le temps de réfléchir et de demander conseil autour de toi. Tu peux également jeter un œil aux https://lookup.icann.org/lookup concernant le nom de domaine. Dans tous les cas, ne clique pas sur les liens qui te semblent étranges. Rends-toi plutôt sur le site en question en le cherchant sur ton moteur de recherche préféré.\n"+
                            "Pour en savoir plus, sur ces arnaques et les moyens de s’en protéger, je t’invite à lire ces https://www.cnil.fr/fr/phishing-detecter-un-message-malveillant de la Commission nationale de l’informatique et des libertés (CNIL) ou cet https://cyberguerre.numerama.com/2724-phishing-comment-font-les-hackers-comment-vous-proteger.html publié sur le site Numerama.com.          ",
              links: [
                {
                  text: "informations publiques",
                  link: "https://lookup.icann.org/lookup"
                },
                {
                  text: "recommandations",
                  link: "https://www.cnil.fr/fr/phishing-detecter-un-message-malveillant"
                },
                {
                  text: "article",
                  link: "https://cyberguerre.numerama.com/2724-phishing-comment-font-les-hackers-comment-vous-proteger.html"
                },
              ],
            }
          ],
          tweet: "Le phishing ou « hameçonnage », l’une des arnaques en ligne les plus répandue consiste à se faire passer pour un site en imitant (des fois extrêmement bien) son apparence. Voyons comment s’en protéger !",
          //pdf: require("assets/pdf/Lucas_3.pdf"),
          tooltitle: "Arnaques",
          title: "Éviter les arnaques en ligne",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  {
    key: "Sol",
    about: "39 ans. Célibataire. Sans enfants. Entrepreneuse. Top player (Gao the Intruder).",
    image: require("assets/images/community/avatars/B-Sol.png"),
    informationShared: "Contacts, Géolocalisation, Wifi, Réseau, SMS, Alarme, Microphone, Calendrier, Appareil photo, Accéléromètre",
    overview: "Pragmatique, confiante, extravertie, carriériste, ambitieuse, grand réseau de connaissances.",
    recommendations: "Données de réseau social collectées (vente client #15467 – banque): le carnet d’adresse et les contacts réseau du sujet indiquent une importante proportion de connaissances à faible revenu économique (impact -53% sur score bancaire). 61% probabilité de ne pas rembourser un prêt bancaire (estimation sur la base des réseaux d’amis). Envisager un refus de prêt.",
    thought: {
      title: 12,
      tweet: "Qui décide pour nous ?",
      overview: "Nos données peuvent être utilisées pour prendre des décisions qui nous concernent",
      message: "À travers le monde, des entreprises fabriquent des logiciels destinés à prendre des décisions à notre sujet sur la base de nos données personnelles. Elles aimeraient pouvoir savoir qui serait susceptible de rembourser ou non un crédit à sa banque, qui correspond le mieux à un emploi, qui tombera malade au cours des prochaines années, qui pourrait commettre un crime… Des objectifs dont la réalisation pourrait paraître souhaitable. Mais, en pratique, ces outils posent de nombreux problèmes. Leurs prédictions s’avèrent souvent inexactes et révèlent davantage les stéréotypes qu’avaient les personnes qui ont créé ces méthodes qu’autre chose. Par https://web.archive.org/web/20200928120145if_/https://fr.reuters.com/article/technologyNews/idFRKCN1MK26B-OFRIN, un programme conçu par Amazon en 2014 pour trier les candidatures reçues par l’entreprise a fini par systématiquement privilégier les CV des hommes. Une autre difficulté majeure est que les gens jugés négativement n’ont plus la possibilité d’obtenir des explications ou de se défendre. Heureusement, en Europe, les choix entièrement faits par des robots sont interdits si elles peuvent avoir des conséquences importantes pour les personnes concernées. Pour en savoir plus à ce sujet, nous vous recommandons de lire cet https://www.internetactu.net/2018/01/15/de-lautomatisation-des-inegalites/ sur la manière dont les systèmes de décision automatisés renforcent les inégalités ou cette https://www.cnil.fr/fr/profilage-et-decision-entierement-automatisee du site Internet de la Commission nationale de l’informatique et des libertés (CNIL).",
      links: [
        {
          text: "exemple",
          link: "https://web.archive.org/web/20200928120145if_/https://fr.reuters.com/article/technologyNews/idFRKCN1MK26B-OFRIN"
        },
        {
          text: "article",
          link: "https://www.internetactu.net/2018/01/15/de-lautomatisation-des-inegalites/"
        },
        {
          text: "page",
          link: "https://www.cnil.fr/fr/profilage-et-decision-entierement-automatisee"
        }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Contrôler les usages de tes données",
              abstract: "		Il est possible de contacter les organisations privées ou publiques qui collectent, utilisent ou vendent nos données personnelles, et de leur demander des informations, corrections, suppressions ou copies de nos données.\n"+
                            "Cela concerne les services ouverts pour des résidents européens ou ses citoyens dans d’autres pays.\n"+
                            "Chaque pays l’Union européenne a son institution en charge de vérifier la bonne application de ces protections. Voici la liste par pays https://edpb.europa.eu/about-edpb/board/members_fr. Et pour les autres pays du monde, je te recommande cette https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde.\n"+
                            "En France, c’est la Commission nationale de l’informatique et des libertés (CNIL) se charge nous aider à faire valoir nos droits en la matière et nous propose des guides sur nos https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles et des https://www.cnil.fr/modeles/courrier de courriers pour les faire valoir facilement.\n"+
                            "Imaginons : en cherchant tes noms et prénoms sur internet, tu trouves une information personnelle dont tu ne souhaites pas la diffusion. La première étape pour la faire disparaître est d’écrire à l’adresse de contact indiquée sur le site en question. Une fois ta demande envoyée, les personnes qui gèrent le site ont l’obligation de te répondre en moins de 30 jours. Bien souvent, cette seule prise de contact suffit à obtenir une réponse satisfaisante.              ",
              links: [
                {
                  text: "ici",
                  link: "https://edpb.europa.eu/about-edpb/board/members_fr"
                },
                {
                  text: "carte détaillée",
                  link: "https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde"
                },
                {
                  text: "différents droits",
                  link: "https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles"
                },
                {
                  text: "modèles",
                  link: "https://www.cnil.fr/modeles/courrier"
                },
              ],
            },
            {
              title: "Déposer plainte",
              abstract:"		Si jamais cette info n’est pas supprimée après les 30 jours, tu peux envoyer https://www.cnil.fr/fr/plaintes à la CNIL qui pourra sanctionner le site en cas de faute. Malheureusement, la CNIL ne disposant que de peu de ressources (et donc de personnes pour traiter les messages, leur réponse peut mettre un certain temps. Si 30 jours se passent sans réponse de la CNIL tu peux alors https://www.service-public.fr/particuliers/vosdroits/F1435 directement auprès de la police, de la gendarmerie ou procureur de la République pour faire valoir tes droits.",
              links: [
                {
                  text: "une plainte",
                  link: "https://www.cnil.fr/fr/plaintes"
                },
                {
                  text: "porter plainte",
                  link: "https://www.service-public.fr/particuliers/vosdroits/F1435"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_2.pdf"),
          tooltitle: "Droits",
          tweet: "L’Union européenne garantit la protection des données de ses citoyen·nes et ainsi que des personnes résidant sur leur territoire. Depuis son entrée en application en 2018, le Règlement général sur la protection des données (RGPD) permet aux citoyen·nes et résident·es de l’Union européenne de mieux contrôler les usages faits de leurs données. Voyons ensemble de quoi il s’agit !",
          title: "Te protéger par delà les frontières !",
        },
        {
          subskills: [
            {
              title: "De quoi s’agit-il ?",
              abstract: "Pour fonctionner, téléphone portable a besoin d’un système d’exploitation, c’est-à-dire, le d’un logiciel principal de l’appareil qui permet aux applications et aux boutons de fonctionner : il fait l’intermédiaire entre eux et le matériel.\n"+
                            "La quasi-totalité des téléphones modernes fonctionne soit avec Android, développé principalement par Google, soit avec iOS, développé entièrement par l’entreprise Apple.\n"+
                            "Ces systèmes d’exploitation sont conçus par des entreprises qui collectent et utilisent nos données et réduisent nos possibilités de protéger nos données.          ",
            },
            {
              title: "Installer un système alternatif",
              abstract:"Heureusement pour les téléphones sous Android, il est possible de remplacer Android par un système d’exploitation comme https://lineageos.org/, https://e.foundation/fr/ ou https://www.replicant.us/, entièrement libres et gratuits.\n"+
                        "Cette opération peut être complexe à réaliser : tous les systèmes ne sont pas encore compatibles avec tous les modèles de téléphones, il est nécessaire d’effectuer une sauvegarde entière avant de se lancer et suivre scrupuleusement les consignes, car en cas d’erreur, par exemple réaliser l’ensemble des opérations avec un téléphone chargé l’appareil peut, dans certains cas devenir inutilisable. Mieux vaut penser à faire une sauvegarde avant de commencer ! Changer de système d’exploitation est une opération relativement délicate, mais grâce à elle, il est possible de réellement reprendre la main sur ton téléphone et mieux protéger tes données personnelles.\n"+
                        "Pour en savoir plus et peut-être tenter l’expérience, je te recommande de démarrer https://linuxfr.org/news/installer-lineageos-sur-son-appareil-android de la Free Software Foundation Europe et ce https://linuxfr.org/news/installer-lineageos-sur-son-appareil-android publié par LinuxFR.org. Et pour résoudre les problèmes que tu pourrais rencontrer, je te conseille le site https://www.xda-developers.com/.          ",
              links: [
                {
                  text: "Lineage OS",
                  link: "https://lineageos.org/"
                },
                {
                  text: "/e/",
                  link: "https://e.foundation/fr/"
                },
                {
                  text: "Replicant",
                  link: "https://www.replicant.us/"
                },
                {
                  text: "cet article",
                  link: "https://linuxfr.org/news/installer-lineageos-sur-son-appareil-android"
                },
                {
                  text: "guide pratique",
                  link: "https://linuxfr.org/news/installer-lineageos-sur-son-appareil-android"
                },
                {
                  text: "xda-developers.com",
                  link: "https://www.xda-developers.com/"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Sol_2.pdf"),
          tooltitle: "OS alternatif",
          tweet: "Attention : ce savoir-faire est compliqué, mais pas de panique, je t’explique en détail de quoi il s’agit  ",
          title: "Installer un système d’exploitation alternatif sur son téléphone",
        },
        {
          subskills: [
            {
              title: "Connaître tes droits",
              abstract: "Fermer un compte sur un service en ligne est un droit : depuis l’entrée en vigueur du Règlement général sur la protection des données (le RGPD) en 2018, nous https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3#Article17 d’une organisation qu’elle supprime toutes les données et informations qui nous concernent. En théorie, tous les sites qui te proposent de créer un compte doivent également te proposer un moyen de le supprimer, avec les données qu’il contient.\n"+
                            "En pratique, la démarche pourra être très simple ou très compliquée. Souvent, il suffira de cliquer sur un bouton « supprimer mon compte » mais certains sites cachent ce menu, essaient de nous dissuader, imposent un délai, obligent à les contacter par téléphone ou à leur envoyer des documents… ou même ne prévoient pas la possibilité de le faire.          ",
              links: [
                {
                  text: "pouvons exiger",
                  link: "https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3#Article17"
                },
              ],
            },
            {
              title: "Rechercher l’option",
              abstract: "Dans tous les cas, la première étape consiste à vérifier si le site en question propose la suppression de ton compte. Pour cela, identifie-toi et rends-toi dans une partie nommée Paramètres personnels, Gérer mon compte ou quelque chose d’équivalent. Avec un moteur de recherche, il est souvent possible de trouver rapidement des explications détaillées sur la marche à suivre pour la plupart des sites internet.",
            },
            {
              title: "Contacter le site",
              abstract: "Si le site ne propose pas de procédure automatisée pour supprimer ton compte, la seconde étape consiste à s’adresser directement aux personnes qui le gèrent. Sur son site internet, la Commission nationale de l’informatique et des libertés (CNIL) propose un https://www.cnil.fr/fr/retrouver-les-coordonnees-dun-organisme-pour-exercer-vos-droits pour retrouver les coordonnées de la personne à qui s’adresser, ainsi qu’un https://www.cnil.fr/fr/modele/courrier/cloturer-un-compte-en-ligne à compléter et à envoyer (il s’agit de l’autorité administrative française chargée de nous aider à faire valoir nos droits dans ce domaine).",
              links: [
                {
                  text: "guide détaillé",
                  link: "https://www.cnil.fr/fr/retrouver-les-coordonnees-dun-organisme-pour-exercer-vos-droits"
                },
                {
                  text: "modèle de courrier",
                  link: "https://www.cnil.fr/fr/modele/courrier/cloturer-un-compte-en-ligne"
                },
              ],
            },
            {
              title: "Déposer plainte",
              abstract: "Si tu vis dans un pays membre de l’Union européenne, lorsque toutes ces démarches ont échoué et que tu n’as pas obtenu de réponse satisfaisante au bout de 30 jours, il est possible d’adresser une plainte à l’https://edpb.europa.eu/about-edpb/board/members_fr pour lui demander d’intervenir.\n"+
                            "En France, il s’agit de la https://www.cnil.fr/fr/plaintes/internet. En cas de pratique illégale, la CNIL pourra sanctionner le site internet et l’obliger à supprimer ton compte et tes données. Malheureusement, les délais pour obtenir une réponse de leur part sont généralement – vraiment – très longs.          ",
              links: [
                {
                  text: "administration en charge de cette question dans ton pays",
                  link: "https://edpb.europa.eu/about-edpb/board/members_fr"
                },
                {
                  text: "CNIL",
                  link: "https://www.cnil.fr/fr/plaintes/internet"
                },
              ],
            }
          ],
          tweet: "Il existe mille et une bonnes raisons de vouloir fermer l’un de ses comptes sur un site internet. Voyons comment faire en pratique !",
          //pdf: require("assets/pdf/Sol_3.pdf"),
          tooltitle: "Comptes",
          title: "Supprimer un compte sur un service en ligne",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Jimmy" },
  { key: "Julie" },
  {
    key: "Nikki-Thought-1",
    isNikkiTought: true,
    thought: {
      title: 1,
      tweet: "Communications surveillées",
      overview: "Résumé : Nos communications peuvent être surveillées",
      message: "J’ai essayé d’écrire un long message aux fans de Gao Games pour expliquer pourquoi je partais, mais je n’ai pas réussi. C’était trop fatigant, trop compliqué, et pour être honnête, j’ai pas eu le courage… Je sais, c’est nul. J’ai quand même laissé mes notes de brouillon. C’est pas forcément très clair, mais j’espère que quelqu’un pourra en faire quelque chose. Je les ai triées par thématiques, pour essayer de parler de tout. Celle-là est sur l’espionnage des discussions…\n" +
        "Par défaut, nos moyens de communication numérique sont peu sécurisés et ne garantissent pas la confidentialité de nos échanges. Surtout les téléphones portables, car leur manière de fonctionner est volontairement rendue opaque et leur modification difficile.\n" +
        "Toutes nos conversations peuvent être surveillées par des entreprises qui gagnent de l’argent avec cette activité (comme Gao Games), par des États et leurs administrations, ou bien par des personnes malveillantes, autour de nous ou inconnues. L’un des seuls moyens de nous protéger est le chiffrement des communications.",
      links: [],
      skills: [
        {
          subskills: [
            {
              title: "•	Installer Signal",
              abstract: "Chiffrer ses communications, cela signifie que, grâce à un procédé mathématique, tes messages seront illisibles (« indéchiffrables ») à part pour les personnes à qui ils sont réellement destinés. Sans forcément que nous en ayons conscience, ce procédé est mis en œuvre par de nombreux services que nous utilisons au quotidien, par exemple pour la navigation sur internet ou les paiements en ligne.  - https://signal.org/ -",
              links: [
                {
                  text: "Signal",
                  link: "https://signal.org/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss33.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss34.png")
                },
              ],
            },
            {
              title: "•	Pourquoi cette appli ?",
              abstract: "De nombreuses applications de communication proposent cette sécurité, mais… toutes ne sont pas dignes de confiance. Notamment car leur modèle commercial est celui de l’exploitation et la vente de nos données. Par ailleurs, quand leur code n’est pas libre, elles sont encore moins fiables car personne ne peut savoir comment elles fonctionnent vraiment.\n"+
                            "Évidemment, pour que tes échanges soient protégés, il faudra que tes contacts utilisent aussi cette application. Pour en savoir plus, voici un https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-android détaillant toutes les étapes, de l’installation à l’utilisation de l’application.\n"+
                            "Et pour aller plus loin et tout savoir sur le chiffrement, je te recommande la lecture de cet https://www.nextinpact.com/news/99777-chiffrement-notre-antiseche-pour-expliquer-a-vos-parents.htm du site NextINpact.com !          ",
              links: [
                {
                  text: "guide pratique",
                  link: "https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-android"
                },
                {
                  text: "article",
                  link: "https://www.nextinpact.com/news/99777-chiffrement-notre-antiseche-pour-expliquer-a-vos-parents.htm"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Lucas_1.pdf"),
          tooltitle: "Chiffrer",
          tweet: "Un moyen simple d’améliorer la confidentialité de ses communications est de choisir des applications libres et proposant le chiffrement. Aujourd’hui la plus simple d’utilisation est Signal. Même si elle n’est pas parfaite, il s’agit d’un bon compromis entre sécurité et simplicité d’usage.",
          title: "Protéger ses communications",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-2",
    isNikkiTought: true,
    thought: {
      title: 2,
      tweet: "Qui sait où je suis ?",
      overview: "Notre téléphone permet de nous localiser",
      message: "Dès qu’il se connecte à un réseau mobile, notre portable peut être localisé relativement précisément par notre opérateur téléphonique (par ex Free, Orange, SFR, Bouygues...). À courte distance, il est également possible de repérer les portables dont le Wifi ou le Bluetooth est activé. Mais le plus problématique en la matière vient généralement des applications installées sur nos appareils : nombre d’entre elles utilisent le GPS intégré au téléphone pour nous trouver et transmettre notre position à des entreprises qui exploitent ou vendent cette information, dont beaucoup de « Géants du Net ». Ces entreprises et leurs clients — ainsi que les services de police et de renseignement — peuvent connaître où nous sommes, mais aussi où nous avons été, combien de fois, combien de temps, avec qui... Si tu veux en savoir plus sur ce sujet, tu peux lire cet https://ssd.eff.org/fr/module/le-probl%C3%A8me-avec-les-t%C3%A9l%C3%A9phones-portables du site Surveillance Self-Defense.",
      links: [{
        text: "article",
        link: "https://ssd.eff.org/fr/module/le-probl%C3%A8me-avec-les-t%C3%A9l%C3%A9phones-portables"
      }],
      skills: [
        {
          subskills: [
            {
              title: "Faire le ménage dans les autorisations déjà accordées",
              abstract: "Lorsque tu utilises une application pour la première fois, elle doit te demander la permission avant d’accéder à tes données ou aux fonctionnalités de ton téléphone. Il peut par exemple s’agir de sa géolocalisation, du microphone, de l’appareil photo, de la liste de tes contacts… Des données et des fonctions potentiellement sensibles !\n"+
                            "Avec le temps, il peut être difficile de te souvenir des autorisations que tu as ou non accordées à telle ou telle application. Pour les vérifier et les modifier, rends-toi dans les paramètres de ton téléphone, puis dans le menu dédié aux autorisations ou aux applications.          ",
            },
            {
              title: "Où se trouve la liste des autorisations ?",
              abstract: "Tu trouveras la liste complète des applications installées sur ton appareil et les autorisations accordées à chaque application. Si tu cherches par « autorisations », tu pourras voir toutes les applications qui accèdent à une autorisation en particulier (par exemple toutes les applications qui accèdent à ta géolocalisation ou tes contacts). Depuis ces menus tu peux soit désinstaller les applications ou refuser les autorisations déjà accordées simplement en appuyant dessus. ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss4.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss5.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss6.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss7.png")
                },
              ],
            },
            {
              title: "Obtenir des informations détaillées",
              abstract: "		Pour obtenir des informations encore plus détaillées sur les autorisations de ton téléphone, n’hésite pas à utiliser le « scanner » de Nikki ! (si tu ne sais pas ce que c’est, c’est qu’il te reste des choses à découvrir ici...)\n"+
                            "Cela te permettra de connaitre le nombre de pisteurs des applications installées et de suivre sur la durée tes progrès dans la protection de tes données sur le téléphone.            ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss8.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss9.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_1.pdf"),
          tooltitle: "Autorisations",
          tweet: "Pour fonctionner, certaines applications demandent la permission d’accéder à différentes informations et fonctionnalités de nos appareils. Par exemple une application « appareil photo » demande l’accès à la caméra. Parfois, ces demandes d’accès n’ont rien à voir avec le fonctionnement de l’application et ne sont destinées qu’à collecter des données personnelles nous concernant. Par exemple un lecteur de musique qui demande à avoir accès à vos contacts et vos déplacements. Voyons ensemble comment les configurer pour mieux nous protéger !",
          title: "Configurer les autorisations accordées aux applications de son téléphone",
          showCheckBoxes: true
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-3",
    isNikkiTought: true,
    thought: {
      title: 3,
      tweet: "L’argent des données ",
      overview: "Des entreprises gagnent beaucoup d’argent en nous surveillant",
      message: "Le modèle économique de nombreuses entreprises proposant des services en ligne repose sur la collecte et l’utilisation de nos données personnelles. Les plus connues de ces entreprises sont les GAFAM — un acronyme désignant Google, Amazon, Facebook, Apple et Microsoft — mais elles sont loin d’être les seules à le faire. Généralement, elles se servent de nos infos pour nous pousser à faire quelque chose (acheter un produit, voir un film, entrer dans un magasin à proximité…). En les revendant à d’autres compagnies qui aimeraient elles aussi en savoir plus sur nous (par exemple pour être les premières à nous suggérer un service dont nous pourrions avoir bientôt besoin). Plus ces entreprises nous connaissent et plus elles gagnent de l’argent ! La plupart de nos usages numériques et des appareils connectés à Internet autour de nous peuvent les aider à collecter toujours plus de renseignements nous concernant : navigations sur le web, téléphones portables, montres, cartes de paiement et de fidélité, télévisions, aspirateurs, voitures, sonnettes, frigos, compteurs électriques, de gaz et d’eau… Autant d’occasions pour ces entreprises d’accéder à de petites informations, qui une fois mises bout à bout depuis des années, permettent d’en savoir beaucoup sur nous, nos vies, nos idées et nos quotidiens. Bien que ces pratiques soient souvent illégales, elles sont malgré tout très répandues. En attendant l’application réelle de la loi et la fin de cette surveillance, il existe heureusement des moyens de nous en protéger.",
      links: [],
      skills: [
        {
          subskills: [
            {
              title: "Comment circulent les infos entre un site et nous ?",
              abstract: "Il faut s’imaginer que, à chaque fois que tu te connectes à un site internet, ce site et ton ordinateur  « discutent », c’est-à-dire s’envoient des informations. Quand tu veux voir un onglet ou une vidéo, le site que tu consultes doit savoir ce qu’il doit te montrer ou si tu achètes quelque chose, il faut que tu lui envoies ton numéro de carte bancaire. En utilisant le protocole HTTPS ces données sont envoyées de manière secrète (la confidentialité) et complètes (l’intégrité). Si une personne malveillante parvenait à intercepter ces données en cours de route, elle ne pourrait accéder qu’à une suite de caractères brouillée et incompréhensible.\n"+
                            "Aujourd’hui, beaucoup de sites internet proposent cette sécurité par défaut. Si ce n’est pas le cas, il suffit d’ajouter un s au HTTP de l’adresse du site visité (un moyen de s’en souvenir est de penser à s comme « sécurisé »). Et pour éviter d’avoir à le faire soi-même à chaque fois, il est possible d’installer l’extension https://www.eff.org/https-everywhere/, qui s’en charge à notre place !          ",
              links: [
                {
                  text: "HTTPS Everywhere",
                  link: "https://www.eff.org/https-everywhere/"
                },
              ],
            },
            {
              title: "Affichage sur un navigateur web",
              abstract: "Les navigateurs web affichent donc un petit cadenas fermé quand c’est sécurisé, ou barré quand ça ne l’est pas.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss39.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss40.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss41.png")
                },
              ],
            },
            {
              title: "Connexion non sécurisée",
              abstract: "Si tu as ajouté un s au HTTP alors que le site ne propose la possibilité de lui envoyer les informations de manière sécurisée tu verras s’afficher un message d’erreur :",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss42.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss43.png")
                },
              ],
            },
            {
              title: "Que faire ?",
              abstract: "Pas de panique ! Peut-être que le site ne propose tout simplement pas cette sécurité. Dans ce cas, visite le en HTTP simple, mais en faisant particulièrement attention à ne pas indiquer de mot de passe, de numéro de carte bleue ou aucune autre information.\n"+
                            "Si le protocole HTTPS fonctionne habituellement pour ce site internet et que tu vois toujours le même un message d’erreur, il y a deux options.          •	S’il s’agit d’un problème de configuration du site et généralement en revenant dans quelques minutes, tout peut être en ordre.          •	Si le message est toujours là, il pourrait aussi s’agir d’une tentative d’arnaque et le site a été piraté.\n"+
                            "Si tu décides de contourner ces messages, tu poursuis ta navigation à tes risques et périls. Et à nouveau, de ne jamais y indiquer de mot de passe, de numéro de carte bleue ou aucune autre information. Ces messages sont là pour t’indiquer l’état du site et il se peut qu’ils mettent en danger tes données.          ",
            }
          ],
          tweet: "Lorsque l’on va sur internet, mieux vaut se protéger des arnaques les plus fréquentes.",
          //pdf: require("assets/pdf/though 3-1"),
          tooltitle: "Navigation",
          title: "Améliorer la sécurité de sa navigation sur le web  ",
        },
        {
          subskills: [
            {
              title: "Éviter les risques",
              abstract: "	Même pour les spécialistes, il est difficile de savoir avec certitude ce qu’une appli peut faire ou non. Pour en télécharger une nouvelle sans risque, il est important de passer par un magasin d’applications. Ces boutiques vérifient pour nous que les applis proposées ne contiennent pas de fonctionnalités malveillantes et qu’elles nous demanderont la permission avant d’accéder à nos données et aux capteurs de notre appareil.\n"+
                            "Au contraire, installer une application depuis une autre source que ces magasins expose à un risque important de se retrouver avec un programme piégé (par exemple, un fichier. apk proposé gratuitement par un site internet).          ",
            },
            {
              title: "Collecter des indices",
              abstract: "Mais même avec ces magasins, il est parfois difficile de s’y retrouver dans toutes les applications proposées ! Plusieurs éléments peuvent t’aider à mesurer la fiabilité d’une application : le nombre de téléchargements, la description, les avis et commentaires des autres personnes, les autorisations demandées par l’application lors de son premier lancement ou après une mise à jour…\n"+
                            "Tu peux aussi te renseigner sur l’organisation à qui appartient l’application. Par exemple, en plus d’Android, Google possède également Chrome, YouTube, Waze et bien d’autres. De son côté, Facebook contrôle Instagram et WhatsApp. Au moindre doute, mieux vaut ne pas installer l’application en question et en choisir une autre.                        Enfin, il est possible grâce à notre outil qui utilise https://reports.exodus-privacy.eu.org/fr/ de te fournir des renseignements sur les applications les plus connues et t’indique pour chacune d’elles le nombre de pisteurs qu’elle intègre et les permissions qu’elle requiert pour fonctionner.          ",
              links: [
                {
                  text: "Exodus Privacy,",
                  link: "https://reports.exodus-privacy.eu.org/fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss44.png")
                },
              ],
            },
            {
              title: "Utiliser une source fiable",
              abstract: "En plus de ne mettre à disposition que des applications libres et plus respectueuses (et donc plus fiables), le magasin F-Droid propose une fonctionnalité intéressante pour t’aider à mesurer la fiabilité des applications. Dans les descriptions des applis il liste les fonctions qui pourraient vous déplaire. Grâce à elle, il devient plus simple d’y voir clair et de comparer plusieurs applications avant de choisir celle qui nous convient.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss45.png")
                },
              ],
            }
          ],
          tweet: "Avant d’installer une application, il est important d’être sûr·e de pouvoir lui faire confiance car elle pourra ensuite accéder à nos données, en collecter de nouvelles, et les effacer ou les transmettre à des personnes ou entreprises malveillantes.",
          //pdf: require("assets/pdf/though 3-2"),
          tooltitle: "Fiabilité",
          title: "Mesurer la fiabilité d’une application ",
        },
        {
          subskills: [
            {
              title: "Trouver le menu",
              abstract: "Pour sauvegarder tes données, rends-toi dans les Paramètres de ton téléphone, puis dans le menu Autres paramètres et appuie sur Sauvegarder et réinitialiser (tout en bas) :",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss46.png")
                },
              ],
            },
            {
              title: "Sélectionner les données à sauvegarder",
              abstract: "Une fois dans ce menu, appuie sur Sauvegarder et restaurer, puis sur Créer sauvegarde et choisis les données que tu souhaites sauvegarder. Par défaut, tu peux laisser toutes les cases cochées :",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss47.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss48.png")
                },
              ],
            },
            {
              title: "Démarrer la sauvegarde",
              abstract: "	Appuie ensuite sur Démarrer la sauvegarde et c’est parti ! Après quelques instants, ta sauvegarde est terminée et apparaît dans le menu. En appuyant dessus, tu pourras maintenant restaurer les données de ton choix chaque fois que tu auras besoin de le faire. Mais il ne faut pas s’arrêter là,",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss49.png")
                },
              ],
            },
            {
              title: "Exporter ta sauvegarde",
              abstract: "Pour que tes données soient réellement en sécurité, il faut les copier sur un autre support pour pouvoir y accéder et les réinstaller sur ont téléphone. Pour réaliser cette copie, connecte ton téléphone à un ordinateur à l’aide d’un câble USB. Une notification va apparaître sur ton écran : choisis Transférer des fichiers.\n"+
                            "Tu peux maintenant accéder au contenu du téléphone depuis ton ordinateur. Trouves-y le dossier Backup et copie-le sur ton ordinateur : c’est dans ce dossier que sont rangées tes sauvegardes.\n"+
                            "Si tu n’as pas d’ordinateur, tu peux aussi insérer une carte SD dans ton téléphone et y copier le dossier. Dans ce cas, n’oublie pas de ranger la carte SD en sécurité ensuite (ne la laisse pas dans le téléphone, pour ne pas risquer de perdre les 2 en même temps).              ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss50.png")
                },
              ],
            },
            {
              title: "Sécuriser tes sauvegardes",
              abstract: "En matière de sauvegarde une bonne règle est celle des « 3 2 1 * », c’est-à-dire :\n"+
                            "•	3 copies de chaque donnée (l’originale sur le téléphone et2 copies)\n"+
                            "•	sur au moins 2 supports différents (par exemple un ordinateur et un disque dur externe ou une carte SD)\n"+
                            "•	et qu’au moins 1 copie soit stockée dans un autre lieu que les autres (ce peut-être en ligne)\n"+
                            "•	* et l’étoile pour penser que ces sauvegardes doivent être chiffrées.\n"+
                            "Ainsi, quel que soit le problème que tu pourrais rencontrer, l’une des sauvegardes sera toujours disponible pour restaurer tes données. Qu’il s’agisse d’un cambriolage, d’un incendie, d’une erreur de manipulation ou d’un problème technique qui détruirait ton ordinateur et ton téléphone en même temps, tu pourras faire face à toutes ces situations.\n"+
                            "Évidemment, tu peux aussi adopter une méthode plus simple si celle-ci te semble trop compliquée : mieux vaut avoir une seule copie que pas du tout !\n"+
                            "Un élément important pour que tes sauvegardes soient sécurisées est de choisir avec soin le support qui les accueillera. Dans l’idéal, il doit être chiffré, Ce petit https://www.cnil.fr/fr/comment-chiffrer-ses-documents-et-ses-repertoires publié par la Commission nationale de l’informatique et des libertés (CNIL) explique comment faire en pratique.\n"+
                            "Certaines personnes choisissent de confier leurs sauvegardes aux GAFAM ou à d’autres géants du Net. Comme tu l’as deviné, ce n’est pas la solution que je te recommande. Si tu souhaites tout de même le faire, je t’invite à commencer par prendre le temps de faire le tri entre les applications et données que tu synchroniseras en ligne et celles qu’il vaut vraiment mieux sauvegarder ailleurs.\n"+
                            "Un dernier conseil pour finir : n’oublie pas de vérifier de temps en temps que tes sauvegardes ont fonctionné et qu’il est possible de restaurer tes données ! Et si tu préfères une solution sur mesure, par exemple pour pouvoir sauvegarder également les données de tes applications, je te recommande d’utiliser les applications https://f-droid.org/fr/packages/com.machiav3lli.backup/ (uniquement proposée dans le magasin F-Droid) ou https://twrp.me/app/.              ",
              links: [
                {
                  text: "guide,",
                  link: "https://www.cnil.fr/fr/comment-chiffrer-ses-documents-et-ses-repertoires"
                },
                {
                  text: "OandbackupX,",
                  link: "https://f-droid.org/fr/packages/com.machiav3lli.backup/"
                },
                {
                  text: "TWRP",
                  link: "https://twrp.me/app/"
                },
              ],
            }
          ],
          tweet: "L’un des objectifs de la sécurité informatique est de protéger les données de la destruction. Pour cela, il est indispensable de les sauvegarder régulièrement, et ce d’autant plus que les téléphones sont par nature faciles à perdre ou à briser. Un problème arrivera forcément un jour ou l’autre : mieux vaut s’y préparer en avance !",
          //pdf: require("assets/pdf/though 3-3"),
          tooltitle: "Sauvegarde",
          title: "Sauvegarder tes données ",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-4",
    isNikkiTought: true,
    thought: {
      title: 4,
      tweet: "Surveillance et démocratie",
      overview: "La surveillance de masse nuit au fonctionnement de la démocratie",
      message: "En surveillant nos faits et gestes, des entreprises arrivent à déduire nos opinions politiques et les arguments les plus à même de nous convaincre de faire ou de ne pas faire quelque chose. Et même de voter ou de ne pas voter pour un·e candidat·e à une élection démocratique ! La société https://fr.wikipedia.org/wiki/Cambridge_Analytica est accusée d’avoir utilisé des données https://www.slate.fr/story/159250/cambridge-analytica-facebook-scandale-triche en partie via https://la-rem.eu/2020/06/facebook-a-nouveau-condamne-dans-le-cadre-de-laffaire-cambridge-analytica/ pour influencer l’élection de Donald Trump aux États-Unis ou le referendum de sortie de la Grande-Bretagne de l’Union européenne. En tout, Cambridge Analytica aurait pesé sur plus de 200 élections à travers le monde, pour répondre aux demandes des personnes pouvant payer ces services. S’il est difficile d’estimer précisément leurs effets sur les résultats électoraux, ces interventions donnent tout de même un exemple des potentielles conséquences dramatiques que peut avoir la surveillance de masse sur le fonctionnement de la démocratie représentative. Aussi, savoir que l’on est peut-être surveillé·e modifie la façon dont nous nous comportons. Des individus vont par exemple s’abstenir de faire ou dire quelque chose (par exemple https://www.slate.fr/story/116035/surveillance-masse-opinions-minoritaires) de peur d’être pénalisé·es. De manière plus générale, les géants du Net détiennent un pouvoir très important sur les informations https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/ [en] parvenir jusqu’à nous et influencer nos visions du monde. Dans une situation idéale, ce contrôle devrait être entre les mains de celles et ceux qui utilisent ces services, plutôt qu’entre celles des d’entreprises privées.",
      links: [{
        text: "Cambridge Analytica",
        link: "https://fr.wikipedia.org/wiki/Cambridge_Analytica"
      },{
        link: "https://www.slate.fr/story/159250/cambridge-analytica-facebook-scandale-triche",
        text: "collectées",
      }, {
        link: "https://la-rem.eu/2020/06/facebook-a-nouveau-condamne-dans-le-cadre-de-laffaire-cambridge-analytica/",
        text: "Facebook",
      }, {
        link: "https://www.slate.fr/story/116035/surveillance-masse-opinions-minoritaires",
        text: "exprimer une opinion minoritaire",
      },{
        link: "https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/",
        text: "qui peuvent ou non",
      }],
      skills: [
        {
          subskills: [
            {
              title: "Depuis l’écran principal",
              abstract: "La façon la plus simple de désinstaller une application est d’appuyer longuement sur son icône : une bulle apparaîtra, qui te proposera de la Supprimer. Certaines applications préinstallées ne peuvent pas être complètement désinstallées. C’est frustrant, mais dans ce cas, le mieux que tu peux faire est d’ouvrir les informations sur l’appli, forcer l’arrêt et supprimer les autorisations.  ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss36.png")
                },
              ],
            },
            {
              title: "Depuis les paramètres",
              abstract: "Une autre solution pour désinstaller une application est d’aller dans les Paramètres de ton téléphone, puis dans le menu Gestion des applications. Il suffit ensuite de sélectionner l’application en question, d’appuyer sur Désinstaller et de confirmer ce choix.\n"+
                            "Et voilà !  De temps en temps, pense à faire le ménage dans tes applications en supprimant celles que tu ne souhaites pas ou plus utiliser.     ",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss37.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss38.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/though 4"),
          tooltitle: "Désinstallation",
          tweet: "Il existe plusieurs façons de désinstaller une application de ton téléphone. Voilà comment faire :",
          title: "Désinstaller une application",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-5",
    isNikkiTought: true,
    thought: {
      title: 5,
      tweet: "La pollution",
      overview: "Les outils numériques polluent aussi",
      message: "Contrairement à ce que l’on pourrait croire, les usages et outils numériques génèrent une quantité de pollution importante à l’échelle de la planète. La fabrication des appareils et équipements, la navigation sur des sites Internet accessibles 24 h/24, l’envoi de messages électroniques, les impressions à la maison… toutes ces activités, consomment de l’énergie, produisent des déchets et demandent de grands volumes de métaux rares ! Loin de remplacer des usages polluants existants, les outils et services numériques les encouragent et s’y additionnent. Les https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/ démontrent que ces dispositifs et pratiques émettent déjà plus de gaz à effet de serre que le transport aérien, et que ces émissions atteindront bientôt le niveau de celles du transport routier. Les conditions de fabrication de ces équipements et de leurs composants sont également souvent https://www.monde-diplomatique.fr/2020/07/BELKAID/61982 (esclavage, travail des enfants…). Heureusement, des solutions existent et nous pouvons agir pour limiter les conséquences sociales et environnementales de nos pratiques numériques !",
      links: [{
        link: "https://theshiftproject.org/article/pour-une-sobriete-numerique-rapport-shift/",
        text: "études"
      },{
        link: "https://www.monde-diplomatique.fr/2020/07/BELKAID/61982",
        text: "scandaleuses",
      }],
      skills: [
        {
          subskills: [
            {
              title: "Comprendre cet enjeu",
              abstract: "D’après l’Agence de la transition écologique (ADEME), https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-impacts-smartphone.pdf des impacts environnementaux des téléphones portables sont liés à leur fabrication, principalement à cause de leurs écrans et de leurs composants électroniques complexes, tels que les microprocesseurs.              Idem pour leurs conséquences sociales les plus dramatiques : elles ont lieu au moment de l’extraction des matières premières qui composent ces appareils, puis au instant de leur assemblage.          ",
              links: [
                {
                  text: "environ 75 %",
                  link: "https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-impacts-smartphone.pdf"
                },
              ],
            },
            {
              title: "Faire durer tes appareils",
              abstract: "Ainsi, le meilleur moyen pour réduire les conséquences de nos usages est de faire durer la vie de nos appareils et de ne pas les remplacer tant qu’ils fonctionnent encore. Toujours d’après l’ADEME, en les utilisant 4 ans plutôt que 2, nous améliorions le bilan écologique de nos appareils de 50 %.\n"+
                            "Pour cela, nous pouvons les protéger avec des housses, coques et autres films pour écran et veiller à les laisser se reposer lorsqu’ils commencent à surchauffer. Enfin, n’attends pas que la batterie de ton appareil soit complètement vide pour la recharger et évite de la laisser en charge une nuit entière. De cette manière, elle fonctionnera plus longtemps.\n"+
                            "Si l’un de tes appareils tombe malgré tout en panne, il est souvent possible de le faire réparer. S’il a moins de 2 ans, tu bénéficies même d’une garantie légale pour cela ! Tu peux également essayer de le réparer toi-même : de nombreux tutoriels en ligne expliquent comment faire, par exemple ceux du site https://fr.ifixit.com/, et de plus en plus d’associations pourront te donner un coup de main.          ",
              links: [
                {
                  text: "ifixit.com",
                  link: "https://fr.ifixit.com/"
                },
              ],
            },
            {
              title: "Bien réfléchir avant d’acheter",
              abstract: "Et lorsque tu dois remplacer un appareil, n’oublie pas de prendre en considération ces questions. Prends le temps de bien te renseigner, de choisir un appareil qui te conviendra réellement, qui pourra être réparé facilement et dont les marques qui ne font pas volontairement dysfonctionner leurs produits au bout d’un certain temps (on appelle ça l’obsolescence programmée).\n"+
                        "Une bonne idée est de choisir un téléphone d’occasion ou reconditionné : tu le payeras moins cher et tu éviteras le coût environnemental de la fabrication d’un nouvel appareil ! Enfin, n’oublie pas de recycler ton ancien appareil précédent à ce moment.\n"+
                        "Si tu souhaites tout de même un téléphone neuf, tu peux jeter un œil à ceux proposés par l’entreprise https://www.fairphone.com/fr/, qui s’est fixé l’objectif de fabriquer les appareils les plus éthiques possibles ou encore à réfléchir à la location d’appareils plus qu’à la possession.	D’autres fabricants similaires proposeront peut-être la même chose à l’avenir – l’important n’est pas la marque, mais la démarche.                    ",
              links: [
                {
                  text: "Fairphone",
                  link: "https://www.fairphone.com/fr/"
                },
              ],
            }
          ],
          tweet: "Comme nous l’avons vu, les outils numériques polluent eux aussi ! Voilà comment limiter les dégâts.",
          tooltitle: "Ecologie",
          //pdf: require("assets/pdf/though 5"),
          title: "Limiter les conséquences environnementales et sociales de nos pratiques numériques",
        }
      ]
    }
  }
]
