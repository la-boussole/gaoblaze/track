function transformIntoMap(arr) {
  const m = {}

  for (const item of arr) {
    m[item.id] = item
  }

  return m
}

export default transformIntoMap([
  {
    "id": "Sol",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/00_Sol.png"),
    "favoriteGame": "Gao The Intruder",
    "about":"Entrepreneuse au taquet ! J’aime les chats.",
    "avatarText": "ic_00_sol",
  },
  {
    "id": "Rokaya",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/01_Rokaya.png"),
    "favoriteGame": "Gao The Photographer",
    "about":"Influenceuse. Photo addict. <3",
    "avatarText": "ic_01_rokaya",
  },
  {
    "id": "Nikki",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/02_Nikki.png"),
    "favoriteGame": "Gao The Spy",
    "about":"Crazy_Blob",
    "avatarText": "ic_02_nikki",
  },
  {
    "id": "Masako",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/03_Masako.png"),
    "favoriteGame": "Gao The Forger",
    "about":"J’adore Gao games ! Je suis japonaise.",
    "avatarText": "ic_03_masako",
  },
  {
    "id": "Lucas",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/04_Lucas.png"),
    "favoriteGame": "Gao The Prosecutor",
    "about":"RAS",
    "avatarText": "ic_04_lucas",
  },
  {
    "id": "Amin",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/07_Amin.png"),
    "favoriteGame": "Gao The Filekeeper",
    "about":"Calme. Et perfectionniste. :)",
    "avatarText": "ic_07_amin",
  },
  {
    "id": "Ally",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/08_Ally.png"),
    "favoriteGame": "Gao The Athlete",
    "about":"Artiste. Je bosse pour le studio de Gao Games !",
    "avatarText": "ic_08_ally",
  },
  {
    "id": "Alex",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/09_Alex.png"),
    "favoriteGame": "Gao The Intruder",
    "about":"Il paraît que je ris fort ! J’adore rencontrer de nouvelles personnes.",
    "avatarText": "ic_09_alex",
  },
  {
    "id": "Ajay",
    "confidence": 5,
    "avatar": require("assets/images/community/avatars/10_Ajay.png"),
    "favoriteGame": "Gao The Sage",
    "about":"Fan d’electro et du Parrain.",
    "avatarText": "ic_10_ajay",
  },
  {
    "id": "NikkiBlaze",
    "isBlaze": true,
    "avatar": require("assets/images/community/avatars/B-Nikki.png"),
    "avatarText": "ic_nikki_b",
  },
  {
    "id": "AlexBlaze",
    "isBlaze": true,
    "avatar": require("assets/images/community/avatars/B-Alex.png"),
    "avatarText": "ic_alex_b",
  }
])
