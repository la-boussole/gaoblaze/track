const OtherProfiles = [
  {
    "id": "Natalia",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Natalia.jpg")
  },
  {
    "id": "Quentin",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Quentin.jpg")
  },
  {
    "id": "Tanilo",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Tanilo.jpg")
  },
  {
    "id": "Alvaro",
    "confidence": 5,
    "avatar": require("assets/images/avatar/alvie.jpg")
  },
  {
    "id": "Juan-Sebastián",
    "confidence": 5,
    "avatar": require("assets/images/avatar/sebas.jpg")
  },
  {
    "id": "Claire",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Claire.png")
  },
  {
    "id": "Gabriel",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Gabriel.png")
  },
  {
    "id": "Juan-David",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Juan_David.png")
  },
  {
    "id": "Pasca",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Pasca.png")
  },
  {
    "id": "Jean-Yves",
    "confidence": 5,
  },
  {
    "id": "Bintje",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Bintje.png")
  },
  {
    "id": "Edouard",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Edouard.png")
  },
  {
    "id": "Jean-Felix",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Jean-Felix.jpg")
  },
  {
    "id": "Damon",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Damon.jpeg")
  },
  {
    "id": "Max-Leontine",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Jean-Felix.jpg")
  },
  {
    "id": "Sushicat",
    "confidence": 5,
    "avatar": require("assets/images/avatar/Sushicat.jpg")
  },
  {
    "id": "Mao",
    "confidence": 5
  },
  {
    "id": "Ellen",
    "confidence": 4
  },
  {
    "id": "Colette",
    "confidence": 2
  },
  {
    "id": "James",
    "confidence": 1
  },
  {
    "id": "Amy",
    "confidence": 3
  },
  {
    "id": "Sven",
    "confidence": 4
  },
  {
    "id": "James",
    "confidence": 5
  },
  {
    "id": "Kelsey",
    "confidence": 4
  },
  {
    "id": "Tiana",
    "confidence": 1
  },
  {
    "id": "Jordan",
    "confidence": 3
  },
  {
    "id": "Cheryl",
    "confidence": 4
  },
  {
    "id": "Patricia",
    "confidence": 1
  },
  {
    "id": "Peter",
    "confidence": 2
  },
  {
    "id": "Giavanna",
    "confidence": 1
  },
  {
    "id": "Erlinda",
    "confidence": 4
  },
  {
    "id": "Amber",
    "confidence": 4
  },
  {
    "id": "Kelli",
    "confidence": 4
  },
  {
    "id": "Dina",
    "confidence": 1
  },
  {
    "id": "Caitlin",
    "confidence": 5
  },
  {
    "id": "Devin",
    "confidence": 2
  },
  {
    "id": "Jesse",
    "confidence": 5
  },
  {
    "id": "Noémie",
    "confidence": 5
  },
  {
    "id": "Adèle",
    "confidence": 1
  },
  {
    "id": "Luàna",
    "confidence": 3
  },
  {
    "id": "egaeus",
    "confidence": 5
  },
  {
    "id": "Valeria",
    "confidence": 4
  },
  {
    "id": "Christel",
    "confidence": 3
  },
  {
    "id": "Armande",
    "confidence": 3
  },
  {
    "id": "Guy",
    "confidence": 2
  },
  {
    "id": "Clément",
    "confidence": 4
  },
  {
    "id": "Michèle",
    "confidence": 5
  },
  {
    "id": "Cécile",
    "confidence": 1
  },
  {
    "id": "Rémi",
    "confidence": 2
  },
  {
    "id": "Véronique",
    "confidence": 4
  },
  {
    "id": "Bénédicte",
    "confidence": 2
  },
  {
    "id": "Olivier",
    "confidence": 1
  },
  {
    "id": "Jean-Baptiste",
    "confidence": 4
  },
  {
    "id": "Arthur",
    "confidence": 3
  },
  {
    "id": "Atom",
    "confidence": 5
  },
  {
    "id": "Julien",
    "confidence": 4
  },
  {
    "id": "Christel",
    "confidence": 1
  },
  {
    "id": "Thomas",
    "confidence": 2
  },
  {
    "id": "Dorothée",
    "confidence": 5
  },
  {
    "id": "Aurélie",
    "confidence": 3
  },
  {
    "id": "Marthe",
    "confidence": 5
  },
  {
    "id": "Marie",
    "confidence": 1
  },
  {
    "id": "Quinet",
    "confidence": 3
  },
  {
    "id": "Romain",
    "confidence": 4
  },
  {
    "id": "Julien",
    "confidence": 4
  },
  {
    "id": "Tiphaine",
    "confidence": 3
  },
  {
    "id": "Claire",
    "confidence": 5
  },
  {
    "id": "Anaïs",
    "confidence": 5
  },
  {
    "id": "Delphine",
    "confidence": 1
  },
  {
    "id": "Tatiana",
    "confidence": 1
  },
  {
    "id": "Ricardo",
    "confidence": 5
  },
  {
    "id": "Roberto",
    "confidence": 3
  },
  {
    "id": "Rocío",
    "confidence": 1
  },
  {
    "id": "Rodrigo",
    "confidence": 2
  },
  {
    "id": "Rubén",
    "confidence": 4
  },
  {
    "id": "Santiago",
    "confidence": 3
  },
  {
    "id": "Julen",
    "confidence": 1
  },
  {
    "id": "Koldo",
    "confidence": 3
  },
  {
    "id": "Laura",
    "confidence": 3
  },
  {
    "id": "Leticia",
    "confidence": 1
  },
  {
    "id": "Lucia",
    "confidence": 3
  },
  {
    "id": "Maite",
    "confidence": 1
  },
  {
    "id": "Francisco",
    "confidence": 4
  },
  {
    "id": "Gaizka",
    "confidence": 5
  },
  {
    "id": "Gerardo",
    "confidence": 1
  },
  {
    "id": "Guillermo",
    "confidence": 5
  },
  {
    "id": "Gustavo",
    "confidence": 2
  },
  {
    "id": "Hernan",
    "confidence": 1
  },
  {
    "id": "Ignacio",
    "confidence": 4
  },
  {
    "id": "Jaime",
    "confidence": 2
  }
]

export default OtherProfiles
