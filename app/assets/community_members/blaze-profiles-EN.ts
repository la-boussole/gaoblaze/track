import { BlazeProfile } from "./blaze-profiles";

export const BLAZE_PROFILES_EN: BlazeProfile[] = [
  {
    key: "Ajay",
    about: "19 years old. Single. No children. Student. Top player (Gao the Sage).",
    image: require("assets/images/community/avatars/B-Ajay.png"),
    informationShared: "SMS, Accelerometer, Contacts, Geolocation, Storage, Wifi, Network, Notifications, Bluetooth, NFC, Alarm, Wallpaper, Microphone, Fingerprint, Calendar, Camera",
    overview: "Student, vegetarian, sociable, curious, new immigrant, science enthusiast.",
    recommendations: "Interest and social network data collected (customer sale #12579 - National Security Office): Analysis of communications and Gao the Sage's Type 3 response database indicates that the subject has many friendly acquaintances involved in Middle East events. Metadata mapping identified 78% of his contacts.\n"+
      "Internal note: recent drop in communications with his contacts, following the previous data sale (client #55578 - private security agency).",
    thought: {
      title: 6,
      tweet: "Repression",
      overview: "Mass surveillance facilitates state repression",
      message: "Mass surveillance in itself is already a violation of our fundamental rights. Unfortunately, it also allows even more serious violations. Non-democratic governments can use it, for https://en.wikipedia.org/wiki/Pegasus_(spyware) to remove the counter-power of the media or whistleblowers, who by their vigilance strengthen our democracies.\n"+
      "As whistleblower https://en.wikipedia.org/wiki/Edward_Snowden revealed, the surveillance carried out by the Internet giants directly feeds the surveillance carried out by certain States. Protecting ourselves from one allows us to protect ourselves from the other, but also to protect our loved ones. Indeed, our practices have consequences on the people around us. For some of them, keeping their information secret can be very considerable—even vital. Nowadays the Pegasus Program is the most recent example of how some governments monitor others (heads of state, citizens, journalists) through their cell phones.\n",
      links: [
        {
          text: "example",
          link: "https://en.wikipedia.org/wiki/Pegasus_(spyware)"
        },
        {
          text: "Edward Snowden",
          link: "https://en.wikipedia.org/wiki/Edward_Snowden"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Take Stock of Your Situation",
              abstract: "First, let’s start by identifying what data you want to protect, and from what or who, depending on your situation. You can do this by identifying the data that is important or sensitive to you and thinking about the potential dangers if you ever lose it or if anyone else has access to it. This is what is called in computer security “establishing a threat model” but we will call it a protection model.\n"+
              "The next step is to identify, for each piece of data, how serious and probable their loss or dissemination may be. Indeed, each risk does not have the same probability of occurrence nor the same gravity. For example, a very, very, very unlikely risk would be that Martians would use intimate photos to decorate their spaceships, and for very likely risks, that Google would use these same photos to get to know you better and sell you related products.\n"+
              "By taking into account these 2 criteria, severity and probability, it becomes easy to sort out the risks you need to take seriously quickly and those you don’t really need to worry about.",
            },
            {
              title: "Decide on actions to be taken",
              abstract: "Thanks to the mapping you have just done, you can now identify the places that need to be protected in priority.\n \n"+
              "This time, you have to find and be accompanied if you feel the need to imagine possible ways. Each one can be more or less difficult to implement, but don’t get discouraged! Sometimes there are very technical solutions, but other times not at all. For each data to protect, I invite you to ask yourself these questions:\n"+
                  "• Do my devices have a good password? Do I leave them next to the water tap and not have a back-up of their contents?\n"+
                  "• Is the solution I imagine to counter a risk easy or difficult to use?\n"+
                  "• Who can give me advice/ideas on how to protect this or that information?\n"+
                  "• Do these measures concern me individually or rather a group? …\n"+
                "Some ideas will have more disadvantages than advantages or may even make the situation worse, while others will be particularly simple and appropriate. But even without an immediate solution, being aware of the danger is already the beginning of the solution!\n"+
                "Finally, you will only have to choose and plan the actions to be carried out, and then come back to this work from time to time to check that it does not need to be adapted to new needs and contexts, because sometimes, there are no immediate solutions.\n"+
                "To learn more about how to create a safety plan, I recommend you read this short https://ssd.eff.org/en/module/your-security-plan published by the Self-Defense Watch website and https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ENG.cleaned.pdf from training and research cooperative La Boussole.",
              links: [
                {
                  text: "guide",
                  link: "https://ssd.eff.org/en/module/your-security-plan"
                },
                {
                  text: "this one",
                  link: "https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ENG.cleaned.pdf"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_1.pdf"),
          tooltitle: "Protection",
          tweet: "The threats and needs of each individual can be very different. Let’s see how to find a tailor-made solution.",
          title: "What and from whom to protect yourself?",
        },
        {
          subskills: [
            {
              title: "Create Multiple Email Addresses",
              abstract: "First, it is possible to have email addresses for different things. Typically: one for friends, one for work and one for unimportant business, one for health, etc. In this way, we separate the various aspects of our life and it becomes more difficult to connect them.",
            },
            {
              title: "Create Multiple Online Accounts",
              abstract:"In the same way, it is possible to create several accounts on social networks, with different pseudonyms, and to set them up differently depending on the people we want to address. Thus, only the people we choose according to the context will be able to access our online publications and it will be difficult for them to switch from one account to another without our agreement.\n"+
              "To reinforce this protection, a good practice is to limit the amount of information we give to online platforms to what we think is useful. Just because they ask for it (even nicely) does not mean we are obliged to provide it. We can always provide more!",
            },
            {
              title:"Multiply Its Digital Tools",
              abstract:"To make this compartmentalization even more effective, you can also use multiple web browsers, multiple sessions on your computer, or different machines altogether. While these methods alone won’t completely prevent large companies from monitoring you, the tracks you leave each time you use an online service will be scattered and harder to connect.\n"+
              "To learn more about this and other ways to protect your data, I recommend you read this  https://myshadow.org/ published by Tactical Tech",
              links: [
                {
                  text: "article",
                  link: "https://myshadow.org/"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_2.pdf"),
          tooltitle: "Identities",
          tweet: "A simple way to better protect our personal data is to partition the different areas of our lives (e.g., friends, work, health, shopping, etc.). This practice consists on separating our digital spaces. But how to do it?",
          title: "Partitioning our different areas of life (identities)",
        },
        {
          subskills: [
            {
              title: "Choose a Password",
              abstract: "Whether you choose to use a pattern, a sequence of numbers or a password, one of the most important measures to protect your data is to set up an unlock code. After each screen shutdown, it will be necessary to enter it in order to access the data contained in the device.\n"+
              "To set or change it, go to your phone’s Settings, then to the Fingerprint, Face & Password menu. To set the delay before the screen is automatically turned off in case of inactivity, go to the Display & Brightness menu.\n"+
              "For more security, I recommend that you avoid easily circumvented methods such as biometric methods and drawings or codes that are too easy to guess (such as the shape of the first letter of your first name or your date of birth).\n"+
              "To learn more, here are https://securityinabox.org/en/passwords/passwords-and-2fa/ to choose strong passwords and a https://securityinabox.org/en/passwords/tools/ to using a password manager.",
              links: [
                {
                  text: "recommendations",
                  link: "https://securityinabox.org/en/passwords/passwords-and-2fa/"
                },
                {
                  text: "guide",
                  link: "https://securityinabox.org/en/passwords/tools/"
                },
              ],
            },
            {
              title:"Copy Your IMEI Code",
              abstract:"The IMEI code is a number that uniquely identifies your phone. You can get it by dialing *#06#. You will also find it on the box and the invoice of your device. Keep it carefully: you will be asked to block your phone in case of theft.",
            },
            {
              title:"Keep your phone up to date",
              abstract:"Install updates, always and as soon as possible. In addition to new features, they often include bug fixes.\n"+
              "So you don’t have to think about it, go to the Settings of your app store, and then to the Updates menu if you use the Play Store. Here, tap to Enable automatic updates or Automatically fetch updates, and set the frequency of checking.",
            },
            {
              title:"Encrypt your phone",
              abstract:"Most phones offer encryption of the data they contain, meaning that only the person who knows the unlocking password can access the data.\n"+
              "Depending on your Android version the process and method may vary. In case the option is not already activated by default and if it is available, you can encrypt the memory of your phone or your micro SD card. To do this, go to the phone’s Settings, then to the Security menu.\n"+
              "Be careful, though! For an SD card, the operation starts by deleting all the data, so make sure you have a copy. And in any case, if you lost the password, your data would really be lost too.",
            },
            {
              title:"Don’t forget the metadata",
              abstract:"When we take pictures, our cell phone automatically adds other information such as the date, the geolocation, the camera setting. This is the metadata. This information is used to classify our photos, but it can reveal much more information than the image itself!\n"+
              "So, remember to check them before sharing your pictures, or just delete them. For this, you can use the Scrambled Exif application (with https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/ or the https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif).          ",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif"
                },
              ],
            },
            {
              title:"Disable Unnecessary Features",
              abstract:"When not in use, turn off geolocation, wifi and Bluetooth. By doing this, you will limit the collection of data about you and your device and improve its security and battery life. To do this, place your finger on the status bar, slide it down, and press the corresponding icons.",
            },
            {
              title:"Prefer sites to applications",
              abstract:"Generally speaking, it is better to use the website of an online service than its dedicated application: you will limit the data it can access on your phone.",
            },
            {
              title:"Reset your new and old devices",
              abstract:"If you get a phone back or part with yours, don’t forget to erase all the data it contains. This short https://securityinabox.org/en/phones-and-computers/physical-security/#decide-how-you-will-dispose-of-sensitive-information explains how to do this in practice.",
              links: [
                {
                  text: "guide",
                  link: "https://securityinabox.org/en/phones-and-computers/physical-security/#decide-how-you-will-dispose-of-sensitive-information"
                },
              ],
            },
            {
              title:"Take Stock on a Regular Basis",
              abstract:"No IT solution is 100% reliable, not forever. Don’t forget to keep an eye on the subject and ask yourself from time to time if your former choices are still relevant and if they still suit you.",
            },
            {
              title:"Don’t Forget the Details",
              abstract:"When it comes to computer security, a system is only as strong as its weakest link: it can take just one small detail to compromise the whole system. So, we must also pay attention to what may seem like small details.\n"+
              "For example, I recommend that you replace the default keyboard (Gboard) installed on your phone with a free and data-friendly keyboard. The one I recommend is OpenBoard or Android AOSP, which you can install from the  https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/ or https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin. To use it, go to the phone’s Settings, then to Other settings, then to Keyboard and input method to choose OpenBoard or AOSP and configure it.",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin"
                },
              ],
            },
            {
              title:"Adopt a Respectful Attitude",
              abstract:"Beyond all these recommendations, protecting your data and those of your loved ones also involves adopting attitudes that are respectful of each other. For example, don’t forget to ask the person concerned if you can share information about him or her, don’t save everything by default and minimize the amount of data you produce, help each other, chat and share your findings…\n"+
              "And in order to reach really satisfactory solutions and to leave no one by the wayside, it is necessary to organize ourselves collectively to improve the laws and the digital tools we use to protect ourselves. If you are interested in this adventure or simply want to follow the news, I recommend that you visit the https://laquadrature.net/ of La Quadrature du Net.\n"+
              "To go further, you will find many other recommendations on the https://datadetoxkit.org/ website and in the very complete https://holistic-security.tacticaltech.org/.",
              links: [
                {
                  text: "website",
                  link: "https://laquadrature.net/"
                },
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/"
                },
                {
                  text: "Holistic Security Manual",
                  link: "https://holistic-security.tacticaltech.org/"
                },
              ],
            }
          ],
          tweet: "In addition to all the other skills, there are a few simple things we can do to easily and quickly improve the security of our phones. Some people even call them digital hygiene rules! ",
          //pdf: require("assets/pdf/Ajay_3.pdf"),
          tooltitle: "Protecting",
          title: "Protecting our data and those of our loved ones",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Sally" },
  { key: "Pierre" },
  { key: "Molly" },
  {
    key: "Alex",
    about: "28 years old. Divorced (ex-spouse user#1547699, username \"Ben\"). 1 infant (\"Ulysse\"). Unemployed. Top player (Gao the Intruder).",
    image: require("assets/images/community/avatars/B-Alex.png"),
    informationShared: "Contacts, Geolocation, Storage, Wifi, Network, Notifications, SMS, Bluetooth, NFC, Alarm, Wallpaper, Microphone, Fingerprint reader, Calendar, Camera, Accelerometer",
    overview: "Sociable, efficient at work, lack of self-confidence, 86% probability of being depressed, invested in Gao Games (chat moderator), close to Blaze.",
    recommendations: "Emotional state data collected (internal use): Psychologically fragile profile likely to invest a lot of volunteer time in Gao Games in return for recognition. Suggest to give responsibilities. \n"+
      " Internal note (manager): To be monitored manually. Suspicious activities following Blaze's disappearance.",
    enabled: true
  },
  {
    key: "Ally",
    about: "35 years old. Single. No children. Graphic designer at Gao Games. Top player (Gao the Athlete).",
    image: require("assets/images/community/avatars/B-Ally.png"),
    informationShared: "Accelerometer, Contacts, Geolocation, Storage, Wifi, Network, Notifications, SMS, Bluetooth, NFC, Alarm, Wallpaper, Microphone, Fingerprint, Calendar, Camera",
    overview: "Efficient, organized, low empathy, good planner, health problems.",
    recommendations: "Health data collected (customer sale #16854 - health insurance provider): persistent overweight despite regular exercise, 72% probability of thyroid problems, increased risk of cardiovascular accident. Unprofitable subject. Consider termination of health insurance or fee increase to premium.",
    thought: {
      title: 7,
      tweet: "Robots and Humans",
      overview: "Robots can make mistakes too!",
      message: "Digital tools are becoming more and more influential in our daily lives and assist us in an ever-increasing number of activities. Without us necessarily being aware of it, decisions affecting our lives are made by machines. Or rather … by the people who designed them!\n"+
      "Of course, these machines are far from infallible: their computer programs can malfunction, or the data used to make these choices can be incorrect. In other cases, the people who designed these programs imagined that our bodies and our uses would resemble theirs, and … https://www.vox.com/the-goods/2019/4/17/18412450/tsa-airport-full-body-scanners-racist/.\n"+
      "All of these potential problems can lead to the same result: people cannot use these tools normally or benefit from the services they provide. Unfortunately, the people who experience these difficulties the most are often those who are already victims of other forms of inequality. For example, female voices or dark skin are less well recognized by many digital tools than male voices and light skin. Boring, when you want to use the voice command of your GPS or a facial recognition application! If you are interested in this topic, you can listen to this https://www.binge.audio/podcast/les-couilles-sur-la-table/des-ordis-des-souris-et-des-hommes/.\n",
      links: [
        {
          text: "they were wrong",
          link: "https://www.vox.com/the-goods/2019/4/17/18412450/tsa-airport-full-body-scanners-racist/"
        },
        {
          text: "podcast (French only)",
          link: "https://www.binge.audio/podcast/les-couilles-sur-la-table/des-ordis-des-souris-et-des-hommes/"
        }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Cleaning Up Previously Granted Authorizations",
              abstract: "When you use an application for the first time, it must ask your permission before accessing your data or features on your phone. For example, it will be about geolocation, microphone, cameras, contact list… Potentially sensitive data and functions!\n"+
              "Over time, it may be difficult to remember what permissions you have or have not granted to a particular application. To check and change them, go to your phone’s settings, then to the menu dedicated to permissions or applications.",
            },
            {
              title: "Where can I find the list of authorizations?",
              abstract: "You will find the complete list of applications installed on your device and the permissions granted to each application. If you search by “permissions,” you will be able to see all the applications that access specific permission (for example, all the applications that access your geolocation or your contacts). From these menus you can either uninstall the applications or refuse the authorizations already granted simply by pressing them.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss4.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss5.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss6.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss7.png")
                },
              ],
            },
            {
              title: "Get detailed information",
              abstract: "To get even more detailed information about your phone’s permissions, don’t hesitate to use Nikki’s “scanner”! (if you don’t know what it is, you’ve still got a lot to discover here…)\n"+
              "This will allow you to know the number of trackers of the installed applications and to follow your progress in the protection of your data on the phone over time.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss8.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss9.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_1.pdf"),
          tooltitle: "Permissions",
          tweet: "To function, some applications require permission to access different information and features of our devices. For example, a “camera” application asks for access to the camera. Sometimes, these access requests have nothing to do with the functioning of the application and are only intended to collect personal data about us. For example, a music player that asks to have access to your contacts and your movements. Let’s see how to configure them to better protect us!",
          title: "Configure the permissions granted to applications on your phone ",
          showCheckBoxes: true
        },
        {
          subskills: [
            {
              title: "Control the uses of your data",
              abstract: "It is possible to contact private or public organizations that collect, use or sell our personal data, and to request information, corrections, deletions or copies of our data.\n"+
              "This concerns services opened for European residents or its citizens in other countries. Each country of the European Union has its own institution in charge of checking the good application of these protections. https://edpb.europa.eu/about-edpb/about-edpb/members_en is the list of countries. And for the other countries of the world, I recommend you this https://www.cnil.fr/en/la-protection-des-donnees-dans-le-monde.\n"+
              "In France, it is the National Commission of Data Processing and Liberties (CNIL) which is in charge of helping us to assert our rights in this matter and offers us guides on our https://www.cnil.fr/en/les-droits-pour-maitriser-vos-donnees-personnelles and https://www.cnil.fr/modeles/courrier letters to assert them easily.\n"+
              "Let’s imagine: while searching for your name and surname on the Internet, you find personal information that you do not wish to be published. The first step to remove it is to write to the contact address indicated on the site in question. Once you have sent your request, the people who manage the site are obliged to answer you within 30 days. In many cases, this contact alone is enough to get a satisfactory response.",
              links: [
                {
                  text: "Here",
                  link: "https://edpb.europa.eu/about-edpb/about-edpb/members_en"
                },
                {
                  text: "detailed map",
                  link: "https://www.cnil.fr/en/la-protection-des-donnees-dans-le-monde"
                },
                {
                  text: "different rights",
                  link: "https://www.cnil.fr/en/les-droits-pour-maitriser-vos-donnees-personnelles"
                },
                {
                  text: "models",
                  link: "https://www.cnil.fr/modeles/courrier"
                },
              ],
            },
            {
              title: "File a Complaint",
              abstract:"If this information is not removed after 30 days, you can send https://www.cnil.fr/en/plaintes to the CNIL which can sanction the site in case of a fault. Unfortunately, the CNIL only has a few resources (and therefore people) to deal with the messages, so their response may take some time. If 30 days pass without any answer from the CNIL, you can then https://www.service-public.fr/particuliers/vosdroits/F1435 directly with the police, the gendarmerie or the public prosecutor to assert your rights.",
              links: [
                {
                  text: "a complaint",
                  link: "https://www.cnil.fr/en/plaintes"
                },
                {
                  text: "file a complaint",
                  link: "https://www.service-public.fr/particuliers/vosdroits/F1435"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_2.pdf"),
          tooltitle: "Rights",
          tweet: "The European Union guarantees the protection of the data of its citizens and also of the people residing in their territory. Since its implementation in 2018, the General Data Protection Regulation (GDPR) allows citizens and residents of the European Union to better control the uses made of their data. Let’s see what it’s all about!",
          title: "Protecting you across borders!",
        },
        {
          subskills: [
            {
              title: "Browsing the Web With the Tor Browser",
              abstract: "The easiest and most effective way to protect yourself from bugs and maintain your privacy is to use the https://www.torproject.org/en/download/#android.This Firefox-based web browser is a free and open source app, and is specifically designed to bypass surveillance and censorship.\n"+
                "Please note even though Tor Browser is very efficient, it does not exempt you from continuing to browse in HTTPS or from updating your browser regularly. Also, you may still be recognized by the websites you visit in certain situations, for example if you decide to log in with an account.",
              links: [
                {
                  text: "Tor Browser",
                  link: "https://www.torproject.org/en/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Surfing the Web With Duckduckgo Browser",
              abstract: "		If your connection isn’t fast enough or some of the websites you want to visit block Tor Browser, the alternative I recommend is to use the https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android browser (also available in the F-Droid App Store). It’s a free web browser that blocks trackers and informs you about the privacy of the sites you visit. Be careful, though, it won’t make your connection totally anonymous (sites will know your IP address and where you are).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "When we surf the web, cookies follows us (https://myshadow.org/browser-tracking) from site to site to deduce our tastes, our interests and a whole lot of other things about us. Let’s see how to protect ourselves!",          
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Tracking",
          title: "Protect yourself from the bugs that follows your navigation",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Fred" },
  {
    key: "Amin",
    about: "Between 36 and 43 years old. Married (spouse user#6874215, username \"Masako\"). 1 infant (\"Alma\"). Quality engineer. Top player (Gao the Filekeeper).",
    image: require("assets/images/community/avatars/B-Amin.png"),
    informationShared: "Storage, SMS, Alarm, Microphone, Calendar, Camera, Contacts, Geolocation, Wifi, Network",
    overview: "Calm, conscientious, law-abiding, committed with a bank loan (studies), sick daughter (degenerative disease), anxious, nostalgic, dreamer.",
    recommendations: "Emotional state data collected (client sale #03548 - advertising agency): subject under stress (46% daughter's illness, 28% difficult management with wife, 26% overworked) emotionally responsive. Desires to travel. 79% likely to purchase tourist travel services (\"escape\" type).",
    thought: {
      title: 8,
      tweet: "Manipulation",
      overview: "Interfaces designed to mislead and manipulate us",
      message: "On the Internet and in our phones, many companies use fake interfaces, carefully designed to manipulate us. In English, they are called dark patterns. Thanks to deliberately misleading notifications, presentations and color choices, preselected options, hard-to-find information … it is possible to make us accept things that we would probably have refused if they had been presented clearly. \n"+
      "These companies know how to use the mechanisms of our brain related to the processes of pleasure and reward and make us want to use their applications and services, to spend more and more time on them, sometimes to the point of causing real addictions.\n"+
      "All these efforts are aimed at collecting more and more data about us to know us better, to push us to consume. For example, they try to determine the moments when we are tired or when we lack confidence, and they take advantage of this to make us buy things that we would have refused the rest of the time. I contributed to this manipulation with the Gao Games, it was a mistake on my part. When I understood it, I left.\n"+
      "To learn more about this, you can watch this video https://www.arte.tv/en/videos/RC-017841/dopamine/ from ARTE, and this https://www.internetactu.net/2016/06/16/du-design-de-nos-vulnerabilites/ .\n",
      links: [
        {
          text: "series",
          link: "https://www.arte.tv/en/videos/RC-017841/dopamine/"
        },
        {
          text: "article",
          link: "https://www.internetactu.net/2016/06/16/du-design-de-nos-vulnerabilites/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              abstract: "To do this, I recommend that you add the https://ublockorigin.com/ extension to your web browser, and install the https://play.google.com/store/apps/details?id=org.blokada.alarm.dnschanger and/or https://f-droid.org/en/packages/org.adaway/ applications (if you use the F-Droid application store).\n"+
                "Once you have installed one of these applications, all you have to do is to launch it and activate its protection (you’ll see, it’s very simple). And to learn more about how to block advertising on your computer, I suggest you visit https://bloquelapub.net/",
              title: "Installing an ad blocker",
              links: [
                {
                  text: "uBlock Origin",
                  link: "https://ublockorigin.com/"
                },
                {
                  text: "Blokada",
                  link: "https://play.google.com/store/apps/details?id=org.blokada.alarm.dnschanger"
                },
                {
                  text: "AdAway",
                  link: "https://f-droid.org/en/packages/org.adaway/"
                },
                {
                  text: "bloquelapub.net",
                  link: "https://bloquelapub.net/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss12.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss13.png")
                },
              ],
            },
          ],
          tweet: "Advertising can be the door to malicious sites or the installation of viruses on our devices. It also poses many other problems such as computer pollution, slowing down our browsing, increasing energy consumption, spreading stereotypes, encouraging overconsumption. Fortunately, it is rather simple to get rid of it on your phone (and computers)!",
          title: "Block advertising",
          //pdf: require("assets/pdf/Amin_1.pdf"),
          tooltitle: "Publicity",
        },
        {
          subskills: [
            {
              abstract: "The more time we spend on the tools of the Net giants, the more the amount of data they collect on us increases, and the more their profits become important. But also the risk that we develop addictive and compulsive behaviors. The worst thing is that these companies then try to make us believe that it is our fault and https://www.ted.com/talks/tristan_harris_how_better_tech_could_protect_us_from_distraction !\n"+
              "To learn more, I recommend that you read the http://www.internetactu.net/2019/01/14/retro-design-de-lattention-cest-complique/ of the New Generation Internet Foundation (Fing) on this subject.\n"+
              "Recognizing the mechanisms that exploit our unconscious is complicated: they are designed to be difficult to identify. I invite you to watch as much as you like https://www.arte.tv/en/videos/RC-017841/dopamine/ where we understand how this hormone is stimulated by some of the applications on our phones. Understanding is already acting!\n"+
              "But it’s really important to be able to do this and to be able to get around them in order to stay in control and have a healthy relationship with digital tools. Here are some solutions!",
              title: "Understanding This Issue",
              links: [
                {
                  text: "make us feel guilty",
                  link: "https://www.ted.com/talks/tristan_harris_how_better_tech_could_protect_us_from_distraction"
                },
                {
                  text: "conclusions",
                  link: "http://www.internetactu.net/2019/01/14/retro-design-de-lattention-cest-complique/"
                },
                {
                  text: "the Arte miniserie “Dopamine”",
                  link: "https://www.arte.tv/en/videos/RC-017841/dopamine/"
                }
              ],
            },
            {
              abstract: "First of all, start by taking stock of yourself: what do you want to do with your device? What connected uses do you find useful and interesting? Which ones would you rather stop? What negative effects do you want to limit? What you want to give to these objects in your life ...?\n"+
                "Of course, the answers to these questions will not be the same for everyone: some people will find an online service truly rewarding, while others find it a waste of time. It doesn’t matter: the important thing is that you answer these questions honestly and find the answers that work for you, so that you can redirect your attention to what you feel is really worthwhile.\n"+
                "The first habit to adopt is to avoid rushing and to take the time to really read what the sites and applications are offering you. The more websites or services press you to make a decision, the more important it is to make sure you understand what you are about to click on.\n"+
                "Another good habit to adopt is to determine times and places where you choose not to use your digital devices. For example, in the room where you sleep, or only in your bed, in the kitchen, during meals, before going to bed, when you are with someone… You can even chat about it with your loved ones to decide on common rules.",
              title: "Taking Stock and Developing Good Habits",
            },
            {
              abstract: "A good reflex is to systematically be wary of automatic suggestions and invitations to consult an application, especially when they use the register of emotion. To be able to refuse what the device suggests that you do, start by taking the time to ask yourself if you really want to do it.\n"+
                "There is also a technical solution to prevent your phone from disturbing you and to help you check it only when you decide to: disable notifications. To disable them completely, go to your phone’s Settings, then to the Notifications and Status Bar menu and Notification Prompt. Finally, select No Prompt.",
              title: "Turn Off All Notifications",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss14.png")
                },
              ],
            },
            {
              abstract: "If you prefer, it is also possible to choose a finer setting and allow or disallow notifications per application. To do this, go to the phone’s Settings, then to the Notifications and status bar menu and to Manage notifications. There you will find the list of applications installed on your device. By tapping on any of them, you will be able to uncheck Allow notifications or configure them precisely.",
              title: "Refine Your Configuration",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss15.png")
                },
              ],
            },
            {
              abstract: "Another way is to activate the Do Not Disturb mode for a while. To do this, place your finger on the status bar, slide it down, and press the Do Not Disturb icon.\n"+
              "It is also possible to automatically activate this mode during the time slots and days of your choice (for example, to be able to wake up and go to bed quietly), as well as to decide which elements are blocked or allowed (for example, calls from certain contacts or repeated calls). To do this, go to the phone’s Settings and then to the Do Not Disturb menu.",
              title: "Use the “Do Not Disturb” Mode",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss16.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss17.png")
                },
              ],
            },
            {
              abstract: "Another way to stay in control is to limit the use of your device or certain applications to a certain time. Once the time is up, the application in question will be disabled for the rest of the day.\n"+
                "Obviously, this indicator http://www.internetactu.net/2019/01/15/retro-design-de-lattention-depasser-le-temps/. It’s up to you to choose what you think is useful and important! To set up these settings, go to the phone’s Settings, then to the Digital wellbeing & parental controls menu and press Dashboard in Ways to disconnect section.\n"+
                "In this menu you can measure the time you spend on your device and the time you spend using each application. This is also where you can configure the Sleep mode, which allows you to switch your screen to black and white, so that it attracts your attention less. Also in this menu, you can configure the Distraction Free mode, which is similar to the Do Not Disturb setting that we have already seen above.",
              title: "Limit your usage time",
              links: [
                {
                  text: "measures the time spent using an application, but not its usefulness",
                  link: "http://www.internetactu.net/2019/01/15/retro-design-de-lattention-depasser-le-temps/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss18.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss19.png")
                },
              ],
            },
            {
              abstract: "Of course, we can think of many other solutions: put the least useful applications away from the home screen, choose a plan with a limited connection, impose breaks, put a wallpaper that encourages you to ask yourself if you really want to use your device, set your screen to be black and white… Some will work on some people but not on others.\n"+
              "The https://datadetoxkit.org/en/wellbeing/essentials#step-1 pWebsite offers many other solutions. And if these devices really take up more space in your life than you want, don’t hesitate to talk about it with your loved ones or an addictologist.",
              title: "Find a Customized Solution",
              links: [
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/en/wellbeing/essentials#step-1"
                },
              ],
            }
          ],
          tweet: "In order to monopolize our attention and therefore our data, the Internet giants exploit our subconscious and our brains. This is what we call “cognitive and psychological bias.” Let’s see how to protect ourselves!",
          title: "Recognize and circumvent the mechanisms that manipulate us",
          //pdf: require("assets/pdf/Amin_2.pdf"),
          tooltitle: "Manipulation",
        },
        {
          subskills: [
            {
              title: "Browsing the Web With the Tor Browser",
              abstract: "The easiest and most effective way to protect yourself from bugs and maintain your privacy is to use the https://www.torproject.org/en/download/#android.This Firefox-based web browser is a free and open source app, and is specifically designed to bypass surveillance and censorship.\n"+
              "Please note even though Tor Browser is very efficient, it does not exempt you from continuing to browse in HTTPS or from updating your browser regularly. Also, you may still be recognized by the websites you visit in certain situations, for example if you decide to log in with an account.",
              links: [
                {
                  text: "Tor Browser",
                  link: "https://www.torproject.org/en/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Surfing the Web With Duckduckgo Browser",
              abstract: "If your connection isn’t fast enough or some of the websites you want to visit block Tor Browser, the alternative I recommend is to use the https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android browser (also available in the F-Droid App Store). It’s a free web browser that blocks trackers and informs you about the privacy of the sites you visit. Be careful, though, it won’t make your connection totally anonymous (sites will know your IP address and where you are).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "When we surf the web, cookies follows us (https://myshadow.org/browser-tracking) from site to site to deduce our tastes, our interests and a whole lot of other things about us. Let’s see how to protect ourselves!",          
          //pdf: require("assets/pdf/Ally_3.pdf"),
          tooltitle: "Tracking",
          title: "Protect yourself from the bugs that follows your navigation",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "John" },
  { key: "Judith" },
  { key: "Bill" },
  {
    key: "Nikki",
    enabled: true,
    selected: true,
    about: "Eliott Anderson. 123 years old. Divorced. 47 children. Elephant trainer. Favorite color: chocolate banana. Top player (Gao the Spy).",
    image: require("assets/images/community/avatars/B-Nikki.png"),
    recommendations: "Data collected not usable.",
    overview: "null",
    informationShared: "null",
  },
  { key: "Bob" },
  { key: "Eugene" },
  { key: "Maria" },
  { key: "Mariane" },
  { key: "Billy" },
  { key: "Devin" },
  { key: "Dan" },
  {
    key: "Rokaya",
    about: "26 years old. Single. No children. Colorist / influencer. Top player (Gao the Photographer).",
    image: require("assets/images/community/avatars/B-Rokaya.png"),
    informationShared: "Camera, Storage, Accelerometer, Contacts, Geolocation, Wifi, Network, Bluetooth, NFC, Wallpaper, Microphone, Calendar",
    overview: "Influential, artistic, self-confident, dynamic, popular.",
    recommendations: "Advertising recommendations (client sale #69755 - advertiser): very popular user on social networks. Contract signed for the sale of sponsored advertising space in her publications (according to general terms of use). \n"+
      "Internal note 1: Attention, the user has contacted the advertising department to complain about the association with one of our product advertisers. She explains that this brand is linked to a “child slave labor” scandal, and does not wish to support their activity. Awaiting response.\n"+
      "Internal note 2 (manager's response): ignore the user's request, the contract allows us to make all types of advertising associations with her photos.",
    thought: {
      title: 11,
      tweet: "“Tools at our service?”",
      overview: "Tools at our service? Are they really?",
      message: "Using the services of the “Net Giants” has its advantages: they are easy to use, very often free and popular. But these services answer foremost to the interest of these companies: the needs of the users are secondary.\n"+
      "According to their wishes and objectives, they can delete our account or some of our publications, limit access, add or remove features, not show us certain publications or on the contrary only put forward certain opinions … or even sell the activity to another company, or stop it altogether. Very important powers, especially when a lot of people use these services.\n"+
      "Fortunately, there are alternatives that work just as well and give us back the ability to decide! The association Framasoft proposes for example many alternative tools to those of the giants of the Net on the site  https://degooglisons-internet.org/en/.\n",
      links: [
        {
          text: "degooglisonsinternet.org",
          link: "https://degooglisons-internet.org/en/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Use Another Store",
              abstract: "To bypass this predatory company and regain control of your data, there are more ethical, free and open source stores, such as the https://f-droid.org/en/packages/com.aurora.store/. This one allows you to download the same applications as the Play Store, but without needing a Google account and without collecting your personal data for that.\n"+
                "I particularly recommend that you use https://f-droid.org/en/. It only offers you free applications, free and more respectful of your data. Of course, you won’t find all the applications from other stores there! But I bet that alternatives to the ones you’re looking for are there and that you’ll quickly find one that suits you.",
              links: [
                {
                  text: "Aurora Store",
                  link: "https://f-droid.org/en/packages/com.aurora.store/"
                },
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/en/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss20.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss21.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss22.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss23.png")
                },
              ],
            },
            {
              title: "How to install them?",
              abstract: "To use these stores, you must allow them on the menu that allows the installation of applications from unknown sources. To do this, go to your phone’s settings, then to the Security or Device and Privacy menu (depending on your Android version).",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss24.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss25.png")
                },
              ],
            },
            {
              title: "And what about downloading apps from websites?",
              abstract: "Finally, I strongly advise you against installing applications without going through a dedicated store (with an .apk file). This practice is dangerous and makes you run the risk of installing applications modified by malicious ones.",
            }
          ],
          //pdf: require("assets/pdf/Rokaya_1.pdf"),
          tooltitle: "Applications",
          tweet: "Many people use the Google Play Store to choose and install their applications. But this is not the only solution!",
          title: "Change application store",
        },
        {
          subskills: [
            {
              title: "Visit Directories",
              abstract: "To help us find our way among all the existing solutions, sites such as https://prism-break.org/en/categories/android/ and https://degooglisons-internet.org/en/ present lists of ethical services and software. By browsing these pages, we have concrete proposals to replace them.",
              links: [
                {
                  text: "PRISM-Break.org",
                  link: "https://prism-break.org/en/categories/android/"
                },
                {
                  text: "Dégooglisons Internet",
                  link: "https://degooglisons-internet.org/en/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss26.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss27.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss28.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss29.png")
                },
              ],
            },
            {
              title: "Find a Host",
              abstract:"I also invite you to have a look at the Collective of transparent open neutral and united alternative hosts, the https://entraide.chatons.org/en. It gathers structures offering ethical online services and meeting demanding criteria. With a bit of luck, you might be able to find one https://chatons.org/en/find-by-localisation !",
              links: [
                {
                  text: "CHATONS",
                  link: "https://entraide.chatons.org/en"
                },
                {
                  text: "near you",
                  link: "https://chatons.org/en/find-by-localisation"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss30.png")
                },
              ],
            },
            {
              title: "Knowing Important Criteria",
              abstract:"In general, here are 3 essential criteria for an online service to be secure and protective. It must be:\n"+
              "• Free: that is, anyone can study how it works, modify it to suit their desires, and then share the result. This way, we can be more sure that the service does what it claims, and nothing else. To learn more about this ecosystem, I invite you to consult the work of the https://www.april.org/en/articles/intro.\n"+
              "• End-to-end encryption: With this method of protecting data, only those for whom it is intended can currently do so. And nobody else. Not even the people who administer the service in question.\n"+
              "• Decentralized: this means that the digital tool should not depend on a single authority, which could impose its decisions on everyone overnight. Thus, it is a common good: it belongs to everyone and no one can appropriate it to the detriment of others.\n"+
            "To go further, I recommend you read the guide https://ssd.eff.org/module/choosing-your-tools proposed by the site Surveillance Self-Defense.",
              links: [
                {
                  text: "April association",
                  link: "https://www.april.org/en/articles/intro"
                },
                {
                  text: "Choosing your tools",
                  link: "https://ssd.eff.org/module/choosing-your-tools"
                },
              ]
            }
          ],
          //pdf: require("assets/pdf/Rokaya_2.pdf"),
          tooltitle: "Substitute",
          tweet: "Finding a digital tool that really fits your needs, desires and values can be complicated. Let’s see how to do it in practice!",
          title: "Search for an ethical online service ",
        },
        {
          subskills: [
            {
              title: "Understanding this issue",
              abstract: "Although we accept them mechanically, these boxes that we check before accessing a service can have important consequences, especially for the protection of our data and the ownership of the content we produce (our comments, publications, videos…).\n"+
              "Unfortunately, the TOS of most online services are simply https://www.vice.com/en/article/xwbg7j/la-plupart-des-termes-dutilisation-en-ligne-sont-incomprehensibles and sometimes even illegal: https://www.numerama.com/tech/675927-pourquoi-google-affiche-jugement-du-tgi-de-paris-sur-sa-page-daccueil.html, https://www.nextinpact.com/article/29309/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement, https://www.lemonde.fr/pixels/article/2020/06/15/apple-condamne-en-france-pour-les-conditions-abusives-d-itunes-et-apple-music_6042948_4408996.html and many others have even been sued for this. Moreover, these companies usually give themselves the possibility to change these contents whenever they want.",
              links: [
                {
                  text: "incomprehensible",
                  link: "https://www.vice.com/en/article/xwbg7j/la-plupart-des-termes-dutilisation-en-ligne-sont-incomprehensibles"
                },
                {
                  text: "Google",
                  link: "https://www.numerama.com/tech/675927-pourquoi-google-affiche-jugement-du-tgi-de-paris-sur-sa-page-daccueil.html"
                },
                {
                  text: "Facebook",
                  link: "https://www.nextinpact.com/article/29309/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement"
                },
                {
                  text: "Apple",
                  link: "https://www.lemonde.fr/pixels/article/2020/06/15/apple-condamne-en-france-pour-les-conditions-abusives-d-itunes-et-apple-music_6042948_4408996.html"
                },
              ],
            },
            {
              title: "Find the Essential Information",
              abstract: "Fortunately, some online services are more virtuous than these giants of the Net. The members of the https://tosdr.org/ (ToS;DR) project have taken the trouble to summarize and explain these contracts in an understandable way. It can help us to choose the services we want to use!\n"+
                "The https://disinfo.quaidorsay.fr/en/cgus tool allows you to follow and find the evolutions of the CGU of the main online service providers. Useful if you want to compare the different versions over time!",
              links: [
                {
                  text: "Terms of Service; Didn't Read",
                  link: "https://tosdr.org/"
                },
                {
                  text: "Open Terms Archive",
                  link: "https://disinfo.quaidorsay.fr/en/cgus"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss31.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss32.png")
                },
              ],
            }
          ],
          tweet: "To use an online service, you generally have to accept its general terms of use (or GTU) even in cases where you cannot refuse them anyway! Let’s see how to find our way through these complicated documents.",
          //pdf: require("assets/pdf/Rokaya_3.pdf"),
          tooltitle: "Contracts",
          title: "Understanding the contracts that bind us to online services",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Dominic" },
  { key: "Jackson" },
  { key: "Sebastian" },
  { key: "Egaeus nobot" },
  { key: "James" },
  {
    key: "Masako",
    about: "Between 35 and 42 years old. Married (spouse user#8765489, username \"Amin\"). 1 infant child (\"Alma\"). Engineer in materials. Top player (Gao the Forger).",
    image: require("assets/images/community/avatars/B-Masako.png"),
    informationShared: "Wallpaper, Storage, SMS, Accelerometer, Microphone, Calendar, Camera, Contacts, Geolocation, Wifi, Network",
    overview: "Energetic, adventurous, independent, creative, persevering, sick daughter (degenerative disease), involved.",
    recommendations: "Employability data collected (customer sale #35468 - social network and professional recruiter): recent career change. Analysis of message exchanges and online searches indicates a decrease in professional motivation (switch to part-time work, decrease in business-oriented conversations) and an increase in family concerns (significant consultation of children's photos, tension with husband). Indicate to HR recruiters a 27% drop in employability score.",
    thought: {
      title: 10,
      tweet: "Collective data?",
      overview: "Personal data or collective data?",
      message: "Although we generally speak of “personal data,” a large part of what the “Net Giants” exploit commercially is collective data. This is what we call social graphs (or social networks): the connections and relationships between people.\n"+
      "By being able to collect information about someone’s opinions and tastes, these companies can easily deduce those of the people around them. And since communications by nature involve several people, their confidentiality depends on the choices of all participants. If only one recipient of our messages cares less about privacy than the others, even the most protective tools are much less effective.\n"+
      "Taking care of our personal data is also taking care of those of our loved ones. For example, we can ask ourselves whether the people we see in our photos really agree to their being put on the Internet or shared. And this does not only concern human beings: poachers https://qz.com/206069/geotagged-safari-photos-could-lead-poachers-right-to-endangered-rhinos/ the metadata of images posted on the Internet by tourists to locate and hunt animals. To learn more about the collective dimension of “personal” data, you can read this https://scinfolex.com/2018/05/25/gdpr-online-privacy-is-a-collective-issue/.\n",
      links: [
        {
          text: "use",
          link: "https://qz.com/206069/geotagged-safari-photos-could-lead-poachers-right-to-endangered-rhinos/"
        },
        {
          text: "article",
          link: "https://scinfolex.com/2018/05/25/gdpr-online-privacy-is-a-collective-issue/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Choose a Password",
              abstract: "Whether you choose to use a pattern, a sequence of numbers or a password, one of the most important measures to protect your data is to set up an unlock code. After each screen shutdown, it will be necessary to enter it in order to access the data contained in the device.\n"+
              "To set or change it, go to your phone’s Settings, then to the Fingerprint, Face & Password menu. To set the delay before the screen is automatically turned off in case of inactivity, go to the Display & Brightness menu.\n"+
              "For more security, I recommend that you avoid easily circumvented methods such as biometric methods and drawings or codes that are too easy to guess (such as the shape of the first letter of your first name or your date of birth).\n"+
              "To learn more, here are https://securityinabox.org/en/passwords/passwords-and-2fa/ to choose strong passwords and a https://securityinabox.org/en/passwords/tools/ to using a password manager.",
              links: [
                {
                  text: "recommendations",
                  link: "https://securityinabox.org/en/passwords/passwords-and-2fa/"
                },
                {
                  text: "guide",
                  link: "https://securityinabox.org/en/passwords/tools/"
                },
              ],
            },
            {
              title:"Copy Your IMEI Code",
              abstract:"The IMEI code is a number that uniquely identifies your phone. You can get it by dialing *#06#. You will also find it on the box and the invoice of your device. Keep it carefully: you will be asked to block your phone in case of theft.",
            },
            {
              title:"Keep your phone up to date",
              abstract:"Install updates, always and as soon as possible. In addition to new features, they often include bug fixes.\n"+
              "So you don’t have to think about it, go to the Settings of your app store, and then to the Updates menu if you use the Play Store. Here, tap to Enable automatic updates or Automatically fetch updates, and set the frequency of checking.",
            },
            {
              title:"Encrypt your phone",
              abstract:"Most phones offer encryption of the data they contain, meaning that only the person who knows the unlocking password can access the data.\n"+
              "Depending on your Android version the process and method may vary. In case the option is not already activated by default and if it is available, you can encrypt the memory of your phone or your micro SD card. To do this, go to the phone’s Settings, then to the Security menu.\n"+
              "Be careful, though! For an SD card, the operation starts by deleting all the data, so make sure you have a copy. And in any case, if you lost the password, your data would really be lost too.",
            },
            {
              title:"Don’t forget the metadata",
              abstract:"When we take pictures, our cell phone automatically adds other information such as the date, the geolocation, the camera setting. This is the metadata. This information is used to classify our photos, but it can reveal much more information than the image itself!\n"+
              "So, remember to check them before sharing your pictures, or just delete them. For this, you can use the Scrambled Exif application (with  https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/ or the https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif).",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif"
                },
              ],
            },
            {
              title:"Disable Unnecessary Features",
              abstract:"When not in use, turn off geolocation, wifi and Bluetooth. By doing this, you will limit the collection of data about you and your device and improve its security and battery life. To do this, place your finger on the status bar, slide it down, and press the corresponding icons.",
            },
            {
              title:"Prefer sites to applications",
              abstract:"Generally speaking, it is better to use the website of an online service than its dedicated application: you will limit the data it can access on your phone.",
            },
            {
              title:"Reset your new and old devices",
              abstract:"If you get a phone back or part with yours, don’t forget to erase all the data it contains. This short https://securityinabox.org/en/phones-and-computers/physical-security/#decide-how-you-will-dispose-of-sensitive-information explains how to do this in practice.",
              links: [
                {
                  text: "guide",
                  link: "https://securityinabox.org/en/phones-and-computers/physical-security/#decide-how-you-will-dispose-of-sensitive-information"
                },
              ],
            },
            {
              title:"Take Stock on a Regular Basis",
              abstract:"No IT solution is 100% reliable, not forever. Don’t forget to keep an eye on the subject and ask yourself from time to time if your former choices are still relevant and if they still suit you.",
            },
            {
              title:"Don’t Forget the Details",
              abstract:"When it comes to computer security, a system is only as strong as its weakest link: it can take just one small detail to compromise the whole system. So, we must also pay attention to what may seem like small details.\n"+
              "For example, I recommend that you replace the default keyboard (Gboard) installed on your phone with a free and data-friendly keyboard. The one I recommend is OpenBoard or Android AOSP, which you can install from the https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/ or https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin. To use it, go to the phone’s Settings, then to Other settings, then to Keyboard and input method to choose OpenBoard or AOSP and configure it.",
              links: [
                {
                  text: "F-Droid",
                  link: "https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/"
                },
                {
                  text: "Play Store",
                  link: "https://play.google.com/store/apps/details?id=org.dslul.openboard.inputmethod.latin"
                },
              ],
            },
            {
              title:"Adopt a Respectful Attitude",
              abstract:"Beyond all these recommendations, protecting your data and those of your loved ones also involves adopting attitudes that are respectful of each other. For example, don’t forget to ask the person concerned if you can share information about him or her, don’t save everything by default and minimize the amount of data you produce, help each other, chat and share your findings…\n"+
              "And in order to reach really satisfactory solutions and to leave no one by the wayside, it is necessary to organize ourselves collectively to improve the laws and the digital tools we use to protect ourselves. If you are interested in this adventure or simply want to follow the news, I recommend that you visit the https://laquadrature.net/ of La Quadrature du Net.\n"+
              "To go further, you will find many other recommendations on the https://datadetoxkit.org/ website and in the very complete https://holistic-security.tacticaltech.org/.",
              links: [
                {
                  text: "website",
                  link: "https://laquadrature.net/"
                },
                {
                  text: "Data Detox",
                  link: "https://datadetoxkit.org/"
                },
                {
                  text: "Holistic Security Manual",
                  link: "https://holistic-security.tacticaltech.org/"
                },
              ],
            }
          ],
          tweet: "In addition to all the other skills, there are a few simple things we can do to easily and quickly improve the security of our phones. Some people even call them digital hygiene rules! ",
          //pdf: require("assets/pdf/Ajay_3.pdf"),
          tooltitle: "Protecting",
          title: "Protecting our data and those of our loved ones",
        },
        {
          subskills: [
            {
              title: "Visit Directories",
              abstract: "To help us find our way among all the existing solutions, sites such as https://prism-break.org/en/categories/android/ and https://degooglisons-internet.org/en/ present lists of ethical services and software. By browsing these pages, we have concrete proposals to replace them.",
              links: [
                {
                  text: "PRISM-Break.org",
                  link: "https://prism-break.org/en/categories/android/"
                },
                {
                  text: "Dégooglisons Internet",
                  link: "https://degooglisons-internet.org/en/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss26.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss27.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss28.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss29.png")
                },
              ],
            },
            {
              title: "Find a Host",
              abstract:"I also invite you to have a look at the Collective of transparent open neutral and united alternative hosts, the https://entraide.chatons.org/en. It gathers structures offering ethical online services and meeting demanding criteria. With a bit of luck, you might be able to find one https://chatons.org/en/find-by-localisation !",
              links: [
                {
                  text: "CHATONS",
                  link: "https://entraide.chatons.org/en"
                },
                {
                  text: "near you",
                  link: "https://chatons.org/en/find-by-localisation"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss30.png")
                },
              ],
            },
            {
              title: "Knowing Important Criteria",
              abstract:"In general, here are 3 essential criteria for an online service to be secure and protective. It must be:\n"+
              "• Free: that is, anyone can study how it works, modify it to suit their desires, and then share the result. This way, we can be more sure that the service does what it claims, and nothing else. To learn more about this ecosystem, I invite you to consult the work of the https://www.april.org/en/articles/intro.\n"+
              "• End-to-end encryption: With this method of protecting data, only those for whom it is intended can currently do so. And nobody else. Not even the people who administer the service in question.\n"+
              "• Decentralized: this means that the digital tool should not depend on a single authority, which could impose its decisions on everyone overnight. Thus, it is a common good: it belongs to everyone and no one can appropriate it to the detriment of others.\n"+
              "To go further, I recommend you read the guide https://ssd.eff.org/module/choosing-your-tools proposed by the site Surveillance Self-Defense.",
              links: [
                {
                  text: "April association",
                  link: "https://www.april.org/en/articles/intro"
                },
                {
                  text: "Choosing your tools",
                  link: "https://ssd.eff.org/module/choosing-your-tools"
                },
              ]
            }
          ],
          //pdf: require("assets/pdf/Rokaya_2.pdf"),
          tooltitle: "Substitute",
          tweet: "Finding a digital tool that really fits your needs, desires and values can be complicated. Let’s see how to do it in practice!",
          title: "Search for an ethical online service",
        },
        {
          subskills: [
            {
              title: "Browsing the Web With the Tor Browser",
              abstract: "The easiest and most effective way to protect yourself from bugs and maintain your privacy is to use the https://www.torproject.org/en/download/#android.This Firefox-based web browser is a free and open source app, and is specifically designed to bypass surveillance and censorship.\n"+
              "Please note even though Tor Browser is very efficient, it does not exempt you from continuing to browse in HTTPS or from updating your browser regularly. Also, you may still be recognized by the websites you visit in certain situations, for example if you decide to log in with an account.",
              links: [
                {
                  text: "Tor Browser",
                  link: "https://www.torproject.org/en/download/#android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss10.png")
                },
              ],
            },
            {
              title: "Surfing the Web With Duckduckgo Browser",
              abstract: "If your connection isn’t fast enough or some of the websites you want to visit block Tor Browser, the alternative I recommend is to use the https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android (also available in the F-Droid App Store). It’s a free web browser that blocks trackers and informs you about the privacy of the sites you visit. Be careful, though, it won’t make your connection totally anonymous (sites will know your IP address and where you are).",
              links: [
                {
                  text: "Duckduckgo",
                  link: "https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss11.png")
                },
              ],
            }
          ],
          tweet: "When we surf the web, cookies follows us (https://myshadow.org/browser-tracking) from site to site to deduce our tastes, our interests and a whole lot of other things about us. Let’s see how to protect ourselves!",
          //pdf: require("assets/pdf/Ally_3.pdf"),          
          tooltitle: "Tracking",
          title: "Protect yourself from the bugs that follows your navigation",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Joel" },
  { key: "Jillian" },
  { key: "Alvaro" },
  { key: "Atom nobot" },
  {
    key: "Lucas",
    about: "50 years old. In a relationship (partner user#8764563, username «Lola»). No Children. Detective. Top player (Gao the Prosecutor).",
    image: require("assets/images/community/avatars/B-Lucas.png"),
    informationShared: "Call log, Contacts, Geolocation, SMS, Wifi, Network, Storage",
    overview: "Curious, playful, reserved, distrustful, loving, obstinate.",
    recommendations: "Social network data collected (customer sale #XXXXX - multimedia and video game studio): has been in an ongoing romantic relationship with user#8764563 (username \"Lola\"), who is a lawyer, for 6 months\n"+
      "Internal note 1: tell the billing department to keep the client's ID hidden regarding the data transaction.\n"+
      "Internal note 2 (confidential): customer #87654, under judicial investigation by the team of user \"Lola\", was very interested in data that would prove a relationship with user \"Lucas\" (call logs, SMS and email content, full geolocation). In order to maintain good business relations and in the context of the generous offer to buy Gao Games by the client, the transaction must remain confidential.\n"+
      "Internal note 3 (confidential): the user \"Lola\" seems to suspect that her communications are being monitored, and has cut off contact for 72 hours. Information transmitted to client #87654.",
    thought: {
      title: 9,
      tweet: "Not just individuals",
      overview: "Companies and administrations are also monitored",
      message: "The tools of mass surveillance facilitate industrial or economic espionage. Whether it is to compete with companies, to steal their discoveries, to hold them to ransom, to sabotage them, or to find out about their customers through this means, many actors have an interest in obtaining confidential information from companies.\n"+
      "In the same way, this issue is also very important for administrations and public services. More often than not, it is the people who work there and have legitimate access to this confidential information who unwittingly allow malicious people to gain access. And this situation is getting worse as these organizations increasingly rely on telecommuting and open access to their digital tools from outside their premises.\n"+
      "The massive nature and global scale of these practices, as well as the participation of state intelligence services in these activities have been demonstrated by the documents revealed by whistleblower Edward Snowden in 2013 and more recently, in 2021, the Pegasus Program where some governments monitor others (heads of state, citizens, journalists) through their cell phones.\n"+
      "Thus, protecting ourselves from surveillance allows us to protect the data of the structures in which we work and to avoid situations that can be catastrophic. You can find many tips to limit these risks on the https://ssd.eff.org/ website\n",
      links: [
        {
          text: "Surveillance Self Defense",
          link: "https://ssd.eff.org/"
        },
      ],
      skills: [
        {
          subskills: [
            {
              title: "Install Signal",
              abstract: "Encrypting your communications means that, thanks to a mathematical process, your messages will be unreadable (“indecipherable”) except for the people for whom they are really intended. Without necessarily being aware of it, this process is implemented by many services that we use in our daily lives, for example for Internet browsing or online payments. https://signal.org/ -",
              links: [
                {
                  text: "Signal",
                  link: "https://signal.org/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss33.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss34.png")
                },
              ],
            },
            {
              title: "Why this application?",
              abstract: "Many communication applications offer this security, but … not all are trustworthy. Not least because their business model is to exploit and sell our data. Moreover, when their code is not free, they are even less reliable because nobody can know how they really work.\n"+
              "Of course, in order for your exchanges to be protected, your contacts must also use this application. To learn more, here is a https://ssd.eff.org/en/module/how-use-signal-android detailing all the steps, from the installation to the use of the app.\n"+
              "And to go further and know everything about encryption, I recommend that you read this https://ssd.eff.org/en/module/what-should-i-know-about-encryption .",
              links: [
                {
                  text: "practical guide",
                  link: "https://ssd.eff.org/en/module/how-use-signal-android"
                },
                {
                  text: "article",
                  link: "https://ssd.eff.org/en/module/what-should-i-know-about-encryption"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Lucas_1.pdf"),
          tooltitle: "Encryption",
          tweet: "A simple way to improve the confidentiality of your communications is to choose free applications that offer encryption. Today, the easiest to use is Signal. Even if it is not perfect, it is a good compromise between security and ease of use.",
          title: "Protecting your communications",
        },
        {
          subskills: [
            {
              title: "Take Stock of Your Situation",
              abstract: "First, let’s start by identifying what data you want to protect, and from what or who, depending on your situation. You can do this by identifying the data that is important or sensitive to you and thinking about the potential dangers if you ever lose it or if anyone else has access to it. This is what is called in computer security “establishing a threat model” but we will call it a protection model.\n"+
              "The next step is to identify, for each piece of data, how serious and probable their loss or dissemination may be. Indeed, each risk does not have the same probability of occurrence nor the same gravity. For example, a very, very, very unlikely risk would be that Martians would use intimate photos to decorate their spaceships, and for very likely risks, that Google would use these same photos to get to know you better and sell you related products.\n"+
              "By taking into account these 2 criteria, severity and probability, it becomes easy to sort out the risks you need to take seriously quickly and those you don’t really need to worry about. You can use https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ENG.cleaned.pdf to think about your threat model. ",
              links: [
                {
                  text: "this model",
                  link: "https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ENG.cleaned.pdf"
                },
              ],
            },
            {
              title: "Decide on actions to be taken",
              abstract: "Thanks to the mapping you have just done, you can now identify the places that need to be protected in priority.\n \n"+
              "This time, you have to find and be accompanied if you feel the need to imagine possible ways. Each one can be more or less difficult to implement, but don’t get discouraged! Sometimes there are very technical solutions, but other times not at all. For each data to protect, I invite you to ask yourself these questions:\n"+
                  "• Do my devices have a good password? Do I leave them next to the water tap and not have a back-up of their contents?\n"+
                  "• Is the solution I imagine to counter a risk easy or difficult to use?\n"+
                  "• Who can give me advice/ideas on how to protect this or that information?\n"+
                  "• Do these measures concern me individually or rather a group? …\n"+
                "Some ideas will have more disadvantages than advantages or may even make the situation worse, while others will be particularly simple and appropriate. But even without an immediate solution, being aware of the danger is already the beginning of the solution!\n"+
                "Finally, you will only have to choose and plan the actions to be carried out, and then come back to this work from time to time to check that it does not need to be adapted to new needs and contexts, because sometimes, there are no immediate solutions.\n"+
                "To learn more about how to create a safety plan, I recommend you read this short https://ssd.eff.org/en/module/your-security-plan published by the Self-Defense Watch website and https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ENG.cleaned.pdf from training and research cooperative La Boussole.",
              links: [
                {
                  text: "guide",
                  link: "https://ssd.eff.org/en/module/your-security-plan"
                },
                {
                  text: "this one",
                  link: "https://gaoandblaze.org/wp-content/uploads/2022/02/Plan-de-securite-GAO-and-Blaze-ENG.cleaned.pdf"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ajay_1.pdf"),
          tooltitle: "Protection",
          tweet: "The threats and needs of each individual can be very different. Let’s see how to find a tailor-made solution.",
          title: "What and from whom to protect yourself?",
        },
        {
          subskills: [
            {
              title: "Understanding the Risk",
              abstract: "The goal of this scam is to get the information that the deceived visitors could enter on a website. It can also aim to get them to install malware or call a premium-rate service.\n"+
              "Often, links to such sites are sent by email, with messages intended to scare or make us believe in a great stroke of luck or a threat of a virus on our devices. For https://cyberguerre.numerama.com/10020-attention-a-ces-phishings-francais-qui-ne-disparaissent-jamais.html, these messages may announce that you have won a game, that you are late in paying a bill, or that your cell phone has been infected by a virus.",
              links: [
                {
                  text: "example (French only)",
                  link: "https://cyberguerre.numerama.com/10020-attention-a-ces-phishings-francais-qui-ne-disparaissent-jamais.html"
                },
              ],
            },
            {
              title: "Be Vigilant",
              abstract: "To spot these scam attempts, you have to pay attention to the address of the sites you want you to visit. For example, https://gaogames.scam.fr/ would have nothing to do with Gao games. You can see this by pressing the link with your finger for a long time before opening it or in the corner of your browser by hovering over the link with your mouse because under the appearance of going to https://gaogames.fr, you are currently going to https://gaogames.arnaque.fr as we show you below.",
              links: [
                {
                  text: "https://gaogames.fr",
                  link: "https://gaogames.scam.fr/"
                },
                {
                  text: "https://gaogames.scam.fr",
                  link: "https://gaogames.arnaque.fr/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss35.png")
                },
              ],
            },
            {
              title: "How to spot them?",
              abstract: "Here, the most important part of the address is arnarque.fr: this is the domain you would go to. Remember to check that this is where you want to go! Also, beware of errors in the addresses (for example Wikypedia instead of Wikipedia). This requires a lot of attention from you every time you browse or open links.\n"+
              "Other elements can give you a hint: spelling mistakes, legal notices that can’t be found, a message that is too urgent to do something, the overall look of the site…\n"+
              "If in doubt, it is best to take the time to think about it and ask for advice. You can also take a look at the  https://lookup.icann.org/lookup about the domain name. In any case, don’t click on any links that seem strange to you. Instead, go to the site in question by searching for it on your favorite search engine.\n"+
              "To learn more about these scams and how to protect yourself from them, I invite you to read these https://health.ucdavis.edu/itsecurity/spot-phishing-messages.html or this https://cyberguerre.numerama.com/2724-phishing-comment-font-les-hackers-comment-vous-proteger.html published on the Numerama.com website.",
              links: [
                {
                  text: "public information",
                  link: "https://lookup.icann.org/lookup"
                },
                {
                  text: "recommendations",
                  link: "https://health.ucdavis.edu/itsecurity/spot-phishing-messages.html"
                },
                {
                  text: "article (French only)",
                  link: "https://cyberguerre.numerama.com/2724-phishing-comment-font-les-hackers-comment-vous-proteger.html"
                },
              ],
            }
          ],
          tweet: "Phishing, one of the most common online scams, consists of pretending to be a website by imitating (sometimes extremely well) its appearance. Let’s see how to protect yourself!",
          //pdf: require("assets/pdf/Lucas_3.pdf"),
          tooltitle: "Scams",
          title: "Avoiding online scams",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  {
    key: "Sol",
    about: "39 years old. Single. No children. Entrepreneur. Top player (Gao the Intruder).",
    image: require("assets/images/community/avatars/B-Sol.png"),
    informationShared: "Contacts, Geolocation, Wifi, Network, SMS, Alarm, Microphone, Calendar, Camera, Accelerometer",
    overview: "Pragmatic, confident, outgoing, careerist, ambitious, large network of acquaintances.",
    recommendations: "Social network data collected (customer sale #15467 - bank): subject's address book and network contacts indicate a significant proportion of low economic income acquaintances (-53% impact on bank score). 61% probability of not repaying a bank loan (estimate based on friends' networks). Consider loan denial.",
    thought: {
      title: 12,
      tweet: "“Who decides for us?”",
      overview: "Our data may be used to make decisions that affect us",
      message: "Throughout the world, companies are building software to make decisions about us based on our personal data. They would like to know who would be likely to pay back a loan to their bank or not, who is the best fit for a job, who will get sick in the next few years, who might commit a crime…\n"+
      "These may sound like interesting goals, but in practice they have many problems. Their predictions often turn out to be inaccurate and reveal more about the stereotypes that the people who created these methods had than anything else. For https://www.reuters.com/article/us-amazon-com-jobs-automation-insight-idUSKCN1MK08G, a program designed by Amazon in 2014 to sort through applications the company received ended up systematically favoring resumes from men. Another major difficulty is that people considered negatively have no longer the opportunity to get explanations or defend themselves.\n"+
      "Fortunately, in Europe, fully automated decisions are prohibited if they could have significant consequences for the individuals concerned. To learn more about this, we recommend reading this https://www.seas.upenn.edu/~cis399/files/lecture/presentations/Automating_Inequality.pdf on how automated decision systems reinforce inequalities or this https://www.cnil.fr/sites/default/files/atoms/files/20171025_wp251_enpdf_profilage.pdf from the website of the Commission Nationale de l’Informatique et des Libertés (CNIL).\n",
      links: [
        {
          text: "example",
          link: "https://www.reuters.com/article/us-amazon-com-jobs-automation-insight-idUSKCN1MK08G"
        },
        {
          text: "article",
          link: "https://www.seas.upenn.edu/~cis399/files/lecture/presentations/Automating_Inequality.pdf"
        },
        {
          text: "page",
          link: "https://www.cnil.fr/sites/default/files/atoms/files/20171025_wp251_enpdf_profilage.pdf"
        }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Control the uses of your data",
              abstract: "It is possible to contact private or public organizations that collect, use or sell our personal data, and to request information, corrections, deletions or copies of our data.\n"+
              "This concerns services opened for European residents or its citizens in other countries. Each country of the European Union has its own institution in charge of checking the good application of these protections. https://edpb.europa.eu/about-edpb/board/members_es is the list of countries. And for the other countries of the world, I recommend you this https://www.cnil.fr/en/la-protection-des-donnees-dans-le-monde.\n"+
              "In France, it is the National Commission of Data Processing and Liberties (CNIL) which is in charge of helping us to assert our rights in this matter and offers us guides on our https://www.cnil.fr/en/les-droits-pour-maitriser-vos-donnees-personnelles and https://www.cnil.fr/modeles/courrier letters to assert them easily.\n"+
              "Let’s imagine: while searching for your name and surname on the Internet, you find personal information that you do not wish to be published. The first step to remove it is to write to the contact address indicated on the site in question. Once you have sent your request, the people who manage the site are obliged to answer you within 30 days. In many cases, this contact alone is enough to get a satisfactory response.",
              links: [
                {
                  text: "Here",
                  link: "https://edpb.europa.eu/about-edpb/board/members_es"
                },
                {
                  text: "detailed map",
                  link: "https://www.cnil.fr/en/la-protection-des-donnees-dans-le-monde"
                },
                {
                  text: "different rights",
                  link: "https://www.cnil.fr/en/les-droits-pour-maitriser-vos-donnees-personnelles"
                },
                {
                  text: "models",
                  link: "https://www.cnil.fr/modeles/courrier"
                },
              ],
            },
            {
              title: "File a Complaint",
              abstract:"If this information is not removed after 30 days, you can send https://www.cnil.fr/fr/plaintes to the CNIL which can sanction the site in case of a fault. Unfortunately, the CNIL only has a few resources (and therefore people) to deal with the messages, so their response may take some time. If 30 days pass without any answer from the CNIL, you can then https://www.service-public.fr/particuliers/vosdroits/F1435 directly with the police, the gendarmerie or the public prosecutor to assert your rights.",
              links: [
                {
                  text: "a complaint (French version)",
                  link: "https://www.cnil.fr/fr/plaintes"
                },
                {
                  text: "file a complaint (French version)",
                  link: "https://www.service-public.fr/particuliers/vosdroits/F1435"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_2.pdf"),
          tooltitle: "Rights",
          tweet: "The European Union guarantees the protection of the data of its citizens and also of the people residing in their territory. Since its implementation in 2018, the General Data Protection Regulation (GDPR) allows citizens and residents of the European Union to better control the uses made of their data. Let’s see what it’s all about!",
          title: "Protecting you across borders!",
        },
        {
          subskills: [
            {
              title: "What is it about?",
              abstract: "To work a cell phone needs an operating system, that is the core software that allows applications and buttons to work: it acts as an intermediary between them and the hardware.\n"+
            "Almost all modern phones run either Android, developed mainly by Google, or iOS, developed entirely by the company Apple.These operating systems are designed by companies that collect and use our data and reduce our ability to protect our data.",
            },
            {
              title: "Install an alternative system",
              abstract:"Fortunately for Android phones, it is possible to replace Android with an operating system such as https://lineageos.org/, https://e.foundation/en/ or https://www.replicant.us/, which are entirely free.This operation can be complex to perform: not all systems are compatible with all phone models, it is necessary to make a full back-up before starting and follow the instructions carefully, because in case of an error, for example, perform all operations with a loaded phone the device may, in some cases become unusable. It is better to think about making a back-up before starting! Changing the operating system is a relatively delicate operation, but thanks to it, it is possible to really take control of your phone and better protect your personal data.\n"+
              "To learn more and maybe try it, I recommend that you start with https://fsfe.org/news/2012/news-20120228-01.en.html from the Free Software Foundation Europe and this https://wiki.lineageos.org/ . And to solve problems you might encounter, I recommend the https://www.xda-developers.com/ site.",
              links: [
                {
                  text: "Lineage OS",
                  link: "https://lineageos.org/"
                },
                {
                  text: "/e/",
                  link: "https://e.foundation/en/"
                },
                {
                  text: "Replicant",
                  link: "https://www.replicant.us/"
                },
                {
                  text: "this article",
                  link: "https://fsfe.org/news/2012/news-20120228-01.en.html"
                },
                {
                  text: "practical guide",
                  link: "https://wiki.lineageos.org/"
                },
                {
                  text: "xda-developers.com",
                  link: "https://www.xda-developers.com/"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Sol_2.pdf"),
          tooltitle: "Alternative OS",
          tweet: "Warning: this know-how is complicated, but don’t panic, I’ll explain in detail what it is all about.",
          title: "Install an alternative operating system on your phone   ",
        },
        {
          subskills: [
            {
              title: "Know Your Rights",
              abstract: "Closing an account on an online service is a right: since the General Data Protection Regulation (the GDPR) came into force in 2018, we https://gdpr.eu/article-17-right-to-be-forgotten/ that an organization delete all data and information about us. In theory, all sites that offer you to create an account should also offer you a way to delete it and the data it contains.\n"+
            "In practice, the process can be very simple or very complicated. Often, it will be enough to click on a button “delete my account” but some sites hide this menu, try to dissuade us, impose a delay, force us to contact them by phone or send them documents … or even do not provide the possibility to do it.",
              links: [
                {
                  text: "can ask",
                  link: "https://gdpr.eu/article-17-right-to-be-forgotten/"
                },
              ],
            },
            {
              title: "Search for the Option",
              abstract: "In any case, the first step is to check if the site in question offers to delete your account. To do this, log in and go to a section called Personal settings, Manage my account or something similar. With a search engine, it is often possible to quickly find detailed explanations on how to proceed for most websites.",
            },
            {
              title: "Contact the Site",
              abstract: "If the site does not offer an automated procedure for deleting your account, the second step is to contact the people who manage it directly. On its website, the Commission Nationale de l’Informatique et des Libertés (CNIL) offers a https://www.cnil.fr/fr/retrouver-les-coordonnees-dun-organisme-pour-exercer-vos-droits to find the contact details of the person to contact, as well as a https://www.cnil.fr/en/modele/courrier/cloturer-un-compte-en-ligne to complete and send (this is the French administrative authority responsible for helping us assert our rights in this area).",
              links: [
                {
                  text: "detailed guide (French version)",
                  link: "https://www.cnil.fr/fr/retrouver-les-coordonnees-dun-organisme-pour-exercer-vos-droits"
                },
                {
                  text: "model letter (French version)",
                  link: "https://www.cnil.fr/en/modele/courrier/cloturer-un-compte-en-ligne"
                },
              ],
            },
            {
              title: "File a complaint",
              abstract: "If you live in a member state of the European Union, if all these steps have failed and you have not received a satisfactory answer within 30 days, you can send a complaint to the https://edpb.europa.eu/about-edpb/board/members_es and ask it to intervene.\n"+
              "In France, it is the https://www.cnil.fr/en/plaintes/internet. In case of illegal practice, the CNIL can sanction the website and force it to delete your account and your data. Unfortunately, the delays to get an answer from them are usually—really—very long.",
              links: [
                {
                  text: "administration in charge of this issue in your country",
                  link: "https://edpb.europa.eu/about-edpb/board/members_es"
                },
                {
                  text: "CNIL",
                  link: "https://www.cnil.fr/en/plaintes/internet"
                },
              ],
            }
          ],
          tweet: "There are a thousand and one good reasons to want to close one of your accounts on a website. Let’s see how to do it in practice",
          //pdf: require("assets/pdf/Sol_3.pdf"),
          tooltitle: "Accounts",
          title: "Delete an account on an online service",
        }
      ]
    },
    enabled: true,
    hasAnimation: true
  },
  { key: "Jimmy" },
  { key: "Julie" },
  {
    key: "Nikki-Thought-1",
    isNikkiTought: true,
    thought: {
      title: 1,
      tweet: "“Monitored Communications”",
      overview: "Our communications can be monitored",
      message:
        "I tried to write a long message to the Gao Games fans to explain why I was leaving, but I couldn’t. It was too tiring, too complicated, and to be honest, I didn’t have the courage… I know it sucks. I left my draft notes, though. It’s not necessarily very clear, still I hope someone can do something with it. I sorted them by topic, to try to talk about everything. This one is about spying on discussions…\n"+
        "By default, our digital communication means (email, SMS, social networks…) are not very secure and do not guarantee the confidentiality of our exchanges. Especially cell phones, because the way they work is voluntarily made opaque and their modification difficult.\n"+
        "All our conversations can be monitored by companies that make money from this activity (like Gao Games), by states and their administrations, or by malicious people around us or unknown. One of the only ways to protect ourselves from these prying eyes is to encrypt communications.\n",
      links: [],
      skills: [
        {
          subskills: [
            {
              title: "Install Signal",
              abstract: "Encrypting your communications means that, thanks to a mathematical process, your messages will be unreadable (“indecipherable”) except for the people for whom they are really intended. Without necessarily being aware of it, this process is implemented by many services that we use in our daily lives, for example for Internet browsing or online payments.  - https://signal.org/ -",
              links: [
                {
                  text: "Signal",
                  link: "https://signal.org/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss33.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss34.png")
                },
              ],
            },
            {
              title: "Why this application?",
              abstract: "Many communication applications offer this security, but … not all are trustworthy. Not least because their business model is to exploit and sell our data. Moreover, when their code is not free, they are even less reliable because nobody can know how they really work.\n"+
              "Of course, in order for your exchanges to be protected, your contacts must also use this application. To learn more, here is a https://ssd.eff.org/en/module/how-use-signal-android detailing all the steps, from the installation to the use of the app.\n"+
              "And to go further and know everything about encryption, I recommend that you read this https://ssd.eff.org/en/module/what-should-i-know-about-encryption .",
              links: [
                {
                  text: "practical guide",
                  link: "https://ssd.eff.org/en/module/how-use-signal-android"
                },
                {
                  text: "article",
                  link: "https://ssd.eff.org/en/module/what-should-i-know-about-encryption"
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Lucas_1.pdf"),
          tooltitle: "Encryption",
          tweet: "A simple way to improve the confidentiality of your communications is to choose free applications that offer encryption. Today, the easiest to use is Signal. Even if it is not perfect, it is a good compromise between security and ease of use.",
          title: "Protecting your communications",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-2",
    isNikkiTought: true,
    thought: {
      title: 2,
      tweet: "“Who knows where I am?”",
      overview: "Our phone allows us to tell at any time where we are",
      message: "As soon as it connects to a mobile network, our cell phone can be located relatively precisely by our telephone operator (e.g., Free, Orange, SFR, Bouygues…). At a short distance, it is also possible to locate cell phones with Wifi or Bluetooth activated.\n"+
      "But the most problematic part comes from applications already installed. Many of them use the built-in GPS to transmit our location to companies that exploit or market this information, including many of the« Net Giants. ». These companies and their customers—as well as police and intelligence services—can know where we are, but also where we have been, how many times, how long, with whom… If you want to know more about this topic, you can read this https://ssd.eff.org/en/playlist/privacy-breakdown-mobile-phones from the site Surveillance Self-Defense.",
      links: [
        {
         text: "article",
         link: "https://ssd.eff.org/en/playlist/privacy-breakdown-mobile-phones"
       }
      ],
      skills: [
        {
          subskills: [
            {
              title: "Cleaning Up Previously Granted Authorizations",
              abstract: "When you use an application for the first time, it must ask your permission before accessing your data or features on your phone. For example, it will be about geolocation, microphone, cameras, contact list… Potentially sensitive data and functions! \n"+
              "Over time, it may be difficult to remember what permissions you have or have not granted to a particular application. To check and change them, go to your phone’s settings, then to the menu dedicated to permissions or applications.",
            },
            {
              title: "Where can I find the list of authorizations?",
              abstract: "You will find the complete list of applications installed on your device and the permissions granted to each application. If you search by “permissions,” you will be able to see all the applications that access specific permission (for example, all the applications that access your geolocation or your contacts). From these menus you can either uninstall the applications or refuse the authorizations already granted simply by pressing them.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss4.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss5.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss6.png")
                },
                {
                  text: "Screenshot4",
                  image: require("assets/skills/ss7.png")
                },
              ],
            },
            {
              title: "Get detailed information",
              abstract: "To get even more detailed information about your phone’s permissions, don’t hesitate to use Nikki’s “scanner”! (if you don’t know what it is, you’ve still got a lot to discover here…)\n"+
              "This will allow you to know the number of trackers of the installed applications and to follow your progress in the protection of your data on the phone over time.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss8.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss9.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/Ally_1.pdf"),
          tooltitle: "Authorizations",
          tweet: "To function, some applications require permission to access different information and features of our devices. For example, a “camera” application asks for access to the camera. Sometimes, these access requests have nothing to do with the functioning of the application and are only intended to collect personal data about us. For example, a music player that asks to have access to your contacts and your movements. Let’s see how to configure them to better protect us!",
          title: "Configure the permissions granted to applications on your phone",
          showCheckBoxes: true
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-3",
    isNikkiTought: true,
    thought: {
      title: 3,
      tweet: "“Data Money”",
      overview: "How do some companies make money from our data ?",
      message: "The business model of many companies offering online services relies on the collection and use of our personal data. The best known of these companies are some of the GAFAMs—an acronym for Google, Amazon, Facebook, Apple and Microsoft—but they are far from the only ones doing so.\n \n"+
      "By knowing our behaviors, these companies can push us to do something (buy a product, see a movie, enter a store nearby…). The more these companies know about us, the more money they make !\n \n"+
      "Most of our uses can be used to collect more and more information about us: web browsing, cell phones, watches, payment and loyalty cards, televisions, vacuum cleaners, cars, doorbells, refrigerators… So many opportunities for these companies to access small pieces of information, which when put together, allow them to know a lot about us, our lives, our ideas and our daily lives. Although these practices are often illegal, they are nevertheless widespread. Fortunately, there are ways to protect ourselves.\n",
      links: [],
      skills: [
        {
          subskills: [
            {
              title: "From the main screen",
              abstract: "The easiest way to uninstall an application is to long press on its icon: a balloon will appear, offering you to Delete it. Some pre-installed applications cannot be completely uninstalled. It’s frustrating, but in this case, the best you can do is to open the app’s information, force it to stop and remove the permissions.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss36.png")
                },
              ],
            },
            {
              title: "From the Parameters",
              abstract: "Another way to uninstall an application is to go to your phone’s Settings, then to the Application Management menu. Then you just have to select the application in question, press Uninstall and confirm this choice.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss37.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss38.png")
                },
              ],
            }
          ],
          //pdf: require("assets/pdf/though 4"),
          tooltitle: "Uninstall",
          tweet: "There are several ways to uninstall an application from your phone. Here’s how to do it:",
          title: "Uninstall an application",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-4",
    isNikkiTought: true,
    thought: {
      title: 4,
      tweet: "Surveillance and democracy.",      
      overview:"Mass surveillance undermines democracy",
      message: "By monitoring our actions, companies are able to deduce our political opinions and the arguments most likely to convince us to do or not to do something. And even to vote or not to vote for a candidate in a democratic election !\n"+
       "For example, the company https://en.wikipedia.org/wiki/Cambridge_Analytica is accused of having used data https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal to influence the election of Donald Trump in the United States or the referendum on Britain’s exit from the European Union. In all, Cambridge Analytica would have biased more than 200 elections around the world, to meet the demands of people who can pay for these services, but this type of influence goes against the functioning of democracy where each person makes his choices freely and not by being manipulated.\n"+
      "Knowing that we may be watched changes the way we behave. For example, people will refrain from doing or saying something (i.e., https://www.slate.fr/story/116035/surveillance-masse-opinions-minoritaires ) for fear of being penalized.\n"+
      "Also, the Net giants have the power over what information we can or cannot find and therefore https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/.\n",
      links: [{
        text: "Cambridge Analytica",
        link: "https://en.wikipedia.org/wiki/Cambridge_Analytica"
      },{
        link: "https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal",
        text: "collected in part via Facebook",
      }, {
        link: "https://journals.sagepub.com/doi/abs/10.1177/1077699016630255",
        text: "expressing a minority opinion",
      },{
        link: "https://www.politico.com/magazine/story/2015/08/how-google-could-rig-the-2016-election-121548/",
        text: "they can influence us",
      }],
      skills: [
        {
          subskills: [
            {
              title: "Understanding this issue",
              abstract: "According to the French Agency for Ecological Transition (ADEME), about 75% of the environmental impacts of cell phones are related to their manufacture, mainly because of https://www.greenpeace.org/international/story/11783/how-green-is-your-tech/, such as microprocessors.\n" +
              "The same goes for their most dramatic social consequences: they occur at the time of extraction of the raw materials that make up these devices, and then at the time of their assembly.",
              links: [
                {
                  text: "their screens and complex electronic components",
                  link: "https://www.greenpeace.org/international/story/11783/how-green-is-your-tech/"
                },
              ],
            },
            {
              title: "Make your devices last",
              abstract: "Thus, the best way to reduce the consequences of our uses is to make our appliances last and not to replace them while they still work. According to ADEME, by using them for 4 years instead of 2, we improve the ecological balance of our appliances by 50%. \n"+
              "To do this, we can protect them with covers, shells and other screen films and make sure to let them rest when they start to overheat. Finally, don’t leave your battery is completely empty to recharge it and avoid also to leave on charge overnight. This way, it will work longer.\n"+
              "If one of your appliances does break down, it is often possible to have it repaired. If it is less than 2 years old, you even have a legal guarantee for this! You can also try to repair it yourself: many online tutorials explain how to do it, for example those of the website https://ifixit.com/, and plenty more associations will be eager to give you a hand.",
              links: [
                {
                  text: "ifixit.com",
                  link: "https://ifixit.com/"
                },
              ],
            },
            {
              title: "Think Before You Buy",
              abstract: "And when you need to replace a device, don’t forget to consider these issues. Take the time to be well informed, to choose a device that will really suit you, that can be easily repaired and whose brands do not voluntarily make their products malfunction after a certain time (this is called programmed obsolescence).\n"+
              "A good idea is to choose a second hand or refurbished phone: you will pay less for it and you will avoid the environmental cost of manufacturing a new device! Finally, don’t forget to recycle your old device at this time.\n"+
              "If you still want a new phone, you can take a look at the ones offered by the company https://www.fairphone.com/en/,which has set itself the goal of making the most ethical devices possible, or to think about renting devices rather than owning themOther similar manufacturers may offer the same thing in the future—it’s not the brand that matters, but the approach.",
              links: [
                {
                  text: "Fairphone",
                  link: "https://www.fairphone.com/en/"
                },
              ],
            }
          ],
          tweet: "As we have seen, digital tools also pollute! Here is how to limit the damage.",
          tooltitle: "Ecology",
          //pdf: require("assets/pdf/though 5"),
          title: "Limit the environmental and social consequences of our digital practices",
        }
      ]
    }
  },
  {
    key: "Nikki-Thought-5",
    isNikkiTought: true,
    thought: {
      title: 5,
      tweet: "Pollution",
      overview: "Digital tools also pollute",
      message: "Contrary to what we might think, digital uses and tools pollute the planet. The manufacture of devices and equipment, browsing websites accessible 24 hours a day, sending emails, printing at home … all these activities, consume energy, produce waste and require large volumes of rare metals! \n"+
       "Digital tools and services (i.e. a video instead of a plane trip), encourage existing polluting uses instead of replacing them. https://theshiftproject.org/en/article/lean-ict-our-new-report/ show that IT uses already emit more greenhouse gases than air travel, and that these emissions will soon reach the level of those of road transport.The storage of large quantities of data (the “Big Data”) and deep learning are very polluting\n"+
        "The manufacturing conditions of this equipment and their components are also often https://up-magazine.info/en/planete/ressources-naturelles/55976-cobalt-le-cout-humain-et-environnemental-de-lor-bleu-de-nos-transitions-energetiques/ (slavery, child labor…). Fortunately, solutions exist and we can act to limit the social and environmental consequences of our digital practices!\n",
      links: [{
        link: "https://theshiftproject.org/en/article/lean-ict-our-new-report/",
        text: "Studies"
      },{
        link: "https://up-magazine.info/en/planete/ressources-naturelles/55976-cobalt-le-cout-humain-et-environnemental-de-lor-bleu-de-nos-transitions-energetiques/",
        text: "scandalous",
      }],
      skills: [
        {
          subskills: [
            {
              title: "How does the information flow between a site and us?",
              abstract: "You have to imagine that every time you connect to a website, the website and your computer are “chatting,” e.g., sending information to each other. When you want to see a tab or a video, the site you’re visiting needs to know what it should show you, or if you’re buying something, it needs to send your credit card number. By using the HTTPS protocol this data is sent secretly (confidentiality) and completely (integrity). If a naughty person were to intercept this data in the process, he would only be able to access a scrambled and incomprehensible sequence of characters.\n"+

                "Today, many websites offer this security by default. If it is not the case, you just have to add an s to the HTTP address you visit (a way to remember it is to think of s as “secure”). And to avoid having to do it yourself every time, you can install the https://www.eff.org/https-everywhere/,extension, which does it for you!",
              links: [
                {
                  text: "HTTPS Everywhere",
                  link: "https://www.eff.org/https-everywhere/"
                },
              ],
            },
            {
              title: "Display in a web browser",
              abstract: "Web browsers therefore display a small closed padlock when it is secure, or crossed out when it is not.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss39.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss40.png")
                },
                {
                  text: "Screenshot3",
                  image: require("assets/skills/ss41.png")
                },
              ],
            },
            {
              title: "Unsecured connection",
              abstract: "If you have added an s to the HTTP when the site does not offer the possibility of sending the information securely, you will see an error message:",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss42.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss43.png")
                },
              ],
            },
            {
              title: "What to do?",
              abstract: "Don’t panic! Maybe the site just doesn’t offer this security. In this case, visit it in simple HTTP, but be particularly careful not to indicate passwords, credit card numbers or any other information.\n"+
              "If the HTTPS protocol usually works for this website and you still see the same error message, there are two options.\n"+
              "• If it is a site configuration problem and usually by coming back in a few minutes, everything can be in order.\n"+
              "• If the message is still there, it could also be an attempted scam and the site has been hacked.\n"+

              "If you decide to bypass these messages, you continue to browse at your own risk. And again, never enter a password, credit card number or any other information. These messages are there to tell you about the status of the site and they may put your data at risk.",
            }
          ],
          tweet: "When you go on the Internet, it is better to protect yourself from the most common scams.",
          //pdf: require("assets/pdf/though 3-1"),
          tooltitle: "Surf The Net",
          title: "Improve the security of your web browsing",
        },
        {
          subskills: [
            {
              title: "Avoiding risks",
              abstract: "Even for experts, it’s hard to really know what an app can and cannot do. In order to download a new one without risk, it is important to go through an application store. These stores check for us that offered apps do not contain malicious features and that they will ask our permission before accessing our data and sensors on our device.\n"+
                "On the contrary, installing an application from any other source than these stores exposes you to a high risk of ending up with a booby-trapped program (for example, an .apk file offered for free by an Internet site).",
            },
            {
              title: "Collecting clues",
              abstract: "But even with these stores, it is sometimes difficult to find your way through all the applications offered! Several elements can help you to measure the reliability of an application: the number of downloads, the description, the opinions and comments of other people, the authorizations requested by the application at its first launch or after an update…\n"+

              "You can also find out which organization owns the application. For example, in addition to Android, Google also owns Chrome, YouTube, Waze and many others. Facebook, on the other hand, controls Instagram and WhatsApp. If in doubt, it is better not to install the application in question and choose another one.\n"+

              "Finally, thanks to our tool that uses https://reports.exodus-privacy.eu.org/en/ it is possible to provide you with information about the most popular applications and indicate for each of them the number of trackers it incorporates and the permissions it requires to operate.",
              links: [
                {
                  text: "Exodus Privacy,",
                  link: "https://reports.exodus-privacy.eu.org/en/"
                },
              ],
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss44.png")
                },
              ],
            },
            {
              title: "Use a reliable source",
              abstract: "In addition to making available only free and more respectful (and therefore more reliable) applications, the F-Droid store offers an interesting feature to help you measure the reliability of applications. In the descriptions of the app, it lists the features that you might not like. Thanks to it, it becomes easier to see clearly and to compare several applications before choosing the one that suits us.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss45.png")
                },
              ],
            }
          ],
          tweet: "Before installing an application, it is important to be sure that we can trust it because it can then access our data, collect new data, and delete them or transmit them to malicious people or companies.",
          //pdf: require("assets/pdf/though 3-2"),
          tooltitle: "Reliability",
          title: "Measuring the reliability of an application",
        },
        {
          subskills: [
            {
              title: "Find the menu",
              abstract: "To back up your data, go to your phone’s Settings, then to the Other Settings menu and press Back-up and Reset (at the very bottom). Select the data to be backed up in this menu, press Back-up and Restore, then Create Back-up and choose the data you want to back up. By default, you can leave all the boxes checked.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss46.png")
                },
              ],
            },
            {
              title: "Start back-up",
              abstract: "Then press Start Back-up and off you go! After a few moments, your back-up is complete and appears on the menu. By pressing it, you can now restore the data of your choice whenever you need to. But you don’t have to stop there.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss47.png")
                },
                {
                  text: "Screenshot2",
                  image: require("assets/skills/ss48.png")
                },
              ],
            },
            {
              title: "Start back-up",
              abstract: "Then press Start Back-up and off you go! After a few moments, your back-up is complete and appears on the menu. By pressing it, you can now restore the data of your choice whenever you need to. But you don’t have to stop there.",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss49.png")
                },
              ],
            },
            {
              title: "Export Your Backup",
              abstract: "For your data to be really safe, you need to copy it to another medium so you can access it and reinstall it on your phone. To make this copy, connect your phone to a computer with a USB cable. A notification will appear on your screen: choose Transfer files.\n"+
              "You can now access the contents of the phone from your computer. Find the Back-up folder and copy it to your computer: this is where your back-ups are stored.\n"+
              "If you don’t have a computer, you can also insert an SD card in your phone and copy the file to it. In this case, don’t forget to store the SD card in a safe place afterwards (don’t leave it on the phone, so you don’t risk losing both at the same time).",
              images: [
                {
                  text: "Screenshot1",
                  image: require("assets/skills/ss50.png")
                },
              ],
            },
            {
              title: "Securing your back-ups",
              abstract: "A good rule of thumb for back-up is the “3 2 1 *” rule, which means:\n"+
                          "• 3 copies of each data (the actual data on your phone plus 2 copies)\n"+
                          "• on at least 2 different media (e.g., a computer and an external hard drive or SD card)\n"+
                          "• and that at least 1 copy is stored in a different location than the others (this could be online).\n"+
                          "• * and I add a star to remind you to encrypt these back-ups;).\n \n"+
                        "So, whatever problem you might encounter, one of the back-ups will always be available to restore your data. Whether it’s a burglary, a fire, an operating error or a technical problem that destroys your computer and your phone at the same time, you’ll be able to deal with all these situations.\n"+
                        "Of course, you can also adopt a simpler method if this one seems too complicated better to have one copy than none!\n \n"+
                        "An important element for your back-ups to be secure is to carefully choose the medium that will host them. Ideally, it should be encrypted. This short https://securityinabox.org/en/files/secure-file-storage/ explains how to do this in practice.\n \n"+
                        "Some people choose to entrust their back-ups to GAFAM or other Internet giants. As you may have guessed, this is not the solution I recommend. If you still want to do it, I invite you to start by taking the time to sort out the applications and data you will synchronize online and those that are really better saved elsewhere.\n \n"+
                        "One last tip: don’t forget to check from time to time that your back-ups have worked and that it is possible to restore your data! And if you prefer a custom solution, for example to be able to back up your application data as well, I recommend that you use the applications https://f-droid.org/en/packages/com.machiav3lli.backup/ (only available in the F-Droid store) or https://twrp.me/app/.",
              links: [
                {
                  text: "guide",
                  link: "https://securityinabox.org/en/files/secure-file-storage/"
                },
                {
                  text: "OandbackupX,",
                  link: "https://f-droid.org/en/packages/com.machiav3lli.backup/"
                },
                {
                  text: "TWRP",
                  link: "https://twrp.me/app/"
                },
              ],
            }
          ],
          tweet: "One of the objectives of computer security is to protect data from destruction. To do this, it is essential to back up data regularly, especially since phones are by nature easy to lose or break. A problem will inevitably occur at some point: it is better to be prepared in advance!",
          //pdf: require("assets/pdf/though 3-3"),
          tooltitle: "Save",
          title: "Save your data",
        }
      ]
    }
  }
]
