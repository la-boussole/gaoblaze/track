import {MetaGame} from "models/meta-game"
import {RootStore} from "models/root-store"
import * as RNLocalize from "react-native-localize"
import i18n from "i18next"

export interface QuestionMultiple {
  choices: Array<string>,
  onChange?: (string) => void
}

export interface QuestionScale {
  firstLabel: string,
  lastLabel: string,
  onChange?: (number) => void
}

export interface QuestionNumber {
  min: number,
  max: number,
  onChange?: (string) => void
}

export interface QuestionEmail {
  onChange: (string) => void
}

export interface QuestionChecklist {
  choices: Array<string>,
  onChange: (Array) => void
}

export interface QuestionMany {
  questions: Array<Question>
}

export interface QuestionStats {
  getData: (numberVariable, metaGame, rootStore) => any
}

export interface Question {
  id: string,
  title: string,
  titleSend: string,
  type: "multiple" | "scale" | "number" | "checklist" | "many" | "email" | "stat",
  options: QuestionMultiple | QuestionScale | QuestionNumber | QuestionChecklist | QuestionMany | QuestionStats
  mandatory?: boolean
}

export interface Questionary {
  modules: [{
    title: string,
    questions?: Array<Question>,
    type: "questions" | "stats",
    buttonOk: string,
    textConfirmation?: string
  }]
}

const QUESTIONARY: Questionary = {
  modules: [
    {
      title: "Scores",
      type: "stats",
      buttonOk: "Ok",
      questions: [
        {
          id: "S1",
          title: "@1 Gao Games joués et @2 autorisations accordées",
          titleSend: "@1 Gao Games joués et @2 autorisations accordées",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore) {
              if(numberVariable === 1) {
                let sum = 0
                metaGame.gamesPlayed.forEach(game => sum += game.playCount)
                return sum
              } else {
                return metaGame.permissions.length
              }
            }
          }
        },
        {
          id: "S2",
          type: "stat",
          title: "@1 entrées consultées dans la base de connaissances de Nikki",
          titleSend: "@1 entrées consultées dans la base de connaissances de Nikki",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): number {
              return metaGame.readSkills.length + metaGame.readThoughts.length
            }
          }
        },
        {
          id: "S3",
          type: "stat",
          title: "Temps de jeu total: @1",
          titleSend: "Temps de jeu total: @1",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              return Math.round((metaGame.totalTimeInGame / 1000) / 60 ) + " mins"
            }
          }
        },
        {
          id: "S4",
          title: "Jeux les plus joués: @1 (@2), @3 (@4), @5 (@6)",
          titleSend: "Jeux les plus joués: @1 (@2), @3 (@4), @5 (@6)",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              let array = []
              metaGame.gamesPlayed.forEach(item => array.push({game: item.name, count: item.playCount}))
              array = array.sort((a, b) => b.count - a.count)
              return Math.floor((numberVariable - 1)/2) < array.length ?
                (numberVariable%2==1 ? array[Math.floor((numberVariable - 1)/2)].game : array[Math.floor((numberVariable - 1)/2)].count) :
                numberVariable%2==1 ? "-" : "0"
            }
          }
        },
        {
          id: "S5",
          title: "Langue d’utilisation: @1",
          titleSend: "Langue d’utilisation: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              return i18n.language
            }
          }
        },
        {
          id: "S6",
          title: "Pays: @1",
          titleSend: "Pays: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              console.log(RNLocalize.getTimeZone())
              return RNLocalize.getTimeZone()
            }
          }
        },
        {
          id: "S7",
          title: "Choix de jeu final: @1",
          titleSend: "Choix de jeu final: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              console.log(metaGame.lastOptionsSelected)
              return metaGame.lastOptionsSelected
            }
          }
        }
      ]
    },
    {
      title: "Questionnaire",
      questions: [
        {
          id: "1.1",
          title: "Quel âge as-tu?",
          titleSend: "Quel âge as-tu?",
          type: "number",
          options: {
            min: 5,
            max: 99
          },
          mandatory: true
        },
        {
          id: "1.2",
          title: "Tu es",
          titleSend: "Tu es",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Une femme",
              "Un homme",
              "Autre"
            ],
          }
        },
        {
          id: "1.3",
          title: "Quelles études as-tu faites?",
          titleSend: "Quelles études as-tu faites?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Lycée (Baccalauréat)",
              "2-3 années d’études supérieures (BTS/Licence/...)",
              "4-5 années d’études supérieures (Master 1 ou 2...)",
              "Doctorat ou équivalent",
              "Autres études sans diplôme ou pas d’études"
            ],
          }
        },
        {
          id: "1.4",
          title: "Quel est le niveau d’études de tes parents? (le plus haut)",
          titleSend: "Quel est le niveau d’études de tes parents? (le plus haut)",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Lycée (Baccalauréat)",
              "2-3 années d’études supérieures (BTS/Licence/...)",
              "4-5 années d’études supérieures (Master 1 ou 2...)",
              "Doctorat ou équivalent",
              "Autres études sans diplôme ou pas d’études",
              "Je ne sais pas"
            ],
          }
        },
        {
          id: "1.5",
          title: "De 1 à 5, le smartphone est plutôt une contrainte ou un plaisir?",
          titleSend: "De 1 à 5, le smartphone est plutôt une contrainte ou un plaisir?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Une contrainte",
            lastLabel: "Un plaisir"
          }
        },
        {
          id: "1.7",
          title: "D’habitude, joues-tu aux jeux vidéos?",
          titleSend: "D’habitude, joues-tu aux jeux vidéos?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Pas du tout",
              "Assez peu",
              "Quelques fois par mois",
              "Plusieurs fois par semaine",
              "Tous les jours"
            ]
          }
        },
        {
          id: "1.6",
          title: "Est-ce que tu sais te servir facilement d'un ordinateur?",
          titleSend: "Est-ce que tu sais te servir facilement d'un ordinateur?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Pas du tout",
            lastLabel: "Tout à fait"
          }
        }
      ],
      type: "questions",
      buttonOk: "Ok"
    },
    {
      title: "Dernière étape!",
      buttonOk: "Voir la fin",
      type: "questions",
      questions: [
        {
          id: "2.1",
          title: "Acceptes-tu qu’on te recontacte par e-mail une unique fois pour nous aider avec cette recherche en répondant à quelques questions ?",
          titleSend: "Acceptes-tu qu’on te recontacte par e-mail une unique fois pour nous aider avec cette recherche en répondant à quelques questions ?",
          type: "email"
        },
        {
          id: "2.2",
          title: "Quelles émotions t’a causé ce jeu?",
          titleSend: "Quelles émotions t’a causé ce jeu?",
          type: "checklist",
          options: {
            choices: [
              "Surprise",
              "Peur",
              "Colère",
              "Tristesse",
              "Joie",
              "Dégoût",
              "Amusement",
              "Satisfaction",
              "Gêne",
              "Excitation",
              "Culpabilité",
              "Fierté",
              "Soulagement",
              "Honte",
              "Mépris"
            ]
          }
        },
        {
          id: "2.3",
          title: "Étais-tu à l’aise avec l’usage de tes données et les autorisations demandées par le jeu?",
          titleSend: "Étais-tu à l’aise avec l’usage de tes données et les autorisations demandées par le jeu?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Oui, parfaitement",
            lastLabel: "Non, pas du tout"
          }
        },
        {
          id: "2.4",
          title: "As-tu hésité avant de donner une autorisation pour continuer à utiliser les jeux?",
          titleSend: "As-tu hésité avant de donner une autorisation pour continuer à utiliser les jeux?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Oui et j’ai refusé plusieurs fois",
              "Oui et j’ai refusé une fois",
              "Un peu, mais j’ai accepté quand même",
              "Non, je n’y pense pas vraiment",
              "Non, car ça ne me pose aucun problème"
            ]
          }
        },
        {
          id: "2.5",
          title: "Penses-tu que le profilage des données des personnages du jeu a eu des conséquences problématiques sur leurs vies?",
          titleSend: "Penses-tu que le profilage des données des personnages du jeu a eu des conséquences problématiques sur leurs vies?",
          type: "many",
          options: {
            questions: [
              {
                id: "2.5.1",
                title: "Alex et ses conversations",
                titleSend: "Alex et ses conversations",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.2",
                title: "Ally et sa santé",
                titleSend: "Ally et sa santé",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.3",
                title: "Sol et ses réseaux",
                titleSend: "Sol et ses réseaux",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.4",
                title: "Amin et ses états émotionnels",
                titleSend: "Amin et ses états émotionnels",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.5",
                title: "Masako et sa vie familiale",
                titleSend: "Masako et sa vie familiale",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.6",
                title: "Lucas et les personnes à qui il parle",
                titleSend: "Lucas et les personnes à qui il parle",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.7",
                title: "Ajay, ses centres d’intérêt et ses contacts",
                titleSend: "Ajay, ses centres d’intérêt et ses contacts",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              },
              {
                id: "2.5.8",
                title: "Rokaya et sa réputation en ligne",
                titleSend: "Rokaya et sa réputation en ligne",
                type: "scale",
                options: {
                  firstLabel: "Non, ça n’est pas grave",
                  lastLabel: "Oui, c’est très grave"
                }
              }
            ]
          }
        }
      ]
    }
  ]
}


const QUESTIONARY_EN: Questionary = {
  modules: [
    {
      title: "Results",
      type: "stats",
      buttonOk: "Ok",
      questions: [
        {
          id: "S1",
          type: "stat",
          title: "@1 Gao Games played and @2 given permissions",
          titleSend: "@1 Gao Games joués et @2 autorisations accordées",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore) {
              if(numberVariable === 1) {
                let sum = 0
                metaGame.gamesPlayed.forEach(game => sum += game.playCount)
                return sum
              } else {
                return metaGame.permissions.length
              }
            }
          }
        },
        {
          id: "S2",
          type: "stat",
          titleSend: "@1 entrées consultées dans la base de connaissances de Nikki",
          title: "@1 entries readed on Nikki's notebook",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): number {
              return metaGame.readSkills.length + metaGame.readThoughts.length
            }
          }
        },
        {
          id: "S3",
          title: "Time played: @1",
          titleSend: "Temps de jeu total: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              return Math.round((metaGame.totalTimeInGame / 1000) / 60 ) + " mins"
            }
          }
        },
        {
          id: "S4",
          title: "Most played games: @1 (@2), @3 (@4), @5 (@6)",
          titleSend: "Jeux les plus joués: @1 (@2), @3 (@4), @5 (@6)",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              let array = []
              metaGame.gamesPlayed.forEach(item => array.push({game: item.name, count: item.playCount}))
              array = array.sort((a, b) => b.count - a.count)
              return Math.floor((numberVariable - 1)/2) < array.length ?
                (numberVariable%2==1 ? array[Math.floor((numberVariable - 1)/2)].game : array[Math.floor((numberVariable - 1)/2)].count) :
                numberVariable%2==1 ? "-" : "0"
            }
          }
        },
        {
          id: "S5",
          title: "Language: @1",
          titleSend: "Langue d’utilisation: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              return i18n.language
            }
          }
        },
        {
          id: "S6",
          title: "Country: @1",
          titleSend: "Pays: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              console.log(RNLocalize.getTimeZone())
              return RNLocalize.getTimeZone()
            }
          }
        },
        {
          id: "S7",
          title: "Final choice: @1",
          titleSend: "Choix de jeu final: @1",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              console.log(metaGame.lastOptionsSelected)
              return metaGame.lastOptionsSelected
            }
          }
        }
      ]
    },
    {
      title: "Survey",
      questions: [
        {
          id: "1.1",
          titleSend: "Quel âge as-tu?",
          title: "How old are you?",
          type: "number",
          options: {
            min: 5,
            max: 99
          },
          mandatory: true
        },
        {
          id: "1.2",
          title: "Do you identify yourself as ",
          titleSend: "Tu es",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Female",
              "Male",
              "Other"
            ],
          }
        },
        {
          id: "1.3",
          titleSend: "Quelles études as-tu faites?",
          title: "What studies have you complete?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "High school",
              "2-3 yrs of higher education (Technician/Bachelor's...)",
              "4-5 yrs of higher education (Professional undergraduate/Master's...)",
              "Doctorate or equivalent",
              "Other studies without a degree or no studies"
            ],
          }
        },
        {
          id: "1.4",
          title: "What is your parents' highest degree?",
          titleSend: "Quel est le niveau d’études de tes parents? (le plus haut)",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "High school",
              "2-3 yrs of higher education (Technician/Bachelor's...)",
              "4-5 yrs of higher education (Professional undergraduate/Master's...)",
              "Doctorate or equivalent",
              "Other studies without a degree or no studies",
              "I don't know"
            ],
          }
        },
        {
          id: "1.5",
          title: "From 1 to 5, is the smartphone more of a constraint or a pleasure?",
          titleSend: "De 1 à 5, le smartphone est plutôt une contrainte ou un plaisir?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "A constraint",
            lastLabel: "A pleasure"
          }
        }
        ,
        {
          id: "1.7",
          title: "Do you usually play video games?",
          titleSend: "D’habitude, joues-tu aux jeux vidéos?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Not at all",
              "Almost never",
              "A few times a month",
              "A few times a week",
              "Every day"
            ]
          }
        },
        {
          id: "1.6",
          titleSend: "Est-ce que tu sais te servir facilement d'un ordinateur?",
          title: "Can you use a computer with ease?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Not at all",
            lastLabel: "Yes"
          }
        }
      ],
      type: "questions",
      buttonOk: "OK",
    },
    {
      title: "Last step !",
      buttonOk: "Check the end!",
      type: "questions",
      questions: [
        {
          id: "2.1",
          titleSend: "Acceptes-tu qu’on te recontacte par e-mail une unique fois pour nous aider avec cette recherche en répondant à quelques questions ?",
          title: "Do you agree we mail you juste once to help us with this research by answering some questions?",
          type: "email"
        },
        {
          id: "2.2",
          titleSend: "Quelles émotions t’a causé ce jeu?",
          title: "What emotions did this game provoke in you?",
          type: "checklist",
          options: {
            choices: [
              "Surprise",
              "Fear",
              "Anger",
              "Sadness",
              "Joy",
              "Disgust",
              "Fun",
              "Satisfaction",
              "Discomfort",
              "Enthusiasm",
              "Guilt",
              "Pride",
              "Relief",
              "Shame",
              "Contempt"
            ]
          }
        },
        {
          id: "2.3",
          title: "Were you comfortable with the use of your data and the permissions requested by the game?",
          titleSend: "Étais-tu à l’aise avec l’usage de tes données et les autorisations demandées par le jeu?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Yes, perfectly",
            lastLabel: "No, not at all"
          }
        },
        {
          id: "2.4",
          titleSend: "As-tu hésité avant de donner une autorisation pour continuer à utiliser les jeux?",
          title: "Did you hesitate before giving permission to continue using the games?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Yes and I refused several times",
              "Yes and I refused once",
              "A little bit, but I agreed anyway",
              "No, I don't really think about it",
              "No, because I don't have any problem with it"
            ]
          }
        },
        {
          id: "2.5",
          titleSend: "Penses-tu que le profilage des données des personnages du jeu a eu des conséquences problématiques sur leurs vies?",
          title: "Do you think the data profiling of the characters in the game has had any problematic consequences in their lives?",
          type: "many",
          options: {
            questions: [
              {
                id: "2.5.1",
                titleSend: "Alex et ses conversations",
                title: "Alex and her conversations",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.2",
                title: "Ally and her health",
                titleSend: "Ally et sa santé",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.3",
                titleSend: "Sol et ses réseaux",
                title: "Sol and her networks",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.4",
                title: "Amin and his emotional states",
                titleSend: "Amin et ses états émotionnels",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.5",
                title: "Masako and her family life",
                titleSend: "Masako et sa vie familiale",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.6",
                titleSend: "Lucas et les personnes à qui il parle",
                title: "Lucas and the people he talks to",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.7",
                title: "Ajay, his interests and contacts",
                titleSend: "Ajay, ses centres d’intérêt et ses contacts",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              },
              {
                id: "2.5.8",
                titleSend: "Rokaya et sa réputation en ligne",
                title: "Rokaya and her online reputation",
                type: "scale",
                options: {
                  firstLabel: "No, it doesn't matter",
                  lastLabel: "Yes, it is very serious"
                }
              }
            ]
          }
        }
      ]
    }
  ]
}

const QUESTIONARY_ES: Questionary = {
  modules: [
    {
      title: "Scores",
      type: "stats",
      buttonOk: "Ok",
      questions: [
        {
          id: "S1",
          type: "stat",
          title: "@1 Gao Games jugados y @2 permisos dados",
          titleSend: "@1 Gao Games joués et @2 autorisations accordées",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore) {
              if(numberVariable === 1) {
                let sum = 0
                metaGame.gamesPlayed.forEach(game => sum += game.playCount)
                return sum
              } else {
                return metaGame.permissions.length
              }
            }
          }
        },
        {
          id: "S2",
          titleSend: "@1 entrées consultées dans la base de connaissances de Nikki",
          type: "stat",
          title: "@1 notas consultadas del cuaderno de Nikki",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): number {
              return metaGame.readSkills.length + metaGame.readThoughts.length
            }
          }
        },
        {
          id: "S3",
          type: "stat",
          title: "Tiempo de juego total: @1",
          titleSend: "Temps de jeu total: @1",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              return Math.round((metaGame.totalTimeInGame / 1000) / 60 ) + " mins"
            }
          }
        },
        {
          id: "S4",
          title: "Juegos preferidos: @1 (@2), @3 (@4), @5 (@6)",
          titleSend: "Jeux les plus joués: @1 (@2), @3 (@4), @5 (@6)",
          type: "stat",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              let array = []
              metaGame.gamesPlayed.forEach(item => array.push({game: item.name, count: item.playCount}))
              array = array.sort((a, b) => b.count - a.count)
              return Math.floor((numberVariable - 1)/2) < array.length ?
                (numberVariable%2==1 ? array[Math.floor((numberVariable - 1)/2)].game : array[Math.floor((numberVariable - 1)/2)].count) :
                numberVariable%2==1 ? "-" : "0"
            }
          }
        },
        {
          id: "S5",
          type: "stat",
          title: "Idioma de juego: @1",
          titleSend: "Langue d’utilisation: @1",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              return i18n.language
            }
          }
        },
        {
          id: "S6",
          type: "stat",
          titleSend: "Pays: @1",
          title: "Ubicación: @1",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              console.log(RNLocalize.getTimeZone())
              return RNLocalize.getTimeZone()
            }
          }
        },
        {
          id: "S7",
          type: "stat",
          titleSend: "Choix de jeu final: @1",
          title: "Elección final: @1",
          options: {
            getData(numberVariable, metaGame: MetaGame, rootStore: RootStore): any {
              console.log(metaGame.lastOptionsSelected)
              return metaGame.lastOptionsSelected
            }
          }
        }
      ]
    },
    {
      title: "Cuestionario",
      questions: [
        {
          id: "1.1",
          titleSend: "Quel âge as-tu?",
          title: "¿Cuántos años tienes?",
          type: "number",
          options: {
            min: 5,
            max: 99
          },
          mandatory: true
        },
        {
          id: "1.2",
          titleSend: "Tu es",
          title: "Te identificas como ",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Mujer",
              "Hombre",
              "Otro"
            ],
          }
        },
        {
          id: "1.3",
          titleSend: "Quelles études as-tu faites?",
          title: "¿Qué estudios has terminado?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Bachillerato",
              "2-3 años de estudios superiores (Técnico/Licenciatura/...)",
              "4-5 años de estudios superiores (Pregrado/Master/...)",
              "Doctorado o equivalente",
              "Otros estudios sin titulación o sin estudios"
            ],
          }
        },
        {
          id: "1.4",
          title: "¿Cuál es el diploma más alto de tus padres?",
          titleSend: "Quel est le niveau d’études de tes parents? (le plus haut)",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Bachillerato",
              "2-3 años de estudios superiores (Técnico/Licenciatura/...)",
              "4-5 años de estudios superiores (Pregrado/Master/...)",
              "Doctorado o equivalente",
              "Otros estudios sin titulación o sin estudios",
              "No sé"
            ],
          }
        },
        {
          id: "1.5",
          titleSend: "De 1 à 5, le smartphone est plutôt une contrainte ou un plaisir?",
          title: "Del 1 al 5, ¿el smartphone es más bien una limitación o un placer?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Una limitación",
            lastLabel: "Un placer"
          }
        }
        ,
        {
          id: "1.7",
          titleSend: "D’habitude, joues-tu aux jeux vidéos?",
          title: "¿Sueles jugar videojuegos?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Para nada",
              "Casi nunca",
              "Algunas veces al mes",
              "Algunas veces a la semana",
              "Todos los días"
            ]
          }
        },
        {
          id: "1.6",
          titleSend: "Est-ce que tu sais te servir facilement d'un ordinateur?",
          title: "¿Puedes utilizar un computador con facilidad?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Para nada",
            lastLabel: "Sí"
          }
        }
      ],
      type: "questions",
      buttonOk: "Ok",
    },
    {
      title: "Última etapa",
      buttonOk: "Ver el fin",
      type: "questions",
      questions: [
        {
          id: "2.1",
          titleSend: "Acceptes-tu qu’on te recontacte par e-mail une unique fois pour nous aider avec cette recherche en répondant à quelques questions ?",
          title: "¿Aceptas que te contactemos por mail una única vez para apoyarnos con nuestra investigación ?",
          type: "email"
        },
        {
          id: "2.2",
          titleSend: "Quelles émotions t’a causé ce jeu?",
          title: "¿Qué emociones te provocó este juego?",
          type: "checklist",
          options: {
            choices: [
              "Sorpresa",
              "Miedo",
              "Ira",
              "Tristeza",
              "Alegría",
              "Asco",
              "Diversión",
              "Satisfacción",
              "Incomodidad",
              "Entusiasmo",
              "Culpabilidad",
              "Orgullo",
              "Alivio",
              "Vergüenza",
              "Desprecio"
            ]
          }
        },
        {
          id: "2.3",
          titleSend: "Étais-tu à l’aise avec l’usage de tes données et les autorisations demandées par le jeu?",
          title: "¿Te sentiste cómodo/cómoda con el uso de tus datos y los permisos solicitados por el juego?",
          type: "scale",
          mandatory: true,
          options: {
            firstLabel: "Sí, totalmente",
            lastLabel: "No, pura incomodidad"
          }
        },
        {
          id: "2.4",
          titleSend: "As-tu hésité avant de donner une autorisation pour continuer à utiliser les jeux?",
          title: "¿Dudaste antes de dar permiso para seguir utilizando los juegos?",
          type: "multiple",
          mandatory: true,
          options: {
            choices: [
              "Sí y me negué varias veces",
              "Sí y me negué una vez",
              "Un poco, pero acepté de todos modos",
              "No, realmente no pienso en ello",
              "No, porque no tengo ningún problema con ello"
            ]
          }
        },
        {
          id: "2.5",
          titleSend: "Penses-tu que le profilage des données des personnages du jeu a eu des conséquences problématiques sur leurs vies?",
          title: "¿Crees que hacer de perfiles sobre los y las personajes del juego ha tenido alguna consecuencia negativa en sus vidas?",
          type: "many",
          options: {
            questions: [
              {
                id: "2.5.1",
                titleSend: "Alex et ses conversations",
                title: "Alex y sus conversaciones",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.2",
                titleSend: "Ally et sa santé",
                title: "Ally y su salud",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.3",
                titleSend: "Sol et ses réseaux",
                title: "Sol y sus redes",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.4",
                title: "Amin y sus estados emocionales",
                titleSend: "Amin et ses états émotionnels",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.5",
                titleSend: "Masako et sa vie familiale",
                title: "Masako y su vida familiar",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.6",
                titleSend: "Lucas et les personnes à qui il parle",
                title: "Lucas y las personas con las que habla",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.7",
                title: "Ajay, sus intereses y contactos",
                titleSend: "Ajay, ses centres d’intérêt et ses contacts",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              },
              {
                id: "2.5.8",
                titleSend: "Rokaya et sa réputation en ligne",
                title: "Rokaya y su reputación online",
                type: "scale",
                options: {
                  firstLabel: "No, tiene importancia",
                  lastLabel: "Sí, es muy grave"
                }
              }
            ]
          }
        }
      ]
    }
  ]
}

export const getQuestionary = () => {
  return i18n.language === "es" ? QUESTIONARY_ES : i18n.language === "en" ? QUESTIONARY_EN : QUESTIONARY
}
