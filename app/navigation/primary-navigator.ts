import createNativeStackNavigator from "react-native-screens/createNativeStackNavigator"
import screenSetup from "../screens"
import { CommunityNavigator } from "navigation/community-navigator"
import createStackNavigator from "react-native-screens/createNativeStackNavigator"
import {TransitionPresets} from "react-navigation-stack"

export const GaoNavigator = createNativeStackNavigator(screenSetup.screens, screenSetup.meta)
export const PrimaryNavigator = createStackNavigator(
  {
    gaoStack: GaoNavigator,
    community: CommunityNavigator
  },
  {
    initialRouteName: "gaoStack",
    mode: "modal",
    headerMode: "none",
    transparentCard: true,
    defaultNavigationOptions: {
      ...TransitionPresets.FadeFromBottomAndroid,
      cardStyle: {
        backgroundColor: "transparent",
      }
    }
  }
)

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 */
export const exitRoutes: string[] = screenSetup.exitRoutes
