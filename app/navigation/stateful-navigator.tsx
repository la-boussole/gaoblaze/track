import * as React from "react"
import { observer } from "mobx-react-lite"
// @ts-ignore: until they update @type/react-navigation
import { getNavigation, NavigationScreenProp, NavigationState } from "react-navigation"
import { useStores } from "../models/root-store"
import { RootNavigator } from "./root-navigator"
import {useEffect, useRef} from "react"
import { useObserver } from "mobx-react"
import { PlayBack } from "components/sound/play-back"
import {AppState} from "react-native"
import { ToastManager } from '../components/toast/ToastManager'

let currentNavigation: NavigationScreenProp<NavigationState> | undefined

export const StatefulNavigator: React.FunctionComponent<{}> = observer(() => {
  const {
    navigationStore: { state, dispatch, actionSubscribers },
  } = useStores()
  const rootStore = useStores()
  const refSound = useRef()
  const {metaGame} = useStores()

  const refNavigator = useRef()

  currentNavigation = getNavigation(
    RootNavigator.router,
    state,
    dispatch,
    actionSubscribers(),
    {},
    () => currentNavigation,
  )

  useEffect(() => {
    rootStore.setLastDate()
    AppState.addEventListener("change",
      handleAppStateChange)
    return () => AppState.removeEventListener("change", handleAppStateChange)
  }, [])

  const resetLastDate = () => {
    rootStore.setLastDate()
  }

  const handleAppStateChange = (state) => {
    if(state === "active")
      resetLastDate()
    else if(state === "background") {
      const date = rootStore.lastDate
      metaGame.addTimeInGame(new Date().getTime() - date.getTime())
      resetLastDate()
    }
  }

  return useObserver(() => {
    if(refSound.current) {
      if(!rootStore.playSong) {
        refSound.current.stop()
      } else if(rootStore.getSong()&&refSound.current.whoosh&&(!rootStore.getSong().startsWith(refSound.current.whoosh._filename)
        || !refSound.current.whoosh._playing)) {
        refSound.current.restart(rootStore.getSong())
      }
    }
    return (
      <>
        {<PlayBack song={rootStore.song} ref={refSound} mute={!rootStore.playSong} />}
        <RootNavigator ref={refNavigator} navigation={currentNavigation}  />
        { rootStore.getToast() && <ToastManager message={rootStore.getToast()} navigation={currentNavigation} /> }
      </>)
  })
})
