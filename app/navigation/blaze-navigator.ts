import createNativeStackNavigator from "react-native-screens/createNativeStackNavigator"
import screenData from "../screens/full-blaze"
import packageScreens from "screens/utils"

const screenSetup = packageScreens(screenData)
export const BlazeNavigator = createNativeStackNavigator(screenSetup.screens, screenSetup.meta)

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 */
export const exitRoutes: string[] = screenSetup.exitRoutes
