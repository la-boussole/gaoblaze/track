import { PrimaryNavigator } from "./primary-navigator"
import { BlazeNavigator } from "navigation/blaze-navigator"
import createStackNavigator from "react-native-screens/createNativeStackNavigator"

const BAKE_BLAZE_INTO_UNIQUE_APP = true

export const RootNavigator = createStackNavigator(
  Object.assign({},
    {
      gaoGames: { screen: PrimaryNavigator },
    },
    BAKE_BLAZE_INTO_UNIQUE_APP ? { blazeStack: { screen: BlazeNavigator } } : {}),
  {
    headerMode: "none",
    navigationOptions: { gesturesEnabled: false },
  },
)
