import * as React from "react"
import {View, Image, Text, ImageSourcePropType, TextStyle} from "react-native"
import {NavigationScreenProps, FlatList} from "react-navigation"
import {GaoDialog, Button, Wallpaper, GaoTopbar, ImageButton} from "components"
import {useTranslation} from "react-i18next"
import {useStores} from "models/root-store"
import * as styles from "gao-theme/styles"
import RNFS from "react-native-fs"



/* *** Constants to be fine tuned by game design *** */

export interface ShopItem {
  id: string
  assetsPath: ImageSourcePropType
  price: number
}
let URIGIFT : any
let SCOREGIFT : any
const gifs = [
  {
    id: "gao-Gao_the_Athlete.gif",
    image: require("assets/images/gif_store/gao-Gao_the_Athlete.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_Athlete.gif"),
    price: 40
  },
  {
    id: "gao-Gao_the_fileKeeper.gif",
    image: require("assets/images/gif_store/gao-Gao_the_fileKeeper.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_fileKeeper.gif"),
    price: 40
  },
  {
    id: "gao-Gao_the_forger.gif",
    image: require("assets/images/gif_store/gao-Gao_the_forger.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_forger.gif"),
    price: 40
  },
  {
    id: "gao-Gao_the_forger_intro.gif",
    image: require("assets/images/gif_store/gao-Gao_the_forger_intro.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_forger_intro.gif"),
    price: 40

  },
  {
    id: "gao-Gao_the_forger_play.gif",
    image: require("assets/images/gif_store/gao-Gao_the_forger_play.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_forger_play.gif"),
    price: 40
  },
  /*{
    id: "gao-Gao_the_intruder.gif",
    assetsPath: IMAGES_DIRECTORY + "gao-Gao_the_intruder.gif",
    price: 40
  },*/
  {
    id: "gao-Gao_the_photographer.gif",
    image: require("assets/images/gif_store/gao-Gao_the_photographer.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_photographer.gif"),
    price: 40
  },
  {
    id: "gao_gao_the_prosecutor.gif",
    image: require("assets/images/gif_store/gao-Gao_the_prosecutor.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_prosecutor.gif"),
    price: 40
  },
  {
    id: "gao-Gao_the_sage.gif",
    image: require("assets/images/gif_store/gao-Gao_the_sage.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_sage.gif"),
    price: 40
  },
  {
    id: "gao-gao-Gao_the_spy.gif",
    image: require("assets/images/gif_store/gao-Gao_the_spy.png"),
    assetsPath: require("assets/images/download_gifs/gao-Gao_the_spy.gif"),
    price: 40
  }
]

/* *** */

export interface GifsScreenProps extends NavigationScreenProps<{}> {
  onGoBack: () => void
}

export const GifsScreen: React.FunctionComponent<GifsScreenProps> = (props) => {
  const {t} = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props
  const {onGoBack} = props
  const [showGiftDialog, setShowGiftDialog] = React.useState(false)
  const [showNoPawnsDialog, setShowNoPawnsDialog] = React.useState(false)

  return (
    <View style={styles.mainView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/MemesScreenAlpha.png")}/>
      <GaoTopbar onGoBack={onGoBack} navigation={navigation} /* TODO: use GaoContainer */ />
      <View style={styles.tradePawsScreenflexRowView}>
        <Text
          style={styles.titleText}>
          {t("common:gifs", "Gifs")}
        </Text>
      </View>
      <View style={styles.tradePawsContainer}>
        <FlatList
          data={gifs}
          renderItem={({item}) => (
            <View style={styles.flatListItemView}>
              <ImageButton
                blurRadious={metaGame.bought.includes(item.id)?0.99:null}
                disabled={metaGame.bought.includes(item.id)}
                uri={item.image?item.image:{uri: item.assetsPath}}
                style={{width: 70, height: 70, resizeMode: item.id==="gao_gao_the_prosecutor.gif"||item.id==="gao-Gao_the_forger.gif"?"contain":"cover"}} // FIXME
                onPress={() => {
                  if(metaGame.paws >= item.price) {
                    metaGame.buyItem(item)
                      .then(success => {
                        URIGIFT=item.assetsPath
                        SCOREGIFT=item.price
                        setShowGiftDialog(true)
                        setTimeout(() => {
                          setShowGiftDialog(false)
                        }, 4000)
                      }) // Do something with the success or not.
                      .catch(error => console.error("buy action", error)) // Do something with the failure.
                  } else {
                    setShowNoPawnsDialog(true)
                    setTimeout(() => setShowNoPawnsDialog(false), 2000)
                  }
                }}
              />
              <View style={styles.flatListScoreView}>
                <Image source={metaGame.bought.includes(item.id)?require("assets/images/common/right.png"):require("assets/images/common/pawn.png")} style={styles.pawnImage} />
                {!metaGame.bought.includes(item.id) && <Text style={styles.defaultSecondaryText}>{item.price}</Text>}
              </View>
            </View>
          )}
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
      { showGiftDialog ? <GaoGiftDialog /> : null }
      { showNoPawnsDialog ? <GaoNoPawnsDialog /> : null }
      <View style={styles.spacerView}/>
    </View>
  )
}

const GaoGiftDialog = props => {
  const {t} = useTranslation()
  return (
    <GaoDialog
      preset="gift"
      textDialog1={t("common:gift", "A GIFT FOR YOU")}
      navigation={props.navigation}
      textMoney={SCOREGIFT}
      uriGiftImage={URIGIFT}
      uriImage={require("assets/images/avatar/gao_the_sage.png")} // FIXME: add image
        // FIXME: change rewards for game design
    />
  )
}

const GaoNoPawnsDialog = props => {
  const {t} = useTranslation()
  return (
    <GaoDialog
      textDialog2={t("common:no-paws", "Not enough Paws! Meow ^^")}
      textStyle2={{color: "#EB358A"}}
      uriImage={require("assets/images/common/sad_emoji.png")}
        // FIXME: change rewards for game design
    />
  )
}
