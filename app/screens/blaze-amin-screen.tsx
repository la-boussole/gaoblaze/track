import * as React from "react"
import {ImageButton, Wallpaper} from "../components"
import {Animated, Dimensions, Image, Text, TouchableOpacity, View} from "react-native"
import {createRef, useEffect, useRef, useState} from "react"
import {color} from "gao-theme"
import {typography} from "blaze-theme"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next";

const IMAGES = [
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin1.png"),
    waitTime: 2000,
    percent: 100,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin1.png"),
    waitTime: 2000,
    percent: 95,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin1.png"),
    waitTime: 2000,
    percent: 90,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 85,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 80,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 75,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 70,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin7.png"),
    waitTime: 2000,
    percent: 65,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin7.png"),
    waitTime: 2000,
    percent: 60,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin7.png"),
    waitTime: 2000,
    percent: 55,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    message: "message-3",
    glowImage: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_both_glow.png"),
    glowImage: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    percent: 50,
    type: "choose",
    coffee: 11 + 1,
    cell: 29
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin12.png"),
    waitTime: 2000,
    percent: 50,
  },
  /*{
    image: require("assets/images/blaze-character-scenes/01-amin/Amin1.png"),
    waitTime: 2000,
    percent: 100,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin1.png"),
    waitTime: 2000,
    percent: 95,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin1.png"),
    waitTime: 2000,
    percent: 90,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 85,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 80,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 75,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    waitTime: 2000,
    percent: 70,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin7.png"),
    waitTime: 2000,
    percent: 65,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin7.png"),
    waitTime: 2000,
    percent: 60,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin7.png"),
    waitTime: 2000,
    percent: 55,
  },*/
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    message: "message-1",
    glowImage: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/50_glow_cell.png"),
    glowImage: require("assets/images/blaze-character-scenes/01-amin/50_always.png"),
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin2.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin3.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin4.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin5.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin8.png"),
    waitTime: 2000,
    percent: 45,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin9.png"),
    waitTime: 2000,
    percent: 40,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin9.png"),
    waitTime: 2000,
    percent: 35,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
    waitTime: 2000,
    percent: 30,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
    message: "message-2",
    glowImage: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 20,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 15,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 10,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 5,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin14.png"),
    waitTime: 2000,
    percent: 1,
    exit: true,
  },

  //CELLPHONE
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin2.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin3.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin4.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin5.png"),
    waitTime: 2000,
    percent: 50,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin8.png"),
    waitTime: 2000,
    percent: 45,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin9.png"),
    waitTime: 2000,
    percent: 40,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin9.png"),
    waitTime: 2000,
    percent: 35,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
    waitTime: 2000,
    percent: 30,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
    message: "message-4",
    glowImage: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/25_cofee_glow.png"),
    glowImage: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
    percent: 25,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin12.png"),
    waitTime: 2000,
    percent: 25,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
    message: "message-2",
    glowImage: require("assets/images/blaze-character-scenes/01-amin/allways_25.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 20,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 15,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 10,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/0_25.png"),
    waitTime: 2000,
    percent: 5,
  },
  {
    image: require("assets/images/blaze-character-scenes/01-amin/Amin14.png"),
    waitTime: 2000,
    percent: 1,
  },
]

export const BlazeAminScreen = props => {
  const rootStore = useStores()
  const {navigation} = props
  const [count, setCount] = React.useState(0)
  const { metaGame } = useStores()
  const {t} = useTranslation()
  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(0.0))
  const [fadeAnimAll, setFadeAnimAll] = React.useState(new Animated.Value(0.0))
  const [checked, setChecked] = React.useState(0)
  const [loadImages, setLoadImages] = React.useState(IMAGES.filter(item => item.image).length - IMAGES.filter(item => item.image.uri).length)

  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("amin", null, t)
    fadeIn.start()
    startIndex(0)
    return () => rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
  }, [])

  const fadeIn = Animated.timing(fadeAnimAll, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnimAll, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const finish = () => {
    fadeOut.start(() => {
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
    })
  }

  const startAnimation = (index) => {
    if (IMAGES[index].waitTime) {
      setTimeout(() => {
        if (index === IMAGES.length - 1 || IMAGES[index].exit) {
          finish()
        } else {
          startIndex(index + 1)
          setCount(index + 1)
        }
      }, IMAGES[index].waitTime)
    }
  }

  const startAnimationGlow = (anim, otherAnim) => {
    anim.start(({finished}) => {
      if (finished) {
        startAnimationGlow(otherAnim, anim)
      }
    })
    return true
  }

  const startIndex = (index) => {
    if(IMAGES[index].waitTime) {
      startAnimation(index)
    }
    if(IMAGES[index].glowImage) {
      startAnimationGlow(asc, desc)
    }
  }

  return (
    <>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {IMAGES.filter(item => item.image).map((item, index) => <Image key={"load-image-"+index} style={{opacity: 0, position: "absolute"}} source={item.image} onLoadEnd={() => setLoadImages(loadImages - 1)} />)}
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        /*loadImages <= 0 &&*/
    <Animated.View style={{flex: 1, opacity: fadeAnimAll}}>
      <View onTouchEndCapture={(event) => {
        if (IMAGES[count].glowImage && IMAGES[count].type !== "choose" && !IMAGES[count].message) {
          asc.stop()
          desc.stop()
          if (count === IMAGES.length - 1) {
            finish()
          } else {
            startIndex(count + 1)
            setCount(prevCount => prevCount + 1)
          }
        }
      }} style={{flex: 1, backgroundColor: "white"}}>
        {
          IMAGES[count].glowImage &&
          (
            <>
              {
                IMAGES[count].message && (
                  <View style={{width: "100%", height: 300, zIndex: 100, position: "absolute"}} onTouchEndCapture={() => {
                    asc.stop()
                    desc.stop()
                    if (count === IMAGES.length - 1) {
                      finish()
                    } else {
                      startIndex(count + 1)
                      setCount(prevCount => prevCount + 1)
                    }
                  }}>
                    <Image source={require("assets/images/blaze-character-scenes/01-amin/bubble.png")} style={{width: "100%", height: "100%", resizeMode: "stretch", position: "absolute"}} />
                    <Text style={{marginHorizontal: 70, marginVertical: 20, fontFamily: typography.secondary, fontSize: 20}}>{t("amin:"+IMAGES[count].message, "No message")}</Text>
                  </View>
                )
              }
              {
                IMAGES[count].percent && (
                  <View
                    style={{width: 200, height: 30, borderWidth: 5, borderColor: "black", borderRadius: 5, top: 20, left: 10, position: "absolute", zIndex: 100}}>
                    <View style={{height: "100%", width: IMAGES[count].percent + "%", backgroundColor: "black"}} />
                  </View>
                )
              }
              {
                IMAGES[count].type === "choose" && (
                  <>
                    <View style={{position: "absolute", bottom: 0, height: "20%", width: "27%", zIndex: 120,}} onTouchEndCapture={() => {
                      startIndex(IMAGES[count].coffee)
                      setCount(IMAGES[count].coffee)
                    }} />
                    <View style={{position: "absolute", bottom: 0, left: "27%", height: "20%", width: "25%", zIndex: 120,}} onTouchEndCapture={() => {
                      startIndex(IMAGES[count].cell)
                      setCount(IMAGES[count].cell)
                    }} />
                  </>
                )
              }
              <Animated.View style={{zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%"}}>
                <Wallpaper
                  backgroundImage={IMAGES[count].glowImage}/>
              </Animated.View>
            </>
          )
        }
        <Wallpaper backgroundImage={IMAGES[count].image}/>
        {
          IMAGES[count].percent && (
            <View
              style={{width: 200, height: 30, borderWidth: 5, borderColor: "black", borderRadius: 5, top: 20, left: 10}}>
              <View style={{height: "100%", width: IMAGES[count].percent + "%", backgroundColor: "black"}} />
            </View>
          )
        }
      </View>
    </Animated.View>
      }
    </>
  )
}
