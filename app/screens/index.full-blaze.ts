import packageScreens from "./utils"
import setup from "./full-blaze"

export default packageScreens(setup)
