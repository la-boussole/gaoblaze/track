import * as React from "react"
import { observer } from "mobx-react-lite"
import { Image, View } from "react-native"
import { Wallpaper, GaoTopbar, GaoDialog } from "../components"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { NavigationScreenProp } from "react-navigation"
import { useState } from "react"

export interface AlexVsTheInternetEventScreenProps {
  navigation: NavigationScreenProp<{}>
}

export interface InteractiveAnimationSceneConfiguration {
  component: React.ReactNode
  fadeIn?: string
  fadeOut?: string
}

export interface InteractiveAnimationProps {
  scenes: Array<InteractiveAnimationSceneConfiguration>
  onFinished?: () => void
}

export interface InteractiveAnimationSceneProps {
  onNext: () => void
}

const InteractiveAnimation: React.FC<InteractiveAnimationProps> = (props) => {
  const [currentScene, setScene] = useState(0)
  const onFinished = props.onFinished || (() => null)

  // TODO: support fadeIn, fadeOut property using Animated?
  if (!props.scenes.length)
    return null

  const curSceneConfiguration = props.scenes[currentScene]
  return React.createElement(curSceneConfiguration.component, {
    onNext: () => {
      if (currentScene + 1 === props.scenes.length) {
        onFinished()
      } else {
        setScene(currentScene + 1)
      }
    }
  })
}

const LoadScene1: React.FC<InteractiveAnimationSceneProps> = (props) => {
  return (
    <Image source={require("assets/images/gao-alex-gifs/Alex_Animation.gif")} />
  )
}

const LoadScene2: React.FC<InteractiveAnimationSceneProps> = (props) => {
  return (
    <Image source={require("assets/images/gao-alex-gifs/Alex_Animation2.gif")} />
  )
}


export const AlexVsTheInternetEvent: React.FunctionComponent<AlexVsTheInternetEventScreenProps> = observer((props) => {
  return (
    <View style={styles.wrapperColumnView}>
      <InteractiveAnimation scenes={[
        { component: LoadScene1, fadeIn: "white" },
        { component: LoadScene2, fadeOut: "black" }
      ]} />
    </View>
  )
})
