import {BackHandler, Image, Linking, ScrollView, Text, View} from "react-native"
import * as React from "react"
import * as styles from "blaze-theme/styles"
import { Wallpaper } from "components"
import { TouchableOpacity } from "react-native-gesture-handler"
import { length, lensPath } from "ramda"
import { LastMessageQuestionaryScreen } from "./last-message-questionary-screen"
import {useStores} from "models/root-store"
import {NavigationScreenProp} from "react-navigation"
import {useTranslation} from "react-i18next"
import {color} from "blaze-theme"
import i18n from "i18next";

export interface LastMessageScreenProps extends NavigationScreenProp<{}> {

}


export const LastMessageScreen: React.FunctionComponent<LastMessageScreenProps> = (props) => {
  const [selectedEnding, setSelectedEnding] = React.useState(null)
  const [position, setPosition] = React.useState(1000)
  const { navigation } = props
  const [showQuestionary, setShowQuestionary] = React.useState(false)
  const {metaGame} = useStores()
  const rootStore = useStores()
  const {t} = useTranslation()

  const ENDINGS = [
    {
      images: [
        {
          image: i18n.language === "es" ? require("assets/images/end_game/A/es/A1.png") :
            i18n.language === "en" ? require("assets/images/end_game/A/en/A1.png") :
              require("assets/images/end_game/A/A1.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/A/es/A2.png") :
            i18n.language === "en" ? require("assets/images/end_game/A/en/A2.png") :
              require("assets/images/end_game/A/A2.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/A/es/A3.png") :
            i18n.language === "en" ? require("assets/images/end_game/A/en/A3.png") :
              require("assets/images/end_game/A/A3.png"),
          isChat: true,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/A/es/A5.png") :
            i18n.language === "en" ? require("assets/images/end_game/A/en/A5.png") :
              require("assets/images/end_game/A/A5.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/A/es/A6.png") :
            i18n.language === "en" ? require("assets/images/end_game/A/en/A6.png") :
              require("assets/images/end_game/A/A6.png"),
          isChat: false,
          isButton: false
        },
        {
          image: require("assets/images/end_game/A/A4_glow.jpg"),
          isChat: false,
          isButton: true
        },
      ],
    },
    {
      images: [
        {
          image: i18n.language === "es" ? require("assets/images/end_game/B/es/B1.png") :
            i18n.language === "en" ? require("assets/images/end_game/B/en/B1.png") :
              require("assets/images/end_game/B/B1.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/B/es/B2.png") :
            i18n.language === "en" ? require("assets/images/end_game/B/en/B2.png") :
              require("assets/images/end_game/B/B2.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/B/es/B3.png") :
            i18n.language === "en" ? require("assets/images/end_game/B/en/B3.png") :
              require("assets/images/end_game/B/B3.png"),
          isChat: true,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/B/es/B5.png") :
            i18n.language === "en" ? require("assets/images/end_game/B/en/B5.png") :
              require("assets/images/end_game/B/B5.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/B/es/B6.png") :
            i18n.language === "en" ? require("assets/images/end_game/B/en/B6.png") :
              require("assets/images/end_game/B/B6.png"),
          isChat: false,
          isButton: false
        },
        {
          image: require("assets/images/end_game/B/B4_glow.jpg"),
          isChat: false,
          isButton: true
        },
      ],
    },
    {
      images: [
        {
          image: i18n.language === "es" ? require("assets/images/end_game/C/es/C1.png") :
            i18n.language === "en" ? require("assets/images/end_game/C/en/C1.png") :
              require("assets/images/end_game/C/C1.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/C/es/C2.png") :
            i18n.language === "en" ? require("assets/images/end_game/C/en/C2.png") :
              require("assets/images/end_game/C/C2.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/C/es/C3.png") :
            i18n.language === "en" ? require("assets/images/end_game/C/en/C3.png") :
              require("assets/images/end_game/C/C3.png"),
          isChat: true,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/C/es/C5.png") :
            i18n.language === "en" ? require("assets/images/end_game/C/en/C5.png") :
              require("assets/images/end_game/C/C5.png"),
          isChat: false,
          isButton: false
        },
        {
          image: i18n.language === "es" ? require("assets/images/end_game/C/es/C6.png") :
            i18n.language === "en" ? require("assets/images/end_game/C/en/C6.png") :
              require("assets/images/end_game/C/C6.png"),
          isChat: false,
          isButton: false
        },
        {
          image: require("assets/images/end_game/C/C4_glow.jpg"),
          isChat: false,
          isButton: true
        },
      ],
    },
  ]

  React.useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    //navigation.navigate("developer")
    return true
  }

  return <>
    {(!selectedEnding || position >= selectedEnding.images.length) && !showQuestionary && <View style={styles.lastMessageMainQuestionContainer}>
      <Wallpaper preset="cover" backgroundImage={require("assets/images/end_game/background.png")} blurRadious={0.99} />
      <Wallpaper preset="cover" backgroundImage={require("assets/images/wallpaper/blaze/wallpaper_green.png")} blurRadious={0.99} style={{ opacity: 0.9, height: "110%" }} />
      <View style={styles.lastMessageQuestionContainer}>
        <Text style={styles.lastMessageQuestionTitle}>{t("blaze:last-message", "Que vas-tu faire maintenant ?")}</Text>
        <TouchableOpacity style={styles.lastMessageQuestionButton} onPress={() => {
          metaGame.setLastOptionSelected("What will you do now?")
          setPosition(0)
          setSelectedEnding(ENDINGS[0])
        }}>
          <Text style={styles.lastMessageQuestionButtonText}>{t("blaze:last-message-option-1", "Je vais encourager Alex à engager une action en justice contre le Studio.")}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.lastMessageQuestionButton} onPress={() => {
          metaGame.setLastOptionSelected("I will encourage Alex to take legal action against the Studio.")
          setPosition(0)
          setSelectedEnding(ENDINGS[1])
        }}>
          <Text style={styles.lastMessageQuestionButtonText}>{t("blaze:last-message-option-2", "Je vais encourager Alex à accepter l'accord de non-divulgation.")}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.lastMessageQuestionButton} onPress={() => {
          metaGame.setLastOptionSelected("I will encourage Alex to accept the non-disclosure deal.")
          setPosition(0)
          setSelectedEnding(ENDINGS[2])
        }}>
          <Text style={styles.lastMessageQuestionButtonText}>{t("blaze:last-message-option-3", "Je vais remettre ce que j'ai découvert au Studio. Je veux des Papattes !")}</Text>
        </TouchableOpacity>
      </View>
    </View>
    }
    {
      selectedEnding && !showQuestionary && position < selectedEnding.images.length &&
      selectedEnding.images[Math.min(position, selectedEnding.images.length - 1)].isButton &&
      <View style={styles.lastMessageMainQuestionContainer}>
        <Image source={require("assets/images/end_game/questionary/background_questionary.png")} style={{position: "absolute", width: "100%", height: "100%"}}/>
        <View style={{margin: 40}}>
          <View style={{flex: 1}}>
            <Text style={{
              color: color.palette.letterGreen,
              fontSize: 40,
              fontFamily: "Roboto-Black",
              alignSelf: "center",
              marginBottom: 10
            }}>{t("blaze:disclamer-title", "AU FAIT...")}</Text>
            <ScrollView style={{flex: 1}}>
              <Text style={{
                color: color.palette.letterGreen,
                borderRadius: 5,
                fontFamily: "Roboto-Regular",
                fontSize: 18,
              }}>
                {t("blaze:disclamer-line-1", "Quel plaisir de te voir arriver au bout du jeu !")}{"\n"}{"\n"}
                {t("blaze:disclamer-line-2", "Beaucoup d’applis « gratuites » se payent en t’espionnant sans que tu ne le saches, contrairement à Gao&Blaze qui est libre, gratuite et respectueuse de tes données.")}{"\n"}{"\n"}
                {t("blaze:disclamer-line-3", "Ce jeu a été créé dans un projet de recherche non lucratif sur la protection des données, pour lequel nous avons besoin de ton aide.")}{"\n"}{"\n"}
                {t("blaze:disclamer-line-4", "Pour te remercier, nous avons préparé un cadeau : le CAT Scan, un mini scanner pour identifier les applis qui demandent trop de données sur ton smartphone.")}{"\n"}{"\n"}
                {t("blaze:disclamer-line-5", "Acceptes-tu de nous aider en donnant 1 minute pour participer à notre enquête?")}
              </Text>
            </ScrollView>
          </View>
          <View style={{width: "100%"}}>
            <View style={{flexDirection: "row", justifyContent: "space-between", marginBottom: 5}}>
              <View style={styles.buttonQuestionary}><TouchableOpacity onPress={() => {
                rootStore.setSong("blaze_minigames.ogg", false)
                //setPosition(position + 1)
                setShowQuestionary(true)
              }}>
                <Text style={{textAlign: "center"}}>{t("common:yes", "Oui")}</Text>
              </TouchableOpacity></View>
              <View style={styles.buttonQuestionary}><TouchableOpacity
                onPress={() => {
                  console.log("Aca 3")
                  rootStore.setSong("blaze_background.ogg", false)
                  navigation.navigate("credits")
                }}>
                <Text style={{textAlign: "center"}}>{t("common:no", "Non")}</Text>
              </TouchableOpacity></View>
            </View>
            <View><TouchableOpacity
              style={[styles.buttonQuestionary, {alignSelf: "center"}]}
              onPress={() => {
                Linking.openURL("https://gaoandblaze.org/confidentialite-et-donnees1/")
              }}>
              <Text style={{textAlign: "center"}}>{t("common:more-info", "Plus")}</Text>
            </TouchableOpacity></View>
          </View>
        </View>
      </View>
    }
    {selectedEnding && !showQuestionary && position < selectedEnding.images.length &&
      !selectedEnding.images[Math.min(position, selectedEnding.images.length - 1)].isButton && <View style={styles.lastMessageMainQuestionContainer}>
      <Image source={selectedEnding.images[Math.min(position, selectedEnding.images.length - 1)].image} style={styles.lastMessageEndingImage} />
      <View style={{ height: selectedEnding.images[Math.min(position, selectedEnding.images.length - 1)].isChat ? 100 : selectedEnding.images[Math.min(position, selectedEnding.images.length - 1)].isButton ? 0 : 50, backgroundColor: "#cccccc00", position: "absolute", bottom: 0, width: "100%" }} onTouchEndCapture={() => {
        if (position < selectedEnding.images.length - 1) {
          if(selectedEnding.images[position+1].isButton) {
            rootStore.setSong("blaze_minigames.ogg", false)
          }
          setPosition(position + 1)
        }
        else {
          console.log("Aca 2")
          rootStore.setSong("blaze_background.ogg", false)
          navigation.navigate("credits")
        }
      }}></View>
      {selectedEnding.images[Math.min(position, selectedEnding.images.length - 1)].isButton &&
        <View style={{ height: 50, backgroundColor: "#cccccc00", position: "absolute", bottom: 20, right: 20, width: 50 }} onTouchEndCapture={() => setShowQuestionary(true)}>
          <Image style={{ width: "100%", height: "100%", resizeMode: "stretch" }} source={require("assets/images/blaze-icons/right_arrow.png")} />
        </View>
      }
    </View>}
    {showQuestionary && <LastMessageQuestionaryScreen onFinish={() => {
      rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
      console.log(position, selectedEnding.images.length)
      if (position < selectedEnding.images.length - 1) {
        if(selectedEnding.images[position+1].isButton) {
          rootStore.setSong("blaze_minigames.ogg", false)
        }
        setPosition(position + 1)
      }
      else {
        console.log("Aca 1")
        rootStore.setSong("blaze_background.ogg", false)
        navigation.navigate("credits")
      }
    }} />}
  </>
}
