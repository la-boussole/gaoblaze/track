import * as React from "react"
import { BackHandler, TouchableOpacity, View } from "react-native"
import { BlazeContainer, ImageButton, Text } from "../components"
import { NavigationScreenProp } from "react-navigation"
import * as styles from "blaze-theme/styles"
import { useTranslation } from "react-i18next"
import { useEffect } from "react"
import { useStores } from "models/root-store"
//import { BLAZE_PROFILES } from "assets/community_members/blaze-profiles"
import { ScrollView } from "react-native-gesture-handler"
import { BlazeNotebookThoughtPopup } from "./blaze-notebook-thought-popup"
import { useObserver } from "mobx-react"
import { BlazeNotebookSkillPopup } from "./blaze-notebook-skill-popup"
//import { BLAZE_PROFILES } from "assets/community_members/blaze-profiles"
import { BLAZE_PROFILES_FR } from "assets/community_members/blaze-profiles-FR"
import { BLAZE_PROFILES_ES } from "assets/community_members/blaze-profiles-ES"
import { BLAZE_PROFILES_EN } from "assets/community_members/blaze-profiles-EN"
import i18n from "i18n/i18n"

export interface BlazeNotebookScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeNotebookScreen: React.FunctionComponent<BlazeNotebookScreenProps> = BlazeContainer((props) => {
  const BLAZE_PROFILES = i18n.language === 'es' ? BLAZE_PROFILES_ES : i18n.language === 'en' ? BLAZE_PROFILES_EN : BLAZE_PROFILES_FR
  const { t } = useTranslation()
  const { navigation } = props
  const { metaGame } = useStores()
  const [activeTab, setActiveTab] = React.useState("knowledge")
  const thoughtDataSource = BLAZE_PROFILES.filter(item => item.thought && metaGame.showThoughts.includes(item.thought.title)).map(item => item.thought)
  const [activeItems, setActiveItems] = React.useState([])
  const paramThought = navigation.getParam("tought", null)

  const [thought, setThought] = React.useState(paramThought ? BLAZE_PROFILES.find(item => item.thought && item.thought.title === paramThought).thought : null)
  const [thoughtSkill, setThoughtSkill] = React.useState(null)
  const [idSelectedSkill, setIdSelectedSkill] = React.useState(null)

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [activeTab, activeItems])

  const handleBackButtonClick = () => {
    navigation.navigate("database")
    return true
  }

  const getRow = (data) => {

    return (<View key={"know-" + data.index} style={styles.skillThoughtContainer}>
      <TouchableOpacity style={[styles.notebookToughtContainer, !metaGame.readThoughts.includes(data.item.title) ? styles.notebookUnreadToughtContainer : null]} onPress={() => {
        setThought(data.item)
      }}>
        <Text style={[styles.notebookToughtTitle, !metaGame.readThoughts.includes(data.item.title) ? styles.notebookUnreadToughtTitle : null]}>{t("blaze:thoughts", "Réflexions")} {data.item.title}</Text>
        <Text style={[styles.notebookToughtSubTitle, !metaGame.readThoughts.includes(data.item.title) ? styles.notebookUnreadToughtTitle : null]} numberOfLines={2}>{data.item.tweet ? data.item.tweet : data.item.overview}</Text>
      </TouchableOpacity>
    </View>)
  }

  const getRowSkills = (data) => {
    return (<View key={"skill-" + data.index} style={styles.skillThoughtContainer}>
      <View style={[styles.notebookToughtContainer, !metaGame.readThoughts.includes(data.item.title) ? styles.notebookUnreadToughtContainer : null]}>
        {/*<Text style={styles.notebookToughtTitle}>Tought {data.item.title}</Text>*/}
        <Text style={styles.notebookToughtSubTitle} numberOfLines={2}>{data.item.tweet ? data.item.tweet : data.item.overview}</Text>
      </View>
      <View style={styles.skillContainer}>
        <View style={styles.skillContainerButton}>
          <View style={styles.skillLine1} />
          <TouchableOpacity style={metaGame.readSkills.includes(data.item.title+"-0")?styles.skillButton:styles.skillButtonUnread}>
            <Text style={[styles.skillButtonText, !metaGame.readSkills.includes(data.item.title+"-0") ? styles.skillButtonUnreadText : null]} onPress={() => {
              setIdSelectedSkill(0)
              setThoughtSkill(data.item)
            }}>
              {data.item.skills && data.item.skills.length > 0 && data.item.skills[0].tooltitle?data.item.skills[0].tooltitle:"Tool 1"}
            </Text>
          </TouchableOpacity>
        </View>
        { data.item.skills.length > 1 &&
          <View style={styles.skillContainerButton}>
            <View style={styles.skillLine2}/>
            <TouchableOpacity
              style={metaGame.readSkills.includes(data.item.title + "-1") ? styles.skillButton : styles.skillButtonUnread}
              onPress={() => {
                setIdSelectedSkill(1)
                setThoughtSkill(data.item)
              }}>
              <Text
                style={[styles.skillButtonText, !metaGame.readSkills.includes(data.item.title + "-1") ? styles.skillButtonUnreadText : null]}>
                {data.item.skills && data.item.skills.length > 1 && data.item.skills[1].tooltitle ? data.item.skills[1].tooltitle : "Tool 2"}
              </Text>
            </TouchableOpacity>
          </View>
        }
        {data.item.skills.length > 2 &&
        <View style={styles.skillContainerButton}>
          <View style={styles.skillLine3}/>
          <TouchableOpacity
            style={metaGame.readSkills.includes(data.item.title + "-2") ? styles.skillButton : styles.skillButtonUnread}
            onPress={() => {
              setIdSelectedSkill(2)
              setThoughtSkill(data.item)
            }}>
            <Text
              style={[styles.skillButtonText, !metaGame.readSkills.includes(data.item.title + "-2") ? styles.skillButtonUnreadText : null]}>
              {data.item.skills && data.item.skills.length > 2 && data.item.skills[2].tooltitle ? data.item.skills[2].tooltitle : "Tool 3"}
            </Text>
          </TouchableOpacity>
        </View>
        }
      </View>
    </View>)
  }

  return useObserver(() =>
    <>
      {
        thought &&
        <BlazeNotebookThoughtPopup navigation={navigation} thought={thought} onOk={() => {
          if(paramThought)
            navigation.navigate("blazeChat")
          else
            setThought(null)
        }
        } />
      }
      {
        thoughtSkill &&
        <BlazeNotebookSkillPopup navigation={navigation} skill={idSelectedSkill} thought={thoughtSkill} onOk={() => setThoughtSkill(null)} />
      }
      {
        !thought && !thoughtSkill &&
        <View style={styles.blazeChatContainer}>
          <View style={styles.topBarView}>
            <Text style={styles.h2TextTitleNotebook}>
              <Text style={{ ...styles.h2Text, ...styles.boldFontText }}>{t("blaze:thought", "Réflexions")}</Text>
            </Text>
            <View style={styles.spacerView} />
            {metaGame.notebookUnlocked() && (
              <ImageButton
                style={styles.iconImage}
                uri={require("assets/images/blaze-icons/book.png")}
                onPress={() => navigation.navigate("notebook")}
              />
            )}
            {(
              <ImageButton
                style={styles.iconImage}
                uri={require("assets/images/blaze-icons/left-arrow.png")}
                onPress={() => navigation.navigate("database")}
              />
            )}
          </View>
          <View style={styles.blazeChatContainerTab}>
            <View style={styles.blazeChatTabs}>
              <TouchableOpacity style={styles.blazeChatTab} onPress={() => {
                if (activeTab !== "knowledge") {
                  setActiveItems([])
                  setActiveTab("knowledge")
                }
              }}>
                <Text style={activeTab === "knowledge" ? styles.blazeChatTabSelectedText : styles.blazeChatTabText}>{t("blaze:knowledge", "Connaissance")}</Text>
              </TouchableOpacity>
              {metaGame.showSkills && <TouchableOpacity style={styles.blazeChatTab} onPress={() => {
                if (activeTab !== "skills") {
                  setActiveItems([])
                  setActiveTab("skills")
                }
              }}>
                <Text style={activeTab === "skills" ? styles.blazeChatTabSelectedText : styles.blazeChatTabText}>{t("blaze:skills", "Savoir-faire")}</Text>
              </TouchableOpacity>
              }
            </View>
            {
              <ScrollView style={styles.skillBodyContainer}>
                {thoughtDataSource.map((item, index) => activeTab === "knowledge" ? getRow({ item, index }) : getRowSkills({ item, index }))}
              </ScrollView>
            }
          </View>
        </View>
      }
    </>
  )
}, { isDebugMode: false })
