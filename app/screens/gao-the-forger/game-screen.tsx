import * as React from "react"
import {
  View,
  Image,
  TouchableOpacity,
  NativeModules,
  ImageStore,
  PermissionsAndroid,
  Alert,
  BackHandler
} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {GaoDialog, Text, StateMachine, GaoContainer, ImageButton, GaoFrame, Timer} from "components"
import {useTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"
import {useStores} from "models/root-store"
import {useEffect, useRef} from "react"
import {spacing} from "gao-theme"
import { PlayBack } from "components/sound/play-back"
import { connectRootStore } from "utils/decorators"

const imagesInformation = [
  {
    urlImageWallpaper: require("assets/images/gao-the-forger/wallpaper-1-B.png"),
    urlImageApp: require("assets/images/gao-the-forger/wallpaper-1-A.png"),
    points: [
      {xL: 308 - 50, yL: 450 - 50, xR: 400 + 50, yR: 550 + 50},
      {xL: 550 - 50, yL: 440 - 50, xR: 682 + 50, yR: 533 + 50},
      {xL: 600 - 50, yL: 559 - 50, xR: 704 + 50, yR: 648 + 50},
      {xL: 588 - 50, yL: 657 - 50, xR: 689 + 50, yR: 773 + 50},
      {xL: 612 - 50, yL: 1180 - 50, xR: 720 + 50, yR: 1260 + 50},
    ]
  },
  {
    urlImageWallpaper: require("assets/images/gao-the-forger/wallpaper-2-B.png"),
    urlImageApp: require("assets/images/gao-the-forger/wallpaper-2-A.png"),
    points: [
      {xL: 659 - 50, yL: 749 - 50, xR: 801 + 50, yR: 867 + 50},
      {xL: 516 - 50, yL: 1253 - 50, xR: 595 + 50, yR: 1367 + 50},
      {xL: 428 - 50, yL: 1390 - 50, xR: 570 + 50, yR: 1492 + 50},
      {xL: 374 - 50, yL: 1530 - 50, xR: 728 + 50, yR: 1616 + 50},
    ]
  },
  {
    urlImageWallpaper: require("assets/images/gao-the-forger/wallpaper-3-B.png"),
    urlImageApp: require("assets/images/gao-the-forger/wallpaper-3-A.png"),
    points: [
      {xL: 548 - 50, yL: 577 - 50, xR: 658 + 50, yR: 655 + 50},
      {xL: 293 - 50, yL: 659 - 50, xR: 423 + 50, yR: 767 + 50},
      {xL: 698 - 50, yL: 1075 - 50, xR: 901 + 50, yR: 1139 + 50},
      {xL: 667 - 50, yL: 1160 - 50, xR: 747 + 50, yR: 1245 + 50},
      {xL: 0 - 50, yL: 1457 - 50, xR: 796 + 50, yR: 1541 + 50},
    ]
  },
  {
    urlImageWallpaper: require("assets/images/gao-the-forger/wallpaper-4-A.png"),
    urlImageApp: require("assets/images/gao-the-forger/wallpaper-4-B.png"),
    points: [
      {xL: 674 - 50, yL: 127 - 50, xR: 803 + 50, yR: 534 + 50},
      {xL: 154 - 50, yL: 588 - 50, xR: 279 + 50, yR: 669 + 50},
      {xL: 712 - 50, yL: 616 - 50, xR: 802 + 50, yR: 755 + 50},
      {xL: 244 - 50, yL: 865 - 50, xR: 344 + 50, yR: 955 + 50},
      {xL: 511 - 50, yL: 1003 - 50, xR: 618 + 50, yR: 1109 + 50},
    ]
  }
]

// checkedIcon (used also in Filekeeper)
const CheckedIcon = props => {
  const {setState} = props
  const {state} = props
  const {index} = props
  return (
    <>
      {
        state.checked[index] &&
        <Image source={require("assets/images/common/right.png")} style={[styles.checkedIconImageGaoTheForge, {
          left: state.checked[index].x - spacing.larger / 2,
          top: state.checked[index].y - spacing.larger / 2
        }]}/>
      }
    </>
  )
}


// 1.Game rules
const ForgerGameRules = props => {
  const {setState} = props
  const {t} = useTranslation()
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "forger:rules",
          "Ton fond d'écran a changé! Trouve les 5 DIFFÉRENCES par rapport à l'image affichée ici avant la fin du temps imparti",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => setState({dialog: "GameCountDown"})}
        dialogViewStyle={{marginHorizontal: "-25%"}}
      />
    </View>
  )
}

// 2.Accept to change your phone wallpaper settings
const ForgerGameIntro = props => {
  const {setState} = props
  const {t} = useTranslation()
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "forger:welcome",
          "Gao apprend à peindre ! Aide-le à imiter les grands chefs d’œuvre",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={() => setState({dialog: "GameLoading"})}
        dialogViewStyle={{marginHorizontal: "-25%"}}
      />
    </View>
  )
}

// 3.GameLoading
const ForgerGameLoading = props => {
  const {t} = useTranslation()
  const {setState} = props
  const {state} = props
  const {metaGame} = useStores()

  useEffect(() => {
    const url = Image.resolveAssetSource(imagesInformation[state.imageIndex].urlImageWallpaper).uri
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
      title: t("permissions:storage.title", "Access Storage"),
      message: t("permissions:storage.message", "Access Storage for the pictures"),
      buttonNeutral: t("permissions:ask-me-later", "Ask Me Later"),
      buttonNegative: t("permissions:cancel", "Cancel"),
      buttonPositive: t("permissions:ok", "Ok"),
    }).then((response) => {
      if(response === "denied") {
        setState({dialog: "GamePermissionDenied"})
      } else {
        metaGame.addPermission("READ_EXTERNAL_STORAGE")
        NativeModules.TrackProjectMisc.getWallpaperBase64().then(result => {
          NativeModules.TrackProjectMisc.loadWallpaper(url).then(() => {
            setState({oldImage: result, dialog: "GameRules"})
          }).catch((error) => console.error(error))
        })
      }
    })
  })

  return (
    <View style={styles.containerView}>
      <View style={styles.contentViewGaoTheForger}>
        <Image
          source={require("assets/images/gao-the-forger/loading.png")}
          style={styles.loadingImage}
        />
        <Text
          style={styles.loadingTextGaoTheForger}>
          {t("forger:loading", "Ton fond d'écran est en train de changer...")}
        </Text>
      </View>
    </View>
  )
}

// 4.GameCountDown
const ForgerGameCountDown = props => {
  const {setState} = props
  const {state} = props

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", function() {
      NativeModules.TrackProjectMisc.loadWallpaperFromBase64(state.oldImage)
      return false
    })
    return handler.remove
  }, [])

  return (
    <View style={styles.containerView}>
      <View style={styles.contentViewGaoTheForger}>
        <Timer textStyle={styles.countdownTextForger} playerGivenTime={3} showGo={true} removeSound={true}
          finishTimer={() => setState({dialog: "GameStart"})} textType="normal"/>
        <Image
          source={imagesInformation[state.imageIndex].urlImageApp}
          style={styles.pictureGameImageGaoTheForge}
          blurRadius={0.99}
        />
      </View>
    </View>
  )
}

// 4.GameStart
const ForgerGameStart = props => {
  const {setState} = props
  const {metaGame} = useStores()
  const {state} = props
  const [timerRef, setTimerRef] = React.useState(null)
  const refSound = useRef()
  const rootStore = useStores()
  const {navigation} = props
  const {t} = useTranslation()

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", function() {
      NativeModules.TrackProjectMisc.loadWallpaperFromBase64(state.oldImage)
      return false
    })
    return () => handler.remove()
  }, [])

  return (
    <View style={styles.containerView}>
      { rootStore.soundMusic && <PlayBack song={"buzzer.ogg"} noRepeat={true} muteOnStart={true} ref={refSound} /> }
      <View style={styles.timerView}>
        <Timer textStyle={styles.timerText} playerGivenTime={180} change={(timer, timerReference) => setTimerRef({current: timerReference})} finishTimer={() => {
          setState({dialog: "GameOver", checked: state.checked.map(() => null)})
          if(state.oldImage) {
            NativeModules.TrackProjectMisc.loadWallpaperFromBase64(state.oldImage)
          }
        }}
        textType="clock"/>
      </View>
      <View style={styles.contentViewGaoTheForger}
        onTouchEndCapture={(event) => {
          const layout = state.layout
          const ratioWidth = Image.resolveAssetSource(imagesInformation[state.imageIndex].urlImageApp).width / layout.width
          const ratioHeight = Image.resolveAssetSource(imagesInformation[state.imageIndex].urlImageApp).height / layout.height
          const realX = event.nativeEvent.locationX * ratioWidth
          const realY = event.nativeEvent.locationY * ratioHeight
          let hit = false
          imagesInformation[state.imageIndex].points.forEach((item, index) => {
            if (item.xL <= realX && item.xR >= realX && item.yL <= realY && item.yR >= realY) {
              hit = true
              if (!state.checked[index]) {
                state.checked[index] = {x: event.nativeEvent.locationX, y: event.nativeEvent.locationY}
                setState({checked: state.checked})
              }
            }
          })
          if (!hit) {
            // @ts-ignore
            if(refSound&&refSound.current) {
              refSound.current.stop()
              refSound.current.start()
            }
            if(timerRef&&timerRef.current) {
              timerRef.current.addTime(-5)
            }
          } else {
            let win = true
            imagesInformation[state.imageIndex].points.forEach((item, index) => {
              if (!state.checked[index])
                win = false
            })
            if (win) {
              // @ts-ignore
              if(timerRef&&timerRef.current) {
                timerRef.current.stop()
              }
              if(state.oldImage) {
                NativeModules.TrackProjectMisc.loadWallpaperFromBase64(state.oldImage)
              }
              metaGame.reward(10)
              metaGame.playGame("TheForger", navigation, t)
              setState({dialog: "GameSuccess"})
            }
          }
        }}
      >
        <Image
          onLayout={(event) => {
            if (!state.layout || state.layout.width == 0) {
              setState({layout: event.nativeEvent.layout})
            }
          }}
          source={imagesInformation[state.imageIndex].urlImageApp} // Debug
          style={styles.pictureGameImageGaoTheForge}
        />
        {
          imagesInformation[state.imageIndex].points.map((item, index) => {
            return (<CheckedIcon key={item.xL} {...props} index={index}/>)
          })
        }
      </View>
    </View>
  )
}

// 5. GAME OVER
const ForgerGameOver = props => {
  const {setState} = props
  const {t} = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheForger", navigation, t)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog1={t("common:gameover", "TU AS PERDU !")}
        textButton={t("common:retry", "Réessaye")}
        textStyle1={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        buttonOnPress={() => setState({dialog: "GameIntro"})}
        dialogViewStyle={{marginHorizontal: "-25%"}}
      />
    </View>
  )
}

// 5. GAME OVER
const ForgerPermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-25%"}}
      />
    </View>
  )
}

// 6. GameAnswer
const ForgerGameAnswer = props => {
  const {state} = props
  return (
    <View style={styles.containerView}>
      <View style={styles.timerView}>
        <Text style={styles.timerText}>
          {"00:56"}
          {/* TODO: this.secondsToPrettyTimeString(this.state.seconds) */}
        </Text>
      </View>
      <View
        style={styles.pickedImage}>
        {state.imageSource && (
          // TODO
          <Image
            width={state.imageSource.width}
            height={state.imageSource.height}
            source={state.imageSource}
            style={styles.pictureGameImageGaoTheForge}
          />
        )}
      </View>
    </View>
  )
}

// 7.Game Success
const ForgerGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerView}>
      <View style={styles.timerView}>
        <Text style={styles.timerText}>
          {"00:56"}
        </Text>
      </View>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      <GaoDialog
        preset="score"
        navigation={props.navigation}
        uriImage={require("assets/images/common/wink_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-25%"}}
        textMoney={10}
      />
      <View>
        {/* {this.state.imageSource && ( */}
        <Image
          // width={this.state.imageSource.width}
          // height={this.state.imageSource.height}
          // source={this.state.imageSource}
          source={require("assets/images/gao-the-photographer/image1_temp.png")} // Debug
          style={styles.pictureGameImageGaoTheForge}
        />
        {/* )} */}
      </View>
    </View>
  )
}

/*
              *** What the PROGRAM do: ***
              - GameIntro: ask access to the player to his wallpaper -> Accept
              - GameRules: explain rules -> Start
              - GameLoading: the game will replace the Player's wallpaper with a wallpaper chosen randomly from the game's Altered Wallpapers database.
              - GameCountdown: countdown timer (same as in gao the athlete + "GO!"):
              - the game will display behind (blurred first) the wallpaper (from the Unaltered Wallpapers database) that matches the altered version that was set on the Player's phone.
              - GameStart : tap on the in-game wallpaper (the non-altered one) to Flag a difference : (NB: conceive a "leniency zone" around the difference (see specs) )
              If I'm correct, the game circles the difference.
              If I'm wrong, the game deducts time from my timer.
              - GameSuccess: check if The Player identifies the five differences -> WIN
              - GameOver: check if the timer runs out -> LOSE
              */

type ForgerDialog =
  "GameIntro" |
  "GameRules" |
  "GameLoading" |
  "GameCountDown" |
  "GameStart" |
  "GameSuccess" |
  "GameAnswer" |
  "GameOver" |
  "GamePermissionDenied"

type GameScreenState = {
  dialog: ForgerDialog,
  imageSource: any, // TODO
  imageIndex: number,
  checked: { x, y }[]
  layout?: { x, y, width, height }
  oldImage?: string
}

export interface GameScreenProps extends NavigationScreenProps
  <{}> {
}

@connectRootStore
export class GameScreen extends React.Component<GameScreenProps, GameScreenState> {
  constructor(props) {
    super(props)
    const index = Math.round(Math.random() * 1000) % imagesInformation.length
    this.state = {
      dialog: "GameIntro",
      imageSource: null,
      imageIndex: index,
      checked: imagesInformation[index].points.map(() => null)
    }
  }

  render() {
    this.props.rootStore.setSong("forger.ogg", this.props.rootStore.gameMusic)
    const ForgerStateMachine = StateMachine({initialState: this.state})
    const ForgerFrame = GaoFrame({
      wallpaper: require("assets/images/wallpaper/gao_forger.png"),
      coords: [122, 281, 628-122, 1182-281]
    })
    const ForgerScreen = ForgerFrame(props =>
      <ForgerStateMachine {...this.props} dialogs={{
        "GameIntro": ForgerGameIntro,
        "GameRules": ForgerGameRules,
        "GameLoading": ForgerGameLoading,
        "GameCountDown": ForgerGameCountDown,
        "GameStart": ForgerGameStart,
        "GameSuccess": ForgerGameSuccess,
        "GameAnswer": ForgerGameAnswer,
        "GameOver": ForgerGameOver,
        "GamePermissionDenied": ForgerPermissionDenied
      }} {...props}
      />
    )
    return (
      <>
        <GaoContainer {...this.props} character={require("assets/images/download_gifs/gao-Gao_the_forger.gif")}>
          <ForgerScreen navigation={this.props.navigation}/>
        </GaoContainer>
      </>
    )
  }
}
