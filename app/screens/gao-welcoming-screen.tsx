import * as React from "react"
import { observer } from "mobx-react-lite"
import { GaoDialog, GaoCharacter, Wallpaper, GaoTopbar } from "../components"
import { NavigationScreenProp } from "react-navigation"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { View, KeyboardAvoidingView } from "react-native"
import { useStores } from "models/root-store"
import { useEffect } from "react"
import i18n from "i18next";

export interface GaoWelcomingScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const GaoWelcomingScreen: React.FunctionComponent<GaoWelcomingScreenProps> = observer((props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const { metaGame } = useStores()

  if (!metaGame.firstStart)
    return null
  return (
    <View style={styles.wrapperColumnView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/HomeScreenAlpha.png")} disable={true} />
      <View style={styles.wrapperView}>{/* FIXME: In order to fill the screen space */}</View>
      <GaoDialog
        textDialog2={t(
          "common:welcome",
          "Bonjour, "+metaGame.userName+"! Bienvenue à Gao, une petite communauté qui a à cœur d'être le plus accueillante possible. J'espère que tu t'éclateras avec nous !^^"
        ).replace("XXX", metaGame.userName)}
        textStyle2={styles.infoText}
        textButton={t("common:ok", "Ok")}
        buttonOnPress={() => {
          navigation.replace("preHome")
        }}
      />
      <GaoCharacter
        positioning={{paddingLeft: 10}}
        uriImage={require("assets/images/character/blaze.png")}
        imageStyle={styles.avatarBlazeWelcomeImage}
        displayBottom= {true}
      />
    </View>
  )
})
