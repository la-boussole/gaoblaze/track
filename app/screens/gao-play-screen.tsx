import * as React from "react"
import {Text, View, FlatList} from "react-native"
import {NavigationScreenProp} from "react-navigation"
import {Button, GaoTopbar, Wallpaper, ImageButton} from "components"
import {useTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"
import * as LocalS from "local-storage"
import {useEffect} from "react"
import { useStores } from "models/root-store"
import { useObserver } from "mobx-react"

/* *** Constants to be fine tuned by game design *** */

const games = [
  {
    id: "gao_the_forger",
    color: "#8C4E6E",
    background: require("assets/images/playscreen/forger.png")
  },
  {
    id: "gao_the_intruder",
    color: "#525877",
    background: require("assets/images/playscreen/intruder.png")
  },
  {
    id: "gao_the_athlete",
    color: "#E6C2B2",
    background: require("assets/images/playscreen/athlete.png")
  },
  {
    id: "gao_the_filekeeper",
    color: "#BAB3B8",
    background: require("assets/images/playscreen/filekeeper.png")
  },
  {
    id: "gao_the_vagabond",
    color: "#806D8E",
    img: { /* FIXME: Use an image of "?" or a text */ },
    background: require("assets/images/playscreen/vagabond.png")
  },
  {
    id: "gao_the_spy",
    color: "#9C78A1",
    background: require("assets/images/playscreen/spy.png")
  },
  {
    id: "gao_the_sage",
    color: "#EAC684",
    background: require("assets/images/playscreen/sage.png")
  },
  {
    id: "gao_the_photographer",
    color: "#8CAACE",
    background: require("assets/images/playscreen/photographer.png")
  },
  {
    id: "gao_the_prosecutor",
    color: "#5F6E70",
    background: require("assets/images/playscreen/prosecutor.png")
  },
]

/* *** */

export interface PlayScreenProps extends NavigationScreenProp<{}> {
  onGoBack: () => void
  onOpenGame: () => void
}

// FIXME: change <Text /> by their real components when available.
// FIXME: show locked / disabled state for gao games for which the set of permissions is not available.
export const PlayScreen: React.FunctionComponent<PlayScreenProps> = props => {
  const {t} = useTranslation()
  const {navigation} = props
  const {onGoBack} = props
  const {onOpenGame} = props
  const {metaGame} = useStores()
  const rootStore = useStores()
  const [showVagabond, setShowVagabond] = React.useState(metaGame.showGaoTheVagabond)

  return useObserver(() => {
    if(showVagabond !== metaGame.showGaoTheVagabond) {
      setShowVagabond(metaGame.showGaoTheVagabond)
    }
    return (
      <View style={styles.mainView} onLayout={
        (layout) => {
          LocalS.set("width", layout.nativeEvent.layout.width)
          LocalS.set("height", layout.nativeEvent.layout.height)
        }
      }>
        <Wallpaper backgroundImage={require("assets/images/wallpaper/PlayScreenAlpha.png")} disable={rootStore.isFirstStart} />
        <GaoTopbar onGoBack={onGoBack} navigation={navigation} /* TODO: use GaoContainer */ />
        <View style={styles.containerBoxViewPlayMenu}>
          <Text style={styles.titleText}>
            {t("common:minigames", "Mini jeux")}
          </Text>
          <FlatList
            data={games}
            renderItem={({ item }) => {
              return (
                <>
                  { (item.id !== "gao_the_vagabond" || showVagabond) &&
                <ImageButton
                  // i18next-extract-mark-context-next-line ["gao_the_forger","gao_the_intruder","gao_the_athlete","gao_the_filekeeper","gao_the_vagabond","gao_the_spy","gao_the_sage","gao_the_photographer","gao_the_prosecutor",]
                  text={t(`aaaplayscreen:${item.id}`, item.id.charAt(0).toUpperCase() + item.id.split("_").join(" ").substr(1))}
                  uri={item.background}
                  style={[styles.playImage, rootStore.isFirstStart && item.id !== "gao_the_filekeeper"?{opacity: 0.3}:null]}
                  textStyle={styles.playText}
                  numberOfLines={2}
                  disabled={rootStore.isFirstStart && item.id !== "gao_the_filekeeper"}
                  onPress={() => {
                    if(onOpenGame) {
                      onOpenGame()
                    }
                    navigation.navigate(item.id)
                  }}
                />
                  }
                  { item.id === "gao_the_vagabond" && !metaGame.showGaoTheVagabond &&
                  <View style={styles.playImage}/>
                  }
                </>
              )}}
            renderScrollComponent={() => <View />}
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
            scrollEnabled={false}
          />
        </View>
        <View style={styles.spacerView} />
      </View>
    ) }
  )
}
