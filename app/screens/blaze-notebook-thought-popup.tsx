import * as React from "react"
import { Animated, Image, Linking, ScrollView, View } from "react-native"
import * as styles from "../blaze-theme/styles"
import { useTranslation } from "react-i18next"
import { BlazeProfileThought } from "assets/community_members/blaze-profiles"
import { NavigationScreenProp } from "react-navigation"
import HyperLink from "react-native-hyperlink"
import { Text, Button } from "components"
import { TouchableOpacity } from "react-native-gesture-handler"
import { useStores } from "models/root-store"

export interface BlazeThoughtPopupProps {
  thought: BlazeProfileThought
  navigation: NavigationScreenProp<{}>
  onOk?: () => void
}

/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */
export const BlazeNotebookThoughtPopup: React.FunctionComponent<BlazeThoughtPopupProps> = props => {
  const { t } = useTranslation()
  const navigation: NavigationScreenProp<{}> = props.navigation
  const thought: BlazeProfileThought = props.thought
  const onOk = props.onOk
  const [allHeight, setAllHeight] = React.useState(null)
  const [animatedHeight] = React.useState(new Animated.Value(0.0))
  const [activeItems, setActiveItems] = React.useState([])
  const { metaGame } = useStores()

  const interpolate = animatedHeight.interpolate({
    inputRange: [0, 1],
    outputRange: [0, allHeight],
  })

  const openAnim = Animated.timing(animatedHeight, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: false,
  })

  const closeAnim = Animated.timing(animatedHeight, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: false,
  })

  return (
    <>
      <View style={[styles.popupWrapper]}>
        <ScrollView style={[styles.wrapperView, { paddingVertical: 20 }]}>
          <View>
            <TouchableOpacity style={styles.notebookToughtPopupContainer} onPress={() => {
              if (!activeItems.includes(0)) {
                metaGame.addReadThought(thought.title)
                openAnim.start()
                setActiveItems([...activeItems, 0])
              } else {
                closeAnim.start(({ finished }) => {
                  if (finished)
                    setActiveItems([...activeItems.filter(item => item !== 0)])
                })
              }
            }}>
              <View style={styles.notebookToughtPopupTitle}>
                <Text style={styles.notebookToughtTitle}>{t("blaze:thoughts", "Reflexion")} {thought.title}</Text>
                <Text style={styles.notebookToughtSubTitle} numberOfLines={1}>{thought.tweet ? thought.tweet : thought.overview}</Text>
              </View>
              <Animated.Image source={require("assets/images/blaze-icons/arrow_notebook.png")} style={{ transform: [{ rotate: activeItems.includes(0) ? "180deg" : "0deg" }] }} />
            </TouchableOpacity>
            {activeItems.includes(0) && allHeight &&
              <Animated.View style={[styles.notebookToughtContainerText, { height: interpolate }]}>
                <HyperLink
                  onPress={(url: string, text: string) => Linking.openURL(url)}
                  linkDefault={false}
                  linkStyle={styles.boldFontText}
                  linkText={
                    url => {
                      const link = thought.links.find(link => link.link === url)
                      if (link)
                        return link.text
                      return url
                    }
                  }
                >
                  <Text style={styles.h4TextJustify}>
                    {thought.message}
                  </Text>
                </HyperLink>
              </Animated.View>
            }
            {
              !allHeight &&
              <View style={[styles.notebookToughtContainerText, { flex: 0, opacity: 0 }]} onLayout={(event) => setAllHeight(event.nativeEvent.layout.height)}>
                <HyperLink
                  onPress={(url: string, text: string) => Linking.openURL(url)}
                  linkDefault={false}
                  linkStyle={styles.boldFontText}
                  linkText={
                    url => {
                      const link = thought.links.find(link => link.link === url)
                      if (link)
                        return link.text
                      return url
                    }
                  }
                >
                  <Text style={styles.h4TextJustify}>
                    {thought.message}
                  </Text>
                </HyperLink>
              </View>
            }
          </View>
          <Button theme="blaze" preset="default" text={t("blaze:ok", "Ok")}
            onPress={() => {
              if (!onOk) {
                navigation.navigate("database")
              }
              else onOk()
            }} />
        </ScrollView>
      </View>
    </>
  )
}
