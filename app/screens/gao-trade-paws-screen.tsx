import * as React from "react"
import {View, Text} from "react-native"
import {NavigationScreenProps, FlatList} from "react-navigation"
import {Button, Wallpaper, GaoTopbar, ImageButton} from "components"
import {useTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"

/* *** Constants to be fine tuned by game design *** */
const boxes = [
  {
    //FIXME: use i18n
    // id: t("translation:memes","Memes"),
    id: "Memes",
    img: require("assets/images/tradepaws/box4.png"),
    screen: "memes",
  },
  {
    // id: t("translation:wallpapers","Wallpapers"),
    id: "Wallpapers",
    img: require("assets/images/tradepaws/box1.png"),
    screen: "wallpapers",
  },
  {
    // id: t("translation:ringtones","Ringtones"),
    id: "Emojis",
    img: require("assets/images/tradepaws/box3.png"),
    screen: "emojis",
  },
  {
    // id: t("translation:gifs","Gifs"),
    id: "Gifs",
    img: require("assets/images/tradepaws/box2.png"),
    screen: "gifs",
  }
]

/* *** */

export interface TradePawsScreenProps extends NavigationScreenProps<{}> {
  onOptionStart: (item: number) => void;
  onGoBack: () => void;
}

export const TradePawsScreen: React.FunctionComponent<TradePawsScreenProps> = (props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const {onOptionStart} = props
  const {onGoBack} = props

  return (
    <View style={styles.mainView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/rio_grande.png")} />
      <GaoTopbar onGoBack={onGoBack} navigation={navigation} /* TODO: use GaoContainer */ />
      <Text style={styles.titleTextTradePaws}>
        {t("common:trade-paws", "Convertir les papattes")}
      </Text>
      <View style={styles.spacerView} />
      <View>
        <FlatList
          data={boxes}
          renderItem={({ item , index}) => (
            <View style={styles.customFlatListView}>
              <ImageButton
                uri={item.img}
                style={{ width: 150, height: 147 }} // FIXME
                text={item.id}
                textStyle={styles.imageButtonText}
                onPress={() => onOptionStart(index)}
              />
            </View>
          )}
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  )
}
