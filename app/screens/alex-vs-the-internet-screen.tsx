import * as React from "react"
import {ImageButton, Text, Wallpaper} from "../components"
import {Animated, Dimensions, Image, TouchableOpacity, View} from "react-native"
import {createRef, useEffect, useRef, useState} from "react"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next"

const IMAGES = [
  {
    image: require("assets/images/gao-alex-gifs/Alex_1.png"),
    glowImage: require("assets/images/gao-alex-gifs/Alex_1_glow.png")
  },
  {
    image: require("assets/images/download_gifs/Alex_Animation_6fr_testlossy300.gif"),
    waitTime: 9360,
  },
  {
    image: require("assets/images/download_gifs/Alex_Animation2_8fr.gif"),
    waitTime: 2000,
  },
  {
    image: require("assets/images/gao-alex-gifs/Alex_Animation_End.png"),
    glowImage: require("assets/images/gao-alex-gifs/Alex_Animation_End_Glow.png"),
  }
]

export const AlexVsTheInternetEvent = props => {
  const {navigation} = props
  const [count, setCount] = React.useState(0)
  const rootStore = useStores()
  const {metaGame} = useStores()
  const {t} = useTranslation()

  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(0.0))
  const [fadeAnimAll, setFadeAnimAll] = React.useState(new Animated.Value(0.0))
  const [loadImages, setLoadImages] = React.useState(IMAGES.filter(item => item.image).length  - IMAGES.filter(item => item.image.uri).length)


  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("alexvstheinternet", null, t)
    fadeIn.start()
  }, [])

  const fadeIn = Animated.timing(fadeAnimAll, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnimAll, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const finish = () => {
    //fadeOut.start()
    rootStore.setLiveStreamPlayed()
    rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
    navigation.navigate("alexSpecialLiveEvent")
  }

  const startAnimation = () => {
    if(IMAGES[count].waitTime) {
      setTimeout(() => {
        if(count === IMAGES.length - 1) {
          finish()
        } else {
          setCount(prevCount => prevCount + 1)
        }
      }, IMAGES[count].waitTime)
    }
  }

  const startAnimationGlow = (anim, otherAnim) => {
    anim.start(({finished}) => {
      if (finished) {
        startAnimationGlow(otherAnim, anim)
      }
    })
    return true
  }

  console.log(loadImages)

  return (
    <View style={{flex: 1, backgroundColor: "white"}}>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {IMAGES.filter(item => item.image).map((item, index) =>
            <FastImage key={"load-image-"+index} style={{opacity: 0, position: "absolute"}} source={item.image} onLoadEnd={() => setLoadImages(loadImages - 1)} />)}
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        /*loadImages <= 0 &&*/
        <Animated.View style={{flex: 1, opacity: fadeAnimAll}}>
          <View onTouchEndCapture={() => {
            if (IMAGES[count].glowImage) {
              asc.stop()
              desc.stop()
              if(count === IMAGES.length - 1) {
                finish()
              } else {
                setCount(prevCount => prevCount + 1)
              }
            }
          }} style={{flex: 1, backgroundColor: "white"}}>
            {
              IMAGES[count].glowImage && startAnimationGlow(asc, desc) &&
        (
          <Animated.View style={{zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%"}}>
            <Wallpaper
              backgroundImage={IMAGES[count].glowImage}/>
          </Animated.View>
        )
            }
            {startAnimation()}
            <Wallpaper backgroundImage={IMAGES[count].image}/>
          </View>
        </Animated.View>
      }
    </View>
  )
}
