import * as React from "react"
import { observer } from "mobx-react-lite"
import { GaoDialog, GaoCharacter, Wallpaper, GaoTopbar } from "../components"
import { NavigationScreenProp, NavigationActions } from "react-navigation"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import {View, KeyboardAvoidingView, BackHandler} from "react-native"
import { useStores } from "models/root-store"
import { useEffect } from "react"

export interface GaoWelcomingUsernameScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const GaoWelcomingUsernameScreen: React.FunctionComponent<GaoWelcomingUsernameScreenProps> = observer((props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const { metaGame } = useStores()

  // if (!metaGame.firstStart)
  //   return null
  return (
    <KeyboardAvoidingView style={{flex:1}}>
      <View style={styles.wrapperColumnView}>
        <Wallpaper disable={true} backgroundImage={require("assets/images/wallpaper/HomeScreenAlpha.png")} />
        <View style={styles.wrapperView}>{/* FIXME: In order to fill the screen space */}</View>
        <GaoDialog
          preset= "ask-name"
          textDialog2={t(
            "common:name",
            "Avant de continuer, peux-tu me dire ton nom ? ",
          )}
          textStyle2={styles.infoText}
          textButton={t("common:start", "Démarrer")}
          buttonOnPress= {() => {
            navigation.replace("gaoWelcoming")}
          }
        />
        <GaoCharacter
          positioning={{paddingLeft: 10}}
          uriImage={require("assets/images/character/blaze.png")}
          imageStyle={styles.avatarBlazeWelcomeImage}
          displayBottom= {true}
        />
      </View>
    </KeyboardAvoidingView>
  )
})

