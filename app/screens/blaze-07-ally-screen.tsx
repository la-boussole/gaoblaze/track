import * as React from "react"
import {ImageButton, Wallpaper} from "../components"
import {Animated, Dimensions, Image, Text, TouchableOpacity, View} from "react-native"
import {createRef, useEffect, useRef, useState} from "react"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next";

const SCENES_01_ALLY = [
  require("assets/images/download_gifs/Ally1.gif"),
  require("assets/images/blaze-character-scenes/07-ally/vertical/03.png"),
  require("assets/images/download_gifs/Ally2.gif"),
]

export const Blaze07AllyScreen = props => {
  const rootStore = useStores()
  const {navigation} = props
  const [count, setCount] = React.useState(0)
  const { metaGame } = useStores()
  const {t} = useTranslation()
  const [loadImages, setLoadImages] = React.useState(SCENES_01_ALLY.length  - SCENES_01_ALLY.filter(item => item.uri).length)

  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("ally", null, t)
    let timer
    if (count < 1) {
      timer = setTimeout(() => {
        setCount(prevCount => prevCount + 1)
        startAnimation(asc, desc)
      }, 2500)
    }
    return () => {
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      clearTimeout(timer)
    }
  }, [count])

  const animation2 = () => {
    const timer = setTimeout(() => {
      clearInterval(timer)
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
    }, 12000)
  }

  const startAnimation = (anim, otherAnim) => {
    if(count <= 0) {
      anim.start(() => startAnimation(otherAnim, anim))
    }
  }

  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(0.0))

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  return (
    <>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {SCENES_01_ALLY.map((item, index) => <Image key={"load-image-"+index} style={{opacity: 0, position: "absolute"}} source={item} onLoadEnd={() => setLoadImages(loadImages - 1)} />)}
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        /*loadImages <= 0 &&*/
    <TouchableOpacity onPress={() => {
      if (count == 1) {
        setCount(prevCount => prevCount + 1)
        animation2()
      }
    }} style={{flex: 1}}>
      {
        count == 1 &&
      (
        <Animated.View style={{zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%"}}>
          <Wallpaper
            backgroundImage={require("assets/images/blaze-character-scenes/07-ally/vertical/03-glow.png")}/>
        </Animated.View>
      )
      }
      <Wallpaper backgroundImage={SCENES_01_ALLY[count]}/>
    </TouchableOpacity>
      }
    </>
  )
}
