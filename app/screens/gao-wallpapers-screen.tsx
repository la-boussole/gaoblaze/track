import * as React from "react"
import { View, Image, Text, ImageSourcePropType } from "react-native"
import { NavigationScreenProps, FlatList } from "react-navigation"
import { GaoDialog, Button, Wallpaper, GaoTopbar, ImageButton } from "components"
import { useTranslation } from "react-i18next"
import { useStores } from "models/root-store"
import * as styles from "gao-theme/styles"
import RNFS from "react-native-fs"

/* *** Constants to be fine tuned by game design *** */

export interface ShopItem {
  id: string
  assetsPath: ImageSourcePropType
  price: number
}
let URIGIFT : any
let SCOREGIFT : any
const wallpapers = [ // FIXME: add i18n
  {
    id: "gao_athlete.png",
    assetsPath: require("assets/images/tradepaws/wallpapers/wall1.png"),
    price: 30
  },
  {
    id: "gao_filekeeper1.png",
    assetsPath: require("assets/images/tradepaws/wallpapers/wall2.png"),
    price: 30
  },
  {
    id: "gao_forger.png",
    assetsPath: require("assets/images/tradepaws/wallpapers/wall3.png"),
    price: 30
  },
  {
    id: "gao_photographer.png",
    assetsPath: require("assets/images/tradepaws/wallpapers/wall4.png"),
    price: 30

  },
]

/* *** */

export interface WallpapersScreenProps extends NavigationScreenProps<{}> {
  onGoBack: () => void
}

export const WallpapersScreen: React.FunctionComponent<WallpapersScreenProps> = (props) => {
  const { t } = useTranslation()
  const { metaGame } = useStores()
  const { navigation } = props
  const {onGoBack} = props
  const [showGiftDialog, setShowGiftDialog] = React.useState(false)
  const [showNoPawnsDialog, setShowNoPawnsDialog] = React.useState(false)

  return (
    <View style={styles.mainView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/MemesScreenAlpha.png")} />
      <GaoTopbar onGoBack={onGoBack} navigation={navigation} /* TODO: use GaoContainer */ />
      <View style={styles.tradePawsScreenflexRowView} >
        <Text
          style={styles.titleText}>
          {t("common:wallpapers", "Wallpapers")}
        </Text>
      </View>
      <View style={styles.tradePawsContainer} >
        <FlatList
          data={wallpapers}
          renderItem={({ item }) => (
            <View style={styles.flatListItemView}>
              <ImageButton
                blurRadious={metaGame.bought.includes(item.id)?0.99:null}
                disabled={metaGame.bought.includes(item.id)}
                uri={item.assetsPath}
                style={{ width: 70, height: 70 }} // FIXME
                onPress={() => {
                  URIGIFT=item.assetsPath
                  SCOREGIFT=item.price
                  if(metaGame.paws >= item.price) {
                    metaGame.buyItem(item)
                      .then(success => {
                        URIGIFT=item.assetsPath
                        SCOREGIFT=item.price
                        setShowGiftDialog(true)
                        setTimeout(() => {
                          setShowGiftDialog(false)
                        }, 4000)
                      }) // Do something with the success or not.
                      .catch(error => console.error("buy action", error)) // Do something with the failure.
                  } else {
                    setShowNoPawnsDialog(true)
                    setTimeout(() => setShowNoPawnsDialog(false), 2000)
                  }
                }}
              />
              <View style={styles.flatListScoreView}>
                <Image source={metaGame.bought.includes(item.id)?require("assets/images/common/right.png"):require("assets/images/common/pawn.png")} style={styles.pawnImage} />
                {!metaGame.bought.includes(item.id) && <Text style={styles.defaultSecondaryText}>{item.price}</Text>}
              </View>
            </View>
          )}
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
      <View style={styles.spacerView} />
      { showGiftDialog ? <GaoGiftDialog /> : null }
      { showNoPawnsDialog ? <GaoNoPawnsDialog /> : null }
    </View>
  )
}
const GaoGiftDialog = props => {
  const {t} = useTranslation()
  return (
    <GaoDialog
      preset="gift"
      textDialog1={t("common:gift", "A GIFT FOR YOU")}
      navigation={props.navigation}
      textMoney={SCOREGIFT}
      uriGiftImage={URIGIFT}
      uriImage={require("assets/images/avatar/gao_the_sage.png")} // FIXME: add image
        // FIXME: change rewards for game design
    />
  )
}

const GaoNoPawnsDialog = props => {
  const {t} = useTranslation()
  return (
    <GaoDialog
      textDialog2={t("common:no-paws", "Not enough Paws! Meow ^^")}
      textStyle2={{color: "#EB358A"}}
      uriImage={require("assets/images/common/sad_emoji.png")}
        // FIXME: change rewards for game design
    />
  )
}
