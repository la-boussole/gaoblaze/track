import * as React from "react"
import {ImageButton, Wallpaper} from "../components"
import {Animated, Dimensions, Image, Text, TouchableOpacity, View} from "react-native"
import {createRef, useEffect, useRef, useState} from "react"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next"
import FastImage from "react-native-fast-image"

const IMAGES = [
  {
    image: require("assets/images/blaze-character-scenes/02-masako/01.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/01_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/02.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/02_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/03.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/03_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/04.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/04_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/05.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/05_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/06.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/06_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/07.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/07_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/08.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/08_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/09.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/09_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/10.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/10_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/11.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/11_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/12.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/12_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/02-masako/13.png"),
    glowImage: require("assets/images/blaze-character-scenes/02-masako/13_glow.png"),
  },
]

export const Blaze02MasakoScreen = props => {
  const rootStore = useStores()
  const {navigation} = props
  const [count, setCount] = React.useState(0)
  const { metaGame } = useStores()

  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(0.0))
  const [fadeAnimAll, setFadeAnimAll] = React.useState(new Animated.Value(0.0))
  const [loadImages, setLoadImages] = React.useState(IMAGES.filter(item => item.image).length - IMAGES.filter(item => item.image.uri).length)
  const {t} = useTranslation()

  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("masako", null, t)
    fadeIn.start()
    return () => rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
  }, [])

  const fadeIn = Animated.timing(fadeAnimAll, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnimAll, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const finish = () => {
    fadeOut.start(() => {
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
    })
  }

  const startAnimation = () => {
    if(IMAGES[count].waitTime) {
      setTimeout(() => {
        if(count === IMAGES.length - 1) {
          finish()
        } else {
          setCount(prevCount => prevCount + 1)
        }
      }, IMAGES[count].waitTime)
    }
  }

  const startAnimationGlow = (anim, otherAnim) => {
    anim.start(({finished}) => {
      if (finished) {
        startAnimationGlow(otherAnim, anim)
      }
    })
    return true
  }

  return (
    <>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {IMAGES.filter(item => item.image).map((item, index) =>
            <FastImage key={"load-image-"+index} style={{opacity: 0, position: "absolute"}}
              source={item.image} onLoadEnd={() => {
                setLoadImages(loadImages - 1)
              }} />)}
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        true &&
      <Animated.View style={{flex: 1, opacity: fadeAnimAll}}>
        <View onTouchEndCapture={() => {
          if (IMAGES[count].glowImage) {
            asc.stop()
            desc.stop()
            if(count === IMAGES.length - 1) {
              finish()
            } else {
              setCount(prevCount => prevCount + 1)
            }
          }
        }} style={{flex: 1, backgroundColor: "white"}}>
          {
            IMAGES[count].glowImage && startAnimationGlow(asc, desc) &&
          (
            <Animated.View style={{zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%"}}>
              <Wallpaper
                backgroundImage={IMAGES[count].glowImage}/>
            </Animated.View>
          )
          }
          {startAnimation()}
          <Wallpaper backgroundImage={IMAGES[count].image}/>
        </View>
      </Animated.View>
      }
    </>
  )
}
