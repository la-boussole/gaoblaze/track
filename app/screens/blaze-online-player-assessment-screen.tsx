import * as React from "react"
import {Image, ScrollView, View} from "react-native"
import {BlazeContainer, BlazeThoughtPopup, Button, Text} from "../components"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"
import {useState} from "react"
import { useStores } from "models/root-store"

export interface BlazeOnlinePlayerAssessmentScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeOnlinePlayerAssessmentScreen: React.FunctionComponent<BlazeOnlinePlayerAssessmentScreenProps> = BlazeContainer((props) => {
  const {t} = useTranslation()
  const { navigation } = props
  const characterInfo = navigation.getParam("character")
  const [showThought, setShowThought] = useState(false)
  const {metaGame} = useStores()

  const division = characterInfo.recommendations.split("@")

  console.log(characterInfo)

  return (
    <>
      {!showThought &&
      (<>
        <Text style={styles.h2Text}>
          <Text style={{...styles.h2Text, ...styles.boldFontText}}>{t("blaze:online", "Online")} </Text>
          {t("blaze:player-assessment", "player assessment")}
        </Text>
        <ScrollView style={styles.wrapperView}>
          <View style={[styles.centeredView, {height: 150, marginTop: 20}]}>
            {characterInfo.imageUri &&
          <Image style={styles.avatarImage} source={{uri: characterInfo.imageUri}} />

            }
            {(!characterInfo.imageUri || characterInfo.imageUri.length === 0) &&
          <Image style={styles.avatarImage}
            source={characterInfo.image ? characterInfo.image : require("assets/images/community/avatars/B-Sol.png")}/>
            }
          </View>
          <Text style={{...styles.h2Text, ...styles.boldFontText}}>{characterInfo.key}</Text>
          <Text style={{...styles.h2PurpleText, ...styles.boldFontText}}>
            {t("blaze:overview", "Aperçu")}
          </Text>
          <Text style={styles.pText}>
            {characterInfo.overview ? characterInfo.overview : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}
          </Text>
          <Text style={{...styles.h2PurpleText, ...styles.boldFontText}}>
            {t("blaze:recommendations", "Recommendations")}
          </Text>
          <Text style={styles.pText}>
            {characterInfo.recommendations ? division.map((text, index) => index % 2 === 0?text:<Text style={[styles.pText, {fontWeight: "bold"}]}>{text}</Text>) : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}
          </Text>
          <Button theme="blaze" preset="default" text={t("common:ok", "Ok")}
            onPress={() => {
              if (characterInfo.thought&&!metaGame.showThoughts.includes(characterInfo.thought.title)) {
                metaGame.addShowTouhght(characterInfo.thought.title, false, t)
                setShowThought(true)
              } else {
                navigation.navigate("database")
              }
            }}/>
        </ScrollView>
      </>)
      }
      {
        showThought && (
          <BlazeThoughtPopup navigation={navigation} characterInfo={characterInfo} />
        )
      }
    </>
  )
}, {isDebugMode: false})
