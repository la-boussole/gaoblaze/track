import React, { useEffect } from "react"
import { Text } from "components"
import { BackHandler, Image, TouchableOpacity, View } from "react-native"
import { PlayBack } from "components/sound/play-back"
import * as styles from "gao-theme/styles"
import { NavigationScreenProp } from "react-navigation"
import { NavigationActions } from "react-navigation"
import { useTranslation } from "react-i18next"
import { useStores } from "models/root-store"
import { color } from "blaze-theme"


export const BlazeLastMessagePopup: React.FunctionComponent<NavigationScreenProp<{}>> = (props) => {

  const {navigation} = props
  const rootStore = useStores()

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    return true
  }

  const {t} = useTranslation()

  return (
    <View style={styles.defaultContainerView}>
      <View style={styles.mainDialogScoreView}>
        <View style={{backgroundColor: color.palette.black, width: "80%", height: 200, justifyContent: "center", borderRadius: 10, borderColor: color.primaryDarker, borderWidth: 3, alignItems: "center",}}>
          <Text style={[styles.defaultSecondaryTextLastMessage, {color: color.primary}]}>{t("common:last-message1","Alors, que fait-on?")}</Text>
          <TouchableOpacity style={{backgroundColor: color.primaryDarker, width: "80%", borderRadius: 5, margin: 5}} onPress={() => navigation.navigate("lastMessage")}>
            <Text style={[styles.defaultSecondaryText, {color: color.palette.black}]}>{t("common:last-message2","Je sais quoi faire")}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: color.primaryDarker, width: "80%", borderRadius: 5, margin: 5}} onPress={() => navigation.navigate("database")}>
            <Text style={[styles.defaultSecondaryText, {color: color.palette.black}]}>{t("common:last-message3","J’ai besoin de temps")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
