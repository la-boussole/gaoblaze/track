import * as React from "react"
import {BackHandler, Image, ScrollView, View} from "react-native"
import { BlazeContainer, Button, ImageButton, Text } from "../components"
import { NavigationScreenProp } from "react-navigation"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"
import { observer } from "mobx-react-lite"
import { useStores } from "models/root-store"
import { useEffect } from "react"

export interface BlazeProfileScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeProfileScreen: React.FunctionComponent<BlazeProfileScreenProps> = BlazeContainer((props) => {
  const {t} = useTranslation()
  const { navigation } = props
  const { metaGame } = useStores()
  const character = navigation.getParam("character")
  const showAnimation = navigation.getParam("showAnimation", false)
  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  })

  const handleBackButtonClick = () => {
    navigation.navigate("database")
    return true
  }

  return (
    <>
      <View style={styles.topBarView}>
        <Text style={styles.h2Text}>
          <Text style={{...styles.h2Text, ...styles.boldFontText}}>{t("common:player", "Player")} </Text>
          {t("blaze:profile", "profile")}
        </Text>
        <View style={styles.spacerView} />
        <ImageButton
          style={styles.iconImage}
          uri={require("assets/images/blaze-icons/left-arrow.png")}
          onPress={() => navigation.goBack()}
        />
      </View>
      <ScrollView style={styles.wrapperView}>
        <View style={[styles.centeredView, {height: 150, marginTop: 20}]}>
          {characterInfo.imageUri &&
          <Image style={styles.avatarImage} source={{uri: characterInfo.imageUri}} />

          }
          {(!characterInfo.imageUri || characterInfo.imageUri.length === 0) &&
          <Image style={styles.avatarImage}
            source={characterInfo.image ? characterInfo.image : require("assets/images/community/avatars/B-Sol.png")}/>
          }
        </View>
        <Text style={{...styles.h2Text, ...styles.boldFontText}}>{characterInfo.key}</Text>
        <Text style={{...styles.h2PurpleText, ...styles.boldFontText}}>
          {t("common:about", "About")}
        </Text>
        <Text style={styles.pText}>
          {characterInfo.about?characterInfo.about:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}
        </Text>
        <Text style={{...styles.h2PurpleText, ...styles.boldFontText}}>
          {t("blaze:information-shared", "Information shared")}
        </Text>
        <Text style={styles.pText}>{characterInfo.informationShared?characterInfo.informationShared:"---"}</Text>
        <Button theme="blaze" preset="default" text={t("blaze:online-player-assessment", "Évaluation des joueurs en ligne")}
          onPress={() => {
            if (showAnimation /*&& !metaGame.animationCharacters.includes(characterInfo.key)*/)
              navigation.navigate(`${character.toLowerCase()}Animation`, {characterInfo})
            else {
              metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
            }
          }}/>
      </ScrollView>
    </>
  )
}, {isDebugMode: false})
