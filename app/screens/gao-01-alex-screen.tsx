import * as React from "react"
import {Wallpaper} from "../components"
import {TouchableOpacity} from "react-native"

const SCENES_01_ALEX = [
  /*require("assets/images/gao-alex-gifs/Alex_Animation.gif"),
  require("assets/images/gao-alex-gifs/Alex_Animation2.gif"),*/
]

// FIXME: whatch out to freeze onPress while playing first Gif...(check first)
export const Gao01AlexScreen = props => {
  const { navigation } = props
  const [count, setCount] = React.useState(0)
  const onPress = () =>{
    setCount(prevCount => prevCount + 1)
    if (count + 1 >= SCENES_01_ALEX.length)
      navigation.navigate("login")
  }
  return (
    <TouchableOpacity onPress={onPress} style={{flex: 1}}>
      <Wallpaper backgroundImage={SCENES_01_ALEX[count]} />
    </TouchableOpacity>
  )
}
