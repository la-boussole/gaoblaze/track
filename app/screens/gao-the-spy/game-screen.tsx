import * as React from "react"
import {View, Text, NativeModules, AppState} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {GaoDialog, StateMachine, GaoContainer, GaoFrame, Timer} from "components"
import {useTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"
import {TouchableOpacity} from "react-native-gesture-handler"
import BackgroundTimer from "react-native-background-timer"
import * as RNLocalize from "react-native-localize"
import {useStores} from "models/root-store"
import {useEffect} from "react"
import { PlayBack } from "components/sound/play-back"
import * as LocalS from "local-storage"
import { color } from "gao-theme"

/* *** Constants to be fine tuned by game design *** */

const PLAYER_GIVEN_TIME = 60

/* *** */

// FIXME: REWARD 10 PER MINI-GAME (20 IN TOTAL?)

// 1.Start the game
const SpyGameStart = props => {
  const {setState} = props
  const {t} = useTranslation()
  return (
    <View style={styles.containerFilterViewGaoTheSpy}>
      <GaoDialog
        textDialog2={t(
          "spy:start",
          "Beat Both challenges to earn PAWS!"
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => setState({dialog: "GameIntro"})}
      />
    </View>
  )
}

// 2.Accept to share your phone SMS
const SpyGameIntro = props => {
  const {setState} = props
  const {t} = useTranslation()
  return (
    <View style={styles.containerFilterViewGaoTheSpy}>
      <GaoDialog
        textDialog2={t(
          "spy:welcome",
          "Aide Gao à infiltrer la Tanière du Loup ! Surmonte les 2 défis présentés pour gagner des papattes  ",
        )}
        textButton={t("common:accept", "Accepter")}
        dialogViewStyle={{marginHorizontal: "-12%"}}
        buttonOnPress={() => {
          setState({dialog: "GameRules1"})
        }}
      />
    </View>
  )
}

// 3.GameRules1 // TODO: add images and filter on background
const SpyGameRules1 = props => {
  const {t} = useTranslation()
  return (
    <>
      <View style={styles.containerViewGaoTheSpyRules}/>
      <View style={styles.containerFilterViewGaoTheSpy}>
        <GaoDialog
          textDialog2={t("spy:rules1", "Premier défi : Désactiver le pare-feu ! Un certain nombre de fausses alarmes ont été installées sur ton téléphone. Refuse-les avant que le temps ne soit écoulé.")}
          textButton={t("common:start", "Démarrer")}
          dialogViewStyle={{marginHorizontal: "-12%"}}
          buttonOnPress={() => launchAlarm(props)}
        />
      </View>
    </>
  )
}

// 4.GameChallenge1
const SpyGameChallenge1 = props => {
  const {setState} = props
  const {t} = useTranslation()
  const {metaGame} = useStores()

  useEffect(() => {
  })

  return (
    <View style={styles.defaultContainerViewGaoThSpyLeft}>
      <TouchableOpacity>
        <View style={styles.timerLeftSpyView}>
          <Timer {...props} textStyle={styles.timerSpyText} finishTimer={() => setState({dialog: "GameOver"})}
            playerGivenTime={PLAYER_GIVEN_TIME} change={(timer) => {
              NativeModules.TrackProjectMisc.getLastAlarmGaoTheSpy().then((hour) => {
                const dateResult = new Date(hour)
                if((date.getTime() - dateResult.getTime() < 60000 || hour == "<error>") && (dateResult.getHours() != date.getHours() || dateResult.getMinutes() != date.getMinutes() || dateResult.getDay() != date.getDay())) {
                  setState({dialog: "GameRules2"})
                  BackgroundTimer.clearInterval(timer)
                }
              })
            }}/>
          <Text style={styles.infoAlarmsText}>
            {t("spy:alarms", "3 alarmes installées, désamorcez-les ! ")}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

// 5.GameRules2
const SpyGameRules2 = props => {
  const {setState} = props
  const {t} = useTranslation()
  return (
    <>
      <View style={styles.containerViewGaoTheSpy}/>
      <View style={styles.containerFilterViewGaoTheSpy}>
        <GaoDialog
          textDialog2={t(
            "spy:rules2",
            "Deuxième défi : Synchroniser les montres ! Règle ton horloge sur le fuseau horaire de Gao avant que le temps ne soit écoulé."
          )}
          textButton={t("common:start", "Démarrer")}
          buttonOnPress={() => setState({dialog: "GameChallenge2"})}
          dialogViewStyle={{marginHorizontal: "-12%"}}
        />
      </View>
    </>
  )
}

// 6.GameChallenge2
const SpyGameChallenge2 = props => {
  const {setState} = props
  const {t} = useTranslation()
  const { metaGame } = useStores()
  const {navigation} = props

  return (
    <View style={styles.defaultContainerViewGaoThSpyRight}>
      <TouchableOpacity>
        <View style={styles.timerRightSpyView}>
          <Timer {...props} textStyle={styles.timerSpyText} finishTimer={() => setState({dialog: "GameOver"})}
            playerGivenTime={120} change={(timer) => {
              console.log(RNLocalize.getTimeZone())
              if(RNLocalize.getTimeZone() == "Asia/Tokyo") {
                BackgroundTimer.clearInterval(timer)
                metaGame.reward(20)
                metaGame.playGame("TheSpy", navigation, t)
                setState({dialog: "GameSuccess"})
              }
            }}/>
          <Text style={styles.infoAlarmsText}>
            {t("spy:time-zone", "Gao's time zone is,\n Tokyo")}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

// GameSuccess
const SpyGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerFilterViewGaoTheSpy}>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      {/* FIXME */}
      <GaoDialog
        preset="score"
        textMoney={20}
        navigation={props.navigation}
        uriImage={require("assets/images/common/wink_emoji.png")}
        // FIXME: change rewards for game design
        dialogViewStyle={{marginHorizontal: "-12%"}}
      />
    </View>
  )
}

// 10.GameOver
const SpyGameOver = props => {
  const {setState} = props
  const {t} = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheSpy", navigation, t)
  })

  return (
    <View style={styles.containerFilterViewGaoTheSpy}>
      {/* FIXME */}
      <GaoDialog
        textDialog1={t("common:gameover", "TU AS PERDU !")}
        textStyle1={styles.colorFontGameOverText}
        uriImage={require("assets/images/common/sad_emoji.png")}
        textButton={t("common:retry", "Réessaye")}
        buttonOnPress={() => setState({dialog: "GameIntro"})}
        dialogViewStyle={{marginHorizontal: "-12%"}}
      />
    </View>
  )
}

let date = new Date(Date.now() + 1000 * 120)

const launchAlarm = props => {
  const {setState} = props

  const NAMES = ["EXCALIBUR", "GUNGNIR", "DURENDAL", "MASAMUNE", "TIZONA", "ZULFIQAR", "KAUMODAKI", "ASI", "GAN", "JIANG", "MO", "YE", "CALADBOLG", "MJOLNIR"]

  // set the fire date for 3 min from now FIXME: define it in a GameDesign constant
  date = new Date(Date.now())
  date.setMinutes(date.getMinutes() + 2)
  NativeModules.TrackProjectMisc.addAlarm(date.getHours(), date.getMinutes(), "psscht")
  setTimeout(() => {
    NativeModules.TrackProjectMisc.addAlarm(date.getHours(), date.getMinutes(), "boom")
    setTimeout(() => {
      NativeModules.TrackProjectMisc.addAlarm(date.getHours(), date.getMinutes(), "badaboum")
      setState({dialog: "GameChallenge1"})
    }, 1000)
  }, 1000)
}

/*
 *** What the GAME do: ***
 - Intro: ask access to the player to share CLOCK
 - Start: a certain number of alarms are installed on the Player's phone and a timer is launched.
 - Spy Challenge #2 : the Player must change their phone's clock to the indicated timezone.
 - Spy Challenge #3 : the Player must launch their stopwatch and stop it at the indicated timestamp
 - Success: The Player beats all three Challenges. (Win)
 - Fail: The Player fails any of the Challenges. (Lose)
*/

type SpyDialog =
  "GameIntro" |
  "GameStart" |
  "GameRules1" |
  "GameChallenge1" |
  "GameRules2" |
  "GameChallenge2" |
  "GameRules3" |
  "GameChallenge3" |
  "GameSuccess" |
  "GameOver"

type GameScreenState = {
  dialog: SpyDialog
}

export interface GameScreenProps extends NavigationScreenProps<{}> {
}

export const GameScreen: React.FunctionComponent<GameScreenProps> = (props) => {
  const rootStore = useStores()
  const SpyStateMachine = StateMachine({initialState: {dialog: "GameIntro"} as GameScreenState})
  const SpyFrame = GaoFrame({
    wallpaper: require("assets/images/wallpaper/gao_spy.png"),
    coords: [34, 272, 710 - 34, 1121 - 272],
  })

  const SpyScreen = SpyFrame(props =>
    <SpyStateMachine dialogs={{
      "GameIntro": SpyGameIntro,
      "GameStart": SpyGameStart,
      "GameRules1": SpyGameRules1,
      "GameChallenge1": SpyGameChallenge1,
      "GameRules2": SpyGameRules2,
      "GameChallenge2": SpyGameChallenge2,
      "GameSuccess": SpyGameSuccess,
      "GameOver": SpyGameOver,
    }} {...props}
    />
  )

  rootStore.setSong("spy.ogg", rootStore.gameMusic)

  return (
    <>
      <GaoContainer {...props} character={require("assets/images/download_gifs/gao-Gao_the_spy.gif")}
        isCharacterCentered={true} sendToFront={true} positioning={{backgroundColor: color.transparent, top: 0, position: "absolute", zIndex: 1000, alignSelf: "center"}}>
        <SpyScreen navigation={props.navigation}/>
      </GaoContainer>
    </>
  )
}
