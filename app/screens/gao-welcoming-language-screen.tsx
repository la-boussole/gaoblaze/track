import * as React from "react"
import { observer } from "mobx-react-lite"
import { GaoDialog, GaoCharacter, Wallpaper, GaoTopbar } from "../components"
import { NavigationScreenProp, NavigationActions } from "react-navigation"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { View, KeyboardAvoidingView } from "react-native"
import {initCommunity, useStores} from "models/root-store"
import { useEffect } from "react"
import i18n from "i18next";

export interface GaoWelcomingLanguageScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const GaoWelcomingLanguageScreen: React.FunctionComponent<GaoWelcomingLanguageScreenProps> = observer((props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const { metaGame } = useStores()
  const rootStore = useStores()

  console.log(i18n.language)

  // if (!metaGame.firstStart)
  //   return null
  return (
    <KeyboardAvoidingView style={{flex:1}}>
      <View style={styles.wrapperColumnView}>
        <Wallpaper disable={true} backgroundImage={require("assets/images/wallpaper/HomeScreenAlpha.png")} />
        <View style={styles.wrapperView}>{/* FIXME: In order to fill the screen space */}</View>
        <GaoDialog
          preset= "language"
          textDialog2={t(
            "dialogs:select-language",
            "Choisis ta langue de jeu !",
          )}
          textStyle2={styles.infoText}
          textButton= {t(
            "common:start",
            "Démarrer",
          )}
          buttonOnPress= {() => {
            rootStore.setCommunity(i18n.language)
            navigation.replace("gaoWelcomingUsername")}
          }
        />
        <GaoCharacter
          positioning={{paddingLeft: 10}}
          uriImage={require("assets/images/character/blaze.png")}
          imageStyle={styles.avatarBlazeWelcomeImage}
          displayBottom= {true}
        />
      </View>
    </KeyboardAvoidingView>
  )
})

