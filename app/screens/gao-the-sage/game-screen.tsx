import * as React from "react"
import * as LocalS from "local-storage"
import {View, Text, TextInput, Alert, ImageBackground, Image, StyleSheet} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {GaoDialog, StateMachine, GaoContainer, GaoFrame, Button} from "components"
import {useTranslation} from "react-i18next"
import {useEffect, useState} from "react"
import * as styles from "gao-theme/styles"
import {PermissionsAndroid} from "react-native"
import {useStores} from "models/root-store"
import { PlayBack } from "components/sound/play-back"
import i18n from "i18next";

/* *** Constants to be fine tuned by game design *** */

// FIXME: REWARD AMOUNT 30 PER QUESTIONS BATCH.

const STRING_SIMILARITY = 0.8 // game design constant
const stringSimilarity = require("string-similarity")
const stringCmp = (a: string) => (b: string) => stringSimilarity.compareTwoStrings(a.toLocaleLowerCase(), b.toLocaleLowerCase()) >= STRING_SIMILARITY
const alwaysTrue = (_: string) => true // for question that accept all answers as good

const QUESTIONS: ({
  question: string,
  answer: (_: string) => boolean,
  good: string,
  bad?: string
})[] = [{
  question: "Combien de races de chat existent dans le monde, à 20 races près ?",
  answer: x => {
    const n = parseInt(x)
    return 60 <= n && n <= 100
  },
  good: "Bravo, il y en a à peu près 80 !",
  bad: "Eh non, il y en a à peu près 80 !",
}, {
  question: "Est-ce que tu aimes les chats?",
  answer: alwaysTrue,
  good: "Miaou !Je m\'en doutais.",
}, {
  question: "Quel est le nom de la déesse chat dans l\'Égypte antique?",
  answer: stringCmp("Bastet"),
  good: "Bravo !",
  bad: "C\'est Bastet !",
}, {
  question: "À part avec moi, est-ce que tu vis avec un chat?",
  answer: alwaysTrue,
  good: "Miaou, ok !",
}, {
  question: "J\'ai combien d\'oreilles?",
  answer: x => parseInt(x) == 2,
  good: "C\'était pour être sûr que tu suivais",
  bad: "Tu m\'as pris pour un insecte? Miaou !",
}, {
  question: "Comment écris-tu miaou en espagnol?",
  answer: stringCmp("miau"),
  good: "Bravo !",
  bad: "Se dice miau !",
}, {
  question: "Une langue porte mon nom, sur quel continent la parle-t-on?",
  answer: stringCmp("Océanie"),
  good: "Bravo ! Mais t\'aurais pas triché avec Wikipedia par hasard, hm?",
  bad: "C\'est en Océanie, en Papouasie Nouvelle-Guinée !",
}, {
  question: "Moi je parle le chat, le gato et le cat. Et toi, combien de langues parles-tu?",
  answer: alwaysTrue,
  good: "D\'accord !",
}, {
  question: "Il existe une ville qui porte mon nom. Dans quel pays se trouve-t-elle?",
  answer: stringCmp("Mali"),
  good: "Bravo ! Bon, là c\'est sûr que t\'as regardé sur internet. >:)",
  bad: "C\'est au Mali !",
}, {
  question: "Est-ce que tu as déjà voyagé dans un autre pays?",
  answer: alwaysTrue,
  good: "Ok !",
}, {
  question: "Sais tu quel est mon jeu préféré?",
  answer: alwaysTrue,
  good: "J\'aime tous les Gao Games ! C\'est mon côté mégalo, miaou !",
  bad: "",
}, {
  question: "Sais-tu quel est le 1er jeu que le fabricant Nintendo a produit?",
  answer: stringCmp("cartes") || stringCmp("hanafuda") || stringCmp("carte") || stringCmp("jeu de société"),
  good: "Bravo ! Toi t\'aimes jouer, non?",
  bad: "C\'était un jeu de cartes traditionnel japonais, le hanafuda !",
}, {
  question: "Est-ce que tu joues beaucoup aux jeux vidéo?",
  answer: alwaysTrue,
  good: "D\'accord !",
}, {
  question: "Sais-tu d\'où viennent les plus vieilles cartes à jouer qu\'on connaisse?",
  answer: stringCmp("Chine") || stringCmp("Asie"),
  good: "Oui ! Elles datent de la dynastie Tang (618-907)",
  bad: "Elles viennent de Chine, et datent de la dynastie Tang (618-907)",
}, {
  question: "Tu aimes bien jouer aux cartes?",
  answer: alwaysTrue,
  good: "Waou !",
}]

const QUESTIONS_ES: ({
  question: string,
  answer: (_: string) => boolean,
  good: string,
  bad?: string
})[] = [{
  question: "¿Cuántas razas de gato hay en el mundo aproximadamente?",
  answer: x => {
    const n = parseInt(x)
    return 60 <= n && n <= 100
  },
  good: "Mmmm… Te la valgo! Miau. Hay cerca de 80!",
  bad: "¡Estás lejos! Hay entre 70 y 80.",
}, {
  question: "¿Te gustan los gatos?",
  answer: alwaysTrue,
  good: "¡Miau! Me lo imaginaba.",
}, {
  question: "¿Cómo se llama la diosa egipcia con cabeza de gata?",
  answer: stringCmp("Bastet"),
  good: "¡Eso!",
  bad: "¡Es Bastet!",
}, {
  question: "Aparte de mí, ¿vives con un gato?",
  answer: alwaysTrue,
  good: "¡Miau, ok!",
}, {
  question: "¿Cuántas orejas tengo?",
  answer: x => (parseInt(x) == 2) || stringCmp("dos"),
  good: "Era por asegurarme de que me estabas poniendo cuidado.",
  bad: "¡Qué! Ni que fuera un insecto!",
}, {
  question: "¿Cómo se dice miau en francés?",
  answer: stringCmp("miau"),
  good: "¡Très bien !",
  bad: "C'est miaou !",
}, {
  question: "¿En qué continente hablan una lengua que «lleva mi nombre»? ",
  answer: stringCmp("Oceania"),
  good: "¡¿Cómo supiste?! (¿No miraste en Wikipedia, cierto? ^^)",
  bad: "¡Casi! En realidad es en Oceanía, en las Islas Salomón.",
}, {
  question: "Yo hablo gato, cat y chat. ¿Y tú? ¿Qué idiomas hablas?",
  answer: alwaysTrue,
  good: "¡Aquí voy Duolingo! Miau.",
}, {
  question: "¿En qué país hay una ciudad que «lleva mi nombre»?",
  answer: stringCmp("Mali"),
  good: "Correcto… (SEGURO que miraste en Wikipedia. Grrr.)",
  bad: "Fallaste. :( Gao, la ciudad, queda en Malí.",
}, {
  question: "¿Ya has viajado a otros países?",
  answer: alwaysTrue,
  good: "Ok !",
}, {
  question: "A que no adivinas mi mini-juego favorito ^^.",
  answer: alwaysTrue,
  good: "¡Ja! Caíste. Me gustan TODOS. (Soy un poquito vanidoso… Miau.)",
  bad: "",
}, {
  question: "¿Cuál fue el primer producto de Nintendo? ",
  answer: stringCmp("cartas") || stringCmp("hanafuda") || stringCmp("carte") || stringCmp("jeu de société"),
  good: "¡Así es, Nintendopedia!",
  bad: "Yo pensaba lo mismo! Pero no. Los primeros productos de Nintendo fueron barajas tradicionales japonesas «hanafuda»",
}, {
  question: "¿Juegas muchos videojuegos?",
  answer: alwaysTrue,
  good: "No me sorprende. Miau.",
}, {
  question: "¿Sabes en dónde encontraron los naipes más antiguos de la historia?",
  answer: stringCmp("China") || stringCmp("Asia"),
  good: "¡Sí! ¡Y datan de hace más de mil años, de la Dinastía Tang (618-907)!",
  bad: "No, pero entiendo por qué lo pensaste. En realidad los encontraron en China. (¡Y datan de la Dinastía Tang (618-907), hace más de un milenio!",
}, {
  question: "¿Te gustan mucho los naipes, no?",
  answer: alwaysTrue,
  good: "Gaau !",
}]

const QUESTIONS_EN: ({
  question: string,
  answer: (_: string) => boolean,
  good: string,
  bad?: string
})[] = [{
  question: "How many cat varieties exist in the world, give or take 20?",
  answer: x => {
    const n = parseInt(x)
    return 60 <= n && n <= 100
  },
  good: "Well done, there are about 80 of them!",
  bad: "Well, no, there are about 80 of them!",
}, {
  question: "Do you love cats?",
  answer: alwaysTrue,
  good: "Meow! I knew it.",
}, {
  question: "What’s the name of the egyptian cat goddess?",
  answer: stringCmp("Bastet"),
  good: "Great !",
  bad: "It is Bastet !",
}, {
  question: "Besides me, do you live with a cat?",
  answer: alwaysTrue,
  good: "Meow, ok !",
}, {
  question: "How many eyes do I have?",
  answer: x => (parseInt(x) == 2) || stringCmp("two"),
  good: "eow :) I needed to make sure you were paying attention.",
  bad: "Are you crazy! I’m not a bug!",
}, {
  question: "How do you write meow in french?",
  answer: stringCmp("miaou"),
  good: "Bravo !",
  bad: ":( :( :( It’s actually written “miaou",
}, {
  question: " In which continent do they speak a language named “after me”?",
  answer: stringCmp("Oceania"),
  good: "Wow! (You didn’t Wikipedia it, did you? ^^)",
  bad: "Close! It’s actually in Oceania, in the Solomon Islands",
}, {
  question: "I speak cat, gato and chat. What about you? What languages do you speak?",
  answer: alwaysTrue,
  good: "Gonna start Duolingo now. Meow.",
}, {
  question: "In which country would you find a city named “after me”?",
  answer: stringCmp("Mali"),
  good: "That’s right… (I’m SURE you looked for the answer in Wikipedia. Grrr.)",
  bad: "Close but no cigar. You would actually find it in Mali.",
}, {
  question: "Have you ever travelled abroad?",
  answer: alwaysTrue,
  good: "Ok !",
}, {
  question: "Bet you can’t guess my favorite mini-game ^^.",
  answer: alwaysTrue,
  good: " Ha! Got you! I like them ALL. (I’m vain like that… Meow.)",
  bad: "",
}, {
  question: "What was the very first thing that Nintendo produced?",
  answer: stringCmp("cards") || stringCmp("hanafuda") || stringCmp("carte") || stringCmp("jeu de société"),
  good: "That's right, my Nintendo fan! ^^",
  bad: "That's what I thought! But no. Nintendo first produced traditional japanese “hanafuda” playing cards.",
}, {
  question: "Do you play a lot of video games?",
  answer: alwaysTrue,
  good: "Noted! Miau.",
}, {
  question: "Do you know where the oldest known playing cards were found?",
  answer: stringCmp("China") || stringCmp("Asie"),
  good: "Yes! And they date back more than a millennium years, to the Tang Dynasty (618-907)!",
  bad: "Not a bad guess. They were actually found in China, and date back to the Tang Dynasty (618-907).",
}, {
  question: "You seem to really like playing cards, don’t you?",
  answer: alwaysTrue,
  good: "Waou !",
}]
/* *** */

// 1.Start the game
const SageGameIntro = props => {
  const {t} = useTranslation()
  const {setState} = props
  const {metaGame} = useStores()
  const askPermission = async () => {
    /*try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_SMS,
        {
          title: t("permissions:sms.title", "SMS"),
          message: t("permissions:sms.message", "This app would like to access your SMS."),
          buttonNeutral: t("permissions:ask-me-later", "Ask Me Later"),
          buttonNegative: t("permissions:cancel", "Cancel"),
          buttonPositive: t("permissions:ok", "Ok"),
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        metaGame.addPermission("READ_SMS")*/
    setState({dialog: "GameStart"})
    /* } else {
        setState({dialog: "GamePermissionDenied"})
      }
    } catch (err) {
      console.warn(err)
    }*/
  }
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "sage:welcome",
          "Gaophoclès a des questions pressantes. L'aideras-tu à les résoudre ?",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={askPermission}
        dialogViewStyle={{marginHorizontal: "-10%"}}
      />
    </View>
  )
}

// 2.Accept to share your phone SMS
const SageGameStart = props => {
  const {t} = useTranslation()
  const {setState} = props
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "sage:rules",
          "Réponds aux questions de Gao pour gagner des papattes. Mais attention ! Les mauvaises réponses te font en perdre."
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => setState({dialog: "GameQuestions"})}
        dialogViewStyle={{marginHorizontal: "-10%"}}
      />
    </View>
  )
}

const SageGameQuestions = props => {
  const {setState} = props
  const {t} = useTranslation()
  const [item, setItem] = useState((LocalS.get("itemAskGlobal")==null?0:LocalS.get("itemAskGlobal")))
  //Modify default value, get from Local Storage if exists
  const [score, setScore] = useState(0)
  const [streak, setStreak] = useState(0)
  const [input, setInput] = useState("")
  const [answer, setAnswer] = useState("")
  const {metaGame} = useStores()

  const questions = i18n.language === "es" ? QUESTIONS_ES : i18n.language === "en" ? QUESTIONS_EN : QUESTIONS

  useEffect(() => {

  })

  const goToNext = () => {
    if (item == questions.length - 1) {
      // Reset LocalStorage after 15 questions
      LocalS.set("itemAskGlobal",0)
      setState({
        questionsAnswered: score + 1,
        questionsStreak: streak + 1,
        dialog: "GameStats"
      })
    }
    else if(item >=10 && item <  questions.length - 1){
      LocalS.set("itemAskGlobal",0)
    }
    else if(item == 4){
      //Validate the first five
      setState({
        questionsAnswered: score + 1,
        questionsStreak: streak + 1,
        dialog: "GameStats"
      })
      LocalS.set("itemAskGlobal",5)
    }
    else if(item >=0 && item < 4 ){
      LocalS.set("itemAskGlobal",5)
    }
    else if(item == 9){
      //Validate the second five
      setState({
        questionsAnswered: score + 1,
        questionsStreak: streak + 1,
        dialog: "GameStats"
      })
      LocalS.set("itemAskGlobal",10)
    }
    else if(item >=5 && item < 9 ){
      LocalS.set("itemAskGlobal",10)
    }
    //Go to next  question
    setItem(item + 1)
  }

  return (
    <View style={styles.gameContainerView}>
      <View style={StyleSheet.absoluteFill}>
        {answer ?
          setTimeout(() => {
            setAnswer("")
            goToNext()
          }, 2000) && (
            <View style={styles.gaoBubbleContainer}>
              <ImageBackground source={require("assets/images/gao-the-sage/speech_bubble_1.png")}
                               style={styles.gaoBubbleBackground}>
                <Text style={styles.gaoBubbleAnswer}>{answer}</Text>
              </ImageBackground>
            </View>)
          :
          <View style={styles.gaoTheSageQuestionContainer}>
            <Text style={styles.chalk}>{questions[item].question}</Text>
            <TextInput onChangeText={setInput} style={styles.inputTextGaoTheSage}/>
            <Button onPress={() => {
              const success = questions[item].answer(input)
              if (success) {
                setScore(score + 1)
                setStreak(streak + 1)
              } else {
                setStreak(0)
              }
              setAnswer(success ? questions[item].good : questions[item].bad)
              // Switch to next question or back to playing...

            }} text={t("common:submit", "Envoyer")} style={styles.submitGaoTheSage}/>
          </View>
        }
      </View>
    </View>
  )
}

// 4.GameStats: statistics
const SageGameStats = props => {
  const {setState} = props
  const {t} = useTranslation()
  const {state, navigation, screenProps} = props
  const { metaGame } = useStores()

  useEffect(() => {
    setTimeout(() => {
      metaGame.reward(30)
      metaGame.playGame("TheSage", navigation, t)
      setState({
        dialog: "GameSuccess"
      })}, 4000)
  }, [])

  return (
    <View style={styles.gameContainerView}>
      {/* Main text dialog */}
      <View style={styles.blackboardSpaceView}>
        <View style={styles.flexRowView}>
          <View style={styles.flexRowViewText}>
            <Text style={styles.blackboardText}>{t("sage:questions", "QUESTIONS \n RÉSPOUNDES")}</Text>
            <Text style={styles.blackboardNumberText}>{state.questionsAnswered}</Text>
          </View>
          <View style={styles.flexRowViewText}>
            <Text style={styles.blackboardText}>{t("sage:streak", "\n STREAK")}</Text>
            <Text style={styles.blackboardNumberText}>{state.questionsStreak}</Text>
          </View>
        </View>
        <View style={styles.flexRowViewScore}>
          <Text style={styles.blackboardText}>{t("sage:pawns", "Papattes gagnées")}</Text>
          <Text style={styles.blackboardNumberText}>+30</Text>
        </View>
      </View>
    </View>
  )
}


const SageGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerView}>
      {/* FIXME */}
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      <GaoDialog
        preset="score"
        textMoney={30}
        navigation={props.navigation}
        uriImage={require("assets/images/common/wink_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-12%"}}
        // FIXME: change rewards for game design
      />
    </View>
  )
}

const SageGameOver = props => {
  const { t } = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheSage", navigation, t)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog1={t("common:gameover", "TU AS PERDU !")}
        textStyle1={styles.colorFontGameOverText}
        textButton={t("common:retry", "Réessaye")}
        uriImage={require("assets/images/common/sad_emoji.png")}
        buttonOnPress={props.startCountdown}
        dialogViewStyle={{marginHorizontal: "-12%"}}
      />
    </View>
  )
}

const SagePermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-10%"}}
      />
    </View>
  )
}

/*
 *** What the GAME do: ***
 - ask access to the player to share SMS
 - ask player some questions by SMS
 - check the answers and show statistics (good answers ans paws earned)
 */

type SageDialog =
  "GameIntro" |
  "GameStart" |
  "GameQuestions" |
  "GameStats" |
  "GameSuccess" |
  "GameOver" |
  "GamePermissionDenied"

type GameScreenState = {
  dialog: SageDialog
}

export interface GameScreenProps extends NavigationScreenProps<{}> {
}

export const GameScreen: React.FunctionComponent<GameScreenProps> = (props) => {
  const rootStore = useStores()
  const SageStateMachine = StateMachine({
    initialState: {
      dialog: "GameIntro",
    } as GameScreenState
  })
  const SageFrame = GaoFrame({
    wallpaper: require("assets/images/wallpaper/gao_sage.png"),
    coords: [47, 414, 660, 493],
  })
  const SageScreen = SageFrame(props =>
    <SageStateMachine dialogs={{
      "GameIntro": SageGameIntro,
      "GameStart": SageGameStart,
      "GameQuestions": SageGameQuestions,
      "GameStats": SageGameStats,
      "GameOver": SageGameOver,
      "GameSuccess": SageGameSuccess,
      "GamePermissionDenied": SagePermissionDenied
    }} {...props}
    />
  )
  rootStore.setSong("sage.ogg", rootStore.gameMusic)
  return (
    <>
      {/* Change the GaoContainer View, for add GIF with styles */}
      <GaoContainer  {...props} character={require("assets/images/download_gifs/gao-Gao_the_sage.gif")}>
        <SageScreen navigation={props.navigation}/>
      </GaoContainer>
    </>
  )
}
