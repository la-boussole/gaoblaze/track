const defaultMeta = {
  headerMode: "none"
}

function packageScreens(setup) {
  return {
    exitRoutes: setup.exitRoutes || [],
    screens: setup.screens,
    meta: {...defaultMeta, ...(setup.meta || {}) },
  }
}

export default packageScreens
