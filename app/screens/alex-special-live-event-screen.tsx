import * as React from "react"
import { observer } from "mobx-react-lite"
import { View } from "react-native"
import { Wallpaper, GaoTopbar, GaoDialog } from "../components"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { NavigationScreenProp } from "react-navigation"
import {useStores} from "models/root-store"
import {useEffect} from "react"

export interface AlexSpecialLiveEventScreenProps {
  navigation: NavigationScreenProp<{}>
}


export const AlexSpecialLiveEventScreen: React.FunctionComponent<AlexSpecialLiveEventScreenProps> = observer((props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const { navigationStore } = useStores()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setFixedChat(true)
      navigation.navigate("chat", {character: "Alex", hideClose: true})
    }, 3000)
  })

  return (
    //  FIXME: first time welcoming popup. NB: after closing return to home screen
    <View style={styles.wrapperColumnView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/special_event_background.png")} />
      <GaoTopbar navigation={navigation} /* TODO: use GaoContainer */></GaoTopbar>
      <View style={styles.containerView}>
        <GaoDialog
          preset= "live-announcement"
          textDialog1={t("common:alex-announcement", "Nous regrettons le désagrément, le streaming en direct de Gao sera temporairement indisponible.")}
          uriImage={require("assets/images/community/flag.png")}
          textButton="Ok"
          withTimer={false}
          buttonOnPress= {() => {
            navigationStore.navigateTo("preHome")
          }}
        />
      </View>
    </View>
  )
})
