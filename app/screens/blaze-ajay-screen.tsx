/* eslint-disable react-native/no-inline-styles */
import * as React from "react"
import { Wallpaper } from "../components"
import { Animated, Image, Text, View } from "react-native"
import { useEffect } from "react"
import { useStores } from "models/root-store"
import * as styles from "gao-theme/styles"
import {useTranslation} from "react-i18next";

const IMAGES = [
  {
    image: require("assets/images/download_gifs/Ajay_1_12f.gif"),
    waitTime: 18000,
    type: "button",
    messages: [
      {
        message: require("assets/images/blaze-character-scenes/ajay/message4.png"),
        top: 70,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message4.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message2.png"),
        top: 30,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message2.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message1.png"),
        top: 10,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message1.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message3.png"),
        top: 50,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message3.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message6.png"),
        top: 110,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message6.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message5.png"),
        top: 90,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message5.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message4.png"),
        top: 150,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message4.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message2.png"),
        top: 130,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message2.png")
      },
      /*{
        message: require("assets/images/blaze-character-scenes/ajay/message1.png"),
        top: 190,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message1.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message3.png"),
        top: 170,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message3.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message6.png"),
        top: 230,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message6.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message5.png"),
        top: 210,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message5.png")
      },*/
    ]
  },
  {
    image: require("assets/images/blaze-character-scenes/ajay/ajay_door.png"),
    glowImage: require("assets/images/blaze-character-scenes/ajay/ajay_door_glow.png"),
  },
  {
    image: require("assets/images/download_gifs/Ajay_2.gif"),
    waitTime: 12000,
    type: "button",
    messages: [
      {
        message: require("assets/images/blaze-character-scenes/ajay/message2.png"),
        top: 30,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/11_message2.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message1.png"),
        top: 10,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/11_message1.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message4.png"),
        top: 70,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/11_message4.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message3.png"),
        top: 50,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/11_message3.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message6.png"),
        top: 110,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message6.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message5.png"),
        top: 90,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message5.png")
      },
      {
        message: require("assets/images/blaze-character-scenes/ajay/message4.png"),
        top: 150,
        left: Math.round(Math.random()*200),
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/02_message4.png")
      },
    ],
  },
  {
    image: require("assets/images/blaze-character-scenes/ajay/12.png"),
    glowImage: require("assets/images/blaze-character-scenes/ajay/13.png"),
  },
  {
    image: require("assets/images/download_gifs/Ajay_3.gif"),
    waitTime: 7000,
    type: "button",
    messages: [
      {
        message: require("assets/images/blaze-character-scenes/ajay/message1.png"),
        top: 20,
        left: 20,
        backgroundSelect: require("assets/images/blaze-character-scenes/ajay/17_message1.png")
      },
    ],
  },
]

export const BlazeAjayScreen = props => {
  const rootStore = useStores()
  const { navigation } = props
  const [count, setCount] = React.useState(0)
  const { metaGame } = useStores()

  const [fadeAnim] = React.useState(new Animated.Value(0.0))
  const [fadeAnimAll] = React.useState(new Animated.Value(0.0))
  const [seconds, setSeconds] = React.useState(0)
  const [visible, setVisible] = React.useState(0)

  const [animTop, setAnimTop] = React.useState([])
  const [animSize, setAnimSize] = React.useState([])
  const [animFade, setAnimFade] = React.useState([])
  const {t} = useTranslation()
  const [sizeAnimation, setSizeAnimation] = React.useState([])
  const [topAnimation, setTopAnimation] = React.useState([])
  const [opacityAnimationIn, setOpacityAnimationIn] = React.useState([])
  const [opacityAnimationOut, setOpacityAnimationOut] = React.useState([])
  const [isClicked, setIsClicked] = React.useState([])
  const [loadImages, setLoadImages] = React.useState(IMAGES.filter(item => item.image).length - IMAGES.filter(item => item.image.uri).length)

  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("ajay", null, t)
    fadeIn.start()
    startIndex(0)
    return () => rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
  }, [])

  const restartAnimation = (indexCount) => {
    const newSizeAnimation = []
    const newTopAnimation = []
    const newAnimTop = []
    const newAnimSize = []
    const newAnimFade = []
    const newOpacityAnimationIn = []
    const newOpacityAnimationOut = []
    const newIsClicked = []
    IMAGES[indexCount].messages.forEach(item => {
      newIsClicked.push(false)
      const animateOpacity = new Animated.Value(0)
      newAnimFade.push(animateOpacity)
      newOpacityAnimationIn.push(
        Animated.timing(
          animateOpacity, {
            toValue: 1,
            duration: 800,
            useNativeDriver: true
          }
        )
      )
      newOpacityAnimationOut.push(
        Animated.timing(
          animateOpacity, {
            toValue: 0,
            duration: 800,
            useNativeDriver: true
          }
        )
      )
      const animateStateTop = new Animated.Value(item.top)
      newAnimTop.push(animateStateTop)
      newTopAnimation.push(
        Animated.timing(animateStateTop, {
          toValue: 250,
          duration: 1000,
          useNativeDriver: false
        })
      )
      const animateStateSize = new Animated.Value(50)
      newAnimSize.push(animateStateSize)
      newSizeAnimation.push(
        Animated.timing(animateStateSize, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: false
        })
      )
    })
    setAnimFade(newAnimFade)
    setOpacityAnimationIn(newOpacityAnimationIn)
    setOpacityAnimationOut(newOpacityAnimationOut)
    setAnimTop(newAnimTop)
    setAnimSize(newAnimSize)
    setSizeAnimation(newSizeAnimation)
    setTopAnimation(newTopAnimation)
    setIsClicked(newIsClicked)
  }

  const startIndex = (index) => {
    if (IMAGES[index].waitTime) {
      startAnimation(index)
    }
    if (IMAGES[index].type === "button") {
      startButtons(index)
    }
    if (IMAGES[index].glowImage) {
      startAnimationGlow(asc, desc)
    }
  }

  const fadeIn = Animated.timing(fadeAnimAll, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnimAll, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const finish = () => {
    fadeOut.start(() => {
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
    })
  }

  const startAnimation = (index) => {
    if (IMAGES[index].waitTime) {
      setTimeout(() => {
        if (index === IMAGES.length - 1) {
          finish()
        } else {
          setCount(prevCount => prevCount + 1)
          setVisible(0)
          setSeconds(0)
          startIndex(index + 1)
        }
      }, IMAGES[index].waitTime)
    }
  }

  const startAnimationGlow = (anim, otherAnim) => {
    anim.start(({ finished }) => {
      if (finished) {
        startAnimationGlow(otherAnim, anim)
      }
    })
    return true
  }

  let interval = null

  const startButtons = (index) => {
    if (!interval) {
      restartAnimation(index)
      interval = setInterval(() => {
        setSeconds(lastValue => lastValue + 1)
      }, 1000)
      setTimeout(() => {
        clearInterval(interval)
        interval = null
      }, IMAGES[index].waitTime)
    }
  }

  const startAnimationClick = (index) => {
    const newIsClicked = [...isClicked]
    newIsClicked[index] = true
    setIsClicked(newIsClicked)
    opacityAnimationIn[index].start((finishedIn) => {
      if(finishedIn.finished) {
        const newIsClicked = [...isClicked]
        newIsClicked[index] = false
        setIsClicked(newIsClicked)
        opacityAnimationOut[index].start()
        sizeAnimation[index].start()
        topAnimation[index].start()
      }
    })
  }

  return (
    <>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {IMAGES.filter(item => item.image).map((item, index) => {
            return <Image key={"load-image-" + index} style={{opacity: 0, position: "absolute"}} source={item.image}
              onLoadEnd={() => setLoadImages(loadImages - 1)}
            />
          })
          }
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        /*loadImages <= 0 &&*/
    <Animated.View style={{ flex: 1, opacity: fadeAnimAll }}>
      <View onTouchEndCapture={() => {
        if (IMAGES[count].glowImage) {
          asc.stop()
          desc.stop()
          if (count === IMAGES.length - 1) {
            finish()
          } else {
            startIndex(count + 1)
            setCount(prevCount => prevCount + 1)
          }
        }
      }} style={{ flex: 1, backgroundColor: "white" }}>
        {
          IMAGES[count].glowImage &&
          (
            <Animated.View style={{ zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%" }}>
              <Wallpaper
                backgroundImage={IMAGES[count].glowImage} />
            </Animated.View>
          )
        }
        <Wallpaper backgroundImage={IMAGES[count].image} />
        {IMAGES[count].type === "button" && (
          IMAGES[count].messages.map((item, index) => {
            return (
              <>
                {
                  isClicked[index] &&
                  <Animated.View style={[styles.ajayMessage, {opacity: animFade[index], top: item.top + 15, left: item.left + 40, zIndex: 100}]}>
                    <Image style={styles.ajayMessageImage} source={require("assets/images/blaze-character-scenes/ajay/ajay_message.png")} />
                  </Animated.View>
                }
                {
                  seconds > index && (visible & (1<<index)) === 0 &&
                  <Animated.View style={{
                    top: animTop[index],
                    left: item.left,
                    position: "absolute",
                    width: 160,
                    height: animSize[index],
                  }} onTouchEndCapture={() => {
                    startAnimationClick(index)
                  }}
                  >
                    <Image source={item.message} style={{resizeMode: "contain", flex: 1, width: "100%"}} onMagicTap={() => console.log("Tap")} />
                  </Animated.View>
                }
              </>
            )
          })
        )
        }
      </View>
    </Animated.View>
      }
    </>
  )
}
