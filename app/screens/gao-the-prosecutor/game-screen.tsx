import * as React from "react"
import {View, TextInput, KeyboardAvoidingView, Alert} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {Button, GaoDialog, Text, StateMachine, GaoContainer, GaoFrame, Timer} from "components"
import {useTranslation, withTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"
import {PermissionsAndroid} from "react-native"
import {useEffect, useState} from "react"
import {connectRootStore} from "utils/decorators"
import {useStores} from "models/root-store"
import { PlayBack } from "components/sound/play-back"
import Contacts, {Contact} from "react-native-contacts";

/* *** Constants to be fine tuned by game design *** */

const PLAYER_GIVEN_TIME = 180
const STRING_SIMILARITY = 0.8

/* *** */

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  // From https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]]
  }
  return a
}

// 1.Game rules
const ProsecutorGameIntro = props => {
  const {setState} = props
  const {t} = useTranslation()
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "prosecutor:welcome",
          "Gao te soupçonne d’avoir volé ses snacks préférés. Confirme ton alibi et prouve-lui son erreur !",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={() => setState({dialog: "GameRules"})}
        dialogViewStyle={{marginHorizontal: "-5%"}}
      />
    </View>
  )
}

// 2.Accept to change your phone CALL LOG settings
const ProsecutorGameRules = props => {
  const {setState} = props
  const {t} = useTranslation()
  const {metaGame} = useStores()

  /* List call logs matching the filter */
  const filter = {}

  useEffect(() => {
    (async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          {
            title: t("permissions:call-log.title", "Read contacts"),
            message: t("permissions:call-log.message", "This app would like to access your calllogs."),
            buttonNeutral: t("permissions:ask-me-later", "Ask Me Later"),
            buttonNegative: t("permissions:cancel", "Cancel"),
            buttonPositive: t("permissions:ok", "Ok"),
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          metaGame.addPermission("READ_CONTACTS")
        } else {
          setState({dialog: "GamePermissionDenied"})
        }
      } catch (err) {
        //console.warn(err)
      }
    })()
  }, [])

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "prosecutor:rules",
          "Complète la fiche d'information avant que le temps ne soit écoulé !",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => {
          Contacts.getAll((error, contacts) => {
            const contactWithNumbers = contacts.filter(item => item.phoneNumbers.length > 0)
            const contact: Contact = contactWithNumbers[Math.round(Math.random() * 10000)%contactWithNumbers.length]
            console.log(contact)
            props.callLogs([{name: contact["displayName"],
              phoneNumber: contact.phoneNumbers[0].number, dateTime: "Top secret"}])
          })
          /*CallLogs.load(-1, filter).then(c => {
            props.callLogs(shuffle(c).slice(0, 3))
          })*/
          //setState({dialog: "GameStart"})
        }}
        dialogViewStyle={{marginHorizontal: "-15%"}}
      />
    </View>
  )
}

// 3.GameStart
const ProsecutorGameStart = props => {
  const {state, setState} = props
  const {t} = useTranslation()
  const callInfos = state.callLogs[state.question] || {phoneNumber: "", dateTime: "", duration: ""}
  const [answer, setAnswer] = useState("")
  const {metaGame} = useStores()

  useEffect(() => {

  })

  return (
    <KeyboardAvoidingView
      behavior={"height"}
      style={styles.containerKeyboardAvoidingView}>
      <View style={styles.defaultContainerView}>
        <Timer {...props} textStyle={styles.timerProsecutorView} finishTimer={() => setState({dialog: "GameOver"})}
          playerGivenTime={PLAYER_GIVEN_TIME} />
        <View style={styles.envelopeViewGaoTheProsecutor}>
          {/*{state.wrongAnswer && <Text style={styles.wrongText}>{t("prosecutor:wrong information","wrong information")}</Text>}*/}
          <View style={styles.borderView}>
            <Text style={styles.formTitleText}>{t("prosecutor:evidence", "Preuves")}</Text>
            <View style={styles.inputRowContainerView}>
              <Text style={styles.formKeyText}>{t("prosecutor:person-called", "Nom du Contact")}</Text>
              <TextInput
                style={styles.inputText}
                onChangeText={setAnswer}
                value={answer}
              />
            </View>
            <View style={styles.rowContainerView}>
              <Text style={styles.formKeyText}>{t("prosecutor:phone-number", "N° de Téléphone")}</Text>
              <Text style={styles.formValueText}>{callInfos.phoneNumber}</Text>
            </View>
            <View style={styles.rowContainerView}>
              <Text style={styles.formKeyText}>{t("prosecutor:date", "Date")}</Text>
              <Text style={styles.formValueText}>{callInfos.dateTime}</Text>
              <Text style={styles.formKeyText}>{t("prosecutor:duration", "Durée")}</Text>
              <Text style={styles.formValueText}>***</Text>
            </View>
            <Button
              text={t("prosecutor:validate", "Valider")}
              theme="gao"
              disabled={false}
              preset="form_enabled"
              onPress={() => props.checkAnswer(answer)}
              style={{width: "100%"}}
            />
          </View>
        </View>
      </View>
    </KeyboardAvoidingView>
  )
}

// 4. GAME OVER
const ProsecutorGameOver = props => {
  const {t} = useTranslation()
  const {setState} = props
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheProsecutor", navigation, t)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog1={t("common:gameover", "TU AS PERDU !")}
        textButton={t("common:retry", "Réessaye")}
        textStyle1={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        buttonOnPress={() => setState({dialog: "GameStart"})}
        dialogViewStyle={{marginHorizontal: "-12%"}}
      />
    </View>
  )
}

const ProsecutorPermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-10%"}}
      />
    </View>
  )
}

// FIXME: REWARD 20.
// 5.Game Success
const ProsecutorGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerView}>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      <GaoDialog
        preset="score"
        navigation={props.navigation}
        textMoney={20}
        uriImage={require("assets/images/common/wink_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-12%"}}
      />
    </View>
  )
}

/*
 *** What the GAME do: ***
 - GameIntro: ask access to the player to his CALL LOG -> Accept
 - GameRules: explain rules -> Start
 - GameStart: the Player is presented with the metadata from three of their phone calls, one at a time.
      * This metadata, however, is always missing one random element from the following list (Person Called/Phone Number/Time/Date/Duration).
      * To earn Paws, the Player must input the missing element of the three phone calls before the timer runs out (through a Validate Button).
 - GameSuccess: check if The Player enters the three missing items correctly. -> WIN
 - GameOver: check if The timer runs out or the Player enters one wrong item -> LOSE
 */

type ProsecutorDialog =
  "GameIntro" |
  "GameRules" |
  "GameStart" |
  "GameSuccess" |
  "GameOver" |
  "GamePermissionDenied"

type CallLog = { // FIXME
  dateTime: string
  duration: number
  name: string | null,
  phoneNumber: string,
  rawType: number,
  timestamp: number,
  type: string
}

type GameScreenState = {
  dialog: ProsecutorDialog
  callLogs: CallLog[]
  question: number
}

export interface GameScreenProps extends NavigationScreenProps<{}> {
}

@withTranslation()
@connectRootStore
export class GameScreen extends React.Component<GameScreenProps, GameScreenState> {
  constructor(props) {
    super(props)
    this.state = {
      dialog: "GameIntro",
      callLogs: [],
      question: 0,
    }
    this.callLogs = this.callLogs.bind(this)
    this.checkAnswer = this.checkAnswer.bind(this)
  }

  callLogs(c) {
    this.setState({dialog: "GameStart", callLogs: c})
  }

  checkAnswer(answer) {
    const stringSimilarity = require("string-similarity")
    if (this.state.callLogs.length > this.state.question) {
      const answers = answer.split(" ")
      const responses = this.state.callLogs[this.state.question].name.split(" ")
      let similar = false
      answers.forEach(a => {
        responses.forEach((b) => {
          if (stringSimilarity.compareTwoStrings(a.toUpperCase(), b.toUpperCase()) >= STRING_SIMILARITY)
            similar = true
        })
      })
      if (similar) {
        this.props.rootStore.metaGame.reward(20)
        this.props.rootStore.metaGame.playGame("TheProsecutor", this.props.navigation, this.props.t)
        this.setState({dialog: "GameSuccess"})
      } else {
        this.setState({dialog: "GameOver"})
      }

    } else {
      this.setState({dialog: "GameOver"})
    }
  }

  render() {
    this.props.rootStore.setSong("prosecutor.ogg", this.props.rootStore.gameMusic)
    const ProsecutorStateMachine = StateMachine({initialState: this.state})
    const ProsecutorFrame = GaoFrame({
      wallpaper: require("assets/images/wallpaper/gao_prosecutor.png"),
      coords: [49, 214, 706 - 49, 1096 - 214],
    })
    const ProsecutorScreen = ProsecutorFrame(props =>
      <ProsecutorStateMachine dialogs={{
        "GameIntro": ProsecutorGameIntro,
        "GameRules": ProsecutorGameRules,
        "GameStart": ProsecutorGameStart,
        "GameSuccess": ProsecutorGameSuccess,
        "GameOver": ProsecutorGameOver,
        "GamePermissionDenied": ProsecutorPermissionDenied,
      }} {...props}
      callLogs={this.callLogs}
      checkAnswer={this.checkAnswer}
      />
    )
    return (
      <>
        <GaoContainer {...this.props} character={require("assets/images/download_gifs/gao-Gao_the_prosecutor.gif")}
          characterStyle={{width: 400}} isCharacterCentered={false}>
          <ProsecutorScreen navigation={this.props.navigation}/>
        </GaoContainer>
      </>
    )
  }
}
