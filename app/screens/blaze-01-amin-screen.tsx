import * as React from "react"
import {Wallpaper} from "../components"
import {TouchableOpacity} from "react-native";

const SCENES_01_AMIN = [
  require("assets/images/blaze-character-scenes/01-amin/01.png"),
  require("assets/images/blaze-character-scenes/01-amin/02.png"),
  require("assets/images/blaze-character-scenes/01-amin/03.png"),
  require("assets/images/blaze-character-scenes/01-amin/04.png"),
  require("assets/images/blaze-character-scenes/01-amin/05.png"),
  require("assets/images/blaze-character-scenes/01-amin/06.png"),
  require("assets/images/blaze-character-scenes/01-amin/07.png"),
  require("assets/images/blaze-character-scenes/01-amin/08.png"),
  require("assets/images/blaze-character-scenes/01-amin/09.png"),
  require("assets/images/blaze-character-scenes/01-amin/10.png"),
  require("assets/images/blaze-character-scenes/01-amin/11.png"),
  require("assets/images/blaze-character-scenes/01-amin/12.png"),
]

export const Blaze01AminScreen = props => {
  const { navigation } = props
  const [count, setCount] = React.useState(0)
  const onPress = () =>{
    setCount(prevCount => prevCount + 1)
    if (count + 1 >= SCENES_01_AMIN.length)
      navigation.navigate("profile", {
        character: "amin",
        showAnimation: false
      })
  }
  return (
    <TouchableOpacity onPress={onPress} style={{flex: 1}}>
      <Wallpaper backgroundImage={SCENES_01_AMIN[count]} />
    </TouchableOpacity>
  )
}
