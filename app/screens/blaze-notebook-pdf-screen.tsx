import * as React from "react"
import {BackHandler, View} from "react-native"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"
//import Pdf from "react-native-pdf"


export interface BlazeNotebookPDFScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeNotebookPDFScreen: React.FunctionComponent<BlazeNotebookPDFScreenProps> = ((props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const pdf = navigation.getParam("pdf", ":(")

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    navigation.goBack()
    return true
  }

  return (
    <>
      <View style={styles.pdfContainer}>
        {/*<Pdf style={styles.pdf} source={{uri: pdf}} />*/}
      </View>
    </>
  )
})
