import * as React from "react"
import { View, Image, PermissionsAndroid } from "react-native"
import ImgToBase64 from 'react-native-image-base64';
import { NavigationScreenProps } from "react-navigation"
import { connectRootStore } from "utils/decorators"
import {
  Button,
  GaoDialog,
  Text,
  GaoContainer,
  StateMachine,
  GaoFrame,
  Timer
} from "components"
import { useTranslation, withTranslation } from "react-i18next"
import { launchImageLibrary } from "react-native-image-picker"
import CameraRoll from "@react-native-community/cameraroll"
import * as styles from "gao-theme/styles"
import {useEffect, useRef} from "react"
import {useStores} from "models/root-store"
import { PlayBack } from "components/sound/play-back"

/* *** Constants to be fine tuned by game design *** */

const PLAYER_GIVEN_TIME = 180
const FIRST_OF_N_PICTURES = 20

/* *** */

// From https://stackoverflow.com/questions/40627018/how-to-focus-crop-image-in-react-native
const CroppedImage = props => {
  return (
    <View style={{
      // FIXME: put in stylesheet?
      overflow: "hidden",
      backgroundColor: "transparent",
      height: "100%",
      width: "100%",
      position: "absolute",
    }}>
      <Image style={{
        top: props.cropTop * -1,
        left: props.cropLeft * -1,
        width: props.width,
        height: props.height,
      }} source={props.source} />
    </View>
  )
}

/*
 *** What the PROGRAM do: ***
 - ask access to the player to read photos
 - take randomly a photo from user galery
 - crop the photo
 - show the photo to player
 - ask player to select a photo in the gallery
 - check if the photo selected by the user is the good one -> WIN
 - loose when time runs out
 */

type FileKeeperDialog =
  "GameIntro" |
  "GameStart" |
  "GameTimer" |
  "GameAnswer" | // FIXME: add function to go to next screen GameSuccess in 1 sec..
  "GameSuccess" |
  "GameOver" |
  "GameFirstPlay"

// 1.Game rules
const FileKeeperGameStart = props => {
  const { t } = useTranslation()
  const { setState } = props
  const {metaGame} = useStores()

  const askPermission = () => {
    PermissionsAndroid.requestMultiple(
      [PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]
    ).then((granted) => {
      if (granted["android.permission.READ_EXTERNAL_STORAGE"] === "granted" && granted["android.permission.WRITE_EXTERNAL_STORAGE"] === "granted") {
        metaGame.addPermission("READ_EXTERNAL_STORAGE")
        metaGame.addPermission("WRITE_EXTERNAL_STORAGE")
        getRandomImage()
      } else {
        setState({dialog: "GamePermissionDenied"})
      }
    })
  }

  const getRandomImage = () => {
    const fetchParams = {
      first: FIRST_OF_N_PICTURES,
    }

    CameraRoll.getPhotos(fetchParams)
      .then(data => {
        const assets = data.edges
        const images = assets.filter(asset => asset.node.type.includes("image")).map(asset => {
          return asset.node.image
        })
        const random = Math.floor(Math.random() * images.length)

        const image = images[random]

        // c.f. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
        function getRandomInt(max) {
          return Math.floor(Math.random() * Math.floor(max))
        }

        // ImageEditor.cropImage(image.uri, cropData).then(url => {
        setState({
          imageSource: image,
          cropTop: getRandomInt(image.height - 500),
          cropLeft: getRandomInt(image.width - 500),
          dialog: "GameIntro"
        })
        // })
      })
      .catch(err => console.error(err))
  }

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "filekeeper:rules",
          "Une de tes photos a disparu de l’Album de Gao ! Aide-le à la retrouver",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={() => askPermission()}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// 2.Accept to share your phone pictures
const FileKeeperGameIntro = props => {
  const { t } = useTranslation()
  const {setState} = props

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "filekeeper:post-welcome",
          "Dans tes images, clique sur celle qui correspond au bout affiché avant que le temps ne soit écoulé.",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => {
          setState({
            dialog: "GameTimer",
            showError: false
          })
        }}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

const GameFirstPlay = props => {
  const { t } = useTranslation()
  const { setState } = props
  const rootStore = useStores()

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "filekeeper:welcome",
          "Chaque Gao Game te demande des permissions pour fonctionner, mais rassure-toi, elles ne servent que pour le jeu, et PERSONNE d'autre que toi n'y a accès. Si tu as le moindre doute, toute l'info est sur gaoandblaze.org !",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => {
          rootStore.setFirstStart()
          setState({ dialog: "GameStart" })
        }}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// 3.Game on with timer: show fragment
const FileKeeperGameTimer = props => {
  const { state } = props
  const { setState } = props
  const { t } = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  return (
    <>
      <View style={styles.containerView}>
        { state.imageSource && (
          <CroppedImage
            cropWidth="{props.frame.width}"
            cropHeight={props.frame.height}
            cropTop={state.cropTop}
            cropLeft={state.cropLeft}
            width={state.imageSource.width}
            height={state.imageSource.height}
            source={state.imageSource}
            style={props.frame}
          />
        )}
        <View style={styles.timerView}>
          { state.dialog === "GameTimer" && <Timer playerGivenTime={PLAYER_GIVEN_TIME} textStyle={styles.timerText} finishTimer={() => setState({ dialog: "GameOver" })} change={(timer, timerR) => {
            //console.log(timer, state)
          }} />}
        </View>
        <Button
          text={t("filekeeper:find-image", "Cherche l’image")}
          theme="gao"
          preset="play"
          disabled={state.disableButton}
          onPress={() => {
            setState({disableButton: true})
            launchImageLibrary({mediaType: "photo"}, response => {
              // Check if the player win
              setState({ disableButton: false })
              if (response.assets) {
                ImgToBase64.getBase64String(state.imageSource.uri)
                .then(base64Str => {
                  ImgToBase64.getBase64String(response.assets[0].uri).
                  then(anotherBase64 => 
                    {
                      if(base64Str === anotherBase64) {
                        metaGame.reward(10)
                        metaGame.playGame("TheFilekeeper", navigation, t)
                        setState({ dialog: "GameSuccess", disableButton: false })
                      } else {
                        setState({ showError: true, disableButton: false })
                      }
                    }
                    );
                })
                //console.log(timerRef)
              } else {
                if(response.assets)
                  setState({ showError: true, disableButton: false })
              }
            })
          }}
        />
      </View>
      {state.showError && <View style={styles.filekeeperAdvice}>
        <GaoDialog
          preset="advice"
          textDialog2={t("filekeeper:error", "Oups, pas celle-ci !")}
          textStyle2={styles.gaoAdvice}
        /></View> }
    </>
  )
}

// 5. GAME OVER "You lose" (NB: there's no screen "4" in mockups)
const FileKeeperGameOver = props => {
  const { state, setState } = props
  const { t } = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheFilekeeper", navigation, t)
  })

  return (
    <>
      {state.imageSource && (
        <Image
          source={state.imageSource}
          style={{width: "100%", height: "100%", position: "absolute"}}
        />
      )}
      <View style={styles.containerView}>
        <GaoDialog
          textDialog1={t("common:gameover", "TU AS PERDU !")}
          textButton={t("common:retry", "Réessaye")}
          textStyle1={{ color: "#EB358A" }}
          uriImage={require("assets/images/common/sad_emoji.png")}
          buttonOnPress={() => setState({ dialog: "GameStart" })}
          dialogViewStyle={{marginHorizontal: "-25%"}}
        />
      </View>
    </>
  )
}

// 5. GAME OVER
const FilekeeperPermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      props.navigation.navigate("preHome")
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-25%"}}
      />
    </View>
  )
}

// 6. GameAnswer
const FileKeeperGameAnswer = props => {
  const { state } = props
  return (
    <>
      {state.imageSource && (
        <Image
          source={state.imageSource}
          style={{width: "100%", height: "100%", position: "absolute"}}
        />
      )}
      <View style={styles.containerView}>
        <Image source={require("assets/images/common/right.png")} style={styles.iconImage} />
      </View>
    </>
  )
}

// TODO: create checkedIcon components (used also in Forger )
const CheckedIcon = () => (
  <Image source={require("assets/images/common/right.png")} style={styles.checkedIconImage} />
)

// 7.Game Success
const FileKeeperGameSuccess = props => {
  const { state } = props
  const rootStore = useStores()
  return (
    <>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      {state.imageSource && (
        <Image
          source={state.imageSource}
          style={{width: "100%", height: "100%", position: "absolute"}}
        />
      )}
      <View style={styles.containerView}>
        <GaoDialog
          preset="score"
          textMoney={10}
          navigation={props.navigation}
          uriImage={require("assets/images/common/wink_emoji.png")} // FIXME: add image
          dialogViewStyle={{marginHorizontal: "-25%"}}
        />
      </View>
    </>
  )
}

type GameScreenState = {
  imageSource: any
  cropTop: number
  cropLeft: number
  dialog: FileKeeperDialog
  showError: boolean
  time: number
  disableButton: boolean
}

export interface GameScreenProps extends NavigationScreenProps<{}> {}

@withTranslation()
@connectRootStore
export class GameScreen extends React.Component<GameScreenProps, GameScreenState> {
  constructor(props) {
    super(props)
    this.state = {
      imageSource: null,
      cropTop: 0,
      cropLeft: 0,
      dialog: this.props.rootStore.isFirstStart?"GameFirstPlay":"GameStart",
      showError: false,
      time: 0,
      disableButton: false
    }
    //this.startGame = this.startGame.bind(this)
    //this.getRandomImage = this.getRandomImage.bind(this)
    //this.letPlayerPickImage = this.letPlayerPickImage.bind(this)
  }

  // NB: took from https://stackoverflow.com/questions/50472316/how-to-fetch-random-image-from-camera-roll-using-react-native

  render() {
    this.props.rootStore.setSong("filekeeper.ogg", this.props.rootStore.gameMusic)
    const FileKeeperStateMachine = StateMachine({initialState: this.state})
    /* FIXME: change background for screen 3->6 with 'gao_filekeeper2' */
    const FileKeeperFrame = GaoFrame({
      wallpaper: require("assets/images/wallpaper/gao_filekeeper2.png"),
      coords: [120, 285, 515, 760],
    })
    const FileKeeperScreen = FileKeeperFrame(props => (
      <FileKeeperStateMachine dialogs={{
        "GameIntro": FileKeeperGameIntro,
        "GameStart": FileKeeperGameStart,
        "GameTimer": FileKeeperGameTimer,
        "GameAnswer": FileKeeperGameAnswer,
        "GameSuccess": FileKeeperGameSuccess,
        "GameOver": FileKeeperGameOver,
        "GamePermissionDenied": FilekeeperPermissionDenied,
        "GameFirstPlay": GameFirstPlay
      }} {...props}
      //letPlayerPickImage={this.letPlayerPickImage}
      />
    ))

    return (
      <>
        <GaoContainer {...this.props} character={require("assets/images/download_gifs/gao-Gao_the_fileKeeper.gif")}>
          <FileKeeperScreen navigation={this.props.navigation} />
        </GaoContainer>
      </>
    )
  }
}
