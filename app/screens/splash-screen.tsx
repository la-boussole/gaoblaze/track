import * as React from "react"
import { Animated, Image, Text, View } from "react-native"
import * as styles from "gao-theme/styles"


export interface SpecialLiveEventScreenProps {
  onFinish: () => void
}

export const SplashScreen: React.FunctionComponent<SpecialLiveEventScreenProps> = (props) => {
  const [fadeAnim] = React.useState(new Animated.Value(1.0))
  const [fadeAnimWarning] = React.useState(new Animated.Value(0))
  const [fadeAnimClose] = React.useState(new Animated.Value(0.0))
  const [showGif, setShowGif] = React.useState(false)

  React.useEffect(() => {
    setTimeout(
      () => {
        fadeOut.start((result) => {
          if (result.finished) {
            setShowGif(true)
            fadeIn.start()
            setTimeout(() => {
              fadeOutClose.start((finished) => {
                if (finished.finished) {
                  fadeInWarning.start((warningInFinished) => {
                    if (warningInFinished.finished) {
                      fadeOutWarning.start((warningOutFinished) => {
                        if (warningOutFinished.finished) {
                          props.onFinish()
                        }
                      })
                    }
                  })
                }
              })
            }, 4000)
          }
        })
      }
      , 3000)
  }, [])

  const fadeIn = Animated.timing(fadeAnimClose, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1500,
    useNativeDriver: true,
  })

  const fadeInWarning = Animated.timing(fadeAnimWarning, {
    toValue: 1,
    duration: 4000,
    useNativeDriver: true,
  })

  const fadeOutWarning = Animated.timing(fadeAnimWarning, {
    toValue: 0,
    duration: 4000,
    useNativeDriver: true,
  })

  const fadeOutClose = Animated.timing(fadeAnimClose, {
    toValue: 0,
    duration: 1500,
    useNativeDriver: true,
  })

  return (
    <>
      <Animated.View style={[styles.splashGifContainer, { opacity: fadeAnimClose }]}>
        {showGif && <Image style={styles.splashGif} source={require("assets/images/splash/gao_logo_2_2.gif")} />}
      </Animated.View>
      <Animated.View style={[styles.splashMainContainer, { opacity: fadeAnim }]}>
        <Image style={styles.splashBoussoleImage} source={require("assets/images/splash/other_la_boussole_logo.png")} />
        <View style={styles.splashMAIF}>
          <Text style={styles.splashText}>Avec le soutien de:</Text>
          <View style={styles.splashMAIFImages}>
            <Image style={styles.splashMAIFImage} source={require("assets/images/splash/logos.png")} />
          </View>
        </View>
      </Animated.View>
      <Animated.View style={[styles.splashWarningContainer, { opacity: fadeAnimWarning }]}>
        <Image style={styles.splashWarningImage} source={require("assets/images/splash/warning_screen.png")} />
      </Animated.View>
    </>
  )
}
