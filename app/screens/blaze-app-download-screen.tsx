import React, { useEffect } from "react"
import { Text } from "components"
import { BackHandler, Image, View } from "react-native"
import { PlayBack } from "components/sound/play-back"
import * as styles from "gao-theme/styles"
import { NavigationScreenProp } from "react-navigation"
import { NavigationActions } from "react-navigation"
import { useStores } from "models/root-store"


export const BlazeAppDownloadScreen: React.FunctionComponent<NavigationScreenProp<{}>> = (props) => {

  const [imageIndex, setImageIndex] = React.useState(0)
  const {navigation} = props
  const rootStore = useStores()

  const IMAGES = [
    require("assets/images/common/download_bar.png"),
    require("assets/images/common/download_bar_1.png"),
    require("assets/images/common/download_bar_2.png"),
  ]

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    setTimeout(() => {
      changeImage(imageIndex)
    }, 2000)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    return true
  }

  const changeImage = (index) => {
    if(index === IMAGES.length - 1) {
      navigation.navigate("chat")
    } else {
      setImageIndex(index + 1)
      setTimeout(() => {
        changeImage(index + 1)
      }, 2000)
    }
  }

  return (
    <View style={styles.defaultContainerView}>
      <View style={styles.mainDialogScoreView}>
        { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
        <View style={styles.popupContainerDownload}>
          <Text style={styles.defaultSecondaryText}>Blaze.app</Text>
          <View style={styles.downloadContainer}>
            <Image style={styles.downloadBar} source={IMAGES[imageIndex]} />
          </View>
        </View>
      </View>
    </View>
  )
}