import * as React from "react"
import { observer } from "mobx-react-lite"
import {BackHandler, ViewStyle} from "react-native"
import { Button, Screen, Text } from "../../components"
import { NavigationScreenProp } from "react-navigation"
import { useStores } from "models/root-store"
import {useEffect} from "react"
import { PlayBack } from "components/sound/play-back"

export interface DeveloperScreenProps {
  navigation: NavigationScreenProp<{}>
}

const ROOT: ViewStyle = {
  flex: 1, backgroundColor: "#000",
}

export default observer((props: DeveloperScreenProps) => {
  const { navigationStore } = useStores()
  const rootStore = useStores()
  const { navigation } = props

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
    navigation.navigate("preHome")
    return true
  }

  rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)

  return (
    <Screen style={ROOT} preset="scroll">
      <Text preset="header" />
      <Button onPress={() => navigationStore.navigateTo("scan")}
        preset="developer"
        theme="gao"
        text={"Scan"}/>
      <Button onPress={() => navigationStore.navigateTo("blazeLastMessage")}
        preset="developer"
        theme="gao"
        text={"Last message"}/>
      <Button onPress={() => navigationStore.navigateTo("gaoBlazeAnnoucement")}
        preset="developer"
        theme="gao"
        text={"Blaze's annoucement"}/>
      <Button onPress={() => navigationStore.navigateTo("gao_celebration")}
        preset="developer"
        theme="gao"
        text={"Go to Live-stream"}/>
      <Button onPress={() => navigationStore.navigateTo("blazeChat")}
        preset="developer"
        theme="gao"
        text={"Crypted Chat"}/>
      <Button onPress={() => navigationStore.navigateTo("blazeStack")}
        preset="developer"
        theme="gao"
        text={"Go to Blaze"}/>
    </Screen>
  )
})
