import React, {useEffect, useState} from "react"
import {Text, Wallpaper} from "components"
import {BackHandler, Image, View} from "react-native"
import { PlayBack } from "components/sound/play-back"
import * as styles from "gao-theme/styles"
import { NavigationScreenProp } from "react-navigation"
import { useStores } from "models/root-store"
import {FILES} from "assets/DownloadFiles"
import RNFS from "react-native-fs"
import {color} from "gao-theme";


export const GaoAppDownloadScreen: React.FunctionComponent<NavigationScreenProp<{}>> = (props) => {

  const {navigation} = props
  const rootStore = useStores()
  const [downloadIndex, setDownloadIndex] = useState(0)

  useEffect(() => {
    if(rootStore.isFirstOpenApp) {
      RNFS.mkdir(DIRECTORY_DOWNLOAD).then(() => downloadFiles(0))
    }
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    return true
  }

  const DIRECTORY_DOWNLOAD = RNFS.ExternalDirectoryPath + "/Gao"

  const downloadFiles = (index) => {
    setDownloadIndex(index)
    if(index === FILES.length) {
      rootStore.setFirstOpenApp()
      navigation.navigate("gaoWelcomingLanguage")
    } else {
      RNFS.exists(DIRECTORY_DOWNLOAD+"/"+FILES[index]).then(exists => {
        if(!exists) {
          RNFS.downloadFile({
            fromUrl: "https://gaoandblaze.org/wp-content/uploads/2021/11/"+FILES[index]+"",
            toFile: DIRECTORY_DOWNLOAD+"/"+FILES[index],
            connectionTimeout: 10000
          }).promise.then(() => {
            console.log("Descargado", FILES[index])
            downloadFiles(index+1)
          }).catch((er) => {
            console.log("El error", er)
            setTimeout(() => downloadFiles(index), 5000)
          })
        } else {
          console.log("Existe", FILES[index])
          downloadFiles(index+1)
        }
      })
    }
  }

  return (
    <View style={styles.defaultContainerView}>
      <Wallpaper preset="stretch" backgroundImage={require("assets/images/wallpaper/HomeScreenAlpha.png")} disable={true} />
      <Image source={require("assets/images/gao-the-athlete/gao_gao_the_athlete.gif")} style={{position: "absolute", width: "100%", bottom: 10, height: 150, resizeMode: "contain"}} />
      <View style={styles.mainDialogScoreView}>
        { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
        <View style={styles.popupContainerDownload}>
          <Text style={styles.defaultSecondaryText}>Gao a besoin d’internet et va chercher quelques costumes pour t'épater !</Text>
          <View style={styles.gaoDownloadContainer}>
            <View style={styles.gaoDownloadBar}>
              <View style={{backgroundColor: color.palette.green, width: ((downloadIndex*100.)/FILES.length) + "%", height: "100%"}}></View>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}
