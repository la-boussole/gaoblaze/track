import { Image, Text, View } from "react-native"
import * as React from "react"
import * as styles from "blaze-theme/styles"
import { Wallpaper } from "components"
import { ScrollView, TextInput, TouchableOpacity } from "react-native-gesture-handler"
import DropDownPicker from "react-native-dropdown-picker"
import Slider from "@react-native-community/slider"
import { color } from "blaze-theme/index"
import { useStores} from "models/root-store"
import Toast from "react-native-toast-message"
import {useTranslation} from "react-i18next"
import {
  getQuestionary,
  QuestionChecklist,
  QuestionEmail, QuestionMany,
  QuestionMultiple,
  QuestionNumber,
  QuestionScale
} from "assets/Questionary"
import {useRef} from "react"

export interface LastMessageQuestionaryScreenProps {
  onFinish: () => void
}

export const TextInputQuestionary: React.FunctionComponent<QuestionNumber> = (props) => {
  return (<TextInput keyboardType="number-pad" style={styles.lastMessageQuestionaryTextInput} onChangeText={(text) => props.onChange(text)} />)
}

export const TextEmailInputQuestionary: React.FunctionComponent<QuestionEmail> = (props) => {
  return (<TextInput keyboardType="email-address" style={styles.lastMessageQuestionaryTextInput} onChangeText={(text) => props.onChange(text)} placeholder={"tonadresse@e-mail.com"} />)
}

export const MultipleQuestionary: React.FunctionComponent<QuestionMultiple> = (props) => {
  const [height, setHeight] = React.useState(40)
  const {t} = useTranslation()
  return (
    <View style={{ height }}>
      <DropDownPicker items={props.choices.map(item => { return { value: item, label: item } })} containerStyle={{ height: 40, }}
        style={styles.lastMessageQuestionaryMultiple}
        dropDownStyle={styles.lastMessageQuestionaryMultipleItem}
        activeItemStyle={styles.lastMessageQuestionaryMultipleItemSelected}
        activeLabelStyle={styles.lastMessageQuestionaryMultipleItemSelected}
        itemStyle={{ justifyContent: "flex-start" }}
        onOpen={() => setHeight(props.choices.length * 50)}
        onClose={() => setHeight(40)}
        scrollViewProps={{ persistentScrollbar: true }}
        onChangeItem={(item) => props.onChange(item.value)}
        placeholder={t("common:select", "select an item")}
      />
    </View>
  )
}

export const ChecklistQuestionary: React.FunctionComponent<QuestionChecklist> = (props) => {
  const [checks, setChecks] = React.useState([])

  const check = (item) => {
    if (checks.includes(item)) {
      props.onChange(checks.filter(itemSelected => itemSelected !== item))
      setChecks(checks.filter(itemSelected => itemSelected !== item))
    }
    else {
      props.onChange([...checks, item])
      setChecks([...checks, item])
    }
  }

  return (
    <View style={styles.lastMessageQuestionaryChecklist}>
      {
        props.choices.map(item => (
          <>
            <View style={styles.lastMessageQuestionaryChecklistTouchView}>
              <TouchableOpacity style={styles.lastMessageQuestionaryChecklistTouch} onPress={() => check(item)}>
                <View style={styles.lastMessageQuestionaryChecklistTouchImage}>
                  {
                    checks.includes(item) && <Image source={require("assets/images/end_game/questionary/check.png")} style={styles.lastMessageQuestionaryChecklistImage} />
                  }
                </View>
                <Text style={styles.lastMessageQuestionaryChecklistTouchText}>{item}</Text>
              </TouchableOpacity>
            </View>
          </>))
      }
    </View>
  )
}

export const ScaleQuestionary: React.FunctionComponent<QuestionScale> = (props) => {
  const [value, setValue] = React.useState(null)
  return (
    <View>
      <View style={styles.lastMessageQuestionarySlideNumbers}>
        <TouchableOpacity onPress={() => {
          setValue(1)
          props.onChange("1")
        }}>
          <Text style={[styles.lastMessageQuestionarySlideNumber, value===1?{color: "yellow"}:null]}>1</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          setValue(2)
          props.onChange("2")
        }}>
          <Text style={[styles.lastMessageQuestionarySlideNumber, value===2?{color: "yellow"}:null]}>2</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          setValue(3)
          props.onChange("3")
        }}>
          <Text style={[styles.lastMessageQuestionarySlideNumber, value===3?{color: "yellow"}:null]}>3</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          setValue(4)
          props.onChange("4")
        }}>
          <Text style={[styles.lastMessageQuestionarySlideNumber, value===4?{color: "yellow"}:null]}>4</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          setValue(5)
          props.onChange("5")
        }}>
          <Text style={[styles.lastMessageQuestionarySlideNumber, value===5?{color: "yellow"}:null]}>5</Text>
        </TouchableOpacity>
      </View>
      <Slider
        value={value}
        style={{ width: "100%", height: 20 }}
        minimumValue={1}
        maximumValue={5}
        minimumTrackTintColor={color.palette.letterGreen}
        maximumTrackTintColor={color.palette.letterGreen}
        step={1}
        onValueChange={(text) => props.onChange(text + "")}
      />
      <View style={styles.lastMessageQuestionarySlideNumbers}>
        <Text style={styles.lastMessageQuestionarySlideText} numberOfLines={1}>{props.firstLabel}</Text>
        <Text style={styles.lastMessageQuestionarySlideTextRight}  numberOfLines={1}>{props.lastLabel}</Text>
      </View>
    </View>
  )
}

export const LastMessageQuestionaryScreen: React.FunctionComponent<LastMessageQuestionaryScreenProps> = (props) => {
  const { navigation } = props
  const { t } = useTranslation()
  const { metaGame } = useStores()
  const rootStore = useStores()
  const [module, setModule] = React.useState(0)
  const [values, setValues] = React.useState({})
  const [idGamer, setIdGamer] = React.useState(rootStore.idGamer)
  const questionary = getQuestionary()
  const scrollRef = useRef()

  const changeModule = (state: "ok" | "cancel") => {
    if (module === 0 && state === "cancel") {
      props.onFinish()
    }
    if (state === "ok") {
      metaGame.setFillQuestionary()
      if(questionary.modules[module].type === "stats") {
        questionary.modules[module].questions.forEach(item => values[item.titleSend] = getStats(item))
      }
      const modifiedValuesToSend = {...values}
      if(modifiedValuesToSend["Quelles émotions t’a causé ce jeu?"]) {
        modifiedValuesToSend["Quelles émotions t’a causé ce jeu?"] = modifiedValuesToSend["Quelles émotions t’a causé ce jeu?"].join()
      }
      console.log(modifiedValuesToSend)
      Object.keys(modifiedValuesToSend).forEach(key => metaGame.addReponseQuestionPoll(key, modifiedValuesToSend[key]))
      fetch("https://api.airtable.com/v0/appcSfcHamKDI0Wwf/Gao&Blaze",
        {
          method: idGamer.length > 0 ? "PATCH" : "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer key3Q289ve7suZ75K"
          },
          body: idGamer.length > 0 ? JSON.stringify({ records: [{ id: idGamer, fields: modifiedValuesToSend }] }) : JSON.stringify({ records: [{ fields: modifiedValuesToSend }] })
        }).then(response => response.json().then(json => {
        if(idGamer.length === 0) {
          console.log(response)
          setIdGamer(json.records[0].id)
          rootStore.setIdGamer(json.records[0].id)
        }
      }
      )).catch(err => console.log(err))
      if (module < questionary.modules.length - 1)
        setModule(module + 1)
      else
        props.onFinish()
      //console.log(JSON.stringify({records: [{fields: values}]}))
      return
    } else {
      if (module < questionary.modules.length - 1)
        setModule(module + 1)
      else
        props.onFinish()
    }
  }

  const checkMandatory = () => {
    if(questionary.modules[module].type === "questions") {
      let check = true
      questionary.modules[module].questions.forEach(item => {
        if(item.mandatory && !values[item.titleSend])
          check = false
      })
      return check
    }
    return true
  }

  const getStats = (question) => {
    let text = question.title
    while(text.indexOf("@") >= 0) {
      const indexOf = text.indexOf("@")
      const variable = parseInt(text.substr(indexOf + 1, 1))
      text = text.substr(0, indexOf) + question.options.getData(variable, metaGame, rootStore) + text.substr(indexOf + 2, text.length)
    }
    return text
  }

  return (
    <>
      <View style={styles.lastMessageMainQuestionContainer}>
        <Wallpaper preset="stretch" backgroundImage={require("assets/images/end_game/questionary/background_questionary.png")} />
        <View style={styles.lastMessageQuestionaryContainer}>
          <Text style={styles.lastMessageQuestionaryText}>[{questionary.modules[module].title}]</Text>
          <ScrollView style={styles.lastMessageQuestionaryQuestionsContainer} ref={scrollRef}>
            {
              questionary.modules[module].type === "stats" && questionary.modules[module].questions.map(question => (
                <View key={question.id}>
                  <Text style={[styles.lastMessageQuestionaryTextQuestion, {fontSize: 20}]}>{getStats(question)}</Text>
                </View>
              ))
            }
            {
              questionary.modules[module].type === "questions" && questionary.modules[module].questions.map(question => (
                <View key={question.id}>
                  <Text style={styles.lastMessageQuestionaryTextQuestion}>{question.title}</Text>
                  {question.type === "number" && <TextInputQuestionary {...(question.options as QuestionNumber)} onChange={(text) => {
                    values[question.titleSend] = text
                    setValues({ ...values })
                  }} />}
                  {question.type === "multiple" && <MultipleQuestionary {...(question.options as QuestionMultiple)} onChange={(text) => {
                    values[question.titleSend] = text
                    setValues({ ...values })
                  }} />}
                  {question.type === "scale" && <ScaleQuestionary {...(question.options as QuestionScale)} onChange={(text) => {
                    values[question.titleSend] = text
                    setValues({ ...values })
                  }} />}
                  {question.type === "checklist" && <ChecklistQuestionary {...(question.options as QuestionChecklist)} onChange={(text) => {
                    values[question.titleSend] = text
                    setValues({ ...values })
                  }} />}
                  {question.type === "email" && <TextEmailInputQuestionary {...(question.options as QuestionEmail)} onChange={(text) => {
                    values[question.titleSend] = text
                    setValues({ ...values })
                  }} />}
                  {question.type === "many" && (question.options as QuestionMany).questions.map(secondQuestion =>
                    <View key={secondQuestion.id}>
                      <Text style={styles.lastMessageQuestionaryTextQuestion}>{secondQuestion.title}</Text>
                      {secondQuestion.type === "number" && <TextInputQuestionary {...(secondQuestion.options as QuestionNumber)} onChange={(text) => {
                        values[secondQuestion.titleSend] = text
                        setValues({ ...values })
                      }} />}
                      {secondQuestion.type === "multiple" && <MultipleQuestionary {...(secondQuestion.options as QuestionMultiple)} onChange={(text) => {
                        values[secondQuestion.titleSend] = text
                        setValues({ ...values })
                      }} />}
                      {secondQuestion.type === "scale" && <ScaleQuestionary {...(secondQuestion.options as QuestionScale)} onChange={(text) => {
                        values[secondQuestion.titleSend] = text
                        setValues({ ...values })
                      }} />}
                      {secondQuestion.type === "checklist" && <ChecklistQuestionary {...(secondQuestion.options as QuestionChecklist)} onChange={(text) => {
                        values[secondQuestion.titleSend] = text
                        setValues({ ...values })
                      }} />}
                      {secondQuestion.type === "email" && <TextEmailInputQuestionary {...(secondQuestion.options as QuestionEmail)} onChange={(text) => {
                        values[secondQuestion.titleSend] = text
                        setValues({ ...values })
                      }} />}
                    </View>
                  )}
                </View>
              ))
            }
          </ScrollView>
          {questionary.modules[module].textConfirmation && <Text style={styles.lastMessageQuestionaryTextQuestion}>{questionary.modules[module].textConfirmation}</Text>}
          <View style={styles.buttonQuestionaryContainer}>
            <View style={styles.buttonQuestionary}>
              <TouchableOpacity onPress={() => {
                if(!checkMandatory()) {
                  Toast.show({
                    text1: t("blaze:questionary-error", "Il reste des questions sans réponse"),
                    type: "error",
                    visibilityTime: 1500,
                    position: "bottom",
                    onShow: () => console.log("show")
                  })
                } else {
                  changeModule("ok")
                  scrollRef.current.scrollTo({x:0, y:0, animated: false})
                }
              }}>
                <Text style={{textAlign: "center"}}>{questionary.modules[module].buttonOk}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Toast ref={(ref) => Toast.setRef(ref)} />
      </View>
    </>
  )
}
