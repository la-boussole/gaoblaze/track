import * as React from "react"
import {Animated, BackHandler, Image, ScrollView, TouchableOpacity, View} from "react-native"
import {Text} from "components"
import LinearGradient from "react-native-linear-gradient"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next";

const styles = {
  containerView: {
    flex: 1,
    fontFamily: "Daniel-Black",
  },
  containerViewDetail: {
    flex: 1,
    justifyContent: "center"
  },
  imageNavBar: {
    width: "100%",
    resizeMode: "stretch",
    height: 30
  },
  imageBanner: {
    width: "100%",
    resizeMode: "stretch",
    height: 100,
  },
  containerName: {
    width: "100%",
    paddingLeft: 120,
    flexDirection: "row",
    height: 40,
    backgroundColor: "white",
  },
  rokayaName: {
    fontSize: 18
  },
  rokayaAvatar: {
    width: 100,
    height: 100,
    position: "absolute",
    left: 10,
    bottom: 10,
  },
  nameView: {
    flex: 1,
  },
  linesView: {
    flex: 3,
    flexDirection: "row-reverse",
    alignItems: "center"
  },
  nameLineView: {
    width: "100%",
    resizeMode: "stretch",
    height: 7,
  },
  line1: {
    flex: 1,
  },
  line2: {
    flex: 2,
  },
  line3: {
    flex: 3,
  },
  line4: {
    flex: 1,
  },
  lineStretch: {
    resizeMode: "stretch",
    width: "80%",
    height: 10,
  },
  grayZone: {
    height: 20,
    backgroundColor: "#e5e5e5"
  },
  postContainer: {
    flexDirection: "row"
  },
  postViewAvatar: {
    width: 100
  },
  postViewAvatarDetail: {
    width: 80
  },
  postComment: {
    flexDirection: "row",
  },
  postAvatar: {
    resizeMode: "contain",
    width: 60,
    height: 70,
    alignSelf: "center",
  },
  postAvatarDetail: {
    resizeMode: "contain",
    width: 40,
    height: 50,
    alignSelf: "center",
  },
  postImage: {
    resizeMode: "contain",
    width: "100%",
    aspectRatio: 1.45,
  },
  postImageDetail: {
    resizeMode: "contain",
    width: "100%",
    aspectRatio: 1.45,
  },
  postNameLineView: {
    width: 55,
    height: 5,
    resizeMode: "stretch",
  },
  postNameLineViewDetail: {
    width: "95%",
    height: 5,
    resizeMode: "stretch",
  },
  postContainerView: {
    flex: 1,
    paddingRight: 30,
  },
  postContainerViewDetail: {
    flex: 1,
    marginBottom: 20,
  },
  mainPostContainerView: {
    flex: 1,
    backgroundColor: "#d5d5d5"
  },
  mainPostContainerViewDetail: {
    flex: 1,
  },
}

export const POSTS = [
  {
    id: "4",
    type: "text",
    text: "long-post",
    likes: "100k",
    shared: "5k",
    comments: "1.2k",
  },
  {
    id: "3",
    text: "#Selfie",
    likes: "1.5M",
    shared: "117k",
    comments: "521k",
    image: require("assets/images/blaze-character-scenes/rokaya/image2big_0013.png"),
    type: "image",
    commentTexts: [
      {id: "3.1", text: "comment-31", image: require("assets/images/blaze-character-scenes/rokaya/avatar7.png")},
      {id: "3.2", text: "comment-32", image: require("assets/images/blaze-character-scenes/rokaya/avatar8.png")},
      {id: "3.3", text: "comment-33", image: require("assets/images/blaze-character-scenes/rokaya/RokayaAvatar_0024.png")}
    ],
  },
  {
    id: "12",
    text: "#ShelAd",
    likes: "100k",
    shared: "20k",
    comments: "1521k",
    image: require("assets/images/blaze-character-scenes/rokaya/shel_ad.png"),
    type: "image",
    commentTexts: [
      {id: "12.1", text: "comment-121", image: require("assets/images/blaze-character-scenes/rokaya/avatar9.png")},
      {id: "12.2", text: "comment-122", image: require("assets/images/blaze-character-scenes/rokaya/avatar8.png")},
      {id: "12.3", text: "comment-123", image: require("assets/images/blaze-character-scenes/rokaya/avatar7.png")}
    ],
  },
  {
    id: "2",
    text: "post-2",
    likes: "351k",
    shared: "42k",
    comments: "37k",
    image: require("assets/images/blaze-character-scenes/rokaya/image4big_0015.png"),
    type: "image",
    commentTexts: [
      {id: "2.1", text: "This brings me peace ^^", image: require("assets/images/blaze-character-scenes/rokaya/avatar4.png")},
      {id: "2.2", text: "You really nail those Gao challenges!", image: require("assets/images/blaze-character-scenes/rokaya/avatar5.png")},
      {id: "2.3", text: "You should do a rainbow series!", image: require("assets/images/blaze-character-scenes/rokaya/avatar6.png")}
    ],
  },
  {
    id: "1",
    text: "post-1",
    likes: "928",
    shared: "130",
    comments: "739",
    image: require("assets/images/blaze-character-scenes/rokaya/image3big_0014.png"),
    type: "image",
    commentTexts: [
      {id: "1.1", text: "That’s a “insert french ice-cream”! I love them.", image: require("assets/images/blaze-character-scenes/rokaya/avatar1.png")},
      {id: "1.2", text: "Love how you keep it simple :)", image: require("assets/images/blaze-character-scenes/rokaya/avatar2.png")},
      {id: "1.3", text: "Go, Gao! *Cat emote*", image: require("assets/images/blaze-character-scenes/rokaya/avatar3.png")}
    ]
  },
  {
    id: "5",
    likes: "4k",
    shared: "1k",
    comments: "765",
    image: require("assets/images/blaze-character-scenes/rokaya/image1big_0012.png"),
    type: "image",
  },
  {
    id: "6",
    likes: "1.5k",
    shared: "200",
    comments: "528",
    image: require("assets/images/blaze-character-scenes/rokaya/image5big_0016.png"),
    type: "image",
  },
  {
    id: "7",
    likes: "800",
    shared: "160",
    comments: "35",
    image: require("assets/images/blaze-character-scenes/rokaya/image6big_0017.png"),
    type: "image",
  },
  {
    id: "8",
    likes: "256",
    shared: "128",
    comments: "16",
    image: require("assets/images/blaze-character-scenes/rokaya/image7big_0018.png"),
    type: "image",
  },
  {
    id: "9",
    likes: "1",
    shared: "0",
    comments: "1",
    image: require("assets/images/blaze-character-scenes/rokaya/image8big_0019.png"),
    type: "image",
  }
]

const Post = props => {
  const item = props.comment
  const {t} = useTranslation()

  return (
    <View style={{backgroundColor: "white",}}>
      <View key={item.id} style={styles.postContainer} >
        <View style={styles.postViewAvatar}>
          <View>
            <Image style={styles.postAvatarDetail} source={require("assets/images/blaze-character-scenes/rokaya/RokayaAvatar_0024.png")} />
          </View>
        </View>
        <View style={styles.nameView}>
          <Text>Rokaya92</Text>
          <Image style={styles.postNameLineView} source={require("assets/images/blaze-character-scenes/rokaya/Line2.png")} />
          {
            item.type !== "text" && <Text>
              {t("rokaya:"+item.text, item.text?item.text:"")}
            </Text>
          }
        </View>
      </View>
      <View style={styles.postContainerViewDetail}>
        <View style={{flexDirection: "row"}}>
          {
            item.type === "image" && <Image style={styles.postImageDetail} source={item.image} />
          }
        </View>
        <View style={{flexDirection: "row", marginLeft: 20}}>
          <View style={{flexDirection: "row", alignItems: "center"}}>
            <Image style={{width: 22, height: 18, resizeMode: "cover", marginRight: 5,}} source={require("assets/images/blaze-character-scenes/rokaya/hearth.png")} />
            <Text style={{fontSize: 20}}>{item.likes}</Text>
          </View>
          <View style={{flexDirection: "row", marginLeft: 10, alignItems: "center"}}>
            <Image style={{width: 25, height: 18, resizeMode: "cover", marginRight: 5,}} source={require("assets/images/blaze-character-scenes/rokaya/share.png")} />
            <Text style={{fontSize: 20}}>{item.shared}</Text>
          </View>
          <View style={{flexDirection: "row", marginLeft: 10, alignItems: "center"}}>
            <Image style={{width: 30, height: 18, resizeMode: "cover", marginRight: 5,}} source={require("assets/images/blaze-character-scenes/rokaya/comment.png")} />
            <Text style={{fontSize: 20}}>{item.comments}</Text>
          </View>
        </View>
      </View>
      {
        item.commentTexts.map(comment =>
          <View key={comment.id} style={styles.postComment}>
            <View style={styles.postViewAvatarDetail}>
              <Image style={styles.postAvatarDetail} source={comment.image} />
            </View>
            <View style={styles.containerViewDetail}>
              <Text>{t("rokaya:"+comment.text, comment.text)}</Text>
              <Image style={styles.postNameLineViewDetail} source={require("assets/images/blaze-character-scenes/rokaya/Line2.png")} />
            </View>
          </View>
        )
      }
      <TouchableOpacity style={{height: 60}} onPress={() => props.back()}>
        <Image style={{width: "100%", resizeMode: "contain", height: "100%"}} source={require("assets/images/blaze-character-scenes/rokaya/backbtnglow_0031.png")} />
      </TouchableOpacity>
    </View>
  )
}

export const BlazeRokayaScreen = props => {
  const {t} = useTranslation()
  const rootStore = useStores()
  const [comment, setComment] = React.useState(null)
  const {navigation} = props
  const {metaGame} = useStores()
  const characterInfo = navigation.getParam("characterInfo")
  const [fadeAnim] = React.useState(new Animated.Value(1))

  const fadeOut = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1500,
    useNativeDriver: true,
  })

  React.useEffect(() => {
    //console.log()
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
    }
  })

  const handleBackButtonClick = () => {
    if(comment)
      setComment(null)
    else
      return false
    return true
  }

  return (
    <View style={[styles.containerView, {backgroundColor: "black"}]}>
      <Animated.View style={[styles.containerView, {opacity: fadeAnim}]}>
        <ScrollView onScroll={(event) => {
          const offset = event.nativeEvent.contentOffset.y
          const contentSize = event.nativeEvent.contentSize.height
          const size = event.nativeEvent.layoutMeasurement.height
          if(Math.abs(offset+size - contentSize) < 100 && !comment) {
            fadeOut.start((end) => {
              if(end.finished) {
                rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
                metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
              }
            })
          }
        }}>
          <View>
            {!comment && <View style={{position: "absolute", top: "40%", left: 0, height: "60%", width: "100%", zIndex: 10}}>
              <LinearGradient colors={["#aaaaaa00", "#000000ee"]} style={{width: "100%", height: "100%"}}>
                <Text style={{fontFamily: "Daniel-Black", position: "absolute", top: "80%", color: "white", width: "100%", textAlign: "center", fontSize: 30,}}>
                  {t("rokaya:last-message", "Faîtes défiler pour quitter...")}
                </Text>
              </LinearGradient>
            </View>}
            <View>
              <Image style={styles.imageNavBar} source={require("assets/images/blaze-character-scenes/rokaya/navbar.png")} />
              <Image style={styles.imageBanner} source={require("assets/images/blaze-character-scenes/rokaya/banner.png")} />
              <View style={styles.containerName}>
                <View style={styles.nameView}>
                  <Text style={styles.rokayaName}>Rokaya92</Text>
                  <Image style={styles.nameLineView} source={require("assets/images/blaze-character-scenes/rokaya/Line2.png")} />
                </View>
                <View style={styles.linesView}>
                  <View style={styles.line1}>
                    <Image style={styles.lineStretch} source={require("assets/images/blaze-character-scenes/rokaya/line.png")} />
                  </View>
                  <View style={styles.line2}>
                    <Image style={styles.lineStretch} source={require("assets/images/blaze-character-scenes/rokaya/line.png")} />
                  </View>
                  <View style={styles.line3}>
                    <Image style={styles.lineStretch} source={require("assets/images/blaze-character-scenes/rokaya/line.png")} />
                  </View>
                  <View style={styles.line4} />
                </View>
              </View>
              <View style={styles.grayZone}/>
              <Image style={styles.rokayaAvatar} source={require("assets/images/blaze-character-scenes/rokaya/RokayaAvatar_0024.png")} />
            </View>
            <View style={comment?styles.mainPostContainerViewDetail:styles.mainPostContainerView}>
              { comment && <Post comment={comment} back={() => setComment(null)} /> }
              { !comment &&
              POSTS.map(item => (
                <View key={item.id} style={styles.postContainer} >
                  <View style={styles.postViewAvatar}>
                    <View>
                      <Image style={styles.postAvatar} source={require("assets/images/blaze-character-scenes/rokaya/RokayaAvatar_0024.png")} />
                    </View>
                  </View>
                  <View style={styles.postContainerView}>
                    <View style={styles.nameView}>
                      <Text>Rokaya92</Text>
                      <Image style={styles.postNameLineView} source={require("assets/images/blaze-character-scenes/rokaya/Line2.png")} />
                    </View>
                    <TouchableOpacity onPress={() => {
                      if(item.commentTexts) {
                        setComment(item)
                      }
                    }} >
                      <Text>
                        {t("rokaya:"+item.text, item.text?item.text:"")}
                      </Text>
                      {
                        item.type === "image" && <View style={{flexDirection: "row"}}><Image style={styles.postImage} source={item.image} /></View>
                      }
                    </TouchableOpacity>
                    <View style={{flexDirection: "row"}}>
                      <View style={{flexDirection: "row", alignItems: "center"}}>
                        <Image style={{width: 22, height: 18, resizeMode: "cover", marginRight: 5,}} source={require("assets/images/blaze-character-scenes/rokaya/hearth.png")} />
                        <Text style={{fontSize: 20}}>{item.likes}</Text>
                      </View>
                      <View style={{flexDirection: "row", marginLeft: 10, alignItems: "center"}}>
                        <Image style={{width: 25, height: 18, resizeMode: "cover", marginRight: 5,}} source={require("assets/images/blaze-character-scenes/rokaya/share.png")} />
                        <Text style={{fontSize: 20}}>{item.shared}</Text>
                      </View>
                      <View style={{flexDirection: "row", marginLeft: 10, alignItems: "center"}}>
                        <Image style={{width: 30, height: 18, resizeMode: "cover", marginRight: 5,}} source={require("assets/images/blaze-character-scenes/rokaya/comment.png")} />
                        <Text style={{fontSize: 20}}>{item.comments}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              ))
              }
            </View>
          </View>
        </ScrollView>
      </Animated.View>
    </View>
  )
}
