import * as React from "react"
import {Animated, Easing, Image, Linking, NativeModules, PermissionsAndroid, ScrollView, View} from "react-native"
import * as styles from "../blaze-theme/styles"
import { useTranslation } from "react-i18next"
import { BlazeProfileThought } from "assets/community_members/blaze-profiles"
import { NavigationScreenProp } from "react-navigation"
import CheckBox from "@react-native-community/checkbox"
import { Text, Button, ImageButton } from "components"
import { TouchableOpacity } from "react-native-gesture-handler"
import { color } from "blaze-theme"
import RNFS from "react-native-fs"
import { useStores } from "models/root-store"
import PushNotification from "react-native-push-notification"
import HyperLink from "react-native-hyperlink"

const REWARD_DIRECTORY = RNFS.DownloadDirectoryPath

export interface BlazeSkillPopupProps {
  thought: BlazeProfileThought
  navigation: NavigationScreenProp<{}>
  skill: number
  onOk?: () => void
}

/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */
export const BlazeNotebookSkillPopup: React.FunctionComponent<BlazeSkillPopupProps> = props => {
  const { t } = useTranslation()
  const navigation: NavigationScreenProp<{}> = props.navigation
  const thought: BlazeProfileThought = props.thought
  const idSkill = props.skill
  const onOk = props.onOk
  const skill = thought.skills.find((skill, index) => index === idSkill)
  const [activeItems, setActiveItems] = React.useState([])
  const [allHeight, setAllHeight] = React.useState(skill.subskills ? skill.subskills.map(() => null) : [])
  const [selectedImage, setSelectedImage] = React.useState(null)

  const isAllHeight = () => {
    return !allHeight.some((item) => item === null)
  }

  const getRow = (skill, index) => {
    const [animatedHeight] = React.useState(new Animated.Value(0.0))
    const { metaGame } = useStores()

    const interpolate = (indexSubSkill) => {
      return animatedHeight.interpolate({
        inputRange: [0, 1],
        outputRange: [0, allHeight[indexSubSkill]],
      })
    }

    const openAnim = Animated.timing(animatedHeight, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: false,
    })

    const closeAnim = Animated.timing(animatedHeight, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: false,
    })

    const press = (skill) => {
      const index = skill
      if (activeItems.includes(index)) {
        closeAnim.start(({ finished }) => {
          if (finished)
            setActiveItems([...activeItems.filter(item => item !== index)])
        })
      }
      else {
        metaGame.readSkill(thought.title + "-" + idSkill)
        if (activeItems.length > 0) {
          closeAnim.start(({ finished }) => {
            if (finished) {
              setActiveItems([index])
              openAnim.start()
            }
          })
        }
        else {
          metaGame.readSkill(thought.title + "-" + idSkill)
          openAnim.start()
          setActiveItems([index])
        }
      }
    }

    return (
      <View key={"skill-" + index}>
        <View>
          <Text style={styles.notebookSkillTitle}>{skill.title}</Text>
          <Text style={styles.notebookSkillSubTitle}>{skill.tweet}</Text>
        </View>
        {skill.subskills && skill.subskills.map((item, indexSubskill) =>
          <>
            <TouchableOpacity style={styles.notebookToughtPopupContainer} onPress={() => {
              press(indexSubskill)
            }}>
              <Text style={styles.notebookSkillTitle}>{item.title}</Text>
              <Animated.Image source={require("assets/images/blaze-icons/arrow_notebook.png")} style={{ transform: [{ rotate: activeItems.includes(indexSubskill) ? "180deg" : "0deg" }] }} />
            </TouchableOpacity>
            {
              isAllHeight() && activeItems.includes(indexSubskill) &&
              <Animated.View style={[styles.notebookToughtContainerText, { height: interpolate(indexSubskill) }]}>
                <View>
                  {/*<Text style={styles.skillTitle}>{skill.title}</Text>*/}
                  <HyperLink
                    onPress={(url: string, text: string) => Linking.openURL(url)}
                    linkDefault={false}
                    linkStyle={styles.boldFontText}
                    linkText={
                      url => {
                        const link = item.links.find(link => link.link === url)
                        if (link)
                          return link.text
                        return url
                      }
                    }
                  >
                    <Text style={styles.skillAbstract}>{item.abstract}</Text>
                  </HyperLink>
                  <View style={styles.skillScreenShotContainer}>
                    {
                      item.images && item.images.map((image, imageIndex) =>
                        <View style={styles.skillScreenshot} key={imageIndex + "-image"}>
                          <Image style={styles.skillScreenshotText} source={image.image} />
                          <View style={{ width: "100%", height: "100%", backgroundColor: color.transparent, position: "absolute" }}
                            onTouchEndCapture={() => setSelectedImage(image)}></View>
                        </View>
                      )
                    }
                  </View>
                </View>
              </Animated.View>
            }
            {
              !isAllHeight() &&
              <View style={[styles.notebookToughtContainerText, { flex: 1, opacity: 0 }]} onLayout={(event) => {
                allHeight[indexSubskill] = event.nativeEvent.layout.height
                setAllHeight([...allHeight])
              }}>
                <View>
                  {/*<Text style={styles.skillTitle}>{skill.title}</Text>*/}
                  <HyperLink
                    onPress={(url: string, text: string) => Linking.openURL(url)}
                    linkDefault={false}
                    linkStyle={styles.boldFontText}
                    linkText={
                      url => {
                        const link = item.links.find(link => link.link === url)
                        if (link)
                          return link.text
                        return url
                      }
                    }
                  >
                    <Text style={styles.skillAbstract}>{item.abstract}</Text>
                  </HyperLink>
                  <View style={styles.skillScreenShotContainer}>
                    {
                      item.images && item.images.map((image, imageIndex) =>
                        <View style={styles.skillScreenshot} key={imageIndex + "-image"} >
                          <Image style={styles.skillScreenshotText} source={image.image} />
                        </View>
                      )
                    }
                  </View>
                </View>
              </View>
            }
          </>
        )}
        { skill.showCheckBoxes &&
          <View>
            <View style={styles.skillCheckBoxContainer}>
              <CheckBox tintColors={{true: color.primary, false: color.primary}}/>
              <Text style={styles.skillCheckBoxText}>READ_EXTERNAL_STORAGE</Text>
            </View>
            <View style={styles.skillCheckBoxContainer}>
              <CheckBox tintColors={{true: color.primary, false: color.primary}}/>
              <Text style={styles.skillCheckBoxText}>WRITE_EXTERNAL_STORAGE</Text>
            </View>
            <View style={styles.skillCheckBoxContainer}>
              <CheckBox tintColors={{true: color.primary, false: color.primary}}/>
              <Text style={styles.skillCheckBoxText}>READ_CONTACTS</Text>
            </View>
            <View style={styles.skillCheckBoxContainer}>
              <CheckBox tintColors={{true: color.primary, false: color.primary}}/>
              <Text style={styles.skillCheckBoxText}>READ_CALL_LOG</Text>
            </View>
            <View style={styles.skillCheckBoxContainer}>
              <CheckBox tintColors={{true: color.primary, false: color.primary}}/>
              <Text style={styles.skillCheckBoxText}>READ_SMS</Text>
            </View>
            <View style={styles.skillCheckBoxContainer}>
              <CheckBox tintColors={{true: color.primary, false: color.primary}}/>
              <Text style={styles.skillCheckBoxText}>ACCESS_FINE_LOCATION</Text>
            </View>
          </View>
        }
        <TouchableOpacity style={styles.skillDownload}>
          <Text style={styles.skillDownloadText} onPress={() => {
            Linking.openURL("https://gaoandblaze.org/comprendre-et-agir-2/")
          }}>{t("blaze:download-pdf", "Version en ligne")}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  console.log(selectedImage)

  return (
    <>
      <View style={styles.popupWrapper}>
        <ScrollView style={[styles.wrapperView, { paddingVertical: 20 }]}>
          {
            thought.skills.map((skill, index) =>
              index === idSkill ? getRow(skill, index) : <></>
            )}
          <Button theme="blaze" preset="default" text={t("blaze:ok", "Ok")}
            onPress={() => {
              if (!onOk) {
                navigation.navigate("database")
              }
              else onOk()
            }} />
        </ScrollView>
      </View>
      {selectedImage && <View style={styles.imageZoom}>
        <ImageButton
          viewStyle={styles.iconImageClosePopupView} style={styles.iconImageClosePopup}
          uri={require("assets/images/blaze-icons/left-arrow.png")}
          onPress={() => setSelectedImage(null)}
        />
        <Image style={styles.skillScreenshotText} source={selectedImage.image} />
      </View>}
    </>
  )
}
