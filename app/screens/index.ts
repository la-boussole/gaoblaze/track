import packageScreens from "./utils"
import setup from "./full-gao-games"

export default packageScreens(setup)
// export * from "./blaze-01-amin-screen"
export * from "./blaze-02-masako-screen"
export * from "./special-live-event-screen"
export * from "./gao-welcoming-screen"
export * from "./gao-welcoming-username-screen"
