import * as React from "react"
import {View, Animated, Easing, TextInput, BackHandler} from "react-native"
import {BlazeContainer, Text, Button} from "../components"
import {color} from "../blaze-theme"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "../blaze-theme/styles"
import {useTranslation} from "react-i18next"
import {useState} from "react"
import {useStores} from "models/root-store"
import { PlayBack } from "components/sound/play-back"
import {DEPENDENCIES} from "models/character-profile";

export interface BlazeLoginScreenProps {
  navigation: NavigationScreenProp<{}>
}


export const BlazeLoginScreen: React.FunctionComponent<BlazeLoginScreenProps> = BlazeContainer((props) => {
  const passcodeTest= ["rio grande", "río grande", "rÍo grande", "ríogrande", "ríogrande", "rÍogrande"]
  const {t} = useTranslation()
  const {navigation} = props
  const [passcode, setPasscode] = useState("")
  const {metaGame, communityStore} = useStores()

  const animation = new Animated.Value(0)
  const interpolated = animation.interpolate({
    inputRange: [0, .5, 1, 1.5, 2, 2.5, 3],
    outputRange: [0, -15, 0, 15, 0, -15, 0]
  })
  const styleAnim = {
    marginHorizontal: 24, //FIXME: put me in blaz-theme/styles
    transform: [
      { translateX: interpolated}
    ]
  }

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    navigation.navigate("preHome")
    return true
  }

  const triggerAnimation = () => {
    animation.setValue(0)
    Animated.timing(animation, {
      duration:400,
      toValue: 3,
      ease: Easing.bounce,
      useNativeDriver: true
    }).start(() => setPasscode(""))
  }

  const handleChange = text => {
    if (passcodeTest.includes(passcode.toLowerCase())) {
      const dependency = DEPENDENCIES.find(item => item.id === "Alex-9")
      dependency.options.forEach(message => communityStore.fireDependency(message))
      metaGame.refreshChatTriggers(navigation, t)
      metaGame.loginBlaze()
      navigation.navigate("database")
    }
    else {
      triggerAnimation()
    }
  }
  return (
    <>
      <View style={styles.centeredView}>
        <Text style={styles.h1Text}>
          {t("common:blaze", "Blaze")}
        </Text>
      </View>
      <View>
        <TextInput
          style={styles.passcodeText}
          placeholderTextColor={color.primaryDarker}
          placeholder={t("common:passcode", "Passcode")}
          value={passcode}
          maxLength={20}
          onChangeText={text => {
            setPasscode(text)
          }
          }
        />
        <Button
          text={t("common:submit", "Envoyer")}
          onPress={handleChange}
          theme="gao"
          preset="developer"
        />
      </View>
      <View style={styles.spacerView}/>
    </>
  )
}, {isDebugMode: false})
