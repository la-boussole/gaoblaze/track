import { GameScreen } from "./game-screen.tsx"

export default {
  screens: {
    game: GameScreen,
  },
  exitRoutes: ["game"],
}
