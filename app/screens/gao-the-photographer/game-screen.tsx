import * as React from "react"
import {View, Image, NativeModules, PermissionsAndroid} from "react-native"
import {GaoDialog, Text, ImageButton, StateMachine, GaoContainer, GaoFrame} from "components"
import {useTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"
import {color} from "gao-theme"
import {launchCamera} from 'react-native-image-picker'
import {NavigationScreenProps} from "react-navigation"
import {useStores} from "models/root-store"
import {useEffect} from "react"
import {connectRootStore} from "utils/decorators"
import { PlayBack } from "components/sound/play-back"
import Toast from 'react-native-toast-message'

/* *** Constants to be fine tuned by game design *** */

// const COLOR_SENSIBILITY = {r: 0.2, g: 0.2, b: 0.2}; // TODO: Use me!

// test : colors to be asked for the pictures
const COLORS = [
  "yellow",
  "red",
  "blue",
  "green"
]

/* *** */

// FIXME: REWARD 30.

// 1. Start the game
const PhotographerGameStart = props => {
  const {t} = useTranslation()
  const {setState} = props
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("photographer:start",
          "Gao a besoin de quelques photos supplémentaires pour son album. L'aideras-tu ?",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={props.startGame}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// 2. Accept to share your phone pictures
const PhotographerGameIntro = props => {
  const {t} = useTranslation()
  const {setState} = props
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("photographer:welcome",
          "Prends une photo en suivant les instructions de Gao pour gagner des Papattes"
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={() => setState({dialog: "GamePhoto"}) }
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// 3. Game on : take a picture -> with question "I want a blue and yellow image"
const PhotographerGamePhoto = props => {
  const {t} = useTranslation()
  const {state} = props
  const {setState} = props
  const {metaGame} = useStores()
  

  useEffect(() => {

  })

  const takePicture = () => {
    launchCamera({includeBase64: true, quality: 0.5, mediaType: 'photo', saveToPhotos: true},
      (image) => {
        try {
          if(image && image.assets && image.assets[0]) {
            setState({imageSource: image.assets[0].uri})
          }
        } catch(error) {
          Toast.show({
            text1: t("photographer:photographer-error", "Problemas con el uso de tú camara (S.O.)"),
            type: "error",
            visibilityTime: 1500,
            position: "bottom",
            onShow: () => console.log("show")
          })
        } 
      }).catch(() => Toast.show({
        text1: t("photographer:photographer-error", "Problemas con el uso de tú camara (S.O.)"),
        type: "error",
        visibilityTime: 1500,
        position: "bottom",
        onShow: () => console.log("show")
      }));
    /*if (camera.current) {
      const options = {quality: 0.5, base64: true}
      // @ts-ignore
      const data = await camera.current.takePictureAsync(options)
      const uri = data.uri
      setState({imageSource: uri})
    }*/
  }

  return (
    <View style={styles.centerView}>
      <GaoDialog
        preset="advice"
        textDialog2={t("photographer:rules", "Je veux une image X1 et X2 !")
          .replace("X1", t("photographer:"+state.color1, state.color1))
          .replace("X2", t("photographer:"+state.color2, state.color2))
        }
        viewStyle={{top: "-30%", width: "125%", position: "absolute"}}
      />
      <View style={styles.cameraContainerGaoThePhotographer} onTouchEndCapture={() => takePicture()}>
        {
          !state.imageSource &&
          (<Text numberOfLines={2} style={{flex: 1, backgroundColor: "yellow", fontSize: 22, textAlign: "center", textAlignVertical: "center"}}>{t("photographer:choose", "Activate camera")}</Text>)
        }
        {
          state.imageSource &&
          (<Image source={{uri: state.imageSource}} style={styles.cameraGaoThePhotographer}/>)
        }
      </View>
      <View style={styles.containerPhotoButtonGaoThePhotographer}>
        {
          state.imageSource &&
          (
            <ImageButton
              uri={require("assets/images/common/wrong.png")}
              onPress={() => setState({imageSource: null})}
              style={styles.cameraPhotoButtonGaoThePhotographer}
            />
          )
        }
        {
          !state.imageSource &&
          (
            <ImageButton
              uri={require("assets/images/gao-the-photographer/logo_camera.png")}
              onPress={() => takePicture()}
              style={styles.cameraPhotoButtonGaoThePhotographer}
            />
          )
        }
        {
          state.imageSource &&
          (
            <ImageButton
              uri={require("assets/images/common/right.png")}
              onPress={() => setState({dialog: "GamePhotoEvaluation"})}
              style={styles.cameraPhotoButtonGaoThePhotographer}
            />
          )
        }
      </View>
    </View>
  )
}

// 5. Game on : picture took "Evaluating"
const PhotographerGamePhotoEvaluation = props => {
  const {t} = useTranslation()
  const {state} = props
  const {setState} = props
  const { metaGame } = useStores()
  const {navigation} = props

  React.useEffect(() => {
    NativeModules.TrackProjectMisc.percentageOfColor(state.imageSource, state.color1, state.color2).then(
      (result) => {
        if(result) {
          metaGame.reward(30)
          metaGame.playGame("ThePhotographer", navigation, t)
          setState({dialog: "GameSuccess"})
        } else {
          setState({dialog: "GameOver"})
        }
      }).catch((error) => console.error(error))
  })

  return (
    <>
      <View style={styles.centerView}>
        <GaoDialog
          preset="advice"
          textDialog2={t("photographer:evaluating", "En coours d’évaluation…Gao est un peu lent !")}
          viewStyle={{top: "-30%", position: "absolute"}}
          textStyle2={{fontSize: 17}}
        />
        <View style={styles.cameraContainerGaoThePhotographer}>
          <Image source={{uri: state.imageSource}} style={styles.cameraGaoThePhotographer}/>
        </View>
      </View>
    </>
  )
}

// 6. Evaluation Not Ok
const PhotographerGameAnswerWrong = props => {
  const {t} = useTranslation()
  const {state} = props
  return (
    <>
      <Image source={state.imageSource} style={props.frame}/>
      <View style={{...props.frame, backgroundColor: color.dim}}>
        <Text style={styles.messageText}>{t("photographer:not-enough", "Not enough ...")}</Text>
      </View>
      <View style={styles.spacerView}></View>
    </>
  )
}

// 7. GameOver
const PhotographerGameOver = props => {
  const {state} = props
  const {setState} = props
  const {t} = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("ThePhotographer", navigation, t)
  })

  return (
    <>
      <Image source={state.imageSource} style={props.frame}/>
      <View style={styles.containerView}>
        <GaoDialog
          textDialog1={t("common:gameover", "TU AS PERDU !")}
          textButton={t("common:retry", "Réessaye")}
          dialogViewStyle={{marginHorizontal: "-35%"}}
          textStyle1={{color: "#EB358A"}}
          uriImage={require("assets/images/common/sad_emoji.png")}
          buttonOnPress={() => {
            setState({imageSource: null})
            state.imageSource = null
            setState({dialog: "GamePhoto"})
          }}
        />
      </View>
    </>
  )
}

const PhotographerPermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// 8. Evaluation OK // Use new component FilterImage with textFilter="Well Done!"
const PhotographerGameAnswerRight = props => {
  const {state} = props
  const {t} = useTranslation()
  return (
    <>
      <Image source={state.imageSource} style={props.frame}/>
      <View style={styles.containerView}>
        <Text style={styles.messageText}>{t("photographer:well-done", "Well Done!")}</Text>
      </View>
    </>
  )
}

// 9. Win the game !! // FIXME: positionning problem
const PhotographerGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerView}>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      <GaoDialog
        preset="score"
        textMoney={30}
        navigation={props.navigation}
        uriImage={require("assets/images/common/wink_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

type PhotographerDialog =
  "GameIntro" |
  "GameStart" |
  "GamePhoto" |
  "GamePhotoEvaluation" |
  "GameAnswerWrong" |
  "GameAnswerRight" |
  "GameSuccess" |
  "GameOver" |
  "GamePermissionDenied"

/*
 *** What the PROGRAM do: ***
 - ask access to the player to take photos
 - ask a photo with containing two main colors
 - evaluate the photo
 - check if the photo took is good  -> WIN
 - loose if not
 */

type GameScreenState = {
  dialog: PhotographerDialog
  imageSource: any
  color1: string,
  color2: string
}

export interface GameScreenProps extends NavigationScreenProps<{}> {
}

@connectRootStore
export class GameScreen extends React.Component<GameScreenProps, GameScreenState> {
  constructor(props) {
    super(props)

    let index1 = Math.round(Math.random() * 1000) % COLORS.length
    const index2 = Math.round(Math.random() * 1000) % COLORS.length

    if(index1==index2) {
      index1 = (index1 + 1) % COLORS.length
    }

    this.state = {
      dialog: "GameStart",
      color1: COLORS[index1],
      color2: COLORS[index2],
      imageSource: null,
    }

    this.startGame = this.startGame.bind(this)
  }

  async askPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          dialog: "GameIntro"
        })
      } else {
        this.setState({dialog: "GamePermissionDenied"})
      }
    } catch (err) {
      //console.warn(err)
    }
  }

  startGame() {
    this.askPermission()
  }

  render() {
    this.props.rootStore.setSong("photographer.ogg", this.props.rootStore.gameMusic)
    const PhotographerStateMachine = StateMachine({initialState: this.state})
    const PhotographerFrame = GaoFrame({
      wallpaper: require("assets/images/wallpaper/gao_photographer.png"),
      coords: [143, 336, 610 - 143, 1176 - 336],
    })
    const PhotographerScreen = PhotographerFrame(props =>
      <PhotographerStateMachine dialogs={{
        "GameIntro": PhotographerGameIntro,
        "GameStart": PhotographerGameStart,
        "GamePhoto": PhotographerGamePhoto,
        "GamePhotoEvaluation": PhotographerGamePhotoEvaluation,
        "GameAnswerWrong": PhotographerGameAnswerWrong,
        "GameAnswerRight": PhotographerGameAnswerRight,
        "GameSuccess": PhotographerGameSuccess,
        "GameOver": PhotographerGameOver,
        "GamePermissionDenied": PhotographerPermissionDenied,
      }} {...props} startGame={this.startGame}
      />
    )
    return (
      <>
        <GaoContainer {...this.props} character={require("assets/images/download_gifs/gao-Gao_the_photographer.gif")}>
          <PhotographerScreen navigation={this.props.navigation}/>
        </GaoContainer>
        <View style={{
          position: "absolute",
          bottom: 0,
          width: "100%",
          alignItems: "center",
          marginBottom: 60,
        }}>
        </View>
        <Toast ref={(ref) => Toast.setRef(ref)} />
      </>
    )
  }
}
