import * as React from "react"
import {View} from "react-native"
import { NavigationScreenProps } from "react-navigation"
import {GaoDialog, Text, StateMachine, GaoContainer, GaoFrame, Timer} from "components"
import { withTranslation, useTranslation } from "react-i18next"
import { observer } from "mobx-react"
import * as styles from "gao-theme/styles"
import { connectRootStore } from "utils/decorators"
import {useStores} from "models/root-store"
import {useEffect, useRef, useState} from "react"
import { PlayBack } from "components/sound/play-back"
import {PERMISSIONS, request, RESULTS} from "react-native-permissions"

/* *** Constants to be fine tuned by game design *** */

const RHYTHM_CHECKER_HEARTBEAT = 1500 // 1.5 s.
const GOAL = 50

/* *** */

const AthleteGameUnsupported = () => {
  const { t } = useTranslation()
  return (
    <GaoDialog
      textDialog2={t("common:unsupported", "This game is not supported on your device :( !")}
    />
  )
}

// 1.Game rules
const AthleteGameStart = props => {
  const { t } = useTranslation()
  const { setState } = props

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "athlete:start",
          "Gao veut faire la course avec toi ! Accepteras-tu de relever son défi ?",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => setState({ dialog: "GameIntro" })}
      />
    </View>
  )
}

// 2.Ask Permissions
const AthleteGameIntro = props => {
  const { t } = useTranslation()
  const { setState } = props
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "athlete:welcome",
          "Cours le nombre indiqué de pas avant que Gao ne le fasse pour gagner des papattes !",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={() => setState({ dialog: "GameExtraStep" })}
      />
    </View>
  )
}

const AthleteGameExtraStep = props => {
  const { t } = useTranslation()
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "athlete:extra",
          "Cours en bougeant ton téléphone de haut en bas.",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={props.startCountdown}
      />
    </View>
  )
}

const AthleteGameAnswer = props => {
  const { t } = useTranslation()
  const { setState, state } = props
  const {metaGame} = useStores()
  const [gaoSteps, setGaoSteps] = useState(0)
  const [mySteps, setMySteps] = useState(0)
  const [inGame, setInGame] = useState(false)
  const [initialAceleration, setInitialAcceleration] = useState({steps: -1, distance: -1})
  const {navigation} = props
  const [data, setData] = React.useState({})
  const  division = metaGame.gamesPlayed.get("TheAthlete")?metaGame.gamesPlayed.get("TheAthlete").playCount + 2:2

  useEffect(() => {
    setInGame(false)
    const now = new Date()
    const after = new Date()
    now.setHours(0,0,0,0)
    after.setHours(now.getHours() + 1)
  }, [])

  return (
    <>
      {!inGame && (
        <View style={styles.containerView}>
          <Timer textStyle={styles.countdownTextForger} playerGivenTime={3} showGo={true} removeSound={true}
            finishTimer={() => {
              setInGame(true)
            }} textType="normal"/>
        </View>
      )}
      {inGame && (
        <>
          <View style={{position: "absolute", width: "100%", height: "100%", zIndex: 1000}} onTouchEndCapture={() => {
            setMySteps(mySteps + 1)
          }} />
          <View style={styles.containerView}>
            <Timer playerGivenTime={100} textStyle={{display: "none"}} finishTimer={() => {}} change={(timer, refTimer) => {
              const newSteps = gaoSteps + 1
              setGaoSteps(newSteps)
              if(mySteps/division >= GOAL) {
                refTimer.stop()
                metaGame.reward(30)
                metaGame.playGame("TheAthlete", navigation, t)
                setState({
                  dialog: "GameSuccess",
                })
              }
              if (newSteps >= GOAL && mySteps/division < GOAL) {
                refTimer.stop()
                setGaoSteps(Math.min(GOAL, newSteps))
                setState({
                  dialog: "GameOver",
                })
              }
            }} />
            <Text style={styles.gaoStepsText}>{t("athlete:your-steps", "YOUR STEPS")}</Text>
            <Text style={styles.playerStepsBlueText}>{Math.min(Math.floor(mySteps/division), GOAL)}/{GOAL}</Text>
            <Text style={styles.gaoStepsText}>{t("athlete:gao-steps", "GAO STEPS")}</Text>
            <Text style={styles.playerStepsRedText}>{gaoSteps}/{GOAL}</Text>
            {/*TODO: if steps >= 50  color == Silver -> see TextScoreStyle2 */}
            {/*TODO: if steps >= 100  color == Gold -> create TextScoreStyle3 */}
          </View>
        </>
      )}
    </>
  )
}

const AthleteGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerView}>
      {/* FIXME */}
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      <GaoDialog
        preset="score"
        textMoney={30}
        navigation={props.navigation}
        uriImage={require("assets/images/common/wink_emoji.png")} // FIXME: add image
        // FIXME: change rewards for game design
      />
    </View>
  )
}

// 6. Lose ???? (no mockup)
const AthleteGameOver = props => {
  const { t } = useTranslation()
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheAthlete", navigation, t)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog1={t("common:gameover", "TU AS PERDU !")}
        textStyle1={styles.colorFontGameOverText}
        uriImage={require("assets/images/common/sad_emoji.png")}
        textButton={t("common:retry", "Réessaye")}
        buttonOnPress={props.startCountdown}
      />
    </View>
  )
}

const AthletePermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
      />
    </View>
  )
}

const WALLPAPERS = {
  GameIntro: require("assets/images/wallpaper/gao_athlete_1.png"),
  GameStart: require("assets/images/wallpaper/gao_athlete_1bis.png"),
  GameCountDown: require("assets/images/wallpaper/gao_athlete_1.png"),
  GameAnswer: require("assets/images/wallpaper/gao_athlete_2.png"),
  GameSuccess: require("assets/images/wallpaper/gao_athlete.png"),
  GameOver: require("assets/images/wallpaper/gao_athlete.png"),
}

/* *** */

type AthleteDialog =
  "GameIntro" |
  "GameStart" |
  "GameCountDown" |
  "GameSuccess" |
  "GameAnswer" |
  "GameOver" |
  "GameUnsupported" |
  "GamePermissionDenied" |
  "GameExtraStep"

type GameScreenState = {
  dialog: AthleteDialog
}

export interface GameScreenProps extends NavigationScreenProps<{}> {}

@withTranslation()
@connectRootStore
@observer
export class GameScreen extends React.Component<GameScreenProps, GameScreenState> {

  rootStore = null;

  constructor(props) {
    super(props)
    this.rootStore = props.rootStore
    this.state = {
      dialog: "GameStart",
    }
    this.startCountdown = this.startCountdown.bind(this)
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  startCountdown = () => {
    this.setState({ dialog: "GameAnswer" })
  }

  render() {
    this.props.rootStore.setSong("athlete.ogg", this.props.rootStore.gameMusic)
    const AthleteStateMachine = StateMachine({initialState: this.state})
    /* FIXME: change background for screen 3->6 with 'gao_filekeeper2' */
    const AthelteFrame = GaoFrame({
      wallpaper: require("assets/images/download_gifs/gao_Gao_the_Athlete_Half.gif"),
      coords: [20, 150, 750-20, 1182-300],
    })
    const AthleteScreen = AthelteFrame(props => (
      <AthleteStateMachine dialogs={{
        "GameIntro": AthleteGameIntro,
        "GameStart": AthleteGameStart,
        "GameSuccess": AthleteGameSuccess,
        "GameAnswer": AthleteGameAnswer,
        "GameOver": AthleteGameOver,
        "GameUnsupported": AthleteGameUnsupported,
        "GameExtraStep": AthleteGameExtraStep,
        "GamePermissionDenied": AthletePermissionDenied
      }} {...props}
      startCountdown={this.startCountdown}
      />
    ))

    return (
      <>
        <GaoContainer disableOptions={this.state.dialog === "GameAnswer"} {...this.props} >
          {
          //<Wallpaper backgroundImage={require("assets/images/wallpaper/gao_athlete.png")} preset={"cover"} />
          }
          <AthleteScreen navigation={this.props.navigation} />
        </GaoContainer>
      </>
    )
  }
}

