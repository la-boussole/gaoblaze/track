import DeveloperScreen from "screens/developer-screen"
import { GameScreen as GaoTheIntruderScreen } from "screens/gao-the-intruder/game-screen"
import { GameScreen as GaoTheAthleteScreen } from "screens/gao-the-athlete/game-screen"
import { GameScreen as GaoTheFilekeeperScreen } from "screens/gao-the-filekeeper/game-screen"
import { GameScreen as GaoThePhotographerScreen } from "screens/gao-the-photographer/game-screen"
import { GameScreen as GaoTheSageScreen } from "screens/gao-the-sage/game-screen"
import { GameScreen as GaoTheSpyScreen } from "screens/gao-the-spy/game-screen"
import { GameScreen as GaoTheForgerScreen } from "screens/gao-the-forger/game-screen"
import { GameScreen as GaoTheProsecutorScreen } from "screens/gao-the-prosecutor/game-screen"
import { GameScreen as GaoTheVagabondScreen } from "screens/gao-the-vagabond/game-screen"
import { TransitionPresets } from "react-navigation-stack"
import { GaoWelcomingScreen } from "screens/gao-welcoming-screen"
import { GaoBlazeAnnoucementScreen } from "screens/gao-blaze-annoucement-screen"
import { SpecialLiveEventScreen} from "screens/special-live-event-screen"
import { GaoWelcomingUsernameScreen } from "screens/gao-welcoming-username-screen"
import { Gao01AlexScreen } from "screens/gao-01-alex-screen"
import {PreHomeScreen} from "screens/gao-pre-home-screen"
import {AlexSpecialLiveEventScreen} from "screens/alex-special-live-event-screen"
import {GaoWelcomingLanguageScreen} from "screens/gao-welcoming-language-screen";

export default {
  screens: {
    gaoWelcomingLanguage: GaoWelcomingLanguageScreen,
    gaoWelcomingUsername: GaoWelcomingUsernameScreen,
    gaoWelcoming: GaoWelcomingScreen,
    gaoBlazeAnnoucement: GaoBlazeAnnoucementScreen,
    specialLiveEvent: SpecialLiveEventScreen,
    alexSpecialLiveEvent: AlexSpecialLiveEventScreen,
    alex_animation: Gao01AlexScreen,
    preHome: PreHomeScreen,
    developer: DeveloperScreen,
    gao_the_intruder: GaoTheIntruderScreen,
    gao_the_athlete: GaoTheAthleteScreen,
    gao_the_filekeeper: GaoTheFilekeeperScreen,
    gao_the_photographer: GaoThePhotographerScreen,
    gao_the_sage: GaoTheSageScreen,
    gao_the_spy: GaoTheSpyScreen,
    gao_the_forger: GaoTheForgerScreen,
    gao_the_prosecutor: GaoTheProsecutorScreen,
    gao_the_vagabond: GaoTheVagabondScreen
  },
  exitRoutes: ["preHome"],
  meta: {
    defaultNavigationOptions: {
      ...TransitionPresets.RevealFromBottomAndroid,
      cardStyle: {
        backgroundColor: "transparent"
      }
    }
  }
}
