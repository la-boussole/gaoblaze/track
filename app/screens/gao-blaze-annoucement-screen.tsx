import * as React from "react"
import { observer } from "mobx-react-lite"
import { GaoDialog, GaoCharacter, Wallpaper } from "../components"
import { NavigationScreenProp } from "react-navigation"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { View } from "react-native"
import { useStores } from "models/root-store"

export interface GaoBlazeAnnoucementScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const GaoBlazeAnnoucementScreen: React.FunctionComponent<GaoBlazeAnnoucementScreenProps> = observer((props) => {
  const { t } = useTranslation()
  const { navigation } = props
  const rootStore = useStores()

  // FIXME: should be something like that : (TODO: test it)
  // if (gamePlay.playCount < 5)
  //   return null
  return (
    <View style={styles.wrapperColumnView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/HomescreenWelcome.png")} />
      <View style={styles.wrapperView}>{/* FIXME: In order to fill the screen space */}</View>
      <GaoDialog
        textDialog2={t(
          "common:annoucement",
          "Salut ! Juste pour vous dire que Gao Games a été racheté par une Grande maison d'édition. Afin de fêter ce succès, j'ai développé un nouveau jeu Gao, jetez-y un coup d'oeil ! \n",
        )}
        textStyle2={styles.infoText}
        textButton={t("common:ok", "Ok")}
        buttonOnPress={() => {
          rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
          rootStore.metaGame.setShowGaoTheVagabond()
          navigation.navigate("preHome")
        }}
      />
      <GaoCharacter
        positioning={{paddingLeft: 10}}
        uriImage={require("assets/images/character/blaze.png")}
        imageStyle={styles.avatarBlazeWelcomeImage}
        displayBottom= {true}
      />
    </View>
  )
})
