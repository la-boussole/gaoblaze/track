import * as React from "react"
import { NavigationScreenProp } from "react-navigation"
import { useTranslation } from "react-i18next"
import Swiper from "react-native-swiper"
import { View, Text, BackHandler } from "react-native"
import { HomeScreen } from "screens/gao-home-screen"
import { PlayScreen } from "screens/gao-play-screen"
import { TradePawsScreen } from "screens/gao-trade-paws-screen"
import { MemesScreen } from "screens/gao-memes-screen"
import { GifsScreen } from "screens/gao-gifs-screen"
import { EmojisScreen } from "screens/gao-emojis-screen"
import { WallpapersScreen } from "screens/gao-wallpapers-screen"
import { useCallback, useEffect, useReducer, useRef } from "react"
import { useStores } from "models/root-store"
import { PlayBack } from "components/sound/play-back"
import { useObserver } from "mobx-react"

export interface PreHomeScreenProps extends NavigationScreenProp<{}> {
}

export const PreHomeScreen: React.FunctionComponent<PreHomeScreenProps> = (props) => {
  const rootStore = useStores()
  const { t } = useTranslation()
  const { navigation } = props
  const [trade, setTrade] = React.useState(0)
  const swiper = React.useRef()
  const { communityStore, metaGame } = rootStore
  const refSound = useRef()
  const [opacity, setOpacity] = React.useState(0)

  const goBack = () => {
    if (navigation.isFocused()) {
      if (swiper.current) {
        const index = swiper.current.state.index
        if (index === 0)
          swiper.current.scrollBy(1, true)
        if (index === 2 || index === 3)
          swiper.current.scrollBy(-1, true)
      }
      return true
    }
    return false
  }

  useEffect(() => {
    console.log("Use effect prehome")
    setTimeout(() => setOpacity(1), 500)
    BackHandler.addEventListener("hardwareBackPress", goBack)
    return () => {
      BackHandler.removeEventListener("hardwareBackPress", goBack)
    }
  }, [])

  return useObserver(() => (
    <>
      <Swiper ref={swiper} showsButtons={false} showsPagination={false} index={1} removeClippedSubviews={false}
        scrollEnabled={false} style={{ opacity: opacity }}>
        <View style={{ height: "100%" }}>
          <PlayScreen {...props} onGoBack={goBack} onOpenGame={() => {
            if (refSound.current) {
              refSound.current.stop()
            }
          }} />
        </View>
        <View style={{ height: "100%" }} >
          <HomeScreen {...props}
            soundRef={refSound}
            onPlay={() => {
              if (swiper.current) {
                // @ts-ignore
                swiper.current.scrollBy(-1, true)
              }
            }}
            onTradePaws={() => {
              if (swiper.current) {
                // @ts-ignore
                swiper.current.scrollBy(1, true)
              }
            }} onGoBack={goBack}
          />
        </View>
        <View style={{ height: "100%" }}>
          <TradePawsScreen {...props} onOptionStart={(item) => {
            setTrade(item)
            if (swiper.current) {
              // @ts-ignore
              swiper.current.scrollBy(1, true)
            }
          }}
          onGoBack={goBack}
          />
        </View>
        <View style={{ height: "100%" }}>
          {trade == 0 && <MemesScreen {...props} onGoBack={goBack} />}
          {trade == 3 && <GifsScreen {...props} onGoBack={goBack} />}
          {trade == 2 && <EmojisScreen {...props} onGoBack={goBack} />}
          {trade == 1 && <WallpapersScreen {...props} onGoBack={goBack} />}
        </View>
      </Swiper>
    </>
  )
  )
}
