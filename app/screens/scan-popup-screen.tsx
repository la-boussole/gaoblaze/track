import React, { useEffect } from "react"
import { Text } from "components"
import { BackHandler, Image, TouchableOpacity, View } from "react-native"
import * as styles from "gao-theme/styles"
import { useTranslation } from "react-i18next"
import { useStores } from "models/root-store"
import { color } from "blaze-theme"

export interface ScanPopupScreenProps {
  onClose: () => void
}

const IMAGE_LETTERS = ["A", "B", "C", "D", "E"]

export const sendInformationToAirTable = async (metaGame, idGamer, addScan) => {
  const history = addScan?metaGame.addScanHistory():metaGame.historyScan
  const values = {
    scanAverage: history.map(item => IMAGE_LETTERS[item.scanNumber]).join("\n"),
    scanDate: history.map(item => new Date(item.date).toLocaleDateString()).join("\n"),
    scanNumber: history.map(item => item.scanValue.toFixed(2) + "").join("\n"),
  }
  metaGame.questionary.forEach(item => {
    values[item.key] = item.value
  })
  //console.log("Values scan", values)
  return await fetch("https://api.airtable.com/v0/appcSfcHamKDI0Wwf/Gao&Blaze",
    {
      method: idGamer.length > 0 ? "PATCH" : "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer key3Q289ve7suZ75K"
      },
      body: idGamer.length > 0 ? JSON.stringify({ records: [{ id: idGamer, fields: values }] }) : JSON.stringify({ records: [{ fields: values }] })
    })
}

export const ScanPopupScreen: React.FunctionComponent<ScanPopupScreenProps> = (props) => {
  const rootStore = useStores()
  const [idGamer, setIdGamer] = React.useState(rootStore.idGamer)
  const {metaGame} = useStores()
  const {t} = useTranslation()

  return (
    <View style={{backgroundColor: color.scanBackgroundLoading, position: "absolute", width: "100%", height: "100%"}}>
      <View style={styles.mainDialogScoreView}>
        <View style={{backgroundColor: color.palette.black, width: "80%", height: 300, justifyContent: "center", borderRadius: 10, borderColor: color.primaryDarker, borderWidth: 3, alignItems: "center",}}>
          <Text style={[styles.defaultSecondaryTextLastMessage, {color: color.primary}]}>{t("blaze:scan-popup-text","Acceptez-vous de partager votre note globale pour notre projet? Elle restera totalement anonyme et servira uniquement pour nos statistiques.")}</Text>
          <TouchableOpacity style={{backgroundColor: color.primaryDarker, width: "80%", borderRadius: 5, margin: 5}} onPress={() => {
            sendInformationToAirTable(metaGame, idGamer, true).then(response => response.json().then(json => {
              if(idGamer.length === 0) {
                //console.log(response)
                setIdGamer(json.records[0].id)
                rootStore.setIdGamer(json.records[0].id)
              }
              rootStore.setSendDataExodusAccept()
              props.onClose()
            }
            )).catch(error => console.log(error))
          }}>
            <Text style={[styles.defaultSecondaryText, {color: color.palette.black}]}>{t("blaze:scan-popup-yes","Oui")}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: color.primaryDarker, width: "80%", borderRadius: 5, margin: 5}} onPress={() => props.onClose()}>
            <Text style={[styles.defaultSecondaryText, {color: color.palette.black}]}>{t("blaze:scan-popup-no","Non")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
