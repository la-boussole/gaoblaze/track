import packageScreens from "screens/utils"
import ExploreScreen from "./explore"
import ChatScreen from "./chat"
import FriendsScreen from "./friends"
import ProfileScreen from "./profile"
import MyProfileScreen from "./my_profile"
import SettingsScreen from "./settings"
import GaoCelebrationScreen from "./gao_celebration"
import { BlazeAppDownloadScreen } from "screens/blaze-app-download-screen"
import { BlazeLastMessagePopup } from "screens/blaze-last-message-popup"
import {ScanPopupScreen} from "screens/scan-popup-screen";
/*
const SocialStack = createStackNavigator({
  friends: FriendsScreen,
  explore: ExploreScreen,
  profile: ProfileScreen,
  my_profile: MyProfileScreen,
  gao_celebration: GaoCelebrationScreen
})
*/

export default packageScreens({
  screens: {
    friends: FriendsScreen,
    chat: ChatScreen,
    explore: ExploreScreen,
    profile: ProfileScreen,
    settings: SettingsScreen,
    my_profile: MyProfileScreen,
    gao_celebration: GaoCelebrationScreen,
    blazeAppDownload: BlazeAppDownloadScreen,
    blazeLastMessage: BlazeLastMessagePopup
  },
  meta: {
    defaultNavigationOptions: {
      cardOverlayEnabled: true,
      cardStyle: {
        backgroundColor: "transparent",
      }
    }
  }
})
