import * as React from "react"
import {BackHandler, Text, View} from "react-native"
import { NavigationActions, NavigationScreenProps } from "react-navigation"
import { CommunityProfile, GaoCommunity } from "components"
import { useTranslation } from "react-i18next"
import { useEffect } from "react"
import { useStores } from "models/root-store"
import membersData from "assets/community_members/profile"
import { array } from "mobx-state-tree/dist/internal"

export interface ProfileScreenProps extends NavigationScreenProps<{}> {}

// FIXME: add types for props.
function ProfileItem(props) {
  const { item, onClick } = props
  return (
    <View style={item.container}>
      {/* Avatar */}
      <Text>{item.name}</Text>
      {/* Confidence Rating */}
    </View>
  )
}

export default (props: ProfileScreenProps) => {
  const { navigation } = props
  const { t } = useTranslation()
  const character = navigation.getParam("character")
  const members = membersData[character]

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", () => {
      navigation.navigate("chat", {character})
      return true
    })
    return () => {
      handler.remove()
    }
  }
  )

  console.log("......", members.id)

  return (
    <CommunityProfile
      goBack={() => navigation.navigate("chat", {character})}
      avatar={members.avatar?members.avatar: require("assets/images/community/avatars/undefined.png")}
      name={character}
      favoriteGame={members.favoriteGame}
      about={t("common:about-" + members.id.toLowerCase(), members.about)}
      showNotif={false}
    />
  )
}
