import * as React from "react"
import { Text, View, ScrollView, FlatList, Image } from "react-native"
import { NavigationActions, NavigationScreenProps } from "react-navigation"
import { CommunityProfile, GaoCommunity, ChatMessage, ImageButton, StoryChoices } from "components"
import * as styles from "gao-theme/styles"
import { useTranslation } from "react-i18next"
import { memo, useEffect, useRef, useState } from "react"
import { useStores } from "models/root-store"
import { observer, useLocalStore } from "mobx-react-lite"
import {color, typography} from "gao-theme"
import {AlexVsTheInternetEvent} from "screens/alex-vs-the-internet-screen"
import {chatCelebrationHeaderView} from "gao-theme/styles";
import i18n from "i18next"

export interface GaoCelebrationScreenProps extends NavigationScreenProps<{}> {
}

const ChatItem = (props) => {
  const { item, chatStore, changeSize } = props
  const [showName, setShowName] = useState(false)

  const isActive = item.identifier === chatStore.currentMessage.identifier

  return (
    <ChatMessage
      speed={25}
      key={item.identifier}
      message={{ type: "text", text: item.message }}
      dynamic={isActive}
      onShown={isActive ? () => {{
        chatStore.advance(true)
        setShowName(true)
        //changeSize()
      }} : undefined}
      writerName={(item.isMe) ? "me" : item.playerName}
      showName={showName}
      highlight={item.isAlex}
      textStyle={{fontSize: 18, backgroundColor: item.isMe ? null : item.isAlex ? color.primary :color.background, fontStyle: item.isItalic?"italic":"normal"}}
    />
  )
}

export default observer(props => {
  const { navigation } = props
  const { t } = useTranslation()
  const { metaGame } = useStores()
  const { userName } = metaGame
  const flatListRef = useRef(null)
  const rootStore = useStores()

  const preFuckedUpDialog = (i18n.language === "es" ? require("assets/special-event/dialog-pre-fucked-up-es.json") :
    i18n.language === "en" ? require("assets/special-event/dialog-pre-fucked-up-en.json") :
      require("assets/special-event/dialog-pre-fucked-up")).map((item, index) => ({
    ...item,
    identifier: index,
  }))

  const postFuckedUpDialog = (i18n.language === "es" ? require("assets/special-event/dialog-post-fucked-up-es.json") :
    i18n.language === "en" ? require("assets/special-event/dialog-post-fucked-up-en.json") :
      require("assets/special-event/dialog-post-fucked-up.json")).map((item, index) => ({
    ...item,
    identifier: preFuckedUpDialog.length + index,
  }))

  const [imageLoaded, setImageLoaded] = useState(false)


  useEffect(() => {
    rootStore.setSong("error.song", rootStore.soundMusic)
  }, [])

  const chatStore = useLocalStore(source => ({
    preFuckedUpList: preFuckedUpDialog,
    postFuckedUpList: postFuckedUpDialog,
    currentPointer: 0,
    currentChoices: [],
    get messages() {
      // compute using current pointer
      // if cp < len(preFuckedUpList)
      const epsilon = chatStore.currentMessage.isMe ? 0 : 1
      if (chatStore.currentPointer < chatStore.preFuckedUpList.length) {
        return chatStore.preFuckedUpList.slice(0, chatStore.currentPointer + 1)
      } else {
        return [].concat(...chatStore.preFuckedUpList,
          ...chatStore.postFuckedUpList.slice(0, chatStore.currentPointer - chatStore.preFuckedUpList.length + epsilon))
      }
    },
    get currentMessage() {
      if (chatStore.currentPointer < chatStore.preFuckedUpList.length) {
        return chatStore.preFuckedUpList[chatStore.currentPointer]
      } else {
        return chatStore.postFuckedUpList[chatStore.currentPointer - chatStore.preFuckedUpList.length]
      }
    },
    get hasFuckedUp() {
      return chatStore.currentPointer >= chatStore.preFuckedUpList.length
    },
    choose(choice) {
      if(choice.show)
        chatStore.currentMessage.message = choice.displayed
      else
        chatStore.currentMessage.message = ""
      chatStore.advance(false)
    },
    advance(onShown) {
      if (chatStore.currentPointer >= chatStore.preFuckedUpList.length + chatStore.postFuckedUpList.length - 1) {
        setTimeout(() => chatStore.currentPointer++, 4000)
        return
      }

      if (chatStore.currentChoices.length > 0 && onShown) {
        return
      }
      chatStore.currentPointer++
      // Update choice marker.
      if (chatStore.currentMessage.isMe) {
        chatStore.currentChoices = chatStore.currentMessage.message
      } else {
        chatStore.currentChoices = []
      }
    },
  }))

  /* Stuff to use the right animation of blazeRoom based on hour of the day */

  const dayInterval = () => {
    const actualTime = new Date().getHours()
    if (actualTime <=14 && actualTime >= 6)
      return "day"
    if (actualTime <=21 && actualTime > 14)
      return "afternoon"
    else
      return "night"
  }
  const BLAZE_ROOM = {
    day:       require("assets/images/download_gifs/Blaze-room-animation-day.gif"),
    afternoon: require("assets/images/download_gifs/Blaze-room-animation-afternoon.gif"),
    night:     require("assets/images/download_gifs/Blaze-room-animation-night.gif")
  }
  /* *** */

  // Use local store to replicate the chat system.
  // (1) the local store initialize with the contents.
  // (2) it provides the "post fucked up" marker.
  // (3) it maintains a pointer on the current message.
  // (4) it can be a lot simpler as we don't have a graph, but just a linear list.


  return (
    <>
      {
        chatStore.currentPointer >= chatStore.postFuckedUpList.length + chatStore.preFuckedUpList.length && (
          <AlexVsTheInternetEvent navigation={navigation} />
        )
      }
      {
        chatStore.currentPointer < chatStore.postFuckedUpList.length + chatStore.preFuckedUpList.length && (<View style={styles.mainCommunityView}>
          <View style={[styles.mainDialogContainerView, {paddingVertical: 20}]}>
            <View style={[styles.viewFlagView, {top: 25}]}>
              <Image source={require("assets/images/community/flag.png")}
                style={[styles.flagDialogCelebration, {zIndex: 10}]}/>
              <Text numberOfLines={2} style={[styles.titleFlagText, {
                zIndex: 10,
                marginTop: 12,
                position: "absolute",
                fontSize: 16,
              }]}>{t("common:stream", "Gao Celebrates!").toUpperCase()}</Text>
            </View>
            <ImageButton
              uri={require("assets/images/community/cross_icon.png")}
              style={styles.iconImageView}
              onPress={() => navigation.navigate("developer_screen")}
            />
            <View style={styles.chatContainerView}>
              <View style={styles.chatCelebrationHeaderView}>
                <View style={[styles.chatButtonGroupLiveStream, {borderWidth: imageLoaded?3:0}]}>
                  {chatStore.hasFuckedUp && <Image
                    source={
                      BLAZE_ROOM[dayInterval()]
                    }
                    style={styles.blazeRoom}
                    onLoad={() => setImageLoaded(true)}
                  />}
                </View>
                {!imageLoaded && (
                  <View style={{position: "absolute", width: "90%", height: "100%", justifyContent: "center", backgroundColor: color.palette.greyBlue}}>
                    <Text style={{alignSelf: "center", fontFamily: typography.primary, fontSize: 18}}>Loading live stream</Text>
                  </View>)}
              </View>
              <FlatList
                ref={flatListRef}
                keyExtractor={(item, index) => item.identifier.toString()}
                renderItem={({item}) => {
                  if(item.message === "")
                    return (<></>)
                  return (<ChatItem key={item.identifier} item={item} chatStore={chatStore}
                    changeSize={() => flatListRef.current.scrollToEnd()}/>)
                }}
                data={chatStore.messages}
                vertical
                style={styles.chatMessagesView}
                //ListFooterComponent={<View style={{height:15}}></View>}
                contentContainerStyle={{paddingBottom: 50}}
                onContentSizeChange={() => flatListRef.current.scrollToEnd()}
              />
              <StoryChoices
                canChoose={chatStore.currentChoices.length > 0}
                choices={chatStore.currentChoices}
                onChoose={chatStore.choose}
                onResize={() => {
                  flatListRef.current.scrollToEnd()
                }}
                textStyle={styles.chatStoryChoice}
              />
            </View>
          </View>
        </View>)
      }
    </>
  )
})
