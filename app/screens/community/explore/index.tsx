import * as React from "react"
import {Image, ImageSourcePropType, ScrollView, Text, View, TouchableOpacity, BackHandler} from "react-native"
import { NavigationActions, NavigationSwitchProp } from "react-navigation"
import { GaoCommunity, ImageButton } from "components"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import membersData from "assets/community_members/profile"
import { useStores } from "models/root-store"
import {useEffect} from "react"
import OtherProfiles from "assets/community_members/other-profiles"

export interface ExploreCommunityScreenProps extends NavigationSwitchProp<{}> {
}

export default (props: ExploreCommunityScreenProps) => {
  const { navigation } = props
  const {t} = useTranslation()
  const { communityStore } = useStores()
  const { metaGame } = useStores()

  useEffect(() => {

    const handler = BackHandler.addEventListener("hardwareBackPress", () => {
      navigation.dispatch(NavigationActions.back())
      return true
    })
    return () => {
      handler.remove()
    }
  }
  )

  return (
    <GaoCommunity
      navigate={navigation.navigate}
      dismiss={() => {
        navigation.dispatch(NavigationActions.back())
      }}
      currentTab="social"
    >
      <View style={styles.communityContainerView}>
        <View style={styles.communityScreenflexRowView}>
          <Text style={styles.h1TextNoPaddingTop}>
            {t("community:social", "Community")}
          </Text>
          <ImageButton
            uri={require("assets/images/memes/arrow.png")}
            style={styles.topIconArrow}
            onPress={() => navigation.navigate("chat")}
          />
        </View>
        <ScrollView>
          {Array.from(communityStore.characters).filter(item => !membersData[item[0]] || !membersData[item[0]].isBlaze)
            .map((value, index) => {
              const [characterName, {confidence, disabled}] = value // FIXME
              return (
                <TouchableOpacity onPress={() => {
                  if(!disabled) {
                    navigation.navigate("chat", {character: characterName})
                  }
                }} key={index}>
                  <View style={styles.exploreView}>
                    <View>
                      <Image source={membersData[characterName]?membersData[characterName].avatar:
                        OtherProfiles.find(item => item.id === characterName)&&OtherProfiles.find(item => item.id === characterName).avatar?
                          OtherProfiles.find(item => item.id === characterName).avatar:require("assets/images/community/avatars/undefined.png")} style={styles.friendAvatar}/>
                      {
                        metaGame.unreadGaoNPC.has(characterName) &&
                        <Image source={require("assets/images/messages-notification/notifAlert.png")} style={styles.notificationIcon}/>
                      }
                    </View>
                    <Text style={styles.communityNameText}>{characterName}</Text>
                    <View style={styles.spacerView} />
                    <View style={styles.confidenceRowView}>
                      <Text style={styles.h7Text}>Confidence Rating</Text>
                      <View style={styles.communityStarRowView}>
                        {
                          [...Array(membersData[characterName]?membersData[characterName].confidence:confidence)].map(
                            (item, indexStar) => {
                              return (<Image key={characterName + indexStar} source={require("assets/images/community/starIcon.png")}
                                style={styles.IconStar}/>)
                            }
                          )
                        }
                        {[...Array(5 - (membersData[characterName]?membersData[characterName].confidence:confidence))].map(
                          (item, indexStar) => <Image key={characterName + indexStar + "white"} source={require("assets/images/community/emptyStarIcon.png")} style={styles.IconStar}/>
                        )
                        }
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </View>
    </GaoCommunity>
  )
}
