import * as React from "react"
import { Text, View, ScrollView, TouchableWithoutFeedback, FlatList, BackHandler } from "react-native"
import { NavigationActions, NavigationScreenProp } from "react-navigation"
import { BlazeThoughtPopup, Button, ChatMessage, GaoCommunity, StoryChoices } from "components"
import { useStores } from "models/root-store"
import { observer } from "mobx-react-lite"
import { useEffect, useRef, useState } from "react"
import * as styles from "gao-theme/styles"
import { ProfileItem } from "screens/community/friends"
import { useTranslation } from "react-i18next"
import membersData from "assets/community_members/profile"
import { color } from "gao-theme"
import { evaluatePseudoCondition } from "utils/chat-engine/node-visitor"
import { PlayBack } from "components/sound/play-back"

export interface ChatScreenProps extends NavigationScreenProp<{}> {
}

// THIS IS FOR ERROR ABOUT TWINE
const deleteStrings = [
  "gao_gao_the_athlete",
  "gao_gao_the_filekeeper",
  "gao_gao_the_forger",
  "gao_gao_the_intruder",
  "gao_gao_the_photographer",
  "gao_gao_the_prosecutor",
  "gao_gao_the_sage",
  "gao_gao_the_spy",
  "meme",
  "emoji_sad",
  "emoji_thankful",
  "emoji_sick",
  "emoji_dizzy",
  "emoji_wink",
  "emoji_party",
  "emoji_scared"
]

const ACCEPT_MESSAGES = ["Double-click this passage to edit it.",
  "Pas de soucis, à plus tard !", "Très bien ! Je te recontacte bientôt de toute façon !", ":))",
  "C’est à ça que servent les chiffres, tu sais. A compter.\n\nSimple.\n\nBasic.",
  "Non.", "Écoute, c’est pas mon truc de prendre les gens par la main.\nJe suis au max.\nLe mieux c’est que tu testes par toi même. C vraiment pas compliqué.\nEt à la fin\ntout va bien se passer, bb.",
  "Promis", "T’inquiète", "Ok....",
  "¡No pasa nada! ¡Hablamos luego!", "¡Muy bien! ¡Igual te escribo en un rato para ver cómo vas!",
  ":))", "Obvio, baby. xoxo", "No.\n\nxoxo", "Mira. Realmente no soy de las que lleva a los demás de la mano. Siempre estoy a mil. Lo mejor es que pruebes por tu cuenta. No es nada difícil. Al final seguro te las arreglas, baby.\n\nxoxo",
  "Te prometo que no.", "No te preocupes.",
  "You're welcome !", "Great ! I'll text you later then!", ":))",
  "Not like it's good for anything else.",
  "No. ^^", "Listen. I'm not one to guide people by the hand.\nI'm always racing ahead.\nIt's best if you test on your own. It's not complicated.\nI'm sure you can figure it out, baby.",
  "Don't worry. I won't.", "I promise I won't."
]

// FIXME: do the styling.
export default observer((props: ChatScreenProps) => {
  const rootStore = useStores()
  const { t } = useTranslation()
  const flatListRef = useRef(null)
  const soundRef = useRef()

  const { navigation } = props
  const { communityStore, metaGame } = rootStore


  const characterStore = communityStore.selectedProfile

  const character = navigation.getParam("character", characterStore ? characterStore.name : null)
  const hideClose = navigation.getParam("hideClose", false)
  const member = membersData[character]

  const [canClose, setCanClose] = useState(!hideClose)
  const [isLoading, setLoading] = useState(true)

  const chatHistory = characterStore ?
    characterStore.gaoChatHistory // Is a snapshot of our current interactions.
    : null

  // Update only upon change to the character parameter.
  useEffect(() => {
    // FIXME: ideally, that should be goBack.
    // We kick invalid characters back to explore screen.
    if (!character || character.includes("Blaze")) {
      navigation.navigate("explore")
      return
    }

    communityStore.selectCharacter(character, "gao", rootStore.gameState())
    metaGame.markAsRead(character) // Mark as read message.

    setTimeout(() => {
      setLoading(false)
    }, 2000)
  }, [character])

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", () => {
      if(!canClose || rootStore.isFixedChat || !rootStore.isEndedChat)
        return true
      navigation.navigate("explore")
      return true
    })
    return () => {
      handler.remove()
    }
  }
  , []
  )

  const [waitForUser, setHiddenStatus] = useState(chatHistory
    ? chatHistory.requireUserInteraction
    : false)

  if (!characterStore || !characterStore.gaoChatHistory) {
    return (
      <View>
        <Text>{t("common:loading", "Loading...")}</Text>
      </View>
    )
  }

  let isInitNode = chatHistory.staticMessages.length === 0

  const messages = chatHistory.staticMessages.filter(item => {
    if (item.text)
      return !deleteStrings.includes(item.text)
    if (!item.path)
      return false
    return true
  }).map(item => {
    let message = item.storedValue
    if (message.text) {
      let newText = message.text
      deleteStrings.forEach(search => {
        newText = newText.replace(search, "")
      })
      while (newText.startsWith("\n"))
        newText = newText.substr(1)
      message = { ...message, text: newText }
    }
    return message
  })

  const renderedMessages = messages.map(message =>
    (<ChatMessage
      key={message.text}
      imageChat={membersData[character].avatar}
      writerName={message.isMe ? "me" : character}
      message={message} dynamic={false}
    />))

  let resultCondition = true
  if (chatHistory.currentMessage) {
    const condition = evaluatePseudoCondition(chatHistory.currentMessage)
    resultCondition = condition(metaGame)
  }

  if (!waitForUser && chatHistory.currentMessage && resultCondition) {
    isInitNode = isInitNode || chatHistory.scenario.currentNode.isInitNode
    if (chatHistory.currentMessage.text) {
      communityStore.fireDependency(chatHistory.currentMessage.text)
      metaGame.refreshChatTriggers(navigation, t)
    }

    metaGame.textChoice(chatHistory.currentMessage.text, navigation)

    if (!canClose || !rootStore.isEndedChat || rootStore.isFixedChat) {
      if (ACCEPT_MESSAGES.includes(chatHistory.currentMessage.text)) {
        rootStore.setFixedChat(false)
        setCanClose(true)
        rootStore.setEndedChat()
      }
    }

    renderedMessages.push(
      (<ChatMessage
        message={chatHistory.currentMessage}
        dynamic={!isInitNode}
        imageChat={membersData[character].avatar}
        writerName={chatHistory.currentMessage.isMe ? "me" : character}
        onShown={chatHistory.archive}
      />)
    )
  }

  const internalView = (
    <>
      <View style={styles.chatContainerView}>
        {rootStore.soundMusic && <PlayBack song={"friend_request"} noRepeat={true} muteOnStart={true} ref={soundRef} />}
        {isLoading && <View style={styles.chatMessagesViewLoading}><Text>{t("common:loading", "Loading...")}</Text></View>}

        <View style={styles.chatHeaderView}>
          {/* FIXME: show entire name  */}
          <ProfileItem deletePadding={true} item={{ name: member.id, avatar: member.avatar }}
                       style={{ ...styles.contactTiles, marginHorizontal: 0, flexDirection: "column", alignItems: "center", flex: 1 }}
                       showAlert={false} />
          {!isLoading &&
            <View style={[styles.chatButtonGroup, {flex: 3}]}>
              <Button
                disabled={!canClose}
                theme="gao"
                preset="community"
                text={t("community:check-profile", "Consulter le profil")}
                //style={{width: "70%", alignSelf: "flex-end"}}
                textStyle={{textAlign: "center", fontSize: 18}}
                onPress={() => navigation.navigate("profile", { character })}
              />
              {
                !communityStore.friends.find(item => communityStore.selectedProfile.name === item.name) &&
                (<Button
                  theme="gao"
                  preset="store"
                  text={t("community:add_to_friends", "Ajouter à mes contacts")}
                  //style={{width: "70%", alignSelf: "flex-end"}}
                  textStyle={{textAlign: "center", fontSize: 18}}
                  onPress={() => {
                    if(soundRef && soundRef.current)
                      soundRef.current.start()
                    communityStore.addSelectedProfileToFriends()
                  }}
                />)
              }
            </View>
          }
        </View>
        {(renderedMessages.length > 0 || (chatHistory && chatHistory.currentChoices && chatHistory.currentChoices.length > 0)) &&
          <>
            <FlatList
              ref={flatListRef}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => item}
              data={renderedMessages}
              vertical
              //ListFooterComponent={<View style={{height:15}}></View>}
              contentContainerStyle={{ paddingBottom: 50 }}
              onContentSizeChange={() => {
                flatListRef.current.scrollToEnd()
              }}
              style={styles.chatMessagesView} />
            {
              !waitForUser &&
              chatHistory.currentChoices &&
              !isLoading &&
              (<StoryChoices
                style={{ backgroundColor: color.palette.orange }}
                textStyle={styles.chatStoryChoice}
                canChoose={chatHistory.hasFinishedToWrite}
                choices={chatHistory.currentChoices}
                onResize={() => {
                  flatListRef.current.scrollToEnd()
                }}
                onChoose={choice => {
                  if (choice.passageTitle) {
                    communityStore.addSelectedChoice(choice.passageTitle)
                  }
                  console.log(choice)
                  return chatHistory.choose(rootStore.gameState(), choice, -1)
                }}
              />)
            }
          </>
        }
        {renderedMessages.length === 0 && (!chatHistory || !chatHistory.currentChoices || chatHistory.currentChoices.length === 0) &&
          <View style={styles.chatMessagesViewEmpty}><Text style={styles.chatMessagesViewEmptyText}>
            {t("community:empty-chat","Vous n'avez pas le niveau requis pour parler au Top Player !")}
          </Text></View>
        }
      </View>
    </>
  )

  // FIXME(beta): make the view blink under user interaction requirement.
  return (
    <GaoCommunity
      navigate={navigation.navigate}
      dismiss={() => {
        navigation.dispatch(NavigationActions.back())
      }}
      currentTab="chat"
      hideClose={!canClose || rootStore.isFixedChat}
    >
      {waitForUser
        ? (<TouchableWithoutFeedback onPress={() => setHiddenStatus(false)}>
          {internalView}
        </TouchableWithoutFeedback>)
        : internalView}
    </GaoCommunity>
  )
})
