import * as React from "react"
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  PermissionsAndroid,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native"
import {NavigationActions, NavigationScreenProps} from "react-navigation"
import {CommunityProfile, GaoCommunity, ImageButton, Notification} from "components"
import {useTranslation} from "react-i18next"
import {useEffect, useState} from "react"
import {useStores} from "models/root-store"
import * as styles from "gao-theme/styles"
import DropDownPicker from "react-native-dropdown-picker"
import {color, spacing} from "gao-theme"
import { launchImageLibrary } from "react-native-image-picker"
import {useObserver} from "mobx-react-lite"

export interface ProfileScreenProps extends NavigationScreenProps<{}> {
}

// FIXME: dead code ?
// FIXME: add types for props.
function ProfileItem(props) {
  const {item, onClick} = props
  return (
    <View style={item.container}>
      {/* Avatar */}
      <Text>{item.name}</Text>
      {/* Confidence Rating */}
    </View>
  )
}

const PLACEHOLDER_PROFILE = {
  avatar: require("assets/images/community/avatars/00_Sol.png"), // TODO: Add avatar in metagame
  about: "À la découverte de Gao..."
}

export default (props: ProfileScreenProps) => {
  const {navigation, goBack} = props
  const {t} = useTranslation()
  const profile = PLACEHOLDER_PROFILE
  const {metaGame} = useStores()
  const {userName, favoriteGame} = metaGame
  const [editText, setEditText] = useState(false)
  const [text, setText] = useState("")
  const [visibleKeyboard, setVisibleKeyboard] = useState(false)
  const [isChoosingImage, setChoosingImage] = useState(false)

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow)
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide)

    // cleanup function
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow)
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide)
    }
  }, [])

  const _keyboardDidShow = () => {
    setVisibleKeyboard(true)
  }

  const _keyboardDidHide = () => {
    setVisibleKeyboard(false)
  }

  //return (
  /*<CommunityProfile
      avatar={profile.avatar}
      name={userName}
      favoriteGame={favoriteGame}
      about={profile.about}
      goBack={() => {navigation.navigate('preHome')}}
      showNotif={true}
    />*/
  //console.log(navigation)

  const askPermission = async () => {
    try {
      await PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE],
      )
    } catch (err) {
      //console.warn(err)
    }
  }

  return useObserver( () =>
    <>
      <View style={styles.mainCommunityView}>
        <View style={styles.mainDialogContainerView}>
          <ImageButton
            uri={require("assets/images/community/cross_icon.png")}
            style={styles.iconImageView}
            onPress={() => {
              navigation.goBack(null)
            }}
          />

          <View style={styles.communityContainerView}>
            {!visibleKeyboard && (<>
              <Text style={styles.h1CenterText}>{userName}</Text>
              <View style={[styles.avatarView, {width: 100, alignSelf: "center"}]}>
                <View onTouchEndCapture={() => {
                  askPermission().then(() => {
                    if(!isChoosingImage) {
                      setChoosingImage(true)
                      metaGame.addPermission("READ_EXTERNAL_STORAGE")
                      launchImageLibrary({mediaType: "photo", includeBase64: true}, response => {
                        setChoosingImage(false)
                        if(response && response.assets)
                          metaGame.setImagePlayer(response.assets[0].uri)
                      })
                    }
                  }).catch(() => console.log("No se puede escoger la imagen"))
                
                }} style={styles.editIcon}>
                  <Image
                    style={{
                      width: spacing.large,
                      height: spacing.large,
                    }}
                    source={require("assets/images/community/iconEdit.png")}
                  />
                </View>
                {metaGame.imagePlayer.length > 0 &&
                <Image source={{uri: metaGame.imagePlayer}}
                  style={styles.commuProfileAvatar}/>
                }
                {metaGame.imagePlayer.length === 0 &&
                <Image source={profile.avatar} style={styles.commuProfileAvatar}/>
                }
                <Text
                  style={[styles.h2CompactText, {marginTop: 5}]}>{t("common:favorite-game", "Jeu favori:")}</Text>
                <DropDownPicker
                  items={[
                    {label: "Gao the forger", value: "forger"},
                    {label: "Gao the intruder", value: "intruder"},
                    {label: "Gao the athlete", value: "athlete"},
                    {label: "Gao the filekeeper", value: "filekeeper"},
                    {label: "Gao the vagabond", value: "vagabond"},
                    {label: "Gao the spy", value: "spy"},
                    {label: "Gao the sage", value: "sage"},
                    {label: "Gao the photographer", value: "photographer"},
                    {label: "Gao the prosecutor", value: "prosecutor"},
                  ]}
                  placeholder={t("common:select", "Choisir un item")}
                  defaultValue={metaGame.favoriteGame}
                  onChangeItem={item => {
                    metaGame.setFavoriteGame(item.value)
                  }}
                  labelStyle={styles.h3CompactText}
                  containerStyle={{width: 200, height: 40}}
                  arrowStyle={{marginRight: 0}}
                />
              </View>
            </>)}

            <View style={styles.profileAboutMeView}>
              <View onTouchEndCapture={() => {
                if (editText) {
                  metaGame.setAboutMe(text)
                } else {
                  setText(metaGame.aboutMePlayer)
                }
                setEditText(!editText)
              }} style={[styles.editIcon, {right: -3, top: -3}]}>
                <Image
                  style={{
                    width: spacing.large,
                    height: spacing.large,
                  }}
                  source={require("assets/images/community/iconEdit.png")}
                />
              </View>
              <ScrollView>
                <Text style={styles.h4Text}>{t("common:about-me", "Sur moi:")}</Text>
                {!editText &&
                <Text
                  style={styles.h3Text}>{metaGame.aboutMePlayer && metaGame.aboutMePlayer.length > 0 ? metaGame.aboutMePlayer : profile.about}</Text>
                }
                {
                  editText &&
                  <TextInput style={[styles.h3Text, {color: color.palette.lightGrey,}]} value={text}
                    onChangeText={(text) => {
                      setText(text)
                    }} placeholder={profile.about}
                    multiline={true}
                  />
                }
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    </>
  )
}
