import * as React from "react"
import { Text, View, FlatList, TouchableOpacity, Image } from "react-native"
import { Button, GaoCommunity, Notification } from "components"
import { NavigationActions, NavigationScreenProps } from "react-navigation"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { useStores } from "models/root-store"
import membersData from "assets/community_members/profile"

export interface CommunityHomeScreenProps extends NavigationScreenProps<{}> {
  showAlert?: boolean
}

// FIXME: add types for props.
export const ProfileItem = props => {
  const { item, onClick, deletePadding } = props
  return (
    <TouchableOpacity onPress={onClick} style={props.style}>
      <Image style={styles.friendAvatar} source={item.avatar}/>
      <Notification show={props.showAlert} preset={"alert"}>
        <Text style={[styles.h2Text, deletePadding?{padding: 0}:null]}>{item.name}</Text>
      </Notification>
      {/* Confidence Rating */}
    </TouchableOpacity>
  )
}
const EmptyItem = () => <View style={styles.emptyTiles} />

const confidenceRating = 5

const fillTo10 = profils => { // FIXME: this will fail if profile.length > 10
  return profils.concat(Array(10 - profils.length).fill({empty: true}))
}

export default (props: CommunityHomeScreenProps) => {
  const {navigation} = props
  const {t} = useTranslation()
  const { communityStore } = useStores()
  const rootStore = useStores()

  const characterStore = communityStore.selectedProfile

  //const characterName = characterStore.name
  const character = navigation.getParam("character", characterStore ? characterStore.name : null)
  //const member = membersData[character]

  //FIXME: wrong avatar
  const currentFriends = communityStore.friends.map(friend => {
    return {
      ...friend,
      avatar: membersData[friend.name].avatar,
      confidenceRating: confidenceRating,
      empty: false
    }
  })

  return (
    <GaoCommunity
      navigate={navigation.navigate}
      dismiss={() => {
        navigation.dispatch(NavigationActions.back())
      }}
      currentTab="social" style={{backgroundColor: "transparent"}}
    >
      <View style={styles.friendsContainerView}>
        <Text style={styles.h1Text}>{t("community:friends", "Mes contacts")}</Text>
        <FlatList
          data={fillTo10(currentFriends)}
          renderItem={({ item }) => item.empty ? <EmptyItem /> :
            <ProfileItem showAlert={true} item={item} onClick={() => navigation.navigate("chat", {character: item.name})} style={[styles.friendsTiles, {alignItems: "center"}]}/>
          }
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
        />
        <Button
          text={t("community:exploreCommunity", "Explore Community")}
          preset="community"
          theme="gao"
          onPress={() => navigation.navigate("explore")}
          style={rootStore.isFirstStart?{opacity: 0.2}:null}
          disabled={rootStore.isFirstStart}
        />
      </View>
    </GaoCommunity>
  )
}
