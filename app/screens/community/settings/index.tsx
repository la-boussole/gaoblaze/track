import * as React from "react"
import {Image, Linking, NativeModules, ScrollView, View} from "react-native"
import { NavigationActions, NavigationScreenProps } from "react-navigation"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import {Button, Text, ImageButton, GaoDialog} from "components"
import { useState } from "react"
import { useStores } from "models/root-store"
import { TouchableOpacity } from "react-native-gesture-handler"

export interface SettingsScreenProps extends NavigationScreenProps<{}> {}

const LANGUAGES = [ "English", "Français", "Español" ]

export const CheckBox = props => (
  <>
    <Text style={styles.h6Text}>{props.text}</Text>
    <ImageButton
      style={styles.IconCheckBox}
      uri={props.checked ? require("assets/images/community/checkboxTrue.png") : require("assets/images/community/checkboxFalse.png")}
      onPress={props.onPress}
    />
  </>
)

const SettingsScreen = (props: SettingsScreenProps) => {
  const { navigation } = props
  const { t } = useTranslation()
  const rootStore = useStores()
  const [homeMusic, setHomeMusic] = useState(rootStore.homeMusic)
  const [gameMusic, setGameMusic] = useState(rootStore.gameMusic)
  const [soundMusic, setSoundMusic] = useState(rootStore.soundMusic)
  const [showWarning, setShowWarning] = useState(false)
  const [showHelp, setShowHelp] = useState(false)

  return (
    <View style={styles.mainCommunityView}>
      <View style={styles.mainDialogContainerView}>
        <ImageButton
          uri={require("assets/images/community/cross_icon.png")}
          style={styles.iconImageView}
          onPress={() => {
            navigation.dispatch(NavigationActions.back())
          }}
        />
        {/* TODO: add a preset 'settings's in GaoCommunity? */}
        <ScrollView style={styles.communityContainerView}>
          <View style={{paddingVertical: 30}}>
            <View style={styles.centerView}>
              <Button theme="gao" preset="community" text={t("community:my-profile", "Mon profil")} onPress={() => navigation.navigate("my_profile")}/>
            </View>
            <View style={styles.settingsTopBar}></View>
            <View style={styles.containerSettingsView}>
              <Text style={styles.h5Text}>{t("community:language","Langue")}:</Text>
              <View style={styles.flexRowView}>
                <Button theme="gao" preset="store" text={t("community:reset", "Reset")} onPress={() => setShowWarning(true)}/>
                {/*<ImageButton
                  style={styles.IconTriangle}
                  uri={require("assets/images/community/arrowLeft.png")}
                  onPress={() => {
                    setShowWarning(true)
                    const newLanguage = (LANGUAGES.length + languageSelected - 1) % LANGUAGES.length
                    changeLanguage(newLanguage)
                    rootStore.setSettings(newLanguage, homeMusic, gameMusic, soundMusic)
                  }}
                />
                <Text style={styles.h6Text}>{LANGUAGES[languageSelected]}</Text>
                <ImageButton
                  style={styles.IconTriangle}
                  uri={require("assets/images/community/arrowRight.png")}
                  onPress={() => {
                    setShowWarning(true)
                    const newLanguage = (languageSelected + 1) % LANGUAGES.length
                    changeLanguage(newLanguage)
                    rootStore.setSettings(newLanguage, homeMusic, gameMusic, soundMusic)
                  }}
                />*/}
              </View>
              <Text style={styles.h5Text}>{t("community:sounds","Musique démarrage")}:</Text>
              {/**************************/}
              {/* UX Design : expert lvl */}
              {/**************************/}
              <View style={styles.flexRowView}>
                <CheckBox text={t("community:enable", "Oui")} checked={homeMusic} onPress={() => {
                  const newHomeMusic = !homeMusic
                  setHomeMusic(newHomeMusic)
                  rootStore.setSettings(newHomeMusic, gameMusic, soundMusic)
                  if(rootStore.song === 'gao_main_sound.ogg') {
                    rootStore.setSong(rootStore.song, newHomeMusic)
                  }
                }}/>
                <CheckBox text={t("community:off", "Non")} checked={!homeMusic} onPress={() => {
                  const newHomeMusic = !homeMusic
                  setHomeMusic(newHomeMusic)
                  rootStore.setSettings(newHomeMusic, gameMusic, soundMusic)
                  if(rootStore.song === 'gao_main_sound.ogg') {
                    rootStore.setSong(rootStore.song, newHomeMusic)
                  }
                }}/>
              </View>
              <Text style={styles.h5Text}>{t("community:music","Musique des mini-jeux")}:</Text>
              <View style={styles.flexRowView}>
                <CheckBox text={t("community:enable", "Oui")} checked={gameMusic} onPress={() => {
                  const newGameMusic = !gameMusic
                  setGameMusic(newGameMusic)
                  rootStore.setSettings(homeMusic, newGameMusic, soundMusic)
                  if(rootStore.song !== 'gao_main_sound.ogg') {
                    rootStore.setSong(rootStore.song, newGameMusic)
                  }
                }}/>
                <CheckBox text={t("community:off", "Non")} checked={!gameMusic} onPress={() => {
                  const newGameMusic = !gameMusic
                  setGameMusic(newGameMusic)
                  rootStore.setSettings(homeMusic, newGameMusic, soundMusic)
                  if(rootStore.song !== 'gao_main_sound.ogg') {
                    rootStore.setSong(rootStore.song, newGameMusic)
                  }
                }}/>
              </View>
              <Text style={styles.h5Text}>{t("community:button-sound","Sons des boutons")}</Text>
              <View style={styles.flexRowView}>
                <CheckBox text={t("community:enable", "Oui")} checked={soundMusic} onPress={() => {
                  const newSoundMusic = !soundMusic
                  setSoundMusic(newSoundMusic)
                  rootStore.setSettings(homeMusic, gameMusic, newSoundMusic)
                }}/>
                <CheckBox text={t("community:off", "Non")} checked={!soundMusic} onPress={() => {
                  const newSoundMusic = !soundMusic
                  setSoundMusic(newSoundMusic)
                  rootStore.setSettings(homeMusic, gameMusic, newSoundMusic)
                }}/>
              </View>
              <TouchableOpacity style={[styles.gaoBlazePage, {height: 70}]} onPress={() => {setShowHelp(true)}} >
                <Image source={require("assets/images/community/flag.png")} style={{position: "absolute", width: "100%", height: "100%", resizeMode: "stretch"}}></Image>
                <Text style={[styles.gaoBlazePageText, {textAlign: "center", marginBottom: 10}]}>
                  {t("common:help", "Aide")}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </ScrollView>
      </View>
      {showWarning && <GaoDialog
        preset="language-warning"
        textDialog1={t(
          "common:reset-warning",
          "ATTENTION, changer de langue maintenant réinitialisera toutes tes avancées !",
        )}
        textStyle1={styles.infoText}
        textButton="Ok"
        buttonOnPress={() => {
          NativeModules.TrackProjectMisc.clearAppData()
          setShowWarning(false)
        }
        }
        buttonOnCancelPress={() => setShowWarning(false)}
      />
      }
      {showHelp && <GaoDialog
        preset="help-warning"
        textDialog1={t(
          "common:help-message",
          "Joue au moins à 6 Gao Games et complète toutes les discussions avec Alex, Ally et Nikki. Plus d'info en gaoandblaze.org",
        )}
        textStyle1={styles.infoText}
        textButton="Ok"
        buttonOnPress={() => {
          setShowHelp(false)
        }
        }
      />
      }
    </View>
  )
}

export default SettingsScreen
