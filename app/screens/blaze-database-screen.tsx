import * as React from "react"
import {BlazeContainer, ImageButton, ScrollableList, Text} from "../components"
import {NavigationScreenProp} from "react-navigation"

import {observer, useObserver} from "mobx-react-lite"
import {useStores} from "models/root-store"
import {useEffect, useState} from "react"
import {BackHandler, Linking, View} from "react-native"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"
import {TouchableOpacity} from "react-native-gesture-handler"
//import { BLAZE_PROFILES } from "assets/community_members/blaze-profiles"
import { BLAZE_PROFILES_FR } from "assets/community_members/blaze-profiles-FR"
import { BLAZE_PROFILES_ES } from "assets/community_members/blaze-profiles-ES"
import { BLAZE_PROFILES_EN } from "assets/community_members/blaze-profiles-EN"
import i18n from "i18n/i18n"

export interface BlazeDatabaseScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const DebugMode = props => {
  const {t} = useTranslation()
  const [count, setCount] = useState(1)
  const {metaGame} = useStores()
  const {navigation} = props

  const launchDebug = () => {
    if (count > 4) {
      metaGame.setIsDebugMode(t)
      metaGame.refreshChatTriggers(navigation, t)
      //navigation.replace("debugMode")
      setCount(1)
    } else {
      setCount(count + 1)
    }
  }

  return useObserver(() =>
    <>
      {
        metaGame.canDebugMode && !metaGame.isDebugMode && !metaGame.blazeChatUnlocked() &&
        (<TouchableOpacity onPress={launchDebug}>
          <Text style={styles.h2Text} numberOfLines={1}>
            <Text style={{...styles.h2Text, ...styles.boldFontText}}>Gao </Text>
            {t("blaze:player-base", "Player Base")}
          </Text>
        </TouchableOpacity>)
      }
      {(!metaGame.canDebugMode || metaGame.blazeChatUnlocked()) && !metaGame.isDebugMode &&
      (<View style={{flex: 1, marginRight: 10}}>
        <Text style={[styles.h2Text, {textAlign: "left"}]} numberOfLines={1}>
          <Text style={{...styles.h2Text, ...styles.boldFontText}}>Gao </Text>
          {metaGame.isDebugMode ? t("blaze:debug-mode", "Debug Mode") : t("blaze:player-base", "Player Base")}
        </Text>
      </View>)}
      {metaGame.isDebugMode &&
      (<View style={{flex: 1, marginRight: 10}}>
        <Text style={[styles.h2RedText, {textAlign: "left"}]} numberOfLines={1}>
          <Text style={{...styles.h2RedText, ...styles.boldFontText}}>Admin </Text>
          {t("blaze:debug-mode", "Debug Mode")}
        </Text>
      </View>)}
    </>
  )
}

export const BlazeDatabaseScreen: React.FunctionComponent<BlazeDatabaseScreenProps> = BlazeContainer(observer(props => {
  const {navigation} = props
  const { metaGame, communityStore } = useStores()
  const rootStore = useStores()
  const [names, setNames] = useState([])
  const {t} = useTranslation()
  const BLAZE_PROFILES = i18n.language === 'es' ? BLAZE_PROFILES_ES : i18n.language === 'en' ? BLAZE_PROFILES_EN : BLAZE_PROFILES_FR


  useEffect(() => {
    const newNames = [...BLAZE_PROFILES.filter(item => !item.isNikkiTought)]
    newNames.splice(0, 0, {isForReturn: true, key: "[...] Return to Gao Games", enabled: true})
    newNames.push({key: metaGame.userName, about: t("blaze:player-profile", "Nouvel utilisateur/utilisatrice. Profil en cours de génération."), overview: t("blaze:player-resume", "Enthousiaste. Contacts fréquents avec Alex ( user#6548852 ) depuis la procédure d’accueil."), recommendations: t("blaze:player-recomendations", "Note interne 1 (confidentiel) : Mise à jour récente. Activités suspectes auprès d’Alex depuis la disparition de Blaze. Sous surveillance."), informationShared: metaGame.permissions.join(), enabled: true, imageUri: metaGame.imagePlayer.length > 0 ? metaGame.imagePlayer : null})
    setNames(newNames)
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [])

  const handleBackButtonClick = () => {
    navigation.navigate("preHome")
    return true
  }

  return useObserver( () =>
    <>
      <View style={styles.topBarView}>
        {/* Secret feature : Component to launch screen debugMode when clicked 5 times */}
        <DebugMode {...props} />
        {metaGame.notebookUnlocked() && (
          <ImageButton
            style={styles.iconImage}
            uri={require("assets/images/blaze-icons/book.png")}
            onPress={() => navigation.navigate("notebook")}
          />
        )}
        {metaGame.blazeChatUnlocked() && (
          <ImageButton
            style={styles.iconImage}
            uri={require("assets/images/blaze-icons/message.png")}
            onPress={() => navigation.navigate("blazeChat")}
          />
        )}
      </View>
      <ScrollableList
        preset={metaGame.isDebugMode?"debug":null}
        data={names.map(user => ({
          ...user, onPress: () => {
            if(user.isForReturn) {
              rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
              navigation.navigate("preHome")
            } else if(user.enabled) {
              communityStore.addDataBaseVisited()
              metaGame.refreshChatTriggers(navigation, t)
              navigation.navigate("profile", {
                character: user.key,
                showAnimation: user.hasAnimation || false,
                characterInfo: user
              })
            } else if(user.key.toLowerCase() === "atom nobot") {
              Linking.openURL("https://co.linkedin.com/in/alvaro-perez-paez")
            }
          }
        }))}/>
    </>
  )
}))
