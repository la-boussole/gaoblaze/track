/* eslint-disable react-native/no-inline-styles */
import {Image, Text, View, NativeModules, FlatList, BackHandler, Linking} from "react-native"
import * as React from "react"
import * as styles from "blaze-theme/styles"
import {ImageButton, Wallpaper} from "components"
import { ScrollView, TextInput, TouchableOpacity } from "react-native-gesture-handler"
import { color, typography } from "blaze-theme"
import { useStores } from "models/root-store"
import {ProgressBar} from "@react-native-community/progress-bar-android"
import {BlazeNotebookSkillPopup} from "screens/blaze-notebook-skill-popup"
import {useTranslation} from "react-i18next"
import {BlazeNotebookThoughtPopup} from "screens/blaze-notebook-thought-popup"
import {ScanPopupScreen, sendInformationToAirTable} from "screens/scan-popup-screen"
import {LastMessageQuestionaryScreen} from "screens/last-message-questionary-screen"
import Toast from "react-native-toast-message"
import KeepAwake from "react-native-keep-awake"

//import { BLAZE_PROFILES } from "assets/community_members/blaze-profiles"
import { BLAZE_PROFILES_FR } from "assets/community_members/blaze-profiles-FR"
import { BLAZE_PROFILES_ES } from "assets/community_members/blaze-profiles-ES"
import { BLAZE_PROFILES_EN } from "assets/community_members/blaze-profiles-EN"
import i18n from "i18n/i18n"

const IMAGE_LETTERS = [require("assets/images/scan/A_IDLE.png"), require("assets/images/scan/B_IDLE.png"), require("assets/images/scan/C_IDLE.png"), require("assets/images/scan/D_IDLE.png"), require("assets/images/scan/E_IDLE.png")]

export interface ScanPhoneScreenProps {

}

export const ScanPhoneScreen: React.FunctionComponent<ScanPhoneScreenProps> = (props) => {
  const BLAZE_PROFILES = i18n.language === 'es' ? BLAZE_PROFILES_ES : i18n.language === 'en' ? BLAZE_PROFILES_EN : BLAZE_PROFILES_FR
  const { navigation } = props
  const { t } = useTranslation()
  const [list, setList] = React.useState([])
  const { metaGame } = useStores()
  const rootStore = useStores()
  const [checked, setChecked] = React.useState([])
  const [showPopup, setShowPopup] = React.useState(false)
  const [appList, setAppList] = React.useState([])
  const [images, setImages] = React.useState([])
  const [scanValues, setScanValues] = React.useState({})
  const [allScanApp, setAllScanApp] = React.useState(metaGame.getAllScanApps())
  const [showLoading, setShowLoading] = React.useState(false)
  const [thought, setThought] = React.useState(null)
  const [thoughtSkill, setThoughtSkill] = React.useState(null)
  const [idSelectedSkill, setIdSelectedSkill] = React.useState(null)
  const [versions, setVersions] = React.useState({})
  const [showScanPopupScreen, setShowScanPopupScreen] = React.useState(false)
  const [cancel, setCancel] = React.useState(false)
  const [showQuestionary, setShowQuestionary] = React.useState(false)
  const [filterList, setFilterList] = React.useState([])
  const [filterText, setFilterText] = React.useState("")
  const [buttonDisabled, setButtonDisabled] = React.useState(true)

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick)
    NativeModules.TrackProjectMisc.getApplicationList().then((result: string) => {
      const apps = result.split("\n").map((item: string) => item.split(":"))
      if(apps.length === 0) {
        setShowPopup(false)
        setShowScanPopupScreen(false)
        setShowLoading(false)
        Toast.show({
          text1: t("common:scan-phone-error", "No podemos obtener el listado de apps por tú S.O."),
          type: "error",
          visibilityTime: 3000,
          position: "bottom",
          onShow: () => console.log("show")
        })
      } else {
        const scans = {}
        apps.forEach(item => {
          if(metaGame.appScan.has(item[0]))
            scans[item[0]] = metaGame.appScan.get(item[0]).value
        })
        apps.sort((itemA, itemB) => (itemA[1] as string).localeCompare(itemB[1]))
        setScanValues(scans)
        setAppList(apps)
      }
      //loadImages(apps)
    }).catch(() => {
      setShowPopup(false)
      setShowScanPopupScreen(false)
      setShowLoading(false)
      Toast.show({
        text1: t("common:scan-phone-error", "No podemos obtener el listado de apps por tú S.O."),
        type: "error",
        visibilityTime: 3000,
        position: "bottom",
        onShow: () => console.log("show")
      })
    })
    const thoughts = []
    if(!metaGame.fillQuestionary) {
      thoughts.push({isQuestionary: true})
      thoughts.push({isEmpty: true})
    }
    BLAZE_PROFILES.forEach(profile => {
      if (profile.thought) {
        thoughts.push(profile.thought)
        if (profile.thought.skills) {
          let pushSkill = false
          profile.thought.skills.forEach((tool, index) => {
            if(!thoughts.find(search => search && search.tweet === tool.tooltitle)) {
              if (pushSkill)
                thoughts.push({isEmpty: true})
              thoughts.push({index: index, ...profile.thought, tweet: tool.tooltitle})
              pushSkill = true
            }
          })
          if(!pushSkill)
            thoughts.push({isEmpty: true})
        }
        else thoughts.push({isEmpty: true})
      }
    })
    setList(thoughts)
    const checks = []
    metaGame.scanCheck.forEach(item => checks.push(item))
    setChecked(checks)
    setTimeout(() => setButtonDisabled(false), 3000)
    return () => BackHandler.removeEventListener("hardwareBackPress", handleBackButtonClick)
  }, [showPopup, showQuestionary, cancel])

  const handleBackButtonClick = () => {
    /*if(showQuestionary) {
      setShowQuestionary(false)
      return true
    }
    if(showScanPopupScreen)
      return true
    if (showPopup) {
      if(!showLoading) {
        setShowPopup(false)
        if(!rootStore.isSendDataExodusAccept) {
          setShowScanPopupScreen(true)
        } else {
          const idGamer = rootStore.idGamer
          sendInformationToAirTable(metaGame, idGamer, true).then(response => response.json().then(json => {
            if(idGamer.length === 0) {
              //console.log(response)
              rootStore.setIdGamer(json.records[0].id)
            }
          }
          )).catch(err => {
            showError()
            console.log("El error", err)
          })
        }
      } else {
        setCancel(true)
        setShowPopup(false)
        if(!rootStore.isSendDataExodusAccept) {
          setShowScanPopupScreen(true)
        }
      }
    } else {
      return true
    }*/
    return true
  }

  const loadImages = async (list) => {
    const newImages = []
    for (let i = 0; i < list.length; i++) {
      const image = await NativeModules.TrackProjectMisc.getApplicationImage(list[i][0])
      newImages.push(image)
    }
    setImages(newImages)
  }

  const scanAllApps = () => {
    fetch("https://reports.exodus-privacy.eu.org/api/applications?option=short", {
      method: "GET",
      headers: {
        Authorization: "Token 02eb3b17086fc3e469a1f799f1dd39ce59d840d0",
      },
    }).then((response) => response.json().then(json => {
      const names = json.applications.map(item => item.handle.toLowerCase())
      const newAppList = appList.filter(item => names.includes(item[0].toLowerCase()) && item[0].toLowerCase() !== 'com.google.android.webview')
      //console.log(newAppList)
      //const newAppList = appList.filter(item => names.includes(item[0])).filter((item, index1) => index1 < 3)
      //console.log("newAppList", newAppList.length, newAppList)
      loadImages(newAppList)
      setAppList(newAppList)
      searchText("", newAppList)
      scanApps(newAppList, 0, {}, {})
    })).catch(err => {
      showError()
      console.log("El error", err)
    })
  }

  const searchText = (text, newAppList) => {
    setFilterList(newAppList.filter(item => item[1].toLowerCase().includes(text.toLowerCase())))
    setFilterText(text)
  }

  const showError = () => {
    KeepAwake.deactivate()
    setShowLoading(false)
    setShowPopup(false)
    Toast.show({
      text1: t("common:connection-error", "Error"),
      type: "error",
      visibilityTime: 3000,
      position: "bottom",
      onShow: () => console.log("show")
    })
  }

  const scanApps = (newAppList, index, scanedValues, newVersions) => {
    if(cancel && index > 0) return
    if (index < newAppList.length) {
      fetch("https://reports.exodus-privacy.eu.org/api/search/"+newAppList[index][0]+"/details", {
        method: "GET",
        headers: {
          Authorization: "Token 02eb3b17086fc3e469a1f799f1dd39ce59d840d0",
        }
      }).then((response) => response.json().then((json) => {
        const newScanValues = {...scanedValues}
        let temporalValue = -1
        if(json.length > 0) {
          let lastNumber = Number.MAX_VALUE
          let lastVersion = null
          json.forEach((item) => {
            if(Math.abs(parseInt(item.version_code) - parseInt(newAppList[index][3])) < lastNumber) {
              lastNumber = Math.abs(parseInt(item.version_code) - parseInt(newAppList[index][3]))
              lastVersion = item
            }
            else if(Math.abs(parseInt(item.version_code) - parseInt(newAppList[index][3])) === lastNumber) {
              if(lastVersion === null || parseInt(lastVersion.version_code) < parseInt(item.version_code)) {
                lastVersion = item
              }
            }
          })
          if(lastVersion.version_name !== newAppList[index][2]) {
            newVersions[newAppList[index][0]] = {installed: newAppList[index][2], checked: lastVersion.version_name}
            setVersions({...newVersions})
          }
          const trackers = lastVersion.trackers.length
          const criticalPermissions = lastVersion.permissions.filter(item => item.includes("*")).length
          const totalPermissions = lastVersion.permissions.length
          //console.log("=======")
          //console.log(newAppList[index][0], trackers, criticalPermissions, totalPermissions)
          //console.log(lastVersion)
          temporalValue = totalPermissions + (trackers * criticalPermissions)
        }
        const value = metaGame.addAppScan(newAppList[index][0], temporalValue)
        newScanValues[newAppList[index][0]] = value
        setScanValues(newScanValues)
        scanApps(newAppList, index + 1, newScanValues, newVersions)
        setAllScanApp(metaGame.getAllScanApps())
      })).catch(err => {
        showError()
        console.log("El error", err)
      })
    } else {
      setShowLoading(false)
      setScanValues(scanedValues)
    }
  }

  return (<>
    <Wallpaper preset="cover" backgroundImage={require("assets/images/scan/BG.png")} />
    <ScrollView style={{flex: 1}}>
      <View style={[styles.lastMessageMainQuestionContainer, { padding: 20 }]}>
        <Image source={require("assets/images/scan/GAO_LOGO.png")} style={{ height: 80, resizeMode: "contain" }}></Image>
        <View style={{ flexDirection: "row", width: "100%", justifyContent: "center", alignItems: "center" }}>
          <Image source={require("assets/images/scan/parenthesis_left.png")} style={{ height: 30, resizeMode: "contain", width: 20, }}></Image>
          <Text style={{ color: color.primaryDarker, fontFamily: typography.secondary, fontSize: 20 }}>{t("scan:mode", "REAL LIFE MODE")}</Text>
          <Image source={require("assets/images/scan/parenthesis_right.png")} style={{ height: 30, resizeMode: "contain", width: 20 }}></Image>
        </View>
        <TouchableOpacity style={{ margin: 20 }} disabled={buttonDisabled} onPress={() => {
          setCancel(false)
          setShowPopup(true)
          setShowLoading(true)
          KeepAwake.activate()
          sendInformationToAirTable(metaGame, rootStore.idGamer, false).then(response => response.json().then(json => {
            if(rootStore.idGamer.length === 0) {
              //console.log(response)
              rootStore.setIdGamer(json.records[0].id)
            }
            setTimeout(() => scanAllApps(), 1000)
          }
          )).catch(err => {
            showError()
            console.log("El error", err)
          })
        }}>
          <Image source={allScanApp !== null && allScanApp !== undefined ? IMAGE_LETTERS[allScanApp]:require("assets/images/scan/CAT_scan_IDLE.png")} style={{ height: 160, resizeMode: "contain", }}></Image>
        </TouchableOpacity>
        <View style={{ flexDirection: "row", width: "100%", justifyContent: "center", }}>
          <Image source={require("assets/images/scan/parenthesis_left.png")} style={{ height: 30, resizeMode: "contain", width: 20 }}></Image>
          <Text style={{ color: color.primaryDarker, fontFamily: typography.secondary, fontSize: 20 }}>{t("scan:security", "CHECK YOUR PHONES SECURITY!")}</Text>
          <Image source={require("assets/images/scan/parenthesis_right.png")} style={{ height: 30, resizeMode: "contain", width: 20 }}></Image>
        </View>
        <Image source={require("assets/images/scan/line.png")} style={{ height: 30, resizeMode: "stretch", width: "100%" }}></Image>
        <Text style={{ color: color.primaryDarker, fontFamily: typography.secondary, fontSize: 20 }}>{t("scan:improve", "HOW TO IMPROVE IT")}</Text>
        <Text style={{ color: color.primaryDarker, fontFamily: typography.secondary, fontSize: 15, marginVertical: 10 }}>{t("scan:check", "CHECK THE BOXES AS YOU GAIN SKILLS")}</Text>
        <FlatList numColumns={2} keyExtractor={(item, index) => index + ""} data={list} renderItem={(renderItem) => {
          const index = renderItem.index
          const item = renderItem.item
          if(item.isEmpty) {
            return <View style={{ width: "50%", flexDirection: "row", marginVertical: 5, alignItems: "center" }}></View>
          }
          else if(item.isQuestionary) {
            return <View style={{ width: "50%", flexDirection: "row", marginVertical: 5, alignItems: "center" }}>
              {
                checked.includes("Questionnaire") && <Image source={require("assets/images/scan/BOX_ON.png")} style={{ width: 22, height: 22, resizeMode: "stretch", }}></Image>
              }
              {
                !checked.includes("Questionnaire") && <Image source={require("assets/images/scan/BOX_OFF.png")} style={{ width: 20, height: 20, resizeMode: "stretch", }}></Image>
              }
              <TouchableOpacity style={{ height: 30, width: 120, marginHorizontal: 10, backgroundColor: "red", justifyContent: "center", alignItems: "center" }}
                onPress={() => {
                  metaGame.checkScan("Questionnaire")
                  setShowQuestionary(true)
                  if (checked.includes("Questionnaire"))
                    setChecked(checked.filter(search => search !== "Questionnaire"))
                  else
                    setChecked([...checked, "Questionnaire"])
                }}>
                <Text style={{ fontFamily: typography.secondary, }}>{"Questionnaire"}</Text>
              </TouchableOpacity>
            </View>
          } {
            return <View key={"touhgt-" + index} style={{ width: "50%", flexDirection: "row", marginVertical: 5, alignItems: "center" }}>
              {
                checked.includes(item.tweet) && <Image source={require("assets/images/scan/BOX_ON.png")} style={{ width: 22, height: 22, resizeMode: "stretch", }}></Image>
              }
              {
                !checked.includes(item.tweet) && <Image source={require("assets/images/scan/BOX_OFF.png")} style={{ width: 20, height: 20, resizeMode: "stretch", }}></Image>
              }
              <TouchableOpacity style={{ height: 30, width: 120, marginHorizontal: 10, backgroundColor: color.primary, justifyContent: "center", alignItems: "flex-start" }}
                onPress={() => {
                  if(!item.hasOwnProperty("index"))
                    setThought(item)
                  else {
                    setIdSelectedSkill(item.index)
                    setThoughtSkill(item)
                  }
                  metaGame.checkScan(item.tweet)
                  if (!checked.includes(item.tweet))
                    setChecked([...checked, item.tweet])
                }}>
                <Text style={{ fontFamily: typography.secondary, flexWrap: "wrap" }} numberOfLines={1}>{item.tweet}</Text>
              </TouchableOpacity>
            </View>
          }
        }} />
        {/*<ScrollView style={{ flex: 1, margin: 20, width: "100%" }}>
          <View style={{ flex: 1, flexDirection: "row", flexWrap: "wrap", justifyContent: "space-between" }}>
            {
              !metaGame.fillQuestionary && <View style={{ width: "50%", flexDirection: "row", marginVertical: 5, alignItems: "center" }}>
                {
                  checked.includes("Questionnaire") && <Image source={require("assets/images/scan/BOX_ON.png")} style={{ width: 22, height: 22, resizeMode: "stretch", }}></Image>
                }
                {
                  !checked.includes("Questionnaire") && <Image source={require("assets/images/scan/BOX_OFF.png")} style={{ width: 20, height: 20, resizeMode: "stretch", }}></Image>
                }
                <TouchableOpacity style={{ height: 30, width: 120, marginHorizontal: 10, backgroundColor: "red", justifyContent: "center", alignItems: "center" }}
                  onPress={() => {
                    metaGame.checkScan("Questionnaire")
                    setShowQuestionary(true)
                    if (checked.includes("Questionnaire"))
                      setChecked(checked.filter(search => search !== "Questionnaire"))
                    else
                      setChecked([...checked, "Questionnaire"])
                  }}>
                  <Text style={{ fontFamily: typography.secondary, }}>{"Questionnaire"}</Text>
                </TouchableOpacity>
              </View>
            }
            {
              list.map((item, index) => (<View key={"touhgt-" + index} style={{ width: "50%", flexDirection: "row", marginVertical: 5, alignItems: "center" }}>
                {
                  checked.includes(item.tweet) && <Image source={require("assets/images/scan/BOX_ON.png")} style={{ width: 22, height: 22, resizeMode: "stretch", }}></Image>
                }
                {
                  !checked.includes(item.tweet) && <Image source={require("assets/images/scan/BOX_OFF.png")} style={{ width: 20, height: 20, resizeMode: "stretch", }}></Image>
                }
                <TouchableOpacity style={{ height: 30, width: 120, marginHorizontal: 10, backgroundColor: color.primary, justifyContent: "center", alignItems: "flex-start" }}
                  onPress={() => {
                    if(!item.hasOwnProperty("index"))
                      setThought(item)
                    else {
                      setIdSelectedSkill(item.index)
                      setThoughtSkill(item)
                    }
                    metaGame.checkScan(item.tweet)
                    if (!checked.includes(item.tweet))
                      setChecked([...checked, item.tweet])
                  }}>
                  <Text style={{ fontFamily: typography.secondary, flexWrap: "wrap" }} numberOfLines={1}>{item.tweet}</Text>
                </TouchableOpacity>
              </View>))
            }
          </View>
        </ScrollView>*/}
      </View>
    </ScrollView>
    {showPopup && <View style={{ width: "100%", height: "100%", position: "absolute", backgroundColor: color.fade, padding: 20 }}>
      <View style={{ borderRadius: 10, borderWidth: 2, borderColor: color.line, width: "100%", height: "100%", backgroundColor: color.scanBackground }}>
        <View style={{ flexDirection: "row", justifyContent: "center", margin: 10, alignItems: "center" }}>
          <TouchableOpacity onPress={() => {
            setShowPopup(false)
            KeepAwake.deactivate()
            if(!rootStore.isSendDataExodusAccept) {
              setShowScanPopupScreen(true)
            } else {
              const idGamer = rootStore.idGamer
              sendInformationToAirTable(metaGame, idGamer, true).then(response => response.json().then(json => {
                  if(idGamer.length === 0) {
                    //console.log(response)
                    rootStore.setIdGamer(json.records[0].id)
                  }
                }
              )).catch(err => {
                showError()
                console.log("El error", err)
              })
            }
          }}>
            <Image source={require("assets/images/blaze-icons/left-arrow.png")} style={{ height: 40, resizeMode: "contain", width: 40, marginRight: 10 }}></Image>
          </TouchableOpacity>
          <Text style={{ fontFamily: typography.secondary, color: color.primary, fontSize: 25, flex: 1 }}>{t("scan:score", "Votre dernier score")}</Text>
          <TouchableOpacity>
            <Image source={allScanApp?IMAGE_LETTERS[allScanApp]:require("assets/images/scan/CAT_scan_IDLE.png")} style={{ height: 50, resizeMode: "contain", width: 50, marginLeft: 10 }}></Image>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={{ flexDirection: "row", margin: 10, alignItems: "center" }} onPress={() => {
            if(i18n.language === "es")
              Linking.openURL("https://gaoandblaze.org/es/el-catscan/")
            else if(i18n.language === "en")
              Linking.openURL("https://gaoandblaze.org/en/the-catscan/")
            else
              Linking.openURL("https://gaoandblaze.org/catscan/")
          }}>
            <Image source={require("assets/images/blaze-icons/book.png")} style={{ height: 50, resizeMode: "contain", width: 50, marginLeft: 10 }}></Image>
            <Text style={{ fontFamily: typography.secondary, color: color.primary, fontSize: 18, marginLeft: 10, flex: 1 }} numberOfLines={2}>{t("scan:comment", "Comment le score est-il calculé?")}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ backgroundColor: color.scanTextInput, marginHorizontal: 10, flexDirection: "row", alignItems: "center" }}>
          <TextInput style={{ flex: 1, color: color.palette.white, fontFamily: typography.secondary }} placeholder={"CAT Scan (avec Exodus Privacy)"} placeholderTextColor={color.palette.white}
            value={filterText} onChangeText={(newText) => searchText(newText, appList)}
          />

          <Image source={require("assets/images/blaze-icons/search_24px_outlined.png")} style={{ height: 24, resizeMode: "contain", width: 50, }}></Image>
        </View>
        <FlatList style={{ flex: 1, backgroundColor: color.scanBackgroundGrid, marginHorizontal: 10, marginBottom: 10 }}
          keyExtractor={(item) => item[0]}
          overScrollMode={"never"}
          data={filterList}
          renderItem={({ item, index }) => {
            return <View>
              <View style={{ flexDirection: "row", margin: 5, }}>
                <Image source={images[index] ? { uri: "data:image/png;base64," + images[index] } : require("assets/images/scan/CAT_scan_IDLE.png")} style={{ height: 50, resizeMode: "contain", width: 50, marginLeft: 10 }}></Image>
                <Text style={{ flex: 1, margin: 5, textAlignVertical: "center", fontFamily: typography.secondary, color: color.scanTextGrid, fontSize: 15 }} numberOfLines={3}>{item[1]}</Text>
                {(item[0] in scanValues) && <Image source={IMAGE_LETTERS[scanValues[item[0]]]} style={{ height: 50, resizeMode: "contain", width: 50, marginLeft: 10 }} />}
              </View>
              {
                (item[0] in versions) &&
                <Text style={{color: color.scanTextGrid, margin: 10}}>
                  {t("scan:install", "Il n'y a pas de rapport pour cette version installée")} ({versions[item[0]].installed}), {t("scan:version", "les informations affichées sont basées sur une autre version")} ({versions[item[0]].checked})
                </Text>
              }
            </View>
          }}
        />
      </View>
    </View>}
    {
      showPopup && showLoading && <View style={{justifyContent: "center", alignItems: "center", position: "absolute", width: "100%", height: "100%", backgroundColor: color.scanBackgroundLoading}}>
        <ProgressBar styleAttr="Horizontal" style={{width: "80%"}} />
        <Text style={{color: color.palette.letterGreen, margin: 20, alignSelf: "center", textAlign: "center"}} numberOfLines={3}>{t("scan:course", "CatScan en cours, cela prendra un peu de temps, mais s'il te plaît reste sur cette page pour que nous puissions générer ton résultat !")}</Text>
      </View>
    }
    {
      thoughtSkill &&
      <BlazeNotebookSkillPopup navigation={navigation} skill={idSelectedSkill} thought={thoughtSkill} onOk={() => setThoughtSkill(null)} />
    }
    {
      thought &&
      <BlazeNotebookThoughtPopup navigation={navigation} thought={thought} onOk={() => {
        setThought(null)
      }
      } />
    }
    {
      showScanPopupScreen && <ScanPopupScreen onClose={() => {
        setShowLoading(false)
        setShowScanPopupScreen(false)
      }} />
    }
    {showQuestionary && <View style={{position: "absolute", width: "100%", height: "100%"}}><LastMessageQuestionaryScreen onFinish={() => {
      setShowQuestionary(false)
    }} /></View>}
    <Toast ref={(ref) => Toast.setRef(ref)} />
  </>)
}
