import * as React from "react"
import {Image, ScrollView, View} from "react-native"
import {BlazeContainer, CheckboxList, Text} from "../components"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"
const GAO_GAMES = t => [
  {key: t("gao:intruder", "Gao The Intruder"), checked: true},
  {key: t("gao:sage", "Gao The Sage"), checked: true},
  {key: t("gao:athlete", "Gao The Athlete"), checked: true},
  {key: t("gao:photographer", "Gao The Photographer")},
  {key: t("gao:file-keeper", "Gao The File Keeper")},
]

export interface BlazeTutorialsScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeTutorialsScreen: React.FunctionComponent<BlazeTutorialsScreenProps> = BlazeContainer((props) => {
  const {t} = useTranslation()
  return (
    <>
      <View style={styles.topBarView}>
        <Text style={styles.h2Text}>
          {t("blaze:tool", "Outil")} 1: NAME
          <Text style={{...styles.h2Text, ...styles.boldFontText}}> {t("blaze:learned", "LEARNED")}</Text>
        </Text>
        <View style={styles.spacerView} />
        <Image style={styles.iconImage} source={require("assets/images/blaze-icons/left-arrow.png")} />
      </View>
      <ScrollView style={styles.wrapperView}>
        <Text style={{...styles.h2Text, ...styles.boldFontText}}>
          {t("blaze:howto.permissions", "How to manage android permissions?")}
        </Text>
        <Text style={styles.pText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam cursus risus turpis, non egestas urna
          consequat at. Cras varius eros eget justo placerat viverra. In a dignissim felis. In tempor faucibus mauris
          ut luctus.
        </Text>
        <View style={styles.centeredView}>
          <Image style={styles.screenshotImage} source={require("assets/images/placeholder/blaze-screenshot.png")} />
          <Image style={styles.screenshotImage} source={require("assets/images/placeholder/blaze-screenshot.png")} />
        </View>
        <Text style={{...styles.h2Text, ...styles.boldFontText}}>
          {t("blaze:remove-permissions", "Remove GAO GAMES permissions")}
        </Text>
        <CheckboxList theme="blaze" data={GAO_GAMES(t)} />
      </ScrollView>
    </>
  )
}, {isDebugMode: false})
