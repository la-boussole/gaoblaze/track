import * as React from "react"
import {Image, View} from "react-native"
import {BlazeContainer, BlazeThought, TabView, Text} from "../components"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"

const TABS = t => [
  { key: "knowledge", title: t("blaze:knowledge", "Knowledge"), selected: true },
  { key: "skills", title: t("blaze:skills", "Skills") }
]

const THOUGHTS : string[] = [
  "Thought name",
  "Thought name",
  "Thought name",
]

export interface BlazeNotebookKnowledgeScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeNotebookKnowledgeScreen: React.FunctionComponent<BlazeNotebookKnowledgeScreenProps> = BlazeContainer((props) => {
  const {t} = useTranslation()
  return (
    <>
      <View style={styles.topBarView}>
        <Text style={styles.h2Text}>
          <Text style={{...styles.h2Text, ...styles.boldFontText}}>{t("common:blazes", "Blaze's")} </Text>
          {t("blaze:thoughts", "Thoughts")}
        </Text>
        <View style={styles.spacerView} />
        <Image style={styles.iconImage} source={require("assets/images/blaze-icons/left-arrow.png")} />
      </View>
      <TabView theme="blaze" tabs={TABS(t)} Component={THOUGHTS.map((thought, index) => (
        <BlazeThought index={index} thought={thought} />
      ))} />
    </>
  )
})
