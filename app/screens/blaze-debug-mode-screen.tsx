import * as React from "react"
import {View} from "react-native"
import { BlazeContainer, ImageButton, ScrollableList, Text } from "../components"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "../blaze-theme/styles"
import {useTranslation} from "react-i18next"
import { observer } from "mobx-react-lite"
import { useStores } from "models/root-store"

// TODO: it should be in a MobX store, that's some global game state regarding the unlock status of profiles.
const NAMES :  {
  key: string;
  selected?: boolean;
  enabled?: boolean;
  hasAnimation?: boolean;
}[] = [
  {key: 'Sally'},
  {key: 'Pierre'},
  {key: 'Molly'},
  {key: 'Alex', enabled: true},
  {key: 'Fred', enabled: true},
  {key: 'Amin', hasAnimation: true},
  {key: 'John', enabled: true},
  {key: 'Judith'},
  {key: 'Bill'},
  {key: 'Nikki', enabled: true, selected: true},
  {key: 'Bob'},
  {key: 'Eugene'},
  {key: 'Maria'},
  {key: 'Mariane'},
  {key: 'Billy'},
  {key: 'Devin'},
  {key: 'Dan'},
  {key: 'Dominic'},
  {key: 'Jackson'},
  {key: 'James'},
  {key: 'Masako', enabled: true, selected: true, hasAnimation: true},
  {key: 'Joel'},
  {key: 'Jillian'},
  {key: 'Jimmy'},
  {key: 'Julie'},
];

export interface BlazeDebugModeScreenProps {
  navigation: NavigationScreenProp<{}>
}
// TODO: change wallpaper from blazecontainer + refactor (?)
export const BlazeDebugModeScreen: React.FunctionComponent<BlazeDebugModeScreenProps> = BlazeContainer(observer((props) => {
  const { t } = useTranslation()
  const { navigation } = props
  const { metaGame } = useStores()

  return (
    <>
      <View style={styles.topBarView}>
        <Text style={styles.h2RedText}>
          {t("blaze:debug-mode", "Debug Mode")}
        </Text>
        <View style={styles.spacerView} />
        {metaGame.notebookUnlocked() && (
          <ImageButton
            style={styles.iconImage}
            uri={require("assets/images/blaze-icons/message_red.png")}
            onPress={() => navigation.navigate("notebook")}
          />
        )}
        {metaGame.blazeChatUnlocked() && (
          <ImageButton
            style={styles.iconImage}
            uri={require("assets/images/blaze-icons/book_red.png")}
            onPress={() => navigation.navigate("chat")}
          />
        )}
      </View>
      <ScrollableList
        preset="debug"
        data={NAMES.map(user => ({...user, onPress: () => navigation.navigate("profile", {
          character: user.key,
          showAnimation: user.hasAnimation || false
          })}))} />
    </>
  )
}), { isDebugMode: true })
