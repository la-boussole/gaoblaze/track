import * as React from "react"
import {BackHandler, FlatList, TouchableOpacity, View} from "react-native"
import {Text, ImageButton, ChatMessage, Button, StoryChoices, BlazeThoughtPopup, Wallpaper, Screen} from "../components"
import { NavigationScreenProp } from "react-navigation"
import { observer, useObserver } from "mobx-react-lite"
import * as styles from "../blaze-theme/styles"
import { useStores } from "models/root-store"
import { useTranslation } from "react-i18next"
import {useEffect, useRef, useState} from "react"
import {evaluatePseudoCondition} from "utils/chat-engine/node-visitor"
import {color} from "blaze-theme"
import membersData from "assets/community_members/profile"
import {validateSpecialTriggers} from "../models/character-profile/character-profile"
//import { BLAZE_PROFILES } from "assets/community_members/blaze-profiles"
import { BLAZE_PROFILES_FR } from "assets/community_members/blaze-profiles-FR"
import { BLAZE_PROFILES_ES } from "assets/community_members/blaze-profiles-ES"
import { BLAZE_PROFILES_EN } from "assets/community_members/blaze-profiles-EN"
import i18n from "i18n/i18n"

export interface BlazeChatScreenProps {
  navigation: NavigationScreenProp<{}>
  characterNotification: string
}

const deleteStrings = [
  "emoji_sad",
]

export const BlazeChatScreen: React.FunctionComponent<BlazeChatScreenProps> = (observer((props) => {
  // const { someStore } = useStores()

  const BLAZE_PROFILES = i18n.language === 'es' ? BLAZE_PROFILES_ES : i18n.language === 'en' ? BLAZE_PROFILES_EN : BLAZE_PROFILES_FR

  const THOUGHT_MESSAGES = [
    {
      messages: ["Yep.  Nous sommes les seuls à pouvoir lire ces messages. C’est la magie du chiffffrement bout en bout. Tu t’en rends pas compte parce qu’on s’écrit normalement, mais contrairement au mini chat de gao.games, personne ne peut espionner nos conversations.\nSauf si tu fais des captures d’écrans et que tu les mails à quelqu’un du Studio. Au hasard, cette fouine d’Ally.\nMais pourquoi tu ferais ça, hein, bb ?",
        "Sí. ¿Cool, no? Tú y yo somos las únicas cuentas con acceso a estos mensajes. ¡Qué viva la magia del cifrado de extremo a extremo! Lo chistoso es que, exceptuando a la persona que espía tus conversaciones, es lo mismo que el chat de Gao.\n\nLa única manera que hay para que el Estudio sepa de esto es que saques pantallazos de lo que hablamos y se los mandes a Ally... Pero tú no harías eso. ¿Cierto que no, baby?",
        "Yeah. Thank God for the magic of end-to-end encryption; privacy. \n\nThe best thing is that nothing is different, we can write just like we would on Gao, except there's nobody eagerly waiting to read what we say.\n\nThe only way they could have access to what we're saying is if you took some screenshots and sent them to someone within the Studio. Like Ally, for example.\n\nBut you wouldn't do that, would you, baby?"],
      thought: BLAZE_PROFILES.find(item => item.key === "Nikki-Thought-1").thought
    },
    {
      messages: ["Impossible de savoir s’ils le font systématiquement ou non, mais t’as capté : ils le peuvent s’ils le veulent.\nEt c’est pareil pour tous tes appels téléphoniques, les photos ou vidéos que t’envoies, les endroits où tu vas, etc. Dès que tu donnes une autorisation d’accès à une appli, toutes tes données peuvent être potentiellement exploitées.\nEt après, tout finit dans une base donnée, comme celle-ci.",
        "Es imposible saber si lo hacen sistemáticamente o no. Lo que sí es que tienen acceso a ellos, y que pueden hacer con ellos lo que quieran.\nLo mismo pasa con tus llamadas, con las fotos y los videos que compartes, los lugares en los que estás, etc... Apenas le das permiso a una aplicación, esta puede utilizar tus datos como quiera.\n\nY al final todo termina en una base de datos, como esta.",
        "It's impossible to know if they do it systematically, but either way, they have access to it whenever they want.\n\nAnd it's the same for your calls, the pictures or videos you send, the places you visit, etc... the moment you give them access, they're free to do with your information almost anything they want."],
      thought: BLAZE_PROFILES.find(item => item.key === "Nikki-Thought-2").thought
    },
    {
      messages: ["Principalement à faire de l’argent.\nMais ça ne concerne pas que ta petite tête. Ce sont les données de milliards d’individus qui sont collectées. En vrai, ça prend une place de fou. Toute cette montagne hallucinante d’information est stockée dans de gigantesques centre-serveurs alimentés en électricité 24h/24.\nJe te raconte pas la facture énergétique.",
        "Para venderlas, principalmente.\n\nImagina la cantidad de datos que se almacenan a diario; millones de personas dan permiso a todo todo el tiempo. En realidad es tanta la cantidad de datos que se almacenan que ocupa espacio de verdad: la humanidad está construyendo en todas partes Centros de Almacenamiento de Data gigantescos que mantienen prendidos 24/7.\n\nNo me imagino el recibo de la luz. xD",
        "Like making money. A LOT.\n\nEvery day, the information of MILLIONS of people is collected. What's crazy is that it's so much information that it actually takes physical space to store it. These mountains of information are stored in giant data centers that are online 24/7.\n\nCan't imagine the electricity bill. ><"],
      thought: BLAZE_PROFILES.find(item => item.key === "Nikki-Thought-5").thought
    },
    {
      messages: ["Je me suis infiltré.e dans le petit groupe de trolls qui la harcèlent.\nIls utilisent une messagerie via une app chiffrée\nTout le monde est sous pseudo\nTout le monde fait super gaffe à son anonymat.\nC’est propre.\nUn peu trop, à mon avis.\nÇa sent le truc piloté. Ya trop d’argent en jeu.",
        "... Puede que no sepa quienes son realmente. Pero después de infiltrarme en su grupito descubrí un par de cosas:\n1. El chat que utilizan está encriptado.\n2. Utilizan VPNs.\n3. Son hiper-precavidos con su identidad.\n\nTodo está demasiado bien hecho... No parece un grupo de \"jugadores enfurecidos\", parece más bien a un grupo organizado con dinero de por medio.",
        "Ha! Didn't Alex tell you I infiltrated their group?\n\nAnd I learned a few things at that: \n1. Their messaging is encrypted. \n2. They all use fake accounts and VPNs. \n3. They are all very sketchy about their information.\n\nSmells fishy if you ask me... smells like there's money involved."],
      thought: BLAZE_PROFILES.find(item => item.key === "Nikki-Thought-3").thought
    },
    {
      messages: ["Je t’assure, personne ne veut se justifier de la liste des sites qu’il consulte.\nToi-même, tu ne ferais pas les mêmes recherches si tu avais quelqu’un en permanence en train de lire par dessus ton épaule. Et pourtant...",
        "Te GARANTIZO que nadie quisiera tener que justificar su historial de navegación en internet.\nEstoy segura que tú no harías las mismas búsquedas si tuvieras a alguien detrás tuyo 24 horas al día.",
        "They're the same!\n\nI guarantee you, NOBODY would want to explain everything they do online. I'm sure YOU wouldn't do the same things online that you do now if you had someone looking over your shoulder all the time."],
      thought: BLAZE_PROFILES.find(item => item.key === "Nikki-Thought-4").thought
    }
  ]

  const rootStore = useStores()
  const { t } = useTranslation()
  const flatListRef = useRef(null)
  const { navigation } = props
  const [thought, setThought] = useState(null)

  const characterNotification = navigation.getParam("character", "NikkiBlaze")
  const { communityStore, metaGame } = rootStore

  const [character, setCharacter] = React.useState(characterNotification)

  const characterStore = communityStore.characters.get(character)

  const showAlex = metaGame.showAlexBlazeChat 

  const chatHistory = characterStore ?
    characterStore.gaoChatHistory // Is a snapshot of our current interactions.
    : null

  // Update only upon change to the character parameter.
  useEffect(() => {
    // FIXME: ideally, that should be goBack.
    // We kick invalid characters back to explore screen.
    communityStore.selectCharacter(character, "gao", rootStore.gameState())
    metaGame.markAsRead(character) // Mark as read message.
    if (flatListRef && flatListRef.current) flatListRef.current.scrollToEnd()
  }, [character, thought])

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", () => {
      if(metaGame.goToEnd) {
        navigation.navigate("blazeLastMessage")
      } else {
        navigation.navigate("database")
      }
      return true
    })
    return () => {
      handler.remove()
    }
  }
  ,[]
  )

  const [waitForUser, setHiddenStatus] = useState(chatHistory
    ? chatHistory.requireUserInteraction
    : false)

  if (!characterStore || !characterStore.gaoChatHistory) {
    return (
      <Screen style={styles.rootView} preset="scroll">
        <Wallpaper backgroundImage={require("assets/images/wallpaper/blaze/wallpaper_green.png")} />
        <View style={styles.containerView}></View>
      </Screen>
    )
  }

  const messages = chatHistory.staticMessages.filter(item => {
    if (item.text)
      return !deleteStrings.includes(item.text)
    return true
  }).map(item => {
    let message = item.storedValue
    if (message.text) {
      let newText = message.text
      deleteStrings.forEach(search => {
        newText = newText.replace(search, "")
      })
      while (newText.startsWith("\n"))
        newText = newText.substr(1)
      message = { ...message, text: newText }
    }
    return message
  })

  const renderedMessages = messages.map(message =>
    (<ChatMessage
      textStyle={message.isMe ? styles.blazeChatMessageMe : styles.blazeChatMessage}
      key={message.text}
      imageChat={membersData[character].avatar}
      writerName={message.isMe ? "me" : character}
      message={message}
      dynamic={false}
      isBlaze={true}
      writingStyle={{color: color.primaryDarker}}
    />))

  let resultCondition = true
  if(chatHistory.currentMessage) {
    const condition = evaluatePseudoCondition(chatHistory.currentMessage)
    resultCondition = condition(metaGame) && validateSpecialTriggers(chatHistory.scenario.currentNode.id, metaGame)
  }

  if (!waitForUser && chatHistory.currentMessage && resultCondition) {
    const isInitNode = chatHistory.scenario.currentNode.isInitNode
    if(chatHistory.currentMessage.text) {
      communityStore.fireDependency(chatHistory.currentMessage.text)
      metaGame.refreshChatTriggers(navigation, t)
    }

    metaGame.textChoice(chatHistory.currentMessage.text, navigation)

    if(!thought) {
      const findThought = THOUGHT_MESSAGES.find(item => item.messages.includes(chatHistory.currentMessage.text))
      if (findThought) {
        metaGame.addShowTouhght(findThought.thought.title, true, t)
        //setThought(findThought.thought)
      }
    }

    renderedMessages.push(
      (<ChatMessage
        textStyle={chatHistory.currentMessage.isMe ? styles.blazeChatMessageMe : styles.blazeChatMessage}
        message={chatHistory.currentMessage}
        dynamic={!isInitNode}
        imageChat={membersData[character].avatar}
        writerName={chatHistory.currentMessage.isMe ? "me" : character}
        onShown={chatHistory.archive}
        writingStyle={{color: color.primaryDarker}}
        isBlaze={true}
      />)
    )
  }


  const internalView = (
    <View style={styles.blazeChatContainerView}>
      <FlatList
        ref={flatListRef}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => item}
        data={renderedMessages}
        vertical
        //ListFooterComponent={<View style={{height:15}}></View>}
        contentContainerStyle={{paddingBottom:50}}
        onContentSizeChange={() => flatListRef.current.scrollToEnd()}
        style={styles.blazeChatMessagesView} />
      {!waitForUser && chatHistory.currentChoices &&
      (<StoryChoices
        style={styles.blazeChatStoryChoiceView}
        textStyle={styles.blazeChatStoryChoice}
        textStyleSelected={styles.blazeChatStoryChoiceSelected}
        canChoose={chatHistory.hasFinishedToWrite}
        choices={chatHistory.currentChoices}
        selectedStyle={styles.blazeChatStoryChoiceViewSelected}
        icon={require("assets/images/blaze-icons/right-arrow.png")}
        onResize={() => {
          flatListRef.current.scrollToEnd()
        }}
        onChoose={choice => {
          return chatHistory.choose(rootStore.gameState(), choice, -1)
        }}
      />)}
    </View>)


  return (
    <>
      <Screen style={styles.rootView} preset="scroll">
        <Wallpaper backgroundImage={require("assets/images/wallpaper/blaze/wallpaper_green.png")} />
        <View style={styles.containerView}>
          {
            thought &&
        <BlazeThoughtPopup navigation={navigation} characterInfo={{thought: thought}} onOk={() => setThought(null) } />
          }
          <View style={styles.blazeChatContainer}>
            <View style={styles.topBarView}>
              <Text style={styles.h2Text}>
                <Text style={{...styles.h2Text, ...styles.boldFontText}}>{t("blaze:Chat", "Chat")} </Text>
              </Text>
              <View style={styles.spacerView} />
              {metaGame.notebookUnlocked() && (
                <ImageButton
                  style={styles.iconImage}
                  uri={require("assets/images/blaze-icons/book.png")}
                  onPress={() => {
                    if(metaGame.goToEnd) {
                      navigation.navigate("blazeLastMessage")
                    } else {
                      navigation.navigate("notebook")
                    }
                  }}
                />
              )}
              {(
                <ImageButton
                  style={styles.iconImage}
                  uri={require("assets/images/blaze-icons/left-arrow.png")}
                  onPress={() => {
                    if(metaGame.goToEnd) {
                      navigation.navigate("blazeLastMessage")
                    } else {
                      navigation.navigate("database")
                    }
                  }}
                />
              )}
            </View>
            <View style={styles.blazeChatContainerTab}>
              <View style={styles.blazeChatTabs}>
                <TouchableOpacity style={character === "NikkiBlaze"?styles.blazeChatTabSelected:styles.blazeChatTab} onPress={() => {
                  if(character !== "NikkiBlaze")
                    setCharacter("NikkiBlaze")
                }}>
                  <Text style={character === "NikkiBlaze"?styles.blazeChatTabSelectedText:styles.blazeChatTabText}>Nikki</Text>
                </TouchableOpacity>
                {showAlex && <TouchableOpacity style={character === "AlexBlaze"?styles.blazeChatTabSelected:styles.blazeChatTab} onPress={() => {
                  if(character !== "AlexBlaze")
                    setCharacter("AlexBlaze")
                }}>
                  <Text style={character === "AlexBlaze"?styles.blazeChatTabSelectedText:styles.blazeChatTabText}>Alex</Text>
                </TouchableOpacity>}
              </View>
              <View style={styles.blazeChat}>
                {internalView}
              </View>
            </View>
          </View>
        </View>
      </Screen>
    </>
  )
}))
