import * as React from "react"
import {ImageButton, Wallpaper} from "../components"
import {Animated, Dimensions, Image, Text, TouchableOpacity, View} from "react-native"
import {createRef, useEffect, useRef, useState} from "react"
import {color} from "gao-theme"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next";

const IMAGES = [
  {
    image: require("assets/images/blaze-character-scenes/sol/01.png"),
    glowImage: require("assets/images/blaze-character-scenes/sol/01_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/01.png"),
    type: "button",
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/04.png"),
    glowImage: require("assets/images/blaze-character-scenes/sol/04_glow.png"),
  },
  {
    image: require("assets/images/download_gifs/Sol.gif"),
    waitTime: 4000,
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/07.png"),
    waitTime: 3000,
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/08.png"),
    glowImage: require("assets/images/blaze-character-scenes/sol/08_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/09.png"),
    glowImage: require("assets/images/blaze-character-scenes/sol/09_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/10.png"),
    glowImage: require("assets/images/blaze-character-scenes/sol/10_glow.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/11.png"),
    glowImage: require("assets/images/blaze-character-scenes/sol/11.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/sol/12.png"),
    waitTime: 3000,
  },
]

const CARDS = [
  {
    key: "C1",
    image: require("assets/images/blaze-character-scenes/sol/avatar1.png"),
    top: 40,
    left: 20,
  },
  {
    key: "C2",
    image: require("assets/images/blaze-character-scenes/sol/avatar2.png"),
    top: 130,
    right: 20,
  },
  {
    key: "C3",
    image: require("assets/images/blaze-character-scenes/sol/avatar3.png"),
    top: 220,
    left: 20,
  },
  {
    key: "C4",
    image: require("assets/images/blaze-character-scenes/sol/avatar4.png"),
    top: 310,
    right: 20,
  }
]

export const BlazeSolScreen = props => {
  const rootStore = useStores()
  const {navigation} = props
  const [count, setCount] = React.useState(0)
  const { metaGame } = useStores()
  const {t} = useTranslation()
  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(0.0))
  const [fadeAnimAll, setFadeAnimAll] = React.useState(new Animated.Value(0.0))
  const [checked, setChecked] = React.useState(0)
  const [loadImages, setLoadImages] = React.useState(IMAGES.filter(item => item.image).length - IMAGES.filter(item => item.image.uri).length)

  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("sol", null, t)
    fadeIn.start()
    return () => rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
  }, [])

  const fadeIn = Animated.timing(fadeAnimAll, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnimAll, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const finish = () => {
    fadeOut.start(() => {
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
    })
  }

  const startAnimation = () => {
    if (IMAGES[count].waitTime) {
      setTimeout(() => {
        if (count === IMAGES.length - 1) {
          finish()
        } else {
          setCount(prevCount => prevCount + 1)
        }
      }, IMAGES[count].waitTime)
    }
  }

  const startAnimationGlow = (anim, otherAnim) => {
    anim.start(({finished}) => {
      if (finished) {
        startAnimationGlow(otherAnim, anim)
      }
    })
    return true
  }

  const pressButton = (index) => {
    const newChecked = checked | (1<<index)
    setChecked(newChecked)
    if(newChecked === (1<<4) - 1)
      setCount(prevCount => prevCount + 1)
  }

  return (
    <>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {IMAGES.filter(item => item.image).map((item, index) => <Image key={"load-image-"+index} style={{opacity: 0, position: "absolute"}} source={item.image} onLoadEnd={() => setLoadImages(loadImages - 1)} />)}
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        /*loadImages <= 0 &&*/
    <Animated.View style={{flex: 1, opacity: fadeAnimAll}}>
      <View onTouchEndCapture={() => {
        if (IMAGES[count].glowImage) {
          asc.stop()
          desc.stop()
          if (count === IMAGES.length - 1) {
            finish()
          } else {
            setCount(prevCount => prevCount + 1)
          }
        }
      }} style={{flex: 1, backgroundColor: "white"}}>
        {
          IMAGES[count].glowImage && startAnimationGlow(asc, desc) &&
          (
            <Animated.View style={{zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%"}}>
              <Wallpaper
                backgroundImage={IMAGES[count].glowImage}/>
            </Animated.View>
          )
        }
        {IMAGES[count].waitTime && startAnimation()}
        <Wallpaper backgroundImage={IMAGES[count].image}/>
        {IMAGES[count].type === "button" && (
          CARDS.map((item, index) => {
            if((checked & (1<<index)) === 0) {
              return (
                <View key={item.key} style={{
                  height: 120,
                  position: "absolute",
                  width: 200,
                  top: item.top,
                  backgroundColor: "white",
                  padding: 10,
                  borderColor: color.palette.black,
                  borderWidth: 3,
                  borderRadius: 10,
                  right: item.right,
                  left: item.left,
                }}>
                  <Image source={item.image} style={{resizeMode: "contain", flex: 1, width: "100%"}}></Image>
                  <View style={{flexDirection: "row", height: 25,}}>
                    <TouchableOpacity style={{flex: 1}} onPress={() => pressButton(index)}>
                      <Image style={{resizeMode: "center", position: "absolute", width: "100%", height: "100%"}} source={require("assets/images/blaze-character-scenes/sol/btnred_0006.png")}></Image>
                      <Image style={{resizeMode: "center", width: "100%", height: "100%"}} source={require("assets/images/blaze-character-scenes/sol/x.png")}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex: 1}} onPress={() => pressButton(index)}>
                      <Image style={{resizeMode: "center", position: "absolute", width: "100%", height: "100%"}} source={require("assets/images/blaze-character-scenes/sol/btngreen_0005.png")}></Image>
                      <Image style={{resizeMode: "center", width: "100%", height: "100%"}} source={require("assets/images/blaze-character-scenes/sol/check.png")}></Image>
                    </TouchableOpacity>
                  </View>
                </View>
              )
            } else
              return <></>
          }
          ))}
      </View>
    </Animated.View>
      }
    </>
  )
}
