import * as React from "react"
import { observer } from "mobx-react-lite"
import { View } from "react-native"
import { Wallpaper, GaoTopbar, GaoDialog } from "../components"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { NavigationScreenProp } from "react-navigation"
import {useStores} from "models/root-store"

export interface SpecialLiveEventScreenProps {
  navigation: NavigationScreenProp<{}>
}


export const SpecialLiveEventScreen: React.FunctionComponent<SpecialLiveEventScreenProps> = observer((props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const { navigationStore } = useStores()
  return (
    //  FIXME: first time welcoming popup. NB: after closing return to home screen
    <View style={styles.wrapperColumnView}>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/special_event_background.png")} />
      <GaoTopbar navigation={navigation} /* TODO: use GaoContainer */></GaoTopbar>
      <View style={styles.containerView}>
        <GaoDialog
          preset= "live-announcement"
          textDialog1={t("common:live-announcement", "Entre avant qu’il ne soit trop tard!")}
          uriImage={require("assets/images/community/flag.png")}
          textButton={t("common:direct", "En direct")}
          withTimer={true}
          buttonOnPress= {() => {navigationStore.navigateTo("gao_celebration")}}
        />
      </View>
    </View>
  )
})
