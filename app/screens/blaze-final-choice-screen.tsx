import * as React from "react"
import {SafeAreaView, View} from "react-native"
import {Button, Screen, Text, Wallpaper} from "../components"
import {spacing} from "../blaze-theme"
import {NavigationScreenProp} from "react-navigation"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"

export interface BlazeFinalChoiceScreenProps {
  navigation: NavigationScreenProp<{}>
}

export const BlazeFinalChoiceScreen: React.FunctionComponent<BlazeFinalChoiceScreenProps> = props => {
  const {t} = useTranslation()
  return (
    <Screen style={styles.rootView} preset="scroll">
      <Wallpaper backgroundImage={require("assets/images/wallpaper/blaze/wallpaper_green.png")}/>
      <Wallpaper backgroundImage={require("assets/images/wallpaper/blaze/blaze-gao.png")} preset="center" />
      <SafeAreaView style={styles.containerView}>
        {/*TODO: use blaze-container here!*/}
        <View style={styles.centeredView}>
          <Text style={styles.h2Text}>
            {t("blaze:final-choice", "What will you do with all this information?")}
          </Text>
        </View>
        <Button theme="blaze" preset="default" textStyle={styles.boldFontText} style={{marginVertical: spacing.medium}}
          text={t("blaze:final-choice-1", "I will destroy them, so nobody gets hurt.")} />
        <Button theme="blaze" preset="default" textStyle={styles.boldFontText} style={{marginVertical: spacing.medium}}
          text={t("blaze:final-choice-2", "I will hand it over to ALex and encourage her to lead legal action against the Publishert.")} />
        <Button theme="blaze" preset="default" textStyle={styles.boldFontText} style={{marginVertical: spacing.medium}}
          text={t("blaze:final-choice-3", "I will surrender it to the Publisher. I want Paws!")} />
      </SafeAreaView>
    </Screen>
  )
}
