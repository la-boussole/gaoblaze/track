import { BlazeChatScreen } from "screens/blaze-chat-screen"
import { BlazeDatabaseScreen } from "screens/blaze-database-screen"
import { BlazeFinalChoiceScreen } from "screens/blaze-final-choice-screen"
import { BlazeLoginScreen } from "screens/blaze-login-screen"
import { BlazeNotebookScreen } from "screens/blaze-notebook-screen"
import { BlazeOnlinePlayerAssessmentScreen } from "screens/blaze-online-player-assessment-screen"
import { BlazeProfileScreen } from "screens/blaze-profile-screen"
import { BlazeTutorialsScreen } from "screens/blaze-tutorials-screen"
import { Blaze02MasakoScreen } from "screens/blaze-02-masako-screen"
import {Blaze07AllyScreen} from "screens/blaze-07-ally-screen"
import {BlazeLucaScreen} from "screens/blaze-luca-screen"
import {BlazeSolScreen} from "screens/blaze-sol-screen"
import {BlazeAjayScreen} from "screens/blaze-ajay-screen"
import {BlazeAminScreen} from "screens/blaze-amin-screen"
import {BlazeRokayaScreen} from "screens/blaze-rokaya-screen"
import { BlazeNotebookPDFScreen } from "screens/blaze-notebook-pdf-screen"
import { LastMessageScreen } from "screens/last-message-screen"
import { ScanPhoneScreen } from "screens/scan-phone-screen"
import {CreditScreen} from "screens/credit-screen";
import {ScanPopupScreen} from "screens/scan-popup-screen";

export default {
  screens: {
    credits: CreditScreen,
    blazePDF: BlazeNotebookPDFScreen,
    blazeChat: BlazeChatScreen,
    scan: ScanPhoneScreen,
    database: BlazeDatabaseScreen,
    finalChoice: BlazeFinalChoiceScreen,
    login: BlazeLoginScreen,
    notebook: BlazeNotebookScreen,
    onlinePlayerAssessment: BlazeOnlinePlayerAssessmentScreen,
    profile: BlazeProfileScreen,
    tutorials: BlazeTutorialsScreen,
    aminAnimation: BlazeAminScreen,
    masakoAnimation: Blaze02MasakoScreen,
    allyAnimation: Blaze07AllyScreen,
    lucasAnimation: BlazeLucaScreen,
    solAnimation: BlazeSolScreen,
    ajayAnimation: BlazeAjayScreen,
    rokayaAnimation: BlazeRokayaScreen,
    lastMessage: LastMessageScreen,
    scanPopupScreen: ScanPopupScreen
  },
  exitRoutes: ["login"],
  meta: {
    initialRouteName: "login"
  }
}
