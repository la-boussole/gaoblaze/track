import React, {useCallback} from "react"
import {Image, View} from "react-native"
import { NavigationScreenProp } from "react-navigation"
import {Button, Wallpaper, GaoTopbar, GaoDialog, GaoCharacter} from "components"
import {useTranslation} from "react-i18next"
import * as styles from "gao-theme/styles"
import {useStores} from "models/root-store"
import {useEffect} from "react"
import {useObserver} from "mobx-react-lite"
import { TouchableOpacity } from "react-native-gesture-handler"
import {palette} from "gao-theme/palette";

export interface HomeScreenProps extends NavigationScreenProp<{}> {
  onPlay: () => void;
  onTradePaws: () => void;
  onGoBack: () => void;

}

export const HomeScreen: React.FunctionComponent<HomeScreenProps> = (props) => {
  const {t} = useTranslation()
  const {navigation} = props
  const {onPlay} = props
  const {onTradePaws} = props
  const {onGoBack} = props
  const {metaGame, communityStore} = useStores()
  const rootStore = useStores()
  const [size, setSize] = React.useState(null)
  const { navigationStore } = useStores()
  const [showDialog, setShowDialog] = React.useState(null)

  useEffect(() => {
    console.log("Use effect")
    metaGame.refreshChatTriggers(navigation, t)
    if(rootStore.isFirstStart) {
      communityStore.selectCharacter("Alex", "gao", rootStore.gameState)
      communityStore.addSelectedProfileToFriends()
    }
  }, [])

  const COMPUTER_UBICATION = [613, 588, 844 - 613, 731 - 588]
  const IMAGE_SIZE = [1024, 1462]

  const goToBlaze = () => {
    rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
    if(metaGame.blazeLogged)
      navigationStore.navigateTo("database")
    else
      navigationStore.navigateTo("login")
  }

  return useObserver( () => (
    <>
      {showDialog === 2 && <GaoDialog
        textDialog2={t(
          "common:last-announcement",
          "Jouer à différents jeux te permettra de débloquer l'accès à la communauté des Gao Games. Les discussions te feront avancer dans le jeu !\n",
        )}
        textStyle2={styles.infoText}
        textButton={t("common:ok", "Ok")}
        buttonOnPress={() => {
          setShowDialog(null)
          onPlay()
        }}
      /> }
      {showDialog === 1 && <GaoDialog
        textDialog2={t(
          "common:announcement",
          "Joue à des jeux pour obtenir des papattes et gagner des récompenses dans le magasin !\n",
        )}
        textStyle2={styles.infoText}
        textButton={t("common:ok", "Ok")}
        buttonOnPress={() => {
          setShowDialog(2)
        }}
      />}
      {metaGame.showBlazeMessageGaoTheVagabond && (
        <View style={styles.wrapperColumnView}>
          <Wallpaper backgroundImage={require("assets/images/wallpaper/HomescreenWelcome.png")} />
          <View style={styles.wrapperView}>{/* FIXME: In order to fill the screen space */}</View>
          <GaoDialog
            textDialog2={t(
              "common:annoucement",
              "Salut ! Juste pour vous dire que Gao Games a été racheté par une Grande maison d'édition. Afin de fêter ce succès, j'ai développé un nouveau jeu Gao, jetez-y un coup d'oeil ! \n",
            )}
            textStyle2={styles.infoText}
            textButton={t("common:ok", "Ok")}
            buttonOnPress={() => {
              metaGame.setShowGaoTheVagabond()
              metaGame.setShowBlazeMessageGaoTheVagabond()
            }}
          />
          <GaoCharacter
            positioning={{paddingLeft: 10}}
            uriImage={require("assets/images/character/blaze.png")}
            imageStyle={styles.avatarBlazeWelcomeImage}
            displayBottom= {true}
          />
        </View>
      )}
      {!metaGame.showBlazeMessageGaoTheVagabond && (
        <View style={styles.wrapperColumnView} onLayout={(layout) => setSize(layout.nativeEvent.layout)}>
          <Wallpaper backgroundImage={metaGame.developMode?require("assets/images/wallpaper/background_matrix.png"):require("assets/images/wallpaper/HomeScreenAlpha.png")} disable={rootStore.isFirstStart} />
          {metaGame.developMode && size &&
          <View onTouchEndCapture={() => goToBlaze()} style={[styles.matrixImageContainer,
            // eslint-disable-next-line react-native/no-inline-styles
            {
              left: COMPUTER_UBICATION[0]*size.width/IMAGE_SIZE[0],
              top: COMPUTER_UBICATION[1]*size.height/IMAGE_SIZE[1],
              width: COMPUTER_UBICATION[2]*size.width/IMAGE_SIZE[0],
              height: COMPUTER_UBICATION[3]*size.height/IMAGE_SIZE[1],
            }]}>
            <Image style={styles.matrixImage} source={require("assets/images/common/matrix.png")} />
          </View>
          }
          <GaoTopbar onGoBack={onGoBack} navigation={navigation} /* TODO: use GaoContainer */ />
          <View style={styles.wrapperView}>{/* FIXME: In order to fill the screen space */}</View>
          {/* TODO: add gao background animation */}
          {/* FIXME : show screens/gao-welcoming-screen at first launch asking user name */}
          <View style={{ flexDirection: "row" }}>
            <Button
              style={[styles.wrapperView, !rootStore.isEndedChat?{backgroundColor: palette.greenDisabled, borderColor: palette.grey}: null]}
              text={t("common:play", "Jouer")}
              textStyle={[styles.homeButton, !rootStore.isEndedChat?{color: palette.grey}:null]}
              theme="gao"
              preset="play"
              onPress={() => {
                if(rootStore.isFirstStart) {
                  setShowDialog(1)
                } else {
                  onPlay()
                }
              }}
              disabled={!rootStore.isEndedChat}
            />
            <Button
              style={[styles.wrapperView, rootStore.isFirstStart?{backgroundColor: palette.orangeDisabled, borderColor: palette.grey}: null]}
              text={t("common:trade-paws", "Convertir les papattes")}
              textStyle={[styles.homeButton, rootStore.isFirstStart?{color: palette.grey}:null]}
              theme="gao"
              preset="store"
              onPress={() => onTradePaws()}
              disabled={rootStore.isFirstStart}
            />
          </View>
          { false && (<Button
            text={"Go to developer screen"}
            onPress={() => {
              rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
              navigation.navigate("developer")
            }}
            theme="gao"
            preset="developer"
          />)}
        </View>) }
        { /*
        <Button text='Hola mundo' style={{position: 'absolute', left: 0, top: "50%"}} 
          onPress={() => {
            const date = new Date().getTime();
            rootStore.addToast({
              message: "Alooooo " + date + " Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo Alooooo ",
              title: "Title Title" + date,
              vibrate: date%2 === 1,
              soundName: date%2 ? "blaze_message_in.ogg" : "message_in.ogg",
              largeIcon: require("assets/images/community/avatars/00_Sol.png"),
              userInfo: {  },
            })
          }}/>
        */}
    </>)
  )
}
