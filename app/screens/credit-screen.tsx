import * as React from "react"
import {Animated, Image, ScrollView, Text, View} from "react-native"
import {Wallpaper} from "components"
import * as styles from "gao-theme/styles"
import {color} from "blaze-theme"
import { useTranslation } from "react-i18next"
import {useEffect, useRef, useState} from "react"

export interface CreditScreenProps {

}

const BUTTON_UBICATION = [174, 208, 523 - 174, 292 - 208]
const IMAGE_SIZE = [720, 1280]

export const CreditScreen: React.FunctionComponent<CreditScreenProps> = (props) => {
  const [scroll, setScroll] = useState(false)
  const {navigation} = props
  const refScroll = useRef()
  const {t} = useTranslation()

  return (<View style={{flex: 1, padding: 0}}>
    <Image style={{position: "absolute", width: "100%", height: "110%", top: -10, resizeMode: "cover"}} 
      source={require("assets/images/wallpaper/blaze/wallpaper_green.png")} />
    <Image source={require("assets/images/scan/GAO_LOGO.png")} style={{height: 100, resizeMode: "contain", margin: 20}} />
    <View onTouchEndCapture={() => navigation.navigate("scan")} style={{height: 40, marginBottom: 20}}>
      <Image style={[styles.matrixImage, {resizeMode: "contain"}]} source={require("assets/images/credits/empty_buttom.png")}/>
    </View>
    <Text style={{
      color: color.palette.lightGreen,
      fontSize: 40,
      fontFamily: "Roboto-Black",
      alignSelf: "center",
      marginBottom: 10
    }}>{t("blaze:credits-title","Crédits")}</Text>
    <ScrollView scrollEnabled={scroll} ref={refScroll} style={{width: "80%", alignSelf: "center",}}
      onContentSizeChange={(w, h) => {
        let interval = null
        setScroll(false)
        setTimeout(() => {
          let scrollPosition = 0
          interval = setInterval(() => {
            if(scrollPosition <= h) {
              refScroll.current.scrollTo({x: 0, y: scrollPosition})
              scrollPosition = scrollPosition + 3
            } else {
              clearInterval(interval)
              interval = null
              setScroll(true)
            }
          }, 25)}
        , 5000)
      }} >
      <View>
        <Text style={{textAlign: "center", color: color.palette.lightGreen,
          fontSize: 18,
          fontFamily: "Roboto-Black",}}>
          {t("blaze:credits-subtitle","La Boussole, coopérative de formation et de recherche a porté ce projet")}
        </Text>
        <Text style={{textAlign: "center", color: color.palette.lightGreen,
          fontSize: 14,
          fontFamily: "Roboto-Regular",}}>
          {"laboussole.coop\n"}
          {"\n"}
          {"Natalia Calderón Beltrán, PhD\n"}
          {t("blaze:credits-natalia","Recherche, contenus pédagogiques, suivi du développement, gestion administrative et traduction")}
          {"\n"}
          {"\n"}
          {"Quentin Harada\n"}
          {t("blaze:credits-quentin","Conception ludique(game design), co-scénarisation, contenus pédagogiques, suivi du développement, coordination de la production")}
        </Text>
        <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
        <Text style={{textAlign: "center", color: color.palette.lightGreen,
          fontSize: 18,
          fontFamily: "Roboto-Black",}}>
          {t("blaze:credits-dev-title","Développement")}
        </Text>
        <Text style={{textAlign: "center", color: color.palette.lightGreen,
          fontSize: 14,
          fontFamily: "Roboto-Regular",}}>
          {"Alvaro Pérez Páez\n"}
          {"Juan Sebastián Beltrán Rojas"}
        </Text>
        <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      </View>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-scenario-title","Scénario et dialogues")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Claire Deambrogio"}{t("blaze:credits-author","(autrice principale)")}
        {"\n"}
        {"Quentin Harada"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-translate-title","Traduction vers l’anglais et l’espagnol")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Natalia Calderón Beltrán\n"}
        {"Tanilo Errazuriz"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-content-title","Contenus pédagogiques")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Natalia Calderón Beltrán\n"}
        {"Quentin Harada\n"}
        {"Yoann Spicher"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        Madbricks
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {t("blaze:credits-madbricks-title","Studio de jeu video")}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-sound-title","Game design et son")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Tanilo Errazuriz\n"}
        {"Quentin Harada"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-animation-title","Animation et Production")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Gabriel Vasco"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-direction-title","Direction artistique")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Juan David Peña\n"}
        {"Gabriel Vasco"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-art-title","Artistes UI")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Andrés Pascagaza"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-ani-title","Animation")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Gabriel Vasco"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        2D
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Douglas Hozart\n"}
        {"Mauro Alejandro Zúñiga"}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-thanks","Un grand merci pour leur contribution à:")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {"Exodus Privacy\n"}
        {"Studio Accidental Queens\n"}
        {"Thomas Etrillard\n"}
        {"Jean-Yves Ottmann\n"}
        {"Sylvain S\n"}
        {t("blaze:credits-participants","Participantes et participants à notre étude")}
        {"\n"}
        {t("blaze:credits-test","Nos bêta-testeurs et bêta-testeuses de choc qui peuplent la base de données de Blaze!")}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 18,
        fontFamily: "Roboto-Black",}}>
        {t("blaze:credits-project","Ce projet a été réalisé avec le soutien de la Fondation Maif pour la Recherche et l’Association Prévention Maif")}
      </Text>
      <Text style={{textAlign: "center", color: color.palette.lightGreen,
        fontSize: 14,
        fontFamily: "Roboto-Regular",}}>
        {t("blaze:credits-sushicat","(et merci à Sushicat d’avoir servi de modèle!)")}
      </Text>
      <View style={{width: "50%", height: 2, backgroundColor: color.palette.lightGreen, margin: 20, alignSelf: "center"}} />
    </ScrollView>
  </View>)
}
