import * as React from "react"
import { View, Alert, PermissionsAndroid } from "react-native"
import { NavigationScreenProps } from "react-navigation"
import Contacts from "react-native-contacts"
import {Wallpaper, GaoDialog, StateMachine, GaoContainer, secondsToPrettyTimeString, GaoFrame, Timer} from "components"
import { withTranslation, useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { connectRootStore } from "utils/decorators"
import {useEffect} from "react"
import {PlayBack} from "components/sound/play-back"
import { useStores } from "models/root-store"

/* *** Constants to be fine tuned by game design *** */

const PLAYER_GIVEN_TIME = 180

const ALIEN_CONTACT = {
  // FIXME: Translate me!
  recordID: "6b2237ee0df85980",
  backTitle: "",
  company: "",
  emailAddresses: [
    {
      label: "work",
      email: "Meow meow !",
    },
  ],
  familyName: "",
  givenName: "Gao",
  middleName: "",
  jobTitle: "",
  phoneNumbers: [
    {
      label: "mobile",
      number: "0123456789",
    },
  ],
  hasThumbnail: true,
  thumbnailPath: "content://com.android.contacts/display_photo/3",
  postalAddresses: [
    {
      label: "home",
      formattedAddress: "",
      street: "Chez Blaze, sur le bureau ou à côté du carton",
      pobox: "",
      neighborhood: "",
      city: "",
      region: "",
      state: "",
      postCode: "",
      country: "Paris",
    },
    {
      label: "home 1",
      formattedAddress: "",
      street: "At Blaze's, on the desk or next to the cardboard",
      pobox: "",
      neighborhood: "",
      city: "",
      region: "",
      state: "",
      postCode: "",
      country: "Paris",
    },
    {
      label: "home es",
      formattedAddress: "",
      street: "En la casa de Blaze, sobre el escritorio o al lado del cartón",
      pobox: "",
      neighborhood: "",
      city: "",
      region: "",
      state: "",
      postCode: "",
      country: "Paris",
    },
  ],
  prefix: "",
  suffix: "",
  department: "",
  birthday: { year: 1867, month: 11, day: 7 },
  note: "",
  imAddresses: []
}

/* *** */

// Start the game
const IntruderGameStart = props => {
  const { t } = useTranslation()
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "intruder:rules",
          "Lupin, le loup, et sa bande se sont échappés de captivité. Aide Gao à les retrouver !",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={props.askForPermission}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// Accept to share your phone contact
const IntruderGameIntro = props => {
  const { t } = useTranslation()
  const { setState } = props
  const rootStore = useStores()

  const startGame = () => {
    addAlienContact()
    // ..and start the timer

  }

  const addAlienContact = () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
      title: t("permissions:contacts.title", "Contacts"),
      message: t("permissions:contacts.message", "This app would like to view your contacts."),
      buttonPositive: t("permissions:contacts.accept", "Please accept bare mortal"),
    }).then((response) => {
      if(response === "denied"  || response === "never_ask_again") {
        setState({dialog: "GamePermissionDenied"})
      } else {
        rootStore.metaGame.addPermission("WRITE_CONTACTS")
        Contacts.addContact(ALIEN_CONTACT, err => {
          if (err === "denied") {
            setState({dialog: "GamePermissionDenied"})
          } else {
            setState({
              isAlienContactInPhone: true,
              alienContactAdded: true,
              dialog: "GameTimer",
              seconds: PLAYER_GIVEN_TIME,
            })
          }
        })
      }
    })
  }

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "intruder:welcome",
          "Un loup déguisé a infiltré les contacts de ton téléphone. Retrouve-le et efface-le avant la fin du chrono !",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={startGame}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// Game on with timer
const IntruderGameTimer = props => {
  const { state } = props
  const { setState } = props
  const {t} = useTranslation()
  const { metaGame } = props.rootStore
  const {navigation} = props

  const checkAlienContactInPhone = () => {
    const isAlienContact = x =>
      ALIEN_CONTACT.givenName == x.givenName
    Contacts.getAll((err, contacts) => {
      if (err === "denied") {
        setState({dialog: "GamePermissionDenied"})
      } else {
        if(contacts.filter(isAlienContact).length <= 0 && state.isAlienContactInPhone)
          setState({
            isAlienContactInPhone: contacts.filter(isAlienContact).length > 0,
          })
      }
    })
  }

  return (
    <View style={styles.containerView}>
      <Timer {...props} playerGivenTime={PLAYER_GIVEN_TIME} textStyle={styles.timerIntruderView} finishTimer={() => setState({ dialog: "GameOver", timerID: null })}
        change={(timer, timerRef) => {
          console.log("el timer", timer)
          checkAlienContactInPhone()
          if (state.alienContactAdded && !state.isAlienContactInPhone) {
            //BackgroundTimer.clearInterval(timer)
            timerRef.stop()
            metaGame.reward(10)
            metaGame.playGame("TheIntruder", navigation, t)
            setState({
              dialog: "GameSuccess",
              timerID: null,
              alienContactAdded: false, // to be able to restart the game nicely
            })
          }
        }}/>
    </View>
  )
}

const IntruderGameSuccess = props => {
  const rootStore = useStores()
  return (
    <View style={styles.containerView}>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      <GaoDialog
        preset="score"
        textMoney={10}
        navigation={props.navigation}
        uriImage={require("assets/images/common/wink_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-35%", marginTop: "-15%"}}
      />
    </View>
  )
}

// Game Over
const IntruderGameOver = props => {
  const { t } = useTranslation()
  const { setState } = props
  const {metaGame} = useStores()
  const {navigation} = props

  useEffect(() => {
    metaGame.playGame("TheIntruder", navigation, t)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog1={t("common:gameover", "TU AS PERDU !")}
        textButton={t("common:retry", "Réessaye")}
        textStyle1={{ color: "#EB358A" }}
        uriImage={require("assets/images/common/sad_emoji.png")}
        buttonOnPress={() => setState({ dialog: "GameStart" })}
        dialogViewStyle={{marginHorizontal: "-35%"}}
      />
    </View>
  )
}

// 5. GAME OVER
const IntruderPermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-25%"}}
      />
    </View>
  )
}

/* *** */

type IntruderDialog =
  "GameIntro" |
  "GameStart" |
  "GameTimer" |
  "GameSuccess" |
  "GameOver" |
  "GamePermissionDenied"

type GameScreenState = {
  dialog: IntruderDialog
  seconds: number
  isAlienContactInPhone: boolean
  alienContactAdded: boolean
}

export interface GameScreenProps extends NavigationScreenProps<{}> {}

@withTranslation()
@connectRootStore
export class GameScreen extends React.Component<GameScreenProps, GameScreenState> {
  constructor(props) {
    super(props)
    this.state = {
      dialog: "GameStart",
      seconds: PLAYER_GIVEN_TIME,
      isAlienContactInPhone: true,
      alienContactAdded: false,
    }
    this.askForPermission = this.askForPermission.bind(this)
  }

  askForPermission() {
    const { t } = this.props
    // askPermission()
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: t("permissions:contacts.title", "Contacts"),
      message: t("permissions:contacts.message", "This app would like to view your contacts."),
      buttonNeutral: t("permissions:ask-me-later", "Ask Me Later"),
      buttonNegative: t("permissions:cancel", "Cancel"),
      buttonPositive: t("permissions:ok", "Ok"),
    }).then((response) => {
      if(response === "denied" || response === "never_ask_again") {
        this.setState({dialog: "GamePermissionDenied"})
      } else {
        this.props.rootStore.metaGame.addPermission("READ_CONTACTS")
        this.setState({ dialog: "GameIntro" })
      }
    })
  }

  render() {
    this.props.rootStore.setSong("gao_the_intruder.ogg", this.props.rootStore.gameMusic)
    const IntruderStateMachine = StateMachine({initialState: this.state})
    const IntruderFrame = GaoFrame({
      wallpaper: require("assets/images/wallpaper/gao_intruder.png"),
      coords: [130, 200, 650-130, 1120-200]
    })
    const IntruderScreen = IntruderFrame(props => (
      <IntruderStateMachine dialogs={{
        "GameIntro": IntruderGameIntro,
        "GameStart": IntruderGameStart,
        "GameTimer": IntruderGameTimer,
        "GameSuccess": IntruderGameSuccess,
        "GameOver": IntruderGameOver,
        "GamePermissionDenied": IntruderPermissionDenied
      }} {...props}
      askForPermission={this.askForPermission}
      checkAlienContactInPhone={this.checkAlienContactInPhone}
      rootStore={this.props.rootStore}
      />
    ))
    return (
      <GaoContainer {...this.props} character={require("assets/images/download_gifs/gao-Gao_the_intruder_intro.gif")} isCharacterCentered={false}>
        <IntruderScreen navigation={this.props.navigation} />
      </GaoContainer>
    )
  }
}
