import * as React from "react"
import {Alert, Text, View, Animated, Image} from "react-native"
import { NavigationScreenProps } from "react-navigation"
import { GaoDialog, GaoContainer, StateMachine, GaoFrame } from "components"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { useStores } from "models/root-store"
import {useEffect, useRef, useState} from "react"
import { TouchableOpacity } from "react-native"
import { PermissionsAndroid } from "react-native"
import Geolocation from "@react-native-community/geolocation"
import {color, typography} from "gao-theme"
import { PlayBack } from "components/sound/play-back"


/* *** Game design constants *** */

const CLOSE_DISTANCE = 2
const FAR_DISTANCE = 5
const MAX_FINDERS = 50
const INTERVALS = [5000, 4000, 3000, 2000, 1000]
//const INTERVALS = [1000, 1000, 1000, 1000, 1000]
const GRID_SIZE = {
  width: 5,
  height: 5
}

/* *** */

// Ask for permission
const VagabondGameIntro = props => {
  const { t } = useTranslation()
  const { setState } = props
  const {metaGame} = useStores()

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "vagabond:welcome",
          "Welcome! In order to play you need to share your LOCATION",
        )}
        textButton={t("common:accept", "Accepter")}
        buttonOnPress={askPermission}
      />
    </View>
  )
}

// Blaze
const VagabondBlazeMessage = props => {
  const { t } = useTranslation()
  const { setState } = props
  const { metaGame } = useStores()
  const { userName } = metaGame

  const askPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: t("permissions:location.title", "GPS"),
          message: t("permissions:location.message", "This app would like to access your location."),
          buttonNeutral: t("permissions:ask-me-later", "Ask Me Later"),
          buttonNegative: t("permissions:cancel", "Cancel"),
          buttonPositive: t("permissions:ok", "Ok"),
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        metaGame.addPermission("ACCESS_FINE_LOCATION")
        setState({dialog: "SpecialReward"})
      } else {
        setState({dialog: "GamePermissionDenied"})
      }
    } catch (err) {
      //console.warn(err)
    }
  }

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={userName + "! " + t(
          "vagabond:message",
          "Gao est allé dehors et n'est pas revenu.\nM'aiderais-tu à le retrouver?", { userName }
        )}
        textButton={t("common:go", "C’est parti!")}
        buttonOnPress={() => askPermission()}
      />
    </View>
  )
}

// Reward
const VagabondSpecialReward = props => {
  const { t } = useTranslation()
  const { setState } = props
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "vagabond:reward",
          "Fais partie des 50 premières personnes à retrouver Gao pour gagner une invitation spéciale à un live-stream avec le cerveau de Gao, Blaze !",
        )}
        textButton={t("vagabond:continue","Continue")}
        buttonOnPress={() => setState({dialog: "GameRules"})}
        dialogViewStyle={{marginHorizontal: "-15%",}}
      />
    </View>
  )
}

// Rules
const VagabondGameRules = props => {
  const { setState } = props
  const { t } = useTranslation()
  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t(
          "vagabond:rules",
          "Pour trouver Gao, tape n'importe où sur la carte pour voir s'il y est.\nSi tu es proche, le jeu te le fera savoir. Et garde un œil sur les infos de Blaze concernant la position de Gao, elles sont très utiles!",
        )}
        textButton={t("common:start", "Démarrer")}
        buttonOnPress={() => setState({dialog: "GameStart"})}
        dialogViewStyle={{marginHorizontal: "-18%", marginTop: "-30%"}}
      />
    </View>
  )
}

// Start the game
const VagabondGameStart = props => {

  const {t} = useTranslation()
  const {setState} = props
  const getRandomInt = max => Math.floor(Math.random() * 1000000) % max
  const [countTime, setCountTime] = useState({valueFinders: 0, valuePos: 0})
  const [advice, setAdvice] = useState("")
  const [finders, setFinders] = useState({ value: 0})
  const [pin, setPin] = useState({time: new Date(), x: -1, y: -1})
  const [pos, setPos] = useState({
    x: getRandomInt(GRID_SIZE.width),
    y: getRandomInt(GRID_SIZE.height)
  })
  const { metaGame } = useStores()

  const fadeAnim = useRef(new Animated.Value(100)).current

  const fadeOut = () => {
    // Will change fadeAnim value to 0 in 5 seconds
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 2000,
      useNativeDriver: true,
    }).start()
  }

  const changePositionGao = () => {
    const dx = getRandomInt(3) - 1
    let dy = 0
    if(dx == 0)
      dy = getRandomInt(3) - 1
    pos.x = Math.min(Math.max(pos.x + dx, 0), GRID_SIZE.width - 1)
    pos.y = Math.min(Math.max(pos.y + dy, 0), GRID_SIZE.height - 1)
    setState({pos: pos})
    //setPos(pos)
  }

  const addPin = (position) => {
    fadeAnim.setValue(100)
    pin.time = new Date()
    pin.x = position.x
    pin.y = position.y
    setState({pin: pin})
    fadeOut()
  }

  // Log all locations.
  useEffect(() => {
    const interval = setInterval(() => {
      const newTime = countTime.valueFinders + 1000
      const position = Math.min(Math.floor(finders.value / (MAX_FINDERS / INTERVALS.length)), INTERVALS.length - 1)
      if (newTime >= INTERVALS[position]) {
        countTime.valueFinders = 0
        finders.value = finders.value + 1
        setState({countTime: countTime})
        setState({finders: finders})
        //if(finders.value % 5 === 0) {
        //addPin()
        //}
      } else {
        countTime.valueFinders = newTime
        setState({countTime: countTime})
      }
      const newTimePos = countTime.valuePos + 1000
      if(newTimePos >= INTERVALS[INTERVALS.length - position - 1]) {
        countTime.valuePos = 0
        setState({countTime: countTime})
        changePositionGao()
      } else {
        countTime.valuePos = newTimePos
        setState({countTime: countTime})
      }
    }, 1000)
    const watch = Geolocation.watchPosition(
      info => {
        Geolocation.clearWatch(watch)
        metaGame.logLocation(info)
      },
      err => console.error("Vagabond geolocation error", err),
      { enableHighAccuracy: false, maximumAge: 0 }
    )

    return () => {
      clearInterval(interval)
    }
  }, [])

  const userTouchMap = (x, y) => {
    if (x == pos.x && y == pos.y) { // is Gao here?!
      props.setState({ dialog: "GameSuccess" }) // ---> WIN <---
    } else {
      addPin({x, y})
      const distance = Math.abs(pos.x - x) + Math.abs(pos.y - y)
      if (distance <= CLOSE_DISTANCE) {
        setAdvice(t("vagabond:advice-2", "Tu es vraiment proche. Il était là il y a une seconde"))
      } else if (distance >= FAR_DISTANCE) {
        setAdvice(t("vagabond:advice-3","Non, je n'ai pas vu de chat aujourd'hui"))
      } else {
        setAdvice(t("vagabond:advice-1","Oui, j'ai vu un chat. Mais il est probablement loin maintenant."))
      }
    }
  }

  return (
    <>
      <View style={{position: "absolute", top: -20, width: "100%", flexDirection: "row", justifyContent: "space-between"}}>
        <Text style={{fontSize: 20, fontFamily: typography.primary}}>Gao's Finders:</Text>
        <Text style={{fontSize: 20, fontFamily: typography.primary}}>{Math.min(finders.value, MAX_FINDERS - 1)}/{MAX_FINDERS}</Text>
      </View>
      { /*Se elimina por requerimiento*/ }
      {
        false && pin.x >= 0 && (
          <Animated.View
            style={{opacity: fadeAnim, position: "absolute", bottom: -80, right: 0, width: 200, height: 100, justifyContent: "center"}}>
            <Image source={require("assets/images/gao-the-vagabond/gao_the_message.png")}
              style={{width: "100%", resizeMode: "contain", position: "absolute", top: 0, left: 0}}/>
            <Text style={{width: "100%", textAlign: "center", alignSelf: "center", marginTop: 25, color: color.error, fontFamily: typography.primary, fontSize: 18}}>
              Seen at {pin.time.getHours()+":"+(pin.time.getMinutes()<10?"0":"")+pin.time.getMinutes()+":"+(pin.time.getSeconds()<10?"0":"")+pin.time.getSeconds()}
            </Text>
          </Animated.View>
        )
      }
      <View style={[props.frame, {left: 0, top: 7}]}>
        {[...Array(GRID_SIZE.height).keys()].map(y => (
          <View style={{flex: 1, flexDirection: "row"}}>
            {[...Array(GRID_SIZE.width).keys()].map(x => (
              <TouchableOpacity style={{flex: 1}} onPress={() => userTouchMap(x, y)}>
                {pin && pin.x === x && pin.y === y  &&
                (
                  <Animated.View style={{opacity: fadeAnim, width: "100%", height: "100%"}}>
                    <Image style={{width: "100%", height: "100%", resizeMode: "contain"}} source={require("assets/images/gao-the-vagabond/gao-the-cat.png")}></Image>
                  </Animated.View>
                )
                }
              </TouchableOpacity>
            ))}
          </View>
        ))}
      </View>
      <View style={styles.spacerView} />
      {advice ? <GaoDialog
        preset="advice"
        textDialog2={advice}
        textStyle2={styles.gaoAdvice}
      /> : <View />}
    </>
  )
}

// Success
const VagabondGameSuccess = props => {
  const { t } = useTranslation()
  const rootStore = useStores()
  const time = new Date()
  time.setMilliseconds(time.getMilliseconds() + 60*2*1000)

  return (
    <View style={styles.containerView}>
      { rootStore.soundMusic && <PlayBack song={"paws.ogg"} noRepeat={true} /> }
      {  !rootStore.isLiveStreamPlayed &&
        <GaoDialog
          preset="blaze-stream"
          textDialog1={t("vagabond:congrats", "Tu l'as trouvé et parmi les 50 premiers ! Tu as gagné une place dans notre événement spécial Live-stream aujourd'hui à 8:00 P:M!\n\nN’oublie pas de te connecter!").replace("8:00 P:M", time.getHours() + ":" + (time.getMinutes() < 10 ? "0" : "") + time.getMinutes())}
          uriImage={require("assets/images/common/wink_emoji.png")}
          navigation={props.navigation}
        />
      }
      { rootStore.isLiveStreamPlayed &&
        <GaoDialog
          preset="score"
          textMoney={20}
          navigation={props.navigation}
          uriImage={require("assets/images/common/wink_emoji.png")}
        // FIXME: change rewards for game design
          dialogViewStyle={{marginHorizontal: "-12%"}}
        />
      }
    </View>
  )
}

const VagabondPermissionDenied = props => {
  const {setState} = props
  const {t} = useTranslation()
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      props.navigation.navigate("preHome")
    }, 2000)
  })

  return (
    <View style={styles.containerView}>
      <GaoDialog
        textDialog2={t("common:permission-denied", "Why won't you share with me? Wink-wink")}
        textStyle2={{color: "#EB358A"}}
        uriImage={require("assets/images/common/sad_emoji.png")}
        dialogViewStyle={{marginHorizontal: "-10%"}}
      />
    </View>
  )
}

/*
 *** What the GAME do: ***
 - GameIntro: ask access to the player to GPS
 - BlazeMessage
 - SpecialReward: notice from the Publisher telling me of a special reward if I beat the game quickly.
 - GameRules
 - GameStart: This map is loaded from the game's files and is the same for every player with :
    - a counter that "keeps track" of the number of Gao members who find Gao. This counter starts at 00/50 and can only go to 49/50.
    - this counter starts going up at pre-determined incremental speeds (from 0/50 to 10/50 it goes very slow, from 11/50 to 20/50 it goes slow and so on).
    - The more Gao members find Gao, the slower the invisible Gao goes.
    - As a Player, I want to see Gao Pins that appear on the map every now and then. A notification appears alongside these pins displaying at what time
    Gao passed through the indicated spot. These notifications become more frequent as more Gao members find the cat.
 - GameCongrats: congratulatory message appear on the screen when my tap catches the invisible Gao. This message includes an invitation to a live-stream event.

 NB: As a Designer, I want to be able to modify the number of units within the invisible grid that a tap identifies.
 */

type VagabondDialog =
  "GameIntro" |
  "BlazeMessage" |
  "SpecialReward" |
  "GameRules" |
  "GameStart" |
  "GameSuccess" |
  "GamePermissionDenied"

type GameScreenState = {
  dialog: VagabondDialog
}

export interface GameScreenProps extends NavigationScreenProps<{}> {}

export const GameScreen: React.FunctionComponent<GameScreenProps> = props => {
  const rootStore = useStores()
  const VagabondStateMachine = StateMachine({initialState: {dialog: "BlazeMessage"} as GameScreenState})
  const VagabondFrame = GaoFrame({
    wallpaper: require("assets/images/gao-the-vagabond/Gao-the-Vagabond-map.png"),
    coords: [52, 258, 642, 863],
  })
  const VagabondScreen = VagabondFrame(props =>
    <VagabondStateMachine dialogs={{
      "GameIntro": VagabondGameIntro,
      "BlazeMessage": VagabondBlazeMessage,
      "SpecialReward": VagabondSpecialReward,
      "GameRules": VagabondGameRules,
      "GameStart": VagabondGameStart,
      "GameSuccess": VagabondGameSuccess,
      "GamePermissionDenied": VagabondPermissionDenied
    }} {...props}
    />
  )
  rootStore.setSong("vagabond.ogg", rootStore.gameMusic)

  return (
    <>
      <GaoContainer {...props}>
        <VagabondScreen navigation={props.navigation} />
      </GaoContainer>
    </>
  )
}
