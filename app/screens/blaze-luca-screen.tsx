import * as React from "react"
import {ImageButton, Wallpaper} from "../components"
import {
  Animated,
  Image,
  Text,
  TextInput,
  View
} from "react-native"
import {useEffect, useRef} from "react"
import {useStores} from "models/root-store"
import {useTranslation} from "react-i18next";

const NUMBERS = [
  {
    key: "5.1",
    text: "5",
    blueImage: require("assets/images/blaze-character-scenes/luca/5_blue.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/5_red.png"),
  },
  {
    key: "5.2",
    text: "5",
    blueImage: require("assets/images/blaze-character-scenes/luca/5_blue.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/5_red.png"),
  },
  {
    key: "5.3",
    text: "5",
    blueImage: require("assets/images/blaze-character-scenes/luca/5_blue.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/5_red.png"),
  },
  {
    key: "line",
    redImage: require("assets/images/blaze-character-scenes/luca/line.png"),
    blueImage: require("assets/images/blaze-character-scenes/luca/line.png"),
    omitText: true
  },
  {
    key: "3",
    text: "3",
    blueImage: require("assets/images/blaze-character-scenes/luca/3_blue.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/3_red.png"),
  },
  {
    key: "7",
    text: "7",
    blueImage: require("assets/images/blaze-character-scenes/luca/7_blue.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/7_red.png"),
  },
  {
    key: "8",
    text: "8",
    blueImage: require("assets/images/blaze-character-scenes/luca/8_blue.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/8_red.png"),
  },
  {
    key: "9",
    text: "9",
    blueImage: require("assets/images/blaze-character-scenes/luca/blue_9.png"),
    redImage: require("assets/images/blaze-character-scenes/luca/9_red.png"),
  },
]

const IMAGES = [
  {
    image: require("assets/images/blaze-character-scenes/luca/01.png"),
    glowImage: require("assets/images/blaze-character-scenes/luca/01_b.png"),
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/02.png"),
    type: "text",
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/03_b.png"),
    glowImage: require("assets/images/blaze-character-scenes/luca/03_b1.png"),
  },
  {
    image: require("assets/images/download_gifs/Anim1_12F.gif"),
    waitTime: 7500,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/09.png"),
    waitTime: 3000,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/10.png"),
    waitTime: 3000,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/11.png"),
    glowImage: require("assets/images/blaze-character-scenes/luca/10.png"),
  },
  {
    image: require("assets/images/download_gifs/Anim2_12F.gif"),
    waitTime: 7500,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/15.png"),
    waitTime: 3000,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/16.png"),
    glowImage: require("assets/images/blaze-character-scenes/luca/16_b.png"),
  },
  {
    image: require("assets/images/download_gifs/Anim3_12F.gif"),
    waitTime: 7500,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/17.png"),
    waitTime: 3000,
  },
  {
    image: require("assets/images/blaze-character-scenes/luca/17.png"),
    glowImage: require("assets/images/blaze-character-scenes/luca/18_b.png"),
  },
  {
    image: require("assets/images/download_gifs/Anim4_12F_FINAL.gif"),
    waitTime: 6500,
  },
]

export const BlazeLucaScreen = props => {
  const rootStore = useStores()
  const {navigation} = props
  const [count, setCount] = React.useState(0)
  const { metaGame } = useStores()

  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(0.0))
  const [fadeAnimAll, setFadeAnimAll] = React.useState(new Animated.Value(0.0))
  const [indexText, setIndexText] = React.useState(0)
  const [lastLength, setLastLength] = React.useState(0)
  const [loadImages, setLoadImages] = React.useState(IMAGES.filter(item => item.image).length - IMAGES.filter(item => item.image.uri).length)
  const {t} = useTranslation()
  const [heightLayout, setHeightLayout] = React.useState(0)
  const refInput = useRef()

  const characterInfo = navigation.getParam("characterInfo")

  useEffect(() => {
    rootStore.setSong("blaze_minigames.ogg", rootStore.gameMusic)
    metaGame.playGame("luca", null, t)
    fadeIn.start()
    return () => rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
  }, [])

  const fadeIn = Animated.timing(fadeAnimAll, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const fadeOut = Animated.timing(fadeAnimAll, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const asc = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 1000,
    useNativeDriver: true,
  })

  const desc = Animated.timing(fadeAnim, {
    toValue: 0,
    duration: 1000,
    useNativeDriver: true,
  })

  const finish = () => {
    fadeOut.start(() => {
      rootStore.setSong("blaze_background.ogg", rootStore.homeMusic)
      metaGame.performOnlinePlayerAssessment(navigation, characterInfo, t)
    })
  }

  const startAnimation = () => {
    if(IMAGES[count].waitTime) {
      setTimeout(() => {
        if(count === IMAGES.length - 1) {
          finish()
        } else {
          setCount(prevCount => prevCount + 1)
        }
      }, IMAGES[count].waitTime)
    }
  }

  const startAnimationGlow = (anim, otherAnim) => {
    anim.start(({finished}) => {
      if (finished) {
        startAnimationGlow(otherAnim, anim)
      }
    })
    return true
  }

  const clickText = () => {
    if(refInput.current.isFocused())
      refInput.current.blur()
    refInput.current.focus()
  }

  return (
    <>
      {
        /*loadImages > 0 &&
        <View style={{flex: 1, justifyContent: "center"}}>
          {IMAGES.filter(item => item.image).map((item, index) => <Image key={"load-image-"+index} style={{opacity: 0, position: "absolute"}} source={item.image} onLoadEnd={() => setLoadImages(loadImages - 1)} />)}
          <Text style={{alignSelf: "center", zIndex: 10}}>{t("common:loading", "Loading...")}</Text>
        </View>*/
      }
      {
        /*loadImages <= 0 &&*/
    <Animated.View style={{flex: 1, opacity: fadeAnimAll}}>
      <View onTouchEndCapture={() => {
        if (IMAGES[count].glowImage) {
          asc.stop()
          desc.stop()
          if(count === IMAGES.length - 1) {
            finish()
          } else {
            setCount(prevCount => prevCount + 1)
          }
        }
      }} style={{flex: 1, backgroundColor: "white"}} onLayout={(event) => {
        if(heightLayout === 0)
          setHeightLayout(event.nativeEvent.layout.height)
      }}>
        {
          IMAGES[count].glowImage && startAnimationGlow(asc, desc) &&
        (
          <Animated.View style={{zIndex: 1, opacity: fadeAnim, width: "100%", height: "100%"}}>
            <Wallpaper
              backgroundImage={IMAGES[count].glowImage}/>
          </Animated.View>
        )
        }
        { IMAGES[count].waitTime && startAnimation() }
        {IMAGES[count].type === "text" && (
          <Wallpaper backgroundImage={IMAGES[count].image} style={{flex: 1, height: heightLayout}}/>
        )}
        {IMAGES[count].type !== "text" && (
          <Wallpaper backgroundImage={IMAGES[count].image}/>
        )}
        {IMAGES[count].type === "text" && (
          <View style={{flexDirection: "row", width: "70%", alignSelf: "center", height: 100, position: "absolute"}} onTouchEndCapture={() => clickText()}>
            <TextInput ref={refInput} autoFocus={true} style={{position: "absolute", top: -500}} keyboardType={"numeric"}
              blurOnSubmit={true} onChangeText={(event) => {
                {
                  if (event.length > 0 && lastLength < event.length) {
                    let newIndex = indexText
                    if (NUMBERS[indexText].text === event.substr(event.length - 1, 1)) {
                      newIndex++
                    }
                    if (newIndex < NUMBERS.length && NUMBERS[newIndex].omitText) {
                      newIndex++
                    }
                    setIndexText(newIndex)
                    if (newIndex >= NUMBERS.length) {
                      setCount(prevCount => prevCount + 1)
                    }
                  }
                  setLastLength(event.length)
                }
              }}/>
            {
              NUMBERS.map((item, index) => (
                <View key={item.key} style={{flex: 1, paddingTop: 30}} onTouchEndCapture={() => clickText()}>
                  <Image source={index<indexText?item.blueImage:item.redImage} style={{resizeMode: "contain", width: "60%", height: 50} }></Image>
                  {
                    !item.omitText &&
                    (<Image style={{resizeMode: "contain", width: "60%",}} source={require("assets/images/blaze-character-scenes/luca/line.png")}></Image>)
                  }
                </View>
              )
              )
            }
          </View>)
        }
      </View>
    </Animated.View>
      }
    </>
  )
}
