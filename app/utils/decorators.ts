// !! CRITICAL FIXME !! move it to some utils/ folder.
import { RootStoreContext } from "models/root-store/root-store-context" // Ryan: ABSOLUTELY NECESSARY FOR CIRCULAR REFERENCES REASONS.
import React from "react"

export function consumer(ctxs) {
  // Some docs about this.
  // For class-components, it's difficult to inject the required context properly.
  // For performance reason, React.js requires one context type per component.
  // So you need to reduce and create a tower of consumer in order to listen to all desired contexts.
  // Thanksfully, we only use one context in general: RootStoreContext.
  // So we should only see one wrapper.
  // Still, interesting problem.
  return component => {
    const keys = Object.keys(ctxs)
    const consume = keys.reduceRight(
      (inner, key) => {
        return props =>
          React.createElement(ctxs[key].Consumer, null, value =>
            inner({
              ...props,
              [key]: value,
            }),
          )
      },
      props => React.createElement(component, props),
    )

    function Consumer(props) {
      return consume(props)
    }

    Consumer.displayName = `Consumer(${component.displayName || component.name || "Component"})`

    return Consumer
  }
}

export function connectRootStore(component) {
  return consumer({
    rootStore: RootStoreContext,
  })(component)
}
