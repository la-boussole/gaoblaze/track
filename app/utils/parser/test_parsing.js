const parser = require("./twine.js")
const someScenarios = [
  require("../../assets/scenarios/Ally"),
  require("../../assets/scenarios/Amin"),
  require("../../assets/scenarios/Lucas"),
  require("../../assets/scenarios/Masako"),
  require("../../assets/scenarios/Nikki"),
  require("../../assets/scenarios/Rokaya"),
  require("../../assets/scenarios/Sol"),
  require("../../assets/scenarios/Alex")
]
//const util = require("util")


let totalErrors = 0
const globalPassagesMapping = {}
function getChoices(parsedNode) {
  const choiceNode = parsedNode.find(elt => !!elt.choices)
  if (choiceNode) {
    return choiceNode.choices
  } else {
    return null
  }
}
function max(a, b) { return a <= b ? b : a }
function min(a, b) { return a <= b ? a : b }
function parse(node) {
  try {
    const contentsAST = parser.parse(node.text)
    globalPassagesMapping[node.name] = contentsAST
    return contentsAST
  } catch (error) {
    //const advancedCtx = error.location ? node.text.slice(max(0, error.location.start.offset - 25), min(node.text.length - 1, error.location.end.offset + 25)) : "unknown"
    console.error("Failed to parse a node", error)
    totalErrors += 1
    return null
  }
}

const ensureChoicesExists = mapping => node => {
  const choices = getChoices(node)
  if (!choices) return true

  if (!choices.every(choice => choice.passageTitle ? mapping.hasOwnProperty(choice.passageTitle) : mapping.hasOwnProperty(choice))) {
    return false
  }

  return true
}

function ensureConsistency(passageMapping) {
  return Object.values(passageMapping).every(ensureChoicesExists(passageMapping))
}

function loadScenario(scenario) {
  scenario["passages"].map(parse)
}

someScenarios.map(loadScenario)
if (!ensureConsistency(globalPassagesMapping)) {
  console.error("Consistency is not assured.")
}
console.error("Total errors", totalErrors)
