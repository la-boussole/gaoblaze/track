TwineCode = model:TwinePrimitive* {
	return model.filter(x => !!x);
}
TwinePrimitive = DelayedTriggerEventStatement / TriggerEventStatement / WrappedShowStatement / WaitStatement / ShowStatement / BranchStatement / ChoiceStatement / t:Text { return { "type": "text", text: t } }

DelayedTriggerEventStatement = w:WaitStatement _ t:TriggerEventStatement {
       return { "type": "trigger_notification", "delay": w.delay, "unit": w.unit }
}

TriggerEventStatement = "@init" {
       return { "type": "trigger_notification" }
}

WaitStatement = "@wait " delay:Integer unit:UnitIdentifier? {
	return {
            "type": "wait",
    	"delay": delay,
        "unit": (unit || "s")
    }
} / "@wait<" delay:Integer unit:UnitIdentifier? ">" {
	return {
            "type": "wait",
    	"delay": delay,
        "unit": (unit || "s")
    }
}
UnitIdentifier = "mn" / "s" / "ms"

WrappedShowStatement = "{{" code:TwineCode "}}" {
	return {
	    "type": "wrapper",
    	"wrapped": code
    }
}

ShowStatement = "@showIfGameHasBeenPlayed " params:GameIdentifiers _ t:TwineCode
	{ return { "type": "game", "games": params, contents: t } }
	/ "@showOnlyIfChoiceMade " params:Parameters _ t: TwineCode
    { return { "type": "choice", "choices": params, contents: t } }
    / "@showOnlyIfProfileDiscovered " params:Parameters _ t: TwineCode
    { return { "type": "profile", "characters": params, contents: t }}
    / "@insertMedia:" param:MediaPath
    { return { "type": "media", "path": param } }
    / "@pawncount " param:Integer _ t:TwineCode
    { return { "type": "pawnCount", "count": param, contents: t } }
    / "@showIfGameCount " param:Integer _ t:TwineCode
    { return { "type": "gameCount", "count": param, contents: t } }
    / "@passive" _ t:TwineCode
    { return { "type": "passive", contents: t } }

BranchStatement = "@if(" cond:ConditionStatement ")" _ then:TwineCode _ "@else" _ other:TwineCode _ "@endif" _
        { return { "type": "branch",  "condition": cond, "then": then, "else": other } }
        / "@if(" cond:ConditionStatement ")" _ then:TwineCode _ "@endif" _
        { return { "type": "branch", "condition": cond, "then": then, "else": null } }

ChoicePrimitive = "[[" left:Text "|" right:Text "]]"
	{
    	return { displayed: left, passageTitle: right }
    }
	/ "[[" head: Text "]]" {
    	return head
    }

ChoiceStatement = head: ChoicePrimitive _ tail:ChoiceStatement* {
	return {
	    "type": "choices",
    	"choices": [head, ...(tail[0] ? tail[0].choices : [])]
    }
}


Text = _ chars:[0-9A-Za-zÀ-ÖØ-öø-ÿ.*:,’‘"#«»”'œ; \t\xa0\n\r…?!()&¿¡×=/<>–—^_\\-]+ _ {
	return chars.join("").trim();
}
ConditionStatement = ChoiceCondition / GameCondition / ProfileDiscoveryCondition
ChoiceCondition = "choiceHasBeenMade" _ params:Parameters {
	return {
    	"type": "choice",
        "params": params
    }
}
GameCondition = "gameHasBeenPlayed" _ params:GameIdentifiers _ {
	return {
    	"type": "game",
        "params": params
    }
}
ProfileDiscoveryCondition = "profileDiscovered" _ params:Parameters _ {
	return {
    	"type": "profile",
        "params": params
    }
}
GameIdentifiers = _ head:GameIdentifier "," tail:GameIdentifier {
	return [head, ...tail];
   } / _ identifier:GameIdentifier {
   	return [identifier];
   }
Parameters = _ head:StringOrInteger "," tail:Parameters {
	return [head, ...tail];
} / _ param:StringOrInteger {
	return [param];
}

MediaPath = chars:[a-zA-Z_.]* {
	return chars.join("");
}
GameIdentifier = chars:[a-zA-Z]* {
	return chars.join("").toLowerCase();
}
StringOrInteger = Integer / String
Integer "integer"
  = _ [0-9]+ { return parseInt(text(), 10); }

String "string"
  = Quotation_Mark chars:Char* Quotation_Mark { return chars.join(""); }

Char
  = Unescaped
  / Escape
    sequence:(
        '"'
      / "\\"
      / "/"
      / "b" { return "\b"; }
      / "f" { return "\f"; }
      / "n" { return "\n"; }
      / "r" { return "\r"; }
      / "t" { return "\t"; }
      / "u" digits:$(HEXDIG HEXDIG HEXDIG HEXDIG) {
          return String.fromCharCode(parseInt(digits, 16));
        }
    )
    { return sequence; }

Escape
  = "\\"

Quotation_Mark
  = '"'

Unescaped
  = [^\0-\x1F\x22\x5C]

DIGIT  = [0-9]
HEXDIG = [0-9a-f]i

_ "whitespace"
  = [ \t\n\r•]*

