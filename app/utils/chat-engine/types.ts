import { CharacterProfile } from "models/character-profile"
import { GamePlay } from "models/meta-game"

export interface NestedArray<T> extends Array<T | NestedArray<T>> {
}

export interface IGameState {
  pawCount: number;
  profilesDiscovered: Map<string, CharacterProfile>;
  gamesPlayed: Map<string, GamePlay>;
  choicesMade: Map<string, string>;
  databaseVisited: number;
  canDebugMode: boolean;
  showChat: boolean;
  onlinePlayerAssessments: number;
}

export interface IChoice {
  displayed: string;
  passageTitle: string;
}
export type ChoiceType = string | IChoice;
export interface IChoiceNode {
  type: "choices";
  choices: ChoiceType[];
}
export interface IWaitNode {
  type: "wait";
  delay: number;
  unit: "mn" | "seconds";
}

export type ShowType = "game" | "choice" | "profile" | "pawCount" | "gameCount";
export interface IShowNode {
  type: ShowType;
  games?: string[];
  choices?: string[];
  characters?: string[];
  count?: number;
  contents: ISubNode[];
}

export interface IPassiveMarkerNode {
  type: "passive";
  contents: ISubNode[];
}

export type ConditionType = "choiceHasBeenMade" | "gameHasBeenPlayed" | "profileDiscovered";
export interface IConditionNode {
  type: ConditionType;
  params: string[];
}

export interface IBranchNode {
  type: "branch";
  then: ISubNode;
  else: ISubNode;
  condition: IConditionNode;
}

export interface IWrappedNode {
  type: "wrapper";
  wrapped: ISubNode;
}

export interface IRuntimeWrappedNode {
  type: "wrapper";
  wrapped: NestedArray<IRuntimeNode>;
  requiresUserInteraction: boolean;
}

export interface ITextNode {
  type: "text";
  text: string;
  isMe?: boolean;
}

export interface ITriggerNode {
  type: "trigger_notification";
  delay?: number;
  unit?: string;
}

export interface IImageNode {
  type: "media";
  path: string;
  isMe?: boolean;
}

// Parsed IRawNode.text = ISubNode[]
export type ISubNode = ITriggerNode | ITextNode | IChoiceNode | IWaitNode | IShowNode | IBranchNode | IWrappedNode | IPassiveMarkerNode;
// Once evaluation is done, no show node / branch nodes are left, we know exactly what to show.
export type IRuntimeNode = ITextNode | IImageNode | IChoiceNode | IWaitNode | IRuntimeWrappedNode | null; // null is for empty nodes.
// Once it has been shown, it becomes a static node.
export type IStaticNode = ITextNode | IImageNode | IChoiceNode;
