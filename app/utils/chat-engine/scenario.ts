import parser from "utils/parser/twine"

interface IScenario {
  passages: IRawNode[];
}
function collectAllChoices(toplevelNodes: ISubNode[]): IChoiceNode[] {
  let allChoices: IChoiceNode[] = []

  for (const node of toplevelNodes) {
    if (node.type === "choices") {
      allChoices.push(node)
    }

    if (node.type == "wrapper") {
      allChoices = allChoices.concat(...collectAllChoices([node.wrapped]))
    }

    if ("contents" in node) {
      allChoices = allChoices.concat(...collectAllChoices([node.contents]))
    }
  }

  return allChoices
}

function mergeChoiceNodes (choiceNodes: IChoiceNode[]): IChoiceNode {
  return {
    type: "choices",
    choices: ([] as ChoiceType[]).concat(...(choiceNodes.map(c => c.choices)))
  }
}

class Node {
  parsedData: ISubNode[];
  name: string;

  staticChoices: ChoiceType[];
  allChoices: ChoiceType[];

  constructor(
    graph: Graph,
    rawNode: IRawNode
  ) {
    this.parsedData = parser.parse(rawNode.text)

    // 1. Static choices
    const staticChoices = this.parsedData.find(t => t.type == "choices")
    if (staticChoices) {
      this.staticChoices = (staticChoices as IChoiceNode).choices // Take the contents.
    }

    // 2. Perform static evaluation of all possible choices.
    const allChoices = collectAllChoices(this.parsedData)
    if (allChoices) {
      this.allChoices = ([] as ChoiceType[]).concat(...(allChoices as IChoiceNode[]).map(c => c.choices))
    }

    // 2. Metadata

    // 3. Perform static registration.
    graph.registerNode(rawNode.name, this)
  }

  availableChoices (gameState: IGameState): Set<ChoiceType> {

  }

  hasChoice (gameState: IGameState, choice: string): boolean {

  }

  eval (gameState: IGameState): IRuntimeNode[] {
    const nodeVisitor = new NodeVisitor(gameState)
    // Phase 1: do evaluation.
    let runtimeNodes: IRuntimeNode[] = this.parsedData.map(nodeVisitor.visitNode)
    // Phase 2: do filtering.
    runtimeNodes = runtimeNodes.filter(n => !!n) // Filter out null nodes.
    const choiceNodes: IChoiceNode[] = runtimeNodes.filter((n): n is IChoiceNode => n.type === "choices")
    runtimeNodes = runtimeNodes.filter(n => n.type !== "choices") // Filter out choices nodes.
    // Phase 3: merge nodes when they can be merged.
    runtimeNodes.push(mergeChoiceNodes(choiceNodes))

    return runtimeNodes
  }

  show (gameState: IGameState, runtimeNodes: IRuntimeNode[]): IScenarioMessage {

  }

  // Remove waits & wrapping.
  freeze (n: IRuntimeNode): IStaticNode | null {
    if (n.type === "wait") return null
    if (n.type === "wrapper") return this.freeze(n.wrapped) // must converge.

    return (n as IStaticNode)
  }

  multiFreeze (runtimeNodes: IRuntimeNode[]): IStaticNode[] {
    return runtimeNodes.map(this.freeze).filter(n => !!n)
  }
}

class Graph {
  passages: Object
  currentNode: Node
  onScreenNodes: IScenarioMessage[]
  debug: boolean
  constructor(twineScenario, debug = false) {
    this.debug = debug
    this.onScreenNodes = []
    this.parseTwine(twineScenario)
  }

  ensureGraphConsistency (): boolean {
    return true
  }

  // This will load a JSON enriched Twine file and produce the playable graph of chat.
  parseTwine (scenario: IScenario): void {
    this.passages = {}
    const nodes = scenario.passages.map(rawNode => new Node(this, rawNode))
  }

  // Register the node in the adjacency list.
  registerNode (name: string, node: Node): void {
    this.passages[name] = node
  }

  // 1. Evaluate the current node with gameState.
  // 2. This will create extra subnodes depending on the state.
  // 3. Returns the consolidated node, which can be used to compute wait time, etc.
  currentElement (gameState: IGameState): IScenarioMessage {
    const runtimeNodes = this.currentNode.eval(gameState)
    return this.currentNode.show(gameState, runtimeNodes)
  }

  // Move pointer according to the choice
  // If it fails, it returns false.
  movePointer (choice: string, gameState: IGameState): boolean {
    if (!this.passages.hasOwnProperty(choice)) {
      if (this.debug) console.error(`This graph has no out edge towards ${choice} !`)
      return false
    }

    if (!this.currentNode.hasChoice(gameState, choice)) {
      if (this.debug) console.error(`Current node cannot go to ${choice}, according to current game state.`,
        this.currentNode.availableChoices(gameState))
      return false
    }

    this.currentNode = this.passages[choice]
    return true
  }

  // For hydration purpose, like, you've already talked with a character.
  // You want to restore the discussion, so you restore the choices and replay the graph.
  // Move forward the graph to the sub-graph induced by choices.
  // If it fails, it returns false.
  restoreChoices (choices: Set<string>, gameState: IGameState): boolean {
    for (const choice of choices) {
      if (!this.movePointer(choice, gameState)) return false
    }
    return true
  }
}