import { IConditionNode, IGameState, IRuntimeNode, IShowNode, ISubNode, NestedArray } from "utils/chat-engine/types"

const typeMapping = {
  "choice": "choicesMade",
  "gameHasBeenPlayed": "gamesPlayed",
  "profile": "profilesDiscovered"
}

const showDispatchers = {
  "choice": (n, state) => {
    let choicesMades = state.choicesMade
    if(!choicesMades)
      choicesMades = new Map()
    return n.choices.every(value => choicesMades.has(value))
  },
  "game": (n, state) => {
    const names = []
    if(state.gamesPlayed) {
      state.gamesPlayed.forEach(item => names.push(item.name.toLocaleLowerCase()))
      return n.games.every(value => names.includes(value.toLocaleLowerCase()))
    }
    return false
  },
  "gameCount": (n, state) => {
    return state.gamesPlayed.size >= n.count
  },
  "pawCount": (n, state) => n.count >= state.pawCount,
  "profile": (n, state) => n.characters.every(value => {
    let profiles = state.profilesDiscovered
    if(!profiles) profiles = new Map()
    return profiles.has(value)
  }),
  "passive": (n, state) => true
}

export function evaluateCondition (c: IConditionNode): ((state: IGameState) => boolean) {
  return (state => c.params.every(value => {
    return state[typeMapping[c.type]].has(value)
  }))
}

export function evaluateShowCondition (c: IShowNode): ((state: IGameState) => boolean) {
  return (state => showDispatchers[c.type](c, state))
}

export function evaluatePseudoCondition (c: IConditionNode | IShowNode | undefined): ((state: IGameState) => boolean) {
  if ("condition" in c) return evaluateCondition(c)
  else if ("contents" in c) return evaluateShowCondition(c)
  else return () => true
}

export default class NodeVisitor {
  state: IGameState
  constructor(gameState: IGameState) {
    this.state = gameState
  }

  visitCondition = (c: IConditionNode): boolean => {
    return evaluateCondition(c)(this.state)
  }

  visitShow = (n: IShowNode): boolean => {
    return showDispatchers[n.type](n, this.state)
  }

  visitNode = (n: ISubNode): IRuntimeNode | NestedArray<IRuntimeNode> => {
    // Show node.
    if ("contents" in n && n.type !== "passive") {
      if (this.visitShow(n)) {
        return n.contents.map(this.visitNode)
      }
    }

    // Branch node.
    if ("condition" in n) {
      if (this.visitCondition(n.condition)) {
        return this.visitNode(n.then)
      } else {
        if(n.else) {
          return this.visitNode(n.else)
        } else {
          return []
        }
      }
    }

    // Wrapped node.
    if ("wrapped" in n) {
      return {
        type: "wrapper",
        wrapped: [this.visitNode(n.wrapped)],
        requiresUserInteraction: false
      }
    }

    // Passive node.
    if (n.type === "passive") {
      return {
        type: "wrapper",
        wrapped: n.contents.map(this.visitNode),
        requiresUserInteraction: true
      }
    }

    // Trigger notification are ignored, during evaluation, as those are used by trigger system.
    if (n.type === "trigger_notification") {
      return []
    }

    return (n as IRuntimeNode)
  }
}
