import * as React from "react"
import {View} from "react-native"
import {Checkbox} from "../"
import {checkboxListStyles as styles} from "./checkbox-list.styles"

export interface CheckboxListProps {
  data: {key: string, checked?: boolean}[];
  theme: "gao" | "blaze"
}

export const CheckboxList: React.FunctionComponent<CheckboxListProps> = props => {
  return (
    <View style={styles.WRAPPER}>
      {props.data.map(item => (<Checkbox theme={props.theme} text={item.key} value={item.checked} />))}
    </View>
  )
}
