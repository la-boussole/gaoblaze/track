import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { CheckboxList } from "./checkbox-list"

declare let module

storiesOf("CheckboxList", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        {/*<CheckboxList theme="gao" text="CheckboxList" />*/}
      </UseCase>
    </Story>
  ))
