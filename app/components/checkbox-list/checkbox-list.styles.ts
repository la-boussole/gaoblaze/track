import { ViewStyle } from "react-native"
import {spacing} from "blaze-theme"

export const checkboxListStyles = {
  WRAPPER: {
    justifyContent: "center",
    marginBottom: spacing.huge
  } as ViewStyle
}
