import * as React from "react"
import {TextStyle, View, ViewStyle} from "react-native"
import { Text, StoryChoice } from "../"
import { storyChoicesStyles as styles } from "./story-choices.styles"
import { ChoiceType } from "utils/chat-engine/types"
import {useEffect} from "react"

export interface StoryChoicesProps {
  /**
   * Text which is looked up via i18n.
   */
  tx?: string

  /**
   * The text to display if not using `tx` or nested components.
   */
  text?: string

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle,

  textStyle?: TextStyle,

  choices: ChoiceType[]

  canChoose: boolean

  timerChoose?: boolean
  selectedStyle?: ViewStyle
  textStyleSelected?: TextStyle
  icon?: any
  onResize?: () => void
}

/**
 * Stateless functional component for your needs
 *
 * Component description here for TypeScript tips.
 */
export function StoryChoices(props: StoryChoicesProps) {
  // grab the props
  const { choices, onChoose, selectedStyle, textStyleSelected, canChoose, style, icon, onResize, ...rest } = props
  const [selected, setSelected] = React.useState(null)

  if (!canChoose) return null
  return (
    <View style={props.style} onLayout={() => onResize()}>
      {choices.map(choice => (
        <StoryChoice
          onPreChoose={() => {
            setSelected(choice)
          }}
          selected={choice === selected}
          onChoose={() => {
            onChoose(choice)
          }}
          textStyle={props.textStyle}
          key={choice.displayed || choice}
          choice={choice}
          selectedStyle={selectedStyle}
          textStyleSelected={textStyleSelected}
          icon={icon}
        />))}
    </View>
  )
}
