import * as React from "react"
import BackgroundTimer from "react-native-background-timer"
import {TextStyle} from "react-native"
import {Text} from "../"
import {BackHandler} from "react-native"
import { PlayBack } from "components/sound/play-back"
import { connectRootStore } from "utils/decorators"

export const secondsToPrettyTimeString = time => {
  // From https://stackoverflow.com/questions/3733227/javascript-seconds-to-minutes-and-seconds
  const minutes = Math.floor(time / 60)
  const seconds = time - minutes * 60
  const str_pad_left = (string, pad, length) => {
    return (new Array(length + 1).join(pad) + string).slice(-length)
  }
  return str_pad_left(minutes, "0", 2) + ":" + str_pad_left(seconds, "0", 2)
}

// FIXME: Duplicated code ...
type TimerState = {
  timerID: number | null
  clock: number
  seconds: number
  time: number
}

type TimerProps = {
  playerGivenTime: number
  textStyle: TextStyle
  finishTimer: () => void
  change?: (any, timer) => void
  navigation?: any,
  textType?: "normal" | "clock"
  showGo?: boolean
  removeSound?: boolean
}

@connectRootStore
export class Timer extends React.Component<TimerProps, TimerState> {
  timer = null
  soundRef = null

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    this.state = {
      timerID: null,
      seconds: props.playerGivenTime,
      time: new Date().getTime(),
      clock: props.playerGivenTime
    }
    this.startTimer()
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButtonClick)
  }

  componentWillUnmount() {
    this.stop()
    if(this.soundRef) {
      console.log("Enter in this", this.soundRef)
      this.soundRef.stop()
    }
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButtonClick)
  }

  handleBackButtonClick() {
    //if (this.props.navigation)
    //this.props.navigation.goBack(null)
    BackgroundTimer.clearInterval(this.timer)
    return false
  }

  startTimer() {
    this.setState({time: new Date().getTime()})
    this.timer = BackgroundTimer.setInterval(() => {
      if (new Date().getTime() - this.state.time <= this.state.seconds * 1000) {
        this.setState({clock: this.state.clock - 1})
        if (this.props.change) {
          this.props.change(this.timer, this)
        }
      } else {
        if(this.props.showGo) {
          this.setState({clock: this.state.clock - 1})
          BackgroundTimer.clearInterval(this.timer)
          setTimeout(() => {
            this.props.finishTimer()
          }, 1000)
        } else {
          this.props.finishTimer()
          BackgroundTimer.clearInterval(this.timer)
        }
      }
    }, 1000)
  }

  stop() {
    if(this.timer)
      BackgroundTimer.clearInterval(this.timer)
  }

  addTime(time) {
    this.setState({clock: this.state.clock + time})
    this.setState({seconds: this.state.seconds + time})
  }

  render() {
    return (
      <>
        { this.props.rootStore.soundMusic && !this.props.removeSound && <PlayBack song={"timer.ogg"} ref={(ref) => {
          if(this.soundRef === null)
            this.soundRef = ref
        }} /> }
        <Text style={this.props.textStyle}>
          { this.props.showGo && this.state.clock <= 0? "Go!" :
            !this.props.textType || this.props.textType == "clock" ? secondsToPrettyTimeString(Math.max(this.state.clock, 0)) : Math.max(this.state.clock, 0)}
        </Text>
      </>
    )
  }
}
