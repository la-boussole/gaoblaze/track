import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { GaoCommunity } from "./gao-community"

declare let module

storiesOf("GaoCommunity", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        {/*<GaoCommunity />*/}
      </UseCase>
    </Story>
  ))
