import * as React from "react"
import {View} from "react-native"
import {useTranslation} from "react-i18next"
import {ImageButton, TabView} from "components"
import * as styles from "gao-theme/styles"
import { useStores } from "models/root-store"

export interface GaoCommunityProps {
  currentTab: string;
  dismiss: () => void;
  navigate: () => void;
  children: React.ReactElement
}

const TABS = t => [
  {
    key: "chat",
    title: t("community:Chat", "Chat"),
    selected: false,
    onPress: (navigate) => () => navigate("chat"),
    canPress: ({ communityStore }) => {
      return communityStore.hasSelectedProfile()
    }
  },
  {
    key: "social",
    title: t("community:social", "Communauté"),
    selected: false,
    onPress: (navigate) => () => navigate("explore"),
    canPress: () => true
  }
]

export const GaoCommunity: React.FC<GaoCommunityProps> = props => {
  const { t } = useTranslation()
  const { currentTab, dismiss, children, navigate, hideClose } = props
  const rootStore = useStores()

  const currentTabs = TABS(t).filter((item, index) => !rootStore.isFirstStart || (rootStore.isFirstStart && index === 1)).map(tab => ({
    ...tab,
    selected: tab.key === currentTab,
    onPress: tab.canPress(rootStore) ? tab.onPress(navigate) : undefined
  }))

  return (
    <View style={styles.mainCommunityView}>
      <View style={styles.mainDialogContainerView}>
        {
          <ImageButton
            style={[styles.iconImageView, hideClose||!rootStore.isEndedChat?{opacity: 0.5}:null]}
            uri={require("assets/images/community/cross_icon.png")}
            onPress={dismiss}
            disabled={!rootStore.isEndedChat || hideClose}
          />
        }
        <TabView scrollable={false} theme="gao" tabs={currentTabs} Component={children} disabled={rootStore.isFirstStart || hideClose} />
      </View>
    </View>
  )
}
