import * as React from "react"
import { AppState } from "react-native"

type PlayBackState = {
}

type PlayBackProps = {
  song: string
  noRepeat?: boolean
  muteOnStart?: boolean
  mute?: boolean
}

export class PlayBack extends React.Component<PlayBackProps, PlayBackState> {

  sound = null
  whoosh = null
  volume = null

  constructor(props) {
    super(props)
    this.sound = require("react-native-sound")
    this.whoosh = null
    this.sound.setCategory("Playback")
    this.stop = this.stop.bind(this)
    this.handleAppStateChange = this.handleAppStateChange.bind(this)
    if(!this.props.muteOnStart) {
      this.start()
    }
    AppState.addEventListener("change",
      this.handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.handleAppStateChange)
    this.stop()
  }

  handleAppStateChange(state) {
    if(state === "active") {
      if(this.whoosh && this.volume) {
        this.whoosh.setVolume(this.volume)
      }
    }
    else if(state === "background") {
      if(this.whoosh) {
        this.volume = this.whoosh.getVolume()
        this.whoosh.setVolume(0)
      }
    }
  }

  restart(song) {
    this.stop()
    this.startSong(song)
  }

  start() {
    this.startSong(this.props.song)
  }

  startSong(song) {
    try {
      this.whoosh = new this.sound(song, this.sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log("failed to load the sound", error)
          return
        }
        // loaded successfully
        console.log("duration in seconds: " + this.whoosh.getDuration() + "number of channels: " + this.whoosh.getNumberOfChannels())

        if(this.props.mute) {
          this.whoosh.setVolume(0)
        }
        else if(song === "gao_main_sound.ogg") {
          this.whoosh.setVolume(0.5)
        } else {
          this.whoosh.setVolume(1)
        }

        if(this.props.noRepeat) {
          this.whoosh.setNumberOfLoops(0)
        } else
          this.whoosh.setNumberOfLoops(-1)

        // Play the sound with an onEnd callback
        this.whoosh.play((success) => {
          if (success) {
            console.log("successfully finished playing")
          } else {
            console.log("playback failed due to audio decoding errors")
          }
        })
      })
    } catch (e) {
      console.error(e)
    }
  }

  public stop() {
    try {
      if(this.whoosh) {
        this.whoosh.stop((stopped) => console.log(stopped))
        this.whoosh.release()
        this.whoosh.setVolume(0)
      }
    } catch(ex) {
      console.error(ex)
    }
  }

  render() {
    return (
      <></>
    )
  }
}
