import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { GaoTopbar } from "./gao-topbar"

declare let module

storiesOf("GaoTopbar", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        {/*<GaoTopbar />*/}
      </UseCase>
    </Story>
  ))
