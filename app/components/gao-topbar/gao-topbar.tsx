import * as React from "react"
import { View, Image, BackHandler, ViewStyle } from "react-native"
import { Text, ImageButton } from "../"
import { observer } from "mobx-react"
import { useStores } from "models/root-store"
import * as styles from "gao-theme/styles"
import { NavigationActions, NavigationScreenProp } from "react-navigation"
import { Notification } from "components/notification/notification"
import { useEffect } from "react"

export interface GaoTopbarProps extends NavigationScreenProp<{}> {
  onGoBack?: () => void,
  style?: ViewStyle,
  disableOptions?: boolean
}

export const GaoTopbar: React.FC<GaoTopbarProps> = observer(function (props: GaoTopbarProps) {
  const { navigation } = props
  const { onGoBack, style } = props
  // React hook to access the stores (usable only in a Functional Component)
  // For the class version, you have to use the context.
  const { metaGame, communityStore } = useStores()
  const rootStore = useStores()
  // We extract what we want.
  const { paws } = metaGame

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", () => {
      if (navigation.isFocused()) {
        if (!onGoBack) {
          rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
          navigation.goBack()
        }
        else
          onGoBack()
        return true
      }
      return false
    })
    return () => {
      handler.remove()
    }
  }
  )

  return (
    <View style={[styles.topBarView, style]}>
      <ImageButton
        style={[styles.iconTopImage, rootStore.isFirstStart?{opacity: 0.3}:null]}
        onPress={() => {
          if (!onGoBack) {
            rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
            navigation.goBack()
          }
          else
            onGoBack()
        }}
        uri={require("assets/images/memes/arrow.png")} // TODO: show message.png with (!) when waiting messages are available.
      />
      <Image style={[styles.iconTopImage, rootStore.isFirstStart?{opacity: 0.3}:null]} source={require("assets/images/pawns-counter/pawn.png")} />
      <Text style={[styles.topBarText, rootStore.isFirstStart?{opacity: 0.3}:null]}>{paws}</Text>
      <Notification preset='alert'>
        <ImageButton
          style={styles.iconTopImage}
          onPress={() =>
            rootStore.isFirstStart ? navigation.navigate("friends") :
              (communityStore.selectedProfile ? navigation.navigate("chat") :
                communityStore.friends.length === 0 ? navigation.navigate("explore") :
                  navigation.navigate("community"))}
          uri={require("assets/images/messages-notification/messages.png")} // TODO: show message.png with (!) when waiting messages are available.
        />
      </Notification>
      <ImageButton
        style={styles.iconTopImage}
        uri={require("assets/images/user-menu/hamburgerUser.png")}
        onPress={() => navigation.navigate("settings")}
        disabled={props.disableOptions}
      />
    </View>
  )
})
