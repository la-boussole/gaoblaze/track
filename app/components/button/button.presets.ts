import { ViewStyle} from "react-native"
import { color} from "../../gao-theme"
import {
  buttonGaoBaseText,
  buttonGaoBaseView,
  buttonGaoFormDisabled,
  buttonGaoFormEnabled,
  buttonGaoHomeView, buttonGaoLinkText,
  buttonGaoLinkView,
  buttonGaoTabBaseView,
  buttonGaoTabBaseViewSelected
} from "gao-theme/styles"
import {buttonBlazeBaseText, buttonBlazeBaseView, buttonBlaseTabBaseView} from "blaze-theme/styles"


export type ButtonPresetNames =
  "default" |
  "play" |
  "store" |
  "home" |
  "start" |
  "form_enabled" |
  "form_disabled" |
  "link" |
  "tab" |
  "tabSelected" |
  "community" |
  "developer"

export const viewPresets = {
  gao: {
    default: { ...buttonGaoBaseView, backgroundColor: color.palette.green } as ViewStyle,
    play: { ...buttonGaoBaseView, backgroundColor: color.palette.green } as ViewStyle,
    store: { ...buttonGaoBaseView, backgroundColor: color.palette.orange } as ViewStyle,
    home: { ...buttonGaoBaseView, backgroundColor: color.palette.lightBlue, ...buttonGaoHomeView } as ViewStyle,
    start: { ...buttonGaoBaseView, backgroundColor: color.palette.green } as ViewStyle,
    link: buttonGaoLinkView,
    form_enabled: buttonGaoFormEnabled,
    form_disabled: buttonGaoFormDisabled,
    tab: { ...buttonGaoTabBaseView } as ViewStyle,
    tabSelected: { ...buttonGaoTabBaseViewSelected } as ViewStyle,
    community: { ...buttonGaoBaseView, backgroundColor: color.palette.cyan } as ViewStyle,
    developer: { ...buttonBlazeBaseView, backgroundColor: "#000", marginVertical: 0, marginHorizontal: 0, borderWidth: 0} as ViewStyle,
  },
  blaze: {
    default: buttonBlazeBaseView,
    tab: { ...buttonBlazeBaseView, ...buttonBlaseTabBaseView, backgroundColor: color.transparent } as ViewStyle,
    tabSelected: { ...buttonBlazeBaseView, ...buttonBlaseTabBaseView } as ViewStyle,
  }
}

export const textPresets = {
  gao: {
    default: buttonGaoBaseText,
    play: buttonGaoBaseText,
    store: buttonGaoBaseText,
    home: buttonGaoBaseText,
    community: buttonGaoBaseText,
    form_enabled: buttonGaoBaseText,
    form_disabled: buttonGaoBaseText,
    link: buttonGaoLinkText,
    tab: buttonGaoBaseText,
    tabSelected: buttonGaoBaseText,
    developer: buttonBlazeBaseText,
  },
  blaze: {
    default: buttonBlazeBaseText,
    tab: buttonBlazeBaseText,
    tabSelected: buttonBlazeBaseText,
  }
}
