import * as React from "react"
import {TouchableOpacity, View} from "react-native"
import { Text } from "components/text/text"
import { viewPresets, textPresets } from "./button.presets"
import { ButtonProps } from "./button.props"
import { mergeAll, flatten } from "ramda"
import {palette} from "gao-theme/palette";

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Button(props: ButtonProps) {
  const {
    theme = "gao",
    preset = "default",
    tx,
    text,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    ...rest
  } = props // grab the props

  const viewStyle = mergeAll(flatten([viewPresets[theme][preset], styleOverride]))
  const textStyle = mergeAll(flatten([textPresets[theme][preset], textStyleOverride]))
  const content = children || <Text tx={tx} text={text} style={textStyle} />

  return (
      <TouchableOpacity style={viewStyle} {...rest}>
        {content}
      </TouchableOpacity>
  )
}
