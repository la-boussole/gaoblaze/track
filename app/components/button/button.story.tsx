import * as React from "react"
import { /* ViewStyle, TextStyle, */ Alert } from "react-native"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { Button } from "./button"

declare let module

storiesOf("Button", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Play" usage="The play button.">
        <Button text="Click It" preset="play" onPress={() => Alert.alert("pressed")} />
      </UseCase>
      <UseCase text="Store" usage="The store button.">
        <Button text="Click It" preset="store" onPress={() => Alert.alert("pressed")} />
      </UseCase>
      <UseCase text="Home" usage="The home button.">
        <Button text="Click It" preset="home" onPress={() => Alert.alert("pressed")} />
      </UseCase>
      <UseCase text="Start/Accept" usage="The Start/Accept button.">
        <Button text="Click It" preset="default" onPress={() => Alert.alert("pressed")} />
      </UseCase>
      <UseCase text="Disabled" usage="The disabled behaviour of the button.">
        <Button text="Click It" onPress={() => Alert.alert("pressed")} disabled />
      </UseCase>
    </Story>
  ))
