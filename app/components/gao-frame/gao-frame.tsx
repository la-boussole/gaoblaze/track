import * as React from "react"
import {AsyncStorage, Dimensions, Keyboard, View, ViewStyle} from "react-native"
import {Wallpaper} from "../"
import {useEffect} from "react"
import ViewOverflow from "react-native-view-overflow"
import * as LocalS from "local-storage"
import { useStores } from "models/root-store"

export interface GaoFrameProps {
  coords: number[] // [left, top, width, height]
  wallpaper: string
}

export const GaoFrame = (settings: GaoFrameProps) => Component => props => {
  const wallpaperSize = {width: 750, height: 1335}
  const windowSize = {width: parseFloat(LocalS.get("width")), height: parseFloat(LocalS.get("height"))}
  const widthRatio = windowSize.width / wallpaperSize.width
  const heightRatio = windowSize.height / wallpaperSize.height
  const {coords, wallpaper} = settings
  const rootStore = useStores()

  if(isNaN(windowSize.width) || isNaN(windowSize.height)) {
    rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
    props.navigation.navigate("preHome")
  }

  const frame: ViewStyle = {
    position: "absolute",
    left: widthRatio * coords[0] - (widthRatio * wallpaperSize.width - windowSize.width) / 2,
    top: heightRatio * coords[1] - (heightRatio * wallpaperSize.height - windowSize.height) / 2,
    width: widthRatio * coords[2],
    height: heightRatio * coords[3],
  }

  const [keyboardVisible, setKeyBoardVisible] = React.useState(0)
  const [changeFrame, setChangeFrame] = React.useState(frame)

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", _keyboardDidShow)
    Keyboard.addListener("keyboardDidHide", _keyboardDidHide)

    // cleanup function
    return () => {
      Keyboard.removeListener("keyboardDidShow", _keyboardDidShow)
      Keyboard.removeListener("keyboardDidHide", _keyboardDidHide)
    }
  }, [])

  const _keyboardDidShow = (e) => {
    setKeyBoardVisible(e.endCoordinates.height)
    const frame: ViewStyle = {
      position: "absolute",
      left: widthRatio * coords[0] - (widthRatio * wallpaperSize.width - windowSize.width) / 2,
      top: heightRatio * coords[1] - (heightRatio * wallpaperSize.height - windowSize.height) / 2 - 2*e.endCoordinates.height/3,
      width: widthRatio * coords[2],
      height: heightRatio * coords[3],
    }
    setChangeFrame(frame)
  }

  const _keyboardDidHide = () => {
    const frame: ViewStyle = {
      position: "absolute",
      left: widthRatio * coords[0] - (widthRatio * wallpaperSize.width - windowSize.width) / 2,
      top: heightRatio * coords[1] - (heightRatio * wallpaperSize.height - windowSize.height) / 2,
      width: widthRatio * coords[2],
      height: heightRatio * coords[3],
      overflow: "visible",
    }
    setChangeFrame(frame)
    setKeyBoardVisible(0)
  }

  return (
    <>
      {!isNaN(windowSize.width) && !isNaN(windowSize.height) && (
        <>
          <Wallpaper backgroundImage={wallpaper} preset="stretch" style={{top: -keyboardVisible/3}}/>
          <ViewOverflow style={changeFrame}>
            <Component {...props} frame={changeFrame}/>
          </ViewOverflow>
        </>
      )}
    </>
  )
}
