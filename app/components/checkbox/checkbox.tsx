import * as React from "react"
import { TouchableOpacity, View } from "react-native"
import { Text } from "../"
import { CheckboxProps } from "./checkbox.props"
import { mergeAll, flatten } from "ramda"
import * as gaoTheme from "gao-theme/styles"
import * as blazeTheme from "blaze-theme/styles"

const THEMES = { gao: gaoTheme, blaze: blazeTheme }

export function Checkbox(props: CheckboxProps) {
  const numberOfLines = props.multiline ? 0 : 1

  const theme = THEMES[props.theme]
  const rootStyle = mergeAll(flatten([theme.ROOT, props.style]))
  const outlineStyle = mergeAll(flatten([theme.OUTLINE, props.outlineStyle]))
  const fillStyle = mergeAll(flatten([theme.FILL, props.fillStyle]))

  const onPress = props.onToggle ? () => props.onToggle && props.onToggle(!props.value) : null

  return (
    <TouchableOpacity
      activeOpacity={1}
      disabled={!props.onToggle}
      onPress={onPress}
      style={rootStyle}
    >
      <View style={outlineStyle}>{props.value && <View style={fillStyle} />}</View>
      <Text text={props.text} tx={props.tx} numberOfLines={numberOfLines} style={theme.LABEL} />
    </TouchableOpacity>
  )
}
