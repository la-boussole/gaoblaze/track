import * as React from "react"
import {ScrollView, View} from "react-native"
import * as gaoTheme from "gao-theme/styles"
import * as blazeTheme from "blaze-theme/styles"
import {Button} from "components"

export interface TabViewProps {
  tabs: {
    key: string,
    title: string,
    onPress: () => void,
    selected?: boolean
  }[],
  Component: React.ReactNode,
  scrollable?: Boolean
  theme: "blaze" | "gao"
  disabled?: boolean
}

const THEMES = { gao: gaoTheme, blaze: blazeTheme }

export const TabView: React.FunctionComponent<TabViewProps> = props => {
  const theme = THEMES[props.theme]
  const {scrollable = true, disabled} = props
  const ChildComponent = props.Component
  const ContentView = scrollable ? ScrollView : View
  return (
    <View style={theme.wrapperView}>
      <View style={{flexDirection: "row"}}>
        {props.tabs.map(tab => (
          <Button
            disabled={disabled}
            key={tab.key}
            theme={props.theme}
            preset={tab.selected ? "tabSelected" : "tab"}
            text={tab.title}
            onPress={tab.onPress}
            style={disabled?{opacity:0.2}:null}
            textStyle={{fontSize: 18}}
          />
        ))}
      </View>
      <ContentView style={theme.wrapperView}>
        {ChildComponent}
      </ContentView>
    </View>
  )
}
