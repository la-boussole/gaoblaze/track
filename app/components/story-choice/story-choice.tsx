import * as React from "react"
import {Image, TextStyle, TouchableOpacity, View, ViewStyle} from "react-native"
import { Text } from "../"
import { ChoiceType } from "utils/chat-engine/types"
import {color, spacing} from "gao-theme"
import * as styles from "gao-theme/styles"


export interface StoryChoiceProps {
  /**
   * Text which is looked up via i18n.
   */
  tx?: string

  /**
   * The text to display if not using `tx` or nested components.
   */
  text?: string

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle,
  textStyle?: TextStyle,

  choice: ChoiceType,
  selected: boolean,
  selectedStyle?: ViewStyle
  textStyleSelected?: TextStyle
  icon?: any
}

/**
 * Stateless functional component for your needs
 *
 * Component description here for TypeScript tips.
 */
export function StoryChoice(props: StoryChoiceProps) {
  // grab the props
  const { choice, onChoose,  onPreChoose, selected, selectedStyle, textStyleSelected, icon, ...rest } = props

  return (
    <View style={selected?[styles.selectedChooseStyle, selectedStyle]:styles.chooseStyle}>
      {/* eslint-disable-next-line react-native/no-inline-styles */}
      <TouchableOpacity onPress={onPreChoose} style={[props.style, {flex: 1}]}>
        <Text style={[props.textStyle, selected?textStyleSelected:null]} text={typeof choice === "string" ? choice : choice.displayed} />
      </TouchableOpacity>
      {
        selected &&
        (<TouchableOpacity style={{
          borderTopWidth: spacing.border,
          borderColor: color.palette.black,
          height: "100%",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row"
        }} onPress={onChoose}>
          <Image style={{height: 30, resizeMode: "center", alignSelf: "center"}}
            source={icon?icon:require("assets/images/community/arrowRight.png")} />
        </TouchableOpacity>)
      }
    </View>
  )
}
