import * as React from "react"
import { View, Text, Image } from "react-native"
import * as styles from "gao-theme/styles"
import { ImageButton } from "components"
import { useStores } from "models/root-store"
import { useObserver } from "mobx-react"

type NotificationPreset = "alert" | "edit"

export interface NotificationProps {
  /*
  */
  isEdit?: boolean
  onPress: () => void
  show: boolean// FIXME
  preset?: NotificationPreset

}

// notif in case Alert
const renderNotifAlertMessage = props => {
  const { metaGame } = useStores()
  const isMessage = metaGame.unreadGaoNPC // let's test

  return useObserver(() => (
    <View>
      {props.children}
      { (isMessage.size > 0 && props.show) || (isMessage.size > 0 && props.show === undefined) ?
        <Image source={require("assets/images/messages-notification/notifAlert.png")} style={styles.notificationIcon}/>
        :<></>}
    </View>
  ))
}

// notif in case Edit (communtity > my_profile)
const renderNotifEdit = props => {
  const { onPress, show } = props
  return (
    show?
      <View>
        <ImageButton
          viewStyle={{zIndex: 100}}
          style={styles.editIcon}
          onPress={onPress}
          uri={require("assets/images/community/iconEdit.png")}
        />
        {props.children}
      </View>
      : (
        <View>
          {props.children}
        </View>
      )
  )
}
/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */

export function Notification(props: NotificationProps) {
  switch (props.preset) {
  case "alert": // alert message
    return renderNotifAlertMessage(props)
  case "edit": // community/my_profile
    return renderNotifEdit(props)
  }
}

