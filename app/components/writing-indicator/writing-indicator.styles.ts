import { ViewStyle } from "react-native"

export const writingIndicatorStyles = {
  WRAPPER: {
    justifyContent: "center"
  } as ViewStyle
}
