import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { WritingIndicator } from "./writing-indicator"

declare let module

storiesOf("WritingIndicator", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <WritingIndicator text="WritingIndicator" />
      </UseCase>
    </Story>
  ))
