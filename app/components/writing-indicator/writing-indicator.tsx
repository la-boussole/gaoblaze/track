import * as React from "react"
import { View, ViewStyle } from "react-native"
import { Text } from "../"
import { writingIndicatorStyles as styles } from "./writing-indicator.styles"
import {useTranslation} from "react-i18next";

export interface WritingIndicatorProps {
  /**
   * The name of the person currently writing something.
   */
  name: string

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
}

/**
 * Stateless functional component for your needs
 *
 * Component description here for TypeScript tips.
 */
export function WritingIndicator(props: WritingIndicatorProps) {
  // grab the props
  const { name, style, ...rest } = props
  const textStyle = { }
  const {t} = useTranslation()

  return (
    <View style={style} {...rest}>
      <Text text={`${name.replace("Blaze", "")} ${t("common:chat-write", "est en train d'écrire...")}`} style={style} />
    </View>
  )
}
