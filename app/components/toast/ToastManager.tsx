import React, {useEffect, useRef, useState} from "react"
import { Animated, Image, Vibration, View } from "react-native"
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationScreenProp } from 'react-navigation';
import { useStores } from '../../models/root-store';
import { PlayBack } from '../sound/play-back';
import { Text } from '../text/text';
import FileViewer from "react-native-file-viewer"


export type ToastManagerMessage = {
  message: string;
  title: string;
  vibrate: boolean;
  largeIcon: any;
  userInfo: any;
  soundName: string;
}

type ToastManagerProps = {
  message: ToastManagerMessage;
  navigation: NavigationScreenProp<{}>
}

export const ToastManager: React.FunctionComponent<ToastManagerProps> = props => {

  const navigation = props.navigation
  const message = props.message;
  const rootStore = useStores()
  const [top] = useState(new Animated.Value(-130))

  useEffect(() => {
    if(message.vibrate)
      Vibration.vibrate();
    Animated.timing(
      top,
      {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }
    ).start();
    setTimeout(() => {
      close(false)
    }, 4000)
  }, [top])

  const close = (execute) => {
    Animated.timing(
      top,
      {
        toValue: -130,
        duration: 300,
        useNativeDriver: false,
      }
    ).start((finish) => {
      if(finish.finished) {
        if(execute) {
          if(message.userInfo.tought) {
            navigation.navigate("notebook", {tought: message.userInfo.tought})
          } else if(message.userInfo && message.userInfo.name) {
            if(message.userInfo.passive&&!rootStore.isFixedChat) {
              navigation.navigate("explore")
            } else
            if(message.userInfo.isBlaze) {
              navigation.navigate("blazeChat", {character: message.userInfo.name,})
            } else if(!rootStore.isFixedChat) {
              navigation.navigate("chat", {character: message.userInfo.name})
            }
          }
          if(message.userInfo && message.userInfo.file) {
            FileViewer.open(message.userInfo.file)
            //Linking.openURL('file:'+notification.userInfo.file)
          }
        }
        rootStore.removeToast()
      }
    });
  }
  
  return (
    <Animated.View style={{width: "100%", position: "absolute", height: 130, top: top, left: 0, backgroundColor: "#000000aa", padding: 10, borderBottomEndRadius: 20, borderBottomStartRadius: 20, flexDirection: "column-reverse"}}>
      <PlayBack song={message.soundName} muteOnStart={false} noRepeat={true} />
      <TouchableOpacity onPress={() => {
        close(true)
      }}>
        <View style={{width: "100%", height: 100, backgroundColor: "white", alignSelf: "center", margin: 5, borderRadius: 20, padding: 10}}>
          <View style={{flexDirection: "row"}}>
            <Image source={require("assets/images/ic_launcher.png")} style={{width: 20, height: 20}} />
            <Text style={{flex: 1, marginLeft: 10, color: "gray", textAlign: "right"}} numberOfLines={1}>{message.title}</Text>
          </View>
          
            <View style={{flexDirection: "row"}}>
              <Text style={{flex: 1, marginRight: 10, textAlignVertical: "center"}} numberOfLines={3}>{message.message}</Text>
              <Image source={message.largeIcon} style={{width: 60, height: 60, resizeMode: "contain"}} />
            </View>
          
        </View>
      </TouchableOpacity>
    </Animated.View>)
}
