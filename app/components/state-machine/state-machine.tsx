import * as React from "react"

export const StateMachine = ({initialState}) => props => {
  const [currentState, setCurrentState] = React.useState(initialState)
  const DialogView = props.dialogs[currentState.dialog]
  return <DialogView {...props} state={currentState} setState={newState => {
    setCurrentState({...currentState, ...newState})
  }}/>
}
