import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { BlazeThoughtPopup } from "./blaze-thought-popup"

declare let module

storiesOf("BlazeThoughtPopup", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        {/* <BlazeThoughtPopup text="BlazeThoughtPopup" /> */}
      </UseCase>
    </Story>
  ))
