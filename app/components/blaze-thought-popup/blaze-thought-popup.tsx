import * as React from "react"
import {Image, Linking, ScrollView, View} from "react-native"
import {Button, Text} from "../"
import * as styles from "../../blaze-theme/styles"
import {useTranslation} from "react-i18next"
import {BlazeProfile} from "assets/community_members/blaze-profiles"
import {NavigationScreenProp} from "react-navigation"
import HyperLink from "react-native-hyperlink"
import {spacing} from "blaze-theme"

export interface BlazeThoughtPopupProps {
  characterInfo: BlazeProfile
  navigation: NavigationScreenProp<{}>
  onOk?: () => void
}

interface TextThought {
  text: string,
  url?: string
}

/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */
export const BlazeThoughtPopup: React.FunctionComponent<BlazeThoughtPopupProps> = props => {
  const {t} = useTranslation()
  const navigation:  NavigationScreenProp<{}> = props.navigation
  const characterInfo: BlazeProfile = props.characterInfo
  const onOk = props.onOk

  return (
    <>
      <View style={styles.popupWrapper}>
        <View style={[styles.wrapperView, {paddingVertical: 20}]}>
          <View style={[styles.centeredView, {flex: 0}]}>
            {/* TODO: background of thought icon is not transparent */}
            <Image style={styles.iconImage} source={require("assets/images/blaze-icons/thought.png")} />
          </View>
          <View style={{flex: 1}}>
            <Text style={{...styles.h2Text, ...styles.boldFontText, fontSize: spacing.medium_large}}>
              {t("blaze:thought", "Réflexion de Blaze")} {characterInfo.thought.title}
            </Text>
            <Text style={{...styles.h3TextLeft, ...styles.boldFontText}} numberOfLines={2}>
              {characterInfo.thought.overview}
            </Text>
            <ScrollView style={{flex: 1,}} contentContainerStyle={{flexWrap:"wrap", flexDirection: "row"}}>
              <HyperLink
                onPress={(url: string, text: string) => Linking.openURL(url)}
                linkDefault={false}
                linkStyle={styles.boldFontText}
                linkText={
                  url => {
                    const link = characterInfo.thought.links.find(link => link.link === url)
                    if(link)
                      return link.text
                    return url
                  }
                }
              >
                <Text style={styles.h4TextJustify}>
                  {characterInfo.thought.message}
                </Text>
              </HyperLink>
            </ScrollView>
          </View>
          <Button theme="blaze" preset="default" text={t("blaze:add-to-notebook", "Ajouter au carnet")}
            onPress={() => {
              if(!onOk) {
                navigation.navigate("database")
              }
              else onOk()
            }}/>
        </View>
      </View>
    </>
  )
}
