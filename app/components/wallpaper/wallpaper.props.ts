import {ImageStyle, LayoutChangeEvent} from "react-native"
import { WallpaperPresets } from "./wallpaper.presets"

export interface WallpaperProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ImageStyle

  blurRadious?: number

  /**
   * An optional background image to override the default image.
   */
  backgroundImage?: any

  /**
   * One of the different types of wallpaper presets.
   */
  preset?: WallpaperPresets

  onLayout?: (event: LayoutChangeEvent) => void

  disable?: boolean
}
