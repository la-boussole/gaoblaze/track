import React from "react"
import {Image, View} from "react-native"
import { presets } from "./wallpaper.presets"
import { WallpaperProps } from "./wallpaper.props"
import FastImage from "react-native-fast-image"

const defaultImage = require("assets/images/wallpaper/blaze/wallpaper_green.png")

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function Wallpaper(props: WallpaperProps) {
  // grab the props
  const {preset = "stretch", style: styleOverride, backgroundImage} = props

  // assemble the style
  const presetToUse = presets[preset] || presets.stretch
  const style = {...presetToUse, ...styleOverride}

  // figure out which image to use
  const source = backgroundImage || defaultImage

  return (!props.disable?<FastImage source={source} style={style} /*blurRadius={props.blurRadious}*/
    resizeMode={style.resizeMode==="contain"?FastImage.resizeMode.contain:
      style.resizeMode==="cover"?FastImage.resizeMode.cover:
        style.resizeMode==="center"?FastImage.resizeMode.center:
          FastImage.resizeMode.stretch} />:
    <View style={{width: "100%", height: "100%", position: "absolute"}}>
      <FastImage source={source} style={style} /*blurRadius={props.blurRadious}*/
        resizeMode={style.resizeMode==="contain"?FastImage.resizeMode.contain:
          style.resizeMode==="cover"?FastImage.resizeMode.cover:
            style.resizeMode==="center"?FastImage.resizeMode.center:
              FastImage.resizeMode.stretch}/>
      <View style={{width: "100%", height: "100%", position: "absolute", backgroundColor: "#ffffffcc"}} />
    </View>)
}
