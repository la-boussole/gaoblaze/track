import * as React from "react"
import { Image, ImageStyle, ViewStyle, View } from "react-native"
import * as styles from "gao-theme/styles"
import { color } from "gao-theme"

export interface GaoCharacterProps {
  uriImage?: any
  imageStyle?: ImageStyle
  positioning?: ViewStyle
  displayBottom?: boolean
  // imagePositioning?: number[] // TODO: (?) add a props 'imagePositioning' for the positioning of the cat in each screen
}

export function GaoCharacter(props: GaoCharacterProps) {
  const { uriImage, imageStyle, positioning, displayBottom=false } = props
  return (
    <View style={positioning}>
      <View style={displayBottom?styles.spacerView:{}} />
      <Image source={uriImage} style={imageStyle} />
    </View>
  )
}
