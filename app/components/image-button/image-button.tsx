import * as React from "react"
import {Image, TouchableOpacity, Text, TextStyle, ViewStyle, ImageSourcePropType, ImageStyle} from "react-native"

export interface ImageButtonProps {
  uri: ImageSourcePropType
  onPress: () => void
  text?: string
  style?: ImageStyle
  textStyle?: TextStyle
  viewStyle?: ViewStyle
  disabled?: boolean
  blurRadious?: number
  numberOfLines?: number
}

export function ImageButton(props: ImageButtonProps) {
  const {
    uri,
    onPress,
    text,
    style,
    textStyle,
    viewStyle,
    disabled,
    blurRadious,
    numberOfLines
  } = props
  return (
    <TouchableOpacity onPress={onPress} style={viewStyle} disabled={disabled}>
      <Image source={uri} style={style} blurRadius={blurRadious} />
      {text && (<Text numberOfLines={numberOfLines} textBreakStrategy={"simple"} style={textStyle}>{text}</Text>)}
    </TouchableOpacity>
  )
}
