import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { BlazeThought } from "./blaze-thought"

declare let module

storiesOf("BlazeThought", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        {/*<BlazeThought />*/}
      </UseCase>
    </Story>
  ))
