import * as React from "react"
import {View} from "react-native"
import {Text} from "../"
import * as styles from "blaze-theme/styles"
import {useTranslation} from "react-i18next"

export interface BlazeThoughtProps {
  index: number,
  thought: string
}

/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */
export const BlazeThought: React.FunctionComponent<BlazeThoughtProps> = props => {
  const {t} = useTranslation()
  return (
    <View style={styles.boxedView}>
      <Text style={styles.h2PurpleText}>{t("blaze:thought", "Thought")} {props.index}</Text>
      <Text style={styles.pText}>{props.thought}</Text>
    </View>
  )
}
