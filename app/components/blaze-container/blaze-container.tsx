import * as React from "react"
import {SafeAreaView} from "react-native"
import {Screen, Wallpaper} from "../"
import * as styles from "../../blaze-theme/styles"
import {useStores} from "models/root-store"
import {useObserver} from "mobx-react-lite"

/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */
//FIXME: not working
export interface BlazeContainerProps {
  isDebugMode: boolean
}

export const BlazeContainer = (Component, settings: BlazeContainerProps = { isDebugMode: null }) => props => {
  const {metaGame} = useStores()
  return useObserver(() =>
    <Screen style={styles.rootView} preset="scroll">
      <Wallpaper backgroundImage={settings.isDebugMode === null && metaGame.isDebugMode? require("assets/images/wallpaper/blaze/wallpaper_red.png") : require("assets/images/wallpaper/blaze/wallpaper_green.png")} />
      <SafeAreaView style={styles.containerView}>
        <Component {...props} />
      </SafeAreaView>
    </Screen>
  )
}
