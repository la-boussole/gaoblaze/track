import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { ScrollableList } from "./scrollable-list"

declare let module

storiesOf("ScrollableList", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        {/*<ScrollableList text="ScrollableList" />*/}
      </UseCase>
    </Story>
  ))
