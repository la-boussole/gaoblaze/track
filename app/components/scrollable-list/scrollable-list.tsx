import * as React from "react"
import { FlatList, ScrollView, TouchableOpacity } from "react-native"
import { Text } from "../"
import * as styles from "../../blaze-theme/styles"


type ScrollableListPreset = "debug"

export interface ScrollableListProps {
  preset?: ScrollableListPreset
  data: {
    key: string;
    selected?: boolean;
    enabled?: boolean;
    onPress?: () => void;
  }[]
}

/**
 * React.FunctionComponent for your hook(s) needs
 *
 * Component description here for TypeScript tips.
 */
const renderDefaultScrollableList: React.FunctionComponent<ScrollableListProps> = props => {
  return (
    <ScrollView style={styles.wrapperView}>
      <FlatList
        data={props.data}
        renderItem={({item}) => (
          <TouchableOpacity onPress={item.onPress}>
            <Text style={{
              ...styles.listItemText,
              ...(item.enabled ? styles.boldFontText : {}),
              ...(item.selected ? styles.secondaryColorText : {})
            }} numberOfLines={1}>{item.key}
            </Text>
          </TouchableOpacity>
        )
        }
      />
    </ScrollView>
  )
}

const renderDebugScrollableList: React.FunctionComponent<ScrollableListProps> = props => {
  return (
    <ScrollView style={styles.wrapperView_Red}>
      <FlatList
        data={props.data}
        renderItem={({item}) => (
          <TouchableOpacity onPress={item.onPress}>
            <Text style={{
              ...styles.listItemTextDebug,
              ...(item.enabled ? styles.boldFontText : {}),
              ...(item.selected ? styles.secondaryColorText : {})
            }} numberOfLines={1}>{item.key}
            </Text>
          </TouchableOpacity>
        )
        }
      />
    </ScrollView>
  )
}

export function ScrollableList(props: ScrollableListProps) {
  switch (props.preset) {
  case "debug": // screen database-debugMode
    return renderDebugScrollableList(props)
  default:
    return renderDefaultScrollableList(props)
  }
}
