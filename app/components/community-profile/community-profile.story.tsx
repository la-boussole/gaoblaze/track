import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { CommunityProfile } from "./community-profile"

declare let module

storiesOf("CommunityProfile", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <CommunityProfile text="CommunityProfile" />
      </UseCase>
    </Story>
  ))
