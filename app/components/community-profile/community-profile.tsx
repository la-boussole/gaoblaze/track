import * as React from "react"
import { Image, ImageSourcePropType, ScrollView, View } from "react-native"
import { ImageButton, Text } from "../"
import * as styles from "gao-theme/styles"
import { useTranslation } from "react-i18next"
import { Notification } from "components/notification/notification"
import DropDownPicker from "react-native-dropdown-picker"
import { useStores } from "models/root-store"

export interface CommunityProfileProps {
  avatar: ImageSourcePropType
  name: string
  favoriteGame: string
  about: string
  goBack: () => void
  showNotif?: boolean
}

export const GAMES = [
  {label: "Gao the forger", value: "forger"},
  {label: "Gao the intruder", value: "intruder"},
  {label: "Gao the athlete", value: "athlete", selected: true},
  {label: "Gao the filekeeper", value: "filekeeper"},
  {label: "Gao the vagabond", value: "vagabond"},
  {label: "Gao the spy", value: "spy"},
  {label: "Gao the sage", value: "sage"},
  {label: "Gao the photographer", value: "photographer"},
  {label: "Gao the prosecutor", value: "prosecutor"},
]

export const CommunityProfile: React.FunctionComponent<CommunityProfileProps> = props => {
  const {avatar, name, favoriteGame, about, goBack, showNotif = true} = props
  const {t} = useTranslation()
  const { metaGame } = useStores()
  return (
    <>
      <View style={styles.mainCommunityView}>
        <View style={styles.mainDialogContainerView}>
          <ImageButton
            uri={require("assets/images/community/cross_icon.png")}
            style={styles.iconImageView}
            onPress={goBack}
          />
          <View style={styles.communityContainerView}>
            {/* name */}
            <Text style={styles.h1CenterText}>{name}</Text>
            <View style={styles.avatarView}>
              {/* avatar */}
              <Notification preset= 'edit' show={showNotif}>
                <Image source={avatar} style={styles.commuProfileAvatar} />
              </Notification>
              {/* favorite Game */}
              <View style={styles.profileInsightView}>
                <Text style={styles.h2CompactText}>{t("common:favorite-game","Jeu favori:")}</Text>
                {/* TODO: put in a preset with the 'edit' notification */}
                { showNotif?
                  <DropDownPicker
                    items={[
                      {label: "Gao the forger", value: "forger"},
                      {label: "Gao the intruder", value: "intruder"},
                      {label: "Gao the athlete", value: "athlete"},
                      {label: "Gao the filekeeper", value: "filekeeper"},
                      {label: "Gao the vagabond", value: "vagabond"},
                      {label: "Gao the spy", value: "spy"},
                      {label: "Gao the sage", value: "sage"},
                      {label: "Gao the photographer", value: "photographer"},
                      {label: "Gao the prosecutor", value: "prosecutor"},
                    ]}
                    //defaultValue={"forger"}
                    /*onChangeItem={item => {
                      metaGame.setFavoriteGame(item.label)
                    }}*/
                    //labelStyle={styles.h3CompactText}
                    containerStyle={{width: 200, height: 40}}
                    //arrowStyle={{marginRight: 0}}
                    //dropDownStyle={{elevation: 300}} // FIXME
                    // dropDownMaxHeight={100}
                    //zIndex={1000000}

                  />
                  :
                  <Text style={styles.h3CompactText}>{favoriteGame}</Text>
                }
              </View>
            </View>
            {/* Description */}
            <Notification preset= 'edit' show={showNotif}>
              <View></View>
            </Notification>
            <ScrollView style={styles.profileAboutMeView}>
              <Text style={styles.h4Text}>{t("common:about-me","Sur moi:")}</Text>
              <Text style={styles.h3Text}>{about}</Text>
              <Text>{/* Some space */}</Text>
              {/*<Text style={styles.h3CompactText}>{t("common:information_shared","Information shared:")}</Text>*/}
              {/*<Text style={styles.h3Text}>- {t("common:contact_list","Contact List")}</Text>*/}
              <Text>{/* Some space */}</Text>
            </ScrollView>
          </View>
        </View>
      </View>
    </>
  )
}
