import * as React from "react"
import { ImageStyle, View, ViewStyle } from "react-native"
import { GaoTopbar, Button, GaoCharacter } from "../"
import * as styles from "gao-theme/styles"
import { useTranslation } from "react-i18next"
import { NavigationScreenProp } from "react-navigation"

export interface GaoContainerProps {
  navigation: NavigationScreenProp<{}>
  children?: React.ReactNode
  character: any
  isCharacterCentered?: boolean
  positioning?: ViewStyle
  characterStyle?: ImageStyle,
  sendToFront?: boolean,
  disableOptions?: boolean
}

export const GaoContainer: React.FunctionComponent<GaoContainerProps> = props => {
  const { t } = useTranslation()
  const { children, character, navigation, isCharacterCentered, positioning, characterStyle, sendToFront } = props

  return (
    <>
      {!sendToFront &&
        <>
          <View style={styles.mainViewGames}>
            {children}
            {character && (
              <GaoCharacter
                uriImage={character}
                imageStyle={[isCharacterCentered ? styles.catCenterGif : styles.catRightGif, { ...characterStyle }]}
                displayBottom={true}
                positioning={positioning}
              //FIXME: Add styles for custom positioning: some cats are on the bottom-right and some not (see spy/athlete/prosecutor/photographer/intruder(?))
              //displayCenterHorizontal..
              //displayCenterVertical...
              />
            )}
          </View>
          <GaoTopbar navigation={navigation} disableOptions={props.disableOptions} />
        </>
      }
      {sendToFront &&
        <>
          <View style={styles.mainViewGames}>
            {children}
            <GaoTopbar navigation={navigation} style={{top: 0}} />
            {character && (
              <GaoCharacter
                uriImage={character}
                imageStyle={[isCharacterCentered ? styles.catCenterGif : styles.catRightGif, { ...characterStyle }]}
                displayBottom={true}
                positioning={positioning}
              //FIXME: Add styles for custom positioning: some cats are on the bottom-right and some not (see spy/athlete/prosecutor/photographer/intruder(?))
              //displayCenterHorizontal..
              //displayCenterVertical...
              />
            )}
          </View>
        </>
      }
    </>
  )
}
