import * as React from "react"
import {Image, PermissionsAndroid, TextStyle, View, ViewStyle} from "react-native"
import {Text, WritingIndicator} from "../"
import {useEffect, useState} from "react"
import * as styles from "gao-theme/styles"
import {IImageNode, ITextNode, IWaitNode} from "utils/chat-engine/types"
import {color} from "gao-theme"
import {color as colorBlaze} from "blaze-theme"
import CameraRoll from "@react-native-community/cameraroll"
import Contacts from "react-native-contacts"
import Geolocation from "@react-native-community/geolocation"
import {useTranslation} from "react-i18next"
import MapView from "react-native-maps"
import {PERMISSIONS} from "react-native-permissions"

export interface ChatMessageProps {
  /**
   * Text which is looked up via i18n.
   */
  tx?: string

  /**
   * The text to display if not using `tx` or nested components.
   */
  text?: string

  image?: string

  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle


  // If true, the message is allowed to perform wait/typing animations.
  // If false, the message is stripped from wait/typing animations.
  dynamic: boolean

  // Who wrote the message.
  writerName: string

  // Time to wait before showing the typing indicator.
  waitTime?: number

  // Callback once every effects were finished.
  onShown?: () => void

  // Word per minute for text.
  speed?: number

  message: ITextNode | IImageNode | IWaitNode

  imageChat?: any

  showName?: boolean

  highlight?: boolean

  textStyle?: TextStyle
  writingStyle?: TextStyle
  isBlaze?: boolean
}

interface TextChatMessageProps {
  text: string
  name: string
  dynamic: boolean
  speed: number // WPM.
  onShown?: () => void
  highlight?: boolean
  textStyle?: TextStyle
  writingStyle?: TextStyle
}

interface ImageChatMessageProps {
  image: string
  name: string
  onShown?: () => void
  isBlaze?: boolean
}

interface WaitChatMessageProps {
  children: React.FunctionComponentElement<ImageChatMessageProps | TextChatMessageProps>
  wait: number
}

// 38 WPM is the average.
const DEFAULT_AVG_WPM = 120

function getTimeToType(text: string, speed: number): number {
  if(!text)
    return 1
  return 100 * 60 * (text.split(" ").length) / speed
}

function TextChatMessage(props: TextChatMessageProps) {
  const {speed, text, name, dynamic, onShown, highlight, textStyle, writingStyle} = props
  const [showTyping, setTyping] = useState(dynamic)

  useEffect(() => {
    if (!showTyping && onShown) {
      onShown()
    }
  }, [showTyping, onShown])

  useEffect(() => {
    if (text && dynamic && showTyping) {
      const ttt = getTimeToType(text, speed)
      const timer = setTimeout(() => {
        setTyping(false)
      }, getTimeToType(text, speed))


      return () => clearTimeout(timer)
    }
  }, [dynamic, text, showTyping])

  return (
    <>
      {showTyping && (<WritingIndicator name={name} style={writingStyle} />)}
      {!showTyping && (<Text text={text} style={[name === "me" ? styles.sentMessage : styles.receivedMessage, {backgroundColor: highlight?color.primary:name === "me"?color.transparent:!textStyle?color.background:null}, textStyle]}/>)}
    </>
  )
}

function ImageChatMessage(props: ImageChatMessageProps) {
  if (props.onShown) useEffect(props.onShown)
  return (

    // eslint-disable-next-line react-native/no-inline-styles
    <View style={[props.name === "me" ? styles.sentMessage : styles.receivedMessage, {width: "85%", height: 250}, props.isBlaze?{backgroundColor:colorBlaze.fade}:null]}>
      <Image
        style={{width: "100%", resizeMode: "center", height: 230}}
        source={{uri: props.image}}
        onLoad={() => console.log("load", props.image)}
        onError={(error => console.log("error", props.image, error))}
      />
    </View>
  )
}

function WaitChatMessage(props: WaitChatMessageProps) {
  const {wait, children} = props

  const [showChildren, setShow] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => setShow(true), wait)
    return () => clearTimeout(timer)
  })

  if (showChildren) return children
  else return null
}

/**
 * A message line, which supports wait, image/GIF and text message.
 */
export function ChatMessage(props: ChatMessageProps) {
  // grab the props
  const {tx, message, style, dynamic, image, waitTime, writerName, onShown, imageChat, showName, highlight, textStyle, writingStyle, isBlaze, ...rest} = props

  const {t} = useTranslation()
  const isText = message ? message.type === "text" : !!rest.text
  const text = isText ? (rest.text || (message as ITextNode).text) : null
  const speed = rest.speed || DEFAULT_AVG_WPM
  const [messageInformation, setMessageInformation] = useState(null)

  useEffect(() => {
    if(((message.text && message.text.includes("<last_photo>")) || (message.type === "media" && message.path.length === 0)) && !messageInformation)
      getLastImage()
    if(message.text && message.text.includes("<random_contact>") && !messageInformation)
      getRandomContact()
    if(message.text && message.text.includes("<geolocalisation>") && !messageInformation)
      getGeolocation()
  }, [messageInformation])

  const getGeolocation = () => {
    PermissionsAndroid.check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
      if(result) {
        const watch = Geolocation.watchPosition(
          info => {
            Geolocation.clearWatch(watch)
            setMessageInformation(info)
            //Geocoder.init("AIzaSyAcjbW8TwIH_UwQoFbINoSgiVIlt4L4r1w")
            //Geocoder.from(info.coords).then((result) => console.log(result)).catch(ex => console.log(ex))
          },
          err => {
            //setMessageInformation("Error " + err)
          },
          {enableHighAccuracy: false, maximumAge: 0}
        )
      } else {
        setMessageInformation({error: true, message: t("common:error-chat", "¡Muy bien, protegiste tú información!")})
      }
    })
  }

  const getLastImage = () => {
    PermissionsAndroid.check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then(result => {
      if(result) {
        const fetchParams = {
          first: 1,
        }

        CameraRoll.getPhotos(fetchParams)
          .then(data => {
            const assets = data.edges
            const images = assets.map(asset => asset.node.image)

            // c.f. https://github.com/react-native-community/react-native-image-editor
            const image = images[0]

            setMessageInformation(image)

            // ImageEditor.cropImage(image.uri, cropData).then(url => {
          })
          .catch(err => console.error(err))
      } else {
        setMessageInformation({error: true, message: t("common:error-chat", "¡Muy bien, protegiste tú información!")})
      }
    })
  }

  const getRandomContact = () => {
    PermissionsAndroid.check(PERMISSIONS.ANDROID.READ_CONTACTS).then(result => {
      if(result) {
        Contacts.getAll((error, contacts) => {
          const contactsFull = contacts.filter(item => item.phoneNumbers.length > 0)
          const contact = contactsFull[Math.round(Math.random() * 10000) % contactsFull.length]
          setMessageInformation(contact)
        })
      } else {
        setMessageInformation({error: true, message: t("common:error-chat", "¡Muy bien, protegiste tú información!")})
      }
    })
  }

  const remainingText = isText?message.text.replace("<last_photo>", "").replace("<random_contact>", "").replace("<geolocalisation>", "").trim():""

  const contents = (
    isText
      ? <View>
        {
          message.text.includes("<last_photo>") ?
            messageInformation&&messageInformation.error?<TextChatMessage speed={speed} text={messageInformation.message} dynamic={false} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} /> :
            messageInformation ? <ImageChatMessage image={messageInformation.uri} name={writerName} onShown={onShown}/> : <></>
            : message.text.includes("<random_contact>") ?
              messageInformation&&messageInformation.error?<TextChatMessage speed={speed} text={messageInformation.message} dynamic={false} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} /> :
                <TextChatMessage speed={speed} text={(messageInformation?messageInformation.displayName+" - "+(messageInformation.phoneNumbers.length > 0?messageInformation.phoneNumbers[0].number:"**********"):"")} dynamic={false} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} />
              : message.text.includes("<geolocalisation>") ?
                messageInformation&&messageInformation.error?<TextChatMessage speed={speed} text={messageInformation.message} dynamic={false} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} /> :
                  <TextChatMessage speed={speed} text={messageInformation&&messageInformation.coords?messageInformation.coords.latitude+","+messageInformation.coords.longitude:"loading..."} dynamic={false} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} />
                : <TextChatMessage speed={speed} text={text} dynamic={dynamic} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} />
        }
        {
          remainingText.length > 0 && (message.text.includes("<last_photo>") || message.text.includes("<random_contact>") || message.text.includes("<geolocalisation>")) && <TextChatMessage speed={speed} text={remainingText} dynamic={false} name={writerName} writingStyle={writingStyle} onShown={onShown} highlight={highlight} textStyle={textStyle} />
        }
      </View>
      : (message.type === "media" && message.path.length === 0) ?
        (messageInformation ? <ImageChatMessage isBlaze={isBlaze} image={messageInformation.uri} name={writerName} onShown={onShown}/> : <></>)
        : <ImageChatMessage image={message.path} name={writerName} isBlaze={isBlaze} onShown={onShown}/>
  )

  return (
    <View style={[style, {flexDirection: writerName !== "me" ? "row" : "row-reverse"}]} {...rest}>
      {
        writerName !== "me" && imageChat &&
        (<View style={{width: 30, overflow: "hidden", height: 30, marginTop: 10, marginRight: 10, borderRadius: 360, borderWidth: 1, borderColor: color.palette.black}}>
          <Image
            source={imageChat}
            style={{width: "100%", height: "100%", overflow: "hidden"}}
          />
        </View>)
      }
      {(waitTime && dynamic)
        ? (<WaitChatMessage wait={waitTime}>
          {contents}
        </WaitChatMessage>)
        : (<View style={{flexDirection: "column", flex: 1}}>
          { showName && (<Text>{writerName}</Text>)}
          <View style={{flex: 1}}>{contents}</View>
        </View>)
      }
    </View>
  )
}
