import * as React from "react"
import {View, Image, TextStyle, GestureResponderEvent, TouchableOpacity, ViewStyle, Linking} from "react-native"
import { Button } from "components/button/button"
import { Text } from "components/text/text"
import { useTranslation } from "react-i18next"
import * as styles from "gao-theme/styles"
import { TextInput } from "react-native-gesture-handler"
import { useStores } from "models/root-store"
import { useObserver } from "mobx-react"
import {color, spacing} from "gao-theme"
import { Timer} from "components"
import {useEffect, useRef, useState} from "react"
import { PlayBack } from "components/sound/play-back"
import {CheckBox} from "screens/community/settings";
import i18n from "i18next";
import {changeLanguage} from "i18n/i18n";
import HyperLink from "react-native-hyperlink"

// Component for the avatar used in dialog of "GameSuccess" screen:
const GaoAvatar = (props) => (
  <View style={styles.viewGaoAvatarView}>
    <Image source={props.uriImage} style={styles.defaultDialogImage} />
  </View>
)

type GaoDialogPreset = "score" | "advice" | "blaze-stream" | "live-announcement" | "ask-name" | "gift" | "download" | "image" | "language" | "language-warning" | "help-warning"

export interface GaoDialogProps {
  /**
   * Text which is looked up via i18n.
   */
  tx?: string
  dialogViewStyle?: ViewStyle

  /**
   * The text to display if not using `tx` or nested components.
   */
  textDialog1?: string
  textDialog2?: any

  textButton?: string

  /**
   * An optional style override useful for padding & margin.
   */
  textStyle1?: TextStyle
  textStyle2?: TextStyle

  viewStyle?: ViewStyle

  /**
   * URI for the image on top of the dialog
   */
  uriImage?: any // FIXME
  uriGiftImage?:any
  textMoney?: any

  /**
   * preset
   */
  preset?: GaoDialogPreset

  buttonOnPress?: (event: GestureResponderEvent) => void

  buttonOnCancelPress?: (event: GestureResponderEvent) => void

  navigation?: any // FIXME
}

// THIS CONCERNS ONLY THE 'YOU WIN' DIALOG
const renderDialogScore = props => {
  const { t } = useTranslation()
  const { uriImage, navigation, dialogViewStyle, textMoney } = props
  const { metaGame } = useStores()
  const rootStore = useStores()
  return (
    <View style={[styles.mainDialogScoreView, {...dialogViewStyle}]}>
      {/* Round image of Gao */}
      <GaoAvatar uriImage={uriImage} />
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      {/* Main text dialog */}
      <TouchableOpacity style={styles.mainDialogBoxView} onPress={() => {
        metaGame.setInGame(false)
        rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
        navigation.navigate("preHome")
      }}>
        <Text style={[styles.defaultPrimaryText, {fontSize: spacing.big}]}>{t("common:you-won", "TU AS GAGNÉ !")}</Text>
        <Text style={styles.defaultThirdText}>{t("common:you-earn", "Tu as obtenu :")}</Text>
        <View style={styles.pawnScoreView}>
          <Image
            source={require("assets/images/common/pawn.png")}
            style={styles.pawnDialogImage}
          />
          <Text style={styles.pawsScoreText}> x {textMoney}</Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

// THIS CONCERNS ONLY THE VAGABOND LAST DIALOG
const renderDialogBlazeStream = props => {
  const { t } = useTranslation()
  const { textDialog1, uriImage, navigation, dialogViewStyle } = props
  const rootStore = useStores()

  useEffect(() => {
    setTimeout(() => {
      rootStore.setSong("gao_main_sound.ogg", rootStore.homeMusic)
      navigation.navigate("specialLiveEvent")
    }, 5000)
  }, [])

  return (
    <View style={[styles.mainDialogView, {...dialogViewStyle}]} >
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      <View style={styles.viewFlagView}>
        <Image source={uriImage} style={styles.flagDialogImage} />
        {/*<Text style={styles.titleFlagText}>{t("common:stream", "CONGRATULATIONS!")}</Text>*/}
      </View>
      <View style={{...styles.mainDialogBoxView, marginTop: spacing.big}}>
        <Text style={styles.infoText}>{textDialog1}</Text>
      </View>
    </View>
  )
}

const renderDialogLiveAnnouncement = props => {
  const { t } = useTranslation()
  const { withTimer, textStyle2, textStyle1, textDialog1, uriImage, textButton, buttonOnPress, dialogViewStyle } = props
  const TextStyle1 = {...styles.titleFlagText, ...textStyle1, fontSize: 20, top: 10}
  const TextStyle2 = {...styles.defaultSecondaryText, ...textStyle2, fontSize: 22}
  const [disabled, setDisabled] = useState(withTimer)
  const rootStore = useStores()

  return (
    <View style={[styles.mainDialogView, {...dialogViewStyle}]}>
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      <View style={styles.viewFlagView}>
        <Image source={uriImage} style={styles.flagDialogImage} />
        <Text style={TextStyle1}>{t("common:stream", "Évènement Spécial Online !")}</Text>
      </View>
      <View style={[styles.mainDialogBoxView, {marginTop: spacing.huge, justifyContent: "space-evenly", width: "80%"}]}>
        {!disabled && <Text style={TextStyle2}>{textDialog1}</Text>}
        {
          withTimer && disabled && (<Timer playerGivenTime={120} textStyle={{marginTop: 10, marginBottom: 10, alignSelf: "center", color: "red", fontSize: 30}}
            finishTimer={() => setDisabled(false)} />)}
        <Button text={textButton} theme="gao" style={[styles.buttonView, {backgroundColor: disabled?color.palette.lightGrey:color.palette.green}]}
          disabled={disabled} onPress={buttonOnPress} />
      </View>
    </View>
  )
}

const renderDialogAskName = props => {
  const { metaGame } = useStores()
  const { textStyle2, textDialog2, textButton, buttonOnPress, dialogViewStyle } = props
  const rootStore = useStores()
  return useObserver(() => (
    <View style={[styles.mainDialogView, {...dialogViewStyle}]}>
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      <View style={styles.mainDialogBoxView}>
        <Text style={textStyle2}>{textDialog2}</Text>
        <TextInput
          style={styles.inputTextWelcome}
          maxLength={14}
          onChangeText={text => metaGame.setUserName(text)}
          value={metaGame.userName}
          underlineColorAndroid="transparent" // debug
        />
        { (!!metaGame.userName && metaGame.userName.length > 0) &&
          <Button text={textButton} theme="gao" style={styles.buttonView} onPress={buttonOnPress}/>
        }
      </View>
    </View>
  ))
}

const renderDialogLanguage = props => {
  const { metaGame } = useStores()
  const { textStyle2, textDialog2, textButton, buttonOnPress, dialogViewStyle } = props
  const rootStore = useStores()
  const [language, setLanguage] = React.useState(i18n.language)


  return useObserver(() => (
    <View style={[styles.mainDialogView, {...dialogViewStyle}]}>
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      <View style={styles.mainDialogBoxView}>
        <Text style={textStyle2}>{textDialog2}</Text>
        <View style={{flexDirection: "row-reverse", justifyContent: "flex-end"}}>
          <CheckBox text={"English"} checked={language === "en"} onPress={() => {
            rootStore.setSelectedLanguage("en")
            changeLanguage("en")
            setLanguage("en")
          }}/>
        </View>
        <View style={{flexDirection: "row-reverse", justifyContent: "flex-end"}}>
          <CheckBox text={"Français"} checked={language === "fr"} onPress={() => {
            rootStore.setSelectedLanguage("fr")
            changeLanguage("fr")
            setLanguage("fr")
          }}/>
        </View>
        <View style={{flexDirection: "row-reverse", justifyContent: "flex-end"}}>
          <CheckBox text={"Español"} checked={language === "es"} onPress={() => {
            rootStore.setSelectedLanguage("es")
            changeLanguage("es")
            setLanguage("es")
          }} />
        </View>
        <Button text={textButton} theme="gao" style={styles.buttonView} onPress={buttonOnPress}/>
      </View>
    </View>
  ))
}

const renderDialogAdvice = props => {
  const { textDialog2, textStyle2, viewStyle } = props
  const TextStyle2 = {...styles.defaultSecondaryText, ...textStyle2}
  const rootStore = useStores()
  return (
    <>
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      <View style={{...styles.adviceBoxView, ...viewStyle}}>
        <Text style={TextStyle2}>{textDialog2}</Text>
      </View>
    </>
  )
}

const renderDialogDefault = props => {
  const { textDialog1, textDialog2, textButton, textStyle1, textStyle2, uriImage, buttonOnPress, dialogViewStyle } = props
  const TextStyle1 = {...styles.defaultPrimaryText, ...textStyle1}
  const TextStyle2 = {...styles.defaultSecondaryText, ...textStyle2}
  const rootStore = useStores()
  return (
    <View style={[styles.mainDialogView, dialogViewStyle]}>
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      {uriImage && <GaoAvatar uriImage={uriImage} style={styles.defaultDialogImage} />}
      <View style={styles.mainDialogBoxView}>
        {textDialog1 && (<Text style={[TextStyle1, {fontSize: spacing.big}]}>{textDialog1}</Text>)}
        {textDialog2 && (<Text style={TextStyle2}>{textDialog2}</Text>)}
        {textButton && (<Button text={textButton} theme="gao" textStyle={{textAlign: "center"}} style={styles.buttonView} onPress={buttonOnPress} />)}
      </View>
    </View>
  )
}

const renderDialogGift = props => {
  const { t } = useTranslation()
  const { textDialog1, textMoney, uriImage, uriGiftImage, navigation } = props
  const rootStore = useStores()
  return (
    <View style={styles.mainDialogScoreView}>
      {/* Round image of Gao */}
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      <GaoAvatar uriImage={uriImage} />
      {/* Main text dialog */}
      <View style={styles.mainDialogBoxView}>
        <Text style={styles.defaultPrimaryText}>{textDialog1}</Text>
        <View style={styles.pawnScoreView}>
          <Image
            source={uriGiftImage}
            style={styles.pawnDialogImage}
          />
        </View>
        { /*<Text style={styles.defaultThirdText}>{t("common:you-earn", "You earned:")}</Text>
        <View style={styles.pawnScoreView}>
          <Image
            source={uriGiftImage}
            style={styles.pawnDialogImage}
          />
          <Text style={styles.pawsScoreText}> x {textMoney}</Text>
        </View>
        */ }
      </View>
    </View>
  )
}

const renderDialogImage = props => {
  const { t } = useTranslation()
  const { textDialog1, uriImage, navigation, textButton, buttonOnPress, imageStyle } = props
  const rootStore = useStores()
  return (
    <View style={styles.mainDialogScoreView}>
      {/* Round image of Gao */}
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      {/* Main text dialog */}
      <View style={styles.mainDialogBoxView}>
        <Text style={styles.defaultSecondaryText}>{textDialog1}</Text>
        <View style={styles.pawnScoreView}>
          <Image
            source={uriImage}
            style={imageStyle}
          />
        </View>
        <Button text={textButton} theme="gao" style={styles.buttonView} onPress={buttonOnPress} />
      </View>
    </View>
  )
}

const renderDialogLanguageWarning = props => {
  const { t } = useTranslation()
  const { textDialog1, uriImage, navigation, textButton, buttonOnPress, buttonOnCancelPress, imageStyle } = props
  const rootStore = useStores()
  return (
    <View style={[styles.mainDialogScoreView, {zIndex: 10000}]}>
      {/* Round image of Gao */}
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      {/* Main text dialog */}
      <View style={styles.mainDialogBoxView}>
        <Text style={styles.defaultSecondaryText}>{textDialog1}</Text>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <Button text={textButton} textStyle={{fontSize: 20}} theme="gao" style={[styles.buttonView, {flex: 1}]} onPress={buttonOnPress} />
          <Button text={t("common:cancel","Cancel")} textStyle={{fontSize: 18}} theme="gao" style={[styles.buttonView, {flex: 1, backgroundColor: color.palette.red}]} onPress={buttonOnCancelPress} />
        </View>
      </View>
    </View>
  )
}

const renderDialogHelpWarning = props => {
  const { t } = useTranslation()
  const { textDialog1, uriImage, navigation, textButton, buttonOnPress, buttonOnCancelPress, imageStyle } = props
  const rootStore = useStores()
  return (
    <View style={[styles.mainDialogScoreView, {zIndex: 10000}]}>
      {/* Round image of Gao */}
      { rootStore.soundMusic && <PlayBack song={"popup.ogg"} noRepeat={true} /> }
      {/* Main text dialog */}
      <View style={styles.mainDialogBoxView}>
        <Text style={styles.defaultSecondaryText}>{textDialog1}</Text>
        <View onTouchEndCapture={() => {
          if(i18n.language === "es")
            Linking.openURL("https://gaoandblaze.org/es/")
          else if(i18n.language === "en")
            Linking.openURL("https://gaoandblaze.org/en/")
          else
          Linking.openURL("https://gaoandblaze.org/")
        }}>
        <Text style={[styles.defaultSecondaryText, {color: "blue", textDecorationLine: "underline"}]}
          >gaoandblaze.org</Text>
          </View>
        <View style={{flexDirection: "row", justifyContent: "space-between", marginTop: 10}}>
          <Button text={textButton} textStyle={{fontSize: 20}} theme="gao" style={[styles.buttonView, {flex: 1}]} onPress={buttonOnPress} />
        </View>
      </View>
    </View>
  )
}

/**
 * Stateless functional component for your needs
 *
 * Component description here for TypeScript tips.
 */
export function GaoDialog(props: GaoDialogProps) {
  switch (props.preset) {
  case "advice": // Gao_photographer_advice
    return renderDialogAdvice(props)
  case "ask-name": // Ask name
    return renderDialogAskName(props)
  case "blaze-stream": // Vagabond last screen -> live-annoucement
    return renderDialogBlazeStream(props)
  case "live-announcement": // Home Screen for live announcement
    return renderDialogLiveAnnouncement(props)
  case "score": // GameSuccess
    return renderDialogScore(props)
  case "gift": // Gift Bought
    return renderDialogGift(props)
  case "image":
    return renderDialogImage(props)
  case "language":
    return renderDialogLanguage(props)
  case "language-warning":
      return renderDialogLanguageWarning(props)
  case "help-warning":
    return renderDialogHelpWarning(props)
  default:
    return renderDialogDefault(props)
  }
}
