import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { GaoDialog } from "./gao-dialog"

declare let module

storiesOf("GaoDialog", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <GaoDialog tx="GaoDialog" /* FIXME: add all needed props */ />
      </UseCase>
    </Story>
  ))
