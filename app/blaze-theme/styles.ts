import { Dimensions, ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { color, spacing, typography } from "blaze-theme/index"
import { palette } from "blaze-theme/palette"
import { communityContainerView, primaryFontText, profileInsightView } from "gao-theme/styles"
import { TextInput } from "react-native-gesture-handler"


/* *** */
export const DEBUG: ViewStyle = {
  borderWidth: 1,
  borderColor: "red",
}

/* *** Fonts *** */

export const defaultFontText: TextStyle = {
  fontFamily: typography.primary,
}

export const boldFontText: TextStyle = {
  fontFamily: typography.secondary,
}

export const secondaryColorText: TextStyle = {
  color: color.secondary,
}

/* *** Checkbox *** */

export const BASE_ROOT: ViewStyle = {
  flexDirection: "row",
  paddingVertical: spacing.border,
  alignSelf: "flex-start",
}
export const ROOT: ViewStyle = {
  ...BASE_ROOT,
  marginLeft: spacing.large,
}

export const BASE_DIMENSIONS = { width: 16, height: 16 }
export const DIMENSIONS = { ...BASE_DIMENSIONS }

export const BASE_OUTLINE: ViewStyle = {
  ...BASE_DIMENSIONS,
  marginTop: 2, // finicky and will depend on font/line-height/baseline/weather
  justifyContent: "center",
  alignItems: "center",
  borderWidth: 1,
  borderColor: color.primaryDarker,
  borderRadius: 1,
}
export const OUTLINE: ViewStyle = { ...BASE_OUTLINE }

export const BASE_FILL: ViewStyle = {
  width: DIMENSIONS.width - 4,
  height: DIMENSIONS.height - 4,
  backgroundColor: color.primary,
}
export const FILL: ViewStyle = { ...BASE_FILL }

export const BASE_LABEL: TextStyle = {
  fontFamily: typography.primary,
  color: color.primary,
  paddingLeft: spacing.tiny
}
export const LABEL: TextStyle = { ...BASE_LABEL }

/* *** Button *** */

export const buttonBlazeBaseView: ViewStyle = {
  paddingHorizontal: spacing.border,
  paddingVertical: spacing.smaller,
  marginHorizontal: spacing.medium,
  marginVertical: spacing.larger,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: palette.lightGreen,
  borderWidth: spacing.border,
  borderColor: color.line,
}

export const buttonBlaseTabBaseView: ViewStyle = {
  flex: 1,
  marginHorizontal: spacing.tiny,
  marginVertical: spacing.none,
  marginTop: spacing.small,
  borderBottomColor: color.transparent,
}

export const buttonBlazeBaseText: TextStyle = {
  paddingHorizontal: spacing.smaller,
  fontFamily: typography.secondary,
  fontSize: spacing.medium,
  color: palette.darkGreen,
}

/* *** Utils *** */

export const spacerView: ViewStyle = {
  flex: 1,
}

export const centeredView: ViewStyle = {
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
}

/* *** Wrappers *** */

export const rootView: ViewStyle = {
  flex: 1,
  backgroundColor: color.palette.black,
}

export const containerView: ViewStyle = {
  flex: 1,
  padding: spacing.small,
  margin: spacing.small,
  borderWidth: spacing.border,
  borderColor: color.dim,
}

export const wrapperView: ViewStyle = {
  flex: 1,
  borderWidth: spacing.border,
  borderColor: color.line,
}

export const imageZoom: ViewStyle = {
  width: "100%",
  height: "100%",
  position: "absolute",
  backgroundColor: color.background,
  left: 12,
  top: 20,
  zIndex: 100,
}

export const wrapperView_Red: ViewStyle = {
  ...wrapperView,
  borderColor: "#811A1A"
}

export const boxedView: ViewStyle = {
  flex: 1,
  marginVertical: spacing.smaller,
  marginHorizontal: spacing.small,
}

export const popupWrapper: ViewStyle = {
  position: "absolute",
  height: "100%",
  width: "100%",
  zIndex: 1,
  padding: spacing.large,
  backgroundColor: "#09121a",
}

/* *** Headings *** */

export const h1Text: TextStyle = {
  ...defaultFontText,
  textAlign: "center",
  fontSize: spacing.huge,
  color: color.primary,
}

export const h2Text: TextStyle = {
  ...h1Text,
  textAlign: "center",
  fontSize: spacing.large,
  paddingHorizontal: spacing.large,
  paddingVertical: spacing.small,
}

export const h3TextLeft: TextStyle = {
  ...h1Text,
  textAlign: "left",
  fontSize: spacing.medium,
}

export const h4TextLeft: TextStyle = {
  ...h1Text,
  textAlign: "left",
  fontSize: spacing.small,
}

export const h4TextJustify: TextStyle = {
  ...h1Text,
  textAlign: "justify",
  fontSize: spacing.small,
}

export const h2PurpleText: TextStyle = {
  ...h2Text,
  textAlign: "left",
}

export const h2RedText: TextStyle = {
  ...h2Text,
  color: "#A34837"
}

export const pText: TextStyle = {
  ...defaultFontText,
  fontSize: spacing.medium,
  paddingHorizontal: spacing.large,
  color: color.primaryDarker,
}

/* *** Images *** */

export const iconImage: ImageStyle = {
  width: spacing.huge,
  height: spacing.huge,
  resizeMode: "contain",
}

export const avatarImage: ImageStyle = {
  ...iconImage,
  width: "100%",
  height: "100%",
}

export const screenshotImage: ImageStyle = {
  ...avatarImage,
  marginHorizontal: spacing.smaller,
  marginVertical: spacing.small,
}

/* *** Misc *** */

export const topBarView: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}

export const passcodeText: TextStyle = {
  color: color.primaryDarker,
  backgroundColor: color.primary,
}

export const listItemText: TextStyle = {
  ...defaultFontText,
  fontSize: spacing.large,
  paddingHorizontal: spacing.large,
  color: color.primary,
}

export const listItemTextDebug: TextStyle = {
  ...listItemText,
  color: "#A34837"

}

// Blaze chat
export const blazeChatContainer: ViewStyle = {
  flex: 1,
}

export const blazeChatContainerTab: ViewStyle = {
  flex: 1,
  borderColor: color.primary,
  borderWidth: 1,
  padding: 2,
}

export const blazeChatTabs: ViewStyle = {
  flexDirection: "row",
}

export const blazeChatTabText: TextStyle = {
  fontFamily: typography.secondary,
  fontSize: spacing.medium_large,
  textAlign: "center",
  color: color.primary,
  flex: 1,
  textAlignVertical: "center",
}

export const blazeChatTabSelectedText: TextStyle = {
  ...blazeChatTabText,
  backgroundColor: color.primary,
  color: color.fade,
}

export const blazeChatTab: ViewStyle = {
  flex: 1,
  backgroundColor: color.fade,
  height: 50,
}

export const blazeChatTabSelected: ViewStyle = {
  ...blazeChatTab,
  backgroundColor: color.primary,
}

export const blazeChat: ViewStyle = {
  flex: 1,
}

export const blazeChatContainerView: ViewStyle = {
  flex: 1,
  paddingVertical: spacing.none,
  paddingHorizontal: spacing.none,
}

export const blazeChatMessagesView: ViewStyle = {
  marginTop: spacing.medium,
  paddingVertical: spacing.tiny,
  paddingHorizontal: spacing.large,
  borderRadius: spacing.small,
  backgroundColor: color.fade,
  marginHorizontal: spacing.none,
  flex: 1,
}

export const blazeChatStoryChoice: TextStyle = {
  ...h2PurpleText,
  fontSize: spacing.medium,
  borderTopWidth: spacing.border,
  borderColor: color.palette.black,
  color: color.primary,
  fontFamily: typography.secondary,
}

export const blazeChatStoryChoiceSelected: TextStyle = {
  ...blazeChatStoryChoice,
  color: color.fade,
}

export const blazeChatStoryChoiceView: TextStyle = {
  backgroundColor: color.fade,
  borderWidth: 1,
  borderColor: color.primary
}

export const blazeChatStoryChoiceViewSelected: TextStyle = {
  backgroundColor: color.primaryDarker,
  borderWidth: 1,
  borderColor: color.fade,
}

export const blazeChatMessage: TextStyle = {
  color: color.primaryDarker,
  backgroundColor: color.fade,
  borderColor: color.primaryDarker,
  borderWidth: 1,
  borderRadius: 0,
  fontFamily: typography.primary,
  fontSize: spacing.medium,
  fontWeight: "100",
  padding: spacing.small,
  textAlign: "right",
}

export const blazeChatMessageMe: TextStyle = {
  ...blazeChatMessage,
  borderWidth: 0,
  fontWeight: "bold"
}

export const h2TextTitleNotebook: TextStyle = {
  ...h1Text,
  textAlign: "center",
  fontSize: spacing.large,
  paddingLeft: spacing.large,
  paddingVertical: spacing.small,
}

export const notebookToughtContainer: ViewStyle = {
  width: "50%",
  borderColor: color.primary,
  borderWidth: 1,
  padding: 10,
  marginBottom: 0,
  justifyContent: "space-between",
}

export const notebookUnreadToughtContainer: ViewStyle = {
  backgroundColor: "white"
}

export const notebookToughtContainerText: ViewStyle = {
  width: "80%",
  padding: 10,
  alignSelf: "center",
  overflow: "hidden",
}

export const notebookToughtPopupTitle: ViewStyle = {
  flex: 1,
}

export const notebookToughtTitle: TextStyle = {
  flex: 1,
  textAlign: "center",
  color: color.primary,
  fontFamily: typography.primary,
  fontWeight: "bold",
  fontSize: 18,
  textAlignVertical: "center",
}

export const notebookSkillTitle: TextStyle = {
  ...notebookToughtTitle,
  borderColor: color.primary,
  padding: 5,
}

export const notebookSkillSubTitle: TextStyle = {
  ...notebookToughtTitle,
  borderColor: color.primary,
  padding: 5,
  fontWeight: "normal",
}

export const notebookToughtSubTitle: TextStyle = {
  flex: 1,
  textAlign: "center",
  color: color.primary,
  fontFamily: typography.primary,
  fontSize: 15,
  textAlignVertical: "center",
  fontWeight: "700"
}

export const notebookUnreadToughtTitle: TextStyle = {
}

export const skillContainer: ViewStyle = {
  flex: 1,
  flexDirection: "column",
}

export const skillButton: ViewStyle = {
  width: "60%",
  backgroundColor: color.primary,
}

export const skillButtonUnread: ViewStyle = {
  width: "60%",
  backgroundColor: "white",
}

export const skillButtonUnreadText: TextStyle = {
  color: color.primary,
}

export const skillContainerButton: ViewStyle = {
  flex: 1,
  paddingVertical: 1,
  width: "100%",
  flexDirection: "row",
  maxHeight: "34%",
}

export const skillLine: ViewStyle = {
  height: 1,
  backgroundColor: color.primary,
  alignSelf: "center"
}

export const skillLine1: ViewStyle = {
  ...skillLine,
  width: "10%",
}

export const skillLine2: ViewStyle = {
  ...skillLine,
  width: "25%",
}

export const skillLine3: ViewStyle = {
  ...skillLine,
  width: "40%",
}

export const skillButtonText: TextStyle = {
  flex: 1,
  textAlign: "center",
  color: color.fade,
  fontFamily: typography.primary,
  fontSize: 14,
  paddingVertical: 5,
  fontWeight: "bold"
}

export const skillTitle: TextStyle = {
  textAlign: "center",
  color: color.primary,
  fontFamily: typography.primary,
  fontSize: 15,
  fontWeight: "bold"
}

export const skillAbstract: TextStyle = {
  color: color.primary,
  fontFamily: typography.primary,
  fontSize: 15,
  marginVertical: 15,
  textAlign: "justify"
}

export const skillScreenShotContainer: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  marginBottom: 20,
}

export const skillScreenshot: ViewStyle = {
  width: "100%",
  height: 250,
  justifyContent: "center",
  alignItems: "center",
}

export const skillScreenshotText: ImageStyle = {
  resizeMode: "contain",
  width: "100%",
  height: "100%"
}

export const skillCheckBoxContainer: ViewStyle = {
  flexDirection: "row",
  alignItems: "center"
}

export const skillCheckBox: ViewStyle = {
  width: 15,
  height: 15,
  borderColor: color.primary,
  borderWidth: 1,
  margin: 5,
}

export const skillCheckBoxText: TextStyle = {
  color: color.primary,
  fontFamily: typography.primary,
  fontSize: 12,
}

export const skillDownload: ViewStyle = {
  margin: 20,
  alignSelf: "center",
  backgroundColor: color.primary,
  width: "80%",
  height: 30,
  alignItems: "center",
  justifyContent: "center",
}

export const skillDownloadText: TextStyle = {
  textAlign: "center",
  color: color.fade,
  fontFamily: typography.primary,
  fontSize: 15,
  fontWeight: "bold",
}

export const pdfContainer: ViewStyle = {
  justifyContent: "flex-start",
  alignItems: "center",
  marginTop: 25,
  width: "100%",
  height: "100%",
  backgroundColor: color.transparent
}

export const pdf = {
  flex: 1,
  width: "100%",
  height: "100%",
}

export const skillBodyContainer: ViewStyle = {
  flex: 1,
  padding: 10,
}

export const skillThoughtContainer: ViewStyle = {
  flexDirection: "row",
  marginVertical: 5,
  height: 90,
  alignItems: "center",
}

export const notebookToughtPopupContainer: ViewStyle = {
  flex: 1,
  flexDirection: "row"
}

// End game

export const lastMessageMainQuestionContainer: ViewStyle = {
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "100%",
}

export const lastMessageQuestionaryContainer: ViewStyle = {
  width: "85%",
  height: "95%",
  padding: 20,
}

export const lastMessageQuestionaryText: TextStyle = {
  color: color.palette.letterGreen,
  fontSize: 25,
  fontFamily: "Roboto-Black",
  alignSelf: "center"
}

export const lastMessageQuestionaryTextQuestion: TextStyle = {
  ...lastMessageQuestionaryText,
  fontSize: 25,
  alignSelf: "flex-start",
  marginTop: 20,
  marginBottom: 10,
}

export const lastMessageQuestionaryTextInput: TextStyle = {
  backgroundColor: color.line,
  borderRadius: 5,
  height: 40,
  fontFamily: "Roboto-Regular"
}

export const lastMessageQuestionaryMultiple: TextStyle = {
  backgroundColor: color.line,
  borderRadius: 5,
  borderWidth: 0,
  fontFamily: "Roboto-Regular"
}

export const lastMessageQuestionarySlideNumber: TextStyle = {
  color: color.palette.letterGreen,
  fontSize: 40,
  fontFamily: "Roboto-Regular"
}

export const lastMessageQuestionarySlideText: TextStyle = {
  color: color.palette.letterGreen,
  fontSize: 13,
  fontFamily: "Roboto-Regular",
  width: "48%",
}

export const lastMessageQuestionarySlideTextRight: TextStyle = {
  color: color.palette.letterGreen,
  fontSize: 13,
  fontFamily: "Roboto-Regular",
  width: "48%",
  textAlign: "right"
}


export const lastMessageQuestionarySlideNumbers: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between"
}

export const buttonQuestionaryContainer: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-around",
  marginTop: 20,
}

export const buttonQuestionary: ViewStyle = {
  backgroundColor: color.primary,
  padding: 10,
  borderRadius: 5,
  width: "48%",
  alignItems: "stretch"
}

export const lastMessageQuestionaryMultipleItem: TextStyle = {
  backgroundColor: color.line,
  borderWidth: 0,
  fontFamily: "Roboto-Regular"
}

export const lastMessageQuestionaryChecklistTouchView: ViewStyle =
  { flexDirection: "row", margin: 3, width: "45%" }

export const lastMessageQuestionaryChecklistTouch: ViewStyle =
  { flexDirection: "row",}

export const lastMessageQuestionaryChecklistImage: ImageStyle = {
  width: "100%",
  height: "100%",
  resizeMode: "stretch"
}

export const lastMessageQuestionaryMultipleItemSelected: TextStyle = {
  backgroundColor: color.fade,
  borderWidth: 0,
  color: color.line,
  textAlign: "left",
  fontFamily: "Roboto-Regular"
}

export const lastMessageQuestionaryChecklist: ViewStyle = {
  flexDirection: "row",
  flexWrap: "wrap",
  alignItems: "flex-start",
  flex: 1,
}

export const lastMessageQuestionaryQuestionsContainer: ViewStyle = {
  flex: 1,
  marginTop: 20,
}

export const lastMessageQuestionaryChecklistTouchImage: ViewStyle =
{width: 20, height: 20, borderWidth: 2, borderColor: color.palette.letterGreen, marginHorizontal: 5,}

export const lastMessageQuestionaryChecklistTouchText: TextStyle = {
  fontFamily: "Roboto-Regular",
  color: color.primary,
  fontSize: 15
}

export const lastMessageQuestionContainer: ViewStyle = {
  height: "80%",
  justifyContent: "space-around",
  alignItems: "center",
}

export const lastMessageQuestionTitle: TextStyle = {
  fontFamily: typography.primary,
  color: color.primary,
  fontSize: 32,
  textAlign: "center"
}

export const lastMessageQuestionButton: ViewStyle = {
  width: "80%",
  backgroundColor: color.primary,
  padding: 20,
}

export const lastMessageQuestionButtonText: TextStyle = {
  fontFamily: typography.secondary,
  color: color.background,
  fontSize: 20,
  textAlign: "center",
}

export const lastMessageEndingImage: ImageStyle = {
  flex: 1,
  resizeMode: "stretch",
  width: "100%",
}

export const lastMessageEndingContainerImageBox: ViewStyle = {
  height: 300,
  width: "100%",
}

export const lastMessageEndingImageBox: ImageStyle = {
  width: "100%",
  height: "100%",
  resizeMode: "contain",
  position: "absolute",
  top: 0,
  left: 0,
}

export const lastMessageEndingContainerText: ViewStyle = {
  width: "100%",
  height: "100%",
  padding: 30,
  justifyContent: "space-around"
}

export const lastMessageEndingText: TextStyle = {
  fontFamily: typography.primary,
  color: color.primary,
  fontSize: 24
}

export const lastMessageEndingButton: ViewStyle = {
  backgroundColor: color.primary,
  padding: 10,
  width: "50%",
  alignSelf: "center"
}

export const lastMessageEndingButtonText: TextStyle = {
  fontFamily: typography.secondary,
  color: color.background,
  fontSize: 18,
  textAlign: "center",
}

export const iconImageClosePopup: ImageStyle = {
  width: spacing.huge,
  height: spacing.huge,
  resizeMode: "contain",
}

export const iconImageClosePopupView: ViewStyle = {
  position: "absolute",
  right: 20,
  top: 20,
  zIndex: 100,
}
