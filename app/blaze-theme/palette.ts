export const palette = {
  black: "rgba(9,18,26,1.0)",
  darkGreen: "rgba(85,145,77,1.0)",
  lightGreen: "rgba(124,212,113,1.0)",
  pink: "rgba(226,73,128,1.0)",
  letterGreen: "#9de7c7",
  white: "#ffffff"
}
