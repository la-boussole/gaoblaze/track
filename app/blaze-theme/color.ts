import { palette } from "./palette"

/**
 * Roles for colors.  Prefer using these over the palette.  It makes it easier
 * to change things.
 *
 * The only roles we need to place in here are the ones that span through the app.
 *
 * If you have a specific use-case, like a spinner color.  It makes more sense to
 * put that in the <Spinner /> component.
 */
export const color = {
  /**
   * The palette is available to use, but prefer using the name.
   */
  palette,
  /**
   * A helper for making something see-thru. Use sparingly as many layers of transparency
   * can cause older Android devices to slow down due to the excessive compositing required
   * by their under-powered GPUs.
   */
  transparent: "rgba(0,0,0,0.0)",
  /**
   * Used as background in pop-up
   */
  fade: "rgba(0,0,0,0.9)",
  /**
   * The screen background.
   */
  background: palette.black,
  /**
   * The main tinting color.
   */
  primary: palette.lightGreen,
  /**
   * The main tinting color, but darker.
   */
  primaryDarker: palette.darkGreen,
  /**
   * The alternative tinting color.
   */
  secondary: palette.pink,
  /**
   * A subtle color used for borders and lines.
   */
  line: "rgba(85,145,77,0.6)",
  /**
   * Secondary information.
   */
  dim: "rgba(85,145,77,0.2)",

  scanBackground: "#131b22",

  scanBackgroundLoading: "#131b22cc",

  scanBackgroundGrid: "#091119",

  scanTextInput: "#212529",

  scanTextGrid: "#3d4245",
}
