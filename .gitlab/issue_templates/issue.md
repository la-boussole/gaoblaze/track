## Sommaire / Summary

(Résumez le bug rencontré en 4 ou 5 mots)
(Please describe the bug in 4 or 5 words)


## Équipement / Device

Votre modèle de téléphone / tablette / autre : 
Your smartphone / tablet / other model : 


## Étapes de reproduction / Reproducing steps

(Comment reproduire le problème - c'est très important)
(How to reproduce the bug - it's very important)


## Quel est le comportement actuel du bogue ? / What is the current bug behaviour?

(Ce qui se passe concrètement)
(What is actually happening)


## Quel est le comportement correct attendu ? / What is the expected behaviour?

(Ce que vous devriez voir à la place)
(What you should see instead)


## Captures d'écran et/ou messages d'erreur pertinents / Relevant screenshots and/or logs

(Si jamais vous avez l'habitude, collez tous les logs pertinents en utilisant des blocs de code (`comme ça`) pour formater la sortie de la console, les logs et le code car il est très difficile de les lire autrement.)
(If you're familiar with that, please paste all relevant logs with code blocks (`like that`) to format it in a user-friendly way)


## Corrections possibles

(Si vous le pouvez, créez un lien vers la ligne de code qui pourrait être responsable du problème)
(If you can, please attach a link to the responsible code line)

