{ stdenv, lib, config, callPackage, bash, gradle,
  androidEnvShellHook,
  nodejs, openjdk, unzip, zlib }:

{ finalDependencies, build-type, src, secrets-file ? "", # Path to the file containing secret environment variables
  env ? {} # Attribute set containing environment variables to expose to the build script
}:

let
  inherit (lib) attrByPath hasAttrByPath optionalAttrs;
  env' = env;
  inherit build-type;
  # FIXME(Raito): improve external configration usage, enable signature
  # FIXME(Raito): fix build number generation
  build-number = "2";
  gradle-opts = (attrByPath ["gradle-opts"] "" config);
  keystore-file = (attrByPath ["keystore-file"] "" config); # Path to the .keystore file used to sign the package
  baseName = "release-android";
  name = "track-react-build-${baseName}";
  gradleHome = "$NIX_BUILD_TOP/.gradle";
  localMavenRepo = "${finalDependencies.drv}/.m2/repository";
  sourceProjectDir = "${finalDependencies.drv}/project";
  envFileName =
    if (build-type == "release" || build-type == "nightly") then ".env.${build-type}" else ".env";
  buildType = if (build-type == "pr") then "pr" else "release";
  apksPath = "$sourceRoot/android/app/build/outputs/apk/${buildType}";

in stdenv.mkDerivation {
  inherit name src;
  nativeBuildInputs = [ bash gradle unzip ];
  buildInputs = [ nodejs openjdk ];
  phases = [ "unpackPhase" "buildPhase" "checkPhase" "installPhase" ];
  unpackPhase = ''
    runHook preUnpack

    cp -r $src ./project
    chmod u+w -R ./project

    export sourceRoot=$PWD/project
    export GRADLE_USER_HOME="${gradleHome}"

    runHook postUnpack
  '';
  postUnpack = ''
    mkdir -p ${gradleHome}

    echo "Created Gradle Home"

    ${if keystore-file != "" then "cp -a --no-preserve=ownership ${keystore-file} ${gradleHome}/; export TRACK_RELEASE_STORE_FILE=${gradleHome}/$(basename ${keystore-file})" else ""}

    # Ensure we have the right .env file
    cp -f $sourceRoot/${envFileName} $sourceRoot/.env

    echo "Environment file injected."

    # Copy android/ directory
    cp -a --no-preserve=ownership ${sourceProjectDir}/android/ $sourceRoot/
    chmod u+w $sourceRoot/android
    chmod u+w $sourceRoot/android/app
    mkdir $sourceRoot/android/build && chmod -R u+w $sourceRoot/android/build

    echo "Android folder injected."

    # Copy node_modules/ directory
    cp -a --no-preserve=ownership ${sourceProjectDir}/node_modules/ $sourceRoot/

    echo "Node modules injected."
    # Make android/build directories writable under node_modules
    for d in `find $sourceRoot/node_modules -type f -name build.gradle | xargs dirname`; do
      chmod -R u+w $d
    done

    echo "Permissions rewritten."
  '';
  buildPhase =
    let
      inherit (lib) concatStringsSep mapAttrsToList makeLibraryPath optionalString substring toUpper;
      # Take the env attribute set and build a couple of scripts
      #  (one to export the environment variables, and another to unset them)
      exportEnvVars = concatStringsSep ";" (mapAttrsToList (name: value: "export ${name}='${value}'") env');
      unsetEnvVars = concatStringsSep ";" (mapAttrsToList (name: value: "unset ${name}") env');
      adhocEnvVars = optionalString stdenv.isLinux "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${makeLibraryPath [ zlib ]}";
      capitalizedBuildType = toUpper (substring 0 1 buildType) + substring 1 (-1) buildType;
    in ''
    export HOME=$sourceRoot
    export ANDROID_SDK_HOME=$(mktemp -d)

    ${exportEnvVars}
    ${if secrets-file != "" then "source ${secrets-file}" else ""}

    export GRADLE_OPTS="${gradle-opts}"

    ${androidEnvShellHook}
    ${finalDependencies.shell.shellHook}
    
    # ensure permissions are fine.
    chmod +w -R $sourceRoot/android
    pushd $sourceRoot/android
    ${adhocEnvVars} gradle --stacktrace -Dmaven.repo.local='${localMavenRepo}' -PversionCode=${assert build-number != ""; build-number} --offline assemble${capitalizedBuildType} || exit
    popd > /dev/null

    ${unsetEnvVars}
  '';
  doCheck = true;
  checkPhase = ''
    ls ${apksPath}/*.apk | xargs -n1 unzip -qql | grep 'assets/index.android.bundle'
  '';
  installPhase = ''
    mkdir -p $out
    cp ${apksPath}/*.apk $out/

    mkdir $out/nix-support
    for i in $out/*.apk; do
      echo "file apk $i" >> $out/nix-support/hydra-build-products
    done
  '';
}
