{ pkgs ? import <nixpkgs> {} }:
with builtins;
with pkgs;
with lib;
{ root, packagefile ? "package.json", lockfile ? "package-lock.json", development ? false, shell ? false, global-packages ? [], override ? {}}:
  let
      supplementFile = writeText "supplement.json" (toJSON global-packages);
      converted = runCommand "convert-npm"
        {
          # FIXME(Raito): don't pass root, build the path package file and lock file from root and concat paths.
          inherit root packagefile lockfile;
          __noChroot = true;
          buildInputs = [ nodePackages.node2nix ];
        }
        ''
          mkdir -p "$out/"
          cp -r "$root/$packagefile" "$root/$lockfile" "$out/"
          chmod +w -R "$out"
          cd "$out"
          node2nix -i $packagefile -l $lockfile --supplement-input ${supplementFile} ${optionalString development "-d"}
        '';
      attrValue = (if shell == false then "package" else "shell");
    in
    (callPackage "${converted}" {})."${attrValue}".override override
