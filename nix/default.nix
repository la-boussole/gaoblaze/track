{ config ? {}, pkgs ? import ./pkgs.nix { inherit config; } }:
with pkgs;
let
    localMavenRepoBuilder = callPackage ./tools/maven-repo-builder.nix {
      inherit stdenv;
    };
    npmDeps = callPackage ./npm/dynamic-node2nix.nix {};
    gradle = callPackage ./gradle.nix {};
    androidEnv = callPackage ./android/android-env.nix {};
    mavenAndNpmDeps = callPackage ./merge-maven-and-npm.nix {
      inherit gradle localMavenRepoBuilder;
    };

    release = callPackage ./build-track-app.nix {
      inherit config gradle;
      androidEnvShellHook = androidEnv.shell.shellHook;
    };

    generate-maven-and-npm-deps-shell = callPackage ./maven/shell.nix {
      inherit gradle;
      androidEnvShellHook = androidEnv.shell.shellHook;
    };

    buildInputs = [
      mavenAndNpmDeps.drv openjdk gradle
    ];
  in {
    inherit release generate-maven-and-npm-deps-shell mavenAndNpmDeps npmDeps buildInputs;
    inherit (androidEnv) androidComposition;
}
