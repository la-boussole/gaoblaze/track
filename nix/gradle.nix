{ gradleGen, fetchurl }:

gradleGen.gradleGen rec {
  name = "gradle-5.5";
  nativeVersion = "0.17";

  src = fetchurl {
    url = "https://services.gradle.org/distributions/${name}-bin.zip";
    sha256 = "1g2104dmn10137z2qcqzd0z7gi31jw8ns661d78pmw77cgnv4y4d";
  };
}
