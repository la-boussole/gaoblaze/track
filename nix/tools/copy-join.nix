{ pkgs ? import <nixpkgs> {} }:
with pkgs;
with lib;
args_@{ name, paths, preferLocalBuild ? true, allowSubstitutes ? false, postBuild ? "", ... }:
let
  args = removeAttrs args_ [ "name" "postBuild" ]
  // { inherit preferLocalBuild allowSubstitutes; };
in
  runCommand name args
  ''
    mkdir -p $out
    for i in $paths; do
      echo copying $i
      cp -rv $i/* $out/
      chmod +w -R $out
    done
    ${postBuild}
  ''
