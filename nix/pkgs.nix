{ config ? {} }:

let
  defaultConfig = {
    android.sdk.accept_license = true;

    packageOverrides = pkgs: rec {
      nodejs = pkgs.nodejs-12_x;
    };

  };
  pkgs = (import <nixpkgs>) { config = defaultConfig // config; };
in
  pkgs
