{ mkShell, curl, git, gradle, jq, maven, nodejs, androidEnvShellHook }:

projectNodePackage: 
mkShell {
  buildInputs = [
    curl
    git
    gradle
    jq
    maven
    nodejs
    projectNodePackage
  ];

  shellHook = ''
    ${androidEnvShellHook}
  '';
}
