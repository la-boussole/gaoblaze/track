{ pkgs ? import <nixpkgs> {}, gradle ? pkgs.gradle, jre ? pkgs.jre }:
{
  mkGradleDependencies = { name, src, tasksPath ? ".", buildInputs ? [],
  depsHash ? "0000000000000000000000000000000000000000000000000000000000000000",
  depsTask ? "build" }: pkgs.stdenv.mkDerivation {
    name = "${name}-gradle-dependencies";
    inherit src;
    nativeBuildInputs = [ gradle pkgs.perl ] ++ buildInputs;

    buildPhase = ''
      export GRADLE_USER_HOME=$(mktemp -d)
      export ANDROID_HOME=$GRADLE_USER_HOME
      export USER_HOME=$ANDROID_HOME
      export HOME=$ANDROID_HOME
      cd ${tasksPath}
      gradle --no-daemon --refresh-dependencies androidDependencies
    '';

    installPhase = ''
      find $GRADLE_USER_HOME/caches/modules-2 -type f -regex '.*\.\(jar\|pom\)' \
        | perl -pe 's#(.*/([^/]+)/([^/]+)/([^/]+)/[0-9a-f]{30,40}/([^/\s]+))$# ($x = $2) =~ tr|\.|/|; "install -Dm444 $1 \$out/$x/$3/$4/$5" #e' \
        | sh
    '';

    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = "${depsHash}";
  };

  mkGradleInit = deps: pkgs.writeText "init.gradle" ''
    logger.lifecycle 'Replacing Maven repositories with ${deps}...'

    gradle.projectsLoaded {
      rootProject.allprojects {
        buildscript {
          repositories {
            clear()
            maven { url '${deps}' }
          }
        }
        repositories { 
          clear()
          maven { url '${deps}' }
        }
      }
    }
  '';
}



